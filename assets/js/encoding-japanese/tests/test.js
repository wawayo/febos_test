'use strict';

var assert = require('assert');
var fs = require('fs');
var encoding = require('../encoding');

describe('Encoding', function() {
  var encodings = ['SJIS', 'UTF-8', 'JIS', 'EUC-JP'];
  var urlEncoded = {
    SJIS:
      '%82%B1%82%CC%83e%83L%83X%83g%82%CD%20SJIS%20%82%C5%8F%91%82%A9%82%EA%82%C4%82%A2%82%DC%82%B7%81B',
    UTF8:
      '%E3%81%93%E3%81%AE%E3%83%86%E3%82%AD%E3%82%B9%E3%83%88%E3%81%AF%20UTF-8%20%E3%81%A7%E6%9B%B8%E3%81%8B%E3%82%8C%E3%81%A6%E3%81%84%E3%81%BE%E3%81%99%E3%80%82',
    JIS:
      '%1B%24B%243%24N%25F%25-%259%25H%24O%1B(B%20JIS%20%1B%24B%24G%3Dq%24%2B%24l%24F%24%24%24%5E%249!%23%1B(B',
    EUCJP:
      '%A4%B3%A4%CE%A5%C6%A5%AD%A5%B9%A5%C8%A4%CF%20EUC-JP%20%A4%C7%BD%F1%A4%AB%A4%EC%A4%C6%A4%A4%A4%DE%A4%B9%A1%A3'
  };

  var getExpectedName = function(name) {
    return name.replace(/\W/g, '');
  };

  var getExpectedText = function(name) {
    return (
      '\u3053\u306e\u30c6\u30ad\u30b9\u30c8\u306f ' +
      name +
      ' \u3067\u66f8\u304b\u308c\u3066\u3044\u307e\u3059\u3002'
    );
  };

  var getFileName = function(name) {
    return __dirname + '/encoding-' + getExpectedName(name).toLowerCase() + '.txt';
  };

  var getCode = function(data) {
    var code = [];
    for (var i = 0, len = data.length; i < len; i++) {
      code.push(data[i]);
    }
    return code;
  };

  var buffers = {};
  var tests = {};

  before(function() {
    tests.unicode = [];
    for (var i = 0; i <= 0xffff; i++) {
      tests.unicode.push(i);
    }
    tests.surrogatePairs = [0xd844, 0xde7b];

    var jisx0208 = fs.readFileSync(__dirname + '/jis-x-0208-utf8.txt');
    var jisx0208Len = jisx0208.length + 1;
    tests.jisx0208 = new Buffer(jisx0208Len);
    // Prepend an ascii character for UTF-16 detection.
    tests.jisx0208[0] = 'a'.charCodeAt(0);
    for (i = 1; i < jisx0208Len; i++) {
      tests.jisx0208[i] = jisx0208[i - 1];
    }
    assert.deepEqual(tests.jisx0208.slice(1), jisx0208);

    tests.jisx0208Array = [];
    var len = tests.jisx0208.length;
    for (i = 0; i < len; i++) {
      tests.jisx0208Array.push(tests.jisx0208[i]);
    }
    tests.ascii = 'Hello World.';
    tests.surrogatePairs2 = fs.readFileSync(__dirname + '/surrogate-pairs-utf8.txt');

    encodings.forEach(function(encodingName) {
      var data = fs.readFileSync(getFileName(encodingName));
      buffers[encodingName] = data;
    });
  });

  describe('detect', function() {
    encodings.forEach(function(encodingName) {
      it(encodingName, function() {
        var res = encoding.detect(buffers[encodingName]);
        assert.equal(res, getExpectedName(encodingName));
      });
    });

    it('UTF-16, UTF-16BE', function() {
      var utf16 = [
        0xfe,
        0xff,
        0x30,
        0x53,
        0x30,
        0x6e,
        0x30,
        0xc6,
        0x30,
        0xad,
        0x30,
        0xb9,
        0x30,
        0xc8,
        0x30,
        0x6f,
        0x00,
        0x20,
        0x00,
        0x55,
        0x00,
        0x54,
        0x00,
        0x46,
        0x00,
        0x2d,
        0x00,
        0x31,
        0x00,
        0x36,
        0x00,
        0x20,
        0x30,
        0x67,
        0x66,
        0xf8,
        0x30,
        0x4b,
        0x30,
        0x8c,
        0x30,
        0x66,
        0x30,
        0x44,
        0x30,
        0x7e,
        0x30,
        0x59,
        0x30,
        0x02
      ];
      assert(encoding.detect(utf16, 'utf-16'));
      assert(encoding.detect(utf16) === 'UTF16');

      var utf16_noBom = utf16.slice(2);
      assert(encoding.detect(utf16_noBom, 'utf-16'));
      assert(/^UTF16/.test(encoding.detect(utf16_noBom)));
    });

    it('UTF-16LE', function() {
      var utf16le = [
        0x53,
        0x30,
        0x6e,
        0x30,
        0xc6,
        0x30,
        0xad,
        0x30,
        0xb9,
        0x30,
        0xc8,
        0x30,
        0x6f,
        0x30,
        0x20,
        0x00,
        0x55,
        0x00,
        0x54,
        0x00,
        0x46,
        0x00,
        0x2d,
        0x00,
        0x31,
        0x00,
        0x36,
        0x00,
        0x4c,
        0x00,
        0x45,
        0x00,
        0x20,
        0x00,
        0x67,
        0x30,
        0xf8,
        0x66,
        0x4b,
        0x30,
        0x8c,
        0x30,
        0x66,
        0x30,
        0x44,
        0x30,
        0x7e,
        0x30,
        0x59,
        0x30,
        0x02,
        0x30
      ];
      assert(encoding.detect(utf16le, 'utf-16'));
      assert(encoding.detect(utf16le) === 'UTF16');
    });

    it('UTF-32, UTF-32BE', function() {
      var utf32 = [
        0x00,
        0x00,
        0xfe,
        0xff,
        0x00,
        0x00,
        0x30,
        0x53,
        0x00,
        0x00,
        0x30,
        0x6e,
        0x00,
        0x00,
        0x30,
        0xc6,
        0x00,
        0x00,
        0x30,
        0xad,
        0x00,
        0x00,
        0x30,
        0xb9,
        0x00,
        0x00,
        0x30,
        0xc8,
        0x00,
        0x00,
        0x30,
        0x6f,
        0x00,
        0x00,
        0x00,
        0x20,
        0x00,
        0x00,
        0x00,
        0x55,
        0x00,
        0x00,
        0x00,
        0x54,
        0x00,
        0x00,
        0x00,
        0x46,
        0x00,
        0x00,
        0x00,
        0x2d,
        0x00,
        0x00,
        0x00,
        0x33,
        0x00,
        0x00,
        0x00,
        0x32,
        0x00,
        0x00,
        0x00,
        0x20,
        0x00,
        0x00,
        0x30,
        0x67,
        0x00,
        0x00,
        0x66,
        0xf8,
        0x00,
        0x00,
        0x30,
        0x4b,
        0x00,
        0x00,
        0x30,
        0x8c,
        0x00,
        0x00,
        0x30,
        0x66,
        0x00,
        0x00,
        0x30,
        0x44,
        0x00,
        0x00,
        0x30,
        0x7e,
        0x00,
        0x00,
        0x30,
        0x59,
        0x00,
        0x00,
        0x30,
        0x02
      ];
      assert(encoding.detect(utf32, 'utf-32'));
      assert(encoding.detect(utf32) === 'UTF32');

      var utf32_noBom = utf32.slice(4);
      assert(encoding.detect(utf32_noBom, 'utf-32'));
      assert(/^UTF32/.test(encoding.detect(utf32_noBom)));
    });

    it('UTF-32LE', function() {
      var utf32le = [
        0x53,
        0x30,
        0x00,
        0x00,
        0x6e,
        0x30,
        0x00,
        0x00,
        0xc6,
        0x30,
        0x00,
        0x00,
        0xad,
        0x30,
        0x00,
        0x00,
        0xb9,
        0x30,
        0x00,
        0x00,
        0xc8,
        0x30,
        0x00,
        0x00,
        0x6f,
        0x30,
        0x00,
        0x00,
        0x20,
        0x00,
        0x00,
        0x00,
        0x55,
        0x00,
        0x00,
        0x00,
        0x54,
        0x00,
        0x00,
        0x00,
        0x46,
        0x00,
        0x00,
        0x00,
        0x2d,
        0x00,
        0x00,
        0x00,
        0x33,
        0x00,
        0x00,
        0x00,
        0x32,
        0x00,
        0x00,
        0x00,
        0x4c,
        0x00,
        0x00,
        0x00,
        0x45,
        0x00,
        0x00,
        0x00,
        0x20,
        0x00,
        0x00,
        0x00,
        0x67,
        0x30,
        0x00,
        0x00,
        0xf8,
        0x66,
        0x00,
        0x00,
        0x4b,
        0x30,
        0x00,
        0x00,
        0x8c,
        0x30,
        0x00,
        0x00,
        0x66,
        0x30,
        0x00,
        0x00,
        0x44,
        0x30,
        0x00,
        0x00,
        0x7e,
        0x30,
        0x00,
        0x00,
        0x59,
        0x30,
        0x00,
        0x00,
        0x02,
        0x30,
        0x00,
        0x00
      ];
      assert(encoding.detect(utf32le, 'utf-32'));
      assert(encoding.detect(utf32le) === 'UTF32');
    });

    it('Specifying multiple encodings', function() {
      var unicode = 'ユニコード';

      assert.equal(encoding.detect(unicode, 'UNICODE'), 'UNICODE');
      assert.equal(encoding.detect(unicode, ['UNICODE']), 'UNICODE');
      assert.equal(encoding.detect(unicode, { encoding: 'UNICODE' }), 'UNICODE');
      assert.equal(encoding.detect(unicode, { encoding: ['UNICODE'] }), 'UNICODE');
      assert.equal(encoding.detect(unicode, []), false);
      assert.equal(encoding.detect(unicode, ['UNICODE', 'ASCII']), 'UNICODE');
      assert.equal(encoding.detect(unicode, 'ASCII, EUCJP, UNICODE'), 'UNICODE');
      assert.equal(encoding.detect(unicode, ['SJIS', 'UTF8', 'ASCII']), false);
    });
  });

  describe('convert', function() {
    encodings.forEach(function(encodingName) {
      it(encodingName, function() {
        var res = encoding.codeToString(
          encoding.convert(buffers[encodingName], 'unicode', encodingName)
        );
        assert.equal(res, getExpectedText(encodingName));
      });
    });

    it('ASCII', function() {
      assert(tests.ascii.length > 0);
      var encoded = encoding.convert(tests.ascii, 'sjis', 'auto');
      assert(encoded.length > 0);
      var decoded = encoding.convert(encoded, 'unicode', 'auto');
      assert(decoded.length > 0);
      assert.deepEqual(decoded, tests.ascii);
    });

    it('Unicode/UTF-8', function() {
      assert(tests.unicode.length === 65536);
      var utf8 = encoding.convert(tests.unicode, 'utf-8', 'unicode');
      assert(utf8.length > 0);
      assert.notDeepEqual(utf8, tests.unicode);
      var unicode = encoding.convert(utf8, 'unicode', 'utf-8');
      assert(unicode.length === 65536);
      assert.deepEqual(unicode, tests.unicode);
    });

    it('Object arguments', function() {
      var text = getExpectedText(getExpectedName('UTF-8'));
      var data = encoding.stringToCode(text);
      assert(data.length > 0);
      assert(encoding.detect(data, 'UNICODE'));

      var utf8 = encoding.convert(data, {
        to: 'utf-8',
        from: 'unicode'
      });
      assert(utf8.length > 0);
      assert.notDeepEqual(utf8, data);
      assert(encoding.detect(utf8, 'utf-8'));

      var unicode = encoding.convert(utf8, {
        to: 'unicode',
        from: 'utf-8'
      });
      assert(unicode.length > 0);
      assert.deepEqual(unicode, data);
      assert(encoding.detect(unicode, 'unicode'));
    });

    it('Surrogate pairs', function() {
      assert(tests.surrogatePairs.length >= 2);
      var utf8 = encoding.convert(tests.surrogatePairs, 'utf-8', 'unicode');
      assert(utf8.length > 0);
      assert.notDeepEqual(utf8, tests.surrogatePairs);
      var unicode = encoding.convert(utf8, 'unicode', 'utf-8');
      assert(unicode.length >= 2);
      assert.deepEqual(unicode, tests.surrogatePairs);
    });

    it('Surrogate pairs and UTF-8 conversion', function() {
      var surrogatePairs = [
        83,
        117,
        114,
        114,
        111,
        103,
        97,
        116,
        101,
        32,
        80,
        97,
        105,
        114,
        115,
        32,
        84,
        101,
        115,
        116,
        10,
        55362,
        57271,
        37326,
        23478,
        12391,
        55399,
        56893,
        10
      ];
      var surrogatePairs_utf8 = [
        0x53,
        0x75,
        0x72,
        0x72,
        0x6f,
        0x67,
        0x61,
        0x74,
        0x65,
        0x20,
        0x50,
        0x61,
        0x69,
        0x72,
        0x73,
        0x20,
        0x54,
        0x65,
        0x73,
        0x74,
        0x0a,
        0xf0,
        0xa0,
        0xae,
        0xb7,
        0xe9,
        0x87,
        0x8e,
        0xe5,
        0xae,
        0xb6,
        0xe3,
        0x81,
        0xa7,
        0xf0,
        0xa9,
        0xb8,
        0xbd,
        0x0a
      ];
      var utf8 = encoding.convert(surrogatePairs, 'utf-8', 'unicode');
      assert(utf8.length > 0);
      assert.notDeepEqual(utf8, surrogatePairs);
      assert.deepEqual(utf8, surrogatePairs_utf8);

      var unicode = encoding.convert(utf8, 'unicode', 'utf-8');
      assert(unicode.length > 0);
      assert.notDeepEqual(unicode, utf8);
      assert.deepEqual(unicode, surrogatePairs);
    });

    it('Surrogate pairs and UTF-16 conversion', function() {
      var surrogatePairs = [];
      for (var i = 0; i < tests.surrogatePairs2.length; i++) {
        surrogatePairs.push(tests.surrogatePairs2[i]);
      }
      assert(surrogatePairs.length >= 2);
      var utf8 = encoding.convert(surrogatePairs, 'utf-8', 'unicode');
      assert(utf8.length > 0);
      assert.notDeepEqual(utf8, surrogatePairs);
      var unicode = encoding.convert(utf8, 'unicode', 'utf-8');
      assert(unicode.length >= 2);
      assert.deepEqual(unicode, surrogatePairs);

      var utf16 = encoding.convert(utf8, 'utf-16', 'utf-8');
      assert(utf16.length > 0);
      assert.notDeepEqual(utf16, utf8);
      var isUTF16 = encoding.detect(utf16, 'utf-16');
      assert(isUTF16);
      var c1 = utf16[0];
      var c2 = utf16[1];
      // Check BOM
      assert(!(c1 === 0xfe && c2 === 0xff && (c1 === 0xff && c2 === 0xfe)));
      var newUTF8 = encoding.convert(utf16, 'utf-8', 'utf-16');
      assert.deepEqual(utf8, newUTF8);
      var newUnicode = encoding.convert(utf16, 'unicode', 'utf-16');
      assert.deepEqual(newUnicode, unicode);
    });

    it('UTF-16 with BOM conversion', function() {
      var data = [];
      for (var i = 0; i < tests.surrogatePairs2.length; i++) {
        data.push(tests.surrogatePairs2[i]);
      }
      assert(data.length > 0);
      var utf8 = encoding.convert(data, 'utf-8', 'unicode');
      assert(utf8.length > 0);
      assert.notDeepEqual(utf8, data);
      var unicode = encoding.convert(utf8, 'unicode', 'utf-8');
      assert(unicode.length > 0);
      assert.deepEqual(unicode, data);

      // UTF-16 without BOM
      var utf16_noBom = encoding.convert(utf8, 'utf-16', 'utf-8');
      assert(utf16_noBom.length > 0);
      assert.notDeepEqual(utf16_noBom, utf8);

      var c1 = utf16_noBom[0];
      var c2 = utf16_noBom[1];
      // Check BOM
      assert(!(c1 === 0xfe && c2 === 0xff && (c1 === 0xff && c2 === 0xfe)));

      // Test detect
      var isUTF16 = encoding.detect(utf16_noBom, 'utf-16');
      assert(isUTF16);
      var isUTF16BE = encoding.detect(utf16_noBom, 'utf-16be');
      assert(isUTF16BE);
      var isUTF16LE = encoding.detect(utf16_noBom, 'utf-16le');
      assert(!isUTF16LE);

      // UTF-16 with BOM (BE)
      var utf16_bom_true = encoding.convert(utf8, {
        to: 'utf-16',
        from: 'utf-8',
        bom: true
      });

      c1 = utf16_bom_true[0];
      c2 = utf16_bom_true[1];
      // Check BOM
      assert(c1 === 0xfe && c2 === 0xff);

      // Test detect
      isUTF16 = encoding.detect(utf16_bom_true, 'utf-16');
      assert(isUTF16);
      isUTF16BE = encoding.detect(utf16_bom_true, 'utf-16be');
      assert(isUTF16BE);
      isUTF16LE = encoding.detect(utf16_bom_true, 'utf-16le');
      assert(!isUTF16LE);

      // Check other argument specified
      var utf16_bom_be = encoding.convert(utf8, {
        to: 'utf-16',
        from: 'utf-8',
        bom: 'be'
      });
      assert.deepEqual(utf16_bom_true, utf16_bom_be);

      var newUTF8 = encoding.convert(utf16_bom_be, 'utf-8', 'utf-16');
      assert.deepEqual(utf8, newUTF8);
      var newUnicode = encoding.convert(utf16_bom_be, 'unicode', 'utf-16');
      assert.deepEqual(newUnicode, unicode);

      // UTF-16 with BOM (LE)
      var utf16_bom_le = encoding.convert(utf8, {
        to: 'utf-16',
        from: 'utf-8',
        bom: 'le'
      });

      c1 = utf16_bom_le[0];
      c2 = utf16_bom_le[1];
      // Check BOM
      assert(c1 === 0xff && c2 === 0xfe);

      // Test detect
      isUTF16 = encoding.detect(utf16_bom_le, 'utf-16');
      assert(isUTF16);
      isUTF16BE = encoding.detect(utf16_bom_le, 'utf-16be');
      assert(!isUTF16BE);
      isUTF16LE = encoding.detect(utf16_bom_le, 'utf-16le');
      assert(isUTF16LE);

      newUTF8 = encoding.convert(utf16_bom_le, 'utf-8', 'utf-16');
      assert.deepEqual(utf8, newUTF8);
      newUnicode = encoding.convert(utf16_bom_le, 'unicode', 'utf-16');
      assert.deepEqual(newUnicode, unicode);
    });

    it('UTF-16BE conversion', function() {
      var data = [];
      for (var i = 0; i < tests.surrogatePairs2.length; i++) {
        data.push(tests.surrogatePairs2[i]);
      }
      assert(data.length > 0);
      var utf8 = encoding.convert(data, 'utf-8', 'unicode');
      assert(utf8.length > 0);
      assert.notDeepEqual(utf8, data);
      var unicode = encoding.convert(utf8, 'unicode', 'utf-8');
      assert(unicode.length > 0);
      assert.deepEqual(unicode, data);

      var utf16be = encoding.convert(utf8, 'utf-16be', 'utf-8');
      assert(utf16be.length > 0);
      assert.notDeepEqual(utf16be, utf8);

      var isUTF16BE = encoding.detect(utf16be, 'utf-16be');
      assert(isUTF16BE);
      var isUTF16 = encoding.detect(utf16be, 'utf-16');
      assert(isUTF16);
      var isUTF16LE = encoding.detect(utf16be, 'utf-16le');
      assert(!isUTF16LE);

      var c1 = utf16be[0];
      var c2 = utf16be[1];
      // Check BOM
      assert(!(c1 === 0xfe && c2 === 0xff && (c1 === 0xff && c2 === 0xfe)));
      var newUTF8 = encoding.convert(utf16be, 'utf-8', 'utf-16be');
      assert.deepEqual(utf8, newUTF8);
      var newUnicode = encoding.convert(utf16be, 'unicode', 'utf-16be');
      assert.deepEqual(newUnicode, unicode);
    });

    it('UTF-16LE conversion', function() {
      var data = [];
      for (var i = 0; i < tests.surrogatePairs2.length; i++) {
        data.push(tests.surrogatePairs2[i]);
      }
      assert(data.length > 0);
      var utf8 = encoding.convert(data, 'utf-8', 'unicode');
      assert(utf8.length > 0);
      assert.notDeepEqual(utf8, data);
      var unicode = encoding.convert(utf8, 'unicode', 'utf-8');
      assert(unicode.length > 0);
      assert.deepEqual(unicode, data);

      var utf16le = encoding.convert(utf8, 'utf-16le', 'utf-8');
      assert(utf16le.length > 0);
      assert.notDeepEqual(utf16le, utf8);

      var isUTF16LE = encoding.detect(utf16le, 'utf-16le');
      assert(isUTF16LE);
      var isUTF16 = encoding.detect(utf16le, 'utf-16');
      assert(isUTF16);
      var isUTF16BE = encoding.detect(utf16le, 'utf-16be');
      assert(!isUTF16BE);

      var c1 = utf16le[0];
      var c2 = utf16le[1];
      // Check BOM
      assert(!(c1 === 0xfe && c2 === 0xff && (c1 === 0xff && c2 === 0xfe)));
      var newUTF8 = encoding.convert(utf16le, 'utf-8', 'utf-16le');
      assert.deepEqual(utf8, newUTF8);
      var newUnicode = encoding.convert(utf16le, 'unicode', 'utf-16le');
      assert.deepEqual(newUnicode, unicode);
    });

    it('Halfwidth Katakana conversion', function() {
      var hankana = '｡｢｣､･ｦｧｨｩｪｫｬｭｮｯｰｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜﾝﾞﾟ';
      var hankanas = encoding.stringToCode(hankana);
      assert(hankanas.length > 0);
      assert(encoding.detect(hankanas) === 'UNICODE');

      var sjis_expect = [
        0xa1,
        0xa2,
        0xa3,
        0xa4,
        0xa5,
        0xa6,
        0xa7,
        0xa8,
        0xa9,
        0xaa,
        0xab,
        0xac,
        0xad,
        0xae,
        0xaf,
        0xb0,
        0xb1,
        0xb2,
        0xb3,
        0xb4,
        0xb5,
        0xb6,
        0xb7,
        0xb8,
        0xb9,
        0xba,
        0xbb,
        0xbc,
        0xbd,
        0xbe,
        0xbf,
        0xc0,
        0xc1,
        0xc2,
        0xc3,
        0xc4,
        0xc5,
        0xc6,
        0xc7,
        0xc8,
        0xc9,
        0xca,
        0xcb,
        0xcc,
        0xcd,
        0xce,
        0xcf,
        0xd0,
        0xd1,
        0xd2,
        0xd3,
        0xd4,
        0xd5,
        0xd6,
        0xd7,
        0xd8,
        0xd9,
        0xda,
        0xdb,
        0xdc,
        0xdd,
        0xde,
        0xdf
      ];

      var sjis = encoding.convert(hankanas, 'SJIS', 'UNICODE');
      assert(encoding.detect(sjis) === 'SJIS');
      assert.deepEqual(sjis, sjis_expect);

      var jis_expect = [
        0x1b,
        0x28,
        0x49,
        0x21,
        0x22,
        0x23,
        0x24,
        0x25,
        0x26,
        0x27,
        0x28,
        0x29,
        0x2a,
        0x2b,
        0x2c,
        0x2d,
        0x2e,
        0x2f,
        0x30,
        0x31,
        0x32,
        0x33,
        0x34,
        0x35,
        0x36,
        0x37,
        0x38,
        0x39,
        0x3a,
        0x3b,
        0x3c,
        0x3d,
        0x3e,
        0x3f,
        0x40,
        0x41,
        0x42,
        0x43,
        0x44,
        0x45,
        0x46,
        0x47,
        0x48,
        0x49,
        0x4a,
        0x4b,
        0x4c,
        0x4d,
        0x4e,
        0x4f,
        0x50,
        0x51,
        0x52,
        0x53,
        0x54,
        0x55,
        0x56,
        0x57,
        0x58,
        0x59,
        0x5a,
        0x5b,
        0x5c,
        0x5d,
        0x5e,
        0x5f,
        0x1b,
        0x28,
        0x42
      ];
      var jis = encoding.convert(hankanas, 'jis', 'unicode');
      assert(encoding.detect(jis) === 'JIS');
      assert.deepEqual(jis, jis_expect);

      var eucjp_expect = [
        0x8e,
        0xa1,
        0x8e,
        0xa2,
        0x8e,
        0xa3,
        0x8e,
        0xa4,
        0x8e,
        0xa5,
        0x8e,
        0xa6,
        0x8e,
        0xa7,
        0x8e,
        0xa8,
        0x8e,
        0xa9,
        0x8e,
        0xaa,
        0x8e,
        0xab,
        0x8e,
        0xac,
        0x8e,
        0xad,
        0x8e,
        0xae,
        0x8e,
        0xaf,
        0x8e,
        0xb0,
        0x8e,
        0xb1,
        0x8e,
        0xb2,
        0x8e,
        0xb3,
        0x8e,
        0xb4,
        0x8e,
        0xb5,
        0x8e,
        0xb6,
        0x8e,
        0xb7,
        0x8e,
        0xb8,
        0x8e,
        0xb9,
        0x8e,
        0xba,
        0x8e,
        0xbb,
        0x8e,
        0xbc,
        0x8e,
        0xbd,
        0x8e,
        0xbe,
        0x8e,
        0xbf,
        0x8e,
        0xc0,
        0x8e,
        0xc1,
        0x8e,
        0xc2,
        0x8e,
        0xc3,
        0x8e,
        0xc4,
        0x8e,
        0xc5,
        0x8e,
        0xc6,
        0x8e,
        0xc7,
        0x8e,
        0xc8,
        0x8e,
        0xc9,
        0x8e,
        0xca,
        0x8e,
        0xcb,
        0x8e,
        0xcc,
        0x8e,
        0xcd,
        0x8e,
        0xce,
        0x8e,
        0xcf,
        0x8e,
        0xd0,
        0x8e,
        0xd1,
        0x8e,
        0xd2,
        0x8e,
        0xd3,
        0x8e,
        0xd4,
        0x8e,
        0xd5,
        0x8e,
        0xd6,
        0x8e,
        0xd7,
        0x8e,
        0xd8,
        0x8e,
        0xd9,
        0x8e,
        0xda,
        0x8e,
        0xdb,
        0x8e,
        0xdc,
        0x8e,
        0xdd,
        0x8e,
        0xde,
        0x8e,
        0xdf
      ];

      var eucjp = encoding.convert(hankanas, 'eucjp', 'unicode');
      assert(encoding.detect(eucjp) === 'EUCJP');
      assert.deepEqual(eucjp, eucjp_expect);
    });

    it('JIS special table conversion', function() {
      //NOTE: This characters is not completed for mojibake.
      var chars = [
        0x0030,
        0x0020,
        0x007e,
        0x0020,
        0x005c,
        0x0020,
        0xffe5,
        0x0020,
        0xff5e,
        0x0020,
        0xffe3,
        0x0020,
        0xff02,
        0x0020,
        0x2015,
        0x0020,
        0xffe0,
        0x0020,
        0xffe1,
        0x0020,
        0xffe2,
        0x0020,
        0xffe4,
        0x0020,
        0xff07,
        0x0020,
        0x2225,
        0x0020,
        0x005c,
        0x0020,
        0x002f,
        0x0020,
        0xff3c,
        0x0020,
        0x0020,
        0x2116,
        0x0020,
        0x3231,
        0x0020,
        0x334d,
        0x0020,
        0x0061,
        0x0020,
        0x3042,
        0x0020,
        0x3087,
        0x0020,
        0xff71,
        0x0020,
        0x30a2,
        0x0020,
        0x30a1,
        0x0020,
        0x7533,
        0x0020,
        0x80fd,
        0x0020,
        0x5f0c,
        0x0020,
        0x4e9c,
        0x0020,
        0x7e3a,
        0x0020,
        0x7e67,
        0x0020,
        0x4eab,
        0x0020,
        0x7e8a,
        0x0020,
        0x8868,
        0x0020,
        0x2460,
        0x0020,
        0x2170,
        0x0020,
        0x2164
      ];

      ['JIS', 'SJIS', 'EUCJP', 'UTF8'].forEach(function(encodingName) {
        var encoded = encoding.convert(chars, {
          to: encodingName,
          from: 'auto'
        });
        assert(encoding.detect(encoded) === encodingName);
        assert(encoded.length > 0);

        var decoded = encoding.convert(encoded, {
          to: 'unicode',
          from: 'auto'
        });
        assert.deepEqual(decoded, chars);
      });
    });
  });

  describe('convert JIS-X-0208', function() {
    var encodingNames = ['UTF16', 'UTF16BE', 'UTF16LE', 'SJIS', 'EUCJP', 'JIS', 'UNICODE'];
    encodingNames.forEach(function(encodingName) {
      it('UTF8 to ' + encodingName, function() {
        assert(tests.jisx0208.length > 0);
        assert(encoding.detect(tests.jisx0208, 'utf-8'));
        assert(encoding.detect(tests.jisx0208) === 'UTF8');
        var encoded = encoding.convert(tests.jisx0208, {
          to: encodingName,
          from: 'utf-8'
        });
        assert(encoded.length > 0);
        assert(encoding.detect(encoded, encodingName));

        var detected = encoding.detect(encoded);
        if (/^UTF16/.test(encodingName)) {
          assert(/^UTF16/.test(detected));
        } else {
          assert(detected === encodingName);
        }

        var decoded = encoding.convert(encoded, {
          to: 'utf-8',
          from: encodingName
        });
        assert.deepEqual(decoded, tests.jisx0208Array);
      });
    });

    it('UTF-8 to Unicode', function() {
      var encoded = encoding.convert(tests.jisx0208, {
        to: 'unicode',
        from: 'utf-8'
      });
      assert(encoded.length > 0);
      assert(encoding.detect(encoded, 'unicode'));
      assert(encoding.detect(encoded) === 'UNICODE');
      tests.jisx0208_unicode = encoded;
    });

    encodingNames = ['UTF16', 'UTF16BE', 'UTF16LE', 'SJIS', 'EUCJP', 'JIS', 'UTF8'];
    encodingNames.forEach(function(encodingName) {
      it('UNICODE to ' + encodingName, function() {
        assert(tests.jisx0208_unicode.length > 0);
        assert(encoding.detect(tests.jisx0208_unicode, 'unicode'));
        assert(encoding.detect(tests.jisx0208_unicode) === 'UNICODE');
        var encoded = encoding.convert(tests.jisx0208_unicode, {
          to: encodingName,
          from: 'unicode'
        });
        assert(encoded.length > 0);
        assert(encoding.detect(encoded, encodingName));

        var detected = encoding.detect(encoded);
        if (/^UTF16/.test(encodingName)) {
          assert(/^UTF16/.test(detected));
        } else {
          assert(detected === encodingName);
        }

        var decoded = encoding.convert(encoded, {
          to: 'unicode',
          from: encodingName
        });
        assert.deepEqual(decoded, tests.jisx0208_unicode);
      });
    });

    it('Unicode to Shift_JIS', function() {
      var encoded = encoding.convert(tests.jisx0208, {
        to: 'sjis',
        from: 'utf-8'
      });
      assert(encoded.length > 0);
      assert(encoding.detect(encoded, 'sjis'));
      assert(encoding.detect(encoded) === 'SJIS');
      tests.jisx0208_sjis = encoded;
    });

    encodingNames = ['UTF16', 'UTF16BE', 'UTF16LE', 'UNICODE', 'EUCJP', 'JIS', 'UTF8'];
    encodingNames.forEach(function(encodingName) {
      it('SJIS to ' + encodingName, function() {
        assert(tests.jisx0208_sjis.length > 0);
        assert(encoding.detect(tests.jisx0208_sjis, 'sjis'));
        assert(encoding.detect(tests.jisx0208_sjis) === 'SJIS');
        var encoded = encoding.convert(tests.jisx0208_sjis, {
          to: encodingName,
          from: 'sjis'
        });
        assert(encoded.length > 0);
        assert(encoding.detect(encoded, encodingName));

        var detected = encoding.detect(encoded);
        if (/^UTF16/.test(encodingName)) {
          assert(/^UTF16/.test(detected));
        } else {
          assert(detected === encodingName);
        }

        var decoded = encoding.convert(encoded, {
          to: 'sjis',
          from: encodingName
        });
        assert.deepEqual(decoded, tests.jisx0208_sjis);
      });
    });

    it('Shift_JIS to EUC-JP', function() {
      var encoded = encoding.convert(tests.jisx0208, {
        to: 'eucjp',
        from: 'utf-8'
      });
      assert(encoded.length > 0);
      assert(encoding.detect(encoded, 'eucjp'));
      assert(encoding.detect(encoded) === 'EUCJP');
      tests.jisx0208_eucjp = encoded;
    });

    encodingNames = ['UTF16', 'UTF16BE', 'UTF16LE', 'UNICODE', 'SJIS', 'JIS', 'UTF8'];
    encodingNames.forEach(function(encodingName) {
      it('EUCJP to ' + encodingName, function() {
        assert(tests.jisx0208_eucjp.length > 0);
        assert(encoding.detect(tests.jisx0208_eucjp, 'eucjp'));
        assert(encoding.detect(tests.jisx0208_eucjp) === 'EUCJP');
        var encoded = encoding.convert(tests.jisx0208_eucjp, {
          to: encodingName,
          from: 'eucjp'
        });
        assert(encoded.length > 0);
        assert(encoding.detect(encoded, encodingName));

        var detected = encoding.detect(encoded);
        if (/^UTF16/.test(encodingName)) {
          assert(/^UTF16/.test(detected));
        } else {
          assert(detected === encodingName);
        }

        var decoded = encoding.convert(encoded, {
          to: 'eucjp',
          from: encodingName
        });
        assert.deepEqual(decoded, tests.jisx0208_eucjp);
      });
    });

    it('EUC-JP to JIS', function() {
      var encoded = encoding.convert(tests.jisx0208, {
        to: 'jis',
        from: 'utf-8'
      });
      assert(encoded.length > 0);
      assert(encoding.detect(encoded, 'jis'));
      assert(encoding.detect(encoded) === 'JIS');
      tests.jisx0208_jis = encoded;
    });

    encodingNames = ['UTF16', 'UTF16BE', 'UTF16LE', 'UNICODE', 'SJIS', 'EUCJP', 'UTF8'];
    encodingNames.forEach(function(encodingName) {
      it('JIS to ' + encodingName, function() {
        assert(tests.jisx0208_jis.length > 0);
        assert(encoding.detect(tests.jisx0208_jis, 'jis'));
        assert(encoding.detect(tests.jisx0208_jis) === 'JIS');
        var encoded = encoding.convert(tests.jisx0208_jis, {
          to: encodingName,
          from: 'jis'
        });
        assert(encoded.length > 0);
        assert(encoding.detect(encoded, encodingName));

        var detected = encoding.detect(encoded);
        if (/^UTF16/.test(encodingName)) {
          assert(/^UTF16/.test(detected));
        } else {
          assert(detected === encodingName);
        }

        var decoded = encoding.convert(encoded, {
          to: 'jis',
          from: encodingName
        });
        assert.deepEqual(decoded, tests.jisx0208_jis);
      });
    });
  });

  describe('convert JIS-X-0212', function() {
    var jisx0212_buffer = fs.readFileSync(__dirname + '/jis-x-0212-utf8.txt');
    var jisx0212_array = [];
    for (var i = 0, len = jisx0212_buffer.length; i < len; i++) {
      jisx0212_array.push(jisx0212_buffer[i]);
    }

    var jisx0212_sjis_buffer = fs.readFileSync(__dirname + '/jis-x-0212-sjis-to-utf8.txt');
    var jisx0212_sjis_array = [];
    for (var i = 0, len = jisx0212_sjis_buffer.length; i < len; i++) {
      jisx0212_sjis_array.push(jisx0212_sjis_buffer[i]);
    }

    it('UTF-8 to Unicode', function() {
      var encoded = encoding.convert(jisx0212_buffer, {
        to: 'unicode',
        from: 'utf-8'
      });
      assert(encoded.length > 0);
      assert(encoding.detect(encoded, 'unicode'));
      assert(encoding.detect(encoded) === 'UNICODE');
    });

    it('UTF-8 to SJIS / SJIS to UTF-8', function() {
      var encoded = encoding.convert(jisx0212_buffer, {
        to: 'sjis',
        from: 'utf-8'
      });
      assert(encoded.length > 0);
      assert(encoding.detect(encoded, 'sjis'));
      assert(encoding.detect(encoded) === 'SJIS');
      var encoded_sjis_to_utf8 = encoding.convert(encoded, {
        to: 'utf-8',
        from: 'sjis'
      });
      assert.deepEqual(encoded_sjis_to_utf8, jisx0212_sjis_array);
    });

    var encodingNames = ['UTF16', 'UTF16BE', 'UTF16LE', 'UNICODE', 'JIS', 'EUCJP', 'UTF8'];

    encodingNames.forEach(function(encodingName1) {
      var encoded1 = encoding.convert(jisx0212_array, {
        to: encodingName1,
        from: 'utf-8'
      });
      var detected = encoding.detect(encoded1);
      if (/^UTF16/.test(encodingName1)) {
        assert(/^UTF16/.test(detected));
      } else {
        assert(detected === encodingName1);
      }

      encodingNames.forEach(function(encodingName2) {
        it(encodingName1 + ' to ' + encodingName2, function() {
          var encoded2 = encoding.convert(encoded1, {
            to: encodingName2,
            from: encodingName1
          });
          assert(encoded2.length > 0);

          var detected2 = encoding.detect(encoded2);
          if (/^UTF16/.test(encodingName2)) {
            assert(/^UTF16/.test(detected2));
          } else {
            assert(detected2 === encodingName2);
          }

          var decoded = encoding.convert(encoded2, {
            to: 'utf-8',
            from: encodingName2
          });
          assert.deepEqual(decoded, jisx0212_array);
        });
      });
    });
  });

  describe('urlEncode/urlDecode', function() {
    encodings.forEach(function(encodingName) {
      it(encodingName, function() {
        var data = buffers[encodingName];
        var res = encoding.urlEncode(data);
        assert.equal(res, urlEncoded[getExpectedName(encodingName)]);
        assert.deepEqual(getCode(data), encoding.urlDecode(res));
      });
    });
  });

  describe('base64Encode/base64Decode', function() {
    encodings.forEach(function(encodingName) {
      it(encodingName, function() {
        var data = buffers[encodingName];
        var res = encoding.base64Encode(data);
        assert(typeof res === 'string');
        assert.equal(res, data.toString('base64'));
        assert.deepEqual(getCode(data), encoding.base64Decode(res));
      });
    });
  });

  describe('Assign/Expect encoding names', function() {
    var aliasNames = {
      'UCS-4': 'UTF32BE',
      'UCS-2': 'UTF16BE',
      UCS4: 'UTF32BE',
      UCS2: 'UTF16BE',
      'ISO 646': 'ASCII',
      CP367: 'ASCII',
      Shift_JIS: 'SJIS',
      'x-sjis': 'SJIS',
      'SJIS-open': 'SJIS',
      'SJIS-win': 'SJIS',
      'SHIFT-JIS': 'SJIS',
      SHIFT_JISX0213: 'SJIS',
      CP932: 'SJIS',
      'Windows-31J': 'SJIS',
      'MS-Kanji': 'SJIS',
      'EUC-JP-MS': 'EUCJP',
      'eucJP-MS': 'EUCJP',
      'eucJP-open': 'EUCJP',
      'eucJP-win': 'EUCJP',
      'EUC-JPX0213': 'EUCJP',
      'EUC-JP': 'EUCJP',
      eucJP: 'EUCJP',
      'ISO-2022-JP': 'JIS'
    };

    var text = getExpectedText(getExpectedName('UTF-8'));
    var data = encoding.stringToCode(text);
    assert(data.length > 0);
    assert(encoding.detect(data, 'UNICODE'));

    var sjis = encoding.convert(data, 'sjis');
    assert(sjis.length > 0);
    assert(encoding.detect(sjis, 'SJIS'));

    var eucjp = encoding.convert(data, 'EUCJP');
    assert(eucjp.length > 0);
    assert(encoding.detect(eucjp, 'EUCJP'));

    var codes = {
      SJIS: sjis,
      EUCJP: eucjp
    };

    Object.keys(aliasNames).forEach(function(name) {
      it(name + ' is ' + aliasNames[name], function() {
        var encoded = encoding.convert(data, name);
        assert(encoded.length > 0);
        var encodingName = aliasNames[name];
        if (encodingName in codes) {
          var code = codes[encodingName];
          assert(code.length > 0);
          assert.equal(encoding.detect(code), encodingName);
          assert.deepEqual(encoded, code);
        }
      });
    });
  });

  describe('Result types of convert/detect', function() {
    var string = getExpectedText(getExpectedName('UTF-8'));
    assert(string.length > 0);

    var array = encoding.stringToCode(string);
    assert(array.length > 0);
    assert(encoding.detect(array, 'UNICODE'));

    var isTypedArray = function(a) {
      return !Array.isArray(a) && a != null && typeof a.subarray !== 'undefined';
    };

    var isString = function(a) {
      return typeof a === 'string';
    };

    it('null/undefined', function() {
      var encoded = encoding.convert(null, 'utf-8');
      assert(encoded.length === 0);
      assert(Array.isArray(encoded));

      encoded = encoding.convert(void 0, 'utf-8');
      assert(encoded.length === 0);
      assert(Array.isArray(encoded));
    });

    it('array by default', function() {
      var encoded = encoding.convert([], 'utf-8');
      assert(encoded.length === 0);
      assert(Array.isArray(encoded));

      encoded = encoding.convert([1], 'utf-8');
      assert(encoded.length === 1);
      assert(Array.isArray(encoded));

      encoded = encoding.convert(new Array(), 'utf-8');
      assert(encoded.length === 0);
      assert(Array.isArray(encoded));

      var a = new Array(2);
      a[0] = 1;
      a[1] = 2;
      encoded = encoding.convert(a, 'utf-8');
      assert(encoded.length === 2);
      assert(Array.isArray(encoded));
    });

    it('Pass the string argument', function() {
      var encoded = encoding.convert('', 'utf-8');
      assert(encoded.length === 0);
      assert(isString(encoded));

      encoded = encoding.convert('123', 'utf-8');
      assert(encoded.length === 3);
      assert(isString(encoded));

      var utf8 =
        '\u00E3\u0081\u0093\u00E3\u0082\u0093\u00E3\u0081' +
        '\u00AB\u00E3\u0081\u00A1\u00E3\u0081\u00AF';

      var expect = '\u3053\u3093\u306B\u3061\u306F';

      encoded = encoding.convert(utf8, 'unicode', 'utf-8');
      assert(encoded.length > 0);
      assert(isString(encoded));
      assert.equal(encoded, expect);

      var detected = encoding.detect(utf8);
      assert.equal(detected, 'UTF8');
      detected = encoding.detect(expect);
      assert.equal(detected, 'UNICODE');
    });

    it('Specify { type: "array" }', function() {
      var encoded = encoding.convert(null, {
        to: 'utf-8',
        from: 'unicode',
        type: 'array'
      });
      assert(encoded.length === 0);
      assert(Array.isArray(encoded));

      encoded = encoding.convert(void 0, {
        to: 'utf-8',
        from: 'unicode',
        type: 'array'
      });
      assert(encoded.length === 0);
      assert(Array.isArray(encoded));

      encoded = encoding.convert('', {
        to: 'utf-8',
        from: 'unicode',
        type: 'array'
      });
      assert(encoded.length === 0);
      assert(Array.isArray(encoded));

      encoded = encoding.convert('123', {
        to: 'utf-8',
        from: 'unicode',
        type: 'array'
      });
      assert(encoded.length === 3);
      assert(Array.isArray(encoded));

      encoded = encoding.convert([], {
        to: 'utf-8',
        from: 'unicode',
        type: 'array'
      });
      assert(encoded.length === 0);
      assert(Array.isArray(encoded));

      encoded = encoding.convert([0x61, 0x62], {
        to: 'utf-8',
        from: 'unicode',
        type: 'array'
      });
      assert(encoded.length === 2);
      assert(Array.isArray(encoded));

      var buffer = new Buffer(0);
      encoded = encoding.convert(buffer, {
        to: 'utf-8',
        from: 'unicode',
        type: 'array'
      });
      assert(encoded.length === 0);
      assert(Array.isArray(encoded));

      buffer = new Buffer(2);
      buffer[0] = 0x61;
      buffer[1] = 0x62;
      encoded = encoding.convert(buffer, {
        to: 'utf-8',
        from: 'unicode',
        type: 'array'
      });
      assert(encoded.length === 2);
      assert(Array.isArray(encoded));

      buffer = new Uint8Array(0);
      encoded = encoding.convert(buffer, {
        to: 'utf-8',
        from: 'unicode',
        type: 'array'
      });
      assert(encoded.length === 0);
      assert(Array.isArray(encoded));

      buffer = new Uint8Array(2);
      buffer[0] = 0x61;
      buffer[1] = 0x62;
      encoded = encoding.convert(buffer, {
        to: 'utf-8',
        from: 'unicode',
        type: 'array'
      });
      assert(encoded.length === 2);
      assert(Array.isArray(encoded));
    });

    it('Specify { type: "arraybuffer" }', function() {
      var encoded = encoding.convert(null, {
        to: 'utf-8',
        from: 'unicode',
        type: 'arraybuffer'
      });
      assert(encoded.length === 0);
      assert(isTypedArray(encoded));

      encoded = encoding.convert(void 0, {
        to: 'utf-8',
        from: 'unicode',
        type: 'arraybuffer'
      });
      assert(encoded.length === 0);
      assert(isTypedArray(encoded));

      encoded = encoding.convert('', {
        to: 'utf-8',
        from: 'unicode',
        type: 'arraybuffer'
      });
      assert(encoded.length === 0);
      assert(isTypedArray(encoded));

      encoded = encoding.convert('123', {
        to: 'utf-8',
        from: 'unicode',
        type: 'arraybuffer'
      });
      assert(encoded.length === 3);
      assert(isTypedArray(encoded));

      encoded = encoding.convert([], {
        to: 'utf-8',
        from: 'unicode',
        type: 'arraybuffer'
      });
      assert(encoded.length === 0);
      assert(isTypedArray(encoded));

      encoded = encoding.convert([0x61, 0x62], {
        to: 'utf-8',
        from: 'unicode',
        type: 'arraybuffer'
      });
      assert(encoded.length === 2);
      assert(isTypedArray(encoded));

      var buffer = new Buffer(0);
      encoded = encoding.convert(buffer, {
        to: 'utf-8',
        from: 'unicode',
        type: 'arraybuffer'
      });
      assert(encoded.length === 0);
      assert(isTypedArray(encoded));

      buffer = new Buffer(2);
      buffer[0] = 0x61;
      buffer[1] = 0x62;
      encoded = encoding.convert(buffer, {
        to: 'utf-8',
        from: 'unicode',
        type: 'arraybuffer'
      });
      assert(encoded.length === 2);
      assert(isTypedArray(encoded));

      buffer = new Uint8Array(0);
      encoded = encoding.convert(buffer, {
        to: 'utf-8',
        from: 'unicode',
        type: 'arraybuffer'
      });
      assert(encoded.length === 0);
      assert(isTypedArray(encoded));

      buffer = new Uint8Array(2);
      buffer[0] = 0x61;
      buffer[1] = 0x62;
      encoded = encoding.convert(buffer, {
        to: 'utf-8',
        from: 'unicode',
        type: 'arraybuffer'
      });
      assert(encoded.length === 2);
      assert(isTypedArray(encoded));
    });

    it('Specify { type: "string" }', function() {
      var encoded = encoding.convert(null, {
        to: 'utf-8',
        from: 'unicode',
        type: 'string'
      });
      assert(encoded.length === 0);
      assert(isString(encoded));

      encoded = encoding.convert(void 0, {
        to: 'utf-8',
        from: 'unicode',
        type: 'string'
      });
      assert(encoded.length === 0);
      assert(isString(encoded));

      encoded = encoding.convert('', {
        to: 'utf-8',
        from: 'unicode',
        type: 'string'
      });
      assert(encoded.length === 0);
      assert(isString(encoded));

      encoded = encoding.convert('123', {
        to: 'utf-8',
        from: 'unicode',
        type: 'string'
      });
      assert(encoded.length === 3);
      assert(isString(encoded));

      encoded = encoding.convert([], {
        to: 'utf-8',
        from: 'unicode',
        type: 'string'
      });
      assert(encoded.length === 0);
      assert(isString(encoded));

      encoded = encoding.convert([0x61, 0x62], {
        to: 'utf-8',
        from: 'unicode',
        type: 'string'
      });
      assert(encoded.length === 2);
      assert(isString(encoded));

      var buffer = new Buffer(0);
      encoded = encoding.convert(buffer, {
        to: 'utf-8',
        from: 'unicode',
        type: 'string'
      });
      assert(encoded.length === 0);
      assert(isString(encoded));

      buffer = new Buffer(2);
      buffer[0] = 0x61;
      buffer[1] = 0x62;
      encoded = encoding.convert(buffer, {
        to: 'utf-8',
        from: 'unicode',
        type: 'string'
      });
      assert(encoded.length === 2);
      assert(isString(encoded));

      buffer = new Uint8Array(0);
      encoded = encoding.convert(buffer, {
        to: 'utf-8',
        from: 'unicode',
        type: 'string'
      });
      assert(encoded.length === 0);
      assert(isString(encoded));

      buffer = new Uint8Array(2);
      buffer[0] = 0x61;
      buffer[1] = 0x62;
      encoded = encoding.convert(buffer, {
        to: 'utf-8',
        from: 'unicode',
        type: 'string'
      });
      assert(encoded.length === 2);
      assert(isString(encoded));
    });
  });

  describe('Japanese Zenkaku/Hankaku conversion', function() {
    var hankakus = [
      'Hello World! 12345',
      '!"#$%&\'()*+,-./0123456789:;<=>?@' +
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~'
    ];

    var zenkakus = [
      'Ｈｅｌｌｏ Ｗｏｒｌｄ！ １２３４５',
      '！＂＃＄％＆＇（）＊＋，－．／０１２３４５６７８９：；＜＝＞？＠' +
        'ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ［＼］＾＿｀ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ｛｜｝～'
    ];

    var hankanas = [
      'ﾎﾞﾎﾟｳﾞｧｱｨｲｩｳｪｴｫｵ',
      '､｡｢｣ﾞﾟｧｱｨｲｩｳｪｴｫｵｶｷｸｹｺｻｼｽｾｿﾀﾁｯﾂﾃﾄ' + 'ﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓｬﾔｭﾕｮﾖﾗﾘﾙﾚﾛﾜｦﾝｳﾞヵヶﾜﾞｦﾞ･ｰ'
    ];

    var zenkanas = [
      'ボポヴァアィイゥウェエォオ',
      '、。「」゛゜ァアィイゥウェエォオカキクケコサシスセソタチッツテト' +
        'ナニヌネノハヒフヘホマミムメモャヤュユョヨラリルレロワヲンヴヵヶ\u30F7\u30FA・ー'
    ];

    var hiraganas = [
      'ぼぽ\u3094ぁあぃいぅうぇえぉお',
      '、。「」゛゜ぁあぃいぅうぇえぉおかきくけこさしすせそたちっつてと' +
        'なにぬねのはひふへほまみむめもゃやゅゆょよらりるれろわをんう゛\u3094\u3095\u3096わ゛を゛・ー'
    ];

    var katakanas = [
      'ボポヴァアィイゥウェエォオ',
      '、。「」゛゜ァアィイゥウェエォオカキクケコサシスセソタチッツテト' +
        'ナニヌネノハヒフヘホマミムメモャヤュユョヨラリルレロワヲンウ゛ヴヵヶ\u30F7\u30FA・ー'
    ];

    var hanspace = 'Hello World! 1 2 3 4 5';
    var zenspace = 'Hello\u3000World!\u30001\u30002\u30003\u30004\u30005';

    it('toHankakuCase', function() {
      zenkakus.forEach(function(zenkaku, i) {
        var expect = hankakus[i];
        var res = encoding.toHankakuCase(zenkaku);
        assert.equal(res, expect);

        var zenkakuArray = encoding.stringToCode(zenkaku);
        var expectArray = encoding.stringToCode(expect);
        res = encoding.toHankakuCase(zenkakuArray);
        assert(Array.isArray(res));
        assert.deepEqual(res, expectArray);
      });
    });

    it('toZenkakuCase', function() {
      hankakus.forEach(function(hankaku, i) {
        var expect = zenkakus[i];
        var res = encoding.toZenkakuCase(hankaku);
        assert.equal(res, expect);

        var hankakuArray = encoding.stringToCode(hankaku);
        var expectArray = encoding.stringToCode(expect);
        res = encoding.toZenkakuCase(hankakuArray);
        assert(Array.isArray(res));
        assert.deepEqual(res, expectArray);
      });
    });

    it('toHiraganaCase', function() {
      katakanas.forEach(function(katakana, i) {
        var expect = hiraganas[i];
        var res = encoding.toHiraganaCase(katakana);
        assert.equal(res, expect);

        var zenkanaArray = encoding.stringToCode(katakana);
        var expectArray = encoding.stringToCode(expect);
        res = encoding.toHiraganaCase(zenkanaArray);
        assert(Array.isArray(res));
        assert.deepEqual(res, expectArray);
      });
    });

    it('toKatakanaCase', function() {
      hiraganas.forEach(function(hiragana, i) {
        var expect = katakanas[i];
        var res = encoding.toKatakanaCase(hiragana);
        assert.equal(res, expect);

        var hiraganaArray = encoding.stringToCode(hiragana);
        var expectArray = encoding.stringToCode(expect);
        res = encoding.toKatakanaCase(hiraganaArray);
        assert(Array.isArray(res));
        assert.deepEqual(res, expectArray);
      });
    });

    it('toHankanaCase', function() {
      zenkanas.forEach(function(zenkana, i) {
        var expect = hankanas[i];
        var res = encoding.toHankanaCase(zenkana);
        assert.equal(res, expect);

        var zenkanaArray = encoding.stringToCode(zenkana);
        var expectArray = encoding.stringToCode(expect);
        res = encoding.toHankanaCase(zenkanaArray);
        assert(Array.isArray(res));
        assert.deepEqual(res, expectArray);
      });
    });

    it('toZenkanaCase', function() {
      hankanas.forEach(function(hankana, i) {
        var expect = zenkanas[i];
        var res = encoding.toZenkanaCase(hankana);
        assert.equal(res, expect);

        var hankanaArray = encoding.stringToCode(hankana);
        var expectArray = encoding.stringToCode(expect);
        res = encoding.toZenkanaCase(hankanaArray);
        assert(Array.isArray(res));
        assert.deepEqual(res, expectArray);
      });
    });

    it('toHankakuSpace', function() {
      var expect = hanspace;
      var res = encoding.toHankakuSpace(zenspace);
      assert.equal(res, expect);

      var zenspaceArray = encoding.stringToCode(zenspace);
      var expectArray = encoding.stringToCode(expect);
      res = encoding.toHankakuSpace(zenspaceArray);
      assert(Array.isArray(res));
      assert.deepEqual(res, expectArray);
    });

    it('toZenkakuSpace', function() {
      var expect = zenspace;
      var res = encoding.toZenkakuSpace(hanspace);
      assert.equal(res, expect);

      var hanspaceArray = encoding.stringToCode(hanspace);
      var expectArray = encoding.stringToCode(expect);
      res = encoding.toZenkakuSpace(hanspaceArray);
      assert(Array.isArray(res));
      assert.deepEqual(res, expectArray);
    });
  });

  describe('codeToString / stringToCode', function() {
    it('Test for JISX0208', function() {
      assert(Array.isArray(tests.jisx0208Array));
      assert(tests.jisx0208Array.length > 0);

      var string = encoding.codeToString(tests.jisx0208Array);
      assert(typeof string === 'string');

      var code = encoding.stringToCode(string);
      assert.deepEqual(code, tests.jisx0208Array);
    });

    it('Test for a long string', function() {
      this.timeout(5000);

      var config = require('../src/config');
      var longArray = [];
      var max = config.APPLY_BUFFER_SIZE;
      assert(typeof max === 'number');
      assert(max > 0);

      while (longArray.length < max) {
        for (var i = 0; i < tests.jisx0208Array.length; i++) {
          longArray.push(tests.jisx0208Array[i]);
        }
      }
      assert(longArray.length > max);

      var string = encoding.codeToString(longArray);
      assert(typeof string === 'string');
      var code = encoding.stringToCode(string);
      assert.deepEqual(code, longArray);

      // Run 2 times to check if APPLY_BUFFER_SIZE_OK is set up expected
      string = encoding.codeToString(longArray);
      code = encoding.stringToCode(string);
      assert.deepEqual(code, longArray);
    });
  });
});
