function regiones() {
  return [
    { codigo: '2', descripcion: 'I Región de Tarapacá' },
    { codigo: '3', descripcion: 'II Región de Antofagasta' },
    { codigo: '4', descripcion: 'III Región de Atacama' },
    { codigo: '5', descripcion: 'IV Región de Coquimbo' },
    { codigo: '6', descripcion: 'V Región de Valparaíso' },
    { codigo: '8', descripcion: 'VI Región del Libertador General Bernardo O’Higgins' },
    { codigo: '9', descripcion: 'VII Región del Maule' },
    { codigo: '10', descripcion: 'VIII Región del Biobío' },
    { codigo: '11', descripcion: 'IX Región de La Araucanía' },
    { codigo: '13', descripcion: 'X Región de Los Lagos' },
    { codigo: '14', descripcion: 'XI Región Aysén del General Carlos Ibáñez del Campo' },
    { codigo: '15', descripcion: 'XII Región de Magallanes y Antártica Chilena' },
    { codigo: '7', descripcion: 'XIII Región Metropolitana de Santiago' },
    { codigo: '12', descripcion: 'XIV Región de Los Ríos' },
    { codigo: '1', descripcion: 'XV Región de Arica y Parinacota' },
    { codigo: '16', descripcion: 'XVI Región de Ñuble' }
  ];
}

function paises() {
  return [
    { codigo: 'AD', descripcion: 'Andorra' },
    { codigo: 'AE', descripcion: 'Emiratos Árabes Unidos' },
    { codigo: 'AF', descripcion: 'Afganistán' },
    { codigo: 'AG', descripcion: 'Antigua y Barbuda' },
    { codigo: 'AI', descripcion: 'Anguila' },
    { codigo: 'AL', descripcion: 'Albania' },
    { codigo: 'AM', descripcion: 'Armenia' },
    { codigo: 'AN', descripcion: 'Antillas Neerlandesas' },
    { codigo: 'AO', descripcion: 'Angola' },
    { codigo: 'AQ', descripcion: 'Antártida' },
    { codigo: 'AR', descripcion: 'Argentina' },
    { codigo: 'AS', descripcion: 'Samoa Americana' },
    { codigo: 'AT', descripcion: 'Austria' },
    { codigo: 'AU', descripcion: 'Australia' },
    { codigo: 'AW', descripcion: 'Aruba' },
    { codigo: 'AX', descripcion: 'Islas Áland' },
    { codigo: 'AZ', descripcion: 'Azerbaiyán' },
    { codigo: 'BA', descripcion: 'Bosnia y Herzegovina' },
    { codigo: 'BB', descripcion: 'Barbados' },
    { codigo: 'BD', descripcion: 'Bangladesh' },
    { codigo: 'BE', descripcion: 'Bélgica' },
    { codigo: 'BF', descripcion: 'Burkina Faso' },
    { codigo: 'BG', descripcion: 'Bulgaria' },
    { codigo: 'BH', descripcion: 'Bahréin' },
    { codigo: 'BI', descripcion: 'Burundi' },
    { codigo: 'BJ', descripcion: 'Benin' },
    { codigo: 'BL', descripcion: 'San Bartolomé' },
    { codigo: 'BM', descripcion: 'Bermudas' },
    { codigo: 'BN', descripcion: 'Brunéi' },
    { codigo: 'BO', descripcion: 'Bolivia' },
    { codigo: 'BR', descripcion: 'Brasil' },
    { codigo: 'BS', descripcion: 'Bahamas' },
    { codigo: 'BT', descripcion: 'Bhután' },
    { codigo: 'BV', descripcion: 'Isla Bouvet' },
    { codigo: 'BW', descripcion: 'Botsuana' },
    { codigo: 'BY', descripcion: 'Belarús' },
    { codigo: 'BZ', descripcion: 'Belice' },
    { codigo: 'CA', descripcion: 'Canadá' },
    { codigo: 'CC', descripcion: 'Islas Cocos' },
    { codigo: 'CF', descripcion: 'República Centro-Africana' },
    { codigo: 'CG', descripcion: 'Congo' },
    { codigo: 'CH', descripcion: 'Suiza' },
    { codigo: 'CI', descripcion: 'Costa de Marfil' },
    { codigo: 'CK', descripcion: 'Islas Cook' },
    { codigo: 'CL', descripcion: 'Chile' },
    { codigo: 'CM', descripcion: 'Camerún' },
    { codigo: 'CN', descripcion: 'China' },
    { codigo: 'CO', descripcion: 'Colombia' },
    { codigo: 'CR', descripcion: 'Costa Rica' },
    { codigo: 'CU', descripcion: 'Cuba' },
    { codigo: 'CV', descripcion: 'Cabo Verde' },
    { codigo: 'CX', descripcion: 'Islas Christmas' },
    { codigo: 'CY', descripcion: 'Chipre' },
    { codigo: 'CZ', descripcion: 'República Checa' },
    { codigo: 'DE', descripcion: 'Alemania' },
    { codigo: 'DJ', descripcion: 'Yibuti' },
    { codigo: 'DK', descripcion: 'Dinamarca' },
    { codigo: 'DM', descripcion: 'Domínica' },
    { codigo: 'DO', descripcion: 'República Dominicana' },
    { codigo: 'DZ', descripcion: 'Argel' },
    { codigo: 'EC', descripcion: 'Ecuador' },
    { codigo: 'EE', descripcion: 'Estonia' },
    { codigo: 'EG', descripcion: 'Egipto' },
    { codigo: 'EH', descripcion: 'Sahara Occidental' },
    { codigo: 'ER', descripcion: 'Eritrea' },
    { codigo: 'ES', descripcion: 'España' },
    { codigo: 'ET', descripcion: 'Etiopía' },
    { codigo: 'FI', descripcion: 'Finlandia' },
    { codigo: 'FJ', descripcion: 'Fiji' },
    { codigo: 'FK', descripcion: 'Islas Malvinas' },
    { codigo: 'FM', descripcion: 'Micronesia' },
    { codigo: 'FO', descripcion: 'Islas Faroe' },
    { codigo: 'FR', descripcion: 'Francia' },
    { codigo: 'GA', descripcion: 'Gabón' },
    { codigo: 'GB', descripcion: 'Reino Unido' },
    { codigo: 'GD', descripcion: 'Granada' },
    { codigo: 'GE', descripcion: 'Georgia' },
    { codigo: 'GF', descripcion: 'Guayana Francesa' },
    { codigo: 'GG', descripcion: 'Guernsey' },
    { codigo: 'GH', descripcion: 'Ghana' },
    { codigo: 'GI', descripcion: 'Gibraltar' },
    { codigo: 'GL', descripcion: 'Groenlandia' },
    { codigo: 'GM', descripcion: 'Gambia' },
    { codigo: 'GN', descripcion: 'Guinea' },
    { codigo: 'GP', descripcion: 'Guadalupe' },
    { codigo: 'GQ', descripcion: 'Guinea Ecuatorial' },
    { codigo: 'GR', descripcion: 'Grecia' },
    { codigo: 'GS', descripcion: 'Georgia del Sur e Islas Sandwich del Sur' },
    { codigo: 'GT', descripcion: 'Guatemala' },
    { codigo: 'GU', descripcion: 'Guam' },
    { codigo: 'GW', descripcion: 'Guinea-Bissau' },
    { codigo: 'GY', descripcion: 'Guayana' },
    { codigo: 'HK', descripcion: 'Hong Kong' },
    { codigo: 'HM', descripcion: 'Islas Heard y McDonald' },
    { codigo: 'HN', descripcion: 'Honduras' },
    { codigo: 'HR', descripcion: 'Croacia' },
    { codigo: 'HT', descripcion: 'Haití' },
    { codigo: 'HU', descripcion: 'Hungría' },
    { codigo: 'ID', descripcion: 'Indonesia' },
    { codigo: 'IE', descripcion: 'Irlanda' },
    { codigo: 'IL', descripcion: 'Israel' },
    { codigo: 'IM', descripcion: 'Isla de Man' },
    { codigo: 'IN', descripcion: 'India' },
    { codigo: 'IO', descripcion: 'Territorio Británico del Océano Índico' },
    { codigo: 'IQ', descripcion: 'Irak' },
    { codigo: 'IR', descripcion: 'Irán' },
    { codigo: 'IS', descripcion: 'Islandia' },
    { codigo: 'IT', descripcion: 'Italia' },
    { codigo: 'JE', descripcion: 'Jersey' },
    { codigo: 'JM', descripcion: 'Jamaica' },
    { codigo: 'JO', descripcion: 'Jordania' },
    { codigo: 'JP', descripcion: 'Japón' },
    { codigo: 'KE', descripcion: 'Kenia' },
    { codigo: 'KG', descripcion: 'Kirguistán' },
    { codigo: 'KH', descripcion: 'Camboya' },
    { codigo: 'KI', descripcion: 'Kiribati' },
    { codigo: 'KM', descripcion: 'Comoros' },
    { codigo: 'KN', descripcion: 'San Cristóbal y Nieves' },
    { codigo: 'KP', descripcion: 'Corea del Norte' },
    { codigo: 'KR', descripcion: 'Corea del Sur' },
    { codigo: 'KW', descripcion: 'Kuwait' },
    { codigo: 'KY', descripcion: 'Islas Caimán' },
    { codigo: 'KZ', descripcion: 'Kazajstán' },
    { codigo: 'LA', descripcion: 'Laos' },
    { codigo: 'LB', descripcion: 'Líbano' },
    { codigo: 'LC', descripcion: 'Santa Lucía' },
    { codigo: 'LI', descripcion: 'Liechtenstein' },
    { codigo: 'LK', descripcion: 'Sri Lanka' },
    { codigo: 'LR', descripcion: 'Liberia' },
    { codigo: 'LS', descripcion: 'Lesotho' },
    { codigo: 'LT', descripcion: 'Lituania' },
    { codigo: 'LU', descripcion: 'Luxemburgo' },
    { codigo: 'LV', descripcion: 'Letonia' },
    { codigo: 'LY', descripcion: 'Libia' },
    { codigo: 'MA', descripcion: 'Marruecos' },
    { codigo: 'MC', descripcion: 'Mónaco' },
    { codigo: 'MD', descripcion: 'Moldova' },
    { codigo: 'ME', descripcion: 'Montenegro' },
    { codigo: 'MG', descripcion: 'Madagascar' },
    { codigo: 'MH', descripcion: 'Islas Marshall' },
    { codigo: 'MK', descripcion: 'Macedonia' },
    { codigo: 'ML', descripcion: 'Mali' },
    { codigo: 'MM', descripcion: 'Myanmar' },
    { codigo: 'MN', descripcion: 'Mongolia' },
    { codigo: 'MO', descripcion: 'Macao' },
    { codigo: 'MQ', descripcion: 'Martinica' },
    { codigo: 'MR', descripcion: 'Mauritania' },
    { codigo: 'MS', descripcion: 'Montserrat' },
    { codigo: 'MT', descripcion: 'Malta' },
    { codigo: 'MU', descripcion: 'Mauricio' },
    { codigo: 'MV', descripcion: 'Maldivas' },
    { codigo: 'MW', descripcion: 'Malawi' },
    { codigo: 'MX', descripcion: 'México' },
    { codigo: 'MY', descripcion: 'Malasia' },
    { codigo: 'MZ', descripcion: 'Mozambique' },
    { codigo: 'NA', descripcion: 'Namibia' },
    { codigo: 'NC', descripcion: 'Nueva Caledonia' },
    { codigo: 'NE', descripcion: 'Níger' },
    { codigo: 'NF', descripcion: 'Islas Norkfolk' },
    { codigo: 'NG', descripcion: 'Nigeria' },
    { codigo: 'NI', descripcion: 'Nicaragua' },
    { codigo: 'NL', descripcion: 'Países Bajos' },
    { codigo: 'NO', descripcion: 'Noruega' },
    { codigo: 'NP', descripcion: 'Nepal' },
    { codigo: 'NR', descripcion: 'Nauru' },
    { codigo: 'NU', descripcion: 'Niue' },
    { codigo: 'NZ', descripcion: 'Nueva Zelanda' },
    { codigo: 'OM', descripcion: 'Omán' },
    { codigo: 'PA', descripcion: 'Panamá' },
    { codigo: 'PE', descripcion: 'Perú' },
    { codigo: 'PF', descripcion: 'Polinesia Francesa' },
    { codigo: 'PG', descripcion: 'Papúa Nueva Guinea' },
    { codigo: 'PH', descripcion: 'Filipinas' },
    { codigo: 'PK', descripcion: 'Pakistán' },
    { codigo: 'PL', descripcion: 'Polonia' },
    { codigo: 'PM', descripcion: 'San Pedro y Miquelón' },
    { codigo: 'PN', descripcion: 'Islas Pitcairn' },
    { codigo: 'PR', descripcion: 'Puerto Rico' },
    { codigo: 'PS', descripcion: 'Palestina' },
    { codigo: 'PT', descripcion: 'Portugal' },
    { codigo: 'PW', descripcion: 'Islas Palaos' },
    { codigo: 'PY', descripcion: 'Paraguay' },
    { codigo: 'QA', descripcion: 'Qatar' },
    { codigo: 'RE', descripcion: 'Reunión' },
    { codigo: 'RO', descripcion: 'Rumanía' },
    { codigo: 'RS', descripcion: 'Serbia y Montenegro' },
    { codigo: 'RU', descripcion: 'Rusia' },
    { codigo: 'RW', descripcion: 'Ruanda' },
    { codigo: 'SA', descripcion: 'Arabia Saudita' },
    { codigo: 'SB', descripcion: 'Islas Solomón' },
    { codigo: 'SC', descripcion: 'Seychelles' },
    { codigo: 'SD', descripcion: 'Sudán' },
    { codigo: 'SE', descripcion: 'Suecia' },
    { codigo: 'SG', descripcion: 'Singapur' },
    { codigo: 'SH', descripcion: 'Santa Elena' },
    { codigo: 'SI', descripcion: 'Eslovenia' },
    { codigo: 'SJ', descripcion: 'Islas Svalbard y Jan Mayen' },
    { codigo: 'SK', descripcion: 'Eslovaquia' },
    { codigo: 'SL', descripcion: 'Sierra Leona' },
    { codigo: 'SM', descripcion: 'San Marino' },
    { codigo: 'SN', descripcion: 'Senegal' },
    { codigo: 'SO', descripcion: 'Somalia' },
    { codigo: 'SR', descripcion: 'Surinam' },
    { codigo: 'ST', descripcion: 'Santo Tomé y Príncipe' },
    { codigo: 'SV', descripcion: 'El Salvador' },
    { codigo: 'SY', descripcion: 'Siria' },
    { codigo: 'SZ', descripcion: 'Suazilandia' },
    { codigo: 'TC', descripcion: 'Islas Turcas y Caicos' },
    { codigo: 'TD', descripcion: 'Chad' },
    { codigo: 'TF', descripcion: 'Territorios Australes Franceses' },
    { codigo: 'TG', descripcion: 'Togo' },
    { codigo: 'TH', descripcion: 'Tailandia' },
    { codigo: 'TH', descripcion: 'Tanzania' },
    { codigo: 'TJ', descripcion: 'Tayikistán' },
    { codigo: 'TK', descripcion: 'Tokelau' },
    { codigo: 'TL', descripcion: 'Timor-Leste' },
    { codigo: 'TM', descripcion: 'Turkmenistán' },
    { codigo: 'TN', descripcion: 'Túnez' },
    { codigo: 'TO', descripcion: 'Tonga' },
    { codigo: 'TR', descripcion: 'Turquía' },
    { codigo: 'TT', descripcion: 'Trinidad y Tobago' },
    { codigo: 'TV', descripcion: 'Tuvalu' },
    { codigo: 'TW', descripcion: 'Taiwán' },
    { codigo: 'UA', descripcion: 'Ucrania' },
    { codigo: 'UG', descripcion: 'Uganda' },
    { codigo: 'US', descripcion: 'Estados Unidos de América' },
    { codigo: 'UY', descripcion: 'Uruguay' },
    { codigo: 'UZ', descripcion: 'Uzbekistán' },
    { codigo: 'VA', descripcion: 'Ciudad del Vaticano' },
    { codigo: 'VC', descripcion: 'San Vicente y las Granadinas' },
    { codigo: 'VE', descripcion: 'Venezuela' },
    { codigo: 'VG', descripcion: 'Islas Vírgenes Británicas' },
    { codigo: 'VI', descripcion: 'Islas Vírgenes de los Estados Unidos de América' },
    { codigo: 'VN', descripcion: 'Vietnam' },
    { codigo: 'VU', descripcion: 'Vanuatu' },
    { codigo: 'WF', descripcion: 'Wallis y Futuna' },
    { codigo: 'WS', descripcion: 'Samoa' },
    { codigo: 'YE', descripcion: 'Yemen' },
    { codigo: 'YT', descripcion: 'Mayotte' },
    { codigo: 'ZA', descripcion: 'Sudáfrica' }
  ];
}

function seleccionarCiudad(region) {
  switch (region) {
    case '2':
      return [
        { codigo: '3', descripcion: 'Iquique' },
        { codigo: '4', descripcion: 'El Tamarugal' }
      ];

    case '3':
      return [
        { codigo: '5', descripcion: 'Antofagasta' },
        { codigo: '6', descripcion: 'El Loa' },
        { codigo: '7', descripcion: 'Tocopilla' }
      ];

    case '4':
      return [
        { codigo: '8', descripcion: 'Chañaral' },
        { codigo: '9', descripcion: 'Copiapó' },
        { codigo: '10', descripcion: 'Huasco' }
      ];

    case '5':
      return [
        { codigo: '11', descripcion: 'Choapa' },
        { codigo: '12', descripcion: 'Elqui' },
        { codigo: '13', descripcion: 'Limarí' }
      ];

    case '6':
      return [
        { codigo: '14', descripcion: 'Isla de Pascua' },
        { codigo: '15', descripcion: 'Los Andes' },
        { codigo: '16', descripcion: 'Petorca' },
        { codigo: '17', descripcion: 'Quillota' },
        { codigo: '18', descripcion: 'San Antonio' },
        { codigo: '19', descripcion: 'San Felipe de Aconcagua' },
        { codigo: '20', descripcion: 'Valparaíso' },
        { codigo: '54', descripcion: 'Marga Marga' }
      ];

    case '8':
      return [
        { codigo: '27', descripcion: 'Cachapoal' },
        { codigo: '28', descripcion: 'Cardenal Caro' },
        { codigo: '29', descripcion: 'Colchagua' }
      ];

    case '9':
      return [
        { codigo: '30', descripcion: 'Cauquenes' },
        { codigo: '31', descripcion: 'Curicó' },
        { codigo: '32', descripcion: 'Linares' },
        { codigo: '33', descripcion: 'Talca' }
      ];

    case '10':
      return [
        { codigo: '34', descripcion: 'Arauco' },
        { codigo: '35', descripcion: 'Biobío' },
        { codigo: '36', descripcion: 'Concepción' }
      ];

    case '11':
      return [{ codigo: '38', descripcion: 'Cautín' }, { codigo: '39', descripcion: 'Malleco' }];

    case '13':
      return [
        { codigo: '42', descripcion: 'Chiloé' },
        { codigo: '43', descripcion: 'Llanquihue' },
        { codigo: '44', descripcion: 'Osorno' },
        { codigo: '45', descripcion: 'Palena' }
      ];

    case '14':
      return [
        { codigo: '46', descripcion: 'Aysen' },
        { codigo: '47', descripcion: 'Capitán Prat' },
        { codigo: '48', descripcion: 'Coyhaique' },
        { codigo: '49', descripcion: 'General Carrera' }
      ];

    case '15':
      return [
        { codigo: '50', descripcion: 'Antártica Chilena' },
        { codigo: '51', descripcion: 'Magallanes' },
        { codigo: '52', descripcion: 'Tierra del Fuego' },
        { codigo: '53', descripcion: 'Última Esperanza' }
      ];

    case '7':
      return [
        { codigo: '21', descripcion: 'Chacabuco' },
        { codigo: '22', descripcion: 'Cordillera' },
        { codigo: '23', descripcion: 'Maipo' },
        { codigo: '24', descripcion: 'Melipilla' },
        { codigo: '25', descripcion: 'Santiago' },
        { codigo: '26', descripcion: 'Talagante' }
      ];

    case '12':
      return [{ codigo: '40', descripcion: 'Valdivia' }, { codigo: '41', descripcion: 'Ranco' }];

    case '1':
      return [{ codigo: '1', descripcion: 'Arica' }, { codigo: '2', descripcion: 'Parinacota' }];

    case '16':
      return [
        { codigo: '55', descripcion: 'Itata' },
        { codigo: '56', descripcion: 'Diguillín' },
        { codigo: '57', descripcion: 'Punilla' }
      ];
  }
}

function seleccionarComuna(provincia) {
  switch (provincia) {
    case '1':
      return [{ codigo: '1', descripcion: 'Arica' }, { codigo: '2', descripcion: 'Camarones' }];

    case '2':
      return [{ codigo: '3', descripcion: 'General Lagos' }, { codigo: '4', descripcion: 'Putre' }];

    case '3':
      return [
        { codigo: '5', descripcion: 'Alto Hospicio' },
        { codigo: '6', descripcion: 'Iquique' }
      ];

    case '4':
      return [
        { codigo: '7', descripcion: 'Camiña' },
        { codigo: '8', descripcion: 'Colchane' },
        { codigo: '9', descripcion: 'Huara' },
        { codigo: '10', descripcion: 'Pica' },
        { codigo: '11', descripcion: 'Pozo Almonte' }
      ];

    case '5':
      return [
        { codigo: '12', descripcion: 'Antofagasta' },
        { codigo: '13', descripcion: 'Mejillones' },
        { codigo: '14', descripcion: 'Sierra Gorda' },
        { codigo: '15', descripcion: 'Taltal' }
      ];

    case '6':
      return [
        { codigo: '16', descripcion: 'Calama' },
        { codigo: '17', descripcion: 'Ollague' },
        { codigo: '18', descripcion: 'San Pedro de Atacama' }
      ];

    case '7':
      return [
        { codigo: '19', descripcion: 'María Elena' },
        { codigo: '20', descripcion: 'Tocopilla' }
      ];

    case '8':
      return [
        { codigo: '21', descripcion: 'Chañaral' },
        { codigo: '22', descripcion: 'Diego de Almagro' }
      ];

    case '9':
      return [
        { codigo: '23', descripcion: 'Caldera' },
        { codigo: '24', descripcion: 'Copiapó' },
        { codigo: '25', descripcion: 'Tierra Amarilla' }
      ];

    case '10':
      return [
        { codigo: '26', descripcion: 'Alto del Carmen' },
        { codigo: '27', descripcion: 'Freirina' },
        { codigo: '28', descripcion: 'Huasco' },
        { codigo: '29', descripcion: 'Vallenar' }
      ];

    case '11':
      return [
        { codigo: '30', descripcion: 'Canela' },
        { codigo: '31', descripcion: 'Illapel' },
        { codigo: '32', descripcion: 'Los Vilos' },
        { codigo: '33', descripcion: 'Salamanca' }
      ];

    case '12':
      return [
        { codigo: '34', descripcion: 'Andacollo' },
        { codigo: '35', descripcion: 'Coquimbo' },
        { codigo: '36', descripcion: 'La Higuera' },
        { codigo: '37', descripcion: 'La Serena' },
        { codigo: '38', descripcion: 'Paihuaco' },
        { codigo: '39', descripcion: 'Vicuña' }
      ];

    case '13':
      return [
        { codigo: '40', descripcion: 'Combarbalá' },
        { codigo: '41', descripcion: 'Monte Patria' },
        { codigo: '42', descripcion: 'Ovalle' },
        { codigo: '43', descripcion: 'Punitaqui' },
        { codigo: '44', descripcion: 'Río Hurtado' }
      ];

    case '14':
      return [{ codigo: '45', descripcion: 'Isla de Pascua' }];

    case '15':
      return [
        { codigo: '46', descripcion: 'Calle Larga' },
        { codigo: '47', descripcion: 'Los Andes' },
        { codigo: '48', descripcion: 'Rinconada' },
        { codigo: '49', descripcion: 'San Esteban' }
      ];

    case '16':
      return [
        { codigo: '346', descripcion: 'Cabildo' },
        { codigo: '50', descripcion: 'La Ligua' },
        { codigo: '51', descripcion: 'Papudo' },
        { codigo: '52', descripcion: 'Petorca' },
        { codigo: '53', descripcion: 'Zapallar' }
      ];

    case '17':
      return [
        { codigo: '54', descripcion: 'Hijuelas' },
        { codigo: '55', descripcion: 'La Calera' },
        { codigo: '56', descripcion: 'La Cruz' },
        { codigo: '58', descripcion: 'Nogales' },
        { codigo: '60', descripcion: 'Quillota' }
      ];

    case '18':
      return [
        { codigo: '61', descripcion: 'Algarrobo' },
        { codigo: '62', descripcion: 'Cartagena' },
        { codigo: '63', descripcion: 'El Quisco' },
        { codigo: '64', descripcion: 'El Tabo' },
        { codigo: '65', descripcion: 'San Antonio' },
        { codigo: '66', descripcion: 'Santo Domingo' }
      ];

    case '19':
      return [
        { codigo: '67', descripcion: 'Catemu' },
        { codigo: '68', descripcion: 'Llay-llay' },
        { codigo: '69', descripcion: 'Panquehue' },
        { codigo: '70', descripcion: 'Putaendo' },
        { codigo: '71', descripcion: 'San Felipe' },
        { codigo: '72', descripcion: 'Santa María' }
      ];

    case '20':
      return [
        { codigo: '73', descripcion: 'Casablanca' },
        { codigo: '74', descripcion: 'Concón' },
        { codigo: '75', descripcion: 'Juan Fernández' },
        { codigo: '76', descripcion: 'Puchuncaví' },
        { codigo: '78', descripcion: 'Quintero' },
        { codigo: '79', descripcion: 'Valparaíso' },
        { codigo: '81', descripcion: 'Viña del Mar' }
      ];

    case '21':
      return [
        { codigo: '82', descripcion: 'Colina' },
        { codigo: '83', descripcion: 'Lampa' },
        { codigo: '84', descripcion: 'Til Til' }
      ];

    case '22':
      return [
        { codigo: '85', descripcion: 'Pirque' },
        { codigo: '86', descripcion: 'Puente Alto' },
        { codigo: '87', descripcion: 'San José de Maipo' }
      ];

    case '23':
      return [
        { codigo: '88', descripcion: 'Buin' },
        { codigo: '89', descripcion: 'Calera de Tango' },
        { codigo: '90', descripcion: 'Paine' },
        { codigo: '91', descripcion: 'San Bernardo' }
      ];

    case '24':
      return [
        { codigo: '92', descripcion: 'Alhué' },
        { codigo: '93', descripcion: 'Curacaví' },
        { codigo: '94', descripcion: 'María Pinto' },
        { codigo: '95', descripcion: 'Melipilla' },
        { codigo: '96', descripcion: 'San Pedro' }
      ];

    case '25':
      return [
        { codigo: '97', descripcion: 'Cerrillos' },
        { codigo: '98', descripcion: 'Cerro Navia' },
        { codigo: '99', descripcion: 'Conchalí' },
        { codigo: '100', descripcion: 'El Bosque' },
        { codigo: '101', descripcion: 'Estación Central' },
        { codigo: '102', descripcion: 'Huechuraba' },
        { codigo: '103', descripcion: 'Independencia' },
        { codigo: '104', descripcion: 'La Cisterna' },
        { codigo: '105', descripcion: 'La Granja' },
        { codigo: '106', descripcion: 'La Florida' },
        { codigo: '107', descripcion: 'La Pintana' },
        { codigo: '108', descripcion: 'La Reina' },
        { codigo: '109', descripcion: 'Las Condes' },
        { codigo: '110', descripcion: 'Lo Barnechea' },
        { codigo: '111', descripcion: 'Lo Espejo' },
        { codigo: '112', descripcion: 'Lo Prado' },
        { codigo: '113', descripcion: 'Macul' },
        { codigo: '114', descripcion: 'Maipú' },
        { codigo: '115', descripcion: 'Ñuñoa' },
        { codigo: '116', descripcion: 'Pedro Aguirre Cerda' },
        { codigo: '117', descripcion: 'Peñalolén' },
        { codigo: '118', descripcion: 'Providencia' },
        { codigo: '119', descripcion: 'Pudahuel' },
        { codigo: '120', descripcion: 'Quilicura' },
        { codigo: '121', descripcion: 'Quinta Normal' },
        { codigo: '122', descripcion: 'Recoleta' },
        { codigo: '123', descripcion: 'Renca' },
        { codigo: '124', descripcion: 'San Miguel' },
        { codigo: '125', descripcion: 'San Joaquín' },
        { codigo: '126', descripcion: 'San Ramón' },
        { codigo: '127', descripcion: 'Santiago' },
        { codigo: '128', descripcion: 'Vitacura' }
      ];

    case '26':
      return [
        { codigo: '129', descripcion: 'El Monte' },
        { codigo: '130', descripcion: 'Isla de Maipo' },
        { codigo: '131', descripcion: 'Padre Hurtado' },
        { codigo: '132', descripcion: 'Peñaflor' },
        { codigo: '133', descripcion: 'Talagante' }
      ];

    case '27':
      return [
        { codigo: '134', descripcion: 'Codegua' },
        { codigo: '135', descripcion: 'Coínco' },
        { codigo: '136', descripcion: 'Coltauco' },
        { codigo: '137', descripcion: 'Doñihue' },
        { codigo: '138', descripcion: 'Graneros' },
        { codigo: '139', descripcion: 'Las Cabras' },
        { codigo: '140', descripcion: 'Machalí' },
        { codigo: '141', descripcion: 'Malloa' },
        { codigo: '142', descripcion: 'Mostazal' },
        { codigo: '143', descripcion: 'Olivar' },
        { codigo: '144', descripcion: 'Peumo' },
        { codigo: '145', descripcion: 'Pichidegua' },
        { codigo: '146', descripcion: 'Quinta de Tilcoco' },
        { codigo: '147', descripcion: 'Rancagua' },
        { codigo: '148', descripcion: 'Rengo' },
        { codigo: '149', descripcion: 'Requínoa' },
        { codigo: '150', descripcion: 'San Vicente de Tagua Tagua' }
      ];

    case '28':
      return [
        { codigo: '151', descripcion: 'La Estrella' },
        { codigo: '152', descripcion: 'Litueche' },
        { codigo: '153', descripcion: 'Marchihue' },
        { codigo: '154', descripcion: 'Navidad' },
        { codigo: '155', descripcion: 'Paredones' },
        { codigo: '156', descripcion: 'Pichilemu' }
      ];

    case '29':
      return [
        { codigo: '157', descripcion: 'Chépica' },
        { codigo: '158', descripcion: 'Chimbarongo' },
        { codigo: '159', descripcion: 'Lolol' },
        { codigo: '160', descripcion: 'Nancagua' },
        { codigo: '161', descripcion: 'Palmilla' },
        { codigo: '162', descripcion: 'Peralillo' },
        { codigo: '163', descripcion: 'Placilla' },
        { codigo: '164', descripcion: 'Pumanque' },
        { codigo: '165', descripcion: 'San Fernando' },
        { codigo: '166', descripcion: 'Santa Cruz' }
      ];

    case '30':
      return [
        { codigo: '167', descripcion: 'Cauquenes' },
        { codigo: '168', descripcion: 'Chanco' },
        { codigo: '169', descripcion: 'Pelluhue' }
      ];

    case '31':
      return [
        { codigo: '170', descripcion: 'Curicó' },
        { codigo: '171', descripcion: 'Hualañé' },
        { codigo: '172', descripcion: 'Licantén' },
        { codigo: '173', descripcion: 'Molina' },
        { codigo: '174', descripcion: 'Rauco' },
        { codigo: '175', descripcion: 'Romeral' },
        { codigo: '176', descripcion: 'Sagrada Familia' },
        { codigo: '177', descripcion: 'Teno' },
        { codigo: '178', descripcion: 'Vichuquén' }
      ];

    case '32':
      return [
        { codigo: '179', descripcion: 'Colbún' },
        { codigo: '180', descripcion: 'Linares' },
        { codigo: '181', descripcion: 'Longaví' },
        { codigo: '182', descripcion: 'Parral' },
        { codigo: '183', descripcion: 'Retiro' },
        { codigo: '184', descripcion: 'San Javier' },
        { codigo: '185', descripcion: 'Villa Alegre' },
        { codigo: '186', descripcion: 'Yerbas Buenas' }
      ];

    case '33':
      return [
        { codigo: '187', descripcion: 'Constitución' },
        { codigo: '188', descripcion: 'Curepto' },
        { codigo: '189', descripcion: 'Empedrado' },
        { codigo: '190', descripcion: 'Maule' },
        { codigo: '191', descripcion: 'Pelarco' },
        { codigo: '192', descripcion: 'Pencahue' },
        { codigo: '193', descripcion: 'Río Claro' },
        { codigo: '194', descripcion: 'San Clemente' },
        { codigo: '195', descripcion: 'San Rafael' },
        { codigo: '196', descripcion: 'Talca' }
      ];

    case '34':
      return [
        { codigo: '197', descripcion: 'Arauco' },
        { codigo: '198', descripcion: 'Cañete' },
        { codigo: '199', descripcion: 'Contulmo' },
        { codigo: '200', descripcion: 'Curanilahue' },
        { codigo: '201', descripcion: 'Lebu' },
        { codigo: '202', descripcion: 'Los Álamos' },
        { codigo: '203', descripcion: 'Tirúa' }
      ];

    case '35':
      return [
        { codigo: '204', descripcion: 'Alto Biobío' },
        { codigo: '205', descripcion: 'Antuco' },
        { codigo: '206', descripcion: 'Cabrero' },
        { codigo: '207', descripcion: 'Laja' },
        { codigo: '208', descripcion: 'Los Ángeles' },
        { codigo: '209', descripcion: 'Mulchén' },
        { codigo: '210', descripcion: 'Nacimiento' },
        { codigo: '211', descripcion: 'Negrete' },
        { codigo: '212', descripcion: 'Quilaco' },
        { codigo: '213', descripcion: 'Quilleco' },
        { codigo: '214', descripcion: 'San Rosendo' },
        { codigo: '215', descripcion: 'Santa Bárbara' },
        { codigo: '216', descripcion: 'Tucapel' },
        { codigo: '217', descripcion: 'Yumbel' }
      ];

    case '36':
      return [
        { codigo: '218', descripcion: 'Chiguayante' },
        { codigo: '219', descripcion: 'Concepción' },
        { codigo: '220', descripcion: 'Coronel' },
        { codigo: '221', descripcion: 'Florida' },
        { codigo: '222', descripcion: 'Hualpén' },
        { codigo: '223', descripcion: 'Hualqui' },
        { codigo: '224', descripcion: 'Lota' },
        { codigo: '225', descripcion: 'Penco' },
        { codigo: '226', descripcion: 'San Pedro de La Paz' },
        { codigo: '227', descripcion: 'Santa Juana' },
        { codigo: '228', descripcion: 'Talcahuano' },
        { codigo: '229', descripcion: 'Tomé' }
      ];

    case '38':
      return [
        { codigo: '251', descripcion: 'Carahue' },
        { codigo: '252', descripcion: 'Cholchol' },
        { codigo: '253', descripcion: 'Cunco' },
        { codigo: '254', descripcion: 'Curarrehue' },
        { codigo: '255', descripcion: 'Freire' },
        { codigo: '256', descripcion: 'Galvarino' },
        { codigo: '257', descripcion: 'Gorbea' },
        { codigo: '258', descripcion: 'Lautaro' },
        { codigo: '259', descripcion: 'Loncoche' },
        { codigo: '260', descripcion: 'Melipeuco' },
        { codigo: '261', descripcion: 'Nueva Imperial' },
        { codigo: '262', descripcion: 'Padre Las Casas' },
        { codigo: '263', descripcion: 'Perquenco' },
        { codigo: '264', descripcion: 'Pitrufquén' },
        { codigo: '265', descripcion: 'Pucón' },
        { codigo: '266', descripcion: 'Puerto Saavedra' },
        { codigo: '267', descripcion: 'Temuco' },
        { codigo: '268', descripcion: 'Teodoro Schmidt' },
        { codigo: '269', descripcion: 'Toltén' },
        { codigo: '270', descripcion: 'Vilcún' },
        { codigo: '271', descripcion: 'Villarrica' }
      ];

    case '39':
      return [
        { codigo: '272', descripcion: 'Angol' },
        { codigo: '273', descripcion: 'Collipulli' },
        { codigo: '274', descripcion: 'Curacautín' },
        { codigo: '275', descripcion: 'Ercilla' },
        { codigo: '276', descripcion: 'Lonquimay' },
        { codigo: '277', descripcion: 'Los Sauces' },
        { codigo: '278', descripcion: 'Lumaco' },
        { codigo: '279', descripcion: 'Purén' },
        { codigo: '280', descripcion: 'Renaico' },
        { codigo: '281', descripcion: 'Traiguén' },
        { codigo: '282', descripcion: 'Victoria' }
      ];

    case '40':
      return [
        { codigo: '283', descripcion: 'Corral' },
        { codigo: '284', descripcion: 'Lanco' },
        { codigo: '285', descripcion: 'Los Lagos' },
        { codigo: '286', descripcion: 'Máfil' },
        { codigo: '287', descripcion: 'Mariquina' },
        { codigo: '288', descripcion: 'Paillaco' },
        { codigo: '289', descripcion: 'Panguipulli' },
        { codigo: '290', descripcion: 'Valdivia' }
      ];

    case '41':
      return [
        { codigo: '291', descripcion: 'Futrono' },
        { codigo: '292', descripcion: 'La Unión' },
        { codigo: '293', descripcion: 'Lago Ranco' },
        { codigo: '294', descripcion: 'Río Bueno' }
      ];

    case '42':
      return [
        { codigo: '295', descripcion: 'Ancud' },
        { codigo: '296', descripcion: 'Castro' },
        { codigo: '297', descripcion: 'Chonchi' },
        { codigo: '298', descripcion: 'Curaco de Vélez' },
        { codigo: '299', descripcion: 'Dalcahue' },
        { codigo: '300', descripcion: 'Puqueldón' },
        { codigo: '301', descripcion: 'Queilén' },
        { codigo: '302', descripcion: 'Quemchi' },
        { codigo: '303', descripcion: 'Quellón' },
        { codigo: '304', descripcion: 'Quinchao' }
      ];

    case '43':
      return [
        { codigo: '305', descripcion: 'Calbuco' },
        { codigo: '306', descripcion: 'Cochamó' },
        { codigo: '307', descripcion: 'Fresia' },
        { codigo: '308', descripcion: 'Frutillar' },
        { codigo: '309', descripcion: 'Llanquihue' },
        { codigo: '310', descripcion: 'Los Muermos' },
        { codigo: '311', descripcion: 'Maullín' },
        { codigo: '312', descripcion: 'Puerto Montt' },
        { codigo: '313', descripcion: 'Puerto Varas' }
      ];

    case '44':
      return [
        { codigo: '314', descripcion: 'Osorno' },
        { codigo: '315', descripcion: 'Puero Octay' },
        { codigo: '316', descripcion: 'Purranque' },
        { codigo: '317', descripcion: 'Puyehue' },
        { codigo: '318', descripcion: 'Río Negro' },
        { codigo: '319', descripcion: 'San Juan de la Costa' },
        { codigo: '320', descripcion: 'San Pablo' }
      ];

    case '45':
      return [
        { codigo: '321', descripcion: 'Chaitén' },
        { codigo: '322', descripcion: 'Futaleufú' },
        { codigo: '323', descripcion: 'Hualaihué' },
        { codigo: '324', descripcion: 'Palena' }
      ];

    case '46':
      return [
        { codigo: '325', descripcion: 'Aysén' },
        { codigo: '326', descripcion: 'Cisnes' },
        { codigo: '327', descripcion: 'Guaitecas' }
      ];

    case '47':
      return [
        { codigo: '328', descripcion: 'Cochrane' },
        { codigo: '329', descripcion: 'O higgins' },
        { codigo: '330', descripcion: 'Tortel' }
      ];

    case '48':
      return [
        { codigo: '331', descripcion: 'Coihaique' },
        { codigo: '332', descripcion: 'Lago Verde' }
      ];

    case '49':
      return [
        { codigo: '333', descripcion: 'Chile Chico' },
        { codigo: '334', descripcion: 'Río Ibáñez' }
      ];

    case '50':
      return [
        { codigo: '335', descripcion: 'Antártica' },
        { codigo: '336', descripcion: 'Cabo de Hornos' }
      ];

    case '51':
      return [
        { codigo: '337', descripcion: 'Laguna Blanca' },
        { codigo: '338', descripcion: 'Punta Arenas' },
        { codigo: '339', descripcion: 'Río Verde' },
        { codigo: '340', descripcion: 'San Gregorio' }
      ];

    case '52':
      return [
        { codigo: '341', descripcion: 'Porvenir' },
        { codigo: '342', descripcion: 'Primavera' },
        { codigo: '343', descripcion: 'Timaukel' }
      ];

    case '53':
      return [
        { codigo: '344', descripcion: 'Natales' },
        { codigo: '345', descripcion: 'Torres del Paine' }
      ];

    case '54':
      return [
        { codigo: '57', descripcion: 'Limache' },
        { codigo: '59', descripcion: 'Olmué' },
        { codigo: '77', descripcion: 'Quilpué' },
        { codigo: '80', descripcion: 'Villa Alemana' }
      ];

    case '55':
      return [
        { codigo: '233', descripcion: 'Cobquecura' },
        { codigo: '234', descripcion: 'Coelemu' },
        { codigo: '237', descripcion: 'Ninhue' },
        { codigo: '241', descripcion: 'Portezuelo' },
        { codigo: '243', descripcion: 'Quirihue' },
        { codigo: '244', descripcion: 'Ránquil' },
        { codigo: '249', descripcion: 'Treguaco' }
      ];

    case '56':
      return [
        { codigo: '230', descripcion: 'Bulnes' },
        { codigo: '231', descripcion: 'Chillán' },
        { codigo: '232', descripcion: 'Chillán Viejo' },
        { codigo: '236', descripcion: 'El Carmen' },
        { codigo: '239', descripcion: 'Pemuco' },
        { codigo: '240', descripcion: 'Pinto' },
        { codigo: '242', descripcion: 'Quillón' },
        { codigo: '247', descripcion: 'San Ignacio' },
        { codigo: '250', descripcion: 'Yungay' }
      ];

    case '57':
      return [
        { codigo: '235', descripcion: 'Coihueco' },
        { codigo: '238', descripcion: 'Ñiquen' },
        { codigo: '245', descripcion: 'San Carlos' },
        { codigo: '246', descripcion: 'San Fabián' },
        { codigo: '248', descripcion: 'San Nicolás' }
      ];
  }
}

function tipoDocumento() {
  return [
    { codigo: '30', descripcion: 'Factura' },
    { codigo: '32', descripcion: 'Factura Exenta' },
    { codigo: '33', descripcion: 'Factura Electrónica' },
    { codigo: '34', descripcion: 'Factura Electrónica Exenta' },
    { codigo: '110', descripcion: 'Factura de Exportación' },
    { codigo: '45', descripcion: 'Factura de Compra' },
    { codigo: '46', descripcion: 'Factura Electrónica de Compra' },
    { codigo: '50', descripcion: 'Guía de Despacho' },
    { codigo: '52', descripcion: 'Guía Electrónica de Despacho' },
    { codigo: '60', descripcion: 'Nota Crédito' },
    { codigo: '61', descripcion: 'Nota Electrónica de Crédito' },
    { codigo: '112', descripcion: 'Nota Crédito de Exportación' },
    { codigo: '55', descripcion: 'Nota Débito' },
    { codigo: '56', descripcion: 'Nota Electrónica de Débito' },
    { codigo: '111', descripcion: 'Nota Débito de Exportación' },
    { codigo: '35', descripcion: 'Boleta' },
    { codigo: '38', descripcion: 'Boleta Exenta' },
    { codigo: '39', descripcion: 'Boleta Electrónica' },
    { codigo: '41', descripcion: 'Boleta Electrónica Exenta' },
    { codigo: 'HES', descripcion: 'HES - Hoja Entrada Servicio' },
    { codigo: '801', descripcion: 'Orden de Compra' },
    { codigo: '802', descripcion: 'Nota de Pedido' },
    { codigo: '803', descripcion: 'Contrato' },
    { codigo: '804', descripcion: 'Resolución' },
    { codigo: '805', descripcion: 'Proceso Chile Compra' },
    { codigo: '806', descripcion: 'Ficha Chile Compra' },
    { codigo: '807', descripcion: 'DUS' },
    { codigo: '808', descripcion: 'B/L Conocimiento de Embarque' },
    { codigo: '809', descripcion: 'AWB' },
    { codigo: '810', descripcion: 'MIC/DATA' },
    { codigo: '811', descripcion: 'Carta de Porte' },
    { codigo: '812', descripcion: 'Res. SNA Servicio Exportación' },
    { codigo: '813', descripcion: 'Pasaporte' },
    { codigo: '814', descripcion: 'Cert. Depósito BP' },
    { codigo: '815', descripcion: 'Vale Prenda BP' },
    { codigo: '-1', descripcion: 'Centro de Costo' },
    { codigo: '0', descripcion: 'Otros' }
  ];
}

function tipoImpuesto() {
  return [
    {
      codigo: '23',
      descripcion: '15% Art. de Oro, Joyas y Pieles Finas',
      helper: 'Artículos de oro, platino y marfil; joyas y piedras preciosas.',
      porcentaje: '15'
    },
    {
      codigo: '45',
      descripcion: '50% Artículos de Pirotecnia',
      helper: 'Los artículos de pirotecnia, tales como fuegos artificiales, petardos y similares.',
      porcentaje: '50'
    },
    {
      codigo: '44',
      descripcion: '15% Tapices, Casas Rod., Caviar y Arm. de Aire',
      helper:
        'Pieles finas; alfombras, tapices y cualquier otro artículo de similar naturaleza, calificados como finos por el Servicio de Impuestos Internos; vehículos casa-rodantes autopropulsados; conservas de caviar y sucedáneos; armas de aire o gas comprimido, sus accesorios y proyectiles, excepto los de caza submarina.',
      porcentaje: '15'
    },
    {
      codigo: '24',
      descripcion: '31,5% Licores, Pisco, Destilados',
      helper:
        'Licores, piscos, whisky, aguardientes y destilados, incluyendo los vinos licorosos o aromatizados similares al vermouth.',
      porcentaje: '31.5'
    },
    {
      codigo: '25',
      descripcion: '20,5% Vinos, Chichas, Sidras',
      helper:
        'Vinos destinados al consumo, comprendidos los vinos gasificados, los espumosos o champaña, los generosos o asoleados, chichas y sidras destinadas al consumo, cualquiera que sea su envase, cervezas y otras bebidas alcohólicas, cualquiera que sea su tipo, calidad o denominación.',
      porcentaje: '20.5'
    },
    {
      codigo: '26',
      descripcion: '12% Cervezas y otras bebidas Alcohólicas',
      helper: '',
      porcentaje: '12'
    },
    {
      codigo: '27',
      descripcion: '10% Aguas Minerales y Bebidas Analcohólicas',
      helper:
        'Bebidas analcohólicas naturales o artificiales, energizantes o hipertónicas, jarabes y en general cualquier otro producto que las sustituya o que sirva para preparar bebidas similares, y aguas minerales o termales a las cuales se les haya adicionado colorante, sabor o edulcorantes.',
      porcentaje: '10'
    },
    {
      codigo: '271',
      descripcion: '18% Bebidas Analcohólicas Altas en Azúcares',
      helper:
        'Bebidas analcohólicas naturales o artificiales que presenten la composición nutricional de elevado contenido de azúcares a que se refiere el artículo 5° de la ley N°20.606, la que para estos efectos se considerará existente cuando tengan más de 15 gramos (g) por cada 240 mililitros (ml) o porción equivalente.',
      porcentaje: '18'
    },
    {
      codigo: '18',
      descripcion: 'Impuesto Específico Diesel',
      helper: 'La tasa del impuesto es de 1,5 UTM por m3.',
      porcentaje: ''
    },
    {
      codigo: '35',
      descripcion: 'Impuesto Específico Gasolinas',
      helper: 'La tasa del impuesto es de 6 UTM por m3.',
      porcentaje: ''
    }
  ];
}

function unidadMedida() {
  return [
    { codigo: 'SUM', descripcion: 'S.U.M.' },
    { codigo: 'TMB', descripcion: 'TMB' },
    { codigo: 'U', descripcion: 'U' },
    { codigo: 'DOC', descripcion: 'DOC' },
    { codigo: 'UJGO', descripcion: 'U(JGO)' },
    { codigo: 'MU', descripcion: 'MU' },
    { codigo: 'MT', descripcion: 'MT' },
    { codigo: 'MT2', descripcion: 'MT2' },
    { codigo: 'MCUB', descripcion: 'MCUB' },
    { codigo: 'PAR', descripcion: 'PAR' },
    { codigo: 'KNFC', descripcion: 'KNFC' },
    { codigo: 'CARTON', descripcion: 'CARTON' },
    { codigo: 'QMB', descripcion: 'QMB' },
    { codigo: 'KWH', descripcion: 'KWH' },
    { codigo: 'BAR', descripcion: 'BAR' },
    { codigo: 'M2', descripcion: 'M2/1MM' },
    { codigo: 'MKWH', descripcion: 'MKWH' },
    { codigo: 'TMN', descripcion: 'TMN' },
    { codigo: 'KLT', descripcion: 'KLT' },
    { codigo: 'NK', descripcion: 'KN' },
    { codigo: 'GN', descripcion: 'GN' },
    { codigo: 'HL', descripcion: 'HL' },
    { codigo: 'LT', descripcion: 'LT' }
  ];
}

function tipoBulto() {
  return [
    { codigo: '1', descripcion: 'POLVO' },
    { codigo: '2', descripcion: 'GRANOS' },
    { codigo: '3', descripcion: 'NÓDULOS' },
    { codigo: '4', descripcion: 'LIQUIDO' },
    { codigo: '5', descripcion: 'GAS' },
    { codigo: '6', descripcion: 'PIEZA' },
    { codigo: '7', descripcion: 'TUBO' },
    { codigo: '8', descripcion: 'CILINDRO' },
    { codigo: '9', descripcion: 'ROLLO' },
    { codigo: '10', descripcion: 'BARRA' },
    { codigo: '11', descripcion: 'LINGOTE' },
    { codigo: '12', descripcion: 'TRONCO' },
    { codigo: '13', descripcion: 'BLOQUE' },
    { codigo: '14', descripcion: 'ROLLIZO' },
    { codigo: '15', descripcion: 'CAJÓN' },
    { codigo: '16', descripcion: 'CAJA DE CARTÓN' },
    { codigo: '17', descripcion: 'FARDO' },
    { codigo: '18', descripcion: 'BAÚL' },
    { codigo: '19', descripcion: 'COFRE' },
    { codigo: '20', descripcion: 'ARMAZÓN' },
    { codigo: '21', descripcion: 'BANDEJA' },
    { codigo: '22', descripcion: 'CAJAMADERA' },
    { codigo: '23', descripcion: 'CAJA LATA' },
    { codigo: '24', descripcion: 'BOTELLAGAS' },
    { codigo: '25', descripcion: 'BOTELLA' },
    { codigo: '26', descripcion: 'JAULA' },
    { codigo: '27', descripcion: 'BIDÓN' },
    { codigo: '28', descripcion: 'JABA' },
    { codigo: '29', descripcion: 'CESTA' },
    { codigo: '30', descripcion: 'BARRILETE' },
    { codigo: '31', descripcion: 'TONEL' },
    { codigo: '32', descripcion: 'PIPA' },
    { codigo: '33', descripcion: 'JARRO' },
    { codigo: '34', descripcion: 'FRASCO' },
    { codigo: '35', descripcion: 'DAMAJUANA' },
    { codigo: '36', descripcion: 'BARRIL' },
    { codigo: '37', descripcion: 'TAMBOR' },
    { codigo: '38', descripcion: 'CUñETE' },
    { codigo: '39', descripcion: 'TARRO' },
    { codigo: '40', descripcion: 'CUBO' },
    { codigo: '41', descripcion: 'PAQUETE' },
    { codigo: '42', descripcion: 'SACO' },
    { codigo: '43', descripcion: 'MALETA' },
    { codigo: '44', descripcion: 'BOLSA' },
    { codigo: '45', descripcion: 'BALA' },
    { codigo: '46', descripcion: 'RED' },
    { codigo: '47', descripcion: 'SOBRE' },
    { codigo: '48', descripcion: 'CONT20' },
    { codigo: '49', descripcion: 'CONT40' },
    { codigo: '50', descripcion: 'REEFER20' },
    { codigo: '51', descripcion: 'REEFER40' },
    { codigo: '52', descripcion: 'ESTANQUE' },
    { codigo: '53', descripcion: 'CONTNOESP' },
    { codigo: '54', descripcion: 'PALLET' },
    { codigo: '55', descripcion: 'TABLERO' },
    { codigo: '56', descripcion: 'LAMINA' },
    { codigo: '57', descripcion: 'CARRETE' },
    { codigo: '58', descripcion: 'AUTOMOTOR' },
    { codigo: '59', descripcion: 'ATAÚD' },
    { codigo: '60', descripcion: 'MAQUINARIA' },
    { codigo: '61', descripcion: 'PLANCHA' },
    { codigo: '62', descripcion: 'ATADO' },
    { codigo: '63', descripcion: 'BOBINA' },
    { codigo: '64', descripcion: 'BULTONOESP' },
    { codigo: '65', descripcion: 'SIN BULTO' },
    { codigo: '66', descripcion: 'S/EMBALAR' }
  ];
}

function tipoClausula() {
  return [
    { codigo: 'CIF', descripcion: 'CIF' },
    { codigo: 'FCA', descripcion: 'FCA' },
    { codigo: 'CPT', descripcion: 'CPT' },
    { codigo: 'CIP', descripcion: 'CIP' },
    { codigo: 'DAT', descripcion: 'DAT' },
    { codigo: 'DAP', descripcion: 'DAP' },
    { codigo: 'CFR', descripcion: 'CFR' },
    { codigo: 'EXW', descripcion: 'EXW' },
    { codigo: 'FAS', descripcion: 'FAS' },
    { codigo: 'FOB', descripcion: 'FOB' },
    { codigo: 'SCL', descripcion: 'S/CL' },
    { codigo: 'OTROS', descripcion: 'OTROS' },
    { codigo: 'DDP', descripcion: 'DDP' }
  ];
}

function puerto(codigo) {
  switch (codigo) {
    case 'CA':
      return [
        { descripcion: 'MONTREAL', codigo: '111' },
        { descripcion: 'COSTA DEL PACÍFICO, OTROS NO ESPECIFICADOS', codigo: '112' },
        { descripcion: 'HALIFAX', codigo: '113' },
        { descripcion: 'VANCOUVER', codigo: '114' },
        { descripcion: 'SAINT JOHN', codigo: '115' },
        { descripcion: 'TORONTO', codigo: '116' },
        { descripcion: 'OTROS PUERTOS DE CANADÁ NO IDENTIFICADOS', codigo: '117' },
        { descripcion: 'BAYSIDE', codigo: '118' },
        { descripcion: 'PORT CARTIES', codigo: '120' },
        { descripcion: 'QUEBEC', codigo: '124' },
        { descripcion: 'PRINCE RUPERT', codigo: '125' },
        { descripcion: 'HAMILTON', codigo: '126' }
      ];

    case 'US':
    case 'VI':
      return [
        { descripcion: 'BOSTON', codigo: '131' },
        { descripcion: 'NEW HAVEN', codigo: '132' },
        { descripcion: 'BRIDGEPORT', codigo: '133' },
        { descripcion: 'NEW YORK', codigo: '134' },
        { descripcion: 'FILADELFIA', codigo: '135' },
        { descripcion: 'BALTIMORE', codigo: '136' },
        { descripcion: 'NORFOLK', codigo: '137' },
        { descripcion: 'CHARLESTON', codigo: '139' },
        { descripcion: 'SAVANAH', codigo: '140' },
        { descripcion: 'MIAMI', codigo: '141' },
        {
          descripcion:
            'COSTA DEL ATLÁNTICO, OTROS NO ESPECIFICADOS COMPRENDIDOS ENTRE MAINE Y KEY WEST',
          codigo: '121'
        },
        { descripcion: 'TAMPA', codigo: '151' },
        { descripcion: 'PENSACOLA', codigo: '152' },
        { descripcion: 'MOBILE', codigo: '153' },
        { descripcion: 'NEW ORLEANS', codigo: '154' },
        { descripcion: 'PORT ARTHUR', codigo: '155' },
        { descripcion: 'GALVESTON', codigo: '156' },
        { descripcion: 'CORPUS CRISTI', codigo: '157' },
        { descripcion: 'BROWNSVILLE', codigo: '158' },
        { descripcion: 'HOUSTON', codigo: '159' },
        {
          descripcion:
            'PUERTOS DEL GOLFO DE MÉXICO, OTROS NO ESPECIFICADOS COMPRENDIDOS ENTRE KEY WEST Y BROWNSVILLE',
          codigo: '122'
        },
        { descripcion: 'SEATTLE', codigo: '171' },
        { descripcion: 'PORTLAND', codigo: '172' },
        { descripcion: 'SAN FRANCISCO', codigo: '173' },
        { descripcion: 'LOS ANGELES', codigo: '174' },
        { descripcion: 'LONG BEACH', codigo: '175' },
        { descripcion: 'SAN DIEGO', codigo: '176' },
        { descripcion: 'COSTA DEL PACÍFICO, OTROS NO ESPECIFICADOS', codigo: '123' },
        { descripcion: 'EVERGLADES', codigo: '142' },
        { descripcion: 'JACKSONVILLE', codigo: '143' },
        { descripcion: 'PALM BEACH', codigo: '145' },
        { descripcion: 'BATON ROUGE', codigo: '146' },
        { descripcion: 'COLUMBRES', codigo: '147' },
        { descripcion: 'PITTSBURGH', codigo: '148' },
        { descripcion: 'DULUTH', codigo: '149' },
        { descripcion: 'MILWAUKEE', codigo: '150' },
        { descripcion: 'OAKLAND', codigo: '160' },
        { descripcion: 'STOCKTON', codigo: '161' },
        { descripcion: 'OTROS PUERTOS DE ESTADOS UNIDOS NO ESPECIFICADOS', codigo: '180' }
      ];

    case 'MX':
      return [
        { descripcion: 'TAMPICO', codigo: '211' },
        { descripcion: 'VERACRUZ', codigo: '213' },
        { descripcion: 'GOLFO DE MÉXICO, OTROS NO ESPECIFICADOS', codigo: '219' },
        { descripcion: 'COATZACOALCOS', codigo: '214' },
        { descripcion: 'GUAYMAS', codigo: '215' },
        { descripcion: 'MAZATLAN', codigo: '216' },
        { descripcion: 'MANZANILLO', codigo: '217' },
        { descripcion: 'ACAPULCO', codigo: '218' },
        { descripcion: 'COSTA DEL PACÍFICO, OTROS PUERTOS', codigo: '212' },
        { descripcion: 'OTROS PUERTOS DE MÉXICO NO ESPECIFICADOS', codigo: '210' },
        { descripcion: 'ALTAMIRA', codigo: '220' }
      ];

    case 'PA':
      return [
        { descripcion: 'CRISTOBAL', codigo: '221' },
        { descripcion: 'BALBOA', codigo: '222' },
        { descripcion: 'COLON', codigo: '223' },
        { descripcion: 'OTROS PUERTOS DE PANAMÁ NO ESPECIFICADOS', codigo: '224' }
      ];

    case 'CO':
      return [
        { descripcion: 'BUENAVENTURA', codigo: '232' },
        { descripcion: 'OTROS PUERTOS DE COLOMBIA NO ESPECIFICADOS', codigo: '231' },
        { descripcion: 'BARRANQUILLA', codigo: '233' }
      ];

    case 'EC':
      return [
        { descripcion: 'GUAYAQUIL', codigo: '242' },
        { descripcion: 'OTROS PUERTOS DE ECUADOR NO ESPECIFICADOS', codigo: '241' }
      ];

    case 'PE':
      return [
        { descripcion: 'CALLAO', codigo: '252' },
        { descripcion: 'ILO', codigo: '253' },
        { descripcion: 'IQUITOS', codigo: '254' },
        { descripcion: 'OTROS PUERTOS DE PERÚ NO ESPECIFICADOS', codigo: '251' }
      ];

    case 'AR':
      return [
        { descripcion: 'BUENOS AIRES', codigo: '262' },
        { descripcion: 'NECOCHEA', codigo: '263' },
        { descripcion: 'MENDOZA', codigo: '264' },
        { descripcion: 'CÓRDOBA', codigo: '265' },
        { descripcion: 'OTROS PUERTOS DE ARGENTINA NO ESPECIFICADOS', codigo: '261' },
        { descripcion: 'BAHIA BLANCA', codigo: '266' },
        { descripcion: 'COMODORO RIVADAVIA', codigo: '267' },
        { descripcion: 'PUERTO MADRYN', codigo: '268' },
        { descripcion: 'MAR DEL PLATA', codigo: '269' },
        { descripcion: 'ROSARIO', codigo: '270' }
      ];

    case 'UY':
      return [
        { descripcion: 'MONTEVIDEO', codigo: '272' },
        { descripcion: 'OTROS PUERTOS DE URUGUAY NO ESPECIFICADOS', codigo: '271' }
      ];

    case 'VE':
      return [
        { descripcion: 'LA GUAIRA', codigo: '282' },
        { descripcion: 'OTROS PUERTOS DE VENEZUELA NO ESPECIFICADOS', codigo: '281' },
        { descripcion: 'MARACAIBO', codigo: '285' }
      ];

    case 'BR':
      return [
        { descripcion: 'SANTOS', codigo: '292' },
        { descripcion: 'RIO DE JANEIRO', codigo: '293' },
        { descripcion: 'RIO GRANDE DEL SUR', codigo: '294' },
        { descripcion: 'PARANAGUA', codigo: '295' },
        { descripcion: 'SAO PAULO', codigo: '296' },
        { descripcion: 'SALVADOR', codigo: '297' },
        { descripcion: 'OTROS PUERTOS DE BRASIL NO ESPECIFICADOS', codigo: '291' }
      ];

    case 'AN':
      return [
        { descripcion: 'CURAZAO', codigo: '302' },
        { descripcion: 'OTROS PUERTOS DE LAS ANTILLAS HOLANDESAS NO ESPECIFICADOS', codigo: '301' }
      ];

    case 'CN':
      return [
        { descripcion: 'SHANGAI', codigo: '411' },
        { descripcion: 'DAIREN', codigo: '412' },
        { descripcion: 'OTROS PUERTOS DE CHINA NO ESPECIFICADOS', codigo: '413' }
      ];

    case 'KP':
      return [
        { descripcion: 'NAMPO', codigo: '421' },
        { descripcion: 'OTROS PUERTOS DE COREA DEL NORTE NO ESPECIFICADOS', codigo: '420' }
      ];

    case 'KR':
      return [
        { descripcion: 'BUSAN', codigo: '422' },
        { descripcion: 'OTROS PUERTOS DE COREA DEL SUR NO ESPECIFICADOS', codigo: '423' }
      ];

    case 'PH':
      return [
        { descripcion: 'MANILA', codigo: '431' },
        { descripcion: 'OTROS PUERTOS DE FILIPINAS NO ESPECIFICADOS', codigo: '432' }
      ];

    case 'JP':
      return [
        { descripcion: 'OSAKA', codigo: '442' },
        { descripcion: 'KOBE', codigo: '443' },
        { descripcion: 'YOKOHAMA', codigo: '444' },
        { descripcion: 'NAGOYA', codigo: '445' },
        { descripcion: 'SHIMIZUI', codigo: '446' },
        { descripcion: 'MOJI', codigo: '447' },
        { descripcion: 'YAWATA', codigo: '448' },
        { descripcion: 'FUKUYAMA', codigo: '449' },
        { descripcion: 'OTROS PUERTOS DE JAPON NO ESPECIFICADOS', codigo: '441' }
      ];

    case 'TW':
      return [
        { descripcion: 'KAOHSIUNG', codigo: '451' },
        { descripcion: 'KEELUNG', codigo: '452' },
        { descripcion: 'OTROS PUERTOS DE TAIWAN NO ESPECIFICADOS', codigo: '453' }
      ];

    case 'IR':
      return [
        { descripcion: 'KARHG ISLAND', codigo: '461' },
        { descripcion: 'OTROS PUERTOS DE IRAN NO ESPECIFICADOS', codigo: '462' }
      ];

    case 'IN':
      return [
        { descripcion: 'CALCUTA', codigo: '471' },
        { descripcion: 'OTROS PUERTOS DE INDIA NO ESPECIFICADOS', codigo: '472' }
      ];

    case 'BD':
      return [
        { descripcion: 'CHALNA', codigo: '481' },
        { descripcion: 'OTROS PUERTOS DE BANGLADESH NO ESPECIFICADOS', codigo: '482' }
      ];

    case 'SG':
      return [
        { descripcion: 'HONG KONG', codigo: '492' },
        { descripcion: 'OTROS PUERTOS DE SINGAPUR NO ESPECIFICADOS', codigo: '491' }
      ];

    case 'RO':
      return [
        { descripcion: 'CONSTANZA', codigo: '511' },
        { descripcion: 'OTROS PUERTOS DE RUMANIA NO ESPECIFICADOS', codigo: '512' }
      ];

    case 'BG':
      return [
        { descripcion: 'VARNA', codigo: '521' },
        { descripcion: 'OTROS PUERTOS DE BULGARIA NO ESPECIFICADOS', codigo: '522' }
      ];

    case 'HR':
      return [
        { descripcion: 'RIJEKA', codigo: '538' },
        { descripcion: 'OTROS PUERTOS DE CROACIA NO ESPECIFICADOS', codigo: '537' }
      ];

    case 'IT':
      return [
        { descripcion: 'GENOVA', codigo: '542' },
        { descripcion: 'LIORNA, LIVORNO', codigo: '543' },
        { descripcion: 'NAPOLES', codigo: '544' },
        { descripcion: 'SALERNO', codigo: '545' },
        { descripcion: 'AUGUSTA', codigo: '546' },
        { descripcion: 'SAVONA', codigo: '547' },
        { descripcion: 'OTROS PUERTOS DE ITALIA NO ESPECIFICADOS', codigo: '541' }
      ];

    case 'FR':
      return [
        { descripcion: 'LA PALLICE', codigo: '552' },
        { descripcion: 'LE HAVRE', codigo: '553' },
        { descripcion: 'MARSELLA', codigo: '554' },
        { descripcion: 'OTROS PUERTOS DE FRANCIA NO ESPECIFICADOS', codigo: '551' },
        { descripcion: 'BURDEOS', codigo: '555' },
        { descripcion: 'CALAIS', codigo: '556' },
        { descripcion: 'BREST', codigo: '557' },
        { descripcion: 'RUAN', codigo: '558' }
      ];

    case 'ES':
      return [
        { descripcion: 'CADIZ', codigo: '562' },
        { descripcion: 'BARCELONA', codigo: '563' },
        { descripcion: 'BILBAO', codigo: '564' },
        { descripcion: 'HUELVA', codigo: '565' },
        { descripcion: 'SEVILLA', codigo: '566' },
        { descripcion: 'OTROS PUERTOS DE ESPAÑA NO ESPECIFICADOS', codigo: '561' },
        { descripcion: 'TARRAGONA', codigo: '567' },
        { descripcion: 'ALGECIRAS', codigo: '568' },
        { descripcion: 'VALENCIA ', codigo: '569' }
      ];

    case 'GB':
      return [
        { descripcion: 'LIVERPOOL', codigo: '571' },
        { descripcion: 'LONDRES', codigo: '572' },
        { descripcion: 'ROCHESTER', codigo: '573' },
        { descripcion: 'ETEN SALVERRY', codigo: '574' },
        { descripcion: 'OTROS PUERTOS DE INGLATERRA NO ESPECIFICADOS', codigo: '576' },
        { descripcion: 'DOVER', codigo: '577' },
        { descripcion: 'PLYMOUTH', codigo: '578' }
      ];

    case 'FI':
      return [
        { descripcion: 'HELSINSKI', codigo: '581' },
        { descripcion: 'HANKO', codigo: '583' },
        { descripcion: 'KEMI', codigo: '584' },
        { descripcion: 'KOKKOLA', codigo: '585' },
        { descripcion: 'KOTKA', codigo: '586' },
        { descripcion: 'OULO', codigo: '587' },
        { descripcion: 'PIETARSAARI', codigo: '588' },
        { descripcion: 'PORI', codigo: '589' },
        { descripcion: 'OTROS PUERTOS DE FINLANDIA NO ESPECIFICADOS', codigo: '582' }
      ];

    case 'DE':
      return [
        { descripcion: 'BREMEN', codigo: '591' },
        { descripcion: 'HAMBURGO', codigo: '592' },
        { descripcion: 'NUREMBERG', codigo: '593' },
        { descripcion: 'FRANKFURT', codigo: '594' },
        { descripcion: 'DUSSELDORF', codigo: '595' },
        { descripcion: 'OTROS PUERTOS DE ALEMANIA NO ESPECIFICADOS', codigo: '596' },
        { descripcion: 'CUXHAVEN', codigo: '597' },
        { descripcion: 'ROSTOCK', codigo: '598' },
        { descripcion: 'OLDENBURG', codigo: '599' }
      ];

    case 'BE':
      return [
        { descripcion: 'AMBERES', codigo: '601' },
        { descripcion: 'OTROS PUERTOS DE BÉLGICA NO ESPECIFICADOS', codigo: '602' },
        { descripcion: 'GHENT', codigo: '604' },
        { descripcion: 'OOSTENDE', codigo: '605' },
        { descripcion: 'ZEEBRUGGE', codigo: '603' }
      ];

    case 'PT':
      return [
        { descripcion: 'LISBOA', codigo: '611' },
        { descripcion: 'OTROS PUERTOS DE PORTUGAL NO ESPECIFICADOS', codigo: '612' },
        { descripcion: 'SETUBAL', codigo: '613' }
      ];

    case 'NL':
      return [
        { descripcion: 'AMSTERDAM', codigo: '621' },
        { descripcion: 'ROTTERDAM', codigo: '622' },
        { descripcion: 'OTROS PUERTOS DE HOLANDA NO ESPECIFICADOS', codigo: '623' }
      ];

    case 'SE':
      return [
        { descripcion: 'GOTEMBURGO', codigo: '631' },
        { descripcion: 'MALMO', codigo: '633' },
        { descripcion: 'HELSIMBORG', codigo: '634' },
        { descripcion: 'KALMAR', codigo: '635' },
        { descripcion: 'OTROS PUERTOS DE SUECIA NO ESPECIFICADOS', codigo: '632' }
      ];

    case 'DK':
      return [
        { descripcion: 'AARHUS', codigo: '641' },
        { descripcion: 'COPENHAGEN', codigo: '642' },
        { descripcion: 'OTROS PUERTOS DE DINAMARCA NO ESPECIFICADOS', codigo: '643' },
        { descripcion: 'AALBORG', codigo: '644' },
        { descripcion: 'ODENSE', codigo: '645' }
      ];

    case 'NO':
      return [
        { descripcion: 'OSLO', codigo: '651' },
        { descripcion: 'OTROS PUERTOS DE NORUEGA NO ESPECIFICADOS', codigo: '652' },
        { descripcion: 'STAVANGER', codigo: '653' }
      ];

    case 'RS':
      return [
        { descripcion: 'BELGRADO', codigo: '533' },
        { descripcion: 'OTROS PUERTOS DE SERBIA NO ESPECIFICADOS', codigo: '534' }
      ];

    case 'ME':
      return [
        { descripcion: 'PODGORITSA', codigo: '535' },
        { descripcion: 'OTROS PUERTOS DE MONTENEGRO NO ESPECIFICADOS', codigo: '536' }
      ];

    case 'ZA':
      return [
        { descripcion: 'DURBAM', codigo: '711' },
        { descripcion: 'CIUDAD DEL CABO', codigo: '712' },
        { descripcion: 'OTROS PUERTOS DE SUDÁFRICA NO ESPECIFICADOS', codigo: '713' },
        { descripcion: 'SALDANHA', codigo: '714' },
        { descripcion: 'PORT-ELIZABETH', codigo: '715' },
        { descripcion: 'MOSSEL-BAY', codigo: '716' },
        { descripcion: 'EAST-LONDON', codigo: '717' }
      ];

    case 'AU':
      return [
        { descripcion: 'SIDNEY', codigo: '811' },
        { descripcion: 'FREMANTLE', codigo: '812' },
        { descripcion: 'ADELAIDA', codigo: '814' },
        { descripcion: 'DARWIN', codigo: '815' },
        { descripcion: 'GERALDTON', codigo: '816' },
        { descripcion: 'OTROS PUERTOS DE AUSTRALIA NO ESPECIFICADOS', codigo: '813' }
      ];

    case 'CL':
      return [
        { descripcion: 'ZONA FRANCA IQUIQUE', codigo: '952' },
        { descripcion: 'ZONA FRANCA PUNTA ARENAS', codigo: '953' },
        { descripcion: 'PUERTO: ARICA', codigo: '901' },
        { descripcion: 'PUERTO: IQUIQUE', codigo: '902' },
        { descripcion: 'PUERTO: ANTOFAGASTA', codigo: '903' },
        { descripcion: 'PUERTO: COQUIMBO', codigo: '904' },
        { descripcion: 'PUERTO: VALPARAÍSO', codigo: '905' },
        { descripcion: 'PUERTO: SAN ANTONIO', codigo: '906' },
        { descripcion: 'PUERTO: TALCAHUANO', codigo: '907' },
        { descripcion: 'PUERTO: SAN VICENTE', codigo: '908' },
        { descripcion: 'PUERTO: LIRQUEN', codigo: '909' },
        { descripcion: 'PUERTO: PUERTO MONTT', codigo: '910' },
        { descripcion: 'PUERTO: CHACABUCO / PUERTO AYSEN', codigo: '911' },
        { descripcion: 'PUERTO: PUNTA ARENAS', codigo: '912' },
        { descripcion: 'PUERTO: PATILLOS', codigo: '913' },
        { descripcion: 'PUERTO: TOCOPILLA', codigo: '914' },
        { descripcion: 'PUERTO: MEJILLONES', codigo: '915' },
        { descripcion: 'PUERTO: TALTAL', codigo: '916' },
        { descripcion: 'PUERTO: CHAÑARAL / BARQUITO', codigo: '917' },
        { descripcion: 'PUERTO: CALDERA', codigo: '918' },
        { descripcion: 'PUERTO: CALDERILLA', codigo: '919' },
        { descripcion: 'PUERTO: HUASCO / GUACOLDA', codigo: '920' },
        { descripcion: 'PUERTO: QUINTERO', codigo: '921' },
        { descripcion: 'PUERTO: JUAN FERNANDEZ', codigo: '922' },
        { descripcion: 'PUERTO: CONSTITUCION', codigo: '923' },
        { descripcion: 'PUERTO: TOME', codigo: '924' },
        { descripcion: 'PUERTO: PENCO', codigo: '925' },
        { descripcion: 'PUERTO: CORONEL', codigo: '926' },
        { descripcion: 'PUERTO: LOTA', codigo: '927' },
        { descripcion: 'PUERTO: LEBU', codigo: '928' },
        { descripcion: 'PUERTO: ISLA DE PASCUA', codigo: '929' },
        { descripcion: 'PUERTO: CORRAL', codigo: '930' },
        { descripcion: 'PUERTO: ANCUD', codigo: '931' },
        { descripcion: 'PUERTO: CASTRO', codigo: '932' },
        { descripcion: 'PUERTO: QUELLÓN', codigo: '933' },
        { descripcion: 'PUERTO: CHAITÉN', codigo: '934' },
        { descripcion: 'PUERTO: TORTEL', codigo: '935' },
        { descripcion: 'PUERTO: NATALES', codigo: '936' },
        { descripcion: 'PUERTO: GUARELLO', codigo: '937' },
        { descripcion: 'PUERTO: PERCY', codigo: '939' },
        { descripcion: 'PUERTO: CLARENCIA', codigo: '940' },
        { descripcion: 'PUERTO: GREGORIO', codigo: '941' },
        { descripcion: 'PUERTO: CABO NEGRO', codigo: '942' },
        { descripcion: 'PUERTO: PUERTO WILLIAMS', codigo: '943' },
        { descripcion: 'PUERTO: TERRITORIO ANTÁRTICO CHILENO', codigo: '944' },
        { descripcion: 'PUERTO: AEROPUERTO CARRIEL SUR', codigo: '945' },
        { descripcion: 'PUERTO: GUAYACAN', codigo: '946' },
        { descripcion: 'PUERTO: VENTANAS', codigo: '948' },
        { descripcion: 'PUERTO: LOS VILOS', codigo: '199' },
        { descripcion: 'PUERTO: CALETA COLOSO', codigo: '950' },
        { descripcion: 'PUERTO: PATACHE', codigo: '204' },
        { descripcion: 'PUERTO: CALBUCO', codigo: '205' },
        { descripcion: 'PUERTO: MICHILLA', codigo: '206' },
        { descripcion: 'PUERTO: PUERTO ANGAMOS', codigo: '207' },
        { descripcion: 'PUERTO: POSEIDON', codigo: '208' },
        { descripcion: 'PUERTO: TRES PUENTES', codigo: '209' },
        { descripcion: 'PUERTO: PUERTO ANDINO ', codigo: '938' },
        { descripcion: 'PUERTO: OTROS PUERTOS CHILENOS', codigo: '997' },
        {
          descripcion: 'PUERTO: RANCHO DE NAVES Y AERONAVES DE TRANSPORTE INTERNACIONAL',
          codigo: '900'
        },
        {
          descripcion:
            'PUERTO: COMBUSTIBLES Y LUBRICANTES DESTINADOS AL CONSUMO DE NAVES Y AERONAVES DE TRANSPORTE INTERNACIONAL',
          codigo: '900'
        },
        { descripcion: 'AVANZADAS: VISVIRI', codigo: '956' },
        { descripcion: 'AVANZADAS: CHACALLUTA', codigo: '957' },
        { descripcion: 'AVANZADAS: CHUNGARÁ', codigo: '958' },
        { descripcion: 'AVANZADAS: COLCHANE', codigo: '959' },
        { descripcion: 'AVANZADAS: ABRA DE NAPA', codigo: '960' },
        { descripcion: 'AVANZADAS: OLLAGUE', codigo: '961' },
        { descripcion: 'AVANZADAS: SAN PEDRO DE ATACAMA', codigo: '962' },
        { descripcion: 'AVANZADAS: SOCOMPA', codigo: '963' },
        { descripcion: 'AVANZADAS: SAN FRANCISCO', codigo: '964' },
        { descripcion: 'AVANZADAS: AGUAS NEGRAS', codigo: '951' },
        { descripcion: 'AVANZADAS: LOS LIBERTADORES', codigo: '965' },
        { descripcion: 'AVANZADAS: MAHUIL MALAL', codigo: '966' },
        { descripcion: 'AVANZADAS: CARDENAL SAMORE', codigo: '967' },
        { descripcion: 'AVANZADAS: PEREZ ROSALES', codigo: '968' },
        { descripcion: 'AVANZADAS: FUTALEUFU', codigo: '969' },
        { descripcion: 'AVANZADAS: PALENA - CARRENLEUFU', codigo: '970' },
        { descripcion: 'AVANZADAS: PANGUIPULLI', codigo: '971' },
        { descripcion: 'AVANZADAS: HUAHUM', codigo: '972' },
        { descripcion: 'AVANZADAS: LAGO VERDE', codigo: '973' },
        { descripcion: 'AVANZADAS: APPELEG', codigo: '974' },
        { descripcion: 'AVANZADAS: PAMPA ALTA', codigo: '975' },
        { descripcion: 'AVANZADAS: HUEMULES', codigo: '976' },
        { descripcion: 'AVANZADAS: CHILE CHICO', codigo: '977' },
        { descripcion: 'AVANZADAS: BAKER', codigo: '978' },
        { descripcion: 'AVANZADAS: DOROTEA', codigo: '979' },
        { descripcion: 'AVANZADAS: CASAS VIEJAS', codigo: '980' },
        { descripcion: 'AVANZADAS: MONTE AYMOND', codigo: '981' },
        { descripcion: 'AVANZADAS: SAN SEBASTIAN', codigo: '982' },
        { descripcion: 'AVANZADAS: COYHAIQUE ALTO', codigo: '983' },
        { descripcion: 'AVANZADAS: TRIANA', codigo: '984' },
        { descripcion: 'AVANZADAS: IBAÑEZ PALAVICINI', codigo: '985' },
        { descripcion: "AVANZADAS: VILLA O'HIGGINS", codigo: '986' },
        { descripcion: 'AVANZADAS: RÍO MAYER', codigo: '954' },
        { descripcion: 'AVANZADAS: RÍO MOSCO', codigo: '955' },
        { descripcion: 'AVANZADAS: PINO HACHADO (LIUCURA)', codigo: '949' },
        { descripcion: 'AVANZADAS: PASO JAMA ', codigo: '998' },
        { descripcion: 'AEROP. CHACALLUTA', codigo: '987' },
        { descripcion: 'AEROP. DIEGO ARACENA', codigo: '988' },
        { descripcion: 'AEROP. CERRO MORENO', codigo: '989' },
        { descripcion: 'AEROP. EL TEPUAL', codigo: '990' },
        { descripcion: 'AEROP. C.I. DEL CAMPO', codigo: '991' },
        { descripcion: 'AEROP. A.M. BENITEZ', codigo: '992' },
        { descripcion: 'ESTACIÓN DE FERROCARRIL: ARICA-TACNA', codigo: '994' },
        { descripcion: 'ESTACIÓN DE FERROCARRIL: ARICA-LA PAZ', codigo: '995' }
      ];

    default:
      return [
        { descripcion: 'OTROS PUERTOS DE EUROPA NO ESPECIFICADOS ', codigo: '699' },
        { descripcion: 'OTROS PUERTOS ASIÁTICOS NO ESPECIFICADOS', codigo: '499' },
        { descripcion: 'OTROS PUERTOS DE AMÉRICA NO ESPECIFICADOS', codigo: '399' },
        { descripcion: 'OTROS PUERTOS DE ÁFRICA NO ESPECIFICADOS', codigo: '799' },
        { descripcion: 'OTROS PUERTOS DE OCEANÍA NO ESPECIFICADOS', codigo: '899' }
      ];
  }
}
