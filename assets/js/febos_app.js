// Production steps of ECMA-262, Edition 5, 15.4.4.21
// Reference: http://es5.github.io/#x15.4.4.21
// https://tc39.github.io/ecma262/#sec-array.prototype.reduce
if (!Array.prototype.reduce) {
  Object.defineProperty(Array.prototype, 'reduce', {
    value: function(callback /*, initialValue*/) {
      if (this === null) {
        throw new TypeError('Array.prototype.reduce ' + 'called on null or undefined');
      }
      if (typeof callback !== 'function') {
        throw new TypeError(callback + ' is not a function');
      }

      // 1. Let O be ? ToObject(this value).
      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // Steps 3, 4, 5, 6, 7
      var k = 0;
      var value;

      if (arguments.length >= 2) {
        value = arguments[1];
      } else {
        while (k < len && !(k in o)) {
          k++;
        }

        // 3. If len is 0 and initialValue is not present,
        //    throw a TypeError exception.
        if (k >= len) {
          throw new TypeError('Reduce of empty array ' + 'with no initial value');
        }
        value = o[k++];
      }

      // 8. Repeat, while k < len
      while (k < len) {
        // a. Let Pk be ! ToString(k).
        // b. Let kPresent be ? HasProperty(O, Pk).
        // c. If kPresent is true, then
        //    i.  Let kValue be ? Get(O, Pk).
        //    ii. Let accumulator be ? Call(
        //          callbackfn, undefined,
        //          « accumulator, kValue, k, O »).
        if (k in o) {
          value = callback(value, o[k], k, o);
        }

        // d. Increase k by 1.
        k++;
      }

      // 9. Return accumulator.
      return value;
    }
  });
}

// Production steps of ECMA-262, Edition 5, 15.4.4.19
// Reference: http://es5.github.io/#x15.4.4.19
if (!Array.prototype.map) {
  Array.prototype.map = function(callback /*, thisArg*/) {
    var T, A, k;

    if (this == null) {
      throw new TypeError('this is null or not defined');
    }

    // 1. Let O be the result of calling ToObject passing the |this|
    //    value as the argument.
    var O = Object(this);

    // 2. Let lenValue be the result of calling the Get internal
    //    method of O with the argument "length".
    // 3. Let len be ToUint32(lenValue).
    var len = O.length >>> 0;

    // 4. If IsCallable(callback) is false, throw a TypeError exception.
    // See: http://es5.github.com/#x9.11
    if (typeof callback !== 'function') {
      throw new TypeError(callback + ' is not a function');
    }

    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
    if (arguments.length > 1) {
      T = arguments[1];
    }

    // 6. Let A be a new array created as if by the expression new Array(len)
    //    where Array is the standard built-in constructor with that name and
    //    len is the value of len.
    A = new Array(len);

    // 7. Let k be 0
    k = 0;

    // 8. Repeat, while k < len
    while (k < len) {
      var kValue, mappedValue;

      // a. Let Pk be ToString(k).
      //   This is implicit for LHS operands of the in operator
      // b. Let kPresent be the result of calling the HasProperty internal
      //    method of O with argument Pk.
      //   This step can be combined with c
      // c. If kPresent is true, then
      if (k in O) {
        // i. Let kValue be the result of calling the Get internal
        //    method of O with argument Pk.
        kValue = O[k];

        // ii. Let mappedValue be the result of calling the Call internal
        //     method of callback with T as the this value and argument
        //     list containing kValue, k, and O.
        mappedValue = callback.call(T, kValue, k, O);

        // iii. Call the DefineOwnProperty internal method of A with arguments
        // Pk, Property Descriptor
        // { Value: mappedValue,
        //   Writable: true,
        //   Enumerable: true,
        //   Configurable: true },
        // and false.

        // In browsers that support Object.defineProperty, use the following:
        // Object.defineProperty(A, k, {
        //   value: mappedValue,
        //   writable: true,
        //   enumerable: true,
        //   configurable: true
        // });

        // For best browser support, use the following:
        A[k] = mappedValue;
      }
      // d. Increase k by 1.
      k++;
    }

    // 9. return A
    return A;
  };
}
// https://tc39.github.io/ecma262/#sec-array.prototype.find
if (!Array.prototype.find) {
  Object.defineProperty(Array.prototype, 'find', {
    value: function(predicate) {
      // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If IsCallable(predicate) is false, throw a TypeError exception.
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }

      // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
      var thisArg = arguments[1];

      // 5. Let k be 0.
      var k = 0;

      // 6. Repeat, while k < len
      while (k < len) {
        // a. Let Pk be ! ToString(k).
        // b. Let kValue be ? Get(O, Pk).
        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
        // d. If testResult is true, return kValue.
        var kValue = o[k];
        if (predicate.call(thisArg, kValue, k, o)) {
          return kValue;
        }
        // e. Increase k by 1.
        k++;
      }

      // 7. Return undefined.
      return undefined;
    },
    configurable: true,
    writable: true
  });
}
// https://tc39.github.io/ecma262/#sec-array.prototype.findindex
if (!Array.prototype.findIndex) {
  Object.defineProperty(Array.prototype, 'findIndex', {
    value: function(predicate) {
      // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If IsCallable(predicate) is false, throw a TypeError exception.
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }

      // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
      var thisArg = arguments[1];

      // 5. Let k be 0.
      var k = 0;

      // 6. Repeat, while k < len
      while (k < len) {
        // a. Let Pk be ! ToString(k).
        // b. Let kValue be ? Get(O, Pk).
        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
        // d. If testResult is true, return k.
        var kValue = o[k];
        if (predicate.call(thisArg, kValue, k, o)) {
          return k;
        }
        // e. Increase k by 1.
        k++;
      }

      // 7. Return -1.
      return -1;
    },
    configurable: true,
    writable: true
  });
}

/*
 *  Altair Admin AngularJS
 */
('use strict');

var febosApp = angular.module('febosApp', [
  'ui.router',
  'oc.lazyLoad',
  'ngSanitize',
  'ngRetina',
  'ncy-angular-breadcrumb',
  'ConsoleLogger',
  'ngStorage',
  'matchMedia',
  'ngIdle',
  'ngTagsInput'
]);

// inputs vacios en tags-inputs (placeholder="" no funca como argumento)
febosApp.config(function(tagsInputConfigProvider) {
  tagsInputConfigProvider.setDefaults('tagsInput', {
    placeholder: '',
    minLength: 1
  });
});

febosApp.constant('variables', {
  header_main_height: 48,
  easing_swiftOut: [0.4, 0, 0.2, 1],
  bez_easing_swiftOut: $.bez([0.4, 0, 0.2, 1])
});
febosApp.constant('CONFIGURACION', {
  FEBOS_WEB_VERSION: '[FEBOS_WEB_VERSION]',
  AMBITO_POR_DEFECTO: 'cloud',
  PAIS: 'Chile',
  AMBIENTE_POR_DEFECTO: 'desarrollo',
  BASE_API_URL: 'https://api.febos.' + 'cl' + '/',
  MAX_REINTENTOS_API_CALLS: 3, //para cuando da timeout
  DOMINIOS_CON_ENVIO_DE_CABECERAS_AUTOMATICO: ['api.febos.' + 'cl'],
  DOMINIO: window.location.href.split('/')[2].split(':')[0]
});

febosApp.config(function($sceDelegateProvider) {
  $sceDelegateProvider.resourceUrlWhitelist([
    'self',
    'https://www.youtube.com/**',
    'https://w.soundcloud.com/**'
  ]);
});

febosApp.config([
  '$localStorageProvider',
  'CONFIGURACION',
  function($localStorageProvider, CONFIGURACION) {
    var ambiente = 'desarrollo';
    var posiblesAmbientes = /desarrollo|produccion|certificacion|pruebas/i;
    try {
      var partes = window.location.href
        .replace('https://', '')
        .replace('http://', '')
        .split('/');
      ambiente =
        partes[1].match(posiblesAmbientes) != null ? partes[1] : CONFIGURACION.AMBIENTE_POR_DEFECTO;
    } catch (e) {
      console.log(
        'imposible detectar ambiente, utilizando el por defecto: ' +
          CONFIGURACION.AMBIENTE_POR_DEFECTO
      );
      ambiente = CONFIGURACION.AMBIENTE_POR_DEFECTO;
    }
    $localStorageProvider.setKeyPrefix('Febos.' + CONFIGURACION.PAIS + '.' + ambiente + '.');
  }
]);
febosApp.config(function() {
  //LOCALIZACION PARA MOMENTJS
  moment.locale('es', {
    months: 'enero_febrero_marzo_abril_mayo_junio_julio_agosto_septiembre_octubre_noviembre_diciembre'.split(
      '_'
    ),
    monthsShort: 'ene._feb._mar._abr._may._jun._jul._ago._sep._oct._nov._dic.'.split('_'),
    monthsParseExact: true,
    weekdays: 'domingo_lunes_martes_miercoles_jueves_viernes_sabado'.split('_'),
    weekdaysShort: 'dom._lun._mar._mie._jue._vie._sab.'.split('_'),
    weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_'),
    weekdaysParseExact: true,
    longDateFormat: {
      LT: 'HH:mm',
      LTS: 'HH:mm:ss',
      L: 'DD/MM/YYYY',
      LL: 'D MMMM YYYY',
      LLL: 'D MMMM YYYY HH:mm',
      LLLL: 'dddd D MMMM YYYY HH:mm'
    },
    calendar: {
      sameDay: '[Hoy] LT',
      nextDay: '[Mañana] LT',
      nextWeek: 'dddd [el] LT',
      lastDay: '[Ayer] LT',
      lastWeek: 'dddd [el] LT',
      sameElse: 'L'
    },
    relativeTime: {
      future: 'a las %s',
      past: 'a las %s',
      s: 'segundos',
      m: 'minuto',
      mm: '%d minuto',
      h: 'un hora',
      hh: '%d horas',
      d: 'un día',
      dd: '%d días',
      M: 'un mes',
      MM: '%d meses',
      y: 'un año',
      yy: '%d años'
    },
    dayOfMonthOrdinalParse: /\d{1,2}(er|e)/,
    ordinal: function(number) {
      return number + (number === 1 ? 'er' : 'e');
    },
    meridiemParse: /PD|MD/,
    isPM: function(input) {
      return input.charAt(0) === 'M';
    },
    // In case the meridiem units are not separated around 12, then implement
    // this function (look at locale/id.js for an example).
    // meridiemHour : function (hour, meridiem) {
    //     return /* 0-23 hour, given meridiem token and hour 1-12 */ ;
    // },
    meridiem: function(hours, minutes, isLower) {
      return hours < 12 ? 'AM' : 'PM';
    },
    week: {
      dow: 1, // Monday is the first day of the week.
      doy: 4 // The week that contains Jan 4th is the first week of the year.
    }
  });
});
// breadcrumbs
febosApp.config(function($breadcrumbProvider) {
  $breadcrumbProvider.setOptions({
    prefixStateName: 'restricted.dashboard',
    templateUrl: 'app/templates/breadcrumbs.tpl.html'
  });
});

//KEEP ALIVE!
febosApp.config([
  'IdleProvider',
  'KeepaliveProvider',
  function(IdleProvider, KeepaliveProvider) {
    IdleProvider.idle(60 * 14); // 14 minutos sin hacer nada, y se mata la sesion
    IdleProvider.timeout(30); // 30 segundos para reaccionar
    IdleProvider.keepalive(true); //enviar un heartbeat para mantener la session activa mientras se usa
    KeepaliveProvider.interval(60); //cada 10 minutos en enviar un heartbeat
  }
]);

/* detect IE */
function detectIE() {
  var a = window.navigator.userAgent,
    b = a.indexOf('MSIE ');
  if (0 < b) return parseInt(a.substring(b + 5, a.indexOf('.', b)), 10);
  if (0 < a.indexOf('Trident/'))
    return (b = a.indexOf('rv:')), parseInt(a.substring(b + 3, a.indexOf('.', b)), 10);
  b = a.indexOf('Edge/');
  return 0 < b ? parseInt(a.substring(b + 5, a.indexOf('.', b)), 10) : !1;
}

/* Run Block */
febosApp
  .run([
    '$rootScope',
    '$state',
    '$stateParams',
    '$http',
    '$window',
    '$timeout',
    'preloaders',
    'variables',
    '$location',
    '$localStorage',
    function(
      $rootScope,
      $state,
      $stateParams,
      $http,
      $window,
      $timeout,
      preloaders,
      variables,
      $location,
      $localStorage
    ) {
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;
      $rootScope.modalError = '';

      $rootScope.$on('febosError', function(evento, obj) {
        console.log('invocando error!');
        $rootScope.modalError = UIkit.modal('#modal_febos_error');
        console.log('error: ', obj);

        $rootScope.febosError = obj;
        $rootScope.modalError.show();

        $timeout(function() {
          console.log('root', $rootScope);
          $rootScope.pageLoaded = true;
          $rootScope.pageLoading = false;
        }, 400);
      });

      //carga de version y changelog
      $rootScope.converter = new showdown.Converter();
      $rootScope.converter.setOption('emoji', true);
      $http.get('./changelog.md').then(function onSuccess(response) {
        var markup = response.data.replace(/^- \*\*\[co\]\*\*.*\n?/gm, '');
        markup = markup.replace(/^- \*\*\[cl\]\*\*/gm, '- ');
        markup = markup.replace(/^- \*\*\[io\]\*\*/gm, '- ');
        $rootScope.changelog = $rootScope.converter.makeHtml(markup);
        setTimeout(function() {
          $rootScope.$apply();
        }, 200);
      });

      $rootScope.verChangelog = function() {
        UIkit.modal('#mdlChangelog').show();
      };

      $rootScope.logout = function() {
        if ($localStorage.usuarioActual != '') {
          $localStorage.sesiones[$localStorage.usuarioActual].usuario.token = '';
        }
        $localStorage.usuarioActual = '';
        console.log('logout!');
        try {
          if (typeof $rootScope.modalError != 'undefined' && $rootScope.modalError.isActive()) {
            $rootScope.modalError.hide();
          }
        } catch (e) {}
        $timeout(function() {
          $rootScope.pageLoaded = true;
          $rootScope.pageLoading = false;
          $location.search({});
          $location.path('/' + $stateParams.app + '/ingreso');
        }, 400);
      };

      $rootScope.$on('$stateChangeSuccess', function() {
        // scroll view to top
        $('html, body').animate(
          {
            scrollTop: 0
          },
          200
        );

        if (detectIE()) {
          $('svg,canvas,video').each(function() {
            $(this).css('height', 0);
          });
        }

        $timeout(function() {
          $rootScope.pageLoading = false;
        }, 300);

        $timeout(function() {
          $rootScope.pageLoaded = true;
          $rootScope.appInitialized = true;
          // wave effects
          $window.Waves.attach('.md-btn-wave,.md-fab-wave', ['waves-button']);
          $window.Waves.attach('.md-btn-wave-light,.md-fab-wave-light', [
            'waves-button',
            'waves-light'
          ]);
          if (detectIE()) {
            $('svg,canvas,video').each(function() {
              var $this = $(this),
                height = $(this).attr('height'),
                width = $(this).attr('width');

              if (height) {
                $this.css('height', height);
              }
              if (width) {
                $this.css('width', width);
              }
              var peity = $this.prev('.peity_data,.peity');
              if (peity.length) {
                peity.peity().change();
              }
            });
          }
        }, 600);
      });

      $rootScope.$on('$stateChangeStart', function(
        event,
        toState,
        toParams,
        fromState,
        fromParams
      ) {
        // main search
        $rootScope.mainSearchActive = false;
        // secondary sidebar
        $rootScope.sidebar_secondary = false;
        $rootScope.secondarySidebarHiddenLarge = false;

        if ($($window).width() < 1220) {
          // hide primary sidebar
          $rootScope.primarySidebarActive = false;
          $rootScope.hide_content_sidebar = false;
        }
        if (!toParams.hasOwnProperty('hidePreloader')) {
          $rootScope.pageLoading = true;
          $rootScope.pageLoaded = false;
        }
      });

      // fastclick (eliminate the 300ms delay between a physical tap and the firing of a click event on mobile browsers)
      FastClick.attach(document.body);

      // get version from package.json
      $http.get('./package.json').then(function onSuccess(response) {
        $rootScope.appVer = response.data.version;
        console.log('v', $rootScope.appVer);
      });

      // modernizr
      $rootScope.Modernizr = Modernizr;

      // get window width
      var w = angular.element($window);
      $rootScope.largeScreen = w.width() >= 1220;

      w.on('resize', function() {
        return ($rootScope.largeScreen = w.width() >= 1220);
      });

      // show/hide main menu on page load
      $rootScope.primarySidebarOpen = $rootScope.largeScreen;

      $rootScope.pageLoading = true;

      // wave effects
      $window.Waves.init();
    }
  ])
  .run([
    'PrintToConsole',
    function(PrintToConsole) {
      PrintToConsole.active = false;
    }
  ]);

/*
 *  Altair Admin angularjs
 *  controller
 */

angular.module('febosApp').controller('mainCtrl', [
  '$rootScope',
  '$scope',
  'SesionFebos',
  '$location',
  'ContextoFebos',
  'FebosAPI',
  'Idle',
  'screenSize',
  function($rootScope, $scope, SesionFebos, $location, ContextoFebos, FebosAPI, Idle, screenSize) {
    var modal = UIkit.modal('#modal_idle', {
      bgclose: false
    });
    $scope.sesion = SesionFebos();

    try {
      _chatlio.identify(SesionFebos().usuario.id, {
        name: SesionFebos().usuario.nombre,
        email: SesionFebos().usuario.correo,
        rut: SesionFebos().usuario.iut,
        rutEmpresa: SesionFebos().empresa.iut,
        empresa: SesionFebos().empresa.razonSocial,
        empresaID: SesionFebos().empresa.id
      });
    } catch (e) {}

    $rootScope.tipoPantalla = screenSize.get();
    screenSize.on('xs, sm, md, lg', function(isMacth) {
      if (screenSize.get() != $rootScope.tipoPantalla) {
        console.log(
          '"$rootScope.tipoPantalla": nuevo tamaño de pantalla detectado: ' + screenSize.get() + ''
        );
      }
      $rootScope.tipoPantalla = screenSize.get();
    });

    var $sessionCounter = $('#sessionSecondsRemaining');
    Idle.watch();
    $scope.$on('IdleWarn', (e, countdown) => {
      modal.show();
      $sessionCounter.html(countdown);
    });

    $scope.$on('IdleEnd', function() {
      modal.hide();
      $sessionCounter.html('');
    });

    $scope.$on('IdleTimeout', function() {
      modal.hide();
      //$location.path("/ingreso");
      $rootScope.logout();
      //$state.go('login');
      //console.log("MANTENIENDO SESION VIVA!");
    });
    $scope.$on('Keepalive', function() {
      FebosAPI['cl' + '_latido_usuario']({}, {}, false, false).then(
        function(response) {
          SesionFebos().usuario.ultimoPingValido == new Date();
        },
        function(response) {}
      );
    });
    //FIN MANEJO TIMEOUT PARA SESION
  }
]); /*.config(function (IdleProvider, KeepaliveProvider) {
         
         IdleProvider.idle(60 * 14); // 14 minutos sin hacer nada, y se mata la sesion
         IdleProvider.timeout(30); // 30 segundos para reaccionar
         IdleProvider.keepalive(true); //enviar un heartbeat para mantener la session activa mientras se usa
         KeepaliveProvider.interval(60 * 10); //cada 10 minutos en enviar un heartbeat
         });*/

/*
*  Altair Admin AngularJS
*  directives
*/
('use strict');

febosApp
  // page title
  .directive('pageTitle', [
    '$rootScope',
    '$timeout',
    function($rootScope, $timeout) {
      return {
        restrict: 'A',
        link: function() {
          var listener = function(event, toState) {
            var default_title = 'Altair Admin';
            $timeout(function() {
              $rootScope.page_title =
                toState.data && toState.data.pageTitle ? toState.data.pageTitle : '';
            });
          };
          $rootScope.$on('$stateChangeSuccess', listener);
        }
      };
    }
  ])
  // add width/height properities to Image
  .directive('addImageProp', [
    '$timeout',
    'utils',
    function($timeout, utils) {
      return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
          elem.on('load', function() {
            $timeout(function() {
              var w = !utils.isHighDensity()
                  ? $(elem).actual('width')
                  : $(elem).actual('width') / 2,
                h = !utils.isHighDensity()
                  ? $(elem).actual('height')
                  : $(elem).actual('height') / 2;
              $(elem)
                .attr('width', w)
                .attr('height', h);
            });
          });
        }
      };
    }
  ])
  // print page
  .directive('printPage', [
    '$rootScope',
    '$timeout',
    function($rootScope, $timeout) {
      return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
          var message = attrs['printMessage'];
          $(elem).on('click', function(e) {
            e.preventDefault();
            UIkit.modal.confirm(
              message ? message : 'Do you want to print this page?',
              function() {
                // hide sidebar
                $rootScope.primarySidebarActive = false;
                $rootScope.primarySidebarOpen = false;
                // wait for dialog to fully hide
                $timeout(function() {
                  window.print();
                }, 300);
              },
              {
                labels: {
                  Ok: 'print'
                }
              }
            );
          });
        }
      };
    }
  ])
  // full screen
  .directive('fullScreenToggle', [
    function() {
      return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
          $(elem).on('click', function(e) {
            e.preventDefault();
            screenfull.toggle();
            $(window).resize();
          });
        }
      };
    }
  ])
  // single card
  .directive('singleCard', [
    '$window',
    '$timeout',
    function($window, $timeout) {
      return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
          var $md_card_single = $(elem),
            w = angular.element($window);

          function md_card_content_height() {
            var content_height = w.height() - (48 * 2 + 12);
            $md_card_single.find('.md-card-content:not(#selector)').innerHeight(content_height);
          }

          $timeout(function() {
            md_card_content_height();
          }, 100);

          w.on('resize', function(e) {
            // Reset timeout
            $timeout.cancel(scope.resizingTimer);
            // Add a timeout to not call the resizing function every pixel
            scope.resizingTimer = $timeout(function() {
              md_card_content_height();
              return scope.$apply();
            }, 280);
          });
        }
      };
    }
  ])
  // outside list
  .directive('listOutside', [
    '$window',
    '$timeout',
    function($window, $timeout) {
      return {
        restrict: 'A',
        link: function(scope, elem, attr) {
          var $md_list_outside_wrapper = $(elem),
            w = angular.element($window);

          function md_list_outside_height() {
            var content_height = w.height() - (48 * 2 + 10);
            $md_list_outside_wrapper.height(content_height);
          }

          md_list_outside_height();

          w.on('resize', function(e) {
            // Reset timeout
            $timeout.cancel(scope.resizingTimer);
            // Add a timeout to not call the resizing function every pixel
            scope.resizingTimer = $timeout(function() {
              md_list_outside_height();
              return scope.$apply();
            }, 280);
          });
        }
      };
    }
  ])
  // callback on last element in ng-repeat
  .directive('onLastRepeat', function($timeout) {
    return function(scope, element, attrs) {
      if (scope.$last) {
        $timeout(function() {
          scope.$emit('onLastRepeat', element, attrs);
        });
      }
    };
  })
  // check table row
  .directive('tableCheckAll', [
    '$window',
    '$timeout',
    function($window, $timeout) {
      return {
        restrict: 'A',
        link: function(scope, elem, attr) {
          var $checkRow = $(elem)
            .closest('table')
            .find('.check_row');

          $(elem)
            .on('ifChecked', function() {
              $checkRow.iCheck('check');
            })
            .on('ifUnchecked', function() {
              $checkRow.iCheck('uncheck');
            });
        }
      };
    }
  ])
  // table row check
  .directive('tableCheckRow', [
    '$window',
    '$timeout',
    function($window, $timeout) {
      return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, elem, attr, ngModel) {
          var $this = $(elem);

          scope.$watch(
            function() {
              return ngModel.$modelValue;
            },
            function(newValue) {
              if (newValue) {
                $this.closest('tr').addClass('row_checked');
              } else {
                $this.closest('tr').removeClass('row_checked');
              }
            }
          );
        }
      };
    }
  ])
  // content sidebar
  .directive('contentSidebar', [
    '$rootScope',
    '$document',
    function($rootScope, $document) {
      return {
        restrict: 'A',
        link: function(scope, el, attr) {
          if (!$rootScope.header_double_height) {
            $rootScope.$watch('hide_content_sidebar', function() {
              if ($rootScope.hide_content_sidebar) {
                $('#page_content').css('max-height', $('html').height() - 40);
                $('html').css({
                  paddingRight: scrollbarWidth(),
                  overflow: 'hidden'
                });
              } else {
                $('#page_content').css('max-height', '');
                $('html').css({
                  paddingRight: '',
                  overflow: ''
                });
              }
            });
          }
        }
      };
    }
  ])
  // attach events to document
  .directive('documentEvents', [
    '$rootScope',
    '$window',
    '$timeout',
    'variables',
    'SesionFebos',
    'FebosUtil',
    function($rootScope, $window, $timeout, variables, SesionFebos, FebosUtil) {
      return {
        restrict: 'A',
        link: function(scope, el, attr) {
          var hidePrimarySidebar = function() {
            $rootScope.primarySidebarActive = false;
            $rootScope.primarySidebarOpen = false;
            $rootScope.hide_content_sidebar = false;
            $rootScope.primarySidebarHiding = true;
            $timeout(function() {
              $rootScope.primarySidebarHiding = false;
            }, 280);
          };

          var hideSecondarySidebar = function() {
            $rootScope.secondarySidebarActive = false;
          };

          var hideMainSearch = function() {
            var $header_main = $('#header_main');
            $header_main.children('.header_main_search_form').velocity('transition.slideUpBigOut', {
              duration: 280,
              easing: variables.easing_swiftOut,
              begin: function() {
                $header_main.velocity('reverse');
                $rootScope.mainSearchActive = false;
              },
              complete: function() {
                $header_main
                  .children('.header_main_content')
                  .velocity('transition.slideDownBigIn', {
                    duration: 280,
                    easing: variables.easing_swiftOut,
                    complete: function() {
                      $('.header_main_search_input')
                        .blur()
                        .val('');
                    }
                  });
              }
            });
          };

          // hide components on $document click
          scope.onClick = function($event) {
            // primary sidebar
            if (
              $rootScope.primarySidebarActive &&
              !$($event.target).closest('#sidebar_main').length &&
              !$($event.target).closest('#sSwitch_primary').length &&
              !$rootScope.largeScreen
            ) {
              hidePrimarySidebar();
            }
            // secondary sidebar
            if (
              $rootScope.secondarySidebarActive &&
              !$($event.target).closest('#sidebar_secondary').length &&
              !$($event.target).closest('#sSwitch_secondary').length &&
              !$rootScope.secondarySidebarPersistent
            ) {
              hideSecondarySidebar();
            }
            // main search form
            if (
              $rootScope.mainSearchActive &&
              !$($event.target).closest('.header_main_search_form').length &&
              !$($event.target).closest('#main_search_btn').length
            ) {
              hideMainSearch();
            }
            // style switcher
            if (
              $rootScope.styleSwitcherActive &&
              !$($event.target).closest('#style_switcher').length
            ) {
              $rootScope.styleSwitcherActive = false;
            }
            // sticky notes form
            if ($rootScope.noteFormActive && !$($event.target).closest('#note_form').length) {
              $rootScope.noteFormActive = false;
            }
          };

          // hide components on key press (esc)
          scope.onKeyUp = function($event) {
            // primary sidebar
            if (
              $rootScope.primarySidebarActive &&
              !$rootScope.largeScreen &&
              $event.keyCode == 27
            ) {
              hidePrimarySidebar();
            }
            // secondary sidebar
            if (
              !$rootScope.secondarySidebarPersistent &&
              $rootScope.secondarySidebarActive &&
              $event.keyCode == 27
            ) {
              hideSecondarySidebar();
            }
            // main search form
            if ($rootScope.mainSearchActive && $event.keyCode == 27) {
              hideMainSearch();
            }
            // style switcher
            if ($rootScope.styleSwitcherActive && $event.keyCode == 27) {
              $rootScope.styleSwitcherActive = false;
            }
            // sticky notes form
            if ($rootScope.noteFormActive && $event.keyCode == 27) {
              $rootScope.noteFormActive = false;
            }

            //ACCIONES PARA FEBOS
            if ($event.keyCode == 77 && $event.altKey && FebosUtil.tienePermiso('FEB99')) {
              if ($('[permiso].mostrarPermiso').length > 0) {
                $('[permiso]').removeClass('mostrarPermiso');
              } else {
                $('[permiso]').addClass('mostrarPermiso');
              }
            }
          };
        }
      };
    }
  ])
  // main search show
  .directive('mainSearchShow', [
    '$rootScope',
    '$window',
    'variables',
    '$timeout',
    function($rootScope, $window, variables, $timeout) {
      return {
        restrict: 'E',
        template:
          '<a id="main_search_btn" class="user_action_icon" ng-click="showSearch()"><i class="material-icons md-24 md-light">&#xE8B6;</i></a>',
        replace: true,
        scope: true,
        link: function(scope, el, attr) {
          scope.showSearch = function() {
            $('#header_main')
              .children('.header_main_content')
              .velocity('transition.slideUpBigOut', {
                duration: 280,
                easing: variables.easing_swiftOut,
                begin: function() {
                  $rootScope.mainSearchActive = true;
                },
                complete: function() {
                  $('#header_main')
                    .children('.header_main_search_form')
                    .velocity('transition.slideDownBigIn', {
                      duration: 280,
                      easing: variables.easing_swiftOut,
                      complete: function() {
                        $('.header_main_search_input').focus();
                      }
                    });
                }
              });
          };
        }
      };
    }
  ])
  // main search hide
  .directive('mainSearchHide', [
    '$rootScope',
    '$window',
    'variables',
    function($rootScope, $window, variables) {
      return {
        restrict: 'E',
        template:
          '<i class="md-icon header_main_search_close material-icons" ng-click="hideSearch()">&#xE5CD;</i>',
        replace: true,
        scope: true,
        link: function(scope, el, attr) {
          scope.hideSearch = function() {
            var $header_main = $('#header_main');

            $header_main.children('.header_main_search_form').velocity('transition.slideUpBigOut', {
              duration: 280,
              easing: variables.easing_swiftOut,
              begin: function() {
                $header_main.velocity('reverse');
                $rootScope.mainSearchActive = false;
              },
              complete: function() {
                $header_main
                  .children('.header_main_content')
                  .velocity('transition.slideDownBigIn', {
                    duration: 280,
                    easing: variables.easing_swiftOut,
                    complete: function() {
                      $('.header_main_search_input')
                        .blur()
                        .val('');
                    }
                  });
              }
            });
          };
        }
      };
    }
  ])

  // primary sidebar
  .directive('sidebarPrimary', [
    '$rootScope',
    '$window',
    '$timeout',
    'variables',
    function($rootScope, $window, $timeout, variables) {
      return {
        restrict: 'A',
        scope: 'true',
        link: function(scope, el, attr) {
          var $sidebar_main = $('#sidebar_main');

          scope.submenuToggle = function($event) {
            $event.preventDefault();

            var $this = $($event.currentTarget),
              slideToogle = $this.next('ul').is(':visible') ? 'slideUp' : 'slideDown';

            $this.next('ul').velocity(slideToogle, {
              duration: 400,
              easing: variables.easing_swiftOut,
              begin: function() {
                if (slideToogle == 'slideUp') {
                  $(this)
                    .closest('.submenu_trigger')
                    .removeClass('act_section');
                } else {
                  if ($rootScope.menuAccordionMode) {
                    $this
                      .closest('li')
                      .siblings('.submenu_trigger')
                      .each(function() {
                        $(this)
                          .children('ul')
                          .velocity('slideUp', {
                            duration: 500,
                            easing: variables.easing_swiftOut,
                            begin: function() {
                              $(this)
                                .closest('.submenu_trigger')
                                .removeClass('act_section');
                            }
                          });
                      });
                  }
                  $(this)
                    .closest('.submenu_trigger')
                    .addClass('act_section');
                }
              },
              complete: function() {
                if (slideToogle !== 'slideUp') {
                  var scrollContainer = $sidebar_main.find('.scroll-content').length
                    ? $sidebar_main.find('.scroll-content')
                    : $sidebar_main.find('.scrollbar-inner');
                  $this.closest('.act_section').velocity('scroll', {
                    duration: 500,
                    easing: variables.easing_swiftOut,
                    container: scrollContainer
                  });
                }
              }
            });
          };

          $rootScope.$watch('slimSidebarActive', function(status) {
            if (status) {
              var $body = $('body');
              $sidebar_main
                .mouseenter(function() {
                  $body.removeClass('sidebar_slim_inactive');
                  $body.addClass('sidebar_slim_active');
                })
                .mouseleave(function() {
                  $body.addClass('sidebar_slim_inactive');
                  $body.removeClass('sidebar_slim_active');
                });
            }
          });
        }
      };
    }
  ])
  // toggle primary sidebar
  .directive('sidebarPrimaryToggle', [
    '$rootScope',
    '$window',
    '$timeout',
    function($rootScope, $window, $timeout) {
      return {
        restrict: 'E',
        replace: true,
        scope: true,
        template:
          '<a id="sSwitch_primary" href="#" class="sSwitch sSwitch_left" ng-click="togglePrimarySidebar($event)" ng-hide="miniSidebarActive || slimSidebarActive || topMenuActive"><span class="sSwitchIcon"></span></a>',
        link: function(scope, el, attrs) {
          scope.togglePrimarySidebar = function($event) {
            $event.preventDefault();

            if ($rootScope.primarySidebarActive) {
              $rootScope.primarySidebarHiding = true;
              if ($rootScope.largeScreen) {
                $timeout(function() {
                  $rootScope.primarySidebarHiding = false;
                  $(window).resize();
                }, 290);
              }
            } else {
              if ($rootScope.largeScreen) {
                $timeout(function() {
                  $(window).resize();
                }, 290);
              }
            }

            $rootScope.primarySidebarActive = !$rootScope.primarySidebarActive;

            if (!$rootScope.largeScreen) {
              $rootScope.hide_content_sidebar = $rootScope.primarySidebarActive ? true : false;
            }

            if ($rootScope.primarySidebarOpen) {
              $rootScope.primarySidebarOpen = false;
              $rootScope.primarySidebarActive = false;
            }
            $timeout(function() {
              //$("#sSwitchIcon").css({'height':'2px'});
              if ($('.sidebar_main_active #sidebar_main').length == 0) {
                $('span.sSwitchIcon').css({ height: '2px' });
              } else {
                $('span.sSwitchIcon').css({ height: '0px' });
              }
            }, 350);
          };
        }
      };
    }
  ])
  // secondary sidebar
  .directive('sidebarSecondary', [
    '$rootScope',
    '$timeout',
    'variables',
    function($rootScope, $timeout, variables) {
      return {
        restrict: 'A',
        link: function(scope, el, attrs) {
          $rootScope.sidebar_secondary = true;
          if (attrs.toggleHidden == 'large') {
            $rootScope.secondarySidebarHiddenLarge = true;
          }

          // chat
          var $sidebar_secondary = $(el);
          if ($sidebar_secondary.find('.md-list.chat_users').length) {
            $('.md-list.chat_users').on('click', 'li', function() {
              $('.md-list.chat_users').velocity('transition.slideRightBigOut', {
                duration: 280,
                easing: variables.easing_swiftOut,
                complete: function() {
                  $sidebar_secondary
                    .find('.chat_box_wrapper')
                    .addClass('chat_box_active')
                    .velocity('transition.slideRightBigIn', {
                      duration: 280,
                      easing: variables.easing_swiftOut,
                      begin: function() {
                        $sidebar_secondary.addClass('chat_sidebar');
                      }
                    });
                }
              });
            });

            $sidebar_secondary.find('.chat_sidebar_close').on('click', function() {
              $sidebar_secondary
                .find('.chat_box_wrapper')
                .removeClass('chat_box_active')
                .velocity('transition.slideRightBigOut', {
                  duration: 280,
                  easing: variables.easing_swiftOut,
                  complete: function() {
                    $sidebar_secondary.removeClass('chat_sidebar');
                    $('.md-list.chat_users').velocity('transition.slideRightBigIn', {
                      duration: 280,
                      easing: variables.easing_swiftOut
                    });
                  }
                });
            });

            if ($sidebar_secondary.find('.uk-tab').length) {
              $sidebar_secondary
                .find('.uk-tab')
                .on('change.uk.tab', function(event, active_item, previous_item) {
                  if (
                    $(active_item).hasClass('chat_sidebar_tab') &&
                    $sidebar_secondary.find('.chat_box_wrapper').hasClass('chat_box_active')
                  ) {
                    $sidebar_secondary.addClass('chat_sidebar');
                  } else {
                    $sidebar_secondary.removeClass('chat_sidebar');
                  }
                });
            }
          }
        }
      };
    }
  ])
  // toggle secondary sidebar
  .directive('sidebarSecondaryToggle', [
    '$rootScope',
    '$window',
    '$timeout',
    function($rootScope, $window, $timeout) {
      return {
        restrict: 'E',
        replace: true,
        template:
          '<a href="#" id="sSwitch_secondary" class="sSwitch sSwitch_right" ng-show="sidebar_secondary" ng-click="toggleSecondarySidebar($event)"><span class="sSwitchIcon"></span></a>',
        link: function(scope, el, attrs) {
          scope.toggleSecondarySidebar = function($event) {
            $event.preventDefault();
            $rootScope.secondarySidebarActive = !$rootScope.secondarySidebarActive;
          };
        }
      };
    }
  ])
  // activate card fullscreen
  .directive('cardFullscreenActivate', [
    '$rootScope',
    'variables',
    function($rootScope, variables) {
      return {
        restrict: 'E',
        replace: true,
        scope: true,
        template:
          '<i class="md-icon material-icons md-card-fullscreen-activate" ng-click="cardFullscreenActivate($event)">&#xE5D0;</i>',
        link: function(scope, el, attrs) {
          scope.cardFullscreenActivate = function($event) {
            $event.preventDefault();

            var $thisCard = $(el).closest('.md-card'),
              mdCardToolbarFixed = $thisCard.hasClass('toolbar-fixed'),
              mdCard_h = $thisCard.height(),
              mdCard_w = $thisCard.width(),
              body_scroll_top = $('body').scrollTop(),
              mdCard_offset = $thisCard.offset();

            // create placeholder for card
            $thisCard.after(
              '<div class="md-card-placeholder" style="width:' +
                mdCard_w +
                'px;height:' +
                mdCard_h +
                'px;"/>'
            );
            // add overflow hidden to #page_content (fix for ios)
            //$body.addClass('md-card-fullscreen-active');
            // add width/height to card (preserve original size)
            $thisCard
              .addClass('md-card-fullscreen')
              .css({
                width: mdCard_w,
                height: mdCard_h,
                left: mdCard_offset.left,
                top: mdCard_offset.top - body_scroll_top
              })
              // animate card to top/left position
              .velocity(
                {
                  left: 0,
                  top: 0
                },
                {
                  duration: 400,
                  easing: variables.easing_swiftOut,
                  begin: function(elements) {
                    $rootScope.card_fullscreen = true;
                    $rootScope.hide_content_sidebar = true;
                    // add back button
                    //$thisCard.find('.md-card-toolbar').prepend('<span class="md-icon md-card-fullscreen-deactivate material-icons uk-float-left">&#xE5C4;</span>');
                    //altair_page_content.hide_content_sidebar();
                  }
                }
              )
              // resize card to full width/height
              .velocity(
                {
                  height: '100%',
                  width: '100%'
                },
                {
                  duration: 400,
                  easing: variables.easing_swiftOut,
                  complete: function(elements) {
                    // activate onResize callback for some js plugins
                    //$(window).resize();
                    // show fullscreen content
                    $thisCard
                      .find('.md-card-fullscreen-content')
                      .velocity('transition.slideUpBigIn', {
                        duration: 400,
                        easing: variables.easing_swiftOut,
                        complete: function(elements) {
                          // activate onResize callback for some js plugins
                          $(window).resize();
                        }
                      });
                    if (mdCardToolbarFixed) {
                      $thisCard.addClass('mdToolbar_fixed');
                    }
                  }
                }
              );
          };
        }
      };
    }
  ])
  // deactivate card fullscreen
  .directive('cardFullscreenDeactivate', [
    '$rootScope',
    '$window',
    'variables',
    function($rootScope, $window, variables) {
      return {
        restrict: 'E',
        replace: true,
        template:
          '<span class="md-icon md-card-fullscreen-deactivate material-icons uk-float-left" ng-show="card_fullscreen" ng-click="cardFullscreenDeactivate($event)">&#xE5C4;</span>',
        link: function(scope, el, attrs) {
          scope.cardFullscreenDeactivate = function($event) {
            $event.preventDefault();

            // get card placeholder width/height and offset
            var $thisPlaceholderCard = $('.md-card-placeholder'),
              mdPlaceholderCard_h = $thisPlaceholderCard.height(),
              mdPlaceholderCard_w = $thisPlaceholderCard.width(),
              body_scroll_top = $('body').scrollTop(),
              mdPlaceholderCard_offset_top = $thisPlaceholderCard.offset().top - body_scroll_top,
              mdPlaceholderCard_offset_left = $thisPlaceholderCard.offset().left,
              $thisCard = $('.md-card-fullscreen'),
              mdCardToolbarFixed = $thisCard.hasClass('toolbar-fixed');

            $thisCard
              // resize card to original size
              .velocity(
                {
                  height: mdPlaceholderCard_h,
                  width: mdPlaceholderCard_w
                },
                {
                  duration: 400,
                  easing: variables.easing_swiftOut,
                  begin: function(elements) {
                    // hide fullscreen content
                    $thisCard
                      .find('.md-card-fullscreen-content')
                      .velocity('transition.slideDownOut', {
                        duration: 280,
                        easing: variables.easing_swiftOut
                      });
                    $rootScope.card_fullscreen = false;
                    if (mdCardToolbarFixed) {
                      $thisCard.removeClass('mdToolbar_fixed');
                    }
                  },
                  complete: function(elements) {
                    $rootScope.hide_content_sidebar = false;
                  }
                }
              )
              // move card to original position
              .velocity(
                {
                  left: mdPlaceholderCard_offset_left,
                  top: mdPlaceholderCard_offset_top
                },
                {
                  duration: 400,
                  easing: variables.easing_swiftOut,
                  complete: function(elements) {
                    // remove some styles added by velocity.js
                    $thisCard.removeClass('md-card-fullscreen').css({
                      width: '',
                      height: '',
                      left: '',
                      top: ''
                    });
                    // remove card placeholder
                    $thisPlaceholderCard.remove();
                    $(window).resize();
                  }
                }
              );
          };
        }
      };
    }
  ])
  // fullscren on card click
  .directive('cardFullscreenWhole', [
    '$rootScope',
    'variables',
    function($rootScope, variables) {
      return {
        restrict: 'A',
        replace: true,
        scope: true,
        link: function(scope, el, attrs) {
          $(el).on('click', function(e) {
            e.preventDefault();
            var $this = $(this);
            if (!$this.hasClass('md-card-fullscreen')) {
              scope.cardFSActivate();
            }
          });

          $(el).on('click', '.cardFSDeactivate', function(e) {
            e.preventDefault();
            var $this = $(this);
            if (!$this.hasClass('md-card-fullscreen')) {
              scope.cardFSDeactivate();
            }
          });

          scope.cardFSActivate = function() {
            var $thisCard = $(el),
              mdCardToolbarFixed = $thisCard.hasClass('toolbar-fixed'),
              mdCard_h = $thisCard.height(),
              mdCard_w = $thisCard.width(),
              body_scroll_top = $('body').scrollTop(),
              mdCard_offset = $thisCard.offset();

            // create placeholder for card
            $thisCard.after(
              '<div class="md-card-placeholder" style="width:' +
                mdCard_w +
                'px;height:' +
                mdCard_h +
                'px;"/>'
            );
            // add width/height to card (preserve original size)
            $thisCard
              .addClass('md-card-fullscreen')
              .css({
                width: mdCard_w,
                height: mdCard_h
              })
              // animate card to top/left position
              .velocity(
                {
                  left: 0,
                  top: 0
                },
                {
                  duration: 400,
                  easing: variables.easing_swiftOut,
                  begin: function(elements) {
                    $rootScope.card_fullscreen = true;
                    $rootScope.hide_content_sidebar = true;
                    // add back button
                    $thisCard.append(
                      '<span class="md-icon material-icons uk-position-top-right cardFSDeactivate" style="margin:10px 10px 0 0">&#xE5CD;</span>'
                    );
                  }
                }
              )
              // resize card to full width/height
              .velocity(
                {
                  height: '100%',
                  width: '100%'
                },
                {
                  duration: 400,
                  easing: variables.easing_swiftOut,
                  complete: function(elements) {
                    // show fullscreen content
                    $thisCard
                      .find('.md-card-fullscreen-content')
                      .velocity('transition.slideUpBigIn', {
                        duration: 400,
                        easing: variables.easing_swiftOut,
                        complete: function(elements) {
                          // activate onResize callback for some js plugins
                          $(window).resize();
                        }
                      });
                    if (mdCardToolbarFixed) {
                      $thisCard.addClass('mdToolbar_fixed');
                    }
                  }
                }
              );
          };
          scope.cardFSDeactivate = function() {
            // get card placeholder width/height and offset
            var $thisPlaceholderCard = $('.md-card-placeholder'),
              mdPlaceholderCard_h = $thisPlaceholderCard.height(),
              mdPlaceholderCard_w = $thisPlaceholderCard.width(),
              mdPlaceholderCard_offset_top = $thisPlaceholderCard.offset().top,
              mdPlaceholderCard_offset_left = $thisPlaceholderCard.offset().left,
              $thisCard = $('.md-card-fullscreen'),
              mdCardToolbarFixed = $thisCard.hasClass('toolbar-fixed');

            $thisCard
              // resize card to original size
              .velocity(
                {
                  height: mdPlaceholderCard_h,
                  width: mdPlaceholderCard_w
                },
                {
                  duration: 400,
                  easing: variables.easing_swiftOut,
                  begin: function(elements) {
                    // hide fullscreen content
                    $thisCard
                      .find('.md-card-fullscreen-content')
                      .velocity('transition.slideDownOut', {
                        duration: 280,
                        easing: variables.easing_swiftOut
                      });
                    $rootScope.card_fullscreen = false;
                    if (mdCardToolbarFixed) {
                      $thisCard.removeClass('mdToolbar_fixed');
                    }
                    $thisCard.find('.cardFSDeactivate').remove();
                  },
                  complete: function(elements) {
                    $rootScope.hide_content_sidebar = false;
                  }
                }
              )
              // move card to original position
              .velocity(
                {
                  left: mdPlaceholderCard_offset_left,
                  top: mdPlaceholderCard_offset_top
                },
                {
                  duration: 400,
                  easing: variables.easing_swiftOut,
                  complete: function(elements) {
                    // remove some styles added by velocity.js
                    $thisCard.removeClass('md-card-fullscreen').css({
                      width: '',
                      height: '',
                      left: '',
                      top: ''
                    });
                    // remove card placeholder
                    $thisPlaceholderCard.remove();
                    $(window).resize();
                  }
                }
              );
          };
        }
      };
    }
  ])
  // card close
  .directive('cardClose', [
    'utils',
    function(utils) {
      return {
        restrict: 'E',
        replace: true,
        scope: true,
        template:
          '<i class="md-icon material-icons md-card-close" ng-click="cardClose($event)">&#xE14C;</i>',
        link: function(scope, el, attrs) {
          scope.cardClose = function($event) {
            $event.preventDefault();

            var $this = $(el),
              thisCard = $this.closest('.md-card'),
              removeCard = function() {
                $(thisCard).remove();
                $(window).resize();
              };

            utils.card_show_hide(thisCard, undefined, removeCard);
          };
        }
      };
    }
  ])
  // card toggle
  .directive('cardToggle', [
    'variables',
    function(variables) {
      return {
        restrict: 'E',
        replace: true,
        scope: true,
        template:
          '<i class="md-icon material-icons md-card-toggle" ng-click="cardToggle($event)">&#xE316;</i>',
        link: function(scope, el, attrs) {
          scope.cardToggle = function($event) {
            $event.preventDefault();

            var $this = $(el),
              thisCard = $this.closest('.md-card');

            $(thisCard)
              .toggleClass('md-card-collapsed')
              .children('.md-card-content')
              .slideToggle('280', variables.bez_easing_swiftOut);

            $this.velocity(
              {
                scale: 0,
                opacity: 0.2
              },
              {
                duration: 280,
                easing: variables.easing_swiftOut,
                complete: function() {
                  $(thisCard).hasClass('md-card-collapsed')
                    ? $this.html('&#xE313;')
                    : $this.html('&#xE316;');
                  $this.velocity('reverse');
                  $(window).resize();
                }
              }
            );
          };

          // hide card content on page load
          var thisCard = $(el).closest('.md-card');
          if (thisCard.hasClass('md-card-collapsed')) {
            var $this_toggle = thisCard.find('.md-card-toggle');

            $this_toggle.html('&#xE313;');
            thisCard.children('.md-card-content').hide();
          }
        }
      };
    }
  ]) // card toggle
  .directive('cardToggleBoton', [
    'variables',
    function(variables) {
      return {
        restrict: 'E',
        replace: true,
        scope: true,
        template:
          '<a class="md-btn md-btn-wave"><i class="material-icons" ng-click="cardToggle($event)">&#xE316;</i></a>',
        link: function(scope, el, attrs) {
          scope.cardToggle = function($event) {
            $event.preventDefault();

            var $this = $(el),
              thisCard = $this.closest('.md-card');

            $(thisCard)
              .toggleClass('md-card-collapsed')
              .children('.md-card-content')
              .slideToggle('280', variables.bez_easing_swiftOut);

            $this.velocity(
              {
                scale: 0,
                opacity: 0.2
              },
              {
                duration: 280,
                easing: variables.easing_swiftOut,
                complete: function() {
                  $(thisCard).hasClass('md-card-collapsed')
                    ? $this.html('&#xE313;')
                    : $this.html('&#xE316;');
                  $this.velocity('reverse');
                  $(window).resize();
                }
              }
            );
          };

          // hide card content on page load
          var thisCard = $(el).closest('.md-card');
          if (thisCard.hasClass('md-card-collapsed')) {
            var $this_toggle = thisCard.find('.md-card-toggle');

            $this_toggle.html('&#xE313;');
            thisCard.children('.md-card-content').hide();
          }
        }
      };
    }
  ])
  // card overlay toggle
  .directive('cardOverlayToggle', [
    function() {
      return {
        restrict: 'E',
        template: '<i class="md-icon material-icons" ng-click="toggleOverlay($event)">&#xE5D4;</i>',
        replace: true,
        scope: true,
        link: function(scope, el, attrs) {
          if (el.closest('.md-card').hasClass('md-card-overlay-active')) {
            el.html('&#xE5CD;');
          }

          scope.toggleOverlay = function($event) {
            $event.preventDefault();

            if (!el.closest('.md-card').hasClass('md-card-overlay-active')) {
              el.html('&#xE5CD;')
                .closest('.md-card')
                .addClass('md-card-overlay-active');
            } else {
              el.html('&#xE5D4;')
                .closest('.md-card')
                .removeClass('md-card-overlay-active');
            }
          };
        }
      };
    }
  ])
  // card toolbar progress
  .directive('cardProgress', [
    '$timeout',
    function($timeout) {
      return {
        restrict: 'A',
        scope: true,
        link: function(scope, el, attrs) {
          var $this = $(el).children('.md-card-toolbar'),
            bg_percent = parseInt(attrs.cardProgress);

          function updateCard(percent) {
            var bg_color_default = $this.attr('card-bg-default');

            var bg_color = !bg_color_default ? $this.css('backgroundColor') : bg_color_default;
            if (!bg_color_default) {
              $this.attr('card-bg-default', bg_color);
            }

            $this
              .css({
                background:
                  '-moz-linear-gradient(left, ' +
                  bg_color +
                  ' ' +
                  percent +
                  '%, #fff ' +
                  percent +
                  '%)'
              })
              .css({
                background:
                  '-webkit-linear-gradient(left, ' +
                  bg_color +
                  ' ' +
                  percent +
                  '%, #fff ' +
                  percent +
                  '%)'
              })
              .css({
                background:
                  'linear-gradient(to right,  ' +
                  bg_color +
                  ' ' +
                  percent +
                  '%, #fff ' +
                  percent +
                  '%)'
              });

            scope.cardPercentage = percent;
          }

          updateCard(bg_percent);

          scope.$watch(
            function() {
              return $(el).attr('card-progress');
            },
            function(newValue) {
              updateCard(newValue);
            }
          );
        }
      };
    }
  ])
  // custom scrollbar
  .directive('customScrollbar', [
    '$rootScope',
    '$timeout',
    function($rootScope, $timeout) {
      return {
        restrict: 'A',
        scope: true,
        link: function(scope, el, attrs) {
          // check if mini sidebar is enabled
          if (attrs['id'] == 'sidebar_main' && $rootScope.miniSidebarActive) {
            return;
          }

          $(el)
            .addClass('uk-height-1-1')
            .wrapInner("<div class='scrollbar-inner'></div>");

          if (Modernizr.touch) {
            $(el)
              .children('.scrollbar-inner')
              .addClass('touchscroll');
          } else {
            $timeout(function() {
              $(el)
                .children('.scrollbar-inner')
                .scrollbar({
                  //disableBodyScroll: true,
                  scrollx: false,
                  duration: 100
                });
            });
          }
        }
      };
    }
  ])
  // material design inputs
  .directive('mdInput', [
    '$timeout',
    function($timeout) {
      return {
        restrict: 'A',
        scope: {
          ngModel: '='
        },
        controller: function($scope, $element) {
          var $elem = $($element);
          $scope.updateInput = function() {
            // clear wrapper classes
            $elem
              .closest('.md-input-wrapper')
              .removeClass(
                'md-input-wrapper-danger md-input-wrapper-success md-input-wrapper-disabled'
              );

            if ($elem.hasClass('md-input-danger')) {
              $elem.closest('.md-input-wrapper').addClass('md-input-wrapper-danger');
            }
            if ($elem.hasClass('md-input-success')) {
              $elem.closest('.md-input-wrapper').addClass('md-input-wrapper-success');
            }
            if ($elem.prop('disabled')) {
              $elem.closest('.md-input-wrapper').addClass('md-input-wrapper-disabled');
            }
            if ($elem.hasClass('label-fixed')) {
              $elem.closest('.md-input-wrapper').addClass('md-input-filled');
            }
            if ($elem.val() != '') {
              $elem.closest('.md-input-wrapper').addClass('md-input-filled');
            }
          };
        },
        link: function(scope, elem, attrs) {
          var $elem = $(elem);

          $timeout(function() {
            if (!$elem.hasClass('md-input-processed')) {
              var extraClass = '';
              if ($elem.is('[class*="uk-form-width-"]')) {
                var elClasses = $elem.attr('class').split(' ');
                for (var i = 0; i < elClasses.length; i++) {
                  var classPart = elClasses[i].substr(0, 14);
                  if (classPart == 'uk-form-width-') {
                    var extraClass = elClasses[i];
                  }
                }
              }

              if ($elem.prev('label').length) {
                $elem
                  .prev('label')
                  .andSelf()
                  .wrapAll('<div class="md-input-wrapper"/>');
              } else if ($elem.siblings('[data-uk-form-password]').length) {
                $elem
                  .siblings('[data-uk-form-password]')
                  .andSelf()
                  .wrapAll('<div class="md-input-wrapper"/>');
              } else {
                $elem.wrap('<div class="md-input-wrapper"/>');
              }
              $elem
                .addClass('md-input-processed')
                .closest('.md-input-wrapper')
                .append('<span class="md-input-bar ' + extraClass + '"/>');
            }

            scope.updateInput();
          });

          scope.$watch(
            function() {
              return $elem.attr('class');
            },
            function(newValue, oldValue) {
              if (newValue != oldValue) {
                scope.updateInput();
              }
            }
          );

          scope.$watch(
            function() {
              return $elem.val();
            },
            function(newValue, oldValue) {
              if (!$elem.is(':focus') && newValue != oldValue) {
                scope.updateInput();
              }
            }
          );

          $elem
            .on('focus', function() {
              $elem.closest('.md-input-wrapper').addClass('md-input-focus');
            })
            .on('blur', function() {
              $timeout(function() {
                $elem.closest('.md-input-wrapper').removeClass('md-input-focus');
                if ($elem.val() == '') {
                  $elem.closest('.md-input-wrapper').removeClass('md-input-filled');
                } else {
                  $elem.closest('.md-input-wrapper').addClass('md-input-filled');
                }
              }, 100);
            });
        }
      };
    }
  ])
  // material design fab speed dial
  .directive('mdFabSpeedDial', [
    'variables',
    '$timeout',
    function(variables, $timeout) {
      return {
        restrict: 'A',
        scope: true,
        link: function(scope, elem, attrs) {
          var $elem = $(elem),
            $wrapper = $(elem).closest('.md-fab-speed-dial,.md-fab-speed-dial-horizontal');

          function activateFAB(obj) {
            obj.closest('.md-fab-wrapper').addClass('md-fab-active');
            obj.velocity(
              {
                scale: 0
              },
              {
                duration: 140,
                easing: variables.easing_swiftOut,
                complete: function() {
                  obj
                    .velocity(
                      { scale: 1 },
                      {
                        duration: 140,
                        easing: variables.easing_swiftOut
                      }
                    )
                    .children()
                    .toggle();
                }
              }
            );
          }

          function deactivateFAB(obj) {
            obj.closest('.md-fab-wrapper').removeClass('md-fab-active');
            obj.velocity(
              {
                scale: 0
              },
              {
                duration: 140,
                easing: variables.easing_swiftOut,
                complete: function() {
                  obj
                    .velocity(
                      { scale: 1 },
                      {
                        duration: 140,
                        easing: variables.easing_swiftOut
                      }
                    )
                    .children()
                    .toggle();
                }
              }
            );
          }

          $(elem).append(
            '<i class="material-icons md-fab-action-close" style="display:none">&#xE5CD;</i>'
          );

          if ($wrapper.is('[data-fab-hover]')) {
            var deactiveateFabTimeout;
            $wrapper
              .on('mouseenter', function() {
                $wrapper.addClass('md-fab-over');
                $timeout.cancel(deactiveateFabTimeout);
                setTimeout(function() {
                  activateFAB($elem);
                }, 100);
              })
              .on('mouseleave', function() {
                deactivateFAB($elem);
                deactiveateFabTimeout = $timeout(function() {
                  $wrapper.removeClass('md-fab-over');
                }, 500);
              });
          } else {
            $elem
              .on('click', function() {
                if (!$elem.closest('.md-fab-wrapper').hasClass('md-fab-active')) {
                  activateFAB($elem);
                } else {
                  deactivateFAB($elem);
                }
              })
              .closest('.md-fab-wrapper')
              .find('.md-fab-small')
              .on('click', function() {
                deactivateFAB($elem);
              });
          }
          /*$(elem)
                        .append('<i class="material-icons md-fab-action-close" style="display:none">&#xE5CD;</i>')
                        .on('click',function(e) {
                            e.preventDefault();

                            var $this = $(this),
                                $this_wrapper = $this.closest('.md-fab-wrapper');

                            if(!$this_wrapper.hasClass('md-fab-active')) {
                                $this_wrapper.addClass('md-fab-active');
                            } else {
                                $this_wrapper.removeClass('md-fab-active');
                            }

                            $this.velocity({
                                scale: 0
                            },{
                                duration: 140,
                                easing: variables.easing_swiftOut,
                                complete: function() {
                                    $this.children().toggle();
                                    $this.velocity({
                                        scale: 1
                                    },{
                                        duration: 140,
                                        easing: variables.easing_swiftOut
                                    })
                                }
                            })
                        })
                        .closest('.md-fab-wrapper').find('.md-fab-small')
                        .on('click',function() {
                            $(elem).trigger('click');
                        });*/
        }
      };
    }
  ])
  // material design fab toolbar
  .directive('mdFabToolbar', [
    'variables',
    '$document',
    function(variables, $document) {
      return {
        restrict: 'A',
        scope: true,
        link: function(scope, elem, attrs) {
          var openinit = attrs['open'];

          var $fab_toolbar = $(elem);

          var open = function() {
            var toolbarItems = $fab_toolbar.children('.md-fab-toolbar-actions').children().length;

            $fab_toolbar.addClass('md-fab-animated');

            var FAB_padding = !$fab_toolbar.hasClass('md-fab-small') ? 16 : 24,
              FAB_size = !$fab_toolbar.hasClass('md-fab-small') ? 64 : 44;

            setTimeout(function() {
              $fab_toolbar.width(toolbarItems * FAB_size + FAB_padding);
            }, 140);

            setTimeout(function() {
              $fab_toolbar.addClass('md-fab-active');
            }, 420);
          };
          $fab_toolbar.children('i').on('click', function(e) {
            e.preventDefault();
            open();
          });
          if (openinit) {
          } else {
            $($document).on('click scroll', function(e) {
              if ($fab_toolbar.hasClass('md-fab-active')) {
                if (!$(e.target).closest($fab_toolbar).length) {
                  $fab_toolbar.css('width', '').removeClass('md-fab-active');

                  setTimeout(function() {
                    $fab_toolbar.removeClass('md-fab-animated');
                  }, 140);
                }
              }
            });
          }
          if (openinit) {
            setTimeout(function() {
              open();
              console.log('OPENED element', $fab_toolbar);
            }, 420);
          }
        }
      };
    }
  ])
  // material design fab sheet
  .directive('mdFabSheet', [
    'variables',
    '$document',
    function(variables, $document) {
      return {
        restrict: 'A',
        scope: true,
        link: function(scope, elem, attrs) {
          var $fab_sheet = $(elem);

          $fab_sheet.children('i').on('click', function(e) {
            e.preventDefault();

            var sheetItems = $fab_sheet.children('.md-fab-sheet-actions').children('a').length;

            $fab_sheet.addClass('md-fab-animated');

            setTimeout(function() {
              $fab_sheet.width('240px').height(sheetItems * 40 + 8);
            }, 140);

            setTimeout(function() {
              $fab_sheet.addClass('md-fab-active');
            }, 280);
          });

          $($document).on('click scroll', function(e) {
            if ($fab_sheet.hasClass('md-fab-active')) {
              if (!$(e.target).closest($fab_sheet).length) {
                $fab_sheet
                  .css({
                    height: '',
                    width: ''
                  })
                  .removeClass('md-fab-active');

                setTimeout(function() {
                  $fab_sheet.removeClass('md-fab-animated');
                }, 140);
              }
            }
          });
        }
      };
    }
  ])
  // hierarchical show
  .directive('hierarchicalShow', [
    '$timeout',
    '$rootScope',
    function($timeout, $rootScope) {
      return {
        restrict: 'A',
        scope: true,
        link: function(scope, elem, attrs) {
          var parent_el = $(elem),
            baseDelay = 60;

          var add_animation = function(children, length) {
            children
              .each(function(index) {
                $(this).css({
                  '-webkit-animation-delay': index * baseDelay + 'ms',
                  'animation-delay': index * baseDelay + 'ms'
                });
              })
              .end()
              .waypoint({
                element: elem[0],
                handler: function() {
                  parent_el.addClass('hierarchical_show_inView');
                  setTimeout(function() {
                    parent_el
                      .removeClass('hierarchical_show hierarchical_show_inView fast_animation')
                      .children()
                      .css({
                        '-webkit-animation-delay': '',
                        'animation-delay': ''
                      });
                  }, length * baseDelay + 1200);
                  this.destroy();
                },
                context: window,
                offset: '90%'
              });
          };

          $rootScope.$watch('pageLoaded', function() {
            if ($rootScope.pageLoaded) {
              var children = parent_el.children(),
                children_length = children.length;

              $timeout(function() {
                add_animation(children, children_length);
              }, 560);
            }
          });

          scope.$watch(
            function() {
              return parent_el.hasClass('hierarchical_show');
            },
            function(newValue) {
              if ($rootScope.pageLoaded && newValue) {
                var children = parent_el.children(),
                  children_length = children.length;
                add_animation(children, children_length);
              }
            }
          );
        }
      };
    }
  ])
  // hierarchical slide in
  .directive('hierarchicalSlide', [
    '$timeout',
    '$rootScope',
    function($timeout, $rootScope) {
      return {
        restrict: 'A',
        scope: true,
        link: function(scope, elem, attrs) {
          var $this = $(elem),
            baseDelay = 100;

          var add_animation = function(children, context, childrenLength) {
            children.each(function(index) {
              $(this).css({
                '-webkit-animation-delay': index * baseDelay + 'ms',
                'animation-delay': index * baseDelay + 'ms'
              });
            });
            $this.waypoint({
              handler: function() {
                $this.addClass('hierarchical_slide_inView');
                $timeout(function() {
                  $this.removeClass('hierarchical_slide hierarchical_slide_inView');
                  children.css({
                    '-webkit-animation-delay': '',
                    'animation-delay': ''
                  });
                }, childrenLength * baseDelay + 1200);
                this.destroy();
              },
              context: context[0],
              offset: '90%'
            });
          };

          $rootScope.$watch('pageLoaded', function() {
            if ($rootScope.pageLoaded) {
              var thisChildren = attrs['slideChildren']
                  ? $this.children(attrs['slideChildren'])
                  : $this.children(),
                thisContext = attrs['slideContext']
                  ? $this.closest(attrs['slideContext'])
                  : 'window',
                thisChildrenLength = thisChildren.length;

              if (thisChildrenLength >= 1) {
                $timeout(function() {
                  add_animation(thisChildren, thisContext, thisChildrenLength);
                }, 560);
              }
            }
          });

          scope.$watch(
            function() {
              return $this.hasClass('hierarchical_slide');
            },
            function(newValue) {
              if ($rootScope.pageLoaded && newValue) {
                var thisChildren = attrs['slideChildren']
                    ? $this.children(attrs['slideChildren'])
                    : $this.children(),
                  thisContext = attrs['slideContext']
                    ? $this.closest(attrs['slideContext'])
                    : 'window',
                  thisChildrenLength = thisChildren.length;

                add_animation(thisChildren, thisContext, thisChildrenLength);
              }
            }
          );
        }
      };
    }
  ])
  // preloaders
  .directive('mdPreloader', [
    function() {
      return {
        restrict: 'E',
        scope: {
          width: '=?',
          height: '=?',
          strokeWidth: '=?',
          style: '@?'
        },
        template:
          '<div class="md-preloader{{style}}"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" ng-attr-height="{{ height }}" ng-attr-width="{{ width }}" viewbox="0 0 75 75"><circle cx="37.5" cy="37.5" r="33.5" ng-attr-stroke-width="{{ strokeWidth }}"/></svg></div>',
        link: function(scope, elem, attr) {
          scope.width = scope.width ? scope.width : 48;
          scope.height = scope.height ? scope.height : 48;
          scope.strokeWidth = scope.strokeWidth ? scope.strokeWidth : 4;

          attr.$observe('warning', function() {
            scope.style = ' md-preloader-warning';
          });

          attr.$observe('success', function() {
            scope.style = ' md-preloader-success';
          });

          attr.$observe('danger', function() {
            scope.style = ' md-preloader-danger';
          });
        }
      };
    }
  ])
  .directive('preloader', [
    '$rootScope',
    'utils',
    function($rootScope, utils) {
      return {
        restrict: 'E',
        scope: {
          width: '=?',
          height: '=?',
          style: '@?'
        },
        template:
          '<img src="assets/img/spinners/{{style}}{{imgDensity}}.gif" alt="" ng-attr-width="{{width}}" ng-attr-height="{{height}}">',
        link: function(scope, elem, attrs) {
          scope.width = scope.width ? scope.width : 32;
          scope.height = scope.height ? scope.height : 32;
          scope.style = scope.style ? scope.style : 'spinner';
          scope.imgDensity = utils.isHighDensity() ? '@2x' : '';

          attrs.$observe('warning', function() {
            scope.style = 'spinner_warning';
          });

          attrs.$observe('success', function() {
            scope.style = 'spinner_success';
          });

          attrs.$observe('danger', function() {
            scope.style = 'spinner_danger';
          });

          attrs.$observe('small', function() {
            scope.style = 'spinner_small';
          });

          attrs.$observe('medium', function() {
            scope.style = 'spinner_medium';
          });

          attrs.$observe('large', function() {
            scope.style = 'spinner_large';
          });
        }
      };
    }
  ])
  // uikit components
  .directive('ukHtmlEditor', [
    '$timeout',
    function($timeout) {
      return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
          $timeout(function() {
            UIkit.htmleditor(elem[0], {
              toolbar: '',
              height: '240'
            });
          });
        }
      };
    }
  ])
  .directive('ukNotification', [
    '$window',
    function($window) {
      return {
        restrict: 'A',
        scope: {
          message: '@',
          status: '@?',
          timeout: '@?',
          group: '@?',
          position: '@?',
          callback: '&?'
        },
        link: function(scope, elem, attrs) {
          var w = angular.element($window),
            $element = $(elem);

          scope.showNotify = function() {
            var thisNotify = UIkit.notify({
              message: scope.message,
              status: scope.status ? scope.status : '',
              timeout: scope.timeout ? scope.timeout : 5000,
              group: scope.group ? scope.group : '',
              pos: scope.position ? scope.position : 'top-center',
              onClose: function() {
                $('body')
                  .find('.md-fab-wrapper')
                  .css('margin-bottom', '');
                clearTimeout(thisNotify.timeout);

                if (scope.callback) {
                  if (angular.isFunction(scope.callback())) {
                    scope.$apply(scope.callback());
                  } else {
                    console.log('Callback is not a function');
                  }
                }
              }
            });
            if (
              (w.width() < 768 &&
                (scope.position == 'bottom-right' ||
                  scope.position == 'bottom-left' ||
                  scope.position == 'bottom-center')) ||
              scope.position == 'bottom-right'
            ) {
              var thisNotify_height = $(thisNotify.element).outerHeight(),
                spacer = w.width() < 768 ? -6 : 8;
              $('body')
                .find('.md-fab-wrapper')
                .css('margin-bottom', thisNotify_height + spacer);
            }
          };

          $element.on('click', function() {
            if ($('body').find('.uk-notify-message').length) {
              $('body')
                .find('.uk-notify-message')
                .click();
              setTimeout(function() {
                scope.showNotify();
              }, 450);
            } else {
              scope.showNotify();
            }
          });
        }
      };
    }
  ])
  .directive('accordionSectionOpen', [
    '$timeout',
    function($timeout) {
      return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
          var sectOpen = attrs.accordionSectionOpen.toString().split(','),
            $elem = $(elem);
          $timeout(function() {
            var data = $(elem).data();
            // close all accordion sections
            $elem.children('.uk-accordion-title.uk-active').each(function() {
              $(this).trigger('click');
            });
            if (!data.accordion.options.collapse && sectOpen.length > 1) {
              // open multiple sections
              for (var $i = 0; $i <= sectOpen.length; $i++) {
                $elem
                  .children('.uk-accordion-title')
                  .eq(sectOpen[$i] - 1)
                  .trigger('click');
              }
            } else {
              $elem
                .children('.uk-accordion-title')
                .eq(sectOpen[0] - 1)
                .trigger('click');
            }
          }, 220);
        }
      };
    }
  ])
  .directive('pageAside', [
    '$timeout',
    '$window',
    function($timeout, $window) {
      return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
          var w = angular.element($window);

          function calculateHeight() {
            var viewportHeight = w.height(),
              asideTop = $(elem).offset().top;

            $(elem).height(viewportHeight - asideTop);
          }

          calculateHeight();
          w.on('debouncedresize', function() {
            calculateHeight();
          });
        }
      };
    }
  ])
  .directive('pageAsideToggle', [
    '$timeout',
    '$window',
    function($timeout, $window) {
      return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
          $(elem).on('click', function() {
            $('body').toggleClass('page_aside_collapsed');
          });
        }
      };
    }
  ])
  .directive('pageOverflow', [
    '$timeout',
    '$window',
    function($timeout, $window) {
      return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
          var w = angular.element($window);

          function calculateHeight() {
            var viewportHeight = w.height(),
              overflowTop = $(elem).offset().top,
              height = viewportHeight - overflowTop;

            if ($(elem).children('.uk-overflow-container').length) {
              $(elem)
                .children('.uk-overflow-container')
                .height(height);
            } else {
              $(elem).height(height);
            }
          }

          calculateHeight();
          w.on('debouncedresize', function() {
            calculateHeight();
          });
        }
      };
    }
  ])
  .directive('altairUiSelect', [
    '$timeout',
    function($timeout) {
      return {
        require: 'uiSelect',
        link: function(scope, element, attrs, $select) {
          $timeout(function() {
            scope.dropdown = $($select.$element).find('.ui-select-dropdown');
          }, 1000);

          scope.onOpenClose = function(isOpen) {
            var $dropdown = scope.dropdown;
            if (isOpen) {
              $dropdown.hide().velocity('slideDown', {
                duration: 200,
                easing: [0.4, 0, 0.2, 1]
              });
            } else {
              scope.dropdown.show().velocity('slideUp', {
                duration: 200,
                easing: [0.4, 0, 0.2, 1]
              });
            }
          };
        }
      };
    }
  ])
  .directive('ngEnter', function() {
    return function(scope, element, attrs) {
      element.bind('keydown keypress', function(event) {
        if (event.which === 13) {
          scope.$apply(function() {
            scope.$eval(attrs.ngEnter);
          });
          event.preventDefault();
        }
      });
    };
  })
  .directive('ngEscape', function() {
    return function(scope, element, attrs) {
      element.bind('keydown keypress', function(event) {
        if (event.which === 27) {
          console.log('escape!');
          scope.$apply(function() {
            scope.$eval(attrs.ngEscape);
          });
          event.preventDefault();
        }
      });
    };
  })
  .directive('uploadAutomatico', [
    'FebosAPI',
    function(FebosAPI, $parse) {
      return {
        restrict: 'A',
        scope: {
          url: '@',
          identificador: '@',
          nombrearchivo: '@',
          formatospermitidos: '@',
          objeto: '=',
          path: '=',
          finalice: '=',
          progrespercent: '=',
          inproces: '=',
          progres: '='
        },
        link: function(scope, element, attributes, controller) {
          element.bind('change', function(changeEvent) {
            var _proceso_al_obtener_url = 3;
            var archivo = element[0].files[0];
            var mime = archivo.type;
            var nombre = archivo.name;
            var time = new Date().getTime();

            scope.objeto.adjuntoNombre = archivo.name;

            if (scope.nombrearchivo != null && scope.nombrearchivo != '') {
              var arrayParNombreExtension = archivo.name.split('.').pop();
              scope.objeto.adjuntoUrl =
                scope.url + scope.nombrearchivo + '.' + arrayParNombreExtension;
            } else {
              scope.objeto.adjuntoUrl =
                scope.url + time + '-' + scope.identificador + '.' + archivo.name.split('.').pop();
            }
            //console.log("formatos terminados"+scope.formatospermitidos);

            if (scope.formatospermitidos != null && scope.formatospermitidos != '') {
              var arregloFormatos = scope.formatospermitidos.split('|');
              var permiteFormato = false;
              for (var f = 0; f < arregloFormatos.length; f++) {
                if (mime == arregloFormatos[f]) {
                  permiteFormato = true;
                }
              }
              if (!permiteFormato) {
                scope.objeto.adjuntoEstado = 3;
                scope.objeto.adjuntoNombre = 'ARCHIVO NO CUMPLE FORMATO';
                console.log(mime + ' no soportado');
                scope.$apply();
                return false;
              }
            }

            scope.objeto.adjuntoMime = mime;
            scope.objeto.fecha = moment().format('YYYY-MM-DD HH:MM:SS');
            scope.objeto.adjuntoEstado = 0;

            if (!scope.progrespercent) {
              scope.progrespercent = {};
              console.log('INIT', scope.progrespercent);
            } else {
              console.log('INIT 2', scope.progrespercent);
            }

            function setProgreso(porcentaje, estimado) {
              try {
                if (scope.progrespercent && (estimado || porcentaje >= _proceso_al_obtener_url)) {
                  scope.progrespercent = {
                    width: Math.round(porcentaje) + '%'
                  };

                  scope.$apply();
                }
              } catch (e) {}
            }

            setProgreso(1, true);
            scope.inproces = true;

            function progress(e) {
              if (e.lengthComputable) {
                var max = e.total;
                var current = e.loaded;
                var porcentaje = (current * 100) / max;
                if (scope.progres && porcentaje > _proceso_al_obtener_url) {
                  scope.progres(porcentaje, scope.progrespercent);
                }
                setProgreso(porcentaje);
                if (porcentaje >= 100) {
                  if (scope.finalice) {
                    scope.finalice(porcentaje, scope.progrespercent);
                  }
                  setTimeout(function() {
                    scope.inproces = false;
                  }, 1500);
                }
              }
            }

            FebosAPI['cl' + '_obtener_url_prefirmada'](
              {
                tipo: 'upload',
                key: scope.objeto.adjuntoUrl,
                contentType: mime
              },
              {},
              true,
              false
            ).then(function(response) {
              scope.objeto.adjuntoEstado = 1;
              setProgreso(_proceso_al_obtener_url, true);
              if (scope.progres) {
                scope.progres(_proceso_al_obtener_url, scope.progrespercent);
              }
              var tryUploadFile = function(replace, replaced, url, archivo, error) {
                $.ajax({
                  type: 'PUT',
                  url: url.replace(replace, replaced),
                  processData: false,
                  data: archivo,
                  headers: {
                    Vary: 'Access-Control-Request-Headers',
                    Vary: 'Access-Control-Request-Method',
                    Vary: 'Origin'
                  },
                  xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                      myXhr.upload.addEventListener('progress', progress, false);
                    }
                    return myXhr;
                  }
                })
                  .success(function(data) {
                    if (scope.path) {
                      scope.path = scope.objeto.adjuntoUrl;
                      console.log('asignamos path ' + scope.path);
                    }
                    scope.objeto.adjuntoEstado = 2;
                    console.log('Archivo subido');
                    scope.$apply();
                  })
                  .error(error);
              };

              tryUploadFile('http', 'https', response.data.url, archivo, function() {
                console.log('ERROR CARGANDO 1');
                tryUploadFile('https', 'http', response.data.url, archivo, function() {
                  scope.objeto.adjuntoEstado = 3;
                  console.log('Archivo NO subido');
                  scope.objeto.adjuntoNombre = 'Archivo NO subido';
                  scope.$apply();
                });
              });
            });
            scope.$apply();
          });
        }
      };
    }
  ])
  .directive('uploadAutomaticos', [
    '$compile',
    'FebosAPI',
    'FebosUtil',
    function($compile, FebosAPI, FebosUtil) {
      return {
        template:
          '<div class="uk-form-file uk-text-primary">\n' +
          '{{textoi}} <i class="material-icons" style="{{estilo}}">{{icon}}</i> <span>{{textod}}</span> \n' +
          '<input type="file" id="{{identificador}}"/>\n' +
          '</div>',
        restrict: 'E',
        scope: {
          url: '@',
          identificador: '@',
          objeto: '=',
          icon: '@?',
          textoi: '@?',
          textod: '@?',
          estilo: '@?',
          template: '@?'
        },
        link: function(scope, element, attributes, controller) {
          console.log('ICONO     ', scope.icon);
          if (scope.icon == undefined) {
            scope.icon = 'attach_file';
          }
          if (scope.textoi == undefined) {
            scope.textoi = '';
          }
          if (scope.textod == undefined) {
            scope.textod = '';
          }
          if (scope.estilo == undefined) {
            scope.estilo = '';
          }
          if (scope.template) {
            element.html(scope.template);
            $compile(element.contents())(scope);
          }
          element.bind('change', function(changeEvent) {
            console.log('change', this);
            var archivo = this.getElementsByTagName('input')[0].files[0];
            var mime = archivo.type;
            var nombre = archivo.name;

            var adjunto = {};
            adjunto.adjuntoId = new Date().getTime() + '-' + FebosUtil.uuid();
            adjunto.adjuntoNombre = nombre;
            if (scope.identificador) {
              adjunto.adjuntoUrl =
                scope.url + scope.identificador + '.' + archivo.name.split('.').pop();
            } else {
              adjunto.adjuntoUrl =
                scope.url + new Date().getTime() + '.' + archivo.name.split('.').pop();
            }
            adjunto.adjuntoMime = mime;
            adjunto.fecha = moment().format('YYYY-MM-DD HH:MM:SS');
            adjunto.adjuntoEstado = 0;
            scope.objeto = scope.objeto || [];
            scope.objeto.push(adjunto);
            try {
              scope.$apply();
            } catch (e) {}
            console.log('CARGANDO 1 ', adjunto.adjuntoEstado);
            FebosAPI.cl_obtener_url_prefirmada(
              {
                tipo: 'upload',
                key: adjunto.adjuntoUrl,
                contentType: mime
              },
              {},
              true,
              false
            ).then(function(response) {
              adjunto.adjuntoEstado = 1;
              console.log('CARGANDO 2 ', adjunto.adjuntoEstado);
              var tryUploadFile = function(replace, replaced, url, archivo, error) {
                $.ajax({
                  type: 'PUT',
                  url: url.replace(replace, replaced),
                  processData: false,
                  data: archivo,
                  headers: {
                    Vary: 'Access-Control-Request-Headers',
                    Vary: 'Access-Control-Request-Method',
                    Vary: 'Origin'
                  },
                  xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                      myXhr.upload.addEventListener('progress', progress, false);
                    }
                    return myXhr;
                  }
                })
                  .success(function(data) {
                    if (scope.path) {
                      scope.path = scope.objeto.adjuntoUrl;
                      console.log('asignamos path ' + scope.path);
                    }
                    scope.objeto.adjuntoEstado = 2;
                    console.log('Archivo subido');
                    scope.$apply();
                  })
                  .error(error);
              };

              tryUploadFile('http', 'https', response.data.url, archivo, function() {
                console.log('ERROR CARGANDO 1');
                tryUploadFile('https', 'http', response.data.url, archivo, function() {
                  scope.objeto.adjuntoEstado = 3;
                  console.log('Archivo NO subido');
                  scope.objeto.adjuntoNombre = 'Archivo NO subido';
                  scope.$apply();
                });
              });
            });
            scope.$apply();
          });
        }
      };
    }
  ])
  .directive('adjuntoDescargar', [
    '$rootScope',
    '$compile',
    'FebosAPI',
    'FebosUtil',
    function($rootScope, $compile, FebosAPI, FebosUtil) {
      return {
        restrict: 'A',
        scope: {
          mensaje: '@?',
          adjuntoObj: '='
        },
        link: function(scope, element, attrs, controller) {
          var mensaje = attrs.mensaje || 'Descargando adjunto  ...';
          var path = attrs.path || '';
          var nombre = attrs.nombre || '';
          element.bind('click', function() {
            $rootScope.blockModal = UIkit.modal.blockUI(
              "<div class='uk-text-center'>" +
                mensaje +
                '' +
                "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
            );
            FebosAPI.cl_obtener_archivo_privado(
              {
                path: path,
                nombre: nombre
              },
              undefined,
              false,
              true
            )
              .then(function(response) {
                $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
                if (response['data']['codigo'] != 0) {
                } else {
                  setTimeout(function() {
                    window.open(response['data']['url']);
                  }, 1000);
                }
              })
              .finally(function() {
                setTimeout(function() {
                  $rootScope.blockModal.hide();
                }, 1000);
              });
          });
        }
      };
    }
  ])
  .directive('volver', function() {
    return {
      restrict: 'E',
      template:
        '<button class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light" ng-click="volver()"><i class="material-icons">undo</i> Volver </button>',
      link: function(scope, element, attrs) {
        element.on('click', function() {
          history.back();
        });
      }
    };
  })
  .directive('noSpecialChar', function() {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function(scope, element, attrs, modelCtrl) {
        modelCtrl.$parsers.push(function(inputValue) {
          if (inputValue == undefined) return '';
          var cleanInputValue = inputValue.replace(/[^\wñ.\s]/gi, '');
          if (cleanInputValue != inputValue) {
            modelCtrl.$setViewValue(cleanInputValue);
            modelCtrl.$render();
          }
          return cleanInputValue;
        });
      }
    };
  })
  .directive('permiso', [
    '$rootScope',
    '$document',
    '$state',
    '$timeout',
    '$location',
    'ComplementoDte',
    '$stateParams',
    'SesionFebos',
    'VistasDte',
    function(
      $rootScope,
      $document,
      $state,
      $timeout,
      $location,
      ComplementoDte,
      $stateParams,
      SesionFebos,
      VistasDte
    ) {
      return {
        restrict: 'A',
        controller: function($scope) {},
        link: function(scope, elem, attr) {
          var permiso = attr.permiso.split('|')[0].split(',');
          var posicion = attr.permiso.split('|')[1];
          attr.permiso = permiso.join(',');
          var tieneTodosLosPermisos = 0;
          $(elem).attr('permiso', permiso);
          $(elem).addClass('superadmin-' + posicion);
          //esconder el elemento completo si el usuario no tiene permisos sobre el
          if (typeof SesionFebos() != 'undefined') {
            for (var i = 0; i < SesionFebos().usuario.permisos.length; i++) {
              for (var x = 0; x < permiso.length; x++) {
                if (SesionFebos().usuario.permisos[i].codigo == permiso[x]) {
                  tieneTodosLosPermisos++;
                }
              }
            }
          }
          if (tieneTodosLosPermisos != permiso.length) {
            elem.hide();
          }
        }
      };
    }
  ])
  .directive('validar', function() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var regla = attrs.validar;
        modelCtrl.$parsers.push(function(valorIngresado) {
          var inputAlterado = ''; //valorIngresado;
          switch (regla) {
            case 'soloNumeros':
              var caracteresValidos = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', ','];

              //se modifica el valor del input, quitando los caracteres invalidos (o dejando solo los invalidos)
              var yaSeMarcoUnDecimal = false;
              for (var i = 0; i < valorIngresado.length; i++) {
                if (valorIngresado[0] == '.' || valorIngresado[0] == ',') continue;
                if (yaSeMarcoUnDecimal && (valorIngresado[i] == '.' || valorIngresado[i] == ','))
                  continue;
                if (caracteresValidos.indexOf(valorIngresado[i]) >= 0) {
                  inputAlterado += valorIngresado[i];
                }
                if (valorIngresado[i] == '.' || valorIngresado[i] == ',') {
                  yaSeMarcoUnDecimal = true;
                }
              }

              //si quitando los caracteres invalidos, el input es distinto del
              //valor alterado, significa que el input original tenia caracteres
              //invalidos, asi que NO se rendera el input original, si no que el
              //alterado, al cual ya se le quitaron los caracteres invalidos
              if (inputAlterado != valorIngresado || valorIngresado.includes(',')) {
                modelCtrl.$setViewValue(inputAlterado.replace(',', '.'));
                modelCtrl.$render();
              }
              break;
            case 'largoMaximo45':
              inputAlterado = valorIngresado.substring(0, 45);
              modelCtrl.$setViewValue(inputAlterado);
              modelCtrl.$render();
              break;
          }
          return inputAlterado;
        });
      }
    };
  })
  .directive('numeroMax', function() {
    return {
      link: function(scope, element, attributes) {
        var oldVal = null;
        element.on('keydown keyup', function(e) {
          if (
            Number(element.val()) > Number(attributes.max) &&
            e.keyCode != 46 && // delete
            e.keyCode != 8 // backspace
          ) {
            e.preventDefault();
            element.val(oldVal);
          } else if (isNaN(element.val())) {
            element.val(0);
          } else {
            oldVal = Number(element.val());
          }
        });
      }
    };
  })
  .directive('formatoNumero', function($filter) {
    return {
      require: 'ngModel',
      link: function(scope, elem, attrs, ngModelCtrl) {
        var prefijo = attrs.prefijo || '';
        var onfocusout = attrs.event || 'focusout';
        var sufijo = attrs.sufijo || '';
        var separadorMiles = attrs.miles || '.';
        var separadorDecimal = attrs.decimal || (separadorMiles == '.' ? ',' : '.');

        ngModelCtrl.$formatters.push(function(modelValue) {
          return setDisplayNumber(modelValue, true);
        });

        // it's best to change the displayed text using elem.val() rather than
        // ngModelCtrl.$setViewValue because the latter will re-trigger the parser
        // and not necessarily in the correct order with the changed value last.
        // see http://radify.io/blog/understanding-ngmodelcontroller-by-example-part-1/
        // for an explanation of how ngModelCtrl works.
        ngModelCtrl.$parsers.push(function(viewValue) {
          setDisplayNumber(viewValue);
          return setModelNumber(viewValue);
        });

        // occasionally the parser chain doesn't run (when the user repeatedly
        // types the same non-numeric character)
        // for these cases, clean up again half a second later using "keyup"
        // (the parser runs much sooner than keyup, so it's better UX to also do it within parser
        // to give the feeling that the comma is added as they type)
        elem.bind(onfocusout, function() {
          setDisplayNumber(elem.val());
        });

        function setDisplayNumber(val, formatter) {
          var valStr, displayValue;

          if (typeof val === 'undefined') {
            return 0;
          }
          var rexMiles = undefined;
          if (separadorMiles == '.') {
            rexMiles = /\./g;
          } else {
            rexMiles = /\,/g;
          }
          var rexDec = undefined;
          if (separadorDecimal == '.') {
            rexDec = /\./g;
          } else {
            rexDec = /\,/g;
          }

          valStr = val.toString();
          displayValue = valStr.replace(/[^1234567890\-\.\,]+/gm, '');
          // displayValue=displayValue.replace(/\D+$/gm, ''); console.log("setDisplayNumberdisplayValue "+displayValue)
          displayValue = displayValue.replace(rexMiles, '');
          displayValue = displayValue.replace(rexDec, '.');
          displayValue = displayValue.replace(/[A-Za-z]/g, '');

          var testValue = '';
          var desimalPart = '0';
          if (displayValue.endsWith('.')) {
            testValue = new String(displayValue) + '0';
            displayValue = new String(displayValue) + '0';
          } else {
            testValue = displayValue;
          }

          displayValue = !isNaN(testValue) ? displayValue.toString() : '';
          var parts = displayValue.split('.');
          displayValue = parts[0];
          try {
            desimalPart = parts[1];
          } catch (e) {}
          // handle leading character -/0
          if (valStr.length === 1 && valStr[0] === '-') {
            displayValue = valStr[0];
          } else if (valStr.length === 1 && valStr[0] === '0') {
            displayValue = '0';
          } else {
            displayValue =
              (prefijo || '') +
              new String(displayValue).replace(/\B(?=(\d{3})+(?!\d))/g, separadorMiles || '.') +
              (desimalPart != undefined && desimalPart.trim().length > 0
                ? separadorDecimal + desimalPart
                : '') +
              (sufijo || '');
            // console.log('1123  ' + displayValue);
          }

          if (typeof formatter !== 'undefined') {
            return displayValue === '' ? 0 : displayValue;
          } else {
            elem.val(displayValue === '0' ? '0' : displayValue);
          }
        }

        function setModelNumber(val) {
          var rexMiles = undefined;
          if (separadorMiles == '.') {
            rexMiles = /\./g;
          } else {
            rexMiles = /\,/g;
          }
          var rexDec = undefined;
          if (separadorDecimal == '.') {
            rexDec = /\./g;
          } else {
            rexDec = /\,/g;
          }

          //console.log('setModelNumber ' + val);
          var modelNum = val
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, '')
            .replace(rexMiles, '')
            .replace(rexDec, '.')
            .replace(/[A-Za-z]/g, '');
          modelNum = parseFloat(modelNum);
          modelNum = !isNaN(modelNum) ? modelNum : 0;
          if (modelNum.toString().indexOf('.') !== -1) {
            modelNum = modelNum; //Math.round((modelNum + 0.00001) * 100) / 100;
          }
          if (attrs.positive) {
            modelNum = Math.abs(modelNum);
          }
          //console.log('modelNum ' + modelNum);
          return modelNum;
        }
      }
    };
  })
  .directive('regexReplace', function($filter) {
    return {
      require: 'ngModel',
      link: function(scope, elem, attrs, ngModelCtrl) {
        ngModelCtrl.$formatters.push(function(modelValue) {
          return setDisplayNumber(modelValue, true);
        });
        ngModelCtrl.$parsers.push(function(viewValue) {
          setDisplayNumber(viewValue);
          return setModelNumber(viewValue);
        });
        elem.bind('keyup focus', function() {
          setDisplayNumber(elem.val());
        });
        var reg = attrs['regexReplace'] || '';
        var repaced = attrs['regexReplaced'] || '';

        function setDisplayNumber(val, formatter) {
          if (typeof val === 'undefined') {
            return '';
          }
          var valStr, displayValue;
          var regex = '/' + reg + '/g';
          displayValue = val.toString().replace(eval(regex), repaced);
          if (typeof formatter !== 'undefined') {
            return displayValue === '' ? 0 : displayValue;
          } else {
            elem.val(displayValue === '0' ? '' : displayValue);
          }
        }

        function setModelNumber(val) {
          var regex = '/' + reg + '/g';
          return val.toString().replace(regex, repaced);
        }
      }
    };
  })
  .directive('includeReplace', function() {
    return {
      require: 'ngInclude',
      restrict: 'A' /* optional */,
      link: function(scope, el, attrs) {
        el.replaceWith(el.children());
      }
    };
  })
  .directive('leerArchivo', function($parse) {
    return {
      restrict: 'A',
      scope: false,
      link: function(scope, element, attrs) {
        var fn = $parse(attrs.leerArchivo);
        console.log('Entro a la directiva!');
        element.on('change', function(onChangeEvent) {
          var reader = new FileReader();

          reader.onload = function(onLoadEvent) {
            scope.$apply(function() {
              console.log('llamando a funcion que procesa!!');
              fn(scope, { $fileContent: onLoadEvent.target.result });
            });
          };

          reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
        });
      }
    };
  });

febosApp
  .directive('ngRut', function($filter) {
    return {
      require: 'ngModel',
      link: function(scope, elem, attrs, ngModelCtrl) {
        var punto = attrs.punto || undefined;

        ngModelCtrl.$formatters.push(function(modelValue) {
          return setDisplayValue(modelValue, true);
        });
        ngModelCtrl.$parsers.push(function(viewValue) {
          setDisplayValue(viewValue);
          return setModelValue(viewValue);
        });
        elem.bind('keyup focus', function() {
          setModelValue(elem.val());
        });

        function setDisplayValue(val, formatter) {
          val = val || '';
          var returnvalue = formatRut(val.toString(), undefined, punto ? 'si' : 'no');
          console.log('display value', returnvalue);

          if (typeof formatter !== 'undefined') {
            return returnvalue;
          } else {
            elem.val(returnvalue);
          }
        }

        function setModelValue(val) {
          val = val || '';
          var returnvalue = formatRut(val.toString());
          return returnvalue;
        }
      }
    };
  })
  .filter('rut', function() {
    return formatRut;
  });

lambdas = febosApp.directive('leerArchivoDetect', function ($parse) {
  return {
    restrict: 'A',
    scope: false,
    link: function (scope, element, attrs) {
      var fn = $parse(attrs.leerArchivoDetect);
      console.log('Entro a la directiva! leer archivo', fn);
      element.on('change', function (onChangeEvent) {
        var reader = new FileReader();
        reader.onload = function (onLoadEvent) {
          scope.$apply(function () {
            var codes = new Uint8Array(onLoadEvent.target.result);
            var encoding = Encoding.detect(codes);
            var unicodeString = Encoding.convert(codes, {
              to: 'unicode',
              from: encoding,
              type: 'string'
            });
            console.log('llamando a funcion que procesa !!  ---  ', attrs.leerArchivo, unicodeString);
            fn(scope, {$fileContent: unicodeString});
          });
        };

        reader.readAsArrayBuffer((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
      });
    }
  };
});
;

febosApp
  .factory('windowDimensions', [
    function() {
      return {
        height: function() {
          return (
            window.innerHeight ||
            document.documentElement.clientHeight ||
            document.body.clientHeight
          );
        },
        width: function() {
          return (
            window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
          );
        }
      };
    }
  ])
  .factory('utils', [
    function() {
      return {
        // Util for finding an object by its 'id' property among an array
        findByItemId: function findById(a, id) {
          for (var i = 0; i < a.length; i++) {
            if (a[i].item_id == id) return a[i];
          }
          return null;
        },
        findById: function findById(a, id) {
          for (var i = 0; i < a.length; i++) {
            if (a[i].id == id) return a[i];
          }
          return null;
        },
        // serialize form
        serializeObject: function(form) {
          var o = {};
          var a = form.serializeArray();
          $.each(a, function() {
            if (o[this.name] !== undefined) {
              if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
              }
              o[this.name].push(this.value || '');
            } else {
              o[this.name] = this.value || '';
            }
          });
          return o;
        },
        // high density test
        isHighDensity: function() {
          return (
            (window.matchMedia &&
              (window.matchMedia(
                'only screen and (min-resolution: 124dpi), only screen and (min-resolution: 1.3dppx), only screen and (min-resolution: 48.8dpcm)'
              ).matches ||
                window.matchMedia(
                  'only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3)'
                ).matches)) ||
            (window.devicePixelRatio && window.devicePixelRatio > 1.3)
          );
        },
        // touch device test
        isTouchDevice: function() {
          return !!('ontouchstart' in window);
        },
        // local storage test
        lsTest: function() {
          var test = 'test';
          try {
            localStorage.setItem(test, test);
            localStorage.removeItem(test);
            return true;
          } catch (e) {
            return false;
          }
        },
        // show/hide card
        card_show_hide: function(card, begin_callback, complete_callback, callback_element) {
          $(card)
            .velocity(
              {
                scale: 0,
                opacity: 0.2
              },
              {
                duration: 400,
                easing: [0.4, 0, 0.2, 1],
                // on begin callback
                begin: function() {
                  if (typeof begin_callback !== 'undefined') {
                    begin_callback(callback_element);
                  }
                },
                // on complete callback
                complete: function() {
                  if (typeof complete_callback !== 'undefined') {
                    complete_callback(callback_element);
                  }
                }
              }
            )
            .velocity('reverse');
        },
        validaciones: {
          largo: function(text, largoMinimo, largoMaximo) {
            if (!text) {
              return false;
            }
            var texto = new String(text);
            var valido = true;
            if (largoMinimo != undefined) {
              var aux = texto.length < largoMinimo;
              console.log(
                ' texto.length [' +
                  texto.length +
                  '] <  largoMinimo [' +
                  largoMinimo +
                  '] aux = ' +
                  aux
              );
              if (aux) {
                valido = false;
              }
            }
            if (largoMaximo != undefined) {
              var aux = texto.length > largoMaximo;

              console.log(
                ' texto.length [' +
                  texto.length +
                  '] >  largoMaximo [' +
                  largoMaximo +
                  '] aux = ' +
                  aux
              );
              if (texto.length > largoMaximo) {
                valido = false;
              }
            }
            return valido;
          },
          email: function(email) {
            if (!email) {
              return false;
            }
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(
              String(email)
                .trim()
                .toLowerCase()
            );
          },
          rut: function(rut) {
            if (!rut) {
              return false;
            }
            var suma = 0;
            var arrRut = rut.split('-');
            var rutSolo = arrRut[0];
            var verif = arrRut[1];
            var continuar = true;
            for (i = 2; continuar; i++) {
              suma += (rutSolo % 10) * i;
              rutSolo = parseInt(rutSolo / 10);
              i = i == 7 ? 1 : i;
              continuar = rutSolo == 0 ? false : true;
            }
            var resto = suma % 11;
            var dv = 11 - resto;
            if (dv == 10) {
              if (verif.toUpperCase() == 'K') return true;
            } else if (dv == 11 && verif == 0) return true;
            else if (dv == verif) return true;
            else return false;
          }
        }
      };
    }
  ]);

angular.module('ConsoleLogger', []).factory('PrintToConsole', [
  '$rootScope',
  function($rootScope) {
    var handler = { active: false };
    handler.toggle = function() {
      handler.active = !handler.active;
    };
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
      if (handler.active) {
        console.log('$stateChangeStart --- event, toState, toParams, fromState, fromParams');
        console.log(arguments);
      }
    });
    $rootScope.$on('$stateChangeError', function(
      event,
      toState,
      toParams,
      fromState,
      fromParams,
      error
    ) {
      if (handler.active) {
        console.log('$stateChangeError --- event, toState, toParams, fromState, fromParams, error');
        console.log(arguments);
      }
    });
    $rootScope.$on('$stateChangeSuccess', function(
      event,
      toState,
      toParams,
      fromState,
      fromParams
    ) {
      if (handler.active) {
        console.log('$stateChangeSuccess --- event, toState, toParams, fromState, fromParams');
        console.log(arguments);
      }
    });
    $rootScope.$on('$viewContentLoading', function(event, viewConfig) {
      if (handler.active) {
        console.log('$viewContentLoading --- event, viewConfig');
        console.log(arguments);
      }
    });
    $rootScope.$on('$viewContentLoaded', function(event) {
      if (handler.active) {
        console.log('$viewContentLoaded --- event');
        console.log(arguments);
      }
    });
    $rootScope.$on('$stateNotFound', function(event, unfoundState, fromState, fromParams) {
      if (handler.active) {
        console.log('$stateNotFound --- event, unfoundState, fromState, fromParams');
        console.log(arguments);
      }
    });
    return handler;
  }
]);
febosApp.factory('FebosUtil', [
  'CONFIGURACION',
  'ContextoFebos',
  'SesionFebos',
  '$stateParams',
  function(CONFIGURACION, ContextoFebos, SesionFebos, $stateParams) {
    var Base64 = {
      _keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',
      encode: function(e) {
        var t = '';
        var n, r, i, s, o, u, a;
        var f = 0;
        e = Base64._utf8_encode(e);
        while (f < e.length) {
          n = e.charCodeAt(f++);
          r = e.charCodeAt(f++);
          i = e.charCodeAt(f++);
          s = n >> 2;
          o = ((n & 3) << 4) | (r >> 4);
          u = ((r & 15) << 2) | (i >> 6);
          a = i & 63;
          if (isNaN(r)) {
            u = a = 64;
          } else if (isNaN(i)) {
            a = 64;
          }
          t =
            t +
            this._keyStr.charAt(s) +
            this._keyStr.charAt(o) +
            this._keyStr.charAt(u) +
            this._keyStr.charAt(a);
        }
        return t;
      },
      decode: function(e) {
        var t = '';
        var n, r, i;
        var s, o, u, a;
        var f = 0;
        e = e.replace(/[^A-Za-z0-9+/=]/g, '');
        while (f < e.length) {
          s = this._keyStr.indexOf(e.charAt(f++));
          o = this._keyStr.indexOf(e.charAt(f++));
          u = this._keyStr.indexOf(e.charAt(f++));
          a = this._keyStr.indexOf(e.charAt(f++));
          n = (s << 2) | (o >> 4);
          r = ((o & 15) << 4) | (u >> 2);
          i = ((u & 3) << 6) | a;
          t = t + String.fromCharCode(n);
          if (u != 64) {
            t = t + String.fromCharCode(r);
          }
          if (a != 64) {
            t = t + String.fromCharCode(i);
          }
        }
        t = Base64._utf8_decode(t);
        return t;
      },
      _utf8_encode: function(e) {
        e = e.replace(/rn/g, 'n');
        var t = '';
        for (var n = 0; n < e.length; n++) {
          var r = e.charCodeAt(n);
          if (r < 128) {
            t += String.fromCharCode(r);
          } else if (r > 127 && r < 2048) {
            t += String.fromCharCode((r >> 6) | 192);
            t += String.fromCharCode((r & 63) | 128);
          } else {
            t += String.fromCharCode((r >> 12) | 224);
            t += String.fromCharCode(((r >> 6) & 63) | 128);
            t += String.fromCharCode((r & 63) | 128);
          }
        }
        return t;
      },
      _utf8_decode: function(e) {
        var t = '';
        var n = 0;
        var r = (c1 = c2 = 0);
        while (n < e.length) {
          r = e.charCodeAt(n);
          if (r < 128) {
            t += String.fromCharCode(r);
            n++;
          } else if (r > 191 && r < 224) {
            c2 = e.charCodeAt(n + 1);
            t += String.fromCharCode(((r & 31) << 6) | (c2 & 63));
            n += 2;
          } else {
            c2 = e.charCodeAt(n + 1);
            c3 = e.charCodeAt(n + 2);
            t += String.fromCharCode(((r & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            n += 3;
          }
        }
        return t;
      }
    };

    return {
      uuid: function() {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
      },
      snakeToCamel: function(s) {
        return s.replace(/(\_\w)/g, function(m) {
          return m[1].toUpperCase();
        });
      },
      modales: function(modales) {
        var m = {};
        for (var i = 0; i < modales.length; i++) {
          m[this.snakeToCamel(modales[i])] = UIkit.modal('#' + modales[i]);
        }
        return m;
      },
      json2xml: function(obj) {
        var xml = '';

        for (var prop in obj) {
          if (!obj.hasOwnProperty(prop)) {
            continue;
          }

          if (obj[prop] == undefined) continue;

          xml += '<' + prop + '>';
          if (typeof obj[prop] == 'object') xml += objectToXml(new Object(obj[prop]));
          else xml += obj[prop];

          xml += '<!--' + prop + '-->';
        }

        return xml;
      },
      contexto: function() {
        return ContextoFebos.sesiones[ContextoFebos.usuarioActual];
      },
      formatoMonto: function(numero, simbolo) {
        var partes = numero.toString().split('.');
        partes[0] = partes[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
        return simbolo + ' ' + partes.join(',');
      },
      obtenerAmbiente: function() {
        var posiblesAmbientes = /desarrollo|produccion|certificacion|pruebas/i;
        try {
          var partes = window.location.href
            .replace('https://', '')
            .replace('http://', '')
            .split('/');
          return partes[1].match(posiblesAmbientes) != null
            ? partes[1]
            : CONFIGURACION.AMBIENTE_POR_DEFECTO;
        } catch (e) {
          console.log(
            'imposible detectar ambiente, utilizando el por defecto: ' +
              CONFIGURACION.AMBIENTE_POR_DEFECTO
          );
          return CONFIGURACION.AMBIENTE_POR_DEFECTO;
        }
      },
      obtenerAmbito: function() {
        if ($stateParams.app != undefined) return $stateParams.app;
        var posiblesAmbitos = /cloud|proveedores|clientes/i;
        try {
          var partes = window.location.href
            .replace('https://', '')
            .replace('http://', '')
            .split('/');
          return partes[1].match(posiblesAmbitos) != null
            ? partes[1]
            : CONFIGURACION.AMBITO_POR_DEFECTO;
        } catch (e) {
          console.log(
            'imposible detectar ambito, utilizando el por defecto: ' +
              CONFIGURACION.AMBITO_POR_DEFECTO
          );
          return CONFIGURACION.AMBITO_POR_DEFECTO;
        }
      },
      obtenerDominio: function() {
        var resultados = window.location.href.match(/^https?\:\/\/([^\/:?#]+)(?:[\/:?#]|$)/i);
        return resultados != null && resultados.length >= 2 ? resultados[1] : '';
      },
      obtenerRequestBase: function() {
        var baseRequest = {};
        if (typeof ContextoFebos.sesiones[ContextoFebos.usuarioActual] == 'undefined') {
          baseRequest.headers = {
            ambito: this.obtenerAmbito(),
            'Content-Type': 'application/json'
          };
        } else {
          baseRequest.headers = {
            token: ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.token,
            empresa: ContextoFebos.sesiones[ContextoFebos.usuarioActual].empresa.iut,
            grupo: ContextoFebos.sesiones[ContextoFebos.usuarioActual].grupo.id,
            ambito: this.obtenerAmbito(),
            rutCliente: ContextoFebos.sesiones[ContextoFebos.usuarioActual].cliente.iut,
            rutProveedor: ContextoFebos.sesiones[ContextoFebos.usuarioActual].proveedor.iut,
            'Content-Type': 'application/json'
          };
        }
        return baseRequest;
      },

      // Propiedades Extras (configurar en tu archivo de configuracion por pais)
      tipoDocumento:function tipoDocumento(codigo, mostrarCodigo, versionCompacta) {
  var nombre = '';
  versionCompacta = typeof versionCompacta == 'undefined' ? false : versionCompacta;
  switch (codigo) {
    /** Borradores **/
    case -33:
      nombre = versionCompacta ? 'F.E. Afecta (B)' : 'Factura A. Elect. (Borrador)';
      break;
    case -34:
      nombre = versionCompacta ? 'F.E. Exenta (B)' : 'Factura E. Elect. (Borrador)';
      break;
    case -39:
      nombre = versionCompacta ? 'Boleta (E)(B)' : 'Boleta Elect. (Borrador)';
      break;
    case -41:
      nombre = versionCompacta ? 'Boleta Exe. (E)(B)' : 'Boleta E. Elect. (Borrador)';
      break;
    case -43:
      nombre = versionCompacta ? 'F.E. Liq. (B)' : 'Factura Liq. Elect. (Borrador)';
      break;
    case -46:
      nombre = versionCompacta ? 'F.E. Compra. (B)' : 'Factura Compra Elect. (Borrador)';
      break;
    case -52:
      nombre = versionCompacta ? 'Guía Desp. (E)(B)' : 'Guía de Depacho Elect. (Borrador)';
      break;
    case -56:
      nombre = versionCompacta ? 'N.D. (E)(B)' : 'N. Déb. Elect. (Borrador)';
      break;
    case -61:
      nombre = versionCompacta ? 'N.C. (E)(B)' : 'N. Créd. Elect. (Borrador)';
      break;
    case -110:
      nombre = versionCompacta ? 'F.E. Exp. (E)(B)' : 'Factura Exp. Elect. (Borrador)';
      break;
    case -111:
      nombre = versionCompacta ? 'N.D. Exp. (E)(B)' : 'N. Déb. Exp. Elect. (Borrador)';
      break;
    case -112:
      nombre = versionCompacta ? 'N.C. Exp. (E)(B)' : 'N. Créd. Exp. Elect. (Borrador)';
      break;
    case 33:
      nombre = versionCompacta ? 'F.E. Afecta' : 'Factura Afecta Electrónica';
      break;
    case 34:
      nombre = versionCompacta ? 'F.E. Exenta' : 'Factura Exenta Electrónica';
      break;
    case 39:
      nombre = versionCompacta ? 'Boleta (E)' : 'Boleta Electrónica';
      break;
    case 41:
      nombre = versionCompacta ? 'Boleta Exe. (E)' : 'Boleta Exenta Electrónica';
      break;
    case 43:
      nombre = versionCompacta ? 'F.E. Liq.' : 'Factura Liquidación Electrónica';
      break;
    case 46:
      nombre = versionCompacta ? 'F.E. Compra.' : 'Factura Compra Electrónica';
      break;
    case 52:
      nombre = versionCompacta ? 'Guía Desp. (E)' : 'Guía de Depacho Electrónica';
      break;
    case 56:
      nombre = versionCompacta ? 'N.D. (E)' : 'Nota de Débito Electrónica';
      break;
    case 61:
      nombre = versionCompacta ? 'N.C. (E)' : 'Nota de Crédito Electrónica';
      break;
    case 110:
      nombre = versionCompacta ? 'F.E. Exp. (E)' : 'Factura Exportación Electrónica';
      break;
    case 111:
      nombre = versionCompacta ? 'N.D. Exp. (E)' : 'Nota de Débito Exportación Electrónica';
      break;
    case 112:
      nombre = versionCompacta ? 'N.C. Exp. (E)' : 'Nota de Crédito Exportación Electrónica';
      break;
    case 66:
      nombre = versionCompacta ? 'B. Honorarios (E)' : 'Boleta Honorarios Electrónica';
      break;
  }
  return mostrarCodigo == true ? '(' + codigo + ') ' + nombre : nombre;
}
,tipoEvento:function tipoEvento(codigo, mostrarCodigo, versionCompacta) {
  var nombre = '';
  versionCompacta = typeof versionCompacta == 'undefined' ? false : versionCompacta;
  switch (codigo) {
    case 1:
      nombre = versionCompacta ? 'Recep. archivo int. TXT' : 'RECEPCION_ARCHIVO_INTEGRACION_TXT';
      break;
    case 2:
      nombre = versionCompacta ? 'Recep. archivo int. XML' : 'RECEPCION_ARCHIVO_INTEGRACION_XML';
      break;
    case 3:
      nombre = versionCompacta ? 'Error integración' : 'ERROR_INTEGRACION';
      break;
    case 4:
      nombre = versionCompacta ? 'Doc. preprocesado' : 'DOCUMENTO_PREPROCESADO';
      break;
    case 5:
      nombre = versionCompacta ? 'Doc. firmado' : 'DOCUMENTO_FIRMADO';
      break;
    case 6:
      nombre = versionCompacta ? 'Doc. enviado EAI' : 'DOCUMENTO_ENVIADO_EAI';
      break;
    case 7:
      nombre = versionCompacta ? 'Doc. recep. EAI' : 'DOCUMENTO_RECEPCIONADO_EAI';
      break;
    case 8:
      nombre = versionCompacta ? 'Error al enviar a EAI' : 'ERROR_AL_ENVIAR_A_EAI';
      break;
    case 9:
      nombre = versionCompacta ? 'Error resp. EAI' : 'ERROR_RESPUESTA_EAI';
      break;
    case 10:
      nombre = versionCompacta ? 'Doc. aceptado EAI' : 'DOCUMENTO_ACEPTADO_EAI';
      break;
    case 11:
      nombre = versionCompacta ? 'Doc. aceptado reparos EAI' : 'DOCUMENTO_ACEPTADO_REPAROS_EAI';
      break;
    case 12:
      nombre = versionCompacta ? 'Doc. rechacho EAI' : 'DOCUMENTO_RECHAZADO_EAI';
      break;
    case 13:
      nombre = versionCompacta ? 'Doc. enviado a receptor' : 'DOCUMENTO_ENVIADO_A_RECEPTOR';
      break;
    case 14:
      nombre = versionCompacta
        ? 'Doc. enviado a direccion personalizada'
        : 'DOCUMENTO_ENVIADO_A_DIRECCION_PERSONALIZADA';
      break;
    case 15:
      nombre = versionCompacta ? 'Doc. recibido por receptor' : 'DOCUMENTO_RECIBIDO_POR_RECEPTOR';
      break;
    case 16:
      nombre = versionCompacta ? 'Envio rechazado por receptor' : 'ENVIO_RECHAZADO_POR_RECEPTOR';
      break;
    case 17:
      nombre = versionCompacta
        ? 'Doc. aceptado comercialmente por receptor'
        : 'DOCUMENTO_ACEPTADO_COMERCIALEMENTE_POR_RECEPTOR';
      break;
    case 18:
      nombre = versionCompacta
        ? 'Doc. aceptado reparos comercialmente por receptor'
        : 'DOCUMENTO_ACEPTADO_REPAROS_COMERCIALEMENTE_POR_RECEPTOR';
      break;
    case 19:
      nombre = versionCompacta
        ? 'Doc. rechazado comercialmente por receptor'
        : 'DOCUMENTO_RECHAZADO_COMERCIALEMENTE_POR_RECEPTOR';
      break;
    case 20:
      nombre = versionCompacta ? 'Envio recibo mercaderías' : 'ENVIO_RECIBO_DE_MERCADERIAS';
      break;
    case 21:
      nombre = versionCompacta
        ? 'Envio recibo mercaderías parcial'
        : 'ENVIO_RECIBO_DE_MERCADERIAS_PARCIAL';
      break;
    case 22:
      nombre = versionCompacta ? 'Devolución total mercaderías' : 'DEVOLUCION_TOTAL_DE_MERCADERIAS';
      break;
    case 23:
      nombre = versionCompacta
        ? 'Devolución parcial de mercadería'
        : 'DEVOLUCION_PARCIAL_DE_MERCADERIAS';
      break;
    case 24:
      nombre = versionCompacta ? 'Comentario de usuario' : 'COMENTARIO_DE_USUARIO';
      break;
    case 25:
      nombre = versionCompacta ? 'Asig. Folio' : 'ASIGNACION_FOLIO';
      break;
    case 26:
      nombre = versionCompacta ? 'Asig. CAF' : 'ASIGNACION_CAF';
      break;
    case 27:
      nombre = versionCompacta ? 'Almacenamiento XML Doc.' : 'ALMACENAMIENTO_XML_DOCUMENTO';
      break;
    case 28:
      nombre = versionCompacta ? 'Visualización XML' : 'VISUALIZACION_XML';
      break;
    case 29:
      nombre = versionCompacta ? 'Visualización Básica' : 'VISUALIZACION_BASICA';
      break;
    case 30:
      nombre = versionCompacta ? 'Visualización imagen tipo 0' : 'VISUALIZACION_IMAGEN_TIPO_0';
      break;
    case 31:
      nombre = versionCompacta ? 'Visualización imagen tipo 1' : 'VISUALIZACION_IMAGEN_TIPO_1';
      break;
    case 32:
      nombre = versionCompacta ? 'Visualización imagen tipo 2' : 'VISUALIZACION_IMAGEN_TIPO_2';
      break;
    case 33:
      nombre = versionCompacta ? 'Visualización imagen tipo 3' : 'VISUALIZACION_IMAGEN_TIPO_3';
      break;
    case 34:
      nombre = versionCompacta ? 'Visualización imagen tipo 4' : 'VISUALIZACION_IMAGEN_TIPO_4';
      break;
    case 35:
      nombre = versionCompacta ? 'Visualización imagen tipo 5' : 'VISUALIZACION_IMAGEN_TIPO_5';
      break;
    case 36:
      nombre = versionCompacta ? 'Visualización imagen tipo 6' : 'VISUALIZACION_IMAGEN_TIPO_6';
      break;
    case 37:
      nombre = versionCompacta ? 'Visualización imagen tipo 7' : 'VISUALIZACION_IMAGEN_TIPO_7';
      break;
    case 38:
      nombre = versionCompacta
        ? 'Recep. DTE sin verificar en SII'
        : 'RECEPCION_DTE_SIN_VERIFICAR_EN_SII';
      break;
    case 39:
      nombre = versionCompacta
        ? 'DTE recibido aceptado por SII'
        : 'DTE_RECIBIDO_ACEPTADO_POR_EL_SII';
      break;
    case 40:
      nombre = versionCompacta
        ? 'DTE rechazado aceptado por SII'
        : 'DTE_RECIBIDO_RECHAZADO_POR_EL_SII';
      break;
    case 41:
      nombre = versionCompacta ? 'Almacenamiento adjunto Doc.' : 'ALMACENAMIENTO_ADJUNTO_DOCUMENTO';
      break;
    case 42:
      nombre = versionCompacta ? 'Doc. consultado EAI' : 'DOCUMENTO_CONSULTADO_EAI';
      break;
    case 43:
      nombre = versionCompacta ? 'Solicitud DTE no recibido' : 'SOLICITUD_DTE_NO_RECIBIDO';
      break;
    case 44:
      nombre = versionCompacta ? 'Doc. Cedido' : 'DOCUMENTO_CEDIDO';
      break;
    case 45:
      nombre = versionCompacta ? 'Cesion Rechazada' : 'CESION_RECHAZADA';
      break;
    case 46:
      nombre = versionCompacta ? 'Información de pago' : 'INFORMACION_DE_PAGO';
      break;
    case 47:
      nombre = versionCompacta ? 'DTE recibido en validación' : 'DTE_RECIBIDO_EN_VALIDACION';
      break;
  }
  return mostrarCodigo == true ? '(' + codigo + ') ' + nombre : nombre;
}
,tipoTag:function tipoTag(codigo) {
  var nombre = '';

  switch (codigo) {
    case 'bitacora.mensaje':
      nombre = 'Mensaje Bitacora';
      break;
    case 'DTE.Encabezado.Emisor.RUTEmisor':
      nombre = 'Rut emisor';
      break;
    case 'DTE.Encabezado.IdDoc.TipoDTE':
      nombre = 'Tipo DTE';
      break;
    case 'DTE.Encabezado.IdDoc.Folio':
      nombre = 'Folio';
      break;
    case 'DTE.Encabezado.IdDoc.FchEmis':
      nombre = 'Fecha Emision';
      break;
    case 'DTE.Encabezado.IdDoc.IndServicio':
      nombre = 'Indicador Servicio';
      break;
    case 'DTE.Encabezado.IdDoc.PeriodoDesde':
      nombre = 'Periodo desde';
      break;
    case 'DTE.Encabezado.IdDoc.PeriodoHasta':
      nombre = 'Periodo hasta';
      break;
    case 'DTE.Encabezado.Emisor.RznSoc':
      nombre = 'Razon Social emisor';
      break;
    case 'DTE.Encabezado.Emisor.GiroEmis':
      nombre = 'Giro emisor';
      break;
    case 'DTE.Encabezado.Emisor.Telefono':
      nombre = 'Telefono emisor';
      break;
    case 'DTE.Encabezado.Emisor.Acteco':
      nombre = 'Acteco emisor';
      break;
    case 'DTE.Encabezado.Emisor.Sucursal':
      nombre = 'Sucursal emisor';
      break;
    case 'DTE.Encabezado.Emisor.DirOrigen':
      nombre = 'Direccion origen emisor';
      break;
    case 'DTE.Encabezado.Emisor.CmnaOrigen':
      nombre = 'Comuna origen emisor';
      break;
    case 'DTE.Encabezado.Receptor.RUTRecep':
      nombre = 'Rut receptor';
      break;
    case 'DTE.Encabezado.Receptor.CdgIntRecep':
      nombre = 'Codigo interno receptor';
      break;
    case 'DTE.Encabezado.Receptor.RznSocRecep':
      nombre = 'Razon social receptor';
      break;
    case 'DTE.Encabezado.Receptor.GiroRecep':
      nombre = 'Giro receptor';
      break;
    case 'DTE.Encabezado.Receptor.Contacto':
      nombre = 'Contacto receptor';
      break;
    case 'DTE.Encabezado.Receptor.DirRecep':
      nombre = 'Direccion receptor';
      break;
    case 'DTE.Encabezado.Receptor.CmnaRecep':
      nombre = 'Comuna receptor';
      break;
    case 'DTE.Encabezado.Receptor.CiudadRecep':
      nombre = 'Ciudad receptor';
      break;
    case 'DTE.Encabezado.Receptor.DirPostal':
      nombre = 'Direccion postal receptor';
      break;
    case 'DTE.Encabezado.Receptor.CmnaPostal':
      nombre = 'Comuna postal receptor';
      break;
    case 'DTE.Encabezado.Receptor.CiudadPostal':
      nombre = 'Ciudad postal receptor';
      break;
    case 'DTE.Encabezado.Totales.MntNeto':
      nombre = 'Monto neto';
      break;
    case 'DTE.Encabezado.Totales.MntExe':
      nombre = 'Monto exento';
      break;
    case 'DTE.Encabezado.Totales.IVA':
      nombre = 'Iva';
      break;
    case 'DTE.Encabezado.Totales.MntTotal':
      nombre = 'Monto total';
      break;
    case 'DTE.Encabezado.Totales.VlrPagar':
      nombre = 'Valor a pagar';
      break;
  }

  return nombre;
}
,tipoTagForCompare:function tipoTagForCompare(codigo) {
  switch (codigo) {
    case 'bitacora.mensaje':
      return codigo;
    case 'DTE.Encabezado.Emisor.RUTEmisor':
      return codigo;
    case 'DTE.Encabezado.IdDoc.TipoDTE':
      return codigo;
    case 'DTE.Encabezado.IdDoc.Folio':
      return codigo;
    case 'DTE.Encabezado.IdDoc.FchEmis':
      return codigo;
    case 'DTE.Encabezado.IdDoc.IndServicio':
      return codigo;
    case 'DTE.Encabezado.IdDoc.PeriodoDesde':
      return codigo;
    case 'DTE.Encabezado.IdDoc.PeriodoHasta':
      return codigo;
    case 'DTE.Encabezado.Emisor.RznSoc':
      return codigo;
    case 'DTE.Encabezado.Emisor.GiroEmis':
      return codigo;
    case 'DTE.Encabezado.Emisor.Telefono':
      return codigo;
    case 'DTE.Encabezado.Emisor.Acteco':
      return codigo;
    case 'DTE.Encabezado.Emisor.Sucursal':
      return codigo;
    case 'DTE.Encabezado.Emisor.DirOrigen':
      return codigo;
    case 'DTE.Encabezado.Emisor.CmnaOrigen':
      return codigo;
    case 'DTE.Encabezado.Receptor.RUTRecep':
      return codigo;
    case 'DTE.Encabezado.Receptor.CdgIntRecep':
      return codigo;
    case 'DTE.Encabezado.Receptor.RznSocRecep':
      return codigo;
    case 'DTE.Encabezado.Receptor.GiroRecep':
      return codigo;
    case 'DTE.Encabezado.Receptor.Contacto':
      return codigo;
    case 'DTE.Encabezado.Receptor.DirRecep':
      return codigo;
    case 'DTE.Encabezado.Receptor.CmnaRecep':
      return codigo;
    case 'DTE.Encabezado.Receptor.CiudadRecep':
      return codigo;
    case 'DTE.Encabezado.Receptor.DirPostal':
      return codigo;
    case 'DTE.Encabezado.Receptor.CmnaPostal':
      return codigo;
    case 'DTE.Encabezado.Receptor.CiudadPostal':
      return codigo;
    case 'DTE.Encabezado.Totales.MntNeto':
      return codigo;
    case 'DTE.Encabezado.Totales.MntExe':
      return codigo;
    case 'DTE.Encabezado.Totales.IVA':
      return codigo;
    case 'DTE.Encabezado.Totales.MntTotal':
      return codigo;
    case 'DTE.Encabezado.Totales.VlrPagar':
      return codigo;
  }

  return '';
}
,

      tipoCondicion: function(codigo) {
        var nombre = '';

        switch (codigo) {
          case '==':
            nombre = 'Igual a';
            break;
          case '!=':
            nombre = 'Distinto a';
            break;
          case '<':
            nombre = 'Menor que';
            break;
          case '<=':
            nombre = 'Menor o igual que';
            break;
          case '>':
            nombre = 'Mayor que';
            break;
          case '>=':
            nombre = 'Mayor o igual que';
            break;
          case 'includes':
            nombre = 'Esta en (lista separada por ,)';
            break;
          case 'indexOf':
            nombre = 'Contiene';
            break;
          case 'startsWith':
            nombre = 'Inicia con';
            break;
          case 'endsWith':
            nombre = 'Termina con';
            break;
        }

        return nombre;
      },
      tipoCondicionForCompare: function(codigo) {
        var nombre = '';

        switch (codigo) {
          case '==':
            return codigo;
          case '!=':
            return codigo;
          case '<':
            return codigo;
          case '<=':
            return codigo;
          case '>':
            return codigo;
          case '>=':
            return codigo;
          case 'includes':
            return codigo;
          case 'indexOf':
            return codigo;
          case 'startsWith':
            return codigo;
          case 'endsWith':
            return codigo;
        }

        return nombre;
      },
      cargarInformacionDeIngreso: function(data) {
        //verifico si tengo información previa del usuario
        ContextoFebos.usuarioActual = data.usuario.id;
        if (typeof ContextoFebos.sesiones[data.usuario.id] === 'undefined') {
          //no hay información asi que se le debe crear el objeto contenedor
          ContextoFebos.sesiones[data.usuario.id] = JSON.parse(
            JSON.stringify(ContextoFebos.template)
          );
        }
        ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.nombre = data.usuario.nombre;
        ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.alias = data.usuario.alias;
        ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.correo = data.usuario.correo;
        ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.iut = data.usuario.iut;
        ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.id = data.usuario.id;
        ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.avatar = data.usuario.avatar;
        ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.token = data.token;
      },
      tienePermiso: function(permiso) {
        if (typeof SesionFebos() == 'undefined') return false;
        for (var i = 0; i < SesionFebos().usuario.permisos.length; i++) {
          if (SesionFebos().usuario.permisos[i].codigo == permiso) {
            return true;
          }
        }
        return false;
      },
      decode: function(content) {
        return Base64.decode(content);
      },
      encode: function(content) {
        return Base64.encode(content);
      },
      encodeIso8859_1: function(str) {
        if (/([^\u0000-\u00ff])/.test(str)) throw Error('String must be ASCII');

        var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
        var o1,
          o2,
          o3,
          bits,
          h1,
          h2,
          h3,
          h4,
          e = [],
          pad = '',
          c;

        c = str.length % 3; // pad string to length of multiple of 3
        if (c > 0) {
          while (c++ < 3) {
            pad += '=';
            str += '\0';
          }
        }
        // note: doing padding here saves us doing special-case packing for trailing 1 or 2 chars

        for (c = 0; c < str.length; c += 3) {
          // pack three octets into four hexets
          o1 = str.charCodeAt(c);
          o2 = str.charCodeAt(c + 1);
          o3 = str.charCodeAt(c + 2);

          bits = (o1 << 16) | (o2 << 8) | o3;

          h1 = (bits >> 18) & 0x3f;
          h2 = (bits >> 12) & 0x3f;
          h3 = (bits >> 6) & 0x3f;
          h4 = bits & 0x3f;

          // use hextets to index into code string
          e[c / 3] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
        }
        str = e.join(''); // use Array.join() for better performance than repeated string appends

        // replace 'A's from padded nulls with '='s
        str = str.slice(0, str.length - pad.length) + pad;

        return str;
      },
      formatearFecha: function(fecha) {
        var year = fecha.getFullYear();
        var month = fecha.getMonth() + 1;
        month = month < 10 ? '0' + month : month;
        var day = fecha.getDate();
        day = day < 10 ? '0' + day : day;
        return year + '-' + month + '-' + day;
      },
      sonIguales: function(x, y) {
        var i, l, leftChain, rightChain;

        var compare2Objects = function(x, y) {
          var p;

          // remember that NaN === NaN returns false
          // and isNaN(undefined) returns true
          if (isNaN(x) && isNaN(y) && typeof x === 'number' && typeof y === 'number') {
            return true;
          }

          // Compare primitives and functions.
          // Check if both arguments link to the same object.
          // Especially useful on the step where we compare prototypes
          if (x === y) {
            return true;
          }

          // Works in case when functions are created in constructor.
          // Comparing dates is a common scenario. Another built-ins?
          // We can even handle functions passed across iframes
          if (
            (typeof x === 'function' && typeof y === 'function') ||
            (x instanceof Date && y instanceof Date) ||
            (x instanceof RegExp && y instanceof RegExp) ||
            (x instanceof String && y instanceof String) ||
            (x instanceof Number && y instanceof Number)
          ) {
            return x.toString() === y.toString();
          }

          // At last checking prototypes as good as we can
          if (!(x instanceof Object && y instanceof Object)) {
            return false;
          }

          if (x.isPrototypeOf(y) || y.isPrototypeOf(x)) {
            return false;
          }

          if (x.constructor !== y.constructor) {
            return false;
          }

          if (x.prototype !== y.prototype) {
            return false;
          }

          // Check for infinitive linking loops
          if (leftChain.indexOf(x) > -1 || rightChain.indexOf(y) > -1) {
            return false;
          }

          // Quick checking of one object being a subset of another.
          // todo: cache the structure of arguments[0] for performance
          for (p in y) {
            if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
              return false;
            } else if (typeof y[p] !== typeof x[p]) {
              return false;
            }
          }

          for (p in x) {
            if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
              return false;
            } else if (typeof y[p] !== typeof x[p]) {
              return false;
            }

            switch (typeof x[p]) {
              case 'object':
              case 'function':
                leftChain.push(x);
                rightChain.push(y);

                if (!compare2Objects(x[p], y[p])) {
                  return false;
                }

                leftChain.pop();
                rightChain.pop();
                break;

              default:
                if (x[p] !== y[p]) {
                  return false;
                }
                break;
            }
          }

          return true;
        };

        if (arguments.length < 1) {
          return true; //Die silently? Don't know how to handle such case, please help...
          // throw "Need two or more arguments to compare";
        }

        for (i = 1, l = arguments.length; i < l; i++) {
          leftChain = []; //Todo: this can be cached
          rightChain = [];

          if (!compare2Objects(arguments[0], arguments[i])) {
            return false;
          }
        }

        return true;
      },
      sortObjectByKey: function(objOriginal, key, asc) {
        function sortByKeyDesc(array, key) {
          return array.sort(function(a, b) {
            var x = a[key];
            var y = b[key];
            return x > y ? -1 : x < y ? 1 : 0;
          });
        }

        function sortByKeyAsc(array, key) {
          return array.sort(function(a, b) {
            var x = a[key];
            var y = b[key];
            return x < y ? -1 : x > y ? 1 : 0;
          });
        }

        if (asc) {
          return sortByKeyAsc(objOriginal, key);
        } else {
          return sortByKeyDesc(objOriginal, key);
        }
      }
    };
  }
]);

febosApp.factory('FebosAPI', [
  '$http',
  'ContextoFebos',
  'SesionFebos',
  'CONFIGURACION',
  '$q',
  '$rootScope',
  function($http, ContextoFebos, SesionFebos, CONFIGURACION, $q, $rootScope) {
    var posiblesAmbientes = /desarrollo|produccion|certificacion|pruebas/i;
    var ambiente = 'desarrollo';
    try {
      var partes = window.location.href
        .replace('https://', '')
        .replace('http://', '')
        .split('/');
      ambiente =
        partes[1].match(posiblesAmbientes) != null ? partes[1] : CONFIGURACION.AMBIENTE_POR_DEFECTO;
    } catch (e) {
      ambiente = CONFIGURACION.AMBIENTE_POR_DEFECTO;
    }
    var posiblesAmbitos = /cloud|proveedores|clientes/i;
    var ambito = 'cloud';
    try {
      var partes = window.location.href
        .replace('https://', '')
        .replace('http://', '')
        .split('/');
      ambito =
        partes[1].match(posiblesAmbitos) != null ? partes[1] : CONFIGURACION.AMBITO_POR_DEFECTO;
    } catch (e) {
      ambito = CONFIGURACION.AMBITO_POR_DEFECTO;
    }

    function generarBaseRequest() {
      var baseRequest = {};
      if (typeof ContextoFebos.sesiones[ContextoFebos.usuarioActual] == 'undefined') {
        baseRequest.headers = {
          ambito: ambito,
          'Content-Type': 'application/json'
        };
      } else {
        baseRequest.headers = {
          token: ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.token,
          empresa: ContextoFebos.sesiones[ContextoFebos.usuarioActual].empresa.iut,
          grupo: ContextoFebos.sesiones[ContextoFebos.usuarioActual].grupo.id,
          rutCliente: ContextoFebos.sesiones[ContextoFebos.usuarioActual].cliente.iut,
          rutProveedor: ContextoFebos.sesiones[ContextoFebos.usuarioActual].proveedor.iut,
          'Content-Type': 'application/json'
        };
      }
      return baseRequest;
    }

    var urlBase = CONFIGURACION.BASE_API_URL + ambiente;
    var TEMPLATE_REQUEST = '[API] Solicitud funcion %';
    var TEMPLATE_RESPONSE = '[API] Respuesta funcion %';
    //if(typeof SesionFebos().microCacheAPI == 'undefined'){
    //   SESION.microCacheAPI={};
    //}
    var generarFuncionParaLlamadaAPI = function(nombreLambda, verbo, url) {
      return function(query, body, interceptar, cerrarBlockUI) {
        var clean = function(obj) {
          for (var propName in obj) {
            if (obj[propName] === null || obj[propName] === undefined) {
              delete obj[propName];
            }
          }
        };

        interceptar = typeof interceptar == 'undefined' || interceptar ? true : false;
        cerrarBlockUI = typeof cerrarBlockUI == 'undefined' || cerrarBlockUI ? true : false;
        var baseRequest = generarBaseRequest();
        var logSolititud = TEMPLATE_REQUEST.replace('%', nombreLambda);
        var logRespuesta = TEMPLATE_RESPONSE.replace('%', nombreLambda);

        baseRequest.method = verbo; //verbo  utilizar
        baseRequest.url = urlBase + eval(url);
        baseRequest.data = body;
        baseRequest.params = query;
        baseRequest.params.dominioPortal = CONFIGURACION.DOMINIO;
        clean(query);
        clean(body);
        clean(baseRequest.headers);
        if (nombreLambda != 'cl' + '_latido_usuario')
          console.log(logSolititud, baseRequest);
        var d = $q.defer();
        var numeroDeLlamada = 1;
        var apiCallInterceptada = function() {
          $http(baseRequest).then(
            function(response) {
              if (
                !nombreLambda.includes('_login') &&
                nombreLambda != 'cl' + '_latido_usuario'
              )
                console.log(logRespuesta, response);
              try {
                SesionFebos().usuario.ultimoPingValido = new Date();
              } catch (e) {}
              try {
                if (cerrarBlockUI && typeof $rootScope.blockModal != 'undefined')
                  $rootScope.blockModal.hide();
              } catch (e) {}
              if (
                (typeof response.data.config != 'undefined' &&
                  response.data.config.method == 'HEAD') ||
                (typeof response.data.codigo !== 'undefined' && response.data.codigo == 10)
              ) {
                d.resolve(response);
              } else {
                $rootScope.$broadcast('febosError', response);
              }
            },
            function(err) {
              console.log(
                'Se detecto error en la respuesta a la llamada de la funcion ' +
                  nombreLambda +
                  ', reintentando...'
              );
              if (numeroDeLlamada <= CONFIGURACION.MAX_REINTENTOS_API_CALLS) {
                apiCallInterceptada();
                numeroDeLlamada++;
              } else {
                console.log(logRespuesta, err);
                if (cerrarBlockUI)
                  try {
                    if (cerrarBlockUI && typeof $rootScope.blockModal != 'undefined')
                      $rootScope.blockModal.hide();
                  } catch (e) {}
                $rootScope.$broadcast('febosError');
                d.reject(err);
              }
            }
          );
        };
        var apiCallSinInterceptar = function() {
          $http(baseRequest).then(
            function(response) {
              if (nombreLambda != 'cl' + '_latido_usuario')
                console.log(logRespuesta, response);
              try {
                SesionFebos().usuario.ultimoPingValido = new Date();
              } catch (e) {}
              d.resolve(response);
            },
            function(err) {
              console.log(
                'Se detecto error en la respuesta a la llamada de la funcion ' +
                  nombreLambda +
                  ', reintentando...'
              );
              if (numeroDeLlamada <= CONFIGURACION.MAX_REINTENTOS_API_CALLS) {
                apiCallSinInterceptar();
                numeroDeLlamada++;
              } else {
                console.log(logRespuesta, err);
                d.reject(err);
              }
            }
          );
        };

        if (interceptar) {
          apiCallInterceptada();
        } else {
          apiCallSinInterceptar();
        }
        return d.promise;
      };
    };

    this.generarFuncionParaLlamadaAPI = generarFuncionParaLlamadaAPI;

    var lambdas = [
      {
        nombre: 'io_adm_opciones_listar',
        verbo: 'GET',
        url: "'/herramientas/opciones'"
      },
      {
        nombre: 'io_adm_configuracion_listar',
        verbo: 'GET',
        url: "'/configuracion/configuraciones'"
      },
      {
        nombre: 'cl' + '_adm_opciones_listar',
        verbo: 'GET',
        url: "'/herramientas/opciones'"
      },
      {
        nombre: 'cl' + '_adm_configuracion_listar',
        verbo: 'GET',
        url: "'/configuracion/configuraciones'"
      }
    ];
    lambdas = lambdas.concat([
  { nombre: 'cl_listar_dte', verbo: 'GET', url: "'/documentos'" },
  { nombre: 'cl_login', verbo: 'POST', url: "'/usuarios/login'" },
  { nombre: 'cl_latido_usuario', verbo: 'HEAD', url: "'/usuarios/0'" },
  {
    nombre: 'cl_listar_permiso_usuario',
    verbo: 'GET',
    url: "'/permisos/' + SesionFebos().usuario.iut"
  },
  { nombre: 'cl_listar_empresas', verbo: 'GET', url: "'/empresas'" },
  { nombre: 'cl_generar_menu', verbo: 'GET', url: "'/menu'" },
  {
    nombre: 'cl_historial_eventos',
    verbo: 'GET',
    url: "'/logs/' + query.seguimientoId"
  },
  {
    nombre: 'cl_listar_flujos',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/flujos'"
  },
  {
    nombre: 'cl_anular_folios',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/caf/legacy'"
  },
  {
    nombre: 'cl_actualizar_empresa',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId"
  },
  {
    nombre: 'cl_agregar_aat',
    verbo: 'POST',
    url: "'/herramientas/automatizaciones'"
  },
  {
    nombre: 'cl_listar_aat',
    verbo: 'GET',
    url: "'/herramientas/automatizaciones'"
  },
  {
    nombre: 'cl_actualizar_aat',
    verbo: 'PUT',
    url: "'/herramientas/automatizaciones/' + query.accionAutomaticaId"
  },
  {
    nombre: 'cl_borrar_aat',
    verbo: 'DELETE',
    url: "'/herramientas/automatizaciones/' + query.accionAutomaticaId"
  },
  {
    nombre: 'cl_actualizar_token_permanente',
    verbo: 'POST',
    url: "'/usuarios/' + query.usuarioId + '/token'"
  },
  {
    nombre: 'cl_actualizar_usuario_proveedor',
    verbo: 'POST',
    url: "'/usuarios/' + query.usuarioId"
  },
  { nombre: 'cl_actualizar_usuarios_grupo', verbo: 'GET', url: '' },
  {
    nombre: 'cl_actualizar_usuario_proveedor',
    verbo: 'POST',
    url: "'/usuarios/' + query.usuarioId"
  },
  {
    nombre: 'cl_actualizar_usuarios_grupo',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/grupos/' + query.grupoId"
  },
  {
    nombre: 'cl_adjuntar_documento',
    verbo: 'PUT',
    url: "'/documentos/' + query.febosId"
  },
  {
    nombre: 'cl_agregar_path_lectura_flujo',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId + '/configuracion'"
  },
  {
    nombre: 'cl_asignar_empresa_usuario',
    verbo: 'POST',
    url: "'/usuarios/' + query.usuarioId + '/empresas'"
  },
  { nombre: 'cl_asignar_permisos', verbo: 'PUT', url: "'/permisos'" },
  {
    nombre: 'cl_asociar_contacto',
    verbo: 'POST',
    url: "'/documentos/' + query.febosId + '/contacto'"
  },
  {
    nombre: 'cl_buscar_proveedores',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/proveedores'"
  },
  {
    nombre: 'cl_cambiar_estado_flujo',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId + '/configuracion'"
  },
  { nombre: 'cl_cargar_dnt', verbo: 'PUT', url: "'/notributarios'" },
  {
    nombre: 'cl_crear_dte_desde_borrador',
    verbo: 'POST',
    url: "'/documentos/borrador/emitir'"
  },{
    nombre: 'cl_crear_dte',
    verbo: 'POST',
    url: "'/documentos'"
  },
  {
    nombre: 'cl_cargar_documento_electronico',
    verbo: 'PUT',
    url: "'/documentos'"
  },
  {
    nombre: 'cl_cargar_documento_manual',
    verbo: 'PUT',
    url: "'/documentos/manuales'"
  },
  {
    nombre: 'cl_cargar_caf',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/caf/legacy'"
  },
  {
    nombre: 'cl_cargar_caf_store',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/caf/legacy/cafstore'"
  },
  { nombre: 'cl_cargar_oc_mp', verbo: 'PUT', url: "'/mercadopublico'" },
  {
    nombre: 'cl_comentarios_bitacoras',
    verbo: 'PUT',
    url: "'/documentos/' + query.id + '/bitacora'"
  },
  {
    nombre: 'cl_configurar_portal_personalizado',
    verbo: 'POST',
    url: "'/configuracion/portales/{dominio}'"
  },
  {
    nombre: 'cl_consulta_estado_tarea_background',
    verbo: 'GET',
    url: "'/herramientas/tareas/' + query.tareaId"
  },
  {
    nombre: 'cl_consulta_fecha_recepcion_sii',
    verbo: 'GET',
    url: "'/sii/dte/consulta/recepcion'"
  },
  { nombre: 'cl_consumo_de_colas_masivo', verbo: 'GET', url: "''" },
  {
    nombre: 'cl_correo_envio_dte',
    verbo: 'POST',
    url: "'/documentos/' + query.febosId + '/envio'"
  },
  {
    nombre: 'cl_crea_info_pago',
    verbo: 'POST',
    url: "'/documentos/' + query.febosId + '/pagos'"
  },
  {
    nombre: 'cl_crear_cuenta',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/cuentas'"
  },
  { nombre: 'cl_crear_empresa', verbo: 'POST', url: "'/empresas'" },
  {
    nombre: 'cl_crear_grupo',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/grupos'"
  },
  {
    nombre: 'cl_crear_lista',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/listas'"
  },
  { nombre: 'cl_crear_menu', verbo: 'PUT', url: "'/menu'" },
  {
    nombre: 'cl_crear_proyecto',
    verbo: 'POST',
    url: "'/portafolio/proyectos'"
  },
  {
    nombre: 'cl_crear_regla_webhook',
    verbo: 'POST',
    url: "'/webhook/' + query.webhookId + '/reglas'"
  },
  {
    nombre: 'cl_crear_rol',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/roles'"
  },
  {
    nombre: 'cl_crear_sucursal',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/sucursales'"
  },
  { nombre: 'cl_crear_ticket_ayuda', verbo: 'POST', url: "'/tickets'" },
  {
    nombre: 'cl_crear_token_permanente',
    verbo: 'PUT',
    url: "'/usuarios/' + query.usuarioId + '/token'"
  },
  { nombre: 'cl_crear_usuario', verbo: 'PUT', url: "'/usuarios'" },
  { nombre: 'cl_crear_webhook', verbo: 'POST', url: "'/webhook'" },
  {
    nombre: 'cl_editar_novedad',
    verbo: 'PUT',
    url: "'/novedades/' + query.novedadId"
  },
  { nombre: 'cl_ejecuta_lambda_remoto', verbo: 'GET', url: "''" },
  {
    nombre: 'cl_ejecutar_flujo',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId"
  },
  {
    nombre: 'cl_elimina_tokens',
    verbo: 'DELETE',
    url: "'/usuarios/' + query.usuarioId + '/token'"
  },
  {
    nombre: 'cl_eliminar_cuenta',
    verbo: 'DELETE',
    url: "'/empresas/' + query.empresaId + '/cuentas'"
  },
  {
    nombre: 'cl_eliminar_flujo',
    verbo: 'DELETE',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId"
  },
  {
    nombre: 'cl_eliminar_grupo',
    verbo: 'DELETE',
    url: "'/empresas/' + query.empresaId + '/grupos/' + query.grupoId"
  },
  {
    nombre: 'cl_eliminar_lista',
    verbo: 'DELETE',
    url: "'/empresas/' + query.empresaId + '/listas/' + query.listaId"
  },
  { nombre: 'cl_eliminar_menu', verbo: 'DELETE', url: "'/menu/{menuid}'" },
  {
    nombre: 'cl_eliminar_path_lectura_flujo',
    verbo: 'DELETE',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId + '/configuracion'"
  },
  {
    nombre: 'cl_eliminar_rol',
    verbo: 'DELETE',
    url: "'/empresas/' + query.empresaId + '/roles/'+query.rolId"
  },
  {
    nombre: 'cl_eliminar_sucursal',
    verbo: 'DELETE',
    url: "'/empresas/' + query.empresaId + '/sucursales/' + query.sucursalId"
  },
  { nombre: 'cl_eliminar_tokens_caducados', verbo: 'DELETE', url: "''" },
  {
    nombre: 'cl_eliminar_usuario',
    verbo: 'DELETE',
    url: "'/usuarios/' + query.usuarioId"
  },
  {
    nombre: 'cl_eliminar_webhook',
    verbo: 'DELETE',
    url: "'/webhook/' + query.webhookId"
  },
  {
    nombre: 'cl_eliminar_webhooks_regla',
    verbo: 'DELETE',
    url: "'/webhook/' + query.webhookId + '/reglas'"
  },
  {
    nombre: 'cl_encola_acciones_masivas_unitarias',
    verbo: 'GET',
    url: "''"
  },
  {
    nombre: 'cl_encolar_tarea_background',
    verbo: 'POST',
    url: "'/herramientas/tareas'"
  },
  {
    nombre: 'cl_estadisticas',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/estadisticas'"
  },
  {
    nombre: 'cl_generador_reportes_dte',
    verbo: 'GET',
    url: "'/documentos/reportes'"
  },
  {
    nombre: 'cl_generar_archivo_cesion',
    verbo: 'POST',
    url: "'/sii/dte/cesion'"
  },
  {
    nombre: 'cl_reenviar_al_sii',
    verbo: 'GET',
    url: "'/sii/dte/reenviodocumento'"
  },
  {
    nombre: 'cl_recuperar_correo_sii',
    verbo: 'GET',
    url: "'/sii/dte/reenviocorreo'"
  },
  {
    nombre: 'cl_actualizar_compra_venta',
    verbo: 'PUT',
    url: "'/documentos/datos/transaccioncompraventa'"
  },
  {
    nombre: 'cl_generar_intercambio_electronico',
    verbo: 'PUT',
    url: "'/documentos/' + query.febosId + '/intercambio'"
  },
  {
    nombre: 'cl_guardar_archivo',
    verbo: 'POST',
    url: "'/configuracion/archivos'"
  },
  {
    nombre: 'cl_guardar_flujo',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId"
  },
  {
    nombre: 'cl_info_empresa',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId"
  },
  {
    nombre: 'cl_informacion_plugins_flujo',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/flujos/plugins'"
  },
  {
    nombre: 'cl_invitar_usuario_proveedor',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/proveedores'"
  },
  {
    nombre: 'cl_invocar_flujo',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId + '/ejecuciones'"
  },
  {
    nombre: 'cl_kpi',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/kpi'"
  },
  {
    nombre: 'cl_kpi_emisor',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/kpiemisor'"
  },
  {
    nombre: 'cl_listar_actividad_empresa',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/actividad'"
  },
  {
    nombre: 'cl_listar_actividades',
    verbo: 'GET',
    url: "'/usuarios/' + query.usuarioId + '/actividades'"
  },
  {
    nombre: 'cl_listar_adjuntos',
    verbo: 'GET',
    url: "'/documentos/adjuntos'"
  },
  {
    nombre: 'cl_listar_arbol_usuarios',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/usuarios'"
  },
  {
    nombre: 'cl_listar_bitacora_dnt',
    verbo: 'GET',
    url: "'/notributarios/' + query.febosId + '/bitacora'"
  },
  {
    nombre: 'cl_listar_bitacora_dte',
    verbo: 'GET',
    url: "'/documentos/' + query.febosId + '/bitacora'"
  },
  {
    nombre: 'cl_listar_cafs',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/caf/legacy'"
  },
  {
    nombre: 'cl_listar_cafs_store',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/caf/legacy/cafstore'"
  },
  {
    nombre: 'cl_listar_cafs_store_delivery',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/caf/legacy/cafstore/' + query.cafId"
  },
  {
    nombre: 'cl_listar_comunas',
    verbo: 'GET',
    url: "'/regiones/' + query.regionid + '/provincias/' + query.provinciaid + '/comunas'"
  },
  {
    nombre: 'cl_listar_configuraciones_de_flujo',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId + '/configuracion'"
  },
  {
    nombre: 'cl_listar_cuentas',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/cuentas'"
  },
  { nombre: 'cl_listar_dnt', verbo: 'GET', url: "'/notributarios'" },
  {
    nombre: 'cl_listar_ejecuciones_flujo',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId + '/ejecuciones'"
  },
  {
    nombre: 'cl_listar_elementos_lista',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/listas/' + query.listaId"
  },
  {
    nombre: 'cl_listar_empresas_canales',
    verbo: 'GET',
    url: "'/empresas/canales'"
  },
  {
    nombre: 'cl_listar_empresas_usuario',
    verbo: 'GET',
    url: "'/usuarios/' + query.usuarioId + '/empresas'"
  },
  {
    nombre: 'cl_listar_folios',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/folios/legacy'"
  },
  {
    nombre: 'cl_listar_grupos',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/grupos'"
  },
  {
    nombre: 'cl_listar_info_pago',
    verbo: 'GET',
    url: "'/documentos/pagos'"
  },
  { nombre: 'cl_listar_lambdas', verbo: 'GET', url: "''" },
  {
    nombre: 'cl_listar_listas',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/listas'"
  },
  { nombre: 'cl_listar_novedades', verbo: 'GET', url: "'/novedades'" },
  { nombre: 'cl_listar_oc_mp', verbo: 'GET', url: "'/mercadopublico'" },
  { nombre: 'cl_listar_permisos', verbo: 'GET', url: "'/permisos'" },
  {
    nombre: 'cl_listar_unidades_tecnicas',
    verbo: 'GET',
    url: "'/portafolio/unidades'"
  },
  { nombre: 'cl_crear_unidades_tecnicas', verbo: 'POST', url: "'/portafolio/unidades'" },
  {
    nombre: 'cl_listar_permisos_rol',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/roles/'+ query.rolId + '/permisos'"
  },
  {
    nombre: 'cl_listar_portales_personalizados',
    verbo: 'GET',
    url: "'/configuracion/portales'"
  },
  {
    nombre: 'cl_listar_provincias',
    verbo: 'GET',
    url: "'/regiones/' + query.regionid + '/provincias'"
  },
  {
    nombre: 'cl_listar_proyectos',
    verbo: 'GET',
    url: "'/portafolio/proyectos'"
  },
  {
    nombre: 'cl_listar_proyectos_cabecera',
    verbo: 'GET',
    url: "'/portafolio/proyectos/cabeceras'"
  },
  {
    nombre: 'cl_listar_boleta_garantia',
    verbo: 'GET',
    url: "'/portafolio/boletas'"
  },
  {
    nombre: 'cl_listar_polizas',
    verbo: 'GET',
    url: "'/portafolio/polizas'"
  },
  {
    nombre: 'cl_listar_receptores_electronicos',
    verbo: 'GET',
    url: "'/receptores/receptoreselectronicos'"
  },
  {
    nombre: 'cl_listar_referencias',
    verbo: 'GET',
    url: "'/documentos/' + query.febosId + '/referencias'"
  },
  { nombre: 'cl_listar_regiones', verbo: 'GET', url: "'/regiones'" },
  {
    nombre: 'cl_listar_roles',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/roles'"
  },
  {
    nombre: 'cl_listar_roles_usuario',
    verbo: 'GET',
    url: "'/usuarios/' + query.usuarioId + '/roles'"
  },
  { nombre: 'cl_listar_states', verbo: 'GET', url: "'/menu/estados'" },
  {
    nombre: 'cl_listar_sucursales',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/sucursales'"
  },
  {
    nombre: 'cl_listar_suscripcion_usuario',
    verbo: 'GET',
    url: "'/webhook/suscripciones/usuario'"
  },
  {
    nombre: 'cl_listar_suscripciones_webhook',
    verbo: 'GET',
    url: "'/webhook/' + query.webhookId + '/suscripciones'"
  },
  {
    nombre: 'cl_listar_tokens',
    verbo: 'GET',
    url: "'/usuarios/' + query.usuarioId + '/token'"
  },
  { nombre: 'cl_listar_usuarios', verbo: 'GET', url: "'/usuarios'" },
  {
    nombre: 'cl_listar_bitacora_portafolio',
    verbo: 'GET',
    url: "'/portafolio/boletas/'+query.documentoId+'/bitacora'"
  },
  { nombre: 'cl_listar_webhooks', verbo: 'GET', url: "'/webhook'" },
  {
    nombre: 'cl_listar_webhooks_reglas',
    verbo: 'GET',
    url: "'/webhook/' + query.webhookId + '/reglas'"
  },
  { nombre: 'cl_mailgun2s3', verbo: 'POST', url: "''" },
  {
    nombre: 'cl_modificar_cabecera_lista',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/listas/' + query.listaId"
  },
  {
    nombre: 'cl_modificar_configuracion',
    verbo: 'PUT',
    url: "'/configuracion'"
  },
  {
    nombre: 'cl_modificar_contrasena',
    verbo: 'PUT',
    url: "'/usuarios/login/recuperar'"
  },
  {
    nombre: 'cl_modificar_grupo',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/grupos/' + query.grupoId"
  },
  {
    nombre: 'cl_modificar_info_pago',
    verbo: 'PUT',
    url: "'/documentos/' + query.febosId + '/pagos'"
  },
  {
    nombre: 'cl_modificar_lista',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/listas/' + query.listaId"
  },
  { nombre: 'cl_modificar_menu', verbo: 'POST', url: "'/menu/{menuid}'" },
  {
    nombre: 'cl_modificar_permisos_rol',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/roles/' + query.rolId + '/permisos'"
  },
  {
    nombre: 'cl_modificar_regla_webhook',
    verbo: 'PUT',
    url: "'/webhook/' + query.webhookId + '/reglas'"
  },
  {
    nombre: 'cl_modificar_rol',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/roles/'+ query.rolId"
  },
  {
    nombre: 'cl_modificar_roles_usuario',
    verbo: 'POST',
    url: "'/usuarios/' + query.usuarioId + '/roles'"
  },
  {
    nombre: 'cl_modificar_sucursal',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/sucursales/' + query.sucursalId"
  },
  {
    nombre: 'cl_modificar_suscripcion_webhook',
    verbo: 'POST',
    url: "'/webhook/' + query.webhookId + '/suscripciones'"
  },
  {
    nombre: 'cl_modificar_usuario',
    verbo: 'PUT',
    url: "'/usuarios/' + query.usuarioId"
  },
  {
    nombre: 'cl_modificar_webhook',
    verbo: 'POST',
    url: "'/webhook/' + query.webhookId"
  },
  { nombre: 'cl_nueva_novedad', verbo: 'POST', url: "'/novedades'" },
  {
    nombre: 'cl_obtener_adjunto',
    verbo: 'GET',
    url: "'/documentos/adjunto'"
  },
  {
    nombre: 'cl_obtener_adjunto_bitacora',
    verbo: 'GET',
    url: "'/documentos/'+ query.documentoId +'/bitacora/adjunto'"
  },
  {
    nombre: 'cl_obtener_archivo',
    verbo: 'GET',
    url: "'/configuracion/archivos'"
  },
  {
    nombre: 'cl_obtener_certificado',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/certificado'"
  },
  {
    nombre: 'cl_obtener_configuracion',
    verbo: 'GET',
    url: "'/configuracion'"
  },
  {
    nombre: 'cl_obtener_dnt',
    verbo: 'GET',
    url: "'/notributarios/' + query.febosId"
  },
  {
    nombre: 'cl_obtener_documento',
    verbo: 'GET',
    url: "'/documentos/' + query.febosId"
  },
  {
    nombre: 'cl_obtener_documento_test',
    verbo: 'GET',
    url: "'/configuracion/configuraciones/pruebas/plantilla'"
  },
  {
    nombre: 'cl_obtener_flujo',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId"
  },
  {
    nombre: 'cl_obtener_resumen_ejecucion_flujo',
    verbo: 'GET',
    url:
      "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId + '/ejecuciones/' + query.ejecucionId"
  },
  {
    nombre: 'cl_adm_configuracion_listar',
    verbo: 'GET',
    url: "'/configuracion/configuraciones'"
  },
  {
    nombre: 'cl_adm_configuracion_guardar',
    verbo: 'POST',
    url: "'/configuracion/configuraciones'"
  },
  {
    nombre: 'cl_portal_web_config',
    verbo: 'GET',
    url: "'/configuracion/portales/{dominio}'"
  },
  {
    nombre: 'cl_realizar_accion_masiva',
    verbo: 'POST',
    url: "'/herramientas/masivas'"
  },
  {
    nombre: 'cl_recuperar_clave',
    verbo: 'POST',
    url: "'/usuarios/login/recuperar'"
  },
  {
    nombre: 'cl_reencolar_tarea_background',
    verbo: 'POST',
    url: "'/herramientas/tareas'"
  },
  {
    nombre: 'cl_reenviar_invitacion_proveedor',
    verbo: 'PUT',
    url: "'/usuarios/invitar'"
  },
  {
    nombre: 'cl_sii_consulta_dte',
    verbo: 'GET',
    url: "'/sii/dte/consulta'"
  },
  {
    nombre: 'cl_sii_consulta_historial_eventos',
    verbo: 'GET',
    url: "'/sii/dte/eventos'"
  },
  { nombre: 'cl_sii_obtener_token', verbo: 'PUT', url: "''" },
  {
    nombre: 'cl_sii_solicitar_correo',
    verbo: 'PUT',
    url: "'/sii/dte/reenviocorreo'"
  },
  {
    nombre: 'cl_solicitar_dte_no_recibido',
    verbo: 'PUT',
    url: "'/documentos/'+ query.febosId +'/solicitudes'"
  },
  {
    nombre: 'cl_subir_certificado',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/certificado'"
  },
  {
    nombre: 'cl_ver_novedad',
    verbo: 'GET',
    url: "'/novedades/' + query.novedadId"
  },
  {
    nombre: 'cl_clave_unica',
    verbo: 'POST',
    url: "'/herramientas/webhooks/claveunica'"
  },
  {
    nombre: 'cl_obtener_indicadores_gestion',
    verbo: 'GET',
    url: "'/documentos/indicadoresgestion'"
  },
  {
    nombre: 'cl_obtener_indicadores_emision',
    verbo: 'GET',
    url: "'/documentos/indicadoresemision'"
  },
  {
    nombre: 'cl_obtener_url_prefirmada',
    verbo: 'GET',
    url: "'/herramientas/archivos'"
  },
  {
    nombre: 'cl_obtener_proyecto',
    verbo: 'GET',
    url: "'/portafolio/proyectos/'+query.proyectoId"
  },
  {
    nombre: 'cl_correo_envio_portafolio',
    verbo: 'POST',
    url: "'/portafolio/acciones/envio'"
  },
  {
    nombre: 'cl_listar_bitacora_portafolio',
    verbo: 'GET',
    url: "'/portafolio/boletas/'+query.documentoId+'/bitacora'"
  },
  {
    nombre: 'cl_obtener_documento_portafolio',
    verbo: 'GET',
    url: "'/portafolio/boletas/' + query.documentoId"
  },
  {
    nombre: 'cl_crear_documento_portafolio',
    verbo: 'POST',
    url: "'/portafolio/boletas/'"
  },
  {
    nombre: 'cl_obtener_archivo_privado',
    verbo: 'GET',
    url: "'/herramientas/archivos/privados'"
  },
  {
    nombre: 'io_rss',
    verbo: 'GET',
    url: "'/herramientas/rss?url=' + query.url"
  },
  { nombre: 'cl_listar_webhook', verbo: 'GET', url: "'/webhook'" },

  {
    nombre: 'cl_copiar_roles_usuario_a_usuarios',
    verbo: 'POST',
    url: "'/usuarios/roles'"
  },
  { nombre: 'cl_noticias_listado', verbo: 'GET', url: "'/noticias'" },
  { nombre: 'cl_noticias_crear', verbo: 'POST', url: "'/noticias'" },
  { nombre: 'cl_noticias_eliminar', verbo: 'DELETE', url: "'/noticias'" },
  { nombre: 'cl_crear_partner', verbo: 'POST', url: "'/partners'" },
  { nombre: 'cl_listar_partners', verbo: 'GET', url: "'/partners'" },
  { nombre: 'cl_migrar_empresas_entre_ambientes', verbo: 'PUT', url: "'/empresas/migracion'" },
  { nombre: 'cl_actualizar_partner', verbo: 'PUT', url: "'/partners'" },
  { nombre: 'cl_migracion_documentos_entre_ambientes', verbo: 'PUT', url: "'/documentos/migracion'" },
  { nombre: 'cl_actualizar_partner', verbo: 'PUT', url: "'/partners'" },
  {
      nombre: 'cl_caf_listar_folios',
      verbo: 'GET',
      url: "'/empresas/' + query.empresaId + '/folios'"
  },
  {
      nombre: 'cl_caf_anular_folios',
      verbo: 'POST',
      url: "'/empresas/' + query.empresaId + '/caf'"
  },
  {
      nombre: 'cl_caf_cargar_caf',
      verbo: 'PUT',
      url: "'/empresas/' + query.empresaId + '/caf'"
  },
  {
      nombre: 'cl_caf_cargar_caf_store',
      verbo: 'PUT',
      url: "'/empresas/' + query.empresaId + '/cafstore'"
  },
  {
      nombre: 'cl_caf_listar_cafs',
      verbo: 'GET',
      url: "'/empresas/' + query.empresaId + '/caf'"
  },
  {
      nombre: 'cl_caf_listar_cafs_store',
      verbo: 'GET',
      url: "'/empresas/' + query.empresaId + '/cafstore'"
  },
  {
      nombre: 'cl_caf_listar_cafs_store_delivery',
      verbo: 'GET',
      url: "'/empresas/' + query.empresaId + '/cafstore/' + query.cafId"
  },
  {
      nombre: 'cl_caf_listar_folios',
      verbo: 'GET',
      url: "'/empresas/' + query.empresaId + '/folios'"
  },
  {
    nombre: 'cl_listar_referencias_dnt',
    verbo: 'GET',
    url: "'/documentos/' + query.febosId + '/referenciasdnt'"
  },
  { nombre: 'cl_crear_caja', verbo: 'POST', url: "'/empresas/' + query.empresaId + '/sucursales/' + query.sucursalId + '/cajas'" },
  { nombre: 'cl_modificar_caja', verbo: 'PUT', url: "'/empresas/' + query.empresaId + '/sucursales/' + query.sucursalId + '/cajas/' + query.cajaId" },
  { nombre: 'cl_eliminar_caja', verbo: 'DELETE', url: "'/empresas/' + query.empresaId + '/sucursales/' + query.sucursalId + '/cajas/' + query.cajaId" },
  { nombre: 'cl_obtener_caja', verbo: 'GET', url: "'/empresas/' + query.empresaId + '/sucursales/' + query.sucursalId + '/cajas/' + query.cajaId" },
  { nombre: 'cl_listar_cajas', verbo: 'GET', url: "'/empresas/' + query.empresaId + '/sucursales/' + query.sucursalId + '/cajas'" }
]
);

    //crear funciones dinamicamente
    for (var i = 0; i < lambdas.length; i++) {
      this[lambdas[i].nombre] = generarFuncionParaLlamadaAPI(
        lambdas[i].nombre,
        lambdas[i].verbo,
        lambdas[i].url
      );
    }
    return this;
  }
]);
febosApp.factory('FebosQueueAPI', [
  '$q',
  'FebosAPI',
  '$rootScope',
  'ContextoFebos',
  '$localStorage',
  function($q, FebosAPI, $rootScope, ContextoFebos, $localStorage) {
    $localStorage.cola = [];
    var verificar = function() {
      if ($localStorage.cola.length > 0) {
        ejecutarSiguiente();
      }
    };

    var ejecutarSiguiente = function() {
      var tarea = $localStorage.cola.shift($localStorage.cola);
      console.log('Ejecutando llamada ajax encolada: ' + tarea.lambda);
      FebosAPI[tarea.lambda](tarea.query, tarea.body, false, false).then(
        function(data) {
          tarea.d.resolve(data);
          if ($localStorage.cola.length > 0) {
            ejecutarSiguiente();
          }
        },
        function(err) {
          tarea.d.reject(err);
          $localStorage.cola.shift($localStorage.cola);
          if ($localStorage.cola.length > 0) {
            ejecutarSiguiente();
          }
        }
      );
    };
    return function(lambda, query, body) {
      var d = $q.defer();
      $localStorage.cola.push({
        lambda: lambda,
        query: query,
        body: body,
        d: d
      });
      if ($localStorage.cola.length > 0) {
        ejecutarSiguiente();
      }
      return d.promise;
    };
  }
]);
febosApp.factory('SesionFebos', [
  'ContextoFebos',
  function(ContextoFebos) {
    return function() {
      //console.log("SESION",ContextoFebos.sesiones[ContextoFebos.usuarioActual]);
      return ContextoFebos.sesiones[ContextoFebos.usuarioActual];
    };
  }
]);

febosApp.factory('Autorizacion', [
  '$rootScope',
  '$location',
  'SesionFebos',
  'ContextoFebos',
  function($rootScope, $location, SesionFebos, ContextoFebos) {
    return {
      verificar: function() {
        if (ContextoFebos.usuarioActual == '' || SesionFebos().usuario.ultimoPingValido == null) {
          if (typeof SesionFebos() != 'undefined') {
            SesionFebos().usuario.ultimoPingValido = null;
            SesionFebos().usuario.token = null;
          }
          console.log('Usuario no autenticado, redireccionando [A]');
          $rootScope.logout();
        } else {
          var diaEnMilis = 86400000;
          var horaEnMilis = 3600000;
          var minutoEnMilis = 60000;
          var tiempoMaximoDePermanenciaEnMinutos = 14; //minutos

          var ahora = new Date();
          var diferenciaEnMilis = ahora - new Date(SesionFebos().usuario.ultimoPingValido);
          var diferenciaEnMinutos = Math.round(
            ((diferenciaEnMilis % diaEnMilis) % horaEnMilis) / minutoEnMilis
          );
          if (diferenciaEnMinutos >= tiempoMaximoDePermanenciaEnMinutos) {
            console.log('Usuario no autenticado, redireccionando  [B]');
            $rootScope.logout();
          }
        }
      }
    };
  }
]);

febosApp.factory('ServicioDocumentos', function(
  $http,
  $rootScope,
  SesionFebos,
  FebosAPI,
  FebosUtil
) {
  return {
    mostrarMensajeCarga: function(mensaje) {
      $rootScope.block = UIkit.modal.blockUI(
        "<div class='uk-text-center'>" +
          mensaje +
          "<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
    },
    ocultarMensajeCarga: function() {
      $rootScope.block.hide();
    },
    inicializarFiltros: function(config) {
      for (var i = 0; i < config.campos.length; i++) {
        if (
          config.campos[i].filtro &&
          typeof config.filtros[config.campos[i].nombre] === 'undefined'
        ) {
          var tipoFiltro = config.campos[i].tipo;
          switch (tipoFiltro) {
            case 'fecha':
              config.filtros[config.campos[i].nombre] = {
                desde: '',
                hasta: ''
              };
              break;
            case 'array':
              config.filtros[config.campos[i].nombre] = [];
              break;
            default:
              config.filtros[config.campos[i].nombre] = [];
          }
        }
      }
    },
    construirFiltros: function(filtrosSucios, config) {
      var filtrosLimpios = [];
      for (var key in config.filtrosEstaticos) {
        filtrosSucios[key] = config.filtrosEstaticos[key];
      }
      for (var key in filtrosSucios) {
        var filtro = '';
        if (filtrosSucios[key].constructor.toLocaleString().indexOf('Array') >= 0) {
          if (filtrosSucios[key].length > 0 && typeof filtrosSucios[key][0].text === 'undefined') {
            filtro = key + ':' + filtrosSucios[key].join(',');
          } else {
            var opciones = [];
            for (var i = 0; i < filtrosSucios[key].length; i++) {
              opciones.push(filtrosSucios[key][i].text);
            }
            filtro = key + ':' + opciones.join(',');
          }
        } else if (filtrosSucios[key].constructor.toLocaleString().indexOf('Object') >= 0) {
          filtro = key + ':' + filtrosSucios[key].desde + '--' + filtrosSucios[key].hasta;
        } else if (filtrosSucios[key] != '') {
          filtro = key + ':' + filtrosSucios[key];
        }
        if (filtro.split(':')[1].length > 0 && filtro.split(':')[1] != '--') {
          filtrosLimpios.push(filtro);
        } else {
          if (typeof config.filtrosPorDefecto[key] !== 'undefined') {
            filtrosLimpios.push(key + ':' + config.filtrosPorDefecto[key]);
          }
        }
      }
      return filtrosLimpios.join('|');
    },
    construirCampos: function(config) {
      var campos = [];
      for (var i = 0; i < config.campos.length; i++) {
        campos.push(config.campos[i].nombre);
      }
      return campos.join(',');
    },
    descargarAdjuntoBitacora: function(idBitacora, febosId) {
      console.log('DESCARGANDO ADJUNTO DE BITACORA');
      var ServicioDocumentos = this;
      console.log(idBitacora);
      console.log(febosId);
      ServicioDocumentos.mostrarMensajeCarga('Preparando descarga de archivo adjunto...');
      FebosAPI['cl' + '_obtener_adjunto_bitacora'](
        {
          link: 'si',
          idBitacora: idBitacora
        },
        {},
        true,
        false
      ).then(function(response) {
        ServicioDocumentos.ocultarMensajeCarga();
        console.log(response);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            window.open(response.data.archivo);
          } else {
            console.log(response);
          }
        } catch (e) {
          console.log(e);
        }
      });
    },
    descargarAdjunto: function(documentoId, dteArchivoId) {
      console.log('DESCARGANDO ADJUNTO ');
      var ServicioDocumentos = this;
      ServicioDocumentos.mostrarMensajeCarga('Preparando descarga de archivo adjunto...');
      FebosAPI['cl' + '_obtener_adjunto'](
        {
          idArchivo: dteArchivoId,
          link: 'si'
        },
        {},
        true,
        false
      ).then(function(response) {
        ServicioDocumentos.ocultarMensajeCarga();
        console.log(response);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;

        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            window.open(response.data.archivo);
          } else {
            console.log(response);
          }
        } catch (e) {
          console.log(e);
        }
      });
    },
    calcularPaginas: function(config) {
      var margen = 4;
      config.paginas = [];
      var inicio = parseInt(config.paginaActual) - margen;
      var fin = parseInt(config.paginaActual) + margen;
      console.log('inicio', inicio);
      console.log('fin', fin);
      if (inicio < 1) inicio = 1;
      if (fin > config.totalPaginas) fin = config.totalPaginas;
      for (var i = inicio; i <= fin; i++) {
        config.paginas.push({ numeroPagina: i });
      }
      console.log(config.paginas);
    },
    listarDocumentos: function(config) {
      $rootScope.url = window.location.href;
      this.inicializarFiltros(config);
      var that = this;
      console.log(config);
      config.cargando = true;
      config.documentos = new Array();
      //filtro especial por rut
      if (config.ambito == 'recibidos') {
        config.filtros.rutReceptor = [SesionFebos().empresa.iut];
        //config.filtros.rutEmisor = [];//($scope.filtros.tagsRuts.length > 0 ? 'rutEmisor:' + rutsArray.join(",") : '');
      } else {
        config.filtros.rutEmisor = [SesionFebos().empresa.iut];
        //config.filtros.rutReceptor = [];//($scope.filtros.tagsRuts.length > 0 ? 'rutReceptor:' + rutsArray.join(",") : '');
      }
      config.totalPaginas = 0;
      if (config.origen == 'cedidos') {
        if (req.params.filtros.search('rutCesionario') < 0) {
          req.params.filtros = req.params.filtros + '|rutCesionario$-';
        }
      }
      req.params.filtros = req.params.filtros.replace('razonSocialEmisor:', 'razonSocialEmisor$');
      req.params.filtros = req.params.filtros.replace(
        'razonSocialReceptor:',
        'razonSocialReceptor$'
      );
      req.params.filtros = req.params.filtros.replace('emisorRazonSocial:', 'emisorRazonSocial$');
      req.params.filtros = req.params.filtros.replace(
        'receptorRazonSocial:',
        'receptorRazonSocial$'
      );

      FebosAPI['cl' + '_obtener_documento'](
        {
          pagina: config.pagina,
          itemsPorPagina: config.registrosPorPagina,
          campos: this.construirCampos(config),
          filtros: this.construirFiltros(config.filtros, config),
          orden: config.orden.join(',')
        },
        {},
        true,
        false
      ).then(function(response) {
        config.cargando = false;
        console.log('DENTRO DEL SERVICIO', response);
        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            config.documentos = response.data.documentos;
            config.totalElementos = response.data.totalElementos;
            config.totalPaginas = response.data.totalPaginas;
            config.paginaActual = response.data.paginaActual;
            config.documentosPorPagina = response.data.documentos.length;
            that.calcularPaginas(config);
          } else {
            console.log(response);
          }
        } catch (e) {
          console.log(e);
        }
      });
    },
    listarDocumentosPaginacion: function(config, cantRegistros) {
      $rootScope.url = window.location.href;
      this.inicializarFiltros(config);
      var that = this;
      console.log(config);
      config.cargando = true;
      config.documentos = new Array();
      //filtro especial por rut
      if (config.ambito == 'recibidos') {
        config.filtros.rutReceptor = [SesionFebos().empresa.iut];
        //config.filtros.rutEmisor = [];//($scope.filtros.tagsRuts.length > 0 ? 'rutEmisor:' + rutsArray.join(",") : '');
      } else {
        config.filtros.rutEmisor = [SesionFebos().empresa.iut];
        //config.filtros.rutReceptor = [];//($scope.filtros.tagsRuts.length > 0 ? 'rutReceptor:' + rutsArray.join(",") : '');
      }
      config.totalPaginas = 0;
      console.log('origen: ' + config.origen);

      if (config.origen == 'cedidos') {
        req.params.filtros = req.params.filtros.replace('rutCesionario:-', 'rutCesionario$-');
      }
      req.params.filtros = req.params.filtros.replace('razonSocialEmisor:', 'razonSocialEmisor$');
      req.params.filtros = req.params.filtros.replace(
        'razonSocialReceptor:',
        'razonSocialReceptor$'
      );
      req.params.filtros = req.params.filtros.replace('emisorRazonSocial:', 'emisorRazonSocial$');
      req.params.filtros = req.params.filtros.replace(
        'receptorRazonSocial:',
        'receptorRazonSocial$'
      );

      FebosAPI['cl' + '_obtener_documento'](
        {
          pagina: config.pagina,
          itemsPorPagina: cantRegistros,
          campos: this.construirCampos(config),
          filtros: this.construirFiltros(config.filtros, config),
          orden: config.orden.join(',')
        },
        {},
        true,
        false
      ).then(function(response) {
        config.cargando = false;
        console.log('DENTRO DEL SERVICIO', response);
        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            config.documentos = response.data.documentos;
            config.totalElementos = response.data.totalElementos;
            config.totalPaginas = response.data.totalPaginas;
            config.paginaActual = response.data.paginaActual;
            config.documentosPorPagina = response.data.documentos.length;
            that.calcularPaginas(config);
          } else {
            console.log(response);
          }
        } catch (e) {
          console.log(e);
        }
      });
    },
    listarDocumentosNoTributarios: function(config) {
      $rootScope.url = window.location.href;
      var that = this;

      this.inicializarFiltros(config);
      console.log(config);
      config.cargando = true;
      config.documentos = new Array();
      //filtro especial por rut
      if (config.ambito == 'recibidos') {
        config.filtros.receptorRut = [SesionFebos().empresa.iut];
        //config.filtros.rutEmisor = [];//($scope.filtros.tagsRuts.length > 0 ? 'rutEmisor:' + rutsArray.join(",") : '');
      } else {
        config.filtros.emisorRut = [SesionFebos().empresa.iut];
        //config.filtros.rutReceptor = [];//($scope.filtros.tagsRuts.length > 0 ? 'rutReceptor:' + rutsArray.join(",") : '');
      }

      FebosAPI['cl' + '_listar_dnt'](
        {
          pagina: config.pagina,
          itemsPorPagina: config.registrosPorPagina,
          campos: this.construirCampos(config),
          filtros: this.construirFiltros(config.filtros, config),
          orden: config.orden.join(',')
        },
        {},
        true,
        false
      ).then(function(response) {
        config.cargando = false;
        console.log('DENTRO DEL SERVICIO', response);
        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            config.documentos = response.data.documentos;
            config.totalElementos = response.data.totalElementos;
            config.totalPaginas = response.data.totalPaginas;
            config.paginaActual = response.data.paginaActual;
            config.documentosPorPagina = response.data.documentos.length;
            that.calcularPaginas(config);
          } else {
            console.log(response);
          }
        } catch (e) {
          console.log(e);
        }
      });
    },

    enviaTareaCsvDte: function(config) {
      var payload = {
        pagina: 1,
        itemsPorPagina: 999999999,
        campos: this.construirCampos(config),
        filtros: this.construirFiltros(config.filtros, config),
        orden: config.orden.join(',')
      };

      console.log('payload');
      console.log(payload);

      var payload64 = btoa(unescape(encodeURIComponent(JSON.stringify(payload))));

      FebosAPI['cl' + '_encolar_tarea_background'](
        {
          proceso: 'cl' + '_listar_dte'
        },
        {
          payload: payload64
        },
        true,
        false
      ).then(function(response) {
        console.log('response enviaTareaCsvDte');
        console.log(response);
        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            config.tareaId = response.data.tareaId;
          } else {
            console.log(response);
          }
        } catch (e) {
          console.log(e);
        }
      });
    },
    enviaTareaCsvDnt: function(config) {
      var payload = {
        pagina: 1,
        itemsPorPagina: 999999999,
        campos: this.construirCampos(config),
        filtros: this.construirFiltros(config.filtros, config),
        orden: config.orden.join(',')
      };

      console.log('payload');
      console.log(payload);

      var payload64 = btoa(unescape(encodeURIComponent(JSON.stringify(payload))));

      FebosAPI['cl' + '_encolar_tarea_background'](
        {
          proceso: 'cl' + '_listar_dnt'
        },
        {
          payload: payload64
        },
        true,
        false
      ).then(function(response) {
        console.log('response enviaTareaCsvDnt');
        console.log(response);
        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            config.tareaId = response.data.tareaId;
          } else {
            console.log(response);
          }
        } catch (e) {
          console.log(e);
        }
      });
    },
    preguntaPorTarea: function(config) {
      //"/herramientas/tareas/" + config.tareaId
      FebosAPI['cl' + '_consulta_estado_tarea_background'](
        {
          tareaId: 'config.tareaId'
        },
        {},
        true,
        false
      ).then(function(response) {
        console.log('response preguntaPorTarea');
        console.log(response);
        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            config.porcentajeExportar = response.data.avance;
            config.estadoExportar = response.data.estado;
            config.resultadoExportar = response.data.resultado;
          } else {
            console.log(response);
          }
        } catch (e) {
          console.log(e);
        }
      });
    },
    interpretarValores: function(documento) {
      //plazo
      documento.plazo = documento.plazo
        ? documento.plazo < 0
          ? 'Vencido'
          : documento.plazo + ' días'
        : '';

      //monto total
      documento.montoTotal = documento.montoTotal
        ? documento.montoTotal.toLocaleString('de-DE')
        : '';

      //totalPrecioTotalNeto
      documento.totalPrecioTotalNeto = documento.totalPrecioTotalNeto
        ? documento.totalPrecioTotalNeto.toLocaleString('de-DE')
        : '';

      //tipo documento
      var tp = '';
      switch (documento.tipoDocumento) {
        case 33:
          tp = 'Factura Afecta Electrónica';
          break;
        case 34:
          tp = 'Factura Exenta Electrónica';
          break;
        case 39:
          tp = 'Boleta Electrónica';
          break;
        case 41:
          tp = 'Boleta Exenta Electrónica';
          break;
        case 43:
          tp = 'Factura Liquidación Electrónica';
          break;
        case 46:
          tp = 'Factura Compra Electrónica';
          break;
        case 52:
          tp = 'Guía de Depacho Electrónica';
          break;
        case 56:
          tp = 'Nota de Débito Electrónica';
          break;
        case 61:
          tp = 'Nota de Crédito Electrónica';
          break;
        case 66:
          tp = 'Boleta de Honorarios Electrónica';
          break;
        case 110:
          tp = 'Factura Exportación Electrónica';
          break;
        case 111:
          tp = 'Nota de Débito Exportación Electrónica';
          break;
        case 112:
          tp = 'Nota de Crédito Exportación Electrónica';
          break;
        default:
          tp = documento.tipoDocumento;
      }
      documento.tipoDocumento = tp + ' (' + documento.tipoDocumento + ')';

      //estado comercial
      tp = '';
      switch (documento.estadoComercial) {
        case 1:
          tp = 'ACEPTADO';
          break;
        case 2:
          tp = 'ACEPTADO EN EL SII';
          break;
        case '3':
          tp = 'PRE-RECHAZADO';
          break;
        case 4:
          tp = 'RECHAZADO EN EL SII';
          break;
        case 5:
          tp = 'RECLAMO PARCIAL EN EL SII';
          break;
        case 6:
          tp = 'RECLAMO TOTAL EN EL SII';
          break;
        case 7:
          tp = 'RECIBO MERCADERIAS EN EL SII';
          break;
        default:
          tp = 'SIN ACCIÓN COMERCIAL';
      }
      documento.estadoComercial = tp;

      //formaDePago
      tp = '';
      switch (documento.formaDePago) {
        case 1:
          tp = 'CONTADO';
          break;
        case 2:
          tp = 'CRÉDITO';
          break;
        case 3:
          tp = 'GRATIS';
          break;
        default:
          tp = 'SIN INFORMACIÓN';
      }
      documento.formaDePago = tp;

      //codigoSii
      tp = '';
      switch (documento.codigoSii) {
        case 'FAN':
          tp = 'ANULADO';
          break;
        case 'TMD':
          tp = 'TEXTO MODIFICADO (N.D.)';
          break;
        case 'TMC':
          tp = 'TEXTO MODIFICADO (N.C.)';
          break;
        case 'MMD':
          tp = 'MONTO MODIFICADO (N.D.)';
          break;
        case 'MMC':
          tp = 'MONTO MODIFICADO (N.C.)';
          break;
        case 'AND':
          tp = 'ANULADO (N.D.)';
          break;
        case 'ANC':
          tp = 'ANULADO (N.C.)';
          break;
        default:
          tp = '';
      }
      documento.codigoSii = tp;

      //tipo
      tp = '';
      switch (documento.tipo) {
        case 1:
          tp = 'Compromiso';
          break;
        case 2:
          tp = 'Enviado a pago';
          break;
        case 3:
          tp = 'Pagado';
          break;
        case 4:
          tp = 'Cobrado';
          break;
        case 5:
          tp = 'No Pagado';
          break;
        case '801':
          tp = 'Orden de Compra (801)';
          break;
        case 'HES':
          tp = 'Hoja de Entrega de Servicio (HES)';
          break;
        case 'HEM':
          tp = 'Hoja de Entrega de Materiales (HEM)';
          break;
        case 'MP':
          tp = 'OC Mercado Público (MP)';
          break;
        default:
          tp = '';
      }
      documento.tipo = tp;

      //medio
      tp = '';
      switch (documento.medio) {
        case 'CH':
          tp = 'Cheque al día';
          break;
        case 'CF':
          tp = 'Cheque a fecha';
          break;
        case 'LT':
          tp = 'Letra';
          break;
        case 'EF':
          tp = 'Efectivo';
          break;
        case 'PE':
          tp = 'Transferencia';
          break;
        case 'TC':
          tp = 'Tarjeta Crédito';
          break;
        case 'VV':
          tp = 'Vale Vista';
          break;
        case 'CO':
          tp = 'Confirming';
          break;
        case 'OT':
          tp = 'Otro';
          break;
        default:
          tp = '';
      }
      documento.medio = tp;
      //medio
      tp = '';
      switch (documento.indicadorDeTraslado) {
        case 1:
          tp = 'OPERACIÓN CONSTITUYE VENTA';
          break;
        case 2:
          tp = 'VENTAS POR EFECTUAR';
          break;
        case 3:
          tp = 'CONSIGNACIONES';
          break;
        case 4:
          tp = 'ENTREGA GRATUITA';
          break;
        case 5:
          tp = 'TRASLADOS INTERNOS';
          break;
        case 6:
          tp = 'OTROS TRASLADOS';
          break;
        case 7:
          tp = 'GUÍA DE DEVOLUCIÓN';
          break;
        case 8:
          tp = 'TRASLADO PARA EXPORTACIÓN';
          break;
        case 9:
          tp = 'VENTA PARA EXPORTACIÓN';
          break;
        default:
          tp = '';
      }
      documento.indicadorDeTraslado = tp;
    },

    obtenerJsonCsv: function(config) {
      var parent = this;
      var request = {
        method: 'GET',
        url: config.resultadoExportar
      };

      $http(request).then(
        function(response) {
          try {
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
              var csvCompleto = '';

              var campos = ['febosId'];
              for (var i = 0; i < config.campos.length; i++) {
                if (config.campos[i].mostrarColumna) {
                  campos.push(config.campos[i].nombre);
                }
              }

              var valores = [];
              console.log('VALORES:');
              for (var i = 0; i < response.data.documentos.length; i++) {
                parent.interpretarValores(response.data.documentos[i]);
                console.log(response.data.documentos[i]);
              }

              console.log(config.campos.nombres);
              csvCompleto = Papa.unparse(
                {
                  fields: campos,
                  data: response.data.documentos
                },
                {
                  delimiter: ';'
                }
              );

              var link = document.createElement('a');
              link.href = 'data:text/csv;charset=utf-8;base64,' + btoa(csvCompleto);
              link.download = 'ExportadoFebos.csv';
              document.body.appendChild(link);
              try {
                setTimeout(function() {
                  link.click();
                }, 150);
              } catch (e) {}
            } else {
              febosSingleton.error(
                response.data.codigo,
                response.data.mensaje,
                response.data.seguimientoId
              );
            }
          } catch (e) {
            console.log(e);
            febosSingleton.error(response.data.codigo, e.message, response.data.seguimientoId);
          }
        },
        function(response) {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
          febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');
        }
      );
    },

    exportarACSV_DTE: function(config) {
      config.estadoExportar = 0;
      var parent = this;

      this.enviaTareaCsvDte(config);
      var refrescoId = setInterval(function() {
        if (typeof config.tareaId !== 'undefined') {
          if (config.estadoExportar == 3 || config.estadoExportar == 4) {
            console.log('config.resultadoExportar');
            console.log(config.resultadoExportar);
            parent.obtenerJsonCsv(config);

            clearInterval(refrescoId);
          } else {
            parent.preguntaPorTarea(config);
          }
        }
        console.log();
      }, 5000);
    },

    generarDocumentosReporte: function(config) {
      var tipoDocumentoSeleccionado;
      var parametrosSeleccionados = '';
      var filtrosSeleccionados = '';
      var ServicioDocumentos = this;

      if (config.campos != undefined) {
        for (var i = 0; i < config.campos.length; i++) {
          parametrosSeleccionados = parametrosSeleccionados.concat(config.campos[i]).concat(',');
        }
      }

      if (config.filtros.rutCesionario != undefined) {
        if (config.filtros.rutCesionario[0] == 'si') {
          parametrosSeleccionados = parametrosSeleccionados.concat('rut_cesionario').concat(',');
        }
      }

      parametrosSeleccionados = parametrosSeleccionados.substring(
        0,
        parametrosSeleccionados.length - 1
      );

      delete config.filtros.rutCesionario;

      var subreq = {
        campos: parametrosSeleccionados,
        filtros: this.construirFiltros(config.filtros, config),
        orden: config.orden.join(',')
      };
      //var reqEncoded = FebosUtil.Base64.encode(JSON.stringify(subreq));
      var reqEncoded = FebosUtil.encode(JSON.stringify(subreq));
      if (parametrosSeleccionados != '') {
        ServicioDocumentos.mostrarMensajeCarga('Generando reporte de documento...');

        FebosAPI['cl' + '_encolar_tarea_background'](
          {
            proceso: 'cl' + '_generador_reportes_dte'
          },
          { payload: reqEncoded },
          true,
          false
        ).then(function(response) {
          console.log('DENTRO DEL SERVICIO', response);
          ServicioDocumentos.ocultarMensajeCarga();
          UIkit.modal.alert(
            ' Su reporte fue creado exitosamente, por favor revise su correo electronico para descargar el archivo '
          );
        });
      } else {
        UIkit.modal.alert(
          'Por favor elija los parametros en el arbol de selección para generar su reporte'
        );
      }
    },

    exportarACSV_DNT: function(config) {
      config.estadoExportar = 0;
      var parent = this;

      this.enviaTareaCsvDnt(config);
      var refrescoId = setInterval(function() {
        if (typeof config.tareaId !== 'undefined') {
          if (config.estadoExportar == 3 || config.estadoExportar == 4) {
            console.log('config.resultadoExportar');
            console.log(config.resultadoExportar);
            parent.obtenerJsonCsv(config);

            clearInterval(refrescoId);
          } else {
            parent.preguntaPorTarea(config);
          }
        }
        console.log();
      }, 5000);
    }
  };
});

febosApp
  .filter('multiSelectFilter', function() {
    return function(items, filterData) {
      if (filterData == undefined) return items;
      var keys = Object.keys(filterData);
      var filtered = [];
      var populate = true;

      for (var i = 0; i < items.length; i++) {
        var item = items[i];
        populate = true;
        for (var j = 0; j < keys.length; j++) {
          if (filterData[keys[j]] != undefined) {
            if (filterData[keys[j]].length == 0 || filterData[keys[j]].contains(item[keys[j]])) {
              populate = true;
            } else {
              populate = false;
              break;
            }
          }
        }
        if (populate) {
          filtered.push(item);
        }
      }
      return filtered;
    };
  })
  .filter('jsonDate', function() {
    return function(x) {
      if (x) return new Date(x);
      else return null;
    };
  })
  .filter('momentDate', function() {
    return function(x, date_format_i, date_format_o) {
      if (x) {
        if (date_format_i) {
          return moment(x, date_format_i).format(date_format_o);
        } else {
          return moment(new Date(x)).format(date_format_o);
        }
      } else return null;
    };
  })
  .filter('initials', function() {
    return function(x) {
      if (x) {
        return x
          .split(' ')
          .map(function(s) {
            return s.charAt(0);
          })
          .join('');
      } else {
        return null;
      }
    };
  })
  .filter('reverseOrder', function() {
    return function(items) {
      return items.slice().reverse();
    };
  })
  .filter('trust', [
    '$sce',
    function($sce) {
      return function(htmlCode) {
        return $sce.trustAsHtml(htmlCode);
      };
    }
  ])
  .filter('tipoDeDato', [
    function() {
      return function(variable) {
        return typeof variable;
      };
    }
  ])
  .filter('tipoEstadoDTE', [
    function() {
      return function(tipo) {
        if (tipo == 0) {
          return 'Creado';
        }
        if (tipo == 1) {
          return 'Firmado';
        }
        if (tipo == 2) {
          return 'Enviado a la Dian';
        }
        if (tipo == 3) {
          return 'Error en la Dian';
        }
        if (tipo == 4) {
          return 'Válido por la Dian';
        }
        return null;
      };
    }
  ])
  .filter('trusted', [
    '$sce',
    function($sce) {
      var div = document.createElement('div');
      return function(text) {
        div.innerHTML = text;
        return $sce.trustAsHtml(div.textContent);
      };
    }
  ])
  .filter('sumByKey', function() {
    return function(data, key) {
      /*EJEMPLO:  <span class="badge badge-success pull-right">{{myList|sumByKey:'total'}}</span>*/
      if (typeof data === 'undefined' || typeof key === 'undefined') {
        return 0;
      }

      var sum = 0;
      for (var i = data.length - 1; i >= 0; i--) {
        sum += parseInt(data[i][key]);
      }

      return sum;
    };
  })
  .filter('transformarFechaReverso', function() {
    return function(fecha) {
      /*EJEMPLO:  <span class="badge badge-success pull-right">{{myList|sumByKey:'total'}}</span>*/
      try {
        if (fecha == undefined || fecha == null || fecha == 'null' || isNaN(fecha)) {
          return '--';
        }
        var d = new Date(fecha);
        var m = d.getMonth() + 1;
        var dia = d.getDate();
        m = m < 10 ? '0' + m : m;
        ddia = dia < 10 ? '0' + d : d;
        return d.getFullYear() + '-' + m + '-' + dia;
      } catch (e) {
        console.log(e);
        return '';
      }
    };
  })
  .filter('capitalize', function() {
    return function(input) {
      return !!input ? input.charAt(0).toUpperCase() + input.substr(1) : '';
    };
  })
  .filter('monedaSimbolo', function(MONEDAS) {
    return function(monto, moneda) {
      if (moneda == undefined || moneda.toString().trim().length == 0) {
        return monto;
      }
      for (var m in MONEDAS) {
        if (MONEDAS[m].id == moneda) {
          return MONEDAS[m].simbolo + ' ' + monto + ' ' + MONEDAS[m].sufijo;
        }
      }
      return monto;
    };
  });

/* ocLazyLoad config */

febosApp.config([
  '$ocLazyLoadProvider',
  function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
      debug: false,
      events: false,
      modules: [
        // ----------- UIKIT ------------------
        {
          name: 'lazy_uikit',
          files: [
            // uikit core
            'bower_components/uikit/js/uikit.min.js',
            // uikit components
            'bower_components/uikit/js/components/accordion.min.js',
            'bower_components/uikit/js/components/autocomplete.min.js',
            'assets/js/custom/uikit_datepicker.js',
            'bower_components/uikit/js/components/form-password.min.js',
            'bower_components/uikit/js/components/form-select.min.js',
            'bower_components/uikit/js/components/grid.min.js',
            'bower_components/uikit/js/components/lightbox.min.js',
            'bower_components/uikit/js/components/nestable.min.js',
            'bower_components/uikit/js/components/notify.min.js',
            'bower_components/uikit/js/components/slider.min.js',
            'bower_components/uikit/js/components/slideshow.min.js',
            'bower_components/uikit/js/components/sortable.min.js',
            'bower_components/uikit/js/components/sticky.min.js',
            'bower_components/uikit/js/components/tooltip.min.js',
            'assets/js/custom/uikit_timepicker.min.js',
            'bower_components/uikit/js/components/upload.min.js',
            'assets/js/custom/uikit_beforeready.min.js',
            // styles
            'bower_components/uikit/css/uikit.almost-flat.min.css'
          ],
          insertBefore: '#main_stylesheet',
          serie: true
        },

        // ----------- FORM ELEMENTS -----------
        {
          name: 'lazy_autosize',
          files: [
            'bower_components/autosize/dist/autosize.min.js',
            'app/modules/angular-autosize.min.js'
          ],
          serie: true
        },
        {
          name: 'lazy_iCheck',
          files: ['bower_components/iCheck/icheck.min.js', 'app/modules/angular-icheck.min.js'],
          serie: true
        },
        {
          name: 'lazy_selectizeJS',
          files: [
            'bower_components/selectize/dist/js/standalone/selectize.min.js',
            'app/modules/angular-selectize.min.js'
          ],
          serie: true
        },
        {
          name: 'lazy_switchery',
          files: [
            'bower_components/switchery/dist/switchery.min.js',
            'app/modules/angular-switchery.js'
          ],
          serie: true
        },
        {
          name: 'lazy_ionRangeSlider',
          files: [
            'bower_components/ion.rangeslider/js/ion.rangeSlider.min.js',
            'app/modules/angular-ionRangeSlider.min.js'
          ],
          serie: true
        },
        {
          name: 'lazy_masked_inputs',
          files: ['bower_components/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js']
        },
        {
          name: 'lazy_character_counter',
          files: ['app/modules/angular-character-counter.min.js']
        },
        {
          name: 'lazy_parsleyjs',
          files: [
            'assets/js/custom/parsleyjs_config.min.js',
            'bower_components/parsleyjs/dist/parsley.min.js'
          ],
          serie: true
        },
        {
          name: 'lazy_wizard',
          files: ['bower_components/angular-wizard/dist/angular-wizard.min.js']
        },
        {
          name: 'lazy_ckeditor',
          files: ['bower_components/ckeditor/ckeditor.js', 'app/modules/angular-ckeditor.min.js'],
          serie: true
        },
        {
          name: 'lazy_tinymce',
          files: ['bower_components/tinymce/tinymce.min.js', 'app/modules/angular-tinymce.min.js'],
          serie: true
        },

        // ----------- CHARTS -----------
        {
          name: 'lazy_charts_chartist',
          files: [
            'bower_components/chartist/dist/chartist.min.css',
            'bower_components/chartist/dist/chartist.min.js',
            'app/modules/angular-chartist.min.js'
          ],
          insertBefore: '#main_stylesheet',
          serie: true
        },
        {
          name: 'lazy_charts_easypiechart',
          files: ['bower_components/jquery.easy-pie-chart/dist/angular.easypiechart.min.js']
        },
        {
          name: 'lazy_charts_metricsgraphics',
          files: [
            'bower_components/metrics-graphics/dist/metricsgraphics.css',
            'bower_components/d3/d3.min.js',
            'bower_components/metrics-graphics/dist/metricsgraphics.min.js',
            'app/modules/angular-metrics-graphics.min.js'
          ],
          insertBefore: '#main_stylesheet',
          serie: true
        },
        {
          name: 'lazy_charts_c3',
          files: [
            'bower_components/c3js-chart/c3.min.css',
            'bower_components/d3/d3.min.js',
            'bower_components/c3js-chart/c3.min.js',
            'bower_components/c3-angular/c3-angular.min.js'
          ],
          insertBefore: '#main_stylesheet',
          serie: true
        },
        {
          name: 'lazy_charts_peity',
          files: ['bower_components/peity/jquery.peity.min.js', 'app/modules/angular-peity.min.js'],
          serie: true
        },
        {
          name: 'lazy_echarts',
          files: [
            'bower_components/echarts/dist/echarts.js',
            'assets/js/custom/echarts/maps/china.js',
            'app/modules/angular-echarts.js'
          ],
          serie: true
        },

        // ----------- COMPONENTS -----------
        {
          name: 'lazy_countUp',
          files: [
            'bower_components/countUp.js/dist/countUp.min.js',
            'app/modules/angular-countUp.min.js'
          ],
          serie: true
        },
        {
          name: 'lazy_clndr',
          files: [
            'bower_components/clndr/clndr.min.js',
            'bower_components/angular-clndr/angular-clndr.min.js'
          ],
          serie: true
        },
        {
          name: 'lazy_google_maps',
          files: [
            'bower_components/ngmap/build/scripts/ng-map.min.js',
            'bower_components/ngmap/directives/places-auto-complete.js',
            'bower_components/ngmap/directives/marker.js'
          ],
          serie: true
        },
        {
          name: 'lazy_weathericons',
          files: ['bower_components/weather-icons/css/weather-icons.min.css'],
          insertBefore: '#main_stylesheet',
          serie: true
        },
        {
          name: 'lazy_prismJS',
          files: [
            'bower_components/prism/prism.js',
            'bower_components/prism/components/prism-php.js',
            'bower_components/prism/plugins/line-numbers/prism-line-numbers.js',
            'app/modules/angular-prism.min.js'
          ],
          serie: true
        },
        {
          name: 'lazy_dragula',
          files: ['bower_components/angular-dragula/dist/angular-dragula.min.js']
        },
        {
          name: 'lazy_pagination',
          files: ['bower_components/angularUtils-pagination/dirPagination.js']
        },
        {
          name: 'lazy_diff',
          files: ['bower_components/jsdiff/diff.min.js']
        },

        // ----------- PLUGINS -----------
        {
          name: 'lazy_fullcalendar',
          files: [
            'bower_components/fullcalendar/dist/fullcalendar.min.css',
            'bower_components/fullcalendar/dist/fullcalendar.min.js',
            'bower_components/fullcalendar/dist/gcal.js',
            'bower_components/angular-ui-calendar/src/calendar.js'
          ],
          insertBefore: '#main_stylesheet',
          serie: true
        },
        {
          name: 'lazy_codemirror',
          files: [
            'bower_components/codemirror/lib/codemirror.css',
            'assets/css/codemirror_themes.min.css',
            'bower_components/codemirror/lib/codemirror.js',
            'assets/js/custom/codemirror_fullscreen.min.js',
            'bower_components/codemirror/addon/edit/matchtags.js',
            'bower_components/codemirror/addon/edit/matchbrackets.js',
            'bower_components/codemirror/addon/fold/xml-fold.js',
            'bower_components/codemirror/mode/htmlmixed/htmlmixed.js',
            'bower_components/codemirror/mode/xml/xml.js',
            'bower_components/codemirror/mode/php/php.js',
            'bower_components/codemirror/mode/clike/clike.js',
            'bower_components/codemirror/mode/javascript/javascript.js',
            'app/modules/angular-codemirror.min.js'
          ],
          insertBefore: '#main_stylesheet',
          serie: true
        },
        {
          name: 'lazy_datatables',
          files: [
            'bower_components/datatables/media/js/jquery.dataTables.min.js',
            'bower_components/angular-datatables/dist/angular-datatables.js',
            'assets/js/custom/jquery.dataTables.columnFilter.js',
            'bower_components/angular-datatables/dist/plugins/columnfilter/angular-datatables.columnfilter.min.js',
            'assets/js/custom/datatables/datatables.uikit.js',
            // buttons
            'bower_components/datatables-buttons/js/dataTables.buttons.js',
            'bower_components/angular-datatables/dist/plugins/buttons/angular-datatables.buttons.min.js',
            'assets/js/custom/datatables/buttons.uikit.js',
            'bower_components/jszip/dist/jszip.min.js',
            'bower_components/pdfmake/build/pdfmake.min.js',
            'bower_components/pdfmake/build/vfs_fonts.js',
            'bower_components/datatables-buttons/js/buttons.colVis.js',
            'bower_components/datatables-buttons/js/buttons.html5.js',
            'bower_components/datatables-buttons/js/buttons.print.js'
          ],
          serie: true
        },
        {
          name: 'lazy_gantt_chart',
          files: [
            'bower_components/jquery-ui/jquery-ui.min.js',
            'assets/js/custom/gantt_chart.min.js'
          ],
          serie: true
        },
        {
          name: 'lazy_tablesorter',
          files: [
            'bower_components/tablesorter/dist/js/jquery.tablesorter.min.js',
            'bower_components/tablesorter/dist/js/jquery.tablesorter.widgets.min.js',
            'bower_components/tablesorter/dist/js/widgets/widget-alignChar.min.js',
            'bower_components/tablesorter/dist/js/widgets/widget-columnSelector.min.js',
            'bower_components/tablesorter/dist/js/widgets/widget-print.min.js',
            'bower_components/tablesorter/dist/js/extras/jquery.tablesorter.pager.min.js'
          ],
          serie: true
        },
        {
          name: 'lazy_vector_maps',
          files: [
            'bower_components/raphael/raphael.min.js',
            'bower_components/jquery-mapael/js/jquery.mapael.js',
            'bower_components/jquery-mapael/js/maps/world_countries.js',
            'bower_components/jquery-mapael/js/maps/usa_states.js'
          ],
          serie: true
        },
        {
          name: 'lazy_dropify',
          files: [
            'assets/skins/dropify/css/dropify.css',
            'bower_components/dropify/dist/js/dropify.min.js'
          ],
          insertBefore: '#main_stylesheet'
        },
        {
          name: 'lazy_tree',
          files: [
            'assets/skins/jquery.fancytree/ui.fancytree.min.css',
            'bower_components/jquery-ui/jquery-ui.min.js',
            'bower_components/jquery.fancytree/dist/jquery.fancytree-all.min.js'
          ],
          insertBefore: '#main_stylesheet',
          serie: true
        },
        {
          name: 'lazy_idle_timeout',
          files: ['bower_components/ng-idle/angular-idle.min.js']
        },
        {
          name: 'lazy_tour',
          files: ['bower_components/enjoyhint/enjoyhint.min.js']
        },
        {
          name: 'lazy_filemanager',
          files: [
            'bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css',
            'file_manager/css/elfinder.min.css',
            'file_manager/themes/material/css/theme.css',
            'bower_components/jquery-ui/jquery-ui.min.js',
            'file_manager/js/elfinder.min.js'
          ],
          insertBefore: '#main_stylesheet',
          serie: true
        },
        {
          name: 'lazy_image_cropper',
          files: [
            'bower_components/cropper/dist/cropper.min.css',
            'bower_components/cropper/dist/cropper.min.js'
          ]
        },
        {
          name: 'lazy_context_menu',
          files: [
            'bower_components/jQuery-contextMenu/dist/jquery.ui.position.min.js',
            'bower_components/jQuery-contextMenu/dist/jquery.contextMenu.min.css',
            'bower_components/jQuery-contextMenu/dist/jquery.contextMenu.min.js'
          ],
          insertBefore: '#main_stylesheet',
          serie: true
        },
        {
          name: 'lazy_quiz',
          files: ['assets/js/custom/slickQuiz/slickQuiz.js']
        },
        {
          name: 'lazy_listNav',
          files: ['bower_components/jquery-listnav/jquery-listnav.min.js']
        },
        {
          name: 'lazy_uiSelect',
          files: [
            'bower_components/angular-ui-select/dist/select.min.css',
            'bower_components/angular-ui-select/dist/select.min.js'
          ]
        },

        // ----------- KENDOUI COMPONENTS -----------
        {
          name: 'lazy_KendoUI',
          files: [
            'bower_components/kendo-ui/js/kendo.core.min.js',
            'bower_components/kendo-ui/js/kendo.color.min.js',
            'bower_components/kendo-ui/js/kendo.data.min.js',
            'bower_components/kendo-ui/js/kendo.calendar.min.js',
            'bower_components/kendo-ui/js/kendo.popup.min.js',
            'bower_components/kendo-ui/js/kendo.datepicker.min.js',
            'bower_components/kendo-ui/js/kendo.timepicker.min.js',
            'bower_components/kendo-ui/js/kendo.datetimepicker.min.js',
            'bower_components/kendo-ui/js/kendo.list.min.js',
            'bower_components/kendo-ui/js/kendo.fx.min.js',
            'bower_components/kendo-ui/js/kendo.userevents.min.js',
            'bower_components/kendo-ui/js/kendo.menu.min.js',
            'bower_components/kendo-ui/js/kendo.draganddrop.min.js',
            'bower_components/kendo-ui/js/kendo.slider.min.js',
            'bower_components/kendo-ui/js/kendo.mobile.scroller.min.js',
            'bower_components/kendo-ui/js/kendo.autocomplete.min.js',
            'bower_components/kendo-ui/js/kendo.combobox.min.js',
            'bower_components/kendo-ui/js/kendo.dropdownlist.min.js',
            'bower_components/kendo-ui/js/kendo.colorpicker.min.js',
            'bower_components/kendo-ui/js/kendo.combobox.min.js',
            'bower_components/kendo-ui/js/kendo.maskedtextbox.min.js',
            'bower_components/kendo-ui/js/kendo.multiselect.min.js',
            'bower_components/kendo-ui/js/kendo.numerictextbox.min.js',
            'bower_components/kendo-ui/js/kendo.toolbar.min.js',
            'bower_components/kendo-ui/js/kendo.panelbar.min.js',
            'bower_components/kendo-ui/js/kendo.window.min.js',
            'bower_components/kendo-ui/js/kendo.angular.min.js',
            'bower_components/kendo-ui/styles/kendo.common-material.min.css',
            'bower_components/kendo-ui/styles/kendo.material.min.css'
          ],
          insertBefore: '#main_stylesheet',
          serie: true
        },

        // ----------- UIKIT HTMLEDITOR -----------
        {
          name: 'lazy_htmleditor',
          files: [
            'bower_components/codemirror/lib/codemirror.js',
            'bower_components/codemirror/mode/markdown/markdown.js',
            'bower_components/codemirror/addon/mode/overlay.js',
            'bower_components/codemirror/mode/javascript/javascript.js',
            'bower_components/codemirror/mode/php/php.js',
            'bower_components/codemirror/mode/gfm/gfm.js',
            'bower_components/codemirror/mode/xml/xml.js',
            'bower_components/marked/lib/marked.js',
            'bower_components/uikit/js/components/htmleditor.js'
          ],
          serie: true
        },

        // ----------- THEMES -------------------
        {
          name: 'lazy_themes',
          files: [
            'assets/css/themes/_theme_a.min.css',
            'assets/css/themes/_theme_b.min.css',
            'assets/css/themes/_theme_c.min.css',
            'assets/css/themes/_theme_d.min.css',
            'assets/css/themes/_theme_e.min.css',
            'assets/css/themes/_theme_f.min.css',
            'assets/css/themes/_theme_g.min.css',
            'assets/css/themes/_theme_h.min.css',
            'assets/css/themes/_theme_i.min.css',
            'assets/css/themes/_theme_dark.min.css'
          ]
        }
      ]
    });
  }
]);

febosApp
  .service('detectBrowser', [
    '$window',
    function($window) {
      // http://stackoverflow.com/questions/22947535/how-to-detect-browser-using-angular
      return function() {
        var userAgent = $window.navigator.userAgent,
          browsers = {
            chrome: /chrome/i,
            safari: /safari/i,
            firefox: /firefox/i,
            ie: /internet explorer/i
          };

        for (var key in browsers) {
          if (browsers[key].test(userAgent)) {
            return key;
          }
        }
        return 'unknown';
      };
    }
  ])
  .service('ContextoFebos', [
    '$localStorage',
    function($localStorage) {
      //guarda en local storage esta info, para no ir a buscarla todo el rato
      var versionCache = 6; //CONTROL DE RESETEO DE LOCAL STORAGE
      $storage = $localStorage;
      if (typeof $storage.versionCache === 'undefined' || $storage.versionCache != versionCache) {
        console.log('vaciando local storage');
        $storage.$reset();
        $storage.versionCache = versionCache;
      }

      if (typeof $storage.sesiones === 'undefined') {
        $storage.sesiones = {};
      }

      if (typeof $storage.usuarioActual == 'undefined') {
        $storage.usuarioActual = '';
      }

      $storage.template = {};
      $storage.template.usuario = {
        nombre: '',
        alias: '',
        correo: '',
        iut: '',
        token: '',
        permisos: [],
        roles: [],
        ultimoPingValido: null
      };
      $storage.template.menu = {
        cloud: [],
        cliente: [],
        proveedores: []
      };
      //para cuando asuma identidad, aqui se guarda temporalmente el usuario mismo original
      $storage.template.usuarioResponsable = {
        nombre: '',
        alias: '',
        correo: '',
        iut: '',
        token: '',
        permisos: [],
        roles: []
      };
      $storage.template.misEmpresas = [];
      $storage.template.misClientes = {};
      $storage.template.misProveedores = {};

      $storage.template.grupo = {
        id: '',
        codigo: '',
        nombre: '',
        descripcion: ''
      };

      $storage.template.empresa = {
        iut: '',
        fantasia: '',
        razonSocial: '',
        id: ''
      };
      $storage.template.cliente = {
        iut: '',
        fantasia: '',
        razonSocial: '',
        id: ''
      };
      $storage.template.proveedor = {
        iut: '',
        fantasia: '',
        razonSocial: '',
        id: ''
      };
      //para borrar todo hay que hacer ContextoFebos.$reset();
      return $storage;
    }
  ])
  .service('VistasPortafolio', [
    'ContextoFebos',
    function(ContextoFebos) {
      //var correo = ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.correo;

      var vistas = {
        cloud: {
          emitidos: {
            //portafolio/cloud/emitidos: anticipos, garantias, polizas, proyectos
            anticipos: {
              titulo: 'Anticipos',
              documentosDisponibles: ['13'],
              filtrosPorDefecto: [],
              filtrosFijos: [{ campo: 'tipo', valor: '13' }]
            },
            garantias: {
              titulo: 'Boletas de Garantía',
              documentosDisponibles: ['12'],
              filtrosPorDefecto: [],
              filtrosFijos: [{ campo: 'tipo', valor: '12' }]
            },
            polizas: {
              titulo: 'Polizas',
              documentosDisponibles: ['11'],
              filtrosPorDefecto: [],
              filtrosFijos: [{ campo: 'tipo', valor: '11' }]
            },
            proyectos: {
              titulo: 'Proyectos',
              documentosDisponibles: ['10'],
              filtrosPorDefecto: [
                //{ campo: 'tipoDocumento', valor: [33, 34, 56, 61, 110, 111, 112] }
              ],
              filtrosFijos: [{ campo: 'tipo', valor: '10' }]
            }
          }
        }
      };
      return vistas;
    }
  ])
  .service('VistasDnt', [
    'ContextoFebos',
    function(ContextoFebos) {
      //var correo = ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.correo;

      var vistas = {};
      vistas = {
    cloud: {
      recibidos: {
        ordenesDeCompra: {
          titulo: 'Ordenes de compra recibidas',
          documentosDisponibles: [801],
          filtrosPorDefecto: [
            //{ campo: 'tipoDocumento', valor: [33, 34, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [{ campo: 'tipo', valor: '801' }]
        },
        HEM: {
          titulo: 'Hojas de entrega de materiales recibidas',
          documentosDisponibles: ['HEM'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HEM' }]
        },
        HES: {
          titulo: 'Hojas de entrega de Servicios recibidas',
          documentosDisponibles: ['HES'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HES' }]
        },
        mercadoPublico: {
          titulo: 'Documentos de Mercado Público recibidos',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        mercadoPublicoMias: {
          titulo: 'Documentos de Mercado Público recibidos por mi',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        referencia: {
          titulo: 'DNT recibidos',
          documentosDisponibles: [801, 'HEM', 'HES', 'MP'],
          filtrosPorDefecto: [],
          filtrosFijos: []
        }
      },
      emitidos: {
        //dnt/cloud/emitidos: HEM, HES, mercadoPublico, mercadoPublicoMias, ordenesDeCompra
        ordenesDeCompra: {
          titulo: 'Ordenes de compra emitidas',
          documentosDisponibles: [801],
          filtrosPorDefecto: [
            //{ campo: 'tipoDocumento', valor: [33, 34, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [{ campo: 'tipo', valor: '801' }]
        },
        HEM: {
          titulo: 'Hojas de entrega de materiales emitidas',
          documentosDisponibles: ['HEM'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HEM' }]
        },
        HES: {
          titulo: 'Hojas de entrega de Servicios emitidas',
          documentosDisponibles: ['HES'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HES' }]
        },
        mercadoPublico: {
          titulo: 'Documentos de Mercado Público emitidos',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        mercadoPublicoMias: {
          titulo: 'Documentos de Mercado Público emitidos por mi',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        referencia: {
          titulo: 'DNT emitidos',
          documentosDisponibles: [801, 'HEM', 'HES', 'MP'],
          filtrosPorDefecto: [],
          filtrosFijos: []
        }
      }
    },
    proveedores: {
      emitidos: {
        ordenesDeCompra: {
          titulo: 'Ordenes de compra emitidas',
          documentosDisponibles: [801],
          filtrosPorDefecto: [
            //{ campo: 'tipoDocumento', valor: [33, 34, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [{ campo: 'tipo', valor: '801' }]
        },
        HEM: {
          titulo: 'Hojas de entrega de materiales emitidas',
          documentosDisponibles: ['HEM'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HEM' }]
        },
        HES: {
          titulo: 'Hojas de entrega de Servicios emitidas',
          documentosDisponibles: ['HES'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HES' }]
        },
        mercadoPublico: {
          titulo: 'Documentos de Mercado Público emitidos',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        mercadoPublicoMias: {
          titulo: 'Documentos de Mercado Público emitidos por mi',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        referencia: {
          titulo: 'DNT emitidos',
          documentosDisponibles: [801, 'HEM', 'HES', 'MP'],
          filtrosPorDefecto: [],
          filtrosFijos: []
        }
      },
      recibidos: {
        //dnt/proveedores/recibidos: HEM, HES, mercadoPublico, ordenesDeCompra
        ordenesDeCompra: {
          titulo: 'Ordenes de compra recibidas',
          documentosDisponibles: [801],
          filtrosPorDefecto: [
            //{ campo: 'tipoDocumento', valor: [33, 34, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [{ campo: 'tipo', valor: '801' }]
        },
        HEM: {
          titulo: 'Hojas de entrega de materiales recibidas',
          documentosDisponibles: ['HEM'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HEM' }]
        },
        HES: {
          titulo: 'Hojas de entrega de Servicios recibidas',
          documentosDisponibles: ['HES'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HES' }]
        },
        mercadoPublico: {
          titulo: 'Documentos de Mercado Público recibidas',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        referencia: {
          titulo: 'DNT recibidos',
          documentosDisponibles: [801, 'HEM', 'HES', 'MP'],
          filtrosPorDefecto: [],
          filtrosFijos: []
        }
      }
    },
    cliente: {
      recibidos: {
        todos: {
          titulo: 'DTE recibidos',
          documentosDisponibles: [33, 34, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        }
      },
      emitidos: {
        //dnt/cliente/emitidos: HEM, HES, mercadoPublico, ordenesDeCompra
        ordenesDeCompra: {
          titulo: 'Ordenes de compra recibidas',
          documentosDisponibles: [801],
          filtrosPorDefecto: [
            //{ campo: 'tipoDocumento', valor: [33, 34, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [{ campo: 'tipo', valor: '801' }]
        },
        HEM: {
          titulo: 'Hojas de entrega de materiales recibidas',
          documentosDisponibles: ['HEM'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HEM' }]
        },
        HES: {
          titulo: 'Hojas de entrega de Servicios recibidas',
          documentosDisponibles: ['HES'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HES' }]
        },
        mercadoPublico: {
          titulo: 'Documentos de Mercado Público recibidas',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        referencia: {
          titulo: 'DNT emitidos',
          documentosDisponibles: [801, 'HEM', 'HES', 'MP'],
          filtrosPorDefecto: [],
          filtrosFijos: []
        }
      }
    }
  };

      return vistas;
    }
  ])
  .service('VistasDte', [
    'ContextoFebos',
    function(ContextoFebos) {
      //var correo = ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.correo;

      var vistas = {};
      vistas = {
    cloud: {
      recibidos: {
        //dte/cloud/recibidos: cesionados, guias, honorarios, manuales, misRecibidos, misRecibidosUnidad,
        //  noRecibidos, pagados, pendientes, preRechazados, rechazados, todos
        cedidos: {
          titulo: 'DTE recibidos que se han cedido',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'rutCesionario', valor: '-' }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        }
      },
      recibidos: {
        //dte/cloud/recibidos: cesionados, guias, honorarios, manuales, misRecibidos, misRecibidosUnidad,
        //  noRecibidos, pagados, pendientes, preRechazados, rechazados, todos
        cedidos: {
          titulo: 'DTE recibidos que se han cedido',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'rutCesionario', valor: '-' }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        guias: {
          titulo: 'Guías de despacho recibidas',
          documentosDisponibles: [52],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [52] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        boletas: {
          titulo: 'Boletas recibidas',
          documentosDisponibles: [39, 41],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [39, 41] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        honorarios: {
          titulo: 'Boletas de honorarios recibidas',
          documentosDisponibles: [66],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [66] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5,6' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        manuales: {
          titulo: 'DTE manuales',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        misRecibidos: {
          titulo: 'Mis DTE recibidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            {
              campo: 'correoReceptor',
              valor: ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.correo
            }
          ]
        },
        misRecibidosUnidad: {
          titulo: 'Mis DTE recibidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            {
              campo: 'correoReceptor',
              valor: ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.correo
            }
          ]
        },
        noRecibidos: {
          titulo: 'DTE no recibidos aún',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'Y' }
          ]
        },
        pagados: {
          titulo: 'DTE recibidos en proceso de pago',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'tipo', valor: [1, 2, 3, 4, 5] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        pendientes: {
          titulo: 'DTE recibidos pendientes de validación en el SII',
          documentosDisponibles: [33, 34, 56, 43, 46, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '7' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        preRechazados: {
          titulo: 'DTE recibidos pre-rechazados',
          documentosDisponibles: [33, 34, 43, 46, 56, 61],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            { campo: 'estadoComercial', valor: [3] }
          ]
        },
        reclamados: {
          titulo: 'DTE recibidos reclamados',
          documentosDisponibles: [33, 34, 43, 46, 56, 61],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            { campo: 'estadoComercial', valor: [4, 5, 6] }
          ]
        },
        sinRecepcionSII: {
          titulo: 'DTE recibidos sin Fecha Recepcion',
          documentosDisponibles: [33, 34, 43, 46, 56, 61],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61] }],
          filtrosFijos: [
            { campo: 'fechaRecepcionSii', valor: 'NULL' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        todos: {
          titulo: 'DTE recibidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        referencia: {
          titulo: 'DTE recibidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: []
        }
      },
      emitidos: {
        //dte/cloud/emitidos: cedidos, guias, misEmitidos, misEmitidosUnidad, noEntregados, todos
        cesionados: {
          titulo: 'DTE emitidos que se han cesionado',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'rutCesionario', valor: '-' }
          ],
          filtrosFijos: [{ campo: 'estadoSii', valor: '2,4,5' }]
        },
        guias: {
          titulo: 'Guías de despacho emitidas',
          documentosDisponibles: [52],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [52] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '2,4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        boletas: {
          titulo: 'Boletas emitidas',
          documentosDisponibles: [39, 41],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [39, 41] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        envioFolios: {
          titulo: 'Envío Consumo Folios',
          documentosDisponibles: [39, 41],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [39, 41] },
            { campo: 'estadoSii', valor: '1,2,3,4,5,6,7,8' }
          ],
          filtrosFijos: [{ campo: 'incompleto', valor: 'N' }]
        },
        prefacturas: {
          titulo: 'Guías de despacho emitidas',
          documentosDisponibles: [52],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [-33, -34, -52, -56, -61] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '0' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        misEmitidos: {
          titulo: 'Mis DTE Emitidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '1,2,4,5,6,7' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        misEmitidosUnidad: {
          titulo: 'Mis DTE Emitidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '2,4,5,6,7' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        noEntregados: {
          titulo: 'DTE emitidos que no se han entregado al receptor',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '2,4,5' },
            { campo: 'incompleto', valor: 'Y' }
          ]
        },
        indicadores: {
          titulo: 'DTE emitidos',
          documentosDisponibles: [33, 34, 39, 41, 56, 61, 110, 111, 112, 801],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 39, 41, 56, 61, 110, 111, 112, 801] }
          ],
          filtrosFijos: [{ campo: 'incompleto', valor: 'N' }]
        },
        todos: {
          titulo: 'DTE emitidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '1,2,4,5,6,7' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        borradores: {
          titulo: 'DTE Borradores',
          documentosDisponibles: [
            -33,
            -34,
            -39,
            -42,
            -43,
            -46,
            -52,
            -56,
            -61,
            -110,
            -111,
            -112
          ],
          filtrosPorDefecto: [
            {
              campo: 'tipoDocumento',
              valor: [-33, -34, -39, -42, -43, -46, -52, -56, -61, -110, -111, -112]
            }
          ],
          filtrosFijos: [
            /*{ campo: 'estadoSii', valor: '2,4,5,6,7' },*/
            /*{ campo: 'incompleto', valor: 'N' }*/
          ]
        },
        referencia: {
          titulo: 'DTE emitidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: []
        }
      }
    },
    proveedores: {
      emitidos: {
        //dte/proveedores/emitidos: cesionados, honorarios, noEntregados, pagados, preRechazados, rechazados, todos
        cesionados: {
          titulo: 'DTE emitidos que se han cesionado',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'rutCesionario', valor: '-' }
          ],
          filtrosFijos: [{ campo: 'estadoSii', valor: '4,5' }]
        },
        honorarios: {
          titulo: 'Boletas de honorarios recibidas',
          documentosDisponibles: [66],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [66] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5,6' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        noEntregados: {
          titulo: 'DTE emitidos que no se han entregado al receptor',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'Y' }
          ]
        },
        pagados: {
          titulo: 'DTE recibidos en proceso de pago',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'tipo', valor: [1, 2, 3, 4, 5] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        preRechazados: {
          titulo: 'DTE recibidos pre-rechazados',
          documentosDisponibles: [33, 34, 43, 46, 56, 61],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            { campo: 'estadoComercial', valor: [3] }
          ]
        },
        rechazados: {
          titulo: 'DTE emitidos rechazados',
          documentosDisponibles: [33, 34, 43, 46, 56, 61],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            { campo: 'estadoComercial', valor: [4, 5, 6] }
          ]
        },
        guias: {
          titulo: 'Guías de despacho emitidas',
              documentosDisponibles: [52],
              filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [52] }],
              filtrosFijos: [
            { campo: 'estadoSii', valor: '2,4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        todos: {
          titulo: 'DTE emitidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        referencia: {
          titulo: 'DTE emitidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: []
        }
      }
    },
    cliente: {
      recibidos: {
        //dte/cliente/recibidos: cesionados, pagados, preRechazados, rechazados, todos
        cesionados: {
          titulo: 'DTE emitidos que se han cesionado',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'rutCesionario', valor: '-' }
          ],
          filtrosFijos: [{ campo: 'estadoSii', valor: '4,5' }]
        },
        pagados: {
          titulo: 'DTE recibidos en proceso de pago',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'tipo', valor: [1, 2, 3, 4, 5] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        preRechazados: {
          titulo: 'DTE recibidos pre-rechazados',
          documentosDisponibles: [33, 34, 43, 46, 56, 61],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            { campo: 'estadoComercial', valor: [3] }
          ]
        },
        rechazados: {
          titulo: 'DTE recibidos rechazados',
          documentosDisponibles: [33, 34, 43, 46, 56, 61],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            { campo: 'estadoComercial', valor: [4, 5, 6] }
          ]
        },
        todos: {
          titulo: 'DTE recibidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        referencia: {
          titulo: 'DTE recibidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: []
        }
      }
    }
  };

      return vistas;
    }
  ])

  .service('VistasEmisionWeb', [
    'ContextoFebos',
    function(ContextoFebos) {
      var vistas = {};
      vistas = {
    cloud: {
      emisionWeb: {
        todos: {
          titulo: 'Emision DTE',
          documentosDisponibles: [33, 34, 39, 41, 56, 61, 110, 111, 112]
        },
        // 33 Factura electrónica - 34 Factura no afecta o exenta electrónica -
        venta: {
          titulo: 'Factura Electronica',
          documentosDisponibles: [33]
        },
        ventaExenta: {
          titulo: 'Factura Exenta Electrónica',
          documentosDisponibles: [34]
        },
        // 56 Nota de débito electrónica - 61 Nota de crédito electrónica -
        notaDebito: {
          titulo: 'Nota de Débito',
          documentosDisponibles: [56]
        },
        notaCredito: {
          titulo: 'Nota de Crédito',
          documentosDisponibles: [61]
        },
        // 52 Guía de despacho electrónica -
        guiaDespacho: {
          titulo: 'Guia de Despacho',
          documentosDisponibles: [52]
        },
        // 39 Boleta electrónica - 41 Boleta exenta electrónica -
        boleta: {
          titulo: 'Boleta Electronica',
          documentosDisponibles: [39]
        },
        boletaExenta: {
          titulo: 'Boleta Exenta Electrónica',
          documentosDisponibles: [41]
        },
        // 110 Factura de exportación electrónica - 111 Nota de débito de exportación electrónica - 112 Nota de crédito de exportación electrónica -
        exportacion: {
          titulo: 'Factura de Exportacion',
          documentosDisponibles: [110]
        },
        notaDebitoExportacion: {
          titulo: 'Nota de Débito de Exportacion',
          documentosDisponibles: [111]
        },
        notaCreditoExportacion: {
          titulo: 'Nota de Crédito de Exportacion',
          documentosDisponibles: [112]
        },
        // 46 Factura de compra electrónica -
        compra: {
          titulo: 'Factura de Compra Electrónica',
          documentosDisponibles: [46]
        }
      }
    },
    proveedores: {
      emisionWeb: {
        todos: {
          titulo: 'Emision DTE',
          documentosDisponibles: [33, 34, 39, 41, 56, 61, 110, 111, 112]
        },
        venta: {
          titulo: 'Factura Electronica',
          documentosDisponibles: [33, 34]
        },
        boleta: {
          titulo: 'Boleta Electronica',
          documentosDisponibles: [39, 41]
        },
        exportacion: {
          titulo: 'Factura de Exportacion',
          documentosDisponibles: [110]
        },
        notaDebito: {
          titulo: 'Nota de Crédito',
          documentosDisponibles: [61, 112]
        },
        notaCredito: {
          titulo: 'Nota de Débito',
          documentosDisponibles: [56, 111]
        }
      }
    },
    cliente: {
      emisionWeb: {
        todos: {
          titulo: 'Emision DTE',
          documentosDisponibles: [33, 34, 39, 41, 56, 61, 110, 111, 112]
        },
        venta: {
          titulo: 'Factura Electronica',
          documentosDisponibles: [33, 34]
        },
        boleta: {
          titulo: 'Boleta Electronica',
          documentosDisponibles: [39, 41]
        },
        exportacion: {
          titulo: 'Factura de Exportacion',
          documentosDisponibles: [110]
        },
        notaDebito: {
          titulo: 'Nota de Crédito',
          documentosDisponibles: [61, 112]
        },
        notaCredito: {
          titulo: 'Nota de Débito',
          documentosDisponibles: [56, 111]
        }
      }
    }
  };;

      return vistas;
    }
  ])

  .service('ComplementoDte', [
    '$localStorage',
    'VistasDte',
    function($localStorage, VistasDte) {
      var vistas = VistasDte;
      var elementos = {};
      elementos = {
    atajosDeRangosDeFecha: {
      '[hoy]': {
        desde: moment().format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[ayerYhoy]': {
        desde: moment()
          .subtract(1, 'days')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[estaSemana]': {
        desde: moment()
          .startOf('week')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[las2UltimasSemanas]': {
        desde: moment()
          .startOf('week')
          .subtract(1, 'week')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[las3UltimasSemanas]': {
        desde: moment()
          .startOf('week')
          .subtract(2, 'weeks')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos5Dias]': {
        desde: moment()
          .subtract(5, 'days')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos10Dias]': {
        desde: moment()
          .subtract(10, 'days')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos15Dias]': {
        desde: moment()
          .subtract(15, 'days')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos20Dias]': {
        desde: moment()
          .subtract(20, 'days')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos30Dias]': {
        desde: moment()
          .subtract(30, 'days')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[esteMes]': {
        desde: moment()
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[elMesPasado]': {
        desde: moment()
          .subtract(1, 'month')
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment()
          .subtract(1, 'month')
          .endOf('month')
          .format('YYYY-MM-DD')
      },
      '[elMesAntesPasado]': {
        desde: moment()
          .subtract(2, 'month')
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment()
          .subtract(2, 'month')
          .endOf('month')
          .format('YYYY-MM-DD')
      },
      '[losUltimos2Meses]': {
        desde: moment()
          .subtract(1, 'month')
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos3Meses]': {
        desde: moment()
          .subtract(2, 'month')
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos4Meses]': {
        desde: moment()
          .subtract(3, 'month')
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos5Meses]': {
        desde: moment()
          .subtract(4, 'month')
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos6Meses]': {
        desde: moment()
          .subtract(5, 'month')
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      }
    },
    posiblesCampos: [
      { campo: 'tipoDocumento', nombre: 'Tipo de documento', tipoFiltro: 'tag', opciones: [] },
      { campo: 'folio', nombre: 'Folio', tipoFiltro: 'tag' },
      { campo: 'rutEmisor', nombre: 'Rut Emisor', tipoFiltro: 'tag' },
      { campo: 'razonSocialEmisor', nombre: 'Razón Social Emisor', tipoFiltro: 'texto' },
      { campo: 'rutReceptor', nombre: 'Rut Receptor', tipoFiltro: 'tag' },
      { campo: 'razonSocialReceptor', nombre: 'Razón Social Receptor', tipoFiltro: 'texto' },
      { campo: 'rutCesionario', nombre: 'Rut Cesionario', tipoFiltro: 'tag' },
      {
        campo: 'razonSocialCesionario',
        nombre: 'Razón Social Cesionario',
        tipoFiltro: 'texto'
      },
      { campo: 'fechaCesion', nombre: 'Fecha de Cesión', tipoFiltro: 'fecha' },
      { campo: 'codigoSii', nombre: 'Codigo SII', tipoFiltro: 'tag', opciones: [] },
      { campo: 'fechaEmision', nombre: 'Fecha de Emisión', tipoFiltro: 'fecha' },
      { campo: 'fechaRecepcion', nombre: 'Fecha Recepción Febos', tipoFiltro: 'fecha' },
      { campo: 'fechaRecepcionSii', nombre: 'Fecha Recepción SII', tipoFiltro: 'fecha' },
      { campo: 'estadoComercial', nombre: 'Estado Comercial', tipoFiltro: 'tag', opciones: [] },
      { campo: 'estadoSii', nombre: 'Estado SII', tipoFiltro: 'tag', opciones: [] },
      {
        campo: 'fechaReciboMercaderia',
        nombre: 'Fecha Recibo Mercaderías',
        tipoFiltro: 'fecha'
      },
      { campo: 'formaDePago', nombre: 'Forma de pago', tipoFiltro: 'tag', opciones: [] },
      { campo: 'contacto', nombre: 'Contacto', tipoFiltro: 'texto' },
      { campo: 'correoReceptor', nombre: 'Correo receptor', tipoFiltro: 'texto' },
      { campo: 'codigoSii', nombre: 'Codigo Respuesta SII', tipoFiltro: 'tag', opciones: [] },
      { campo: 'fechaCesion', nombre: 'Fecha de Cesión', tipoFiltro: 'fecha' }
    ]
  };;

      elementos.obtenerEquivalenteRangoFecha = function(nombre, comoObjeto) {
        if (comoObjeto === true) {
          return elementos.atajosDeRangosDeFecha[nombre];
        } else {
          return (
            elementos.atajosDeRangosDeFecha[nombre].desde +
            '--' +
            elementos.atajosDeRangosDeFecha[nombre].hasta
          );
        }
      };
      elementos.convertirFiltrosEnObjeto = function(filtrosS) {
        var filtros = {};
        var partes = filtrosS.split('|');
        for (var i = 0; i < partes.length; i++) {
          var par = partes[i].includes(':') ? partes[i].split(':') : partes[i].split('$');
          if (elementos.atajosDeRangosDeFecha.hasOwnProperty(par[1])) {
            //var rango = elementos.atajosDeRangosDeFecha[par[1]];
            //filtros[par[0]]=par[1];
            //filtros[par[0]] = rango.desde + "--" + rango.hasta;
            filtros[par[0]] = par[1];
          } else {
            filtros[par[0]] = par[1];
          }
        }
        return filtros;
      };
      elementos.prepararFiltrosDte = function prepararFiltrosDte(query, app, categoria, vista) {
    var filtros = {};
    if (app == 'cloud' && categoria === 'emitidos') {
      filtros.rutEmisor = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
    }

    if (app == 'cloud' && categoria === 'recibidos') {
      filtros.rutReceptor = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
    }

    if (app == 'proveedores' && categoria === 'emitidos') {
      filtros.rutEmisor = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
      filtros.rutReceptor = $localStorage.sesiones[$localStorage.usuarioActual].cliente.iut;
    }

    if (app == 'cliente' && categoria === 'recibidos') {
      filtros.rutReceptor = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
      filtros.rutEmisor = $localStorage.sesiones[$localStorage.usuarioActual].proveedor.iut;
    }

    var tieneFiltrosDeFecha = false;

    if (vista == 'referencia') {
      tieneFiltrosDeFecha = true;
    }

    if (typeof query.filtros !== 'undefined') {
      var partes = query.filtros.split('|');
      for (var i = 0; i < partes.length; i++) {
        var par = partes[i].includes(':') ? partes[i].split(':') : partes[i].split('$');
        var nombreFiltro = par[0];
        var valorFiltro = par[1];
        console.log(nombreFiltro + ' = ' + valorFiltro);
        if (nombreFiltro.includes('fecha')) {
          tieneFiltrosDeFecha = true;
        }
        if (
          (categoria === 'emitidos' && nombreFiltro === 'rutEmisor') ||
          (categoria === 'recibidos' && nombreFiltro === 'rutReceptor')
        ) {
          //no hacer nada, porque se setea en duro por seguridad
        } else {
          if (
            nombreFiltro != '' &&
            nombreFiltro != 'undefined' &&
            typeof nombreFiltro != 'undefined'
          ) {
            filtros[nombreFiltro] = valorFiltro;
          }
        }
        if (nombreFiltro == 'tipoDocumento') {
          var tiposDelUsuario = valorFiltro.split(',');
          var tiposFinales = [];
          for (var x = 0; x < tiposDelUsuario.length; x++) {
            if (
              vistas[app][categoria][vista].documentosDisponibles.indexOf(
                parseInt(tiposDelUsuario[x])
              ) >= 0
            ) {
              //tiposFinales.push(vistas[app][categoria][vista].documentosDisponibles[x]);
              tiposFinales.push(tiposDelUsuario[x]);
            }
          }
          if (tiposFinales.length == 0) {
            for (
              var x = 0;
              x < vistas[app][categoria][vista].documentosDisponibles.length;
              x++
            ) {
              tiposFinales.push(vistas[app][categoria][vista].documentosDisponibles[x]);
            }
          }
          filtros[nombreFiltro] = tiposFinales.join(',');
        }
      }
    }
    console.log(vistas);

    if (!tieneFiltrosDeFecha) {
      if (categoria === 'emitidos') {
        filtros.fechaCreacion = '[losUltimos30Dias]';
      } else {
        filtros.fechaRecepcionSii = '[losUltimos30Dias]';
      }
    }
    console.log('filtros', filtros);
    //valores POR DEFECTO SI ES QUE NO VIENEN
    console.log('app ' + app + ' categoria ' + categoria + ' vista ' + vista);
    for (var i = 0; i < vistas[app][categoria][vista].filtrosPorDefecto.length; i++) {
      var campo = vistas[app][categoria][vista].filtrosPorDefecto[i].campo;
      var valor = vistas[app][categoria][vista].filtrosPorDefecto[i].valor;
      if (typeof filtros[campo] == 'undefined') {
        filtros[campo] = valor;
      }
    }
    // filtros fijos
    for (var i = 0; i < vistas[app][categoria][vista].filtrosFijos.length; i++) {
      var campo = vistas[app][categoria][vista].filtrosFijos[i].campo;
      var valor = vistas[app][categoria][vista].filtrosFijos[i].valor;
      filtros[campo] = valor;
    }

    console.log('filtros2', filtros);
    var comoString = '';
    for (var filtro in filtros) {
      comoString += '|' + filtro + ':' + filtros[filtro];
    }

    console.log('comoString', comoString);

    return comoString.substring(1);
  };;
      return elementos;
    }
  ])
  .service('ComplementoDnt', [
    '$localStorage',
    'VistasDnt',
    function($localStorage, VistasDnt) {
      var vistas = VistasDnt;
      var elementos = {
        atajosDeRangosDeFecha: {
          '[hoy]': {
            desde: moment().format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[ayerYhoy]': {
            desde: moment()
              .subtract(1, 'days')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[estaSemana]': {
            desde: moment()
              .startOf('week')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[las2UltimasSemanas]': {
            desde: moment()
              .startOf('week')
              .subtract(1, 'week')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[las3UltimasSemanas]': {
            desde: moment()
              .startOf('week')
              .subtract(2, 'weeks')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos5Dias]': {
            desde: moment()
              .subtract(5, 'days')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos10Dias]': {
            desde: moment()
              .subtract(10, 'days')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos15Dias]': {
            desde: moment()
              .subtract(15, 'days')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos20Dias]': {
            desde: moment()
              .subtract(20, 'days')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos30Dias]': {
            desde: moment()
              .subtract(30, 'days')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[esteMes]': {
            desde: moment()
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[elMesPasado]': {
            desde: moment()
              .subtract(1, 'month')
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment()
              .subtract(1, 'month')
              .endOf('month')
              .format('YYYY-MM-DD')
          },
          '[elMesAntesPasado]': {
            desde: moment()
              .subtract(2, 'month')
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment()
              .subtract(2, 'month')
              .endOf('month')
              .format('YYYY-MM-DD')
          },
          '[losUltimos2Meses]': {
            desde: moment()
              .subtract(1, 'month')
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos3Meses]': {
            desde: moment()
              .subtract(2, 'month')
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos4Meses]': {
            desde: moment()
              .subtract(3, 'month')
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos5Meses]': {
            desde: moment()
              .subtract(4, 'month')
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos6Meses]': {
            desde: moment()
              .subtract(5, 'month')
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          }
        },
        //tipo,numero,receptorRut,fechaEmision,totalPrecioTotalNeto,totalMonedaTipo,fechaActualizacion,compradorNombre,compradorArea,emisorRut,emisorRazonSocial,receptorRazonSocial,leido
        posiblesCampos: [
          { campo: 'tipo', nombre: 'Tipo de documento', tipoFiltro: 'tag', opciones: [] },
          { campo: 'numero', nombre: 'Folio', tipoFiltro: 'tag' },
          { campo: 'emisorRut', nombre: 'Rut Emisor', tipoFiltro: 'tag' },
          { campo: 'emisorRazonSocial', nombre: 'Razón Social Emisor', tipoFiltro: 'texto' },
          { campo: 'receptorRut', nombre: 'Rut Receptor', tipoFiltro: 'tag' },
          { campo: 'receptorRazonSocial', nombre: 'Razón Social Receptor', tipoFiltro: 'texto' },
          { campo: 'fechaEmision', nombre: 'Fecha de Emisión', tipoFiltro: 'fecha' },
          { campo: 'fechaActualizacion', nombre: 'Fecha de Actualización', tipoFiltro: 'fecha' },
          { campo: 'compradorNombre', nombre: 'Nombre Comprador', tipoFiltro: 'texto' },
          { campo: 'compradorArea', nombre: 'Área Comprador', tipoFiltro: 'texto' },
          { campo: 'leido', nombre: 'Leído', tipoFiltro: 'texto' }
        ]
      };
      elementos.obtenerEquivalenteRangoFecha = function(nombre, comoObjeto) {
        if (comoObjeto === true) {
          return elementos.atajosDeRangosDeFecha[nombre];
        } else {
          return (
            elementos.atajosDeRangosDeFecha[nombre].desde +
            '--' +
            elementos.atajosDeRangosDeFecha[nombre].hasta
          );
        }
      };
      elementos.convertirFiltrosEnObjeto = function(filtrosS) {
        var filtros = {};
        var partes = filtrosS.split('|');
        for (var i = 0; i < partes.length; i++) {
          var par = partes[i].includes(':') ? partes[i].split(':') : partes[i].split('$');
          if (elementos.atajosDeRangosDeFecha.hasOwnProperty(par[1])) {
            //var rango = elementos.atajosDeRangosDeFecha[par[1]];
            //filtros[par[0]]=par[1];
            //filtros[par[0]] = rango.desde + "--" + rango.hasta;
            filtros[par[0]] = par[1];
          } else {
            filtros[par[0]] = par[1];
          }
        }
        return filtros;
      };

      elementos.prepararFiltrosDnt = function(query, app, categoria, vista) {
        var filtros = {};
        if (app == 'cloud' && categoria === 'emitidos') {
          filtros.emisorRut = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
        }

        if (app == 'cloud' && categoria === 'recibidos') {
          filtros.receptorRut = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
        }

        if (app == 'proveedores' && categoria === 'emitidos') {
          filtros.emisorRut = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
          filtros.receptorRut = $localStorage.sesiones[$localStorage.usuarioActual].cliente.iut;
        }
        if (app == 'proveedores' && categoria === 'recibidos') {
          filtros.emisorRut = $localStorage.sesiones[$localStorage.usuarioActual].cliente.iut;
          filtros.receptorRut = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
        }

        if (app == 'cliente' && categoria === 'emitidos') {
          filtros.receptorRut = $localStorage.sesiones[$localStorage.usuarioActual].proveedor.iut;
          filtros.emisorRut = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
        }
        if (app == 'cliente' && categoria === 'recibidos') {
          filtros.receptorRut = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
          filtros.emisorRut = $localStorage.sesiones[$localStorage.usuarioActual].proveedor.iut;
        }

        var tieneFiltrosDeFecha = false;
        if (typeof query.filtros !== 'undefined' && query.filtros != '') {
          var partes = query.filtros.split('|');
          for (var i = 0; i < partes.length; i++) {
            var par = partes[i].includes(':') ? partes[i].split(':') : partes[i].split('$');
            var nombreFiltro = par[0];
            var valorFiltro = par[1];
            console.log(nombreFiltro + ' = ' + valorFiltro);
            if (nombreFiltro.includes('fecha')) {
              tieneFiltrosDeFecha = true;
            }
            if (
              (categoria === 'emitidos' && nombreFiltro === 'emisorRut') ||
              (categoria === 'recibidos' && nombreFiltro === 'receptorRut')
            ) {
              //no hacer nada, porque se setea en duro por seguridad
            } else {
              if (
                nombreFiltro != '' &&
                nombreFiltro != 'undefined' &&
                typeof nombreFiltro != 'undefined'
              ) {
                filtros[nombreFiltro] = valorFiltro;
              }
            }
            if (nombreFiltro == 'tipoDocumento') {
              var tiposDelUsuario = valorFiltro.split(',');
              var tiposFinales = [];
              for (var x = 0; x < tiposDelUsuario.length; x++) {
                if (
                  vistas[app][categoria][vista].documentosDisponibles.indexOf(
                    parseInt(tiposDelUsuario[x])
                  ) >= 0
                ) {
                  tiposFinales.push(vistas[app][categoria][vista].documentosDisponibles[x]);
                }
              }
              if (tiposFinales.length == 0) {
                for (
                  var x = 0;
                  x < vistas[app][categoria][vista].documentosDisponibles.length;
                  x++
                ) {
                  tiposFinales.push(vistas[app][categoria][vista].documentosDisponibles[x]);
                }
              }
              filtros[nombreFiltro] = tiposFinales.join(',');
            }
          }
        }

        //log("FILTROS PROCESADOS",filtros);
        if (!tieneFiltrosDeFecha) {
          filtros.fechaEmision = '[losUltimos15Dias]';
        }

        //valores POR DEFECTO SI ES QUE NO VIENEN
        console.log('valores por defecto');
        console.log(vistas[app][categoria][vista].filtrosPorDefecto);
        for (var i = 0; i < vistas[app][categoria][vista].filtrosPorDefecto.length; i++) {
          console.log(i);
          var campo = vistas[app][categoria][vista].filtrosPorDefecto[i].campo;
          console.log(campo);
          var valor = vistas[app][categoria][vista].filtrosPorDefecto[i].valor;
          console.log(valor);
          if (typeof filtros[campo] == 'undefined') {
            filtros[campo] = valor;
          }
        }
        // filtros fijos
        console.log('valores por fijos');
        for (var i = 0; i < vistas[app][categoria][vista].filtrosFijos.length; i++) {
          var campo = vistas[app][categoria][vista].filtrosFijos[i].campo;
          var valor = vistas[app][categoria][vista].filtrosFijos[i].valor;
          filtros[campo] = valor;
        }

        var comoString = '';
        for (var filtro in filtros) {
          comoString += '|' + filtro + ':' + filtros[filtro];
        }

        return comoString.substring(1);
      };
      return elementos;
    }
  ])
  .service('ComplementoPortafolio', [
    '$localStorage',
    'VistasPortafolio',
    function($localStorage, VistasPortafolio) {
      var vistas = VistasPortafolio;
      var elementos = {
        atajosDeRangosDeFecha: {
          '[hoy]': {
            desde: moment().format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[ayerYhoy]': {
            desde: moment()
              .subtract(1, 'days')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[estaSemana]': {
            desde: moment()
              .startOf('week')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[las2UltimasSemanas]': {
            desde: moment()
              .startOf('week')
              .subtract(1, 'week')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[las3UltimasSemanas]': {
            desde: moment()
              .startOf('week')
              .subtract(2, 'weeks')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos5Dias]': {
            desde: moment()
              .subtract(5, 'days')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos10Dias]': {
            desde: moment()
              .subtract(10, 'days')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos15Dias]': {
            desde: moment()
              .subtract(15, 'days')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos20Dias]': {
            desde: moment()
              .subtract(20, 'days')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[esteMes]': {
            desde: moment()
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[elMesPasado]': {
            desde: moment()
              .subtract(1, 'month')
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment()
              .subtract(1, 'month')
              .endOf('month')
              .format('YYYY-MM-DD')
          },
          '[elMesAntesPasado]': {
            desde: moment()
              .subtract(2, 'month')
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment()
              .subtract(2, 'month')
              .endOf('month')
              .format('YYYY-MM-DD')
          },
          '[losUltimos2Meses]': {
            desde: moment()
              .subtract(1, 'month')
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos3Meses]': {
            desde: moment()
              .subtract(2, 'month')
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos4Meses]': {
            desde: moment()
              .subtract(3, 'month')
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos5Meses]': {
            desde: moment()
              .subtract(4, 'month')
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          },
          '[losUltimos6Meses]': {
            desde: moment()
              .subtract(5, 'month')
              .startOf('month')
              .format('YYYY-MM-DD'),
            hasta: moment().format('YYYY-MM-DD')
          }
        },
        //tipo,numero,receptorRut,fechaEmision,totalPrecioTotalNeto,totalMonedaTipo,fechaActualizacion,compradorNombre,compradorArea,emisorRut,emisorRazonSocial,receptorRazonSocial,leido
        posiblesCampos: [
          { campo: 'tipo', nombre: 'Tipo de documento', tipoFiltro: 'tag', opciones: [] },
          { campo: 'numero', nombre: 'Folio', tipoFiltro: 'tag' },
          { campo: 'emisorRut', nombre: 'Rut Emisor', tipoFiltro: 'tag' },
          { campo: 'emisorRazonSocial', nombre: 'Razón Social Emisor', tipoFiltro: 'texto' },
          { campo: 'receptorRut', nombre: 'Rut Receptor', tipoFiltro: 'tag' },
          { campo: 'receptorRazonSocial', nombre: 'Razón Social Receptor', tipoFiltro: 'texto' },
          { campo: 'fechaEmision', nombre: 'Fecha de Emisión', tipoFiltro: 'fecha' },
          { campo: 'fechaActualizacion', nombre: 'Fecha de Actualización', tipoFiltro: 'fecha' },
          { campo: 'compradorNombre', nombre: 'Nombre Comprador', tipoFiltro: 'texto' },
          { campo: 'compradorArea', nombre: 'Área Comprador', tipoFiltro: 'texto' },
          { campo: 'leido', nombre: 'Leído', tipoFiltro: 'texto' }
        ]
      };
      elementos.obtenerEquivalenteRangoFecha = function(nombre, comoObjeto) {
        if (comoObjeto === true) {
          return elementos.atajosDeRangosDeFecha[nombre];
        } else {
          return (
            elementos.atajosDeRangosDeFecha[nombre].desde +
            '--' +
            elementos.atajosDeRangosDeFecha[nombre].hasta
          );
        }
      };
      elementos.convertirFiltrosEnObjeto = function(filtrosS) {
        var filtros = {};
        var partes = filtrosS.split('|');
        for (var i = 0; i < partes.length; i++) {
          var par = partes[i].includes(':') ? partes[i].split(':') : partes[i].split('$');
          if (elementos.atajosDeRangosDeFecha.hasOwnProperty(par[1])) {
            //var rango = elementos.atajosDeRangosDeFecha[par[1]];
            //filtros[par[0]]=par[1];
            //filtros[par[0]] = rango.desde + "--" + rango.hasta;
            filtros[par[0]] = par[1];
          } else {
            filtros[par[0]] = par[1];
          }
        }
        return filtros;
      };

      elementos.prepararFiltrosDnt = function(query, app, categoria, vista) {
        var filtros = {};
        if (app == 'cloud' && categoria === 'emitidos') {
          filtros.emisorRut = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
        }

        if (app == 'cloud' && categoria === 'recibidos') {
          filtros.receptorRut = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
        }

        if (app == 'proveedores' && categoria === 'emitidos') {
          filtros.emisorRut = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
          filtros.receptorRut = $localStorage.sesiones[$localStorage.usuarioActual].cliente.iut;
        }

        if (app == 'cliente' && categoria === 'recibidos') {
          filtros.receptorRut = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
          filtros.emisorRut = $localStorage.sesiones[$localStorage.usuarioActual].proveedor.iut;
        }
        console.log('ACA');
        var tieneFiltrosDeFecha = false;
        if (typeof query.filtros !== 'undefined' && query.filtros != '') {
          var partes = query.filtros.split('|');
          for (var i = 0; i < partes.length; i++) {
            var par = partes[i].includes(':') ? partes[i].split(':') : partes[i].split('$');
            var nombreFiltro = par[0];
            var valorFiltro = par[1];
            console.log(nombreFiltro + ' = ' + valorFiltro);
            if (nombreFiltro.includes('fecha')) {
              tieneFiltrosDeFecha = true;
            }
            if (
              (categoria === 'emitidos' && nombreFiltro === 'emisorRut') ||
              (categoria === 'recibidos' && nombreFiltro === 'receptorRut')
            ) {
              //no hacer nada, porque se setea en duro por seguridad
            } else {
              if (
                nombreFiltro != '' &&
                nombreFiltro != 'undefined' &&
                typeof nombreFiltro != 'undefined'
              ) {
                filtros[nombreFiltro] = valorFiltro;
              }
            }
            if (nombreFiltro == 'tipoDocumento') {
              var tiposDelUsuario = valorFiltro.split(',');
              var tiposFinales = [];
              for (var x = 0; x < tiposDelUsuario.length; x++) {
                if (
                  vistas[app][categoria][vista].documentosDisponibles.indexOf(
                    parseInt(tiposDelUsuario[x])
                  ) >= 0
                ) {
                  tiposFinales.push(vistas[app][categoria][vista].documentosDisponibles[x]);
                }
              }
              if (tiposFinales.length == 0) {
                for (
                  var x = 0;
                  x < vistas[app][categoria][vista].documentosDisponibles.length;
                  x++
                ) {
                  tiposFinales.push(vistas[app][categoria][vista].documentosDisponibles[x]);
                }
              }
              filtros[nombreFiltro] = tiposFinales.join(',');
            }
          }
        }

        //log("FILTROS PROCESADOS",filtros);
        if (!tieneFiltrosDeFecha) {
          filtros.fechaEmision = '[losUltimos15Dias]';
        }

        //valores POR DEFECTO SI ES QUE NO VIENEN
        console.log('valores por defecto');
        console.log(vistas[app][categoria][vista].filtrosPorDefecto);
        for (var i = 0; i < vistas[app][categoria][vista].filtrosPorDefecto.length; i++) {
          console.log(i);
          var campo = vistas[app][categoria][vista].filtrosPorDefecto[i].campo;
          console.log(campo);
          var valor = vistas[app][categoria][vista].filtrosPorDefecto[i].valor;
          console.log(valor);
          if (typeof filtros[campo] == 'undefined') {
            filtros[campo] = valor;
          }
        }
        // filtros fijos
        console.log('valores por fijos');
        for (var i = 0; i < vistas[app][categoria][vista].filtrosFijos.length; i++) {
          var campo = vistas[app][categoria][vista].filtrosFijos[i].campo;
          var valor = vistas[app][categoria][vista].filtrosFijos[i].valor;
          filtros[campo] = valor;
        }

        var comoString = '';
        for (var filtro in filtros) {
          comoString += '|' + filtro + ':' + filtros[filtro];
        }

        return comoString.substring(1);
      };
      return elementos;
    }
  ])
  .service('preloaders', [
    '$rootScope',
    '$timeout',
    'utils',
    function($rootScope, $timeout, utils) {
      $rootScope.content_preloader_show = function(style, variant, container, width, height) {
        var $body = $('body');
        if (!$body.find('.content-preloader').length) {
          var image_density = utils.isHighDensity() ? '@2x' : '',
            width = width ? width : 48,
            height = height ? height : 48;

          var preloader_content =
            style == 'regular'
              ? '<img src="assets/img/spinners/spinner' +
                image_density +
                '.gif" alt="" width="32" height="32">'
              : '<div><svg x="0px" y="0px" viewBox="0 0 421 424" width="' +
                width +
                '" height="' +
                height +
                '" xmlns="http://www.w3.org/2000/svg" id="r1l9aNt2M"><style>@-webkit-keyframes ry-xg5pVF2M_HJSH1rY3z_Animation{0%{-webkit-transform: translate(700px, 0px);transform: translate(700px, 0px);}100%{-webkit-transform: translate(-1500px, 0px);transform: translate(-1500px, 0px);}}@keyframes ry-xg5pVF2M_HJSH1rY3z_Animation{0%{-webkit-transform: translate(700px, 0px);transform: translate(700px, 0px);}100%{-webkit-transform: translate(-1500px, 0px);transform: translate(-1500px, 0px);}}@-webkit-keyframes Bklgeqa4F3G_HJpGxrt2M_Animation{0%{-webkit-transform: translate(-100px, 0px);transform: translate(-100px, 0px);}100%{-webkit-transform: translate(330px, 0px);transform: translate(330px, 0px);}}@keyframes Bklgeqa4F3G_HJpGxrt2M_Animation{0%{-webkit-transform: translate(-100px, 0px);transform: translate(-100px, 0px);}100%{-webkit-transform: translate(330px, 0px);transform: translate(330px, 0px);}}@-webkit-keyframes H11el5T4tnz_BJrTerK3G_Animation{0%{-webkit-transform: translate(-1500px, 0px);transform: translate(-1500px, 0px);}100%{-webkit-transform: translate(300px, 0px);transform: translate(300px, 0px);}}@keyframes H11el5T4tnz_BJrTerK3G_Animation{0%{-webkit-transform: translate(-1500px, 0px);transform: translate(-1500px, 0px);}100%{-webkit-transform: translate(300px, 0px);transform: translate(300px, 0px);}}@-webkit-keyframes BJAecaVthz_Hkzq0VtnG_Animation{0%{-webkit-transform: translate(-200px, 0px);transform: translate(-200px, 0px);}100%{-webkit-transform: translate(-200px, 500px);transform: translate(-200px, 500px);}}@keyframes BJAecaVthz_Hkzq0VtnG_Animation{0%{-webkit-transform: translate(-200px, 0px);transform: translate(-200px, 0px);}100%{-webkit-transform: translate(-200px, 500px);transform: translate(-200px, 500px);}}@-webkit-keyframes r16g9aNt3z_rJrI04YnM_Animation{0%{-webkit-transform: translate(-150px, 0px);transform: translate(-150px, 0px);}100%{-webkit-transform: translate(200px, 0px);transform: translate(200px, 0px);}}@keyframes r16g9aNt3z_rJrI04YnM_Animation{0%{-webkit-transform: translate(-150px, 0px);transform: translate(-150px, 0px);}100%{-webkit-transform: translate(200px, 0px);transform: translate(200px, 0px);}}@-webkit-keyframes rknxqTNK3M_Sk8SxrF2M_Animation{0%{-webkit-transform: translate(300px, 0px);transform: translate(300px, 0px);}100%{-webkit-transform: translate(-300px, 0px);transform: translate(-300px, 0px);}}@keyframes rknxqTNK3M_Sk8SxrF2M_Animation{0%{-webkit-transform: translate(300px, 0px);transform: translate(300px, 0px);}100%{-webkit-transform: translate(-300px, 0px);transform: translate(-300px, 0px);}}@-webkit-keyframes HJoeqTNt3f_BkkbWBF2G_Animation{0%{-webkit-transform: translate(-400px, -400px);transform: translate(-400px, -400px);}100%{-webkit-transform: translate(400px, 0px);transform: translate(400px, 0px);}}@keyframes HJoeqTNt3f_BkkbWBF2G_Animation{0%{-webkit-transform: translate(-400px, -400px);transform: translate(-400px, -400px);}100%{-webkit-transform: translate(400px, 0px);transform: translate(400px, 0px);}}@-webkit-keyframes Hy9xqTNK3G_BkAFxHK3z_Animation{0%{-webkit-transform: translate(-1000px, 0px);transform: translate(-1000px, 0px);}100%{-webkit-transform: translate(1000px, 0px);transform: translate(1000px, 0px);}}@keyframes Hy9xqTNK3G_BkAFxHK3z_Animation{0%{-webkit-transform: translate(-1000px, 0px);transform: translate(-1000px, 0px);}100%{-webkit-transform: translate(1000px, 0px);transform: translate(1000px, 0px);}}@-webkit-keyframes rkKlcp4tnz_B1JSAEF3M_Animation{0%{-webkit-transform: translate(-500px, 0px);transform: translate(-500px, 0px);}100%{-webkit-transform: translate(500px, 0px);transform: translate(500px, 0px);}}@keyframes rkKlcp4tnz_B1JSAEF3M_Animation{0%{-webkit-transform: translate(-500px, 0px);transform: translate(-500px, 0px);}100%{-webkit-transform: translate(500px, 0px);transform: translate(500px, 0px);}}@-webkit-keyframes SyueqTEFnG_rkwRT4F2f_Animation{0%{-webkit-transform: translate(-200px, 0px);transform: translate(-200px, 0px);}100%{-webkit-transform: translate(320px, 0px);transform: translate(320px, 0px);}}@keyframes SyueqTEFnG_rkwRT4F2f_Animation{0%{-webkit-transform: translate(-200px, 0px);transform: translate(-200px, 0px);}100%{-webkit-transform: translate(320px, 0px);transform: translate(320px, 0px);}}@-webkit-keyframes H1De9aEF3z_r19s04Y2G_Animation{0%{-webkit-transform: translate(500px, 0px);transform: translate(500px, 0px);}100%{-webkit-transform: translate(-300px, 0px);transform: translate(-300px, 0px);}}@keyframes H1De9aEF3z_r19s04Y2G_Animation{0%{-webkit-transform: translate(500px, 0px);transform: translate(500px, 0px);}100%{-webkit-transform: translate(-300px, 0px);transform: translate(-300px, 0px);}}@-webkit-keyframes S1Igq64K2G_ByoDert2z_Animation{0%{-webkit-transform: translate(-1500px, 0px);transform: translate(-1500px, 0px);}100%{-webkit-transform: translate(1500px, 0px);transform: translate(1500px, 0px);}}@keyframes S1Igq64K2G_ByoDert2z_Animation{0%{-webkit-transform: translate(-1500px, 0px);transform: translate(-1500px, 0px);}100%{-webkit-transform: translate(1500px, 0px);transform: translate(1500px, 0px);}}@-webkit-keyframes SJHg5pVK3G_rkoWJSY3G_Animation{0%{-webkit-transform: translate(50px, 0px);transform: translate(50px, 0px);}100%{-webkit-transform: translate(-280px, 0px);transform: translate(-280px, 0px);}}@keyframes SJHg5pVK3G_rkoWJSY3G_Animation{0%{-webkit-transform: translate(50px, 0px);transform: translate(50px, 0px);}100%{-webkit-transform: translate(-280px, 0px);transform: translate(-280px, 0px);}}@-webkit-keyframes H14g9TVYnG_HybAAVFnz_Animation{0%{-webkit-transform: translate(100px, 0px);transform: translate(100px, 0px);}100%{-webkit-transform: translate(-400px, 0px);transform: translate(-400px, 0px);}}@keyframes H14g9TVYnG_HybAAVFnz_Animation{0%{-webkit-transform: translate(100px, 0px);transform: translate(100px, 0px);}100%{-webkit-transform: translate(-400px, 0px);transform: translate(-400px, 0px);}}@-webkit-keyframes HJ7e9p4Y3G_rkjJZSKhG_Animation{0%{-webkit-transform: translate(100px, 0px);transform: translate(100px, 0px);}100%{-webkit-transform: translate(-500px, 0px);transform: translate(-500px, 0px);}}@keyframes HJ7e9p4Y3G_rkjJZSKhG_Animation{0%{-webkit-transform: translate(100px, 0px);transform: translate(100px, 0px);}100%{-webkit-transform: translate(-500px, 0px);transform: translate(-500px, 0px);}}@-webkit-keyframes BJzlqT4FhM_rk3BWrKhM_Animation{0%{-webkit-transform: translate(-800px, 0px);transform: translate(-800px, 0px);}100%{-webkit-transform: translate(800px, 0px);transform: translate(800px, 0px);}}@keyframes BJzlqT4FhM_rk3BWrKhM_Animation{0%{-webkit-transform: translate(-800px, 0px);transform: translate(-800px, 0px);}100%{-webkit-transform: translate(800px, 0px);transform: translate(800px, 0px);}}@-webkit-keyframes rJ-g96EF3G_B1hJCEKnz_Animation{0%{-webkit-transform: translate(-150px, 0px);transform: translate(-150px, 0px);}100%{-webkit-transform: translate(400px, 0px);transform: translate(400px, 0px);}}@keyframes rJ-g96EF3G_B1hJCEKnz_Animation{0%{-webkit-transform: translate(-150px, 0px);transform: translate(-150px, 0px);}100%{-webkit-transform: translate(400px, 0px);transform: translate(400px, 0px);}}@-webkit-keyframes SJle96VFnf_rymj6VF2f_Animation{0%{-webkit-transform: translate(-100px, 0px);transform: translate(-100px, 0px);}100%{-webkit-transform: translate(300px, 0px);transform: translate(300px, 0px);}}@keyframes SJle96VFnf_rymj6VF2f_Animation{0%{-webkit-transform: translate(-100px, 0px);transform: translate(-100px, 0px);}100%{-webkit-transform: translate(300px, 0px);transform: translate(300px, 0px);}}#r1l9aNt2M *{-webkit-animation-duration: 3s;animation-duration: 3s;-webkit-animation-iteration-count: infinite;animation-iteration-count: infinite;-webkit-animation-timing-function: cubic-bezier(0, 0, 1, 1);animation-timing-function: cubic-bezier(0, 0, 1, 1);}#SJle96VFnf{fill: rgb(0, 172, 207);}#rJ-g96EF3G{fill: rgb(0, 172, 207);}#BJzlqT4FhM{fill: rgb(0, 172, 207);}#HJ7e9p4Y3G{fill: rgb(233, 89, 22);}#H14g9TVYnG{fill: rgb(233, 89, 22);}#SJHg5pVK3G{fill: rgb(233, 89, 22);}#S1Igq64K2G{-webkit-transform:  matrix(1, 0, 0, 1.000001, -38.051788, -56.638139);transform:  matrix(1, 0, 0, 1.000001, -38.051788, -56.638139);fill: rgb(0, 172, 207);}#H1De9aEF3z{fill: rgb(233, 89, 22);}#SyueqTEFnG{fill: rgb(0, 172, 207);}#rkKlcp4tnz{fill: rgb(0, 172, 207);}#Hy9xqTNK3G{fill: rgb(0, 172, 207);}#HJoeqTNt3f{fill: rgb(0, 172, 207);}#rknxqTNK3M{fill: rgb(233, 89, 22);}#r16g9aNt3z{fill: rgb(0, 172, 207);}#BJAecaVthz{fill: rgb(0, 172, 207);-webkit-transform:  matrix(0.999999, 0, 0, 1, -38.05167, -37.619385);transform:  matrix(0.999999, 0, 0, 1, -38.05167, -37.619385);}#H11el5T4tnz{-webkit-transform:  matrix(1, 0, 0, 1.000001, -27.298828, 230.173387);transform:  matrix(1, 0, 0, 1.000001, -27.298828, 230.173387);fill: rgb(0, 172, 207);}#Bklgeqa4F3G{-webkit-transform:  matrix(1, 0, 0, 1.000001, -632.706482, -930.054227);transform:  matrix(1, 0, 0, 1.000001, -632.706482, -930.054227);fill: rgb(0, 172, 207);}#ry-xg5pVF2M{-webkit-transform:  matrix(1, 0, 0, 1.000001, -365.580811, -931.99191);transform:  matrix(1, 0, 0, 1.000001, -365.580811, -931.99191);fill: rgb(233, 89, 22);}#BJGxl5p4F3z{fill: rgb(0, 172, 207);}#rymge5pVt2z{fill: rgb(233, 89, 22);}#SJle96VFnf_rymj6VF2f{-webkit-animation-name: SJle96VFnf_rymj6VF2f_Animation;animation-name: SJle96VFnf_rymj6VF2f_Animation;-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(-100px, 0px);transform: translate(-100px, 0px);}#rJ-g96EF3G_B1hJCEKnz{-webkit-animation-name: rJ-g96EF3G_B1hJCEKnz_Animation;animation-name: rJ-g96EF3G_B1hJCEKnz_Animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(-150px, 0px);transform: translate(-150px, 0px);}#BJzlqT4FhM_rk3BWrKhM{-webkit-animation-name: BJzlqT4FhM_rk3BWrKhM_Animation;animation-name: BJzlqT4FhM_rk3BWrKhM_Animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(-800px, 0px);transform: translate(-800px, 0px);}#HJ7e9p4Y3G_rkjJZSKhG{-webkit-animation-name: HJ7e9p4Y3G_rkjJZSKhG_Animation;animation-name: HJ7e9p4Y3G_rkjJZSKhG_Animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(100px, 0px);transform: translate(100px, 0px);}#H14g9TVYnG_HybAAVFnz{-webkit-animation-name: H14g9TVYnG_HybAAVFnz_Animation;animation-name: H14g9TVYnG_HybAAVFnz_Animation;-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(100px, 0px);transform: translate(100px, 0px);}#SJHg5pVK3G_rkoWJSY3G{-webkit-animation-name: SJHg5pVK3G_rkoWJSY3G_Animation;animation-name: SJHg5pVK3G_rkoWJSY3G_Animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(50px, 0px);transform: translate(50px, 0px);}#S1Igq64K2G_ByoDert2z{-webkit-animation-name: S1Igq64K2G_ByoDert2z_Animation;animation-name: S1Igq64K2G_ByoDert2z_Animation;-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(-1500px, 0px);transform: translate(-1500px, 0px);}#H1De9aEF3z_r19s04Y2G{-webkit-animation-name: H1De9aEF3z_r19s04Y2G_Animation;animation-name: H1De9aEF3z_r19s04Y2G_Animation;-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(500px, 0px);transform: translate(500px, 0px);}#SyueqTEFnG_rkwRT4F2f{-webkit-animation-name: SyueqTEFnG_rkwRT4F2f_Animation;animation-name: SyueqTEFnG_rkwRT4F2f_Animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 1, 1);animation-timing-function: cubic-bezier(0.42, 0, 1, 1);-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(-200px, 0px);transform: translate(-200px, 0px);}#rkKlcp4tnz_B1JSAEF3M{-webkit-animation-name: rkKlcp4tnz_B1JSAEF3M_Animation;animation-name: rkKlcp4tnz_B1JSAEF3M_Animation;-webkit-animation-timing-function: cubic-bezier(0, 0, 0.58, 1);animation-timing-function: cubic-bezier(0, 0, 0.58, 1);-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(-500px, 0px);transform: translate(-500px, 0px);}#Hy9xqTNK3G_BkAFxHK3z{-webkit-animation-name: Hy9xqTNK3G_BkAFxHK3z_Animation;animation-name: Hy9xqTNK3G_BkAFxHK3z_Animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 1, 1);animation-timing-function: cubic-bezier(0.42, 0, 1, 1);-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(-1000px, 0px);transform: translate(-1000px, 0px);}#HJoeqTNt3f_BkkbWBF2G{-webkit-animation-name: HJoeqTNt3f_BkkbWBF2G_Animation;animation-name: HJoeqTNt3f_BkkbWBF2G_Animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(-400px, -400px);transform: translate(-400px, -400px);}#rknxqTNK3M_Sk8SxrF2M{-webkit-animation-name: rknxqTNK3M_Sk8SxrF2M_Animation;animation-name: rknxqTNK3M_Sk8SxrF2M_Animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(300px, 0px);transform: translate(300px, 0px);}#r16g9aNt3z_rJrI04YnM{-webkit-animation-name: r16g9aNt3z_rJrI04YnM_Animation;animation-name: r16g9aNt3z_rJrI04YnM_Animation;-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(-150px, 0px);transform: translate(-150px, 0px);}#BJAecaVthz_Hkzq0VtnG{-webkit-animation-name: BJAecaVthz_Hkzq0VtnG_Animation;animation-name: BJAecaVthz_Hkzq0VtnG_Animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(-200px, 0px);transform: translate(-200px, 0px);}#H11el5T4tnz_BJrTerK3G{-webkit-animation-name: H11el5T4tnz_BJrTerK3G_Animation;animation-name: H11el5T4tnz_BJrTerK3G_Animation;-webkit-animation-timing-function: cubic-bezier(0, 0, 0.58, 1);animation-timing-function: cubic-bezier(0, 0, 0.58, 1);-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(-1500px, 0px);transform: translate(-1500px, 0px);}#Bklgeqa4F3G_HJpGxrt2M{-webkit-animation-name: Bklgeqa4F3G_HJpGxrt2M_Animation;animation-name: Bklgeqa4F3G_HJpGxrt2M_Animation;-webkit-animation-timing-function: cubic-bezier(0, 0, 0.58, 1);animation-timing-function: cubic-bezier(0, 0, 0.58, 1);-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(-100px, 0px);transform: translate(-100px, 0px);}#ry-xg5pVF2M_HJSH1rY3z{-webkit-animation-name: ry-xg5pVF2M_HJSH1rY3z_Animation;animation-name: ry-xg5pVF2M_HJSH1rY3z_Animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 1, 1);animation-timing-function: cubic-bezier(0.42, 0, 1, 1);-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;-webkit-transform: translate(700px, 0px);transform: translate(700px, 0px);}</style><g id="SJle96VFnf_rymj6VF2f" data-animator-group="true" data-animator-type="0"><rect x="72.125" y="0.835" width="56.619" height="52.748" id="SJle96VFnf"></rect></g><g id="rJ-g96EF3G_B1hJCEKnz" data-animator-group="true" data-animator-type="0"><rect x="72.883" y="107.056" width="56.619" height="52.748" id="rJ-g96EF3G"></rect></g><g id="BJzlqT4FhM_rk3BWrKhM" data-animator-group="true" data-animator-type="0"><rect x="72.883" y="370.742" width="56.619" height="52.748" id="BJzlqT4FhM"></rect></g><g id="HJ7e9p4Y3G_rkjJZSKhG" data-animator-group="true" data-animator-type="0"><rect x="293.488" y="368.839" width="56.619" height="52.748" id="HJ7e9p4Y3G"></rect></g><g id="H14g9TVYnG_HybAAVFnz" data-animator-group="true" data-animator-type="0"><rect x="293.703" y="0.49" width="56.619" height="52.748" id="H14g9TVYnG"></rect></g><g id="SJHg5pVK3G_rkoWJSY3G" data-animator-group="true" data-animator-type="0"><rect x="293.651" y="105.12" width="56.619" height="52.748" id="SJHg5pVK3G"></rect></g><g id="S1Igq64K2G_ByoDert2z" data-animator-group="true" data-animator-type="0"><rect x="251.725" y="308.871" width="56.619" height="55.652" id="S1Igq64K2G"></rect></g><g id="H1De9aEF3z_r19s04Y2G" data-animator-group="true" data-animator-type="0"><rect x="167.217" y="164.642" width="74.524" height="75.009" id="H1De9aEF3z"></rect></g><g id="SyueqTEFnG_rkwRT4F2f" data-animator-group="true" data-animator-type="0"><rect x="123.422" y="56.727" width="44.521" height="47.91" id="SyueqTEFnG"></rect></g><g id="rkKlcp4tnz_B1JSAEF3M" data-animator-group="true" data-animator-type="0"><rect x="249.725" y="48.5" width="44.521" height="47.91" id="rkKlcp4tnz"></rect></g><g id="Hy9xqTNK3G_BkAFxHK3z" data-animator-group="true" data-animator-type="0"><rect x="249.726" y="315.626" width="44.521" height="47.91" id="Hy9xqTNK3G"></rect></g><g id="HJoeqTNt3f_BkkbWBF2G" data-animator-group="true" data-animator-type="0"><rect x="153.941" y="374.665" width="44.521" height="47.91" id="HJoeqTNt3f"></rect></g><g id="rknxqTNK3M_Sk8SxrF2M" data-animator-group="true" data-animator-type="0"><rect x="131.648" y="257.555" width="44.521" height="47.91" id="rknxqTNK3M"></rect></g><g id="r16g9aNt3z_rJrI04YnM" data-animator-group="true" data-animator-type="0"><rect x="202.786" y="104.151" width="22.261" height="22.746" id="r16g9aNt3z"></rect></g><g id="BJAecaVthz_Hkzq0VtnG" data-animator-group="true" data-animator-type="0"><rect x="172.603" y="202.745" width="22.261" height="22.746" id="BJAecaVthz"></rect></g><g id="H11el5T4tnz_BJrTerK3G" data-animator-group="true" data-animator-type="0"><rect x="265.894" y="157.073" width="22.261" height="22.746" id="H11el5T4tnz"></rect></g><g id="Bklgeqa4F3G_HJpGxrt2M" data-animator-group="true" data-animator-type="0"><rect x="659.827" y="1142.12" width="101.625" height="104.045" id="Bklgeqa4F3G"></rect></g><g id="ry-xg5pVF2M_HJSH1rY3z" data-animator-group="true" data-animator-type="0"><rect x="659.827" y="1142.121" width="101.625" height="104.045" id="ry-xg5pVF2M"></rect></g><rect x="0.505" y="0.834" width="84.203" height="422.466" id="BJGxl5p4F3z"></rect><rect x="338.283" y="0.406" width="84.203" height="421.169" id="rymge5pVt2z"></rect></svg></div>';

          var thisContainer = typeof container !== 'undefined' ? $(container) : $body;

          thisContainer.append(
            '<div class="content-preloader content-preloader-' +
              variant +
              '" style="height:' +
              height +
              'px;width:' +
              width +
              'px;margin-left:-' +
              width / 2 +
              'px">' +
              preloader_content +
              '</div>'
          );
          $timeout(function() {
            $('.content-preloader').addClass('preloader-active');
          });
        }
      };
      $rootScope.content_preloader_hide = function() {
        var $body = $('body');
        if ($body.find('.content-preloader').length) {
          // hide preloader
          $('.content-preloader').removeClass('preloader-active');
          // remove preloader
          $timeout(function() {
            $('.content-preloader').remove();
          }, 500);
        }
      };
    }
  ]);

febosApp.constant('MONEDAS', [
  { id: 'CLP', title: 'Pesos', simbolo: '$', sufijo: '' },
  { id: 'UF', title: 'UF', simbolo: ' ', sufijo: '' },
  { id: 'UTM', title: 'UTM', simbolo: ' ', sufijo: '' },
  { id: 'EUR', title: 'Euro', simbolo: '€', sufijo: '' },
  { id: 'GBP', title: 'Libra esterlina', simbolo: '£', sufijo: '' },
  { id: 'USD', title: 'Dolar', simbolo: 'US$', sufijo: '' }
]);
febosApp.constant('TIPOSGARANTIA', [
  { id: 'poliza', title: 'Poliza' },
  { id: 'boleta', title: 'Boleta' },
  { id: 'pagares', title: 'Pagares' },
  { id: 'swift', title: 'Swift' },
  { id: 'certificado_de_finanza', title: 'Certificado De Fianza' },
  { id: 'cheque_internacional', title: 'Cheque Internacional' },
  { id: 'otro', title: 'Otro Tipo De Garantia' }
]);

febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $locationProvider.hashPrefix('');

                // Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
                $urlRouterProvider
                        .when('/', '/cloud/ingreso')
                        //.otherwise('/error/404');
                        .otherwise('/cloud/ingreso');
                        

                $stateProvider
                        // -- ERROR PAGES --
                        .state("error", {
                            url: "/error",
                            templateUrl: 'app/views/error.html',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit'
                                        ]);
                                    }]
                            }
                        })
                        .state("error.404", {
                            url: "/404",
                            templateUrl: 'app/components/pages/error_404View.html'
                        })
                        .state("error.500", {
                            url: "/500",
                            templateUrl: 'app/components/pages/error_500View.html'
                        })

// -- RESTRICTED --
                        .state("restricted", {
                            abstract: true,
                            url: "",
                            templateUrl: 'app/views/restricted.html',
                            resolve: {

                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_selectizeJS',
                                            'lazy_switchery',
                                            'lazy_prismJS',
                                            'lazy_autosize',
                                            'lazy_iCheck',
                                            'lazy_themes',
                                            'lazy_idle_timeout',
                                            'app/febos/ayuda/ayudaController.js'
                                        ]);
                                    }]
                            }
                        })

                        .state("restringido", {
                            abstract: true,
                            url: "",
                            templateUrl: 'app/views/restringido.html',
                            resolve: {

                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_selectizeJS',
                                            'lazy_switchery',
                                            'lazy_prismJS',
                                            'lazy_autosize',
                                            'lazy_iCheck',
                                            'lazy_themes',
                                            'lazy_idle_timeout',
                                            'app/febos/ayuda/ayudaController.js',
                                            'app/shared/footer/footerController.js'
                                        ]);
                                    }]
                            }
                        })

                        // vista "vacia" para motivos especiales (como el selector de empresas)
                        .state("especial", {
                            abstract: true,
                            url: "",
                            views: {
                                '': {
                                    templateUrl: 'app/views/especial.html'
                                }
                            },
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_selectizeJS',
                                            'lazy_switchery',
                                            'lazy_prismJS',
                                            'lazy_autosize',
                                            'lazy_iCheck',
                                            'lazy_themes',
                                            'lazy_idle_timeout',
                                            'app/shared/footer/footerController.js'
                                        ]);
                                    }]
                            }
                        })

/*
*/
/*
                        .state("restricted.listado_dte", {
                            url: "/documentos/electronicos?:pagina&:itemsPorPagina&:filtros&:orden",
                            templateUrl: 'app/febos/documentos/documentosView.html',
                            controller: 'documentosCtrl',
                            reloadOnSearch:false,
                            // runGuardsAndResolvers: 'paramsOrQueryParamsChange',
                            params:{
                              pagina:"",
                              itemsPorPagina:"" ,
                              filtros:"",
                              orden:""
                            },
                            resolve: {
                                EstadoAnterior: [
                                    "$state",
                                    function ($state) {
                                        var currentStateData = {
                                            estado: $state.current.name,
                                            parametros: $state.params,
                                            url: $state.href($state.current.name, $state.params)
                                        };
                                        return currentStateData;
                                    }
                                ],
                                Autorizacion: ['Autorizacion', function (Autorizacion) {
                                        try {
                                            Autorizacion.verificar();
                                        } catch (e) {
                                            console.log("Error al autenticar", e);
                                        }
                                        return;
                                    }],
                                Datos: ['FebosAPI','$location','$rootScope', function (FebosAPI,$location,$rootScope) {
                                        //'filtros': "rutReceptor:76179952-5|fechaRecepcion:2018-04-01--2018-05-05",
                                        return FebosAPI.cl_listar_dte({
                                            'pagina': $location.search().pagina,
                                            'itemsPorPagina': $location.search().itemsPorPagina,
                                            'campos': "tipoDocumento,folio,rutEmisor,razonSocialEmisor,rutReceptor,razonSocialReceptor,rutCesionario,razonSocialCesionario,fechaCesion,codigoSii,fechaEmision,fechaRecepcion,fechaRecepcionSii,plazo,estadoComercial,estadoSii,fechaReciboMercaderia,formaDePago,montoTotal,contacto,correoReceptor,codigoSii,fechaCesion",
                                            'filtros': $location.search().filtros,
                                            'orden': $location.search().orden
                                        }).then(function (response) {
                                            return response.data;
                                        });
                                        //return [];
                                    }],
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/febos/documentos/documentosController.js',
                                        ]);
                                    }]
                            }
                        })
*/


                        // -- PERSONALIZACION PORTAL --
                        .state("restricted.personalizacion", {
                            url: "/personalizacion",
                            templateUrl: 'app/febos/configuracion/personalizacion/personalizacionView.html',
                            controller: 'personalizacionCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/personalizacion.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            // ocLazyLoad config (app/app.js)
                                            'app/febos/configuracion/personalizacion/personalizacionController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Dashboard'
                            },
                            ncyBreadcrumb: {
                                label: 'Home'
                            }
                        })
                        //Bola de cristal
                       /* .state("restricted.bolaDeCristal", {
                            url: "/soporte/bolaDeCristal",
                            templateUrl: 'app/febos/bola_de_cristal/bolaDeCristalView.html',
                            controller: 'bolaDeCristalCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/bolaDeCristal.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            // ocLazyLoad config (app/app.js)
                                            'app/febos/bola_de_cristal/bolaDeCristalController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Bola de cristal'
                            },
                            ncyBreadcrumb: {
                                label: 'Home'
                            }
                        })*/
                        // GESTION DE LISTAS
                        .state("restricted.listas", {
                            url: "/empresa/listas",
                            templateUrl: 'app/febos/listas/gestionListasView.html',
                            controller: 'gestionListasCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/listas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            // ocLazyLoad config (app/app.js)
                                            'app/febos/listas/gestionListasController.js'
                                        ], {serie: true});
                                    }]
                            }
                        })

                        
                        .state("especial.cambioClave", {
                            url: "/:app/cambio_clave",
                            templateUrl: 'app/febos/usuarios/cambio_clave/cambioClaveView.html',
                            controller: 'cambioClaveCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/usuarios/cambio_clave/cambioClaveController.js'
                                        ]);
                                    }]
                            }
                        })

                        

                        // -- GESTIÓN DOCUMENTOS MANUALES (FM) --
                        .state("restricted.recibidos_fm", {
                            url: "/documentos/manuales/recibidos",
                            templateUrl: 'app/febos/documentos/manuales/recibidos/documentosView.html',
                            controller: 'documentosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/recibidosManuales.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/manuales/recibidos/documentosController.js'
                                        ]);
                                    }]
                            }
                        })
                        // --

                        // -- GESTIÓN DOCUMENTOS ELECTRÓNICOS (FE) --
                        .state("restricted.recibidos_bh", {
                            url: "/documentos/electronicos/honorarios/recibidos",
                            templateUrl: 'app/febos/documentos/electronicos/honorarios/documentosView.html',
                            controller: 'honorariosRecibidosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/honorariosRecibidos.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/honorarios/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })


                        .state("restricted.recibidos_fe", {
                            url: "/documentos/electronicos/recibidos",
                            templateUrl: 'app/febos/documentos/electronicos/recibidos/documentosView.html',
                            controller: 'documentosRecibidosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/documentosRecibidos.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/recibidos/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        // -- GESTIÓN DOCUMENTOS ELECTRÓNICOS PAGADOS --
                        .state("restricted.pagados", {
                            url: "/documentos/electronicos/pagados",
                            templateUrl: 'app/febos/documentos/electronicos/pagados/documentosView.html',
                            controller: 'documentosPagadosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/documentosPagados.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/pagados/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        // -- GESTIÓN DOCUMENTOS CEDIDOS --
                        .state("restricted.cedidos", {
                            url: "/documentos/electronicos/cedidos",
                            templateUrl: 'app/febos/documentos/electronicos/cedidos/documentosView.html',
                            controller: 'documentosCedidosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/documentosCedidos.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/cedidos/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        // -- GESTION DOCUMENTOS GUIAS RECIBIDAS
                        .state("restricted.recibidos_guias", {
                            url: "/documentos/electronicos/guias",
                            templateUrl: 'app/febos/documentos/electronicos/guias/documentosView.html',
                            controller: 'documentosGuiasCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/recibidosGuias.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/guias/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("restricted.recibidos_pendientes_sii", {
                            url: "/documentos/electronicos/pendientes_sii",
                            templateUrl: 'app/febos/documentos/electronicos/pendientes_sii/documentosView.html',
                            controller: 'documentosPendientesSiiCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/recibidosPendientesSii.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/pendientes_sii/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("restricted.misrecibidos", {
                            url: "/documentos/electronicos/mis_recibidos",
                            templateUrl: 'app/febos/documentos/electronicos/mis_recibidos/documentosView.html',
                            controller: 'documentosMisRecibidosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/misRecibidos.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/mis_recibidos/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("restricted.norecibidos", {
                            url: "/documentos/electronicos/no_recibidos",
                            templateUrl: 'app/febos/documentos/electronicos/no_recibidos/documentosView.html',
                            controller: 'documentosNoRecibidosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/noRecibidos.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/solicitarDte.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/no_recibidos/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("restricted.reporte", {
                            url: "/documentos/electronicos/reportes",
                            templateUrl: 'app/febos/documentos/electronicos/reportes/reporteDte.html',
                            controller: 'reporteDteCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/solicitarDte.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/reportes/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })

                        // -- GESTIÓN RECIBIDOS ORDEN DE COMPRA --
                        .state("restricted.emitidos_oc", {//  Listado de ordenes de compra recibidas
                            url: "/documentos/emitidos_oc",
                            templateUrl: 'app/febos/documentos/emitidos_oc/documentosView.html',
                            controller: 'emitidos_ocCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/ocEmitidas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/bitacoraDnt.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            'app/febos/documentos/emitidos_oc/documentosController.js'
                                        ]);
                                    }]
                            }
                        })
                        // --

                        // -- GESTIÓN RECIBIDOS HES --
                        .state("restricted.emitidos_hes", {
                            url: "/documentos/emitidos_hes",
                            templateUrl: 'app/febos/documentos/emitidos_hes/documentosView.html',
                            controller: 'emitidos_hesCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/hesEmitidas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/bitacoraDnt.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            'app/febos/documentos/emitidos_hes/documentosController.js'
                                        ]);
                                    }]
                            }
                        })
                        // --

                        // -- GESTIÓN RECIBIDOS HEM --
                        .state("restricted.emitidos_hem", {
                            url: "/documentos/emitidos_hem",
                            templateUrl: 'app/febos/documentos/emitidos_hem/documentosView.html',
                            controller: 'emitidos_hemCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/hemEmitidas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/bitacoraDnt.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            'app/febos/documentos/emitidos_hem/documentosController.js'
                                        ]);
                                    }]
                            }
                        })
                        // --

                        // flujos


                       
                        // --
                        // -- GESTIÓN DOCUMENTOS ELECTRÓNICOS MERCADO PUBLICO --
                        .state("restricted.emitidos_oc_mp", {
                            url: "/documentos/mercado_publico",
                            templateUrl: 'app/febos/documentos/mercado_publico/documentosView.html',
                            controller: 'mercadoPublicoCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/ocMercadoPublicoEmitidas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/bitacoraDnt.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/actualizarOCMP.js',
                                            'app/febos/documentos/mercado_publico/documentosController.js'
                                        ]);
                                    }]
                            }
                        })
                        // -- GESTIÓN DOCUMENTOS ELECTRÓNICOS MIS OC DE MERCADO PUBLICO --
                        .state("restricted.mis_ocmp_emitidos", {
                            url: "/documentos/mi_mercado_publico",
                            templateUrl: 'app/febos/documentos/mis_oc_mercado_publico/documentosView.html',
                            controller: 'misOcMercadoPublicoCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/ocMercadoPublicoEmitidas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/bitacoraDnt.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            'app/febos/documentos/mis_oc_mercado_publico/documentosController.js'
                                        ]);
                                    }]
                            }
                        })
                        // -- GESTIÓN DOCUMENTOS ELECTRÓNICOS (FE) --
                        .state("restricted.rechazados_fe", {
                            url: "/documentos/electronicos/rechazados",
                            templateUrl: 'app/febos/documentos/electronicos/rechazados/rechazadosView.html',
                            controller: 'rechazadosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/documentosRechazados.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/rechazados/rechazadosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        // -- GESTIÓN DOCUMENTOS ELECTRÓNICOS (FE) --
                        .state("restricted.pre_rechazados_fe", {
                            url: "/documentos/electronicos/pre_rechazados",
                            templateUrl: 'app/febos/documentos/electronicos/pre_rechazados/preRechazadosView.html',
                            controller: 'preRechazadosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/documentosPreRechazados.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/pre_rechazados/preRechazadosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        // - DTEs Boleta Honorarios
                        .state("restricted.boleta_honorarios", {
                            url: "/documentos/boleta_honorarios",
                            templateUrl: 'app/febos/documentos/boleta_honorarios/boletaView.html',
                            controller: 'boletaHonorariosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/boltasHonorarios.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/documentos/boleta_honorarios/boletaController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })

                        // - DTEs Guia de Despacho
                        .state("restricted.guia_despacho", {
                            url: "/documentos/guia_despacho",
                            templateUrl: 'app/febos/documentos/guia_despacho/guiaDespachoView.html',
                            controller: 'guiaDespachoCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/guiasDespachoRecibidas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/documentos/guia_despacho/guiaDespachoController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })

                        // --
                        //
                        // - DTEs Cesiones
                        .state("restricted.cesiones", {
                            url: "/documentos/cesiones",
                            templateUrl: 'app/febos/documentos/cesiones/cesionesView.html',
                            controller: 'cesionesCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/recibidosCedidos.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/documentos/cesiones/cesionesController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })

                        //
                        // -- Gestión de empresas
                        /*.state("restringido.empresas", {
                            url: "/:app/empresas",
                            templateUrl: 'app/febos/empresas/gestionEmpresasView.html',
                            controller: 'gestionEmpresasCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/gestionEmpresas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/empresas/gestionEmpresasController.js'
                                        ]);
                                    }]
                            }
                        })
                        */

                        // -- Gestión de sucursales
                        .state("restricted.sucursales", {
                            url: "/sucursales",
                            templateUrl: 'app/febos/sucursales/gestionSucursalesView.html',
                            controller: 'gestionSucursalesCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/gestionSucursales.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/sucursales/gestionSucursalesController.js'
                                        ]);
                                    }]
                            }
                        })


                        // -- Gestión de certificado digital
                        .state("restricted.certificado", {
                            url: "/operacion/certificado",
                            templateUrl: 'app/febos/operacion/certificado_digital/certificadoDigitalView.html',
                            controller: 'certificadoDigitalCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/certificadoDigital.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/operacion/certificado_digital/certificadoDigitalController.js'
                                        ]);
                                    }]
                            }
                        })

                        // -- Gestión de CAFs
                        .state("restricted.rangos", {
                            url: "/operacion/rangos",
                            templateUrl: 'app/febos/operacion/rangos/rangosView.html',
                            controller: 'rangosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/cafs.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/operacion/rangos/rangosController.js'
                                        ]);
                                    }]
                            }
                        })

                        // SEGURIDAD

                        // -- Roles
                        

                        /*
                        .state("restricted.logs", {
                            url: "/seguridad/logs?seguimientoId",
                            templateUrl: 'app/febos/seguridad/registros/registrosView.html',
                            controller: 'registrosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/logs.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/seguridad/registros/registrosController.js'
                                        ]);
                                    }]
                            }
                        })*/


                        // -- Actividades Usuario
                        .state("restricted.actividad", {
                            url: "/usuarios/actividad/actividad?usuarioId",
                            templateUrl: 'app/febos/usuarios/actividad/actividadView.html',
                            controller: 'actividadCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/actividad.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/usuarios/actividad/actividadController.js'
                                        ]);
                                    }]
                            }
                        })

                        // -- Tokens Activos
                        .state("restricted.tokens", {
                            url: "/seguridad/integraciones",
                            templateUrl: 'app/febos/seguridad/tokens/tokensView.html',
                            controller: 'tokensCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/tokens.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/seguridad/tokens/tokensController.js'
                                        ]);
                                    }]
                            }
                        })

                        // -- Usuarios Activos
                        .state("restricted.usuarios", {
                            url: "/usuarios",
                            templateUrl: 'app/febos/usuarios/gestion/gestionUsuariosView.html',
                            controller: 'gestionUsuariosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/gestionUsuarios.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'lazy_selectizeJS',
                                            'app/febos/usuarios/gestion/gestionUsuariosController.js'
                                        ]);
                                    }]
                            }
                        })

                        // -- Perfil
                        .state("restricted.perfil_usuario", {
                            url: "/usuarios/:usuarioId",
                            templateUrl: 'app/febos/usuarios/perfil/perfilView.html',
                            controller: 'perfilCtrl',
                            //
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/perfil.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_KendoUI',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/usuarios/perfil/perfilController.js'
                                        ]);
                                    }]
                            }
                        })
                      

                        // -- configuracion : edicion de archivos
                        .state("restricted.editor_archivos", {
                            url: "/configuracion/editor",
                            templateUrl: 'app/febos/configuracion/editor/editorView.html',
                            controller: 'editorCtrl',
                            params: {
                                objHtmlEdit: null
                            },
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/editorArchivos.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/configuracion/editor/editorController.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("restricted.layouts", {
                            url: "/configuracion/layouts",
                            templateUrl: 'app/febos/configuracion/layouts/layoutsView.html',
                            controller: 'layoutsCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/layouts.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/configuracion/layouts/layoutsController.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("restricted.parametros", {
                            url: "/configuracion/parametros",
                            templateUrl: 'app/febos/configuracion/parametros/parametrosView.html',
                            controller: 'parametrosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/parametros.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/configuracion/parametros/parametrosController.js'
                                        ]);
                                    }]
                            }
                        })

                        .state("restricted.gestorMenus", {
                            url: "/menu",
                            templateUrl: 'app/febos/menu/gestionMenusView.html',
                            controller: 'gestionMenusCtrl',
                            params: {

                            },
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/menus.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/menu/gestionMenusController.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("restricted.webhooks", {
                            url: "/webhooks",
                            templateUrl: 'app/febos/webhook/webhookView.html',
                            controller: 'webhookCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/webhook.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/webhook/webhookController.js'
                                        ]);
                                    }]
                            }
                        })
                        // -- Usuarios Activos de Proveedores
                        .state("restricted.listar_usuarios", {
                            url: "/usuarios/activos",
                            templateUrl: 'app/febos/usuarios/activos/gestionUsuariosProveedorView.html',
                            controller: 'gestionUsuariosProveedorCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/usuarios/activos/gestionUsuariosProveedorController.js'
                                        ]);
                                    }]
                            }
                        })
                        // - Emitidos (desde proveedores a cloud)
                        // -- Receptores Electrónicos

                        .state("restricted.emitidos_cloud", {
                            url: "/emitidos",
                            templateUrl: 'app/febos/documentos/electronicos/emitidos/emitidosView.html',
                            controller: 'emitidosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/receptores.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/emitidos/emitidosController.js'
                                        ]);
                                    }]
                            }
                        })

                        //Rechazados desde proveedores a cloud
                        .state("restricted.rechazados_cloud", {
                            url: "/rechazados",
                            templateUrl: 'app/febos/documentos/electronicos/rechazados/rechazadosView.html',
                            controller: 'rechazadosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/receptores.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/rechazados/rechazadosController.js'
                                        ]);
                                    }]
                            }
                        })
                        //PreRechazados desde proveedores a cloud
                        .state("restricted.pre_rechazados_cloud", {
                            url: "/preRechazados",
                            templateUrl: 'app/febos/documentos/electronicos/pre_rechazados/preRechazadosView.html',
                            controller: 'preRechazadosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/receptores.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/pre_rechazados/preRechazadosController.js'
                                        ]);
                                    }]
                            }
                        })

                        //Pagados desde proveedores a cloud
                        .state("restricted.pagados_cloud", {
                            url: "/pagados",
                            templateUrl: 'app/febos/documentos/electronicos/pagados_em/pagadosEmView.html',
                            controller: 'pagadosEmCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/receptores.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/pagados_em/pagadosEmController.js'
                                        ]);
                                    }]
                            }
                        })

                        //No entregados desde proveedores a cloud
                        .state("restricted.no_entregados_cloud", {
                            url: "/no_entregados",
                            templateUrl: 'app/febos/documentos/electronicos/no_entregados/noEntregadosView.html',
                            controller: 'noEntregadosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/receptores.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/no_entregados/noEntregadosController.js'
                                        ]);
                                    }]
                            }
                        })

                        //cedidos emitidos desde proveedores a cloud
                        .state("restricted.cedidos_cloud", {
                            url: "/cedidosEm",
                            templateUrl: 'app/febos/documentos/electronicos/cedidos_em/cedidosEmView.html',
                            controller: 'cedidosEmCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/receptores.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/cedidos_em/cedidosEmController.js'
                                        ]);
                                    }]
                            }
                        })


// ESTADOS DEL TEMPLATE - SOLO PARA EJEMPLOS

                        // -- LOGIN PAGE --
                        .state("login", {
                            url: "/login",
                            templateUrl: 'app/components/pages/loginView.html',
                            controller: 'loginCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/components/pages/loginController.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("login_v2", {
                            url: "/login_v2",
                            templateUrl: 'app/components/pages/login_v2View.html',
                            controller: 'login_v2Ctrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/components/pages/login_v2Controller.js'
                                        ]);
                                    }]
                            }
                        })





                        // -- DASHBOARD --
                        .state("restricted.dashboard", {
                            url: "/dashboard",
                            templateUrl: 'app/components/dashboard/dashboardView.html',
                            controller: 'dashboardCtrl',
                            resolve: {
                                Autorizacion: ['Autorizacion', function (Autorizacion) {
                                        try {
                                            Autorizacion.verificar();
                                        } catch (e) {
                                            console.log("Error al autenticar", e);
                                        }
                                        return;
                                    }],
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        console.log("RESOLVIENDO DEPENDECIAS EN EL DASHBOARD!");
                                        return $ocLazyLoad.load([
                                            // ocLazyLoad config (app/app.js)
                                            'lazy_countUp',
                                            'lazy_charts_peity',
                                            'lazy_charts_easypiechart',
                                            'lazy_charts_metricsgraphics',
                                            'lazy_charts_chartist',
                                            'lazy_weathericons',
                                            'lazy_clndr',
                                            'lazy_google_maps',
                                            'app/components/dashboard/dashboardController.js'
                                        ], {serie: true});
                                    }],
                                sale_chart_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/mg_dashboard_chart.min.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                user_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/user_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Dashboard'
                            },
                            ncyBreadcrumb: {
                                label: 'Home'
                            }
                        })
                        // -- FORMS --
                        .state("restricted.forms", {
                            url: "/forms",
                            template: '<div ui-view autoscroll="false"/>',
                            abstract: true
                        })
                        .state("restricted.forms.regular", {
                            url: "/regular",
                            templateUrl: 'app/components/forms/regularView.html',
                            controller: 'regularCtrl',
                            resolve: {
                                Autorizacion: ['Autorizacion', function (Autorizacion) {
                                        try {
                                            Autorizacion.verificar();
                                        } catch (e) {
                                            console.log("Error al autenticar", e);
                                        }
                                        return;
                                    }],
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/forms/regularController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Regular Elements'
                            }
                        })
                        .state("restricted.forms.advanced", {
                            url: "/advanced",
                            templateUrl: 'app/components/forms/advancedView.html',
                            controller: 'advancedCtrl',
                            resolve: {
                                Autorizacion: ['Autorizacion', function (Autorizacion) {
                                        try {
                                            Autorizacion.verificar();
                                        } catch (e) {
                                            console.log("Error al autenticar", e);
                                        }
                                        return;
                                    }],
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_ionRangeSlider',
                                            'lazy_masked_inputs',
                                            'lazy_character_counter',
                                            'bower_components/jquery-ui/jquery-ui.min.js',
                                            'lazy_uiSelect',
                                            'app/components/forms/advancedController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Advanced Elements'
                            }
                        })
                        .state("restricted.forms.dynamic", {
                            url: "/dynamic",
                            templateUrl: 'app/components/forms/dynamicView.html',
                            controller: 'dynamicCtrl',
                            resolve: {
                                Autorizacion: ['Autorizacion', function (Autorizacion) {
                                        try {
                                            Autorizacion.verificar();
                                        } catch (e) {
                                            console.log("Error al autenticar", e);
                                        }
                                        return;
                                    }],
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/forms/dynamicController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Dynamic Elements'
                            }
                        })
                        .state("restricted.forms.file_input", {
                            url: "/file_input",
                            templateUrl: 'app/components/forms/file_inputView.html',
                            controller: 'file_inputCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_dropify',
                                            'app/components/forms/file_inputController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'File Input (dropify)'
                            }
                        })
                        .state("restricted.forms.file_upload", {
                            url: "/file_upload",
                            templateUrl: 'app/components/forms/file_uploadView.html',
                            controller: 'file_uploadCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/forms/file_uploadController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'File Upload'
                            }
                        })
                        .state("restricted.forms.validation", {
                            url: "/validation",
                            templateUrl: 'app/components/forms/validationView.html',
                            controller: 'validationCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_parsleyjs',
                                            'app/components/forms/validationController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Validation'
                            }
                        })
                        .state("restricted.forms.wizard", {
                            url: "/wizard",
                            templateUrl: 'app/components/forms/wizardView.html',
                            controller: 'wizardCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_parsleyjs',
                                            'lazy_wizard',
                                            'app/components/forms/wizardController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Wizard'
                            }
                        })
                        .state("restricted.forms.wysiwyg_ckeditor", {
                            url: "/wysiwyg_ckeditor",
                            templateUrl: 'app/components/forms/wysiwyg_ckeditorView.html',
                            controller: 'ckeditorCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_ckeditor',
                                            'app/components/forms/wysiwyg_ckeditorController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'ckEditor'
                            }
                        })
                        .state("restricted.forms.wysiwyg_tinymce", {
                            url: "/wysiwyg_tinymce",
                            templateUrl: 'app/components/forms/wysiwyg_tinymceView.html',
                            controller: 'tinymceCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_tinymce',
                                            'app/components/forms/wysiwyg_tinymceController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'tinyMCE'
                            }
                        })
                        .state("restricted.forms.wysiwyg_inline", {
                            url: "/wysiwyg_inline",
                            templateUrl: 'app/components/forms/wysiwyg_ckeditorInlineView.html',
                            controller: 'ckeditorInlineCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_ckeditor',
                                            'app/components/forms/wysiwyg_ckeditorInlineController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'ckEditor inline'
                            }
                        })

                        // -- LAYOUT --
                        .state("restricted.layout", {
                            url: "/layout",
                            template: '<div ui-view autoscroll="false"/>',
                            abstract: true
                        })
                        .state("restricted.layout.top_menu", {
                            url: "/top_menu",
                            templateUrl: 'app/components/layout/top_menuView.html',
                            controller: 'top_menuCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/layout/top_menuController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Top Menu'
                            }
                        })
                        .state("restricted.layout.full_header", {
                            url: "/full_header",
                            templateUrl: 'app/components/layout/full_headerView.html',
                            controller: 'full_headerCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/layout/full_headerController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Full Header'
                            }
                        })

                        // -- KENDO UI --
                        .state("restricted.kendoui", {
                            url: "/kendoui",
                            template: '<div ui-view autoscroll="false"/>',
                            abstract: true,
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('lazy_KendoUI');
                                    }]
                            }
                        })
                        .state("restricted.kendoui.autocomplete", {
                            url: "/autocomplete",
                            templateUrl: 'app/components/kendoUI/autocompleteView.html',
                            controller: 'autocompleteCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/autocompleteController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Autocomplete (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.calendar", {
                            url: "/calendar",
                            templateUrl: 'app/components/kendoUI/calendarView.html',
                            controller: 'calendarCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/calendarController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Calendar (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.colorpicker", {
                            url: "/colorPicker",
                            templateUrl: 'app/components/kendoUI/colorPickerView.html',
                            data: {
                                pageTitle: 'ColorPicker (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.combobox", {
                            url: "/combobox",
                            templateUrl: 'app/components/kendoUI/comboboxView.html',
                            controller: 'comboboxCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/comboboxController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Combobox (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.datepicker", {
                            url: "/datepicker",
                            templateUrl: 'app/components/kendoUI/datepickerView.html',
                            controller: 'datepickerCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/datepickerController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Datepicker (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.datetimepicker", {
                            url: "/datetimepicker",
                            templateUrl: 'app/components/kendoUI/datetimepickerView.html',
                            controller: 'datetimepickerCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/datetimepickerController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Datetimepicker (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.dropdown_list", {
                            url: "/dropdown_list",
                            templateUrl: 'app/components/kendoUI/dropdown_listView.html',
                            controller: 'dropdownListCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/dropdown_listController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Dropdown List (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.masked_input", {
                            url: "/masked_input",
                            templateUrl: 'app/components/kendoUI/masked_inputView.html',
                            controller: 'maskedInputCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/masked_inputController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Masked Input (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.menu", {
                            url: "/menu",
                            templateUrl: 'app/components/kendoUI/menuView.html',
                            controller: 'menuCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/menuController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Menu (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.multiselect", {
                            url: "/multiselect",
                            templateUrl: 'app/components/kendoUI/multiselectView.html',
                            controller: 'multiselectCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/multiselectController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Multiselect (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.numeric_textbox", {
                            url: "/numeric_textbox",
                            templateUrl: 'app/components/kendoUI/numeric_textboxView.html',
                            controller: 'numericTextboxCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/numeric_textboxController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Numeric textbox (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.panelbar", {
                            url: "/panelbar",
                            templateUrl: 'app/components/kendoUI/panelbarView.html',
                            data: {
                                pageTitle: 'PanelBar (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.timepicker", {
                            url: "/timepicker",
                            templateUrl: 'app/components/kendoUI/timepickerView.html',
                            data: {
                                pageTitle: 'Timepicker (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.toolbar", {
                            url: "/toolbar",
                            templateUrl: 'app/components/kendoUI/toolbarView.html',
                            controller: 'toolbarCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/toolbarController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Toolbar (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.window", {
                            url: "/window",
                            templateUrl: 'app/components/kendoUI/windowView.html',
                            controller: 'windowCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/windowController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Window (kendoUI)'
                            }
                        })
                        // -- COMPONENTS --
                        .state("restricted.components", {
                            url: "/components",
                            template: '<div ui-view autoscroll="false" ng-class="{ \'uk-height-1-1\': page_full_height }"/>',
                            abstract: true,
                            ncyBreadcrumb: {
                                label: 'Components'
                            }
                        })
                        .state("restricted.components.accordion", {
                            url: "/accordion",
                            controller: 'accordionCtrl',
                            templateUrl: 'app/components/components/accordionView.html',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/accordionController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Accordion (components)'
                            }
                        })
                        .state("restricted.components.animations", {
                            url: "/animations",
                            templateUrl: 'app/components/components/animationsView.html',
                            controller: 'animationsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/animationsController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Animations (MD)'
                            }
                        })
                        .state("restricted.components.autocomplete", {
                            url: "/autocomplete",
                            templateUrl: 'app/components/components/autocompleteView.html',
                            controller: 'autocompleteCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/autocompleteController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Autocomplete (components)'
                            }
                        })
                        .state("restricted.components.breadcrumbs", {
                            url: "/breadcrumbs",
                            templateUrl: 'app/components/components/breadcrumbsView.html',
                            controller: 'breadcrumbsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/breadcrumbsController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Breadcrumbs (components)'
                            },
                            ncyBreadcrumb: {
                                label: 'Breadcrumbs'
                            }})
                        .state("restricted.components.buttons", {
                            url: "/buttons",
                            templateUrl: 'app/components/components/buttonsView.html',
                            data: {
                                pageTitle: 'Buttons (components)'
                            }
                        })
                        .state("restricted.components.buttons_fab", {
                            url: "/buttons_fab",
                            templateUrl: 'app/components/components/fabView.html',
                            data: {
                                pageTitle: 'Buttons FAB (components)'
                            }
                        })
                        .state("restricted.components.cards", {
                            url: "/cards",
                            templateUrl: 'app/components/components/cardsView.html',
                            controller: 'cardsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/cardsController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Cards (components)'
                            }
                        })
                        .state("restricted.components.colors", {
                            url: "/colors",
                            templateUrl: 'app/components/components/colorsView.html',
                            data: {
                                pageTitle: 'Colors (components)'
                            }
                        })
                        .state("restricted.components.common", {
                            url: "/common",
                            templateUrl: 'app/components/components/commonView.html',
                            data: {
                                pageTitle: 'Common (components)'
                            }
                        })
                        .state("restricted.components.dropdowns", {
                            url: "/dropdowns",
                            templateUrl: 'app/components/components/dropdownsView.html',
                            data: {
                                pageTitle: 'Dropdowns (components)'
                            }
                        })
                        .state("restricted.components.dynamic_grid", {
                            url: "/dynamic_grid",
                            templateUrl: 'app/components/components/dynamic_gridView.html',
                            data: {
                                pageTitle: 'Dynamic Grid (components)'
                            }
                        })
                        .state("restricted.components.footer", {
                            url: "/footer",
                            templateUrl: 'app/components/components/footerView.html',
                            controller: 'footerCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/footerController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Footer (components)'
                            }
                        })
                        .state("restricted.components.grid", {
                            url: "/grid",
                            templateUrl: 'app/components/components/gridView.html',
                            data: {
                                pageTitle: 'Grid (components)'
                            }
                        })
                        .state("restricted.components.icons", {
                            url: "/icons",
                            templateUrl: 'app/components/components/iconsView.html',
                            data: {
                                pageTitle: 'Icons (components)'
                            }
                        })
                        .state("restricted.components.list_grid_view", {
                            url: "/list_grid_view",
                            templateUrl: 'app/components/components/list_gridView.html',
                            controller: 'list_gridCtrl',
                            resolve: {
                                products_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/ecommerce_products.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/list_gridController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Lists/Grid View (components)'
                            }
                        })
                        .state("restricted.components.lists", {
                            url: "/lists",
                            templateUrl: 'app/components/components/listsView.html',
                            data: {
                                pageTitle: 'Lists (components)'
                            }
                        })
                        .state("restricted.components.modal", {
                            url: "/modal",
                            templateUrl: 'app/components/components/modalView.html',
                            data: {
                                pageTitle: 'Modals/Lightboxes (components)'
                            }
                        })
                        .state("restricted.components.nestable", {
                            url: "/nestable",
                            templateUrl: 'app/components/components/nestableView.html',
                            controller: 'nestableCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/nestableController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Nestable (components)'
                            }
                        })
                        .state("restricted.components.notifications", {
                            url: "/notifications",
                            templateUrl: 'app/components/components/notificationsView.html',
                            controller: 'notificationsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/notificationsController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Notifications (components)'
                            }
                        })
                        .state("restricted.components.panels", {
                            url: "/panels",
                            templateUrl: 'app/components/components/panelsView.html',
                            data: {
                                pageTitle: 'Panels (components)'
                            }
                        })
                        .state("restricted.components.preloaders", {
                            url: "/preloaders",
                            templateUrl: 'app/components/components/preloadersView.html',
                            controller: 'preloadersCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/preloadersController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Preloaders (components)'
                            }
                        })
                        .state("restricted.components.slider", {
                            url: "/slider",
                            templateUrl: 'app/components/components/sliderView.html',
                            data: {
                                pageTitle: 'Slider (components)'
                            }
                        })
                        .state("restricted.components.slideshow", {
                            url: "/slideshow",
                            templateUrl: 'app/components/components/slideshowView.html',
                            data: {
                                pageTitle: 'Slideshow (components)'
                            }
                        })
                        .state("restricted.components.smooth_scroll", {
                            url: "/smooth_scroll",
                            templateUrl: 'app/components/components/smooth_scrollView.html',
                            data: {
                                pageTitle: 'Smooth Scroll (components)'
                            }
                        })
                        .state("restricted.components.sortable", {
                            url: "/sortable",
                            templateUrl: 'app/components/components/sortableView.html',
                            controller: 'sortableCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_dragula',
                                            'app/components/components/sortableController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Sortable (components)'
                            }
                        })
                        .state("restricted.components.switcher", {
                            url: "/switcher",
                            templateUrl: 'app/components/components/switcherView.html',
                            data: {
                                pageTitle: 'Switcher (components)'
                            }
                        })
                        .state("restricted.components.tables_examples", {
                            url: "/tables_examples",
                            templateUrl: 'app/components/components/tables_examplesView.html',
                            controller: 'tables_examplesCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/components/tables_examplesController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Tables Examples (components)'
                            }
                        })
                        .state("restricted.components.tables", {
                            url: "/tables",
                            templateUrl: 'app/components/components/tablesView.html',
                            data: {
                                pageTitle: 'Tables (components)'
                            }
                        })
                        .state("restricted.components.tabs", {
                            url: "/tabs",
                            templateUrl: 'app/components/components/tabsView.html',
                            data: {
                                pageTitle: 'Tabs (components)'
                            }
                        })
                        .state("restricted.components.timeline", {
                            url: "/timeline",
                            templateUrl: 'app/components/components/timelineView.html',
                            controller: 'timelineCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/components/timelineController.js'
                                        ]);
                                    }],
                                user_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/user_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Timeline'
                            }
                        })
                        .state("restricted.components.tooltips", {
                            url: "/tooltips",
                            templateUrl: 'app/components/components/tooltipsView.html',
                            data: {
                                pageTitle: 'Tooltips (components)'
                            }
                        })
                        .state("restricted.components.typography", {
                            url: "/typography",
                            templateUrl: 'app/components/components/typographyView.html',
                            data: {
                                pageTitle: 'Typography (components)'
                            }
                        })
                        // -- E-COMMERCE --
                        .state("restricted.ecommerce", {
                            url: "/ecommerce",
                            template: '<div ui-view autoscroll="false"/>',
                            abstract: true
                        })
                        .state("restricted.ecommerce.payment_page", {
                            url: "/payment_page",
                            templateUrl: 'app/components/ecommerce/paymentView.html',
                            controller: 'paymentCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/ecommerce/paymentController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Product Details'
                            }
                        })
                        .state("restricted.ecommerce.product_details", {
                            url: "/product_details",
                            templateUrl: 'app/components/ecommerce/product_detailsView.html',
                            controller: 'productCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/ecommerce/productController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Product Details'
                            }
                        })
                        .state("restricted.ecommerce.product_edit", {
                            url: "/product_edit",
                            templateUrl: 'app/components/ecommerce/product_editView.html',
                            controller: 'productCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/ecommerce/productController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Product Edit'
                            }
                        })
                        .state("restricted.ecommerce.products_list", {
                            url: "/products_list",
                            templateUrl: 'app/components/ecommerce/products_listView.html',
                            controller: 'products_listCtrl',
                            resolve: {
                                products_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/ecommerce_products.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_pagination',
                                            'app/components/ecommerce/products_listController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Products List'
                            }
                        })
                        .state("restricted.ecommerce.products_grid", {
                            url: "/products_grid",
                            templateUrl: 'app/components/ecommerce/products_gridView.html',
                            controller: 'products_gridCtrl',
                            resolve: {
                                products_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/ecommerce_products.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/ecommerce/products_gridController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Products Grid'
                            }
                        })
                        // -- PLUGINS --
                        .state("restricted.plugins", {
                            url: "/plugins",
                            template: '<div ui-view autoscroll="false"/>',
                            abstract: true
                        })
                        .state("restricted.plugins.calendar", {
                            url: "/calendar",
                            templateUrl: 'app/components/plugins/calendarView.html',
                            controller: 'calendarCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_fullcalendar',
                                            'app/components/plugins/calendarController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Calendar'
                            }
                        })
                        .state("restricted.plugins.code_editor", {
                            url: "/code_editor",
                            templateUrl: 'app/components/plugins/code_editorView.html',
                            controller: 'code_editorCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_codemirror',
                                            'app/components/plugins/code_editorController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Code Editor'
                            }
                        })
                        .state("restricted.plugins.charts", {
                            url: "/charts",
                            templateUrl: 'app/components/plugins/chartsView.html',
                            controller: 'chartsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_charts_chartist',
                                            'lazy_charts_metricsgraphics',
                                            'lazy_charts_c3',
                                            'app/components/plugins/chartsController.js'
                                        ], {serie: true});
                                    }],
                                mg_chart_linked_1: function ($http) {
                                    return $http({method: 'GET', url: 'data/mg_brief-1.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                mg_chart_linked_2: function ($http) {
                                    return $http({method: 'GET', url: 'data/mg_brief-2.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                mg_confidence_band: function ($http) {
                                    return $http({method: 'GET', url: 'data/mg_confidence_band.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                mg_currency: function ($http) {
                                    return $http({method: 'GET', url: 'data/mg_some_currency.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Charts'
                            }
                        })
                        .state("restricted.plugins.charts_echarts", {
                            url: "/echarts",
                            templateUrl: 'app/components/plugins/charts_echartsView.html',
                            controller: 'charts_echartsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_echarts',
                                            'app/components/plugins/charts_echartsController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Charts (echarts)'
                            }
                        })
                        .state("restricted.plugins.crud_table", {
                            url: "/crud_table",
                            templateUrl: 'app/components/plugins/crud_tableView.html',
                            controller: 'crud_tableCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_masked_inputs',
                                            'app/components/plugins/crud_tableController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Context Menu'
                            }
                        })
                        .state("restricted.plugins.context_menu", {
                            url: "/context_menu",
                            templateUrl: 'app/components/plugins/context_menuView.html',
                            controller: 'context_menuCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_context_menu',
                                            'app/components/plugins/context_menuController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Context Menu'
                            }
                        })
                        .state("restricted.plugins.datatables", {
                            url: "/datatables",
                            templateUrl: 'app/components/plugins/datatablesView.html',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'bower_components/angular-resource/angular-resource.min.js',
                                            'lazy_datatables',
                                            'app/components/plugins/datatablesController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Datatables'
                            }
                        })
                        .state("restricted.plugins.diff_view", {
                            url: "/diff_view",
                            templateUrl: 'app/components/plugins/diff_viewView.html',
                            controller: 'diff_viewCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_diff',
                                            'app/components/plugins/diff_viewController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Diff View'
                            }
                        })
                        .state("restricted.plugins.filemanager", {
                            url: "/filemanager",
                            controller: 'filemanagerCtrl',
                            templateUrl: 'app/components/plugins/filemanagerView.html',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_filemanager',
                                            'app/components/plugins/filemanagerController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'FileManager'
                            }
                        })
                        .state("restricted.plugins.gantt_chart", {
                            url: "/gantt_chart",
                            controller: 'gantt_chartCtrl',
                            templateUrl: 'app/components/plugins/gantt_chartView.html',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_gantt_chart',
                                            'app/components/plugins/gantt_chartController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Gantt Chart'
                            }
                        })
                        .state("restricted.plugins.google_maps", {
                            url: "/google_maps",
                            templateUrl: 'app/components/plugins/google_mapsView.html',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_google_maps',
                                            'app/components/plugins/google_mapsController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Google Maps'
                            }
                        })
                        .state("restricted.plugins.image_cropper", {
                            url: "/image_cropper",
                            templateUrl: 'app/components/plugins/image_cropperView.html',
                            controller: 'image_cropperCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_image_cropper',
                                            'app/components/plugins/image_cropperController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Google Maps'
                            }
                        })
                        .state("restricted.plugins.idle_timeout", {
                            url: "/idle_timeout",
                            templateUrl: 'app/components/plugins/idle_timeoutView.html',
                            controller: 'idle_timeoutCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_idle_timeout',
                                            'app/components/plugins/idle_timeoutController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Idle Timeout'
                            }
                        })
                        .state("restricted.plugins.push_notifications", {
                            url: "/push_notifications",
                            templateUrl: 'app/components/plugins/push_notificationsView.html',
                            controller: 'push_notificationsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            // push.js
                                            'bower_components/push.js/bin/push.min.js',
                                            'app/components/plugins/push_notificationsController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Push Notifications'
                            }
                        })
                        .state("restricted.plugins.tablesorter", {
                            url: "/tablesorter",
                            templateUrl: 'app/components/plugins/tablesorterView.html',
                            controller: 'tablesorterCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_ionRangeSlider',
                                            'lazy_tablesorter',
                                            'app/components/plugins/tablesorterController.js'
                                        ], {serie: true});
                                    }],
                                ts_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/tablesorter.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Tablesorter'
                            }
                        })
                        .state("restricted.plugins.tour", {
                            url: "/tour",
                            templateUrl: 'app/components/plugins/tourView.html',
                            controller: 'tourCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_tour',
                                            'app/components/plugins/tourController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Tour'
                            }
                        })
                        .state("restricted.plugins.tree", {
                            url: "/tree",
                            templateUrl: 'app/components/plugins/treeView.html',
                            controller: 'treeCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_iCheck',
                                            'lazy_tree',
                                            'app/components/plugins/treeController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Tree'
                            }
                        })
                        .state("restricted.plugins.vector_maps", {
                            url: "/vector_maps",
                            templateUrl: 'app/components/plugins/vector_mapsView.html',
                            controller: 'vector_mapsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_vector_maps',
                                            'app/components/plugins/vector_mapsController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Vector Maps'
                            }
                        })

                        // -- PAGES --
                        .state("restricted.pages", {
                            url: "/pages",
                            template: '<div ui-view autoscroll="false" ng-class="{ \'uk-height-1-1\': page_full_height }" />',
                            abstract: true
                        })
                        .state("restricted.pages.blank", {
                            url: "/blank",
                            templateUrl: 'app/components/pages/blankView.html',
                            data: {
                                pageTitle: 'Blank Page'
                            }
                        })
                        .state("restricted.pages.chat", {
                            url: "/chat",
                            templateUrl: 'app/components/pages/chatView.html',
                            controller: 'chatCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/chatController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Chat'
                            }
                        })
                        .state("restricted.pages.chatboxes", {
                            url: "/chatboxes",
                            templateUrl: 'app/components/pages/chatboxesView.html',
                            controller: 'chatboxesCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/chatboxesController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Chatboxes'
                            }
                        })
                        .state("restricted.pages.contact_list", {
                            url: "/contact_list",
                            templateUrl: 'app/components/pages/contact_listView.html',
                            controller: 'contact_listCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/contact_listController.js'
                                        ], {serie: true});
                                    }],
                                contact_list: function ($http) {
                                    return $http({method: 'GET', url: 'data/contact_list.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Contact List'
                            }
                        })
                        .state("restricted.pages.contact_list_horizontal", {
                            url: "/contact_list_horizontal",
                            templateUrl: 'app/components/pages/contact_list_horizontalView.html',
                            controller: 'contact_list_horizontalCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/contact_list_horizontalController.js'
                                        ], {serie: true});
                                    }],
                                contact_list: function ($http) {
                                    return $http({method: 'GET', url: 'data/contact_list.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Contact List Horizontal'
                            }
                        })
                        .state("restricted.pages.contact_list_v2", {
                            url: "/contact_list_v2",
                            templateUrl: 'app/components/pages/contact_list_v2View.html',
                            controller: 'contact_list_v2Ctrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_listNav',
                                            'app/components/pages/contact_list_v2Controller.js'
                                        ], {serie: true});
                                    }],
                                contact_list: function ($http) {
                                    return $http({method: 'GET', url: 'data/contact_list.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Contact List v2'
                            }
                        })
                        .state("restricted.pages.gallery", {
                            url: "/gallery",
                            templateUrl: 'app/components/pages/galleryView.html',
                            controller: 'galleryCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/galleryController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Gallery'
                            }
                        })
                        .state("restricted.pages.gallery_v2", {
                            url: "/gallery_v2",
                            templateUrl: 'app/components/pages/gallery_v2View.html',
                            controller: 'gallery_v2Ctrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/gallery_v2Controller.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Gallery'
                            }
                        })
                        .state("restricted.pages.help", {
                            url: "/help",
                            templateUrl: 'app/components/pages/helpView.html',
                            controller: 'helpCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/helpController.js'
                                        ], {serie: true});
                                    }],
                                help_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/help_faq.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Help Center'
                            }
                        })
                        .state("restricted.pages.invoices", {
                            url: "/invoices",
                            abstract: true,
                            templateUrl: 'app/components/pages/invoices_listView.html',
                            controller: 'invoicesCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/pages/invoicesController.js');
                                    }],
                                invoices_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/invoices_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            }
                        })
                        .state("restricted.pages.invoices.list", {
                            url: "/list",
                            views: {
                                'ivoice_content': {
                                    templateUrl: 'app/components/pages/invoices_blankView.html',
                                    controller: 'invoicesCtrl'
                                }
                            },
                            data: {
                                pageTitle: 'Invoices'
                            }
                        })
                        .state("restricted.pages.invoices.details", {
                            url: "/details/{invoiceId:[0-9]{1,4}}",
                            views: {
                                'ivoice_content': {
                                    templateUrl: 'app/components/pages/invoices_detailView.html',
                                    controller: 'invoicesCtrl'
                                }
                            },
                            params: {hidePreloader: true}
                        })
                        .state("restricted.pages.invoices.add", {
                            url: "/add",
                            views: {
                                'ivoice_content': {
                                    templateUrl: 'app/components/pages/invoices_addView.html',
                                    controller: 'invoicesCtrl'
                                }
                            },
                            params: {hidePreloader: true}
                        })
                        .state("restricted.pages.mailbox", {
                            url: "/mailbox",
                            templateUrl: 'app/components/pages/mailboxView.html',
                            controller: 'mailboxCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/pages/mailboxController.js');
                                    }],
                                messages: function ($http) {
                                    return $http({method: 'GET', url: 'data/mailbox_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Mailbox'
                            }
                        })
                        .state("restricted.pages.mailbox_v2", {
                            url: "/mailbox_v2",
                            templateUrl: 'app/components/pages/mailbox_v2View.html',
                            controller: 'mailbox_v2Ctrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/pages/mailbox_v2Controller.js');
                                    }],
                                messages: function ($http) {
                                    return $http({method: 'GET', url: 'data/mailbox_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Mailbox v2'
                            }
                        })
                        .state("restricted.pages.notes", {
                            url: "/notes",
                            templateUrl: 'app/components/pages/notesView.html',
                            controller: 'notesCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/notesController.js'
                                        ]);
                                    }],
                                notes_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/notes_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Notes'
                            }
                        })
                        .state("restricted.pages.pricing_tables", {
                            url: "/pricing_tables",
                            templateUrl: 'app/components/pages/pricing_tablesView.html',
                            data: {
                                pageTitle: 'Pricing Tables'
                            }
                        })
                        .state("restricted.pages.pricing_tables_v2", {
                            url: "/pricing_tables_v2",
                            templateUrl: 'app/components/pages/pricing_tables_v2View.html',
                            data: {
                                pageTitle: 'Pricing Tables v2'
                            }
                        })
                        .state("restricted.pages.scrum_board", {
                            url: "/scrum_board",
                            templateUrl: 'app/components/pages/scrum_boardView.html',
                            controller: 'scrum_boardCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_dragula',
                                            'app/components/pages/scrum_boardController.js'
                                        ], {serie: true});
                                    }],
                                tasks_list: function ($http) {
                                    return $http({method: 'GET', url: 'data/tasks_list.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Scrum Board'
                            }
                        })
                        .state("restricted.pages.search_results", {
                            url: "/search_results",
                            templateUrl: 'app/components/pages/search_resultsView.html',
                            controller: 'search_resultsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'js!https://maps.google.com/maps/api/js',
                                            'lazy_google_maps',
                                            'app/components/pages/search_resultsController.js'
                                        ], {serie: true})
                                    }]
                            },
                            data: {
                                pageTitle: 'Search Results'
                            }
                        })
                        .state("restricted.pages.sticky_notes", {
                            url: "/sticky_notes",
                            templateUrl: 'app/components/pages/sticky_notesView.html',
                            controller: 'sticky_notesCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/sticky_notesController.js'
                                        ], {serie: true})
                                    }]
                            },
                            data: {
                                pageTitle: 'Sticky Notes'
                            }
                        })
                        .state("restricted.pages.settings", {
                            url: "/settings",
                            templateUrl: 'app/components/pages/settingsView.html',
                            controller: 'settingsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/pages/settingsController.js')
                                    }]
                            },
                            data: {
                                pageTitle: 'Settings'
                            }
                        })
                        .state("restricted.pages.snippets", {
                            url: "/snippets",
                            templateUrl: 'app/components/pages/snippetsView.html',
                            controller: 'snippetsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/snippetsController.js'
                                        ]);
                                    }],
                                snippets_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/snippets.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Snippets'
                            }
                        })
                        .state("restricted.pages.todo", {
                            url: "/todo",
                            templateUrl: 'app/components/pages/todoView.html',
                            controller: 'todoCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/todoController.js'
                                        ]);
                                    }],
                                todo_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/todo_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'User profile'
                            }
                        })
                        .state("restricted.pages.user_profile", {
                            url: "/user_profile",
                            templateUrl: 'app/components/pages/user_profileView.html',
                            controller: 'user_profileCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/user_profileController.js'
                                        ]);
                                    }],
                                user_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/user_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'User profile'
                            }
                        })
                        .state("restricted.pages.user_edit", {
                            url: "/user_edit",
                            templateUrl: 'app/components/pages/user_editView.html',
                            controller: 'user_editCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/js/custom/uikit_fileinput.min.js',
                                            'app/components/pages/user_editController.js'
                                        ], {serie: true});
                                    }],
                                user_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/user_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                groups_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/groups_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'User edit'
                            }
                        })
                        .state("restricted.pages.quiz", {
                            url: "/quiz",
                            templateUrl: 'app/components/pages/quizView.html',
                            controller: 'quizCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_quiz',
                                            'app/components/pages/quizController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Quiz'
                            }
                        })
                        .state("restricted.pages.issues", {
                            url: "/issues",
                            abstract: true,
                            template: '<div ui-view autoscroll="false" ng-class="{ \'uk-height-1-1\': page_full_height }" />',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_tablesorter',
                                            'app/components/pages/issuesController.js'
                                        ]);
                                    }],
                                issues_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/issues.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            }
                        })
                        .state("restricted.pages.issues.list", {
                            url: "/list",
                            templateUrl: 'app/components/pages/issues_listView.html',
                            controller: 'issuesCtrl',
                            data: {
                                pageTitle: 'Issues List'
                            }
                        })
                        .state("restricted.pages.issues.details", {
                            url: "/details/{issueId:[0-9]{1,4}}",
                            controller: 'issuesCtrl',
                            templateUrl: 'app/components/pages/issue_detailsView.html',
                            data: {
                                pageTitle: 'Issue Details'
                            }
                        })
                        .state("restricted.pages.blog", {
                            url: "/blog",
                            template: '<div ui-view autoscroll="false" ng-class="{ \'uk-height-1-1\': page_full_height }" />',
                            controller: 'blogCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/blogController.js'
                                        ]);
                                    }],
                                blog_articles: function ($http) {
                                    return $http({method: 'GET', url: 'data/blog_articles.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            abstract: true
                        })
                        .state("restricted.pages.blog.list", {
                            url: "/list",
                            controller: 'blogCtrl',
                            templateUrl: 'app/components/pages/blog_listView.html',
                            data: {
                                pageTitle: 'Blog List'
                            }
                        })
                        .state("restricted.pages.blog.article", {
                            url: "/article/{articleId:[0-9]{1,4}}",
                            controller: 'blogCtrl',
                            templateUrl: 'app/components/pages/blog_articleView.html',
                            data: {
                                pageTitle: 'Blog Article'
                            }
                        })
            }
        ]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('restringido.aprobacion', {
        url: '/:app/aprobaciones',
        templateUrl: 'app/febos/aprobaciones/lista/aprobacionesView.html',
        controller: 'aprobacionesCtrl',
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/aprobaciones/lista/aprobacionesController.min.js',
                'app/febos/aprobaciones/aprobaciones.css'
              ]);
            }
          ]
        }
      })
      .state('restringido.aprobacionver', {
        url: '/:app/aprobaciones/ver/:aprobacion',
        templateUrl: 'app/febos/aprobaciones/crear/crear-view.html',
        controller: 'aprobacionesCrearCtrl',
        params: { ver: true },
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'lazy_ionRangeSlider',
                'app/febos/aprobaciones/crear/aprobacionesCrearController.min.js',
                'app/febos/aprobaciones/aprobaciones.css'
              ]);
            }
          ]
        }
      })

      .state('restringido.aprobacioncrear', {
        url: '/:app/aprobaciones/crear/:aprobacion',
        templateUrl: 'app/febos/aprobaciones/crear/crear-view.html',
        controller: 'aprobacionesCrearCtrl',
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'lazy_ionRangeSlider',
                'app/febos/aprobaciones/crear/aprobacionesCrearController.min.js',
                'app/febos/aprobaciones/aprobaciones.css'
              ]);
            }
          ]
        }
      })
      .state('restringido.aprobacionesbandejaobservador', {
        url: '/:app/aprobacionesbandejaobservador?pagina=&itemsPorPagina=&filtros',
        templateUrl: 'app/febos/aprobaciones/bandeja/aprobacionesBandeja.html',
        controller: 'aprobacionesBandejaCtrl',
        params: {
          pagina: '1',
          itemsPorPagina: '20',
          filtros: '',
          orden: '',
          bandeja: 'OBSERVADOR'
        },
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/aprobaciones/bandeja/aprobacionesBandejaController.min.js',
                'app/febos/aprobaciones/aprobaciones.css'
              ]);
            }
          ]
        }
      })
      .state('restringido.aprobacionesbandeja', {
        url: '/:app/aprobacionesbandeja?pagina=&itemsPorPagina=&filtros',
        templateUrl: 'app/febos/aprobaciones/bandeja/aprobacionesBandeja.html',
        controller: 'aprobacionesBandejaCtrl',
        params: {
          pagina: '1',
          itemsPorPagina: '20',
          filtros: '',
          orden: '',
          bandeja: 'ENTRADA'
        },
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/aprobaciones/bandeja/aprobacionesBandejaController.min.js',
                'app/febos/aprobaciones/aprobaciones.css'
              ]);
            }
          ]
        }
      })
      .state('restringido.aprobacionesbandejasalida', {
        url: '/:app/aprobacionesbandejasalida?pagina=&itemsPorPagina=&filtros',
        templateUrl: 'app/febos/aprobaciones/bandeja/aprobacionesBandeja.html',
        controller: 'aprobacionesBandejaCtrl',
        params: {
          pagina: '1',
          itemsPorPagina: '20',
          filtros: '',
          orden: '',
          bandeja: 'SALIDA'
        },
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/aprobaciones/bandeja/aprobacionesBandejaController.min.js',
                'app/febos/aprobaciones/aprobaciones.css'
              ]);
            }
          ]
        }
      })

      .state('restringido.aprobacionejecucioncrear', {
        url: '/:app/aprobaciones/ejecucioncrear/:aprobacion',
        templateUrl: 'app/febos/aprobaciones/crear_ejecucion/crear-view.html',
        controller: 'aprobacionesCrearEjecucionCtrl',
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'lazy_ionRangeSlider',
                'app/febos/aprobaciones/crear_ejecucion/aprobacionesCrearEjecucionController.min.js',
                'app/febos/aprobaciones/aprobaciones.css'
              ]);
            }
          ]
        }
      });
  }
]);
febosApp.filter('range', function() {
  return function(val, range) {
    range = parseInt(range);
    for (var i = 0; i < range; i++) val.push(i);
    return val;
  };
});
febosApp.directive('fbDragAndDropPaso', function($compile) {
  return {
    restrict: 'A',
    scope: { size: '=', pasos: '=' },
    link: function($scope, $element, $attrs, ngModel) {
      var $container, $items;
      function DragNSort(config) {
        this.$activeItem = null;
        $container = config.container;
        $items = $container.querySelectorAll('.' + config.itemClass);
        this.dragStartClass = config.dragStartClass;
        this.dragEnterClass = config.dragEnterClass;
      }

      DragNSort.prototype.removeClasses = function() {
        [].forEach.call(
          $items,
          function($item) {
            $item.classList.remove(this.dragStartClass, this.dragEnterClass);
          }.bind(this)
        );
      };

      DragNSort.prototype.on = function(eventType, handler) {
        [].forEach.call(
          $items,
          function(element) {
            console.log('removing');
            element.removeEventListener(eventType, handler.bind(element, this));
          }.bind(this)
        );
        [].forEach.call(
          $items,
          function(element) {
            element.addEventListener(eventType, handler.bind(element, this), false);
          }.bind(this)
        );
      };
      var counter = 0;
      DragNSort.prototype.onDragStart = function(_this, event) {
        _this.$activeItem = this;

        this.classList.add(_this.dragStartClass);
        event.dataTransfer.effectAllowed = 'move';
        event.dataTransfer.setData('text/html', this.innerHTML);
      };

      DragNSort.prototype.onDragEnd = function(_this) {
        this.classList.remove(_this.dragStartClass);
      };

      DragNSort.prototype.onDragEnter = function(_this) {
        counter++;
        this.classList.add(_this.dragEnterClass);
      };

      DragNSort.prototype.onDragLeave = function(_this) {
        counter--;
        //if (counter === 0) {
        this.classList.remove(_this.dragEnterClass);
        //}
      };

      DragNSort.prototype.onDragOver = function(_this, event) {
        if (event.preventDefault) {
          event.preventDefault();
        }
        event.dataTransfer.dropEffect = 'move';
        return false;
      };
      var lastItemDrop, lastItemDropTo;

      DragNSort.prototype.onDrop = function(_this, event) {
        console.log('ondropp');
        if (event.stopPropagation) {
          event.stopPropagation();
        }
        if (_this.$activeItem !== this && _this.$activeItem) {
          var receiveStep = _this.$activeItem.getAttribute('data-st');
          var moveStep = this.getAttribute('data-st');

          if (lastItemDrop == receiveStep && lastItemDropTo == moveStep) {
            //  _this.removeClasses();
            //return;
          }
          lastItemDrop = receiveStep;
          lastItemDropTo = moveStep;

          var tmp = [];
          for (var i = 0; i <= $scope.pasos.length - 1; i++) {
            console.log($scope.pasos[i].data.index, receiveStep, moveStep);
            tmp[i] = {};
            if ($scope.pasos[i].data.index == receiveStep) {
              console.log('recie');
              tmp[i].data = angular.copy($scope.pasos[moveStep - 1].data);
              tmp[i].data.index = receiveStep;
            }
            if ($scope.pasos[i].data.index == moveStep) {
              console.log('move');
              tmp[i].data = angular.copy($scope.pasos[receiveStep - 1].data);
              tmp[i].data.index = moveStep;
            }
          }
          for (var i = 0; i <= tmp.length - 1; i++) {
            if (tmp[i].data) {
              $scope.pasos[i].data = angular.copy(tmp[i].data);
              $scope.pasos[i].data.index = tmp[i].data.index;
            }
          }
          console.log($scope.pasos);

          this.setAttribute('data-st', moveStep);
          _this.$activeItem.setAttribute('data-st', receiveStep);
          $scope.$apply();

          //     draggable.init();
        }
        _this.removeClasses();
        return false;
      };

      DragNSort.prototype.bind = function() {
        this.on('dragstart', this.onDragStart);
        this.on('dragend', this.onDragEnd);
        this.on('dragover', this.onDragOver);
        this.on('dragenter', this.onDragEnter);
        this.on('dragleave', this.onDragLeave);
        this.on('drop', this.onDrop);
      };

      DragNSort.prototype.init = function() {
        this.bind();
      };
      var draggable;
      $scope.$watch('size', function(newValue, oldValue) {
        console.log('dragging');
        if (newValue == oldValue) {
          return;
        }

        draggable = new DragNSort({
          container: $element[0], //document.querySelector('.drag-list'),
          itemClass: 'drag-item',
          dragStartClass: 'drag-start',
          dragEnterClass: 'drag-enter'
        });
        draggable.init();
      });
    }
  };
});

febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.bolaDeCristal", {
                            url: "/:app/soporte/bolaDeCristal",
                            templateUrl: 'app/febos/bola_de_cristal/bolaDeCristalView.html',
                            controller: 'bolaDeCristalCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            // ocLazyLoad config (app/app.js)
                                            'app/febos/bola_de_cristal/bolaDeCristalController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Bola de cristal'
                            },
                            ncyBreadcrumb: {
                                label: 'Home'
                            }
                        })

            }]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.cafs', {
      url: '/:app/cafs',
      templateUrl: 'app/febos/cafs/cafsView.html',
      controller: 'cafsCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        cafs: [
          'FebosAPI',
          '$stateParams',
          'SesionFebos',
          function(FebosAPI, $stateParams, SesionFebos) {
            return FebosAPI.cl_caf_listar_cafs(
              {
                empresaId: SesionFebos().empresa.id
              },
              {},
              false,
              false
            ).then(function(response) {
              /*
                            if(typeof response.data.cafs != 'undefined') {
                                return response.data.cafs;
                            } else {
                                return [];
                            }*/
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/cafs/cafsController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.cafs_legacy', {
      url: '/:app/cafsL',
      templateUrl: 'app/febos/cafsLegacy/cafsView.html',
      controller: 'cafsLegacyCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        cafs: [
          'FebosAPI',
          '$stateParams',
          'SesionFebos',
          function(FebosAPI, $stateParams, SesionFebos) {
            return FebosAPI.cl_listar_cafs(
              {
                empresaId: SesionFebos().empresa.id
              },
              {},
              false,
              false
            ).then(function(response) {
              /*
                            if(typeof response.data.cafs != 'undefined') {
                                return response.data.cafs;
                            } else {
                                return [];
                            }*/
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/cafsLegacy/cafsLegacyController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.cafsDeReserva', {
      url: '/:app/cafsDeReserva',
      templateUrl: 'app/febos/cafsStore/cafsStoreView.html',
      controller: 'cafsStoreCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        cafStores: [
          'FebosAPI',
          '$stateParams',
          'SesionFebos',
          function(FebosAPI, $stateParams, SesionFebos) {
            return FebosAPI.cl_caf_listar_cafs_store(
              {
                empresaId: SesionFebos().empresa.id,
                numeroPagina: 1,
                filasPorPagina: 200,
                tipoCAF: 3
              },
              {},
              false,
              false
            ).then(function(response) {
              console.log(response);
              console.log(response.data);
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/cafsStore/cafsStoreController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.cafsDeReserva_legacy', {
      url: '/:app/cafsDeReservaL',
      templateUrl: 'app/febos/cafsStoreLegacy/cafsStoreView.html',
      controller: 'cafsStoreLegacyCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        cafStores: [
          'FebosAPI',
          '$stateParams',
          'SesionFebos',
          function(FebosAPI, $stateParams, SesionFebos) {
            return FebosAPI.cl_listar_cafs_store(
              {
                empresaId: SesionFebos().empresa.id,
                numeroPagina: 1,
                filasPorPagina: 200
              },
              {},
              false,
              false
            ).then(function(response) {
              /*
                            if(reponse.data.codigo != 0) {
                                if (typeof response.data.cafStores != 'undefined') {
                                    console.log("encontre caf stores: " + response.data.cafStores.length);
                                    return response.data.cafStores;
                                } else {
                                    console.log("No encontre caf stores");
                                    return [];
                                }
                            }*/
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/cafsStoreLegacy/cafsStoreLegacyController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.
                state("restringido.perfil_empresa", {
                            url: "/:app/empresas/perfil",
                            templateUrl: 'app/febos/empresas/modificar/modificarEmpresasView.html',
                            controller: 'modificarEmpresaCtrl',
                            resolve: {
                                
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        'lazy_KendoUI',
                                        'lazy_uikit',
                                        'lazy_iCheck',
                                        'app/febos/empresas/modificar/modificarEmpresasController.js'
                                    ]);
                                }]
                            }
                        })
                    .state("restringido.empresas", {
                        url: "/:app/empresas",
                        templateUrl: 'app/febos/empresas/gestionEmpresasView.html',
                        controller: 'gestionEmpresasCtrl',
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        'lazy_uikit',
                                        'lazy_iCheck',
                                        'app/febos/empresas/gestionEmpresasController.js'
                                    ]);
                                }]
                        }
                    })                        

            }]);
febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.inicio', {
      url: '/:app/inicio',
      templateUrl: 'app/febos/inicio/inicioView.html',
      controller: 'inicioCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load(
              [
                // ocLazyLoad config (app/app.js)
                'lazy_ckeditor',
                'lazy_countUp',
                'lazy_charts_peity',
                'lazy_charts_easypiechart',
                'lazy_charts_metricsgraphics',
                'lazy_charts_chartist',
                'lazy_weathericons',
                'lazy_google_maps',
                'lazy_clndr',
                'app/febos/inicio/inicioController.js'
              ],
              { serie: true }
            );
          }
        ]
      },
      data: {
        pageTitle: 'Dashboard'
      },
      ncyBreadcrumb: {
        label: 'Home'
      }
    });
  }
]);

febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.listas", {
                    url: "/:app/empresa/listas",
                    templateUrl: 'app/febos/listas/gestionListasView.html',
                    controller: 'gestionListasCtrl',
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                // ocLazyLoad config (app/app.js)
                                'app/febos/listas/gestionListasController.js'
                            ], {serie: true});
                        }]
                    }
                })

            }]);
febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider
                .state("restringido.listarNovedades", {
                    url: "/:app/novedades",
                            templateUrl: 'app/febos/novedades/listar/novedadesView.html',
                            controller: 'novedadesListarCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/novedades/listar.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_clndr',
                                            'app/febos/novedades/listar/novedadesController.js'
                                        ], {serie: true});
                                    }]
                            }
                })
                .state("restringido.crearNovedad", {
                    url: "/:app/novedades/escribir",
                    templateUrl: 'app/febos/novedades/crear/novedadView.html',
                    controller: 'novedadCrearCtrl',
                    resolve: {
                        
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_ckeditor',
                                    'lazy_clndr',
                                    'app/febos/novedades/crear/novedadController.js'
                                ], {serie: true});
                            }]
                    }
                })
                .state("restringido.verNovedad", {
                    url: "/:app/novedades/ver/:novedadId",
                    templateUrl: 'app/febos/novedades/ver/novedadView.html',
                    controller: 'novedadVerCtrl',
                    resolve: {                       
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_clndr',
                                    'app/febos/novedades/ver/novedadController.js'
                                ], {serie: true});
                            }]
                    }
                })
                .state("restringido.editarNovedad", {
                    url: "/:app/novedades/editar/:novedadId",
                    templateUrl: 'app/febos/novedades/editar/novedadView.html',
                    controller: 'novedadEditarCtrl',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_ckeditor',
                                    'lazy_clndr',                                    
                                    'app/febos/novedades/editar/novedadController.js'
                                ], {serie: true});
                            }]
                    }
                })

            }]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('restringido.perfil_partner', {
        url: '/:app/partners/perfil',
        templateUrl: 'app/febos/partners/modificar/modificarEmpresasView.html',
        controller: 'modificarEmpresaCtrl',
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_KendoUI',
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/partners/modificar/modificarEmpresasController.js'
              ]);
            }
          ]
        }
      })
      .state('restringido.partners', {
        url: '/:app/partners',
        templateUrl: 'app/febos/partners/gestionEmpresasView.html',
        controller: 'gestionEmpresasCtrl',
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/partners/gestionEmpresasController.js'
              ]);
            }
          ]
        }
      });
  }
]);

febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.receptoresElectronicos", {
                    url: "/:app/receptores",
                    templateUrl: 'app/febos/receptores/gestionReceptoresElectronicosView.html',
                    controller: 'gestionReceptoresElectronicosCtrl',
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_uikit',
                                'lazy_iCheck',
                                'app/febos/receptores/gestionReceptoresElectronicosController.js'
                            ]);
                        }]
                    }
                })

            }]);
febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.roles", {
                    url: "/:app/roles",
                    templateUrl: 'app/febos/roles/gestionRolesView.html',
                    controller: 'gestionRolesCtrl',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_uikit',
                                    'lazy_iCheck',
                                    'app/febos/roles/gestionRolesController.js'
                                ]);
                            }]
                    }
                })

            }]);


febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.gestionSucursales", {
                            url: "/empresas/:empresaid/sucursales",
                            templateUrl: 'app/febos/sucursales/gestionSucursalesView.html',
                            controller: 'gestionSucursalesCtrl',
                            resolve: {
                                EstadoAnterior: [
                                    "$state",
                                    function ($state) {
                                        var currentStateData = {
                                            estado: $state.current.name,
                                            parametros: $state.params,
                                            url: $state.href($state.current.name, $state.params)
                                        };
                                        return currentStateData;
                                    }
                                ],
                                Autorizacion: ['Autorizacion', function (Autorizacion) {
                                        try {
                                            Autorizacion.verificar();
                                        } catch (e) {
                                            console.log("Error al autenticar", e);
                                        }
                                        return;
                                    }],
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            // ocLazyLoad config (app/app.js)
                                            'lazy_countUp',
                                            'lazy_charts_peity',
                                            'lazy_charts_easypiechart',
                                            'lazy_charts_metricsgraphics',
                                            'lazy_charts_chartist',
                                            'lazy_weathericons',
                                            'lazy_google_maps',
                                            'lazy_clndr',
                                            'app/febos/sucursales/gestionSucursalesController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Dashboard'
                            },
                            ncyBreadcrumb: {
                                label: 'Home'
                            }
                        })

            }]);

febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.webhooks", {
                    url: "/:app/webhooks",
                    templateUrl: 'app/febos/webhook/webhookView.html',
                    controller: 'webhookCtrl',
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_uikit',
                                    'lazy_iCheck',
                                    'app/febos/webhook/webhookController.js'
                                ]);
                        }]
                    }
                })

            }]);
'use strict';
febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider) {
    $stateProvider.state('restringido.aat_aceptar', {
      url: '/:app/acciones_automaticas/aceptar',
      templateUrl: 'app/febos/acciones_automaticas/aceptar/aatAceptarView.html',
      controller: 'aatAceptarCtrl',
      resolve: {
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'lazy_ionRangeSlider',
              'app/febos/acciones_automaticas/aceptar/aatAceptarController.min.js'
            ]);
          }
        ]
      }
    });
  }
]);

'use strict';
febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider) {
    $stateProvider.state('restringido.aat_rechazar', {
      url: '/:app/acciones_automaticas/rechazar',
      templateUrl: 'app/febos/acciones_automaticas/rechazar/aatRechazarView.html',
      controller: 'aatRechazarCtrl',
      resolve: {
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'lazy_ionRangeSlider',
              'app/febos/acciones_automaticas/rechazar/aatRechazarController.min.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.folios', {
      url: '/:app/cafs/folios?tipoDTE&tipoCAF',
      templateUrl: 'app/febos/cafs/folios/foliosView.html',
      controller: 'foliosCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        cafsDetalles: [
          'FebosAPI',
          '$stateParams',
          'SesionFebos',
          function(FebosAPI, $stateParams, SesionFebos) {
            return FebosAPI.cl_caf_listar_folios(
              {
                empresaId: SesionFebos().empresa.iut,
                tipoCAF: $stateParams.tipoCAF,
                tipoDTE: $stateParams.tipoDTE
              },
              true,
              false
            ).then(function(response) {
              if (typeof response.data.cafsDetalle != 'undefined') {
                return response.data.cafsDetalle;
              } else {
                return [];
              }
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/cafs/folios/foliosController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.folios_legacy', {
      url: '/:app/cafsL/folios?tipoDTE&tipoCAF',
      templateUrl: 'app/febos/cafsLegacy/folios/foliosView.html',
      controller: 'foliosLegacyCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        cafsDetalles: [
          'FebosAPI',
          '$stateParams',
          'SesionFebos',
          function(FebosAPI, $stateParams, SesionFebos) {
            return FebosAPI.cl_listar_folios(
              {
                empresaId: SesionFebos().empresa.iut,
                tipoCAF: $stateParams.tipoCAF,
                tipoDTE: $stateParams.tipoDTE
              },
              true,
              false
            ).then(function(response) {
              if (typeof response.data.cafsDetalle != 'undefined') {
                return response.data.cafsDetalle;
              } else {
                return [];
              }
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/cafsLegacy/folios/foliosLegacyController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.entregaDeFolios', {
      url: '/:app/cafsDeReserva/:cafId',
      templateUrl: 'app/febos/cafsStore/cafsStoreDelivery/cafsStoreDeliveryView.html',
      controller: 'cafsStoreDeliveryCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        cafsStoreDelivery: [
          'FebosAPI',
          '$stateParams',
          'SesionFebos',
          function(FebosAPI, $stateParams, SesionFebos) {
            return FebosAPI.cl_caf_listar_cafs_store_delivery(
              {
                empresaId: SesionFebos().empresa.id,
                cafId: $stateParams.cafId
              },
              false,
              false
            ).then(function(response) {
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/cafsStore/cafsStoreDelivery/cafsStoreDeliveryController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.entregaDeFolios_legacy', {
      url: '/:app/cafsDeReservaL/:cafId',
      templateUrl: 'app/febos/cafsStoreLegacy/cafsStoreDelivery/cafsStoreDeliveryView.html',
      controller: 'cafsStoreDeliveryLegacyCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        cafsStoreDelivery: [
          'FebosAPI',
          '$stateParams',
          'SesionFebos',
          function(FebosAPI, $stateParams, SesionFebos) {
            return FebosAPI.cl_listar_cafs_store_delivery(
              {
                empresaId: SesionFebos().empresa.iut,
                cafId: $stateParams.cafId,
                numeroPagina: 1,
                filasPorPagina: 200
              },
              false,
              false
            ).then(function(response) {
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/cafsStoreLegacy/cafsStoreDelivery/cafsStoreDeliveryLegacyController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  function($stateProvider) {
    $stateProvider.state('restringido.indicadoresemisor', {
      url: '/:app/indicadores/emisor?opcion&usuario&grupo',
      templateUrl: 'app/febos/dashboard/indicadores/indicadorEmisorView.html',
      controller: 'indicadorEmisorCtrl',
      params: {
        app: 'cloud',
        valor: [],
        opcion: '3',
        usuario: '',
        grupo: ''
      },
      reloadOnSearch: false,
      resolve: {
        Datos: [
          'FebosAPI',
          '$location',
          '$rootScope',
          '$stateParams',
          function(FebosAPI, $location, $rootScope, $stateParams) {
            var desde = moment()
              .subtract(2, 'month')
              .startOf('month')
              .format('YYYY-MM-DD');
            var hasta = moment().format('YYYY-MM-DD');

            if ($stateParams.valor.fechaDesde != undefined) {
              desde = $stateParams.valor.fechaDesde;
            } else {
              $stateParams.valor.fechaDesde = desde;
            }
            if ($stateParams.valor.fechaHasta != undefined) {
              hasta = $stateParams.valor.fechaHasta;
            } else {
              $stateParams.valor.fechaHasta = hasta;
            }
            console.log(desde);
            console.log(hasta);

            var req = {
              fechaDesde: desde,
              fechaHasta: hasta,
              ambitoGrupo: $stateParams.grupo,
              ambitoUsuario: $stateParams.usuario
            };
            console.log(req);
            return FebosAPI.cl_obtener_indicadores_emision(req).then(function(response) {
              return response.data;
            });
          }
        ],
        usuarios: [
          'FebosAPI',
          'SesionFebos',
          function(FebosAPI, SesionFebos) {
            console.log('usuarios!');
            return FebosAPI.cl_listar_arbol_usuarios(
              {
                empresaId: SesionFebos().empresa.iut
              },
              true,
              false
            ).then(function(response) {
              return response.data.usuarios;
            });
          }
        ],
        grupos: [
          'FebosAPI',
          'SesionFebos',
          function(FebosAPI, SesionFebos) {
            console.log('grupos!');
            return FebosAPI.cl_listar_grupos(
              {
                empresaId: SesionFebos().empresa.iut
              },
              true,
              false
            ).then(function(response) {
              return response.data.grupos;
            });
          }
        ],

        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load(
              [
                // ocLazyLoad config (app/app.js)
                'bower_components/jquery-ui/jquery-ui.min.js',
                'lazy_uiSelect',
                'lazy_selectizeJS',
                'app/febos/dashboard/indicadores/indicadorEmisorController.js'
              ],
              { serie: true }
            );
          }
        ]
      },
      data: {
        pageTitle: 'Indicadores de Emisor'
      },
      ncyBreadcrumb: {
        label: 'Indicadores'
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.indicadoresgetion', {
      url: '/:app/indicadores/gestion?opcion&usuario&grupo',
      templateUrl: 'app/febos/dashboard/indicadores/indicadorGestionView.html',
      controller: 'indicadorGestionCtrl',
      params: {
        app: 'cloud',
        valor: [],
        opcion: '3',
        usuario: '',
        grupo: ''
      },
      reloadOnSearch: false,
      resolve: {
        Datos: [
          'FebosAPI',
          '$location',
          '$rootScope',
          '$stateParams',
          'FebosUtil',
          '$state',
          function(FebosAPI, $location, $rootScope, $stateParams, FebosUtil, $state) {
            var desde = moment()
              .subtract(2, 'month')
              .startOf('month')
              .format('YYYY-MM-DD');
            var hasta = moment().format('YYYY-MM-DD');

            if ($stateParams.valor.fechaDesde != undefined) {
              desde = $stateParams.valor.fechaDesde;
            } else {
              $stateParams.valor.fechaDesde = desde;
            }
            if ($stateParams.valor.fechaHasta != undefined) {
              hasta = $stateParams.valor.fechaHasta;
            } else {
              $stateParams.valor.fechaHasta = hasta;
            }
            console.log(desde);
            console.log(hasta);

            var req = {
              fechaDesde: desde,
              fechaHasta: hasta,
              ambitoGrupo: $stateParams.grupo,
              ambitoUsuario: $stateParams.usuario
            };
            console.log(req);
            return FebosAPI.cl_obtener_indicadores_gestion(req).then(function(response) {
              return response.data;
            });
          }
        ],
        usuarios: [
          'FebosAPI',
          'SesionFebos',
          function(FebosAPI, SesionFebos) {
            console.log('usuarios!');
            return FebosAPI.cl_listar_arbol_usuarios(
              {
                empresaId: SesionFebos().empresa.iut
              },
              true,
              false
            ).then(function(response) {
              return response.data.usuarios;
            });
          }
        ],
        grupos: [
          'FebosAPI',
          'SesionFebos',
          function(FebosAPI, SesionFebos) {
            console.log('grupos!');
            return FebosAPI.cl_listar_grupos(
              {
                empresaId: SesionFebos().empresa.iut
              },
              true,
              false
            ).then(function(response) {
              return response.data.grupos;
            });
          }
        ],

        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load(
              [
                // ocLazyLoad config (app/app.js)
                'bower_components/jquery-ui/jquery-ui.min.js',
                'lazy_uiSelect',
                'lazy_selectizeJS',
                'app/febos/dashboard/indicadores/indicadorGestionController.js'
              ],
              { serie: true }
            );
          }
        ]
      },
      data: {
        pageTitle: 'Indicadores de Gestion'
      },
      ncyBreadcrumb: {
        label: 'Indicadores'
      }
    });
  }
]);

febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.kpi", {
                    url: "/:app/kpi",
                    templateUrl: 'app/febos/dashboard/kpi/kpiView.html',
                    controller: 'kpiCtrl',
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_countUp',
                                'lazy_charts_peity',
                                'lazy_charts_easypiechart',
                                'lazy_charts_metricsgraphics',
                                'lazy_charts_chartist',
                                'lazy_weathericons',
                                'lazy_google_maps',
                                'lazy_clndr',
                                'lazy_charts_c3',
                                'app/febos/dashboard/kpi/kpiController.js'
                            ], {serie: true});
                        }]
                    },
                    data: {
                        pageTitle: 'KPI'
                    },
                    ncyBreadcrumb: {
                        label: 'Home'
                    }
                })
            }]);
febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.emisorkpi", {
                    url: "/:app/kpiEmisor",
                    templateUrl: 'app/febos/dashboard/kpi_emisor/kpiEmisorView.html',
                    controller: 'kpiEmisorCtrl',
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_countUp',
                                'lazy_charts_peity',
                                'lazy_charts_easypiechart',
                                'lazy_charts_metricsgraphics',
                                'lazy_charts_chartist',
                                'lazy_weathericons',
                                'lazy_google_maps',
                                'lazy_clndr',
                                'lazy_charts_c3',
                                'app/febos/dashboard/kpi_emisor/kpiEmisorController.js'
                            ], {serie: true});
                        }]
                    },
                    data: {
                        pageTitle: 'KPI Emisor'
                    },
                    ncyBreadcrumb: {
                        label: 'Home'
                    }
                })
            }]);
febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.dte', {
      url: '/:app/documentos/electronicos/:categoria/:vista?pagina&itemsPorPagina&orden&filtros',
      templateUrl: 'app/febos/documentos/electronicos/documentosView.html',
      controller: 'documentosCtrl',
      reloadOnSearch: false,
      params: {
        pagina: '',
        itemsPorPagina: '',
        filtros: '',
        orden: ''
      },
      data: {
        pageTitle: 'Documentos Electrónicos Emitidos'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        verificacionDeParametros: [
          'ComplementoDte',
          '$location',
          '$stateParams',
          'FebosUtil',
          '$state',
          'SesionFebos',
          '$rootScope',
          function(
            ComplementoDte,
            $location,
            $stateParams,
            FebosUtil,
            $state,
            SesionFebos,
            $rootScope
          ) {
            var categoria = $stateParams.categoria || 'recibidos';
            var pagina = $stateParams.filtros != '' ? $location.search().pagina || 1 : 1;
            var itemsPorPagina =
              $stateParams.filtros != '' ? $location.search().itemsPorPagina || 20 : 20;
            var orden =
              $stateParams.filtros != ''
                ? typeof $location.search().orden == 'undefined' || $location.search().orden == ''
                  ? categoria == 'emitidos'
                    ? '-fechaCreacion'
                    : '-fechaRecepcion'
                  : $location.search().orden
                : categoria == 'emitidos'
                  ? '-fechaCreacion'
                  : '-fechaRecepcion';
            //objeto para reemplazar en la ruta URL
            console.log(orden);
            if (
              (orden == '-fechaCreacion' || orden == '-fechaRecepcion') &&
              $stateParams.vista == 'noRecibidos'
            ) {
              orden = '-fechaRecepcionSii';
            }
            console.log(orden);

            var queryParams = {
              pagina: pagina,
              itemsPorPagina: itemsPorPagina,
              orden: orden
            };
            console.log({
              pagina: $stateParams.pagina,
              itemsPorPagina: $stateParams.itemsPorPagina,
              filtros: $stateParams.filtros,
              orden: $stateParams.orden
            });
            console.log($stateParams.app, $stateParams.categoria, $stateParams.vista);

            queryParams.filtros = ComplementoDte.prepararFiltrosDte(
              {
                pagina: $stateParams.pagina,
                itemsPorPagina: $stateParams.itemsPorPagina,
                filtros: $stateParams.filtros,
                orden: $stateParams.orden
              },
              $stateParams.app,
              $stateParams.categoria,
              $stateParams.vista
            );
            console.log('queryParams.filtros', queryParams.filtros);
            console.log('roberto2');
            $rootScope.queryParams = $location.search();

            var st = {};
            st.pagina = queryParams.pagina;
            st.itemsPorPagina = queryParams.itemsPorPagina;
            st.orden = queryParams.orden;
            st.filtros = queryParams.filtros;
            st.app = $stateParams.app;
            st.categoria = $stateParams.categoria;
            st.vista = $stateParams.vista;

            console.log('st', st);
            return st;
          }
        ],
        Datos: [
          'FebosAPI',
          '$location',
          'ComplementoDte',
          '$rootScope',
          'verificacionDeParametros',
          function(FebosAPI, $location, ComplementoDte, $rootScope, verificacionDeParametros) {
            //$stateParams.app;  // cloud, proveedores, clientes
            //$stateParams.sentido; // emitidos, recibidos
            //'filtros': "rutReceptor:76179952-5|fechaRecepcion:2018-04-01--2018-05-05",
            //seteo valores por defecto si no vienen

            $rootScope.configFiltrosActivo = false;
            var filtros = verificacionDeParametros.filtros;
            //var filtros = verificacionDeParametros.filtros;
            var rangos = Object.keys(ComplementoDte.atajosDeRangosDeFecha);
            for (var i = 0; i < rangos.length; i++) {
              var rango = rangos[i];
              if (filtros.includes(rango)) {
                filtros = filtros.replace(
                  rango,
                  ComplementoDte.atajosDeRangosDeFecha[rango].desde +
                    '--' +
                    ComplementoDte.atajosDeRangosDeFecha[rango].hasta
                );
                i--;
              }
            }

            var reemplazosDeBusquedaLike = [
              'rutCesionario',
              'razonSocialEmisor',
              'razonSocialReceptor',
              'razonSocialCesionario',
              'comentario',
              'lugar'
            ];
            for (var i = 0; i < reemplazosDeBusquedaLike.length; i++) {
              if (filtros.includes(reemplazosDeBusquedaLike[i])) {
                filtros = filtros.replace(
                  reemplazosDeBusquedaLike[i] + ':',
                  reemplazosDeBusquedaLike[i] + '$'
                );
              }
            }

            return FebosAPI.cl_listar_dte({
              pagina: verificacionDeParametros.pagina,
              itemsPorPagina: verificacionDeParametros.itemsPorPagina,
              campos:
                'trackId,tipoDocumento,folio,rutEmisor,razonSocialEmisor,rutReceptor,razonSocialReceptor,rutCesionario,razonSocialCesionario,indicadorDeTraslado,fechaCesion,codigoSii,fechaEmision,fechaRecepcion,fechaRecepcionSii,plazo,estadoComercial,estadoSii,fechaReciboMercaderia,formaDePago,montoTotal,iva,contacto,correoReceptor,fechaCesion,tipo,monto,lugar,comentario,fecha,medio,tpoTraVenta,tpoTranCompra,tpoTranCompraCodIva,tieneNc,tieneNd',
              filtros: filtros,
              orden: verificacionDeParametros.orden
            }).then(function(response) {
              console.log('?? Datos');
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'assets/js/encoding-japanese/encoding.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/documentos/electronicos/documentosController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.emisionWeb', {
      url: '/:app/documentos/emitir/:tipo',
      templateUrl: 'app/febos/documentos/emisionWeb/emitirView.html',
      controller: 'emitirCtrl',
      //reloadOnSearch: false,
      params: {
        tipo: ''
      },
      data: {
        pageTitle: 'Emisión de Documentos Electrónicos'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        DatosEmisor: [
          'FebosAPI',
          '$location',
          '$rootScope',
          'SesionFebos',
          function(FebosAPI, $location, $rootScope, SesionFebos) {
            return FebosAPI.cl_info_empresa(
              {
                empresaId: SesionFebos().empresa.iut,
                debug: '',
                simular: '',
                token: SesionFebos().token,
                empresa: SesionFebos().empresa.iut,
                grupo: SesionFebos().grupo.id
              },
              {},
              true,
              false
            ).then(function(response) {
              return response.data;
            });
          }
        ],

        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_selectizeJS',
              'lazy_uiSelect',
              'lazy_KendoUI',
              'lazy_uikit',
              'lazy_masked_inputs',
              'app/febos/documentos/emisionWeb/emitirController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.dnt', {
      url: '/:app/documentos/no_tributarios/:categoria/:vista?pagina&itemsPorPagina&orden&filtros',
      templateUrl: 'app/febos/documentos/no_tributarios/documentosView.html',
      controller: 'documentosNoTributariosCtrl',
      reloadOnSearch: false,
      params: {
        pagina: '',
        itemsPorPagina: '',
        filtros: '',
        orden: ''
      },
      data: {
        pageTitle: 'Documentos No Tributarios'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        verificacionDeParametros: [
          'ComplementoDnt',
          '$location',
          '$stateParams',
          'FebosUtil',
          '$state',
          'SesionFebos',
          '$rootScope',
          function(
            ComplementoDnt,
            $location,
            $stateParams,
            FebosUtil,
            $state,
            SesionFebos,
            $rootScope
          ) {
            var categoria = $stateParams.categoria || 'recibidos';
            var pagina = $stateParams.filtros != '' ? $location.search().pagina || 1 : 1;
            var itemsPorPagina =
              $stateParams.filtros != '' ? $location.search().itemsPorPagina || 20 : 20;
            var orden =
              $stateParams.filtros != ''
                ? typeof $location.search().orden == 'undefined' || $location.search().orden == ''
                  ? categoria == 'emitidos'
                    ? '-fechaEmision'
                    : '-fechaEmision'
                  : $location.search().orden
                : '-fechaEmision';
            //objeto para reemplazar en la ruta URL
            var queryParams = {
              pagina: pagina,
              itemsPorPagina: itemsPorPagina,
              orden: orden
            };

            queryParams.filtros = ComplementoDnt.prepararFiltrosDnt(
              {
                pagina: $stateParams.pagina,
                itemsPorPagina: $stateParams.itemsPorPagina,
                filtros: $stateParams.filtros,
                orden: $stateParams.orden
              },
              $stateParams.app,
              $stateParams.categoria,
              $stateParams.vista
            );
            $rootScope.queryParams = $location.search();

            var st = {};
            st.pagina = queryParams.pagina;
            st.itemsPorPagina = queryParams.itemsPorPagina;
            st.orden = queryParams.orden;
            st.filtros = queryParams.filtros;
            st.app = $stateParams.app;
            st.categoria = $stateParams.categoria;
            st.vista = $stateParams.vista;

            return st;
          }
        ],
        Datos: [
          'FebosAPI',
          '$location',
          'ComplementoDnt',
          '$rootScope',
          'verificacionDeParametros',
          function(FebosAPI, $location, ComplementoDnt, $rootScope, verificacionDeParametros) {
            //$stateParams.app;  // cloud, proveedores, clientes
            //$stateParams.sentido; // emitidos, recibidos
            //'filtros': "rutReceptor:76179952-5|fechaRecepcion:2018-04-01--2018-05-05",
            //seteo valores por defecto si no vienen
            console.log('A');
            $rootScope.configFiltrosActivo = false;
            var filtros = verificacionDeParametros.filtros;
            //var filtros = verificacionDeParametros.filtros;
            var rangos = Object.keys(ComplementoDnt.atajosDeRangosDeFecha);
            for (var i = 0; i < rangos.length; i++) {
              var rango = rangos[i];
              if (filtros.includes(rango)) {
                filtros = filtros.replace(
                  rango,
                  ComplementoDnt.atajosDeRangosDeFecha[rango].desde +
                    '--' +
                    ComplementoDnt.atajosDeRangosDeFecha[rango].hasta
                );
                i--;
              }
            }

            /*
                            var reemplazosDeBusquedaLike=['rutCesionario','razonSocialEmisor','razonSocialReceptor','razonSocialCesionario','comentario','lugar'];
                            for(var i=0;i<reemplazosDeBusquedaLike.length;i++){
                                if(filtros.includes(reemplazosDeBusquedaLike[i])){
                                    filtros = filtros.replace(reemplazosDeBusquedaLike[i]+":",reemplazosDeBusquedaLike[i]+"$");
                                }
                            }
                            */
            return FebosAPI.cl_listar_dnt({
              pagina: verificacionDeParametros.pagina,
              itemsPorPagina: verificacionDeParametros.itemsPorPagina,
              campos:
                'tipo,numero,receptorRut,fechaEmision,totalPrecioTotalNeto,totalMonedaTipo,fechaActualizacion,compradorNombre,compradorArea,emisorRut,emisorRazonSocial,receptorRazonSocial,leido',
              //'filtros': $location.search().filtros,
              filtros: filtros,
              orden: verificacionDeParametros.orden
            }).then(function(response) {
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/documentos/no_tributarios/documentosController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("especial.selector_empresa", {
                    url: "/:app/empresas/seleccion",
                    templateUrl: 'app/febos/empresas/selector/selectorEmpresaView.html',
                    controller: 'selectorEmpresaCtrl',
                    reloadOnSearch: false,
                    data: {
                        pageTitle: 'Selecciona tu empresa'
                    }, 
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                            }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    //'lazy_uikit',
                                    //'lazy_iCheck',
                                    'app/febos/empresas/selector/selectorEmpresaController.js',
                                ]);
                            }]
                    }
                })

            }]);
febosApp
  .config(
    [
      '$stateProvider',
      '$urlRouterProvider',
      '$locationProvider',
      function ($stateProvider, $urlRouterProvider, $locationProvider) {

        $stateProvider.state(
          "restringido.editorPlantilla", 
          {
            url: "/:app/editorPlantillas/editor?id&type",
            templateUrl: 'app/febos/editor_de_plantillas/editor/editorPlantillaView.html',
            controller: 'editorPlantillaCtrl',
            params: {
		          id: null,
              type: null,
              data: null
		        },
            resolve: {
              EstadoAnterior: [
                "$state",
                function ($state) {
                  var currentStateData = {
                    estado: $state.current.name,
                    parametros: $state.params,
                    url: $state.href($state.current.name, $state.params)
                  };
                  return currentStateData;
                }
              ],
              bloquesDeElementos: ['$q', '$templateCache', '$http', function ($q, $templateCache, $http){
                var deferred = $q.defer();
                var baseUrlTemplate = 'app/templates/editorPlantillaTemplates/elementos/';
                var baseUrlEditorTemplate = 'app/templates/editorPlantillaTemplates/modals/elementos/';
                $http.get('data/document_builder_blocks.json')
                .then(function(response){ 
                    var blocks = response.data;  //ajax request to fetch data into $scope.data
                    var blocks_extended = {};
                    var prom = [];
                    Object.keys(blocks).forEach(function (key) {
                      if(blocks[key].base != 'root'){
                        var elementTemp = blocks[key];
                        var baseElement = blocks_extended[elementTemp.base];
                        blocks_extended[key] = angular.merge(
                          angular.merge(
                            {}
                          , angular.copy(baseElement)
                          )
                        , angular.copy(elementTemp)
                        );
                        return;
                      }
                      blocks_extended[key] = angular.copy(blocks[key]);
                      return;
                    });
                    Object.keys(blocks_extended).forEach(function (key) {
                      if(blocks_extended[key].display){
                        prom.push($http.get(baseUrlTemplate + blocks_extended[key].templateUrl).then(function(response){
                          return $templateCache.put(blocks_extended[key].type + "Template", response.data);
                        }));
                        prom.push($http.get(baseUrlEditorTemplate + blocks_extended[key].type + 'Template.tpl.html').then(function(response){
                          return $templateCache.put(blocks_extended[key].type + "TemplateEditor", response.data);
                        }));
                      }
                    });
                    prom.push($http.get(baseUrlEditorTemplate + 'defaultTemplate.tpl.html').then(function(response){
                      return $templateCache.put("defultTemplateEditor", response.data);
                    }));
                    $q.all(prom).then(function () {
                      deferred.resolve(blocks_extended);
                    });
                  },
                  function (error){
                    deferred.resolve({});
                  });
                return deferred.promise;
              }],
/*             Autorizacion: ['Autorizacion', function (Autorizacion) {
                try {
                  Autorizacion.verificar();
                } catch (e) {
                  console.log("Error al autenticar", e);
                }
                return;
              }],*/
              deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                  'https://rawgit.com/RickStrahl/jquery-resizable/master/src/jquery-resizable.js',
                  'bower_components/tinycolor/dist/tinycolor-min.js',
                  'bower_components/angular-color-picker/dist/angularjs-color-picker.min.js',
                  'bower_components/angular-color-picker/dist/angularjs-color-picker.min.css',
                  'lazy_tinymce',
                  'lazy_uikit',
                  'lazy_iCheck',
                  'lazy_parsleyjs',
                  'lazy_dragula',
                  'app/febos/editor_de_plantillas/editor/editorPlantillaController.js'
                ], {serie: true});
              }]
            }
          }
        );
      }
    ]
  );


febosApp
  .config(
    [
      '$stateProvider',
      '$urlRouterProvider',
      '$locationProvider',
      function ($stateProvider, $urlRouterProvider, $locationProvider) {

        $stateProvider.state(
          "restringido.listarPlantillas", 
          {
            url: "/:app/editorPlantillas/listar",
            templateUrl: 'app/febos/editor_de_plantillas/lista_de_plantillas/listaPlantillasView.html',
            controller: 'listaPlantillasCtrl',
            resolve: {
              EstadoAnterior: [
                "$state",
                function ($state) {
                  var currentStateData = {
                    estado: $state.current.name,
                    parametros: $state.params,
                    url: $state.href($state.current.name, $state.params)
                  };
                  return currentStateData;
                }
              ],
/*             Autorizacion: ['Autorizacion', function (Autorizacion) {
                try {
                  Autorizacion.verificar();
                } catch (e) {
                  console.log("Error al autenticar", e);
                }
                return;
              }],*/
              deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                  'lazy_uikit',
                  'lazy_iCheck',
                  'lazy_pagination',
                  'app/febos/editor_de_plantillas/lista_de_plantillas/listaPlantillasController.js'
                ]);
              }]
            }
          }
        );
      }
    ]
  );

febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.configuracion_flujo", {
                    url: "/:app/flujos/configuracion/:flujoId",
                    templateUrl: 'app/febos/flujos/configuracion/configuracionFlujoView.html',
                    controller: 'configuracionFlujoCtrl',
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_uikit',
                                    'lazy_iCheck',
                                    'app/febos/flujos/configuracion/configuracionFlujoController.js'
                                ]);
                        }]
                    }
                })

            }]);
febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.flujos", {
                    url: "/:app/flujos",
                    templateUrl: 'app/febos/flujos/gestion/gestionFlujosView.html',
                    controller: 'gestionFlujosCtrl',
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_uikit',
                                    'lazy_iCheck',
                                    'app/febos/flujos/gestion/gestionFlujosController.js'
                                ]);
                        }]
                    }
                })

            }]);
febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.ejecuciones_flujo", {
                    url: "/:app/flujos/:flujoId/ejecuciones",
                    templateUrl: 'app/febos/flujos/listarEjecucion/listarEjecucionesView.html',
                    controller: 'listarEjecucionesCtrl',
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_uikit',
                                    'lazy_iCheck',
                                    'app/febos/flujos/listarEjecucion/listarEjecucionesController.js'
                                ]);
                        }]
                    }
                })

            }]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('restringido.notificaciones', {
        url: '/:app/notificaciones/listado',
        templateUrl: 'app/febos/notificaciones/listado/notificacionesListadoView.html',
        controller: 'notificacionListadoCtrl',
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/notificaciones/listado/notificacionesListadoController.js'
              ]);
            }
          ]
        }
      })
      .state('restringido.gestion_notificaciones', {
        url: '/:app/notificaciones/gestion/',
        params: { notificacion: null },
        templateUrl: 'app/febos/notificaciones/gestion/notificacionesGestionView.html',
        controller: 'notificacionesGestionCtrl',
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/notificaciones/gestion/notificacionesGestionController.js'
              ]);
            }
          ]
        }
      });
  }
]);

febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.certificado", {
                    url: "/:app/operacion/certificado",
                    templateUrl: 'app/febos/operacion/certificado_digital/certificadoDigitalView.html',
                    controller: 'certificadoDigitalCtrl',
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_uikit',
                                'lazy_iCheck',
                                'app/febos/operacion/certificado_digital/certificadoDigitalController.js'
                            ]);
                        }]
                    }
                })

            }]);
febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('especial.selector_partner', {
      url: '/:app/partners/seleccion',
      templateUrl: 'app/febos/partners/selector/selectorEmpresaView.html',
      controller: 'selectorEmpresaCtrl',
      reloadOnSearch: false,
      data: {
        pageTitle: 'Selecciona tu empresa'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              //'lazy_uikit',
              //'lazy_iCheck',
              'app/febos/partners/selector/selectorEmpresaController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.reporte", {
                            url: "/:app/reportes/dte",
                            templateUrl: 'app/febos/reportes/dte/reporteDte.html',
                            controller: 'reporteDteCtrl',
                            resolve: {
                                EstadoAnterior: [
                                    "$state",
                                    function ($state) {
                                        var currentStateData = {
                                            estado: $state.current.name,
                                            parametros: $state.params,
                                            url: $state.href($state.current.name, $state.params)
                                        };
                                        return currentStateData;
                                    }
                                ],
                                Autorizacion: ['Autorizacion', function (Autorizacion) {
                                        try {
                                            Autorizacion.verificar();
                                        } catch (e) {
                                            console.log("Error al autenticar", e);
                                        }
                                        return;
                                    }],
                                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_countUp',
                                            'lazy_charts_peity',
                                            'lazy_charts_easypiechart',
                                            'lazy_charts_metricsgraphics',
                                            'lazy_charts_chartist',
                                            'lazy_weathericons',
                                            'lazy_google_maps',
                                            'lazy_clndr',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'lazy_tree',
                                            'lazy_wizard',
                                            'app/febos/reportes/dte/reporteDteController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Dashboard'
                            },
                            ncyBreadcrumb: {
                                label: 'Home'
                            }
                        })

            }]);

febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.logs", {
                            url: "/:app/seguridad/logs?seguimientoId",
                            templateUrl: 'app/febos/seguridad/registros/registrosView.html',
                            controller: 'registrosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/logs.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/seguridad/registros/registrosController.js'
                                        ]);
                                    }]
                            }
                        })

            }]);
febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.tokens", {
                            url: "/:app/seguridad/integraciones",
                            templateUrl: 'app/febos/seguridad/tokens/tokensView.html',
                            controller: 'tokensCtrl',
                            resolve: {
                                EstadoAnterior: [
                                    "$state",
                                    function ($state) {
                                        var currentStateData = {
                                            estado: $state.current.name,
                                            parametros: $state.params,
                                            url: $state.href($state.current.name, $state.params)
                                        };
                                        return currentStateData;
                                    }
                                ],
                                Autorizacion: ['Autorizacion', function (Autorizacion) {
                                        try {
                                            Autorizacion.verificar();
                                        } catch (e) {
                                            console.log("Error al autenticar", e);
                                        }
                                        return;
                                    }],
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            // ocLazyLoad config (app/app.js)
                                            'lazy_countUp',
                                            'lazy_charts_peity',
                                            'lazy_charts_easypiechart',
                                            'lazy_charts_metricsgraphics',
                                            'lazy_charts_chartist',
                                            'lazy_weathericons',
                                            'lazy_google_maps',
                                            'lazy_clndr',
                                            'app/febos/seguridad/tokens/tokensController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Dashboard'
                            },
                            ncyBreadcrumb: {
                                label: 'Home'
                            }
                        })

            }]);

/*febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("especial.cambioClave", {
                    url: "/:app/cambio_clave",
                    templateUrl: 'app/febos/usuarios/cambio_clave/cambioClaveView.html',
                    controller: 'cambioClaveCtrl',
                    resolve: {             
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_uikit',
                                    'lazy_iCheck',
                                    'app/febos/usuarios/cambio_clave/cambioClaveController.js'
                                ]);
                            }]
                    }
                })

            }]);

*/
febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.usuarios_invitar', {
      url: '/:app/usuarios/invitar/:vista',
      templateUrl: 'app/febos/usuarios/invitar/invitarUsuariosView.html',
      controller: 'invitarUsuariosCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/usuarios/invitar/invitarUsuariosController.js'
            ]);
          }
        ],
        verificacionDeParametros: [
          '$stateParams',
          '$state',
          function($stateParams, $state) {
            var st = {};
            st.vista = $stateParams.vista;

            return st;
          }
        ]
      }
    });
  }
]);

febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("ingreso", {
                    url: "/:app/ingreso",
                    templateUrl: 'app/febos/usuarios/login/ingresoView.html',
                    controller: 'ingresoCtrl',
                    reloadOnSearch: false,
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                console.log("ingreso!");
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_uikit',
                                    'lazy_iCheck',
                                    'app/febos/usuarios/login/ingresoController.js'
                                ]);
                            }]
                    },
                    data: {
                        pageTitle: 'Ingreso'
                    }
                })

            }]);
febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.perfil_usuario", {
                    url: "/:app/usuarios/perfil",
                    templateUrl: 'app/febos/usuarios/perfil/perfilView.html',
                    controller: 'perfilCtrl',
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                            
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                            }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    // ocLazyLoad config (app/app.js)
                                    'lazy_countUp',
                                    'lazy_charts_peity',
                                    'lazy_charts_easypiechart',
                                    'lazy_charts_metricsgraphics',
                                    'lazy_charts_chartist',
                                    'lazy_weathericons',
                                    'lazy_google_maps',
                                    'lazy_clndr',
                                    'lazy_KendoUI',
                                    'lazy_uikit',
                                    'lazy_iCheck',
                                   // 'lazy_colorPicker',
                                    'app/febos/usuarios/perfil/perfilController.js'
                                ]);
                            }]
                    },
                    data: {
                        pageTitle: 'Dashboard'
                    }
                })

            }]);

febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("especial.precargaCache", {
                    url: "/:app/cargando",
                    templateUrl: 'app/febos/usuarios/precargaCache/precargaCacheView.html',
                    controller: 'precargaCacheCtrl',
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_uikit',
                                    'lazy_iCheck',
                                    'app/febos/usuarios/precargaCache/precargaCacheController.js'
                                ]);
                            }]
                    },
                    data: {
                        pageTitle: 'Cargando...'
                    }
                })

            }]);
febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {
                $stateProvider.state("restringido.arbol_grupos", {
                    url: "/:app/usuarios/organizacion",
                    templateUrl: 'app/febos/usuarios/unidades_organizativas/arbolView.html',
                    controller: 'arbolUsuariosCtrl',
                    resolve: {
                        ath:[
                            function(){
                                console.log("AQUI!");
                            }
                        ],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_uikit', 
                                    'lazy_iCheck',
                                    'lazy_tree',                                
                                    'app/febos/usuarios/unidades_organizativas/arbolController.js'
                                ]);
                            }]
                    }
                })
            }]);


febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.configuracion_templates', {
      url: '/:app/configuraciones/template/documentos',
      templateUrl: 'app/febos/configuraciones/template/documentos/templateView.html',
      controller: 'configurarTemplateCtrl',
      params: {
        app: 'cloud'
      },
      reloadOnSearch: false,
      resolve: {
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          'FebosAPI',
          function(FebosAPI) {
            return FebosAPI.cl_adm_configuracion_listar({ parametros: 'layout.tipos' }).then(
              function(response) {
                if (response.data.codigo == 10 && response.data.configuraciones.length > 0) {
                  //console.log(response.data.configuraciones[0].valor);
                  return { tipos: response.data.configuraciones[0].valor };
                } else {
                  return { tipos: '33,34,39,41' };
                }
              }
            );
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load(
              [
                // ocLazyLoad config (app/app.js)
                'bower_components/jquery-ui/jquery-ui.min.js',
                'lazy_uiSelect',
                'lazy_selectizeJS',
                'app/febos/configuraciones/template/documentos/templateController.js'
              ],
              { serie: true }
            );
          }
        ]
      },
      data: {
        pageTitle: 'Configuracion de Template Documentos'
      },
      ncyBreadcrumb: {
        label: 'Templates'
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.anticipos', {
      url: '/:app/documentos/portafolio/:categoria/anticipos?pagina&itemsPorPagina&orden&filtros',
      templateUrl: 'app/febos/documentos/portafolio/anticipos/documentosView.html',
      controller: 'anticiposCtrl',
      //reloadOnSearch: false,
      params: {
        codigo: '',
        responsable: ''
      },
      data: {
        pageTitle: 'Anticipos'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            console.log('A');
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            console.log('B');
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            return FebosAPI.cl_listar_proyectos(
              {},
              {
                codigo: $stateParams.codigo,
                responsable: $stateParams.responsable
              },
              true,
              true
            ).then(function(response) {
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/documentos/portafolio/anticipos/documentosController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.boletas_garantia', {
      url: '/:app/documentos/portafolio/:categoria/garantias?pagina&itemsPorPagina&orden&filtros',
      templateUrl: 'app/febos/documentos/portafolio/boletas/documentosView.html',
      controller: 'boletasGarantiaCtrl',
      //reloadOnSearch: false,
      params: {
        codigo: '',
        responsable: ''
      },
      data: {
        pageTitle: 'Boletas de Garantia'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            console.log('A');
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            console.log('B');
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            return FebosAPI.cl_listar_proyectos(
              {},
              {
                codigo: $stateParams.codigo,
                responsable: $stateParams.responsable
              },
              true,
              true
            ).then(function(response) {
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/documentos/portafolio/boletas/documentosController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.boletasgarantia', {
      url: '/:app/documentos/portafolio/boletas/garantias/:categoria/:vista',
      templateUrl: 'app/febos/documentos/portafolio/boletasgarantia/documentosView.html',
      controller: 'boletaGarantiaCtrl',
      //reloadOnSearch: false,
      params: {
        codigo: '',
        responsable: ''
      },
      data: {
        pageTitle: 'Boletas de Garantía'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            console.log('ANTERIOR');
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            console.log('NUEVO');
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            console.log('LISTAR', $stateParams);
            return FebosAPI.cl_listar_boleta_garantia(
              { tipo: $stateParams.categoria },
              {
                codigo: $stateParams.codigo,
                responsable: $stateParams.responsable
              },
              true,
              true
            ).then(function(response) {
              console.log('listados');
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            console.log('laz');
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/aprobaciones/aprobacionesService.js',
              'app/febos/documentos/portafolio/boletasgarantia/documentosController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.contratos', {
      url: '/:app/documentos/portafolio/:categoria/contratos?pagina&itemsPorPagina&orden&filtros',
      templateUrl: 'app/febos/documentos/portafolio/contratos/documentosView.html',
      controller: 'contratosCtrl',
      //reloadOnSearch: false,
      params: {
        codigo: '',
        responsable: ''
      },
      data: {
        pageTitle: 'Contratos'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            return FebosAPI.cl_listar_proyectos(
              {},
              {
                codigo: $stateParams.codigo,
                responsable: $stateParams.responsable
              },
              true,
              true
            ).then(function(response) {
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/documentos/portafolio/contratos/documentosController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.hitos', {
      url: '/:app/documentos/portafolio/:categoria/hitos?pagina&itemsPorPagina&orden&filtros',
      templateUrl: 'app/febos/documentos/portafolio/hitos/documentosView.html',
      controller: 'hitosCtrl',
      //reloadOnSearch: false,
      params: {
        codigo: '',
        responsable: ''
      },
      data: {
        pageTitle: 'Hitos de pago'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            return FebosAPI.cl_listar_proyectos(
              {},
              {
                codigo: $stateParams.codigo,
                responsable: $stateParams.responsable
              },
              true,
              true
            ).then(function(response) {
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/documentos/portafolio/hitos/documentosController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.polizas', {
      url: '/:app/documentos/portafolio/:categoria/polizas?pagina&itemsPorPagina&orden&filtros',
      templateUrl: 'app/febos/documentos/portafolio/polizas/documentosView.html',
      controller: 'polizasCtrl',
      //reloadOnSearch: false,
      params: {
        codigo: '',
        responsable: ''
      },
      data: {
        pageTitle: 'Pólizas'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            console.log('A');
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            console.log('B');
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            return FebosAPI.cl_listar_polizas(
              {},
              {
                codigo: $stateParams.codigo,
                responsable: $stateParams.responsable
              },
              true,
              true
            ).then(function(response) {
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/documentos/portafolio/polizas/documentosController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.portafolio', {
      url: '/:app/documentos/portafolio/:categoria/proyectos?pagina&itemsPorPagina&orden&filtros',
      templateUrl: 'app/febos/documentos/portafolio/proyectos/documentosView.html',
      controller: 'portafolioCtrl',
      //reloadOnSearch: false,
      params: {
        codigo: '',
        responsable: ''
      },
      data: {
        pageTitle: 'Documentos de Portafolio'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            console.log('A ==>', currentStateData);
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            console.log('B');
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            return FebosAPI.cl_listar_proyectos(
              {},
              {
                codigo: $stateParams.codigo,
                responsable: $stateParams.responsable
              },
              true,
              true
            ).then(function(response) {
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/documentos/portafolio/proyectos/documentosController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.unidadestecnicas', {
      url: '/:app/documentos/portafolio/unidad/tecnica/?pagina&itemsPorPagina&orden&filtros',
      templateUrl: 'app/febos/documentos/portafolio/unidadestecnicas/unidadesTecnicas.html',
      controller: 'unidadesController',
      //reloadOnSearch: false,
      params: {
        codigo: '',
        responsable: ''
      },
      data: {
        pageTitle: 'Unidades Tecnicas'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            console.log('ANTERIOR');
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            console.log('NUEVO');
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            try {
              console.log('CONSULTANDO CONFIGURACION');
              $stateParams.region = 0;
              return FebosAPI.cl_listar_unidades_tecnicas({}).then(function(response) {
                return response.data.listado;
              });
            } catch (e) {
              console.log(e);
            }
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            console.log('laz');
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/documentos/portafolio/unidadestecnicas/unidadesTecnicasController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.bitacora_dte', {
      url: '/:app/documentos/bitacora/:febosid?esBoleta&tipo',
      templateUrl: 'app/febos/documentos/electronicos/acciones/bitacora/bitacoraDteView.html',
      params: {
        esBoleta: '',
        tipo: ''
      },
      controller: 'bitacoraDteCtrl',
      reloadOnSearch: true,
      data: {
        pageTitle: 'Bitácora'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        bitacora: [
          'FebosAPI',
          '$stateParams',
          function(FebosAPI, $stateParams) {
            if ($stateParams.febosid.length > 40) {
              return FebosAPI.cl_listar_bitacora_portafolio(
                {
                  documentoId: $stateParams.febosid
                },
                {},
                true,
                false
              ).then(function(response) {
                return response.data;
              });
            } else {
              return FebosAPI.cl_listar_bitacora_dte(
                {
                  febosId: $stateParams.febosid,
                  pagina: 1,
                  filas: 200
                },
                {},
                true,
                false
              ).then(function(response) {
                return response.data;
              });
            }
          }
        ],
        esBoleta: [
          '$stateParams',
          function($stateParams) {
            console.log('validando si es boleta');
            console.log($stateParams);
            return $stateParams.esBoleta == 'si';
          }
        ],
        dte: [
          'FebosAPI',
          '$stateParams',
          function(FebosAPI, $stateParams) {
            if ($stateParams.febosid.length > 40) {
              return FebosAPI.cl_obtener_documento_portafolio(
                {
                  documentoId: $stateParams.febosid,
                  //incrustar:$stateParams.esBoleta=='si'?'no':'si',
                  incrustar: 'no',
                  imagen: 'si',
                  tipo: $stateParams.tipo
                },
                {},
                false,
                false
              ).then(
                function(response) {
                  return response.data;
                },
                function(err) {
                  return {};
                }
              );
            } else {
              return FebosAPI.cl_obtener_documento(
                {
                  febosId: $stateParams.febosid,
                  //incrustar:$stateParams.esBoleta=='si'?'no':'si',
                  incrustar: 'no',
                  imagen: 'si',
                  regenerar: 'si',
                  tipoImagen: $stateParams.esBoleta == 'si' ? '0' : '1'
                },
                {},
                false,
                false
              ).then(
                function(response) {
                  return response.data;
                },
                function(err) {
                  return {};
                }
              );
            }
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'app/febos/documentos/electronicos/acciones/bitacora/bitacoraDteController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {
                $stateProvider.state("restringido.bitacora_dnt", {
                    url: "/:app/documentos/no_tributarios/bitacora/:febosid",
                    templateUrl: 'app/febos/documentos/no_tributarios/acciones/bitacora/bitacoraDntView.html',
                    controller: 'bitacoraDntCtrl',
                    reloadOnSearch: true,
                    data: {
                        pageTitle: 'Bitácora'
                    },
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                console.log("a");
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                            console.log("a");
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                            }],
                        bitacora:['FebosAPI','$stateParams',function(FebosAPI,$stateParams){
                            console.log("b");
                                return FebosAPI.cl_listar_bitacora_dnt({
                                    febosId:$stateParams.febosid,
                                    pagina:1,
                                    filas:200
                                },{},true,false).then(function (response) {
                                    return response.data;
                                });
                        }],
                        dte:['FebosAPI','$stateParams',function(FebosAPI,$stateParams){
                            console.log("c");
                                return FebosAPI.cl_obtener_dnt({
                                    febosId:$stateParams.febosid,
                                    incrustar:'si',
                                    imagen:'si',
                                    tipoImagen:'1'
                                },{},true,false).then(function (response) {
                                    return response.data;
                                });
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            console.log("d");
                                return $ocLazyLoad.load([
                                    'bower_components/jquery-ui/jquery-ui.min.js',
                                    'lazy_uiSelect',
                                    'app/febos/documentos/no_tributarios/acciones/bitacora/bitacoraDntController.js',
                                ]);
                            }]
                    }
                })

            }]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.portafolio-crear-proyecto', {
      url: '/:app/portafolio/proyectos/nuevo',
      templateUrl:
        'app/febos/documentos/portafolio/proyectos/acciones/crear/crearProyectoView.html',
      controller: 'crearProyectoCtrl',
      data: {
        pageTitle: 'Nuevo Proyecto'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            console.log('AAA ==>', currentStateData);
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            console.log('B');
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            return {};
          }
        ],
        Provincias: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            try {
              console.log('CONSULTANDO CONFIGURACION');
              $stateParams.region = 0;
              return FebosAPI.cl_adm_configuracion_listar({
                parametros: 'proyecto.provincia.listar'
              }).then(function(response) {
                try {
                  $stateParams.region = response.data.configuraciones[0].valor;
                } catch (e) {
                  console.log('ERROR');
                }
                console.log('RESPUESTA API CONFIGURACION', response);
                return FebosAPI.cl_listar_provincias(
                  { regionid: $stateParams.region },
                  {},
                  true,
                  true
                ).then(function(response) {
                  console.log('respuesta api provincias', response);
                  return response.data.lugares;
                });
              });
            } catch (e) {
              console.log(e);
            }
          }
        ],
        UnidadesOrganizativas: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            try {
              console.log('CONSULTANDO CONFIGURACION');
              $stateParams.region = 0;
              return FebosAPI.cl_listar_unidades_tecnicas({}).then(function(response) {
                return response.data.listado;
              });
            } catch (e) {
              console.log(e);
            }
          }
        ],
        SubTitulos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            try {
              return [
                {
                  numero: '22',
                  nombre: 'SALDO INICIAL DE CAJA',
                  item: [
                    {
                      item: '11',
                      nombre: 'Estudios Propios del giro',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Compensaciones por Daños a Terceros y/o a la Propiedad'
                        }
                      ]
                    }
                  ]
                },
                {
                  numero: '24',
                  nombre: 'TRANSFERENCIAS CORRIENTES',
                  item: [
                    {
                      item: '01',
                      nombre: 'Al Sector Privado',
                      asignacion: [
                        {
                          numero: '100',
                          nombre: 'Aplicación Numeral 2.1 Glosa 02 Común para Gobiernos Regionales'
                        },
                        {
                          numero: '101',
                          nombre: 'Cultura'
                        },
                        {
                          numero: '102',
                          nombre: 'Deporte'
                        },
                        {
                          numero: '103',
                          nombre: 'Seguridad Ciudadna'
                        },
                        {
                          numero: '999',
                          nombre: 'Otros'
                        }
                      ]
                    },
                    {
                      item: '03',
                      nombre: 'A Otras Entidades Públicas',
                      asignacion: [
                        {
                          numero: '100',
                          nombre: 'Aplicación Numeral 2.1 Glosa 02 Común para Gobiernos Regionales'
                        },
                        {
                          numero: '101',
                          nombre: 'Cultura'
                        },
                        {
                          numero: '102',
                          nombre: 'Deporte'
                        },
                        {
                          numero: '103',
                          nombre: 'Seguridad Ciudadna'
                        },
                        {
                          numero: '999',
                          nombre: 'Otros'
                        }
                      ]
                    }
                  ]
                },
                {
                  numero: '26',
                  nombre: 'INICIATIVAS DE INVERSIÓN',
                  item: [
                    {
                      item: '02',
                      nombre: 'Compensaciones por Daños a Terceros y/o a la Propiedad',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Compensaciones por Daños a Terceros y/o a la Propiedad'
                        }
                      ]
                    }
                  ]
                },
                {
                  numero: '29',
                  nombre: 'ADQUISICIÓN DE ACTIVOS NO FINANCIEROS',
                  item: [
                    {
                      item: '01',
                      nombre: 'Terreno',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Terreno'
                        }
                      ]
                    },
                    {
                      item: '02',
                      nombre: 'Edificios',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Edificios'
                        }
                      ]
                    },
                    {
                      item: '03',
                      nombre: 'Vehículos',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Vehículos'
                        }
                      ]
                    },
                    {
                      item: '04',
                      nombre: 'Mobiliario y Otros',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Mobiliario y Otros'
                        }
                      ]
                    },
                    {
                      item: '05',
                      nombre: 'Máquinas y Equipos',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Máquinas y Equipos'
                        }
                      ]
                    },
                    {
                      item: '06',
                      nombre: 'Equipos Informáticos',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Equipos Informáticos'
                        }
                      ]
                    },
                    {
                      item: '07',
                      nombre: 'Programas Informáticos',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Programas Informáticos'
                        },
                        {
                          numero: '002',
                          nombre: 'Sistema de Información '
                        }
                      ]
                    },
                    {
                      item: '99',
                      nombre: 'Otros Activos no Financieros',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Otros Activos no Financieros'
                        }
                      ]
                    }
                  ]
                },
                {
                  numero: '31',
                  nombre: 'INICIATIVAS DE INVERSIÓN',
                  item: [
                    {
                      item: '01',
                      nombre: 'Estudios Básicos',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Gasto administrativo'
                        },
                        {
                          numero: '002',
                          nombre: 'Consultoria'
                        }
                      ]
                    },
                    {
                      item: '02',
                      nombre: 'Proyectos',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Gasto administrativo'
                        },
                        {
                          numero: '002',
                          nombre: 'Consultoria'
                        },
                        {
                          numero: '003',
                          nombre: 'Terreno'
                        },
                        {
                          numero: '004',
                          nombre: 'Obras Civiles'
                        },
                        {
                          numero: '005',
                          nombre: 'Equipamiento'
                        },
                        {
                          numero: '006',
                          nombre: 'Equipos'
                        },
                        {
                          numero: '007',
                          nombre: 'Vehiculos'
                        },
                        {
                          numero: '008',
                          nombre: 'otros'
                        }
                      ]
                    },
                    {
                      item: '03',
                      nombre: 'Programas de Inversión',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Gasto administrativo'
                        },
                        {
                          numero: '002',
                          nombre: 'Consultoria'
                        },
                        {
                          numero: '003',
                          nombre: 'Programa'
                        }
                      ]
                    }
                  ]
                },
                {
                  numero: '33',
                  nombre: 'TRANSFERENCIAS DE CAPITAL',
                  item: [
                    {
                      item: '01',
                      nombre: 'Al Sector Privado',
                      asignacion: [
                        {
                          numero: '004',
                          nombre: 'Junta Nacional del Cuerpo de Bomberos de Chile'
                        },
                        {
                          numero: '010',
                          nombre: 'Aplicación Letra a) Artículo Cuarto Transitorio Ley N°20.378'
                        }
                      ]
                    },
                    {
                      item: '02',
                      nombre: 'Al Gobierno  Central',
                      asignacion: [
                        {
                          numero: '039',
                          nombre: 'Servicio de Salud Valparaíso-San Antonio - FAR'
                        },
                        {
                          numero: '040',
                          nombre: 'SERVIU V REGIÓN'
                        }
                      ]
                    },
                    {
                      item: '03',
                      nombre: 'A Otras Entidades Públicas',
                      asignacion: [
                        {
                          numero: '125',
                          nombre: 'Municipalidades (Fondo Regional de Iniciativa Local)'
                        },
                        {
                          numero: '206',
                          nombre:
                            'SEREMI de Minería - Programa Regularización y Fomento Pequeña Minería Región de Valparaíso (30396032-0)'
                        },
                        {
                          numero: '207',
                          nombre:
                            'INDAP-Transferencia tecnológica para obras civiles de riego IV (30470441-0)'
                        },
                        {
                          numero: '208',
                          nombre:
                            'SEREMI de Minería - Regularización y fomento pequeña minería II Etapa (30458145-0)'
                        },
                        {
                          numero: '209',
                          nombre:
                            'Subsecretaría de Pesca (FAP) - Fomento y desarrollo productivo para el sector pesquero artesanal (30483822-0)'
                        }
                      ]
                    }
                  ]
                }
              ];
            } catch (e) {
              console.log(e);
            }
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'lazy_masked_inputs',
              'app/febos/documentos/portafolio/proyectos/acciones/crear/crearProyectoController.js'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.portafolio-editar-proyecto', {
      url: '/:app/portafolio/proyectos/editar/:proyectoId',
      templateUrl:
        'app/febos/documentos/portafolio/proyectos/acciones/editar/editarProyectoView.html',
      controller: 'editarProyectoCtrl',
      data: {
        pageTitle: 'Editar Proyecto'
      },
      params: {
        proyectoId: ''
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            console.log('EDITANDO!');
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            try {
              return FebosAPI.cl_obtener_proyecto(
                { proyectoId: $stateParams.proyectoId },
                {},
                true,
                true
              ).then(function(response) {
                console.log('respuesta api', response);
                return response.data.proyecto;
              });
            } catch (e) {
              console.log(e);
            }
          }
        ],
        Provincias: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            try {
              console.log('CONSULTANDO CONFIGURACION');
              $stateParams.region = 0;
              return FebosAPI.cl_adm_configuracion_listar({
                parametros: 'proyecto.provincia.listar'
              }).then(function(response) {
                try {
                  $stateParams.region = response.data.configuraciones[0].valor;
                } catch (e) {
                  console.log('ERROR');
                }
                console.log('RESPUESTA API CONFIGURACION', response);
                return FebosAPI.cl_listar_provincias(
                  { regionid: $stateParams.region },
                  {},
                  true,
                  true
                ).then(function(response) {
                  console.log('respuesta api provincias', response);
                  return response.data.lugares;
                });
              });
            } catch (e) {
              console.log(e);
            }
          }
        ],
        UnidadesOrganizativas: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            try {
              console.log('CONSULTANDO CONFIGURACION');
              $stateParams.region = 0;
              return FebosAPI.cl_listar_unidades_tecnicas({}).then(function(response) {
                return response.data.listado;
              });
            } catch (e) {
              console.log(e);
            }
          }
        ],
        SubTitulos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            try {
              return [
                {
                  numero: '22',
                  nombre: 'SALDO INICIAL DE CAJA',
                  item: [
                    {
                      item: '11',
                      nombre: 'Estudios Propios del giro',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Compensaciones por Daños a Terceros y/o a la Propiedad'
                        }
                      ]
                    }
                  ]
                },
                {
                  numero: '24',
                  nombre: 'TRANSFERENCIAS CORRIENTES',
                  item: [
                    {
                      item: '01',
                      nombre: 'Al Sector Privado',
                      asignacion: [
                        {
                          numero: '100',
                          nombre: 'Aplicación Numeral 2.1 Glosa 02 Común para Gobiernos Regionales'
                        },
                        { numero: '101', nombre: 'Cultura' },
                        {
                          numero: '102',
                          nombre: 'Deporte'
                        },
                        { numero: '103', nombre: 'Seguridad Ciudadna' },
                        {
                          numero: '999',
                          nombre: 'Otros'
                        }
                      ]
                    },
                    {
                      item: '03',
                      nombre: 'A Otras Entidades Públicas',
                      asignacion: [
                        {
                          numero: '100',
                          nombre: 'Aplicación Numeral 2.1 Glosa 02 Común para Gobiernos Regionales'
                        },
                        { numero: '101', nombre: 'Cultura' },
                        {
                          numero: '102',
                          nombre: 'Deporte'
                        },
                        { numero: '103', nombre: 'Seguridad Ciudadna' },
                        {
                          numero: '999',
                          nombre: 'Otros'
                        }
                      ]
                    }
                  ]
                },
                {
                  numero: '26',
                  nombre: 'INICIATIVAS DE INVERSIÓN',
                  item: [
                    {
                      item: '02',
                      nombre: 'Compensaciones por Daños a Terceros y/o a la Propiedad',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Compensaciones por Daños a Terceros y/o a la Propiedad'
                        }
                      ]
                    }
                  ]
                },
                {
                  numero: '29',
                  nombre: 'ADQUISICIÓN DE ACTIVOS NO FINANCIEROS',
                  item: [
                    {
                      item: '01',
                      nombre: 'Terreno',
                      asignacion: [{ numero: '001', nombre: 'Terreno' }]
                    },
                    {
                      item: '02',
                      nombre: 'Edificios',
                      asignacion: [{ numero: '001', nombre: 'Edificios' }]
                    },
                    {
                      item: '03',
                      nombre: 'Vehículos',
                      asignacion: [{ numero: '001', nombre: 'Vehículos' }]
                    },
                    {
                      item: '04',
                      nombre: 'Mobiliario y Otros',
                      asignacion: [{ numero: '001', nombre: 'Mobiliario y Otros' }]
                    },
                    {
                      item: '05',
                      nombre: 'Máquinas y Equipos',
                      asignacion: [{ numero: '001', nombre: 'Máquinas y Equipos' }]
                    },
                    {
                      item: '06',
                      nombre: 'Equipos Informáticos',
                      asignacion: [{ numero: '001', nombre: 'Equipos Informáticos' }]
                    },
                    {
                      item: '07',
                      nombre: 'Programas Informáticos',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Programas Informáticos'
                        },
                        { numero: '002', nombre: 'Sistema de Información ' }
                      ]
                    },
                    {
                      item: '99',
                      nombre: 'Otros Activos no Financieros',
                      asignacion: [{ numero: '001', nombre: 'Otros Activos no Financieros' }]
                    }
                  ]
                },
                {
                  numero: '31',
                  nombre: 'INICIATIVAS DE INVERSIÓN',
                  item: [
                    {
                      item: '01',
                      nombre: 'Estudios Básicos',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Gasto administrativo'
                        },
                        { numero: '002', nombre: 'Consultoria' }
                      ]
                    },
                    {
                      item: '02',
                      nombre: 'Proyectos',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Gasto administrativo'
                        },
                        { numero: '002', nombre: 'Consultoria' },
                        {
                          numero: '003',
                          nombre: 'Terreno'
                        },
                        { numero: '004', nombre: 'Obras Civiles' },
                        {
                          numero: '005',
                          nombre: 'Equipamiento'
                        },
                        { numero: '006', nombre: 'Equipos' },
                        {
                          numero: '007',
                          nombre: 'Vehiculos'
                        },
                        { numero: '008', nombre: 'otros' }
                      ]
                    },
                    {
                      item: '03',
                      nombre: 'Programas de Inversión',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Gasto administrativo'
                        },
                        { numero: '002', nombre: 'Consultoria' },
                        {
                          numero: '003',
                          nombre: 'Programa'
                        }
                      ]
                    }
                  ]
                },
                {
                  numero: '33',
                  nombre: 'TRANSFERENCIAS DE CAPITAL',
                  item: [
                    {
                      item: '01',
                      nombre: 'Al Sector Privado',
                      asignacion: [
                        {
                          numero: '004',
                          nombre: 'Junta Nacional del Cuerpo de Bomberos de Chile'
                        },
                        {
                          numero: '010',
                          nombre: 'Aplicación Letra a) Artículo Cuarto Transitorio Ley N°20.378'
                        }
                      ]
                    },
                    {
                      item: '02',
                      nombre: 'Al Gobierno  Central',
                      asignacion: [
                        {
                          numero: '039',
                          nombre: 'Servicio de Salud Valparaíso-San Antonio - FAR'
                        },
                        { numero: '040', nombre: 'SERVIU V REGIÓN' }
                      ]
                    },
                    {
                      item: '03',
                      nombre: 'A Otras Entidades Públicas',
                      asignacion: [
                        {
                          numero: '125',
                          nombre: 'Municipalidades (Fondo Regional de Iniciativa Local)'
                        },
                        {
                          numero: '206',
                          nombre:
                            'SEREMI de Minería - Programa Regularización y Fomento Pequeña Minería Región de Valparaíso (30396032-0)'
                        },
                        {
                          numero: '207',
                          nombre:
                            'INDAP-Transferencia tecnológica para obras civiles de riego IV (30470441-0)'
                        },
                        {
                          numero: '208',
                          nombre:
                            'SEREMI de Minería - Regularización y fomento pequeña minería II Etapa (30458145-0)'
                        },
                        {
                          numero: '209',
                          nombre:
                            'Subsecretaría de Pesca (FAP) - Fomento y desarrollo productivo para el sector pesquero artesanal (30483822-0)'
                        }
                      ]
                    }
                  ]
                }
              ];
            } catch (e) {
              console.log(e);
            }
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/documentos/portafolio/proyectos/acciones/editar/editarProyectoController.js',
              'lazy_masked_inputs'
            ]);
          }
        ]
      }
    });
  }
]);

febosApp.factory('AprobacionService', function($http, FebosAPI) {
  FebosAPI['cl_aprobaciones_listar'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_listar',
    'GET',
    "'/aprobaciones'"
  );

  FebosAPI['cl_aprobaciones_bandeja'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_bandeja',
    'GET',
    "'/aprobaciones/bandejaentrada'"
  );

  FebosAPI['cl_acuse_recibo'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_acusar_recibo',
    'PUT',
    "'/aprobaciones/acuserecibo'"
  );

  FebosAPI['cl_reenviar_ejecucion'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_reenviar_ejecucion',
    'PUT',
    "'/aprobaciones/procesarflujo'"
  );

  FebosAPI['cl_aprobaciones_procesar_flujo'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_procesar_flujo',
    'PUT',
    "'/aprobaciones/procesarflujo'"
  );

  FebosAPI['cl_ejecuciones_estado_actual'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_ejecuciones_estado_actual',
    'GET',
    "'/aprobaciones/ejecucionestadoactual'"
  );

  FebosAPI['cl_aprobaciones_listar_gatillar'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_listar_gatillar',
    'GET',
    "'/aprobaciones/gatilladortipo'"
  );

  FebosAPI['cl_listar_usuarios_grupos'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_listar_usuarios_grupos',
    'GET',
    "'/usuarios/porgrupo'"
  );

  FebosAPI['cl_aprobaciones_crear'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_crear',
    'POST',
    "'/aprobaciones'"
  );

  FebosAPI['cl_aprobaciones_listar_tipo_rol'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_listar_tipo_rol',
    'GET',
    "'/aprobaciones/tiporol'"
  );

  FebosAPI['cl_aprobaciones_listar_tipo_paso'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_listar_tipo_paso',
    'GET',
    "'/aprobaciones/tipopaso'"
  );

  FebosAPI['cl_aprobaciones_listar_gatillador_proceso'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_listar_gatillador_proceso',
    'GET',
    "'/aprobaciones/gatilladorproceso'"
  );

  FebosAPI['cl_aprobaciones_comentario_crear'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_comentario_crear',
    'POST',
    "'/aprobaciones/comentario'"
  );

  return FebosAPI;
});
