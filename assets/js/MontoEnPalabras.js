/*************************************************************/
// NumeroALetras
// The MIT License (MIT)
//
// Copyright (c) 2015 Luis Alfredo Chee
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// @author Rodolfo Carmona
// @contributor Jean (jpbadoino@gmail.com)
/*************************************************************/

function tipoMoneda() {
  return [
    { codigo: 'AED', descripcion: 'Dírham de los Emiratos Árabes Unidos' },
    { codigo: 'AFN', descripcion: 'Afghani' },
    { codigo: 'ALL', descripcion: 'Lek' },
    { codigo: 'AMD', descripcion: 'Dram armenio' },
    { codigo: 'ANG', descripcion: 'Florín antillano neerlandés' },
    { codigo: 'AOA', descripcion: 'Kwanza' },
    { codigo: 'ARS', descripcion: 'Peso Argentino' },
    { codigo: 'AUD', descripcion: 'Dólar australiano' },
    { codigo: 'AWG', descripcion: 'Florín arubeño' },
    { codigo: 'AZN', descripcion: 'Manat azerbaiyano' },
    { codigo: 'BAM', descripcion: 'Marco bosnioherzegovino' },
    { codigo: 'BBD', descripcion: 'Dólar de Barbados' },
    { codigo: 'BDT', descripcion: 'Taka' },
    { codigo: 'BGN', descripcion: 'Lev búlgaro' },
    { codigo: 'BHD', descripcion: 'Dinar de Bahrein' },
    { codigo: 'BIF', descripcion: 'Franco Burundi' },
    { codigo: 'BMD', descripcion: 'Dólar de Bermudas' },
    { codigo: 'BND', descripcion: 'Dólar de Brunei' },
    { codigo: 'BOB', descripcion: 'Boliviano' },
    { codigo: 'BOV', descripcion: 'Mvdol' },
    { codigo: 'BRL', descripcion: 'Real Brasileño' },
    { codigo: 'BSD', descripcion: 'Dólar de las Bahamas' },
    { codigo: 'BTN', descripcion: 'Ngultrum' },
    { codigo: 'BWP', descripcion: 'Pula' },
    { codigo: 'BYR', descripcion: 'Rublo bielorruso' },
    { codigo: 'BZD', descripcion: 'Dólar de Belice' },
    { codigo: 'CAD', descripcion: 'Dólar canadiense' },
    { codigo: 'CDF', descripcion: 'Franco congoleño' },
    { codigo: 'CHE', descripcion: 'Euro WIR' },
    { codigo: 'CHF', descripcion: 'Franco suizo' },
    { codigo: 'CHW', descripcion: 'Franco WIR' },
    { codigo: 'CLF', descripcion: 'Unidad de Fomento' },
    { codigo: 'CLP', descripcion: 'Peso chileno' },
    { codigo: 'CNY', descripcion: 'Yuan' },
    { codigo: 'COP', descripcion: 'Peso ColombianO' },
    { codigo: 'COU', descripcion: 'Unidad de Valor Real' },
    { codigo: 'CRC', descripcion: 'Colón costarricense' },
    { codigo: 'CUC', descripcion: 'Peso Convertible' },
    { codigo: 'CUP', descripcion: 'Peso Cubano' },
    { codigo: 'CVE', descripcion: 'Cabo Verde Escudo' },
    { codigo: 'CZK', descripcion: 'Corona checa' },
    { codigo: 'DJF', descripcion: 'Franco de Djibouti' },
    { codigo: 'DKK', descripcion: 'Corona danesa' },
    { codigo: 'DOP', descripcion: 'Peso Dominicano' },
    { codigo: 'DZD', descripcion: 'Dinar argelino' },
    { codigo: 'EGP', descripcion: 'Libra egipcia' },
    { codigo: 'ERN', descripcion: 'Nakfa' },
    { codigo: 'ETB', descripcion: 'Birr etíope' },
    { codigo: 'EUR', descripcion: 'Euro' },
    { codigo: 'FJD', descripcion: 'Dólar de Fiji' },
    { codigo: 'FKP', descripcion: 'Libra malvinense' },
    { codigo: 'GBP', descripcion: 'Libra esterlina' },
    { codigo: 'GEL', descripcion: 'Lari' },
    { codigo: 'GHS', descripcion: 'Cedi de Ghana' },
    { codigo: 'GIP', descripcion: 'Libra de Gibraltar' },
    { codigo: 'GMD', descripcion: 'Dalasi' },
    { codigo: 'GNF', descripcion: 'Franco guineano' },
    { codigo: 'GTQ', descripcion: 'Quetzal' },
    { codigo: 'GYD', descripcion: 'Dólar guyanés' },
    { codigo: 'HKD', descripcion: 'Dolar de Hong Kong' },
    { codigo: 'HNL', descripcion: 'Lempira' },
    { codigo: 'HRK', descripcion: 'Kuna' },
    { codigo: 'HTG', descripcion: 'Gourde' },
    { codigo: 'HUF', descripcion: 'Florín' },
    { codigo: 'IDR', descripcion: 'Rupia' },
    { codigo: 'ILS', descripcion: 'Nuevo Shekel Israelí' },
    { codigo: 'INR', descripcion: 'Rupia india' },
    { codigo: 'IQD', descripcion: 'Dinar iraquí' },
    { codigo: 'IRR', descripcion: 'Rial iraní' },
    { codigo: 'ISK', descripcion: 'Corona islandesa' },
    { codigo: 'JMD', descripcion: 'Dólar jamaiquino' },
    { codigo: 'JOD', descripcion: 'Dinar jordano' },
    { codigo: 'JPY', descripcion: 'Yen' },
    { codigo: 'KES', descripcion: 'Chelín keniano' },
    { codigo: 'KGS', descripcion: 'Som' },
    { codigo: 'KHR', descripcion: 'Riel' },
    { codigo: 'KMF', descripcion: 'Franco Comoro' },
    { codigo: 'KPW', descripcion: 'Won norcoreano' },
    { codigo: 'KRW', descripcion: 'Won' },
    { codigo: 'KWD', descripcion: 'Dinar kuwaití' },
    { codigo: 'KYD', descripcion: 'Dólar de las Islas Caimán' },
    { codigo: 'KZT', descripcion: 'Tenge' },
    { codigo: 'LAK', descripcion: 'Kip' },
    { codigo: 'LBP', descripcion: 'Libra libanesa' },
    { codigo: 'LKR', descripcion: 'Rupia de Sri Lanka' },
    { codigo: 'LRD', descripcion: 'Dólar liberiano' },
    { codigo: 'LSL', descripcion: 'Loti' },
    { codigo: 'LYD', descripcion: 'Dinar libio' },
    { codigo: 'MAD', descripcion: 'Dirham marroquí' },
    { codigo: 'MDL', descripcion: 'Leu moldavo' },
    { codigo: 'MGA', descripcion: 'Ariary malgache' },
    { codigo: 'MKD', descripcion: 'Denar' },
    { codigo: 'MMK', descripcion: 'Kyat' },
    { codigo: 'MNT', descripcion: 'Tugrik' },
    { codigo: 'MOP', descripcion: 'Pataca' },
    { codigo: 'MRO', descripcion: 'Ouguiya' },
    { codigo: 'MUR', descripcion: 'Rupia de Mauricio' },
    { codigo: 'MVR', descripcion: 'Rufiyaa' },
    { codigo: 'MWK', descripcion: 'Kwacha' },
    { codigo: 'MXN', descripcion: 'Peso MexicanO' },
    { codigo: 'MXV', descripcion: 'Unidad de Inversion Mexicana (UDI)' },
    { codigo: 'MYR', descripcion: 'Ringgit malayo' },
    { codigo: 'MZN', descripcion: 'Metical mozambiqueño' },
    { codigo: 'NAD', descripcion: 'Dólar de Namibia' },
    { codigo: 'NGN', descripcion: 'Naira' },
    { codigo: 'NIO', descripcion: 'Cordoba Oro' },
    { codigo: 'NOK', descripcion: 'Corona noruega' },
    { codigo: 'NPR', descripcion: 'Rupia nepalí' },
    { codigo: 'NZD', descripcion: 'Dólar neozelandés' },
    { codigo: 'OMR', descripcion: 'Rial omaní' },
    { codigo: 'PAB', descripcion: 'Balboa' },
    { codigo: 'PEN', descripcion: 'Nuevo Sol' },
    { codigo: 'PGK', descripcion: 'Kina' },
    { codigo: 'PHP', descripcion: 'Peso filipino' },
    { codigo: 'PKR', descripcion: 'Rupia pakistaní' },
    { codigo: 'PLN', descripcion: 'Zloty' },
    { codigo: 'PYG', descripcion: 'Guaraní' },
    { codigo: 'QAR', descripcion: 'Riyal catarí' },
    { codigo: 'RON', descripcion: 'Leu rumano' },
    { codigo: 'RSD', descripcion: 'Dinar serbio' },
    { codigo: 'RUB', descripcion: 'Rublo ruso' },
    { codigo: 'RWF', descripcion: 'Franco ruandés' },
    { codigo: 'SAR', descripcion: 'Riyal saudí' },
    { codigo: 'SBD', descripcion: 'Dólar de las Islas Salomón' },
    { codigo: 'SCR', descripcion: 'Rupia seychelense' },
    { codigo: 'SDG', descripcion: 'Libra sudanesa' },
    { codigo: 'SEK', descripcion: 'Corona sueca' },
    { codigo: 'SGD', descripcion: 'Dolar de Singapur' },
    { codigo: 'SHP', descripcion: 'Libra de Santa Helena' },
    { codigo: 'SLL', descripcion: 'Leone' },
    { codigo: 'SOS', descripcion: 'Chelín somalí' },
    { codigo: 'SRD', descripcion: 'Dólar surinamés' },
    { codigo: 'SSP', descripcion: 'Libra sursudanesa' },
    { codigo: 'STD', descripcion: 'Dobra' },
    { codigo: 'SVC', descripcion: 'Colón' },
    { codigo: 'SYP', descripcion: 'Libra Siria' },
    { codigo: 'SZL', descripcion: 'Lilangeni' },
    { codigo: 'THB', descripcion: 'Baht' },
    { codigo: 'TJS', descripcion: 'Somoni' },
    { codigo: 'TMT', descripcion: 'Manat turcomano' },
    { codigo: 'TND', descripcion: 'Dinar tunecino' },
    { codigo: 'TOP', descripcion: 'Pa’anga' },
    { codigo: 'TRY', descripcion: 'Lira turca' },
    { codigo: 'TTD', descripcion: 'Dólar trinitense' },
    { codigo: 'TWD', descripcion: 'Nuevo dólar taiwanés' },
    { codigo: 'TZS', descripcion: 'Chelín tanzano' },
    { codigo: 'UAH', descripcion: 'Hryvnia' },
    { codigo: 'UGX', descripcion: 'Chelín ugandés' },
    { codigo: 'USD', descripcion: 'Dólar Americano' },
    { codigo: 'USN', descripcion: 'Dólar Americano (Next day)' },
    { codigo: 'UYI', descripcion: 'Uruguay Peso en Unidades Indexadas (URUIURUI)' },
    { codigo: 'UYU', descripcion: 'Peso Uruguayo' },
    { codigo: 'UZS', descripcion: 'Som uzbeko' },
    { codigo: 'VEF', descripcion: 'Bolívar' },
    { codigo: 'VND', descripcion: 'Dong' },
    { codigo: 'VUV', descripcion: 'Vatu' },
    { codigo: 'WST', descripcion: 'Tala' },
    { codigo: 'XAF', descripcion: 'Franco CFA BEAC' },
    { codigo: 'XCD', descripcion: 'Dólar del Caribe Oriental' },
    { codigo: 'XDR', descripcion: 'SDR (Derechos Especiales de Giro)' },
    { codigo: 'XOF', descripcion: 'Franco CFA BCEAO' },
    { codigo: 'XPF', descripcion: 'Franco CFP' },
    { codigo: 'XSU', descripcion: 'Sucre' },
    { codigo: 'XUA', descripcion: 'Unidad de cuenta del BAD' },
    { codigo: 'YER', descripcion: 'Rial yemení' },
    { codigo: 'ZAR', descripcion: 'Rand' },
    { codigo: 'ZMW', descripcion: 'Kwacha zambiano' },
    { codigo: 'ZWL', descripcion: 'Dólar zimbabuense' }
  ];
}

function monedaPorCodigo(codigo) {
  switch (codigo) {
    case 'AFN':
      return { letrasMonedaSingular: 'Afghani', letrasMonedaPlural: 'Afghanis' };
    case 'MGA':
      return { letrasMonedaSingular: 'Ariary', letrasMonedaPlural: 'Ariary' };
    case 'THB':
      return { letrasMonedaSingular: 'Baht', letrasMonedaPlural: 'Baht' };
    case 'PAB':
      return { letrasMonedaSingular: 'Balboa', letrasMonedaPlural: 'Balboas' };
    case 'ETB':
      return { letrasMonedaSingular: 'Birr', letrasMonedaPlural: 'Birr' };
    case 'VEF':
      return { letrasMonedaSingular: 'Bolívar', letrasMonedaPlural: 'Bolivares' };
    case 'BOB':
      return { letrasMonedaSingular: 'Boliviano', letrasMonedaPlural: 'Bolivianos' };
    case 'CVE':
      return { letrasMonedaSingular: 'Escudo', letrasMonedaPlural: 'Escudos' };
    case 'GHS':
      return { letrasMonedaSingular: 'Cedi', letrasMonedaPlural: 'Cedis' };
    case 'KES':
    case 'SOS':
    case 'TZS':
    case 'UGX':
      return { letrasMonedaSingular: 'Chelín', letrasMonedaPlural: 'Chelines' };
    case 'SVC':
    case 'CRC':
      return { letrasMonedaSingular: 'Colón', letrasMonedaPlural: 'Colones' };
    case 'NIO':
      return { letrasMonedaSingular: 'Córdobas', letrasMonedaPlural: 'Córdobas' };
    case 'CZK':
    case 'DKK':
    case 'ISK':
    case 'NOK':
    case 'SEK':
      return { letrasMonedaSingular: 'Corona', letrasMonedaPlural: 'Coronas' };
    case 'GMD':
      return { letrasMonedaSingular: 'Dalasi', letrasMonedaPlural: 'Dalasis' };
    case 'MKD':
    case 'DZD':
    case 'BHD':
    case 'IQD':
    case 'JOD':
    case 'KWD':
    case 'LYD':
    case 'RSD':
    case 'TND':
      return { letrasMonedaSingular: 'Dinar', letrasMonedaPlural: 'Dinares' };
    case 'AED':
    case 'MAD':
      return { letrasMonedaSingular: 'Dírham', letrasMonedaPlural: 'Dírham' };
    case 'STD':
      return { letrasMonedaSingular: 'Dobra', letrasMonedaPlural: 'Dobras' };
    case 'USD':
    case 'USN':
    case 'AUD':
    case 'CAD':
    case 'BBD':
    case 'BZD':
    case 'BMD':
    case 'BND':
    case 'FJD':
    case 'HKD':
    case 'BSD':
    case 'KYD':
    case 'SBD':
    case 'NAD':
    case 'SGD':
    case 'XCD':
    case 'GYD':
    case 'JMD':
    case 'LRD':
    case 'NZD':
    case 'SRD':
    case 'TTD':
    case 'ZWL':
      return { letrasMonedaSingular: 'Dólar', letrasMonedaPlural: 'Dólares' };
    case 'VND':
      return { letrasMonedaSingular: 'Dong', letrasMonedaPlural: 'Dong' };
    case 'AMD':
      return { letrasMonedaSingular: 'Dram', letrasMonedaPlural: 'Dram' };
    case 'EUR':
    case 'CHE':
      return { letrasMonedaSingular: 'Euro', letrasMonedaPlural: 'Euros' };
    case 'HUF':
    case 'ANG':
    case 'AWG':
      return { letrasMonedaSingular: 'Florín', letrasMonedaPlural: 'Florines' };
    case 'BIF':
    case 'XOF':
    case 'XAF':
    case 'XPF':
    case 'KMF':
    case 'CDF':
    case 'DJF':
    case 'GNF':
    case 'RWF':
    case 'CHF':
    case 'CHW':
      return { letrasMonedaSingular: 'Franco', letrasMonedaPlural: 'Francos' };
    case 'HTG':
      return { letrasMonedaSingular: 'Gourde', letrasMonedaPlural: 'Gourde' };
    case 'PYG':
      return { letrasMonedaSingular: 'Guaraní', letrasMonedaPlural: 'Guaraníes' };
    case 'UAH':
      return { letrasMonedaSingular: 'Hryvnia', letrasMonedaPlural: 'Hryvnia' };
    case 'PGK':
      return { letrasMonedaSingular: 'Kina', letrasMonedaPlural: 'Kinas' };
    case 'LAK':
      return { letrasMonedaSingular: 'Kip', letrasMonedaPlural: 'Kip' };
    case 'HRK':
      return { letrasMonedaSingular: 'Kuna', letrasMonedaPlural: 'Kunas' };
    case 'MWK':
    case 'ZMW':
    case 'AOA':
      return { letrasMonedaSingular: 'Kwanza', letrasMonedaPlural: 'Kwanza' };
    case 'MMK':
      return { letrasMonedaSingular: 'Kyat', letrasMonedaPlural: 'Kyat' };
    case 'GEL':
      return { letrasMonedaSingular: 'Lari', letrasMonedaPlural: 'Lari' };
    case 'ALL':
      return { letrasMonedaSingular: 'Lek', letrasMonedaPlural: 'Lek' };
    case 'HNL':
      return { letrasMonedaSingular: 'Lempira', letrasMonedaPlural: 'Lempira' };
    case 'SLL':
      return { letrasMonedaSingular: 'Leone', letrasMonedaPlural: 'Leone' };
    case 'MDL':
    case 'RON':
      return { letrasMonedaSingular: 'Leu', letrasMonedaPlural: 'Lei' };
    case 'BGN':
      return { letrasMonedaSingular: 'Lev', letrasMonedaPlural: 'Lev' };
    case 'GIP':
    case 'SHP':
    case 'EGP':
    case 'GBP':
    case 'LBP':
    case 'FKP':
    case 'SYP':
    case 'SDG':
    case 'SSP':
      return { letrasMonedaSingular: 'Libra', letrasMonedaPlural: 'Libras' };
    case 'SZL':
      return { letrasMonedaSingular: 'Lilangeni', letrasMonedaPlural: 'Lilangeni' };
    case 'TRY':
      return { letrasMonedaSingular: 'Lira', letrasMonedaPlural: 'Liras' };
    case 'LSL':
      return { letrasMonedaSingular: 'Loti', letrasMonedaPlural: 'Maloti' };
    case 'AZN':
    case 'TMT':
      return { letrasMonedaSingular: 'Manat', letrasMonedaPlural: 'Manat' };
    case 'BAM':
      return { letrasMonedaSingular: 'Marco', letrasMonedaPlural: 'Marcos' };
    case 'MZN':
      return { letrasMonedaSingular: 'Metical', letrasMonedaPlural: 'Meticales' };
    case 'BOV':
      return { letrasMonedaSingular: 'Mvdol', letrasMonedaPlural: 'Mvdol' };
    case 'NGN':
      return { letrasMonedaSingular: 'Naira', letrasMonedaPlural: 'Nairas' };
    case 'ERN':
      return { letrasMonedaSingular: 'Nakfa', letrasMonedaPlural: 'Nafkas' };
    case 'BTN':
      return { letrasMonedaSingular: 'Ngultrum', letrasMonedaPlural: 'Ngultrum' };
    case 'TWD':
      return { letrasMonedaSingular: 'Dólar', letrasMonedaPlural: 'Dólares' };
    case 'ILS':
      return { letrasMonedaSingular: 'Shekel', letrasMonedaPlural: 'Shekels' };
    case 'PEN':
      return { letrasMonedaSingular: 'Sol', letrasMonedaPlural: 'Soles' };
    case 'MRO':
      return { letrasMonedaSingular: 'Ouguiya', letrasMonedaPlural: 'Ouguiya' };
    case 'TOP':
      return { letrasMonedaSingular: 'Pa’anga', letrasMonedaPlural: 'Pa’anga' };
    case 'MOP':
      return { letrasMonedaSingular: 'Pataca', letrasMonedaPlural: 'Patacas' };
    case 'ARS':
    case 'CLP':
    case 'COP':
    case 'CUC':
    case 'CUP':
    case 'DOP':
    case 'PHP':
    case 'MXN':
    case 'UYU':
      return { letrasMonedaSingular: 'Peso', letrasMonedaPlural: 'Pesos' };
    case 'BWP':
      return { letrasMonedaSingular: 'Pula', letrasMonedaPlural: 'Pulas' };
    case 'GTQ':
      return { letrasMonedaSingular: 'Quetzal', letrasMonedaPlural: 'Quetzales' };
    case 'ZAR':
      return { letrasMonedaSingular: 'Rand', letrasMonedaPlural: 'Rand' };
    case 'BRL':
      return { letrasMonedaSingular: 'Real', letrasMonedaPlural: 'Reales' };
    case 'IRR':
    case 'OMR':
    case 'YER':
      return { letrasMonedaSingular: 'Rial', letrasMonedaPlural: 'Riales' };
    case 'KHR':
      return { letrasMonedaSingular: 'Riel', letrasMonedaPlural: 'Riel' };
    case 'MYR':
      return { letrasMonedaSingular: 'Ringgit', letrasMonedaPlural: 'Ringgit' };
    case 'QAR':
    case 'SAR':
      return { letrasMonedaSingular: 'Riyal', letrasMonedaPlural: 'Riyales' };
    case 'BYR':
    case 'RUB':
      return { letrasMonedaSingular: 'Rublo', letrasMonedaPlural: 'Rublos' };
    case 'MVR':
      return { letrasMonedaSingular: 'Rufiyaa', letrasMonedaPlural: 'Rufiyaa' };
    case 'IDR':
    case 'MUR':
    case 'LKR':
    case 'INR':
    case 'NPR':
    case 'PKR':
    case 'SCR':
      return { letrasMonedaSingular: 'Rupia', letrasMonedaPlural: 'Rupias' };
    case 'XDR':
      return {
        letrasMonedaSingular: 'SDR (Derechos Especiales de Giro)',
        letrasMonedaPlural: 'SDR (Derechos Especiales de Giro)'
      };
    case 'KGS':
    case 'UZS':
      return { letrasMonedaSingular: 'Som', letrasMonedaPlural: 'Som' };
    case 'TJS':
      return { letrasMonedaSingular: 'Somoni', letrasMonedaPlural: 'Somonis' };
    case 'XSU':
      return { letrasMonedaSingular: 'Sucre', letrasMonedaPlural: 'Sucre' };
    case 'BDT':
      return { letrasMonedaSingular: 'Taka', letrasMonedaPlural: 'Taka' };
    case 'WST':
      return { letrasMonedaSingular: 'Tala', letrasMonedaPlural: 'Tala' };
    case 'KZT':
      return { letrasMonedaSingular: 'Tenge', letrasMonedaPlural: 'Tenge' };
    case 'MNT':
      return { letrasMonedaSingular: 'Tugrik', letrasMonedaPlural: 'Tugrik' };
    case 'VUV':
      return { letrasMonedaSingular: 'Vatu', letrasMonedaPlural: 'Vatus' };
    case 'KRW':
    case 'KPW':
      return { letrasMonedaSingular: 'Won', letrasMonedaPlural: 'Won' };
    case 'JPY':
      return { letrasMonedaSingular: 'Yen', letrasMonedaPlural: 'Yenes' };
    case 'CNY':
      return { letrasMonedaSingular: 'Yuan', letrasMonedaPlural: 'Yuan' };
    case 'PLN':
      return { letrasMonedaSingular: 'Zloty', letrasMonedaPlural: 'Zloty' };
  }
  return { letrasMonedaSingular: '', letrasMonedaPlural: '' };
}

function Unidades(num) {
  switch (num) {
    case 1:
      return 'UN';
    case 2:
      return 'DOS';
    case 3:
      return 'TRES';
    case 4:
      return 'CUATRO';
    case 5:
      return 'CINCO';
    case 6:
      return 'SEIS';
    case 7:
      return 'SIETE';
    case 8:
      return 'OCHO';
    case 9:
      return 'NUEVE';
  }

  return '';
} //Unidades()

function Decenas(num) {
  decena = Math.floor(num / 10);
  unidad = num - decena * 10;

  switch (decena) {
    case 1:
      switch (unidad) {
        case 0:
          return 'DIEZ';
        case 1:
          return 'ONCE';
        case 2:
          return 'DOCE';
        case 3:
          return 'TRECE';
        case 4:
          return 'CATORCE';
        case 5:
          return 'QUINCE';
        default:
          return 'DIECI' + Unidades(unidad);
      }
    case 2:
      switch (unidad) {
        case 0:
          return 'VEINTE';
        default:
          return 'VEINTI' + Unidades(unidad);
      }
    case 3:
      return DecenasY('TREINTA', unidad);
    case 4:
      return DecenasY('CUARENTA', unidad);
    case 5:
      return DecenasY('CINCUENTA', unidad);
    case 6:
      return DecenasY('SESENTA', unidad);
    case 7:
      return DecenasY('SETENTA', unidad);
    case 8:
      return DecenasY('OCHENTA', unidad);
    case 9:
      return DecenasY('NOVENTA', unidad);
    case 0:
      return Unidades(unidad);
  }
} //Unidades()

function DecenasY(strSin, numUnidades) {
  if (numUnidades > 0) return strSin + ' Y ' + Unidades(numUnidades);

  return strSin;
} //DecenasY()

function Centenas(num) {
  centenas = Math.floor(num / 100);
  decenas = num - centenas * 100;

  switch (centenas) {
    case 1:
      if (decenas > 0) return 'CIENTO ' + Decenas(decenas);
      return 'CIEN';
    case 2:
      return 'DOSCIENTOS ' + Decenas(decenas);
    case 3:
      return 'TRESCIENTOS ' + Decenas(decenas);
    case 4:
      return 'CUATROCIENTOS ' + Decenas(decenas);
    case 5:
      return 'QUINIENTOS ' + Decenas(decenas);
    case 6:
      return 'SEISCIENTOS ' + Decenas(decenas);
    case 7:
      return 'SETECIENTOS ' + Decenas(decenas);
    case 8:
      return 'OCHOCIENTOS ' + Decenas(decenas);
    case 9:
      return 'NOVECIENTOS ' + Decenas(decenas);
  }

  return Decenas(decenas);
} //Centenas()

function Seccion(num, divisor, strSingular, strPlural) {
  cientos = Math.floor(num / divisor);
  resto = num - cientos * divisor;

  letras = '';

  if (cientos > 0)
    if (cientos > 1) letras = Centenas(cientos) + ' ' + strPlural;
    else letras = strSingular;

  if (resto > 0) letras += '';

  return letras;
} //Seccion()

function Miles(num) {
  divisor = 1000;
  cientos = Math.floor(num / divisor);
  resto = num - cientos * divisor;

  strMiles = Seccion(num, divisor, 'UN MIL', 'MIL');
  strCentenas = Centenas(resto);

  if (strMiles == '') return strCentenas;

  return strMiles + ' ' + strCentenas;
} //Miles()

function Millones(num) {
  divisor = 1000000;
  cientos = Math.floor(num / divisor);
  resto = num - cientos * divisor;

  strMillones = Seccion(num, divisor, 'UN MILLON ', 'MILLONES ');
  strMiles = Miles(resto);

  if (strMillones == '') return strMiles;

  return strMillones + ' ' + strMiles;
} //Millones()

function MilMillones(num) {
  divisor = 1000000000;
  cientos = Math.floor(num / divisor);
  resto = num - cientos * divisor;

  strMilMillones = Seccion(num, divisor, 'UN MIL ', 'MIL ');
  strMillones = Millones(resto);

  if (strMilMillones == '') return strMillones;

  return strMilMillones + ' ' + strMillones;
} //MilMillones()

function NumeroALetras(num, codigo) {
  var monedas = monedaPorCodigo(codigo);
  var data = {
    numero: num,
    enteros: Math.floor(num),
    centavos: Math.round(num * 100) - Math.floor(num) * 100,
    letrasCentavos: '',
    letrasMonedaPlural: monedas.letrasMonedaPlural.toUpperCase(), //"PESOS", 'Dólares', 'Bolívares', 'etcs'
    letrasMonedaSingular: monedas.letrasMonedaSingular.toUpperCase(), //"PESO", 'Dólar', 'Bolivar', 'etc'

    letrasMonedaCentavoPlural: 'CENTAVOS',
    letrasMonedaCentavoSingular: 'CENTAVO'
  };

  if (data.centavos > 0) {
    data.letrasCentavos =
      'CON ' +
      (function() {
        if (data.centavos == 1) {
          return MilMillones(data.centavos) + ' ' + data.letrasMonedaCentavoSingular;
        } else {
          return MilMillones(data.centavos) + ' ' + data.letrasMonedaCentavoPlural;
        }
      })();
  }

  if (data.enteros == 0) return 'CERO ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
  if (data.enteros == 1) {
    return MilMillones(data.enteros) + ' ' + data.letrasMonedaSingular + ' ' + data.letrasCentavos;
  } else {
    return MilMillones(data.enteros) + ' ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
  }
} //NumeroALetras()
