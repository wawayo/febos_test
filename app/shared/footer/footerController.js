angular
        .module('febosApp')
        .controller('footerCtrl', [
            '$timeout',
            '$scope',
            '$window',
            '$state',
            '$rootScope',
            '$location',
            'FebosUtil',
            function ($timeout, $scope, $window, $state, $rootScope, $location,FebosUtil) {
                
                $scope.ambiente=FebosUtil.obtenerAmbiente();
                $scope.ambito=FebosUtil.obtenerAmbito();

            }
        ]);