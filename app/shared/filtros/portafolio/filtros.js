angular
        .module('febosApp')
        .directive('filtrosPortafolio', ['$rootScope', '$document', '$state', '$timeout', '$location', 'ComplementoDnt', '$stateParams', 'SesionFebos', 'VistasDnt',
            function ($rootScope, $document, $state, $timeout, $location, ComplementoDnt, $stateParams, SesionFebos, VistasDnt) {
                return {
                    restrict: 'E',
                    templateUrl: 'app/shared/filtros/portafolio/filtros.html',
                    controller: function ($scope) {

                    },
                    link: function (scope, elem, attr) {
                        //console.log("Documentos disponibles para la vista", $rootScope.vistaDTE.documentosDisponibles);
                        //console.log("H",$location.search($rootScope.locationSearch));
                        //console.log("H2",$rootScope.locationSearch);  
                        
                        //los campos bloqueados son los que tienen filtros fijos
                        scope.campoBloqueado=function(campo){
                            var fijos=VistasDnt[$stateParams.app][$stateParams.categoria][$stateParams.vista].filtrosFijos;
                            for(var i=0;i<fijos.length;i++){
                                if(campo == fijos[i].campo){
                                    return true;
                                }
                            }
                            return false;
                        }
                        var ordenes = $location.search().orden.split(",");
                        var query = ComplementoDnt.convertirFiltrosEnObjeto($location.search().filtros);
                        if (typeof SesionFebos().filtrosDnt == 'undefined') {
                            SesionFebos().filtrosDnt = {
                                emitidos: [],
                                recibidos: []
                            };
                        }
                        scope.filtrosGuardados = SesionFebos().filtrosDnt[$stateParams.categoria];
                        scope.hayFiltroaGuardados = scope.filtrosGuardados.length > 0;
                        scope.activo = '';
                        scope.selectize_config = {
                            maxItems: null,
                            valueField: 'valor',
                            labelField: 'nombre',
                            searchField: 'nombre',
                            create: true,
                            dropdownParent: "body",
                            render: {
                                option: function (data, escape) {
                                    return  '<div class="option">' +
                                            '<span class="title">' + escape(data.nombre) + '</span>' +
                                            '</div>';
                                },
                                item: function (data, escape) {
                                    return '<div class="item">' + escape(data.nombre) + '</div>';
                                },
                                option_create: function (data, escape) {
                                    return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
                                }

                            }
                        };
                        
                        scope.seleccionManual = function (evt) {
                            if (evt == 'manual') {
                                //buscar la fecha que se esta editando
                                for (var i = 0; i < scope.filtros.length; i++) {
                                    if (scope.filtros[i].configurando == true) {
                                        console.log("este es el campo que se esta configurando: ", scope.filtros[i]);
                                        scope.filtros[i].manual = true;
                                    }
                                }
                            }

                        }
                        scope.selectize_fecha_config = {
                            maxItems: 1,
                            valueField: 'valor',
                            labelField: 'nombre',
                            searchField: 'nombre',
                            create: false,
                            dropdownParent: "body",
                            onChange: scope.seleccionManual,
                            render: {
                                option: function (data, escape) {
                                    return  '<div class="option">' +
                                            '<span class="title">' + escape(data.nombre) + '</span>' +
                                            '</div>';
                                },
                                item: function (data, escape) {
                                    return '<div class="item">' + escape(data.nombre) + '</div>';
                                },
                                option_create: function (data, escape) {
                                    return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
                                }

                            }
                        };

                        var fechasSimples = [
                            {"nombre": "Hoy", "valor": "[hoy"},
                            {"nombre": "Ayer y hoy", "valor": "[ayerYhoy]"},
                            {"nombre": "Esta semana", "valor": "[estaSemana]"},
                            {"nombre": "Las últimas 2 semanas", "valor": "[las2UltimasSemanas]"},
                            {"nombre": "Las últimas 3 semanas", "valor": "[las3UltimasSemanas]"},
                            {"nombre": "Los últimos 5 días", "valor": "[losUltimos5Dias]"},
                            {"nombre": "Los últimos 10 días", "valor": "[losUltimos10Dias]"},
                            {"nombre": "Los últimos 15 días", "valor": "[losUltimos15Dias]"},
                            {"nombre": "Los últimos 20 días", "valor": "[losUltimos20Dias]"},
                            {"nombre": "Este mes", "valor": "[esteMes]"},
                            {"nombre": "El mes pasado", "valor": "[elMesPasado]"},
                            {"nombre": "El mes antes pasado", "valor": "[elMesAntesPasado]"},
                            {"nombre": "Los últimos 2 meses", "valor": "[losUltimos2Meses]"},
                            {"nombre": "Los últimos 3 meses", "valor": "[losUltimos3Meses]"},
                            {"nombre": "Los últimos 4 meses", "valor": "[losUltimos4Meses]"},
                            {"nombre": "Los últimos 5 meses", "valor": "[losUltimos5Meses]"},
                            {"nombre": "Los últimos 6 meses", "valor": "[losUltimos6Meses]"},
                        ]
                        scope.convertirFechaEnProsa = function (valor) {
                            for (var i = 0; i < fechasSimples.length; i++) {
                                if (fechasSimples[i].valor == valor)
                                    return fechasSimples[i].nombre;
                            }
                            return "Fecha inválida";
                        }
                        scope.tiposDeDocumentos = [
                            {"valor": "801", "nombre": "Orden de Compra"},
                            {"valor": "MP", "nombre": "OC de Mercado Público"},
                            {"valor": "HES", "nombre": "HES"},
                            {"valor": "HEM", "nombre": "HEM/MIGO"}
                        ]
                        
                        scope.posiblesCampos = [
                            { "campo": "tipo", "nombre": "Tipo de documento", tipoFiltro: "tag", opciones: [] },
                            { "campo": "numero", "nombre": "Folio", tipoFiltro: "tag" },
                            { "campo": "emisorRut", "nombre": "Rut Emisor", tipoFiltro: "tag" },
                            { "campo": "emisorRazonSocial", "nombre": "Razón Social Emisor", tipoFiltro: "texto" },
                            { "campo": "receptorRut", "nombre": "Rut Receptor", tipoFiltro: "tag" },
                            { "campo": "receptorRazonSocial", "nombre": "Razón Social Receptor", tipoFiltro: "texto" },
                            { "campo": "fechaEmision", "nombre": "Fecha de Emisión", tipoFiltro: "fecha" ,opciones:fechasSimples},
                            { "campo": "fechaActualizacion", "nombre": "Fecha de Actualización", tipoFiltro: "fecha",opciones:fechasSimples },
                            { "campo": "compradorNombre", "nombre": "Nombre Comprador", tipoFiltro: "texto" },
                            { "campo": "compradorArea", "nombre": "Área Comprador", tipoFiltro: "texto" },
                            { "campo": "leido", "nombre": "Leído", tipoFiltro: "texto" }
                        ];


                        scope.agregarDocumentosPermitidos = function () {
                            for (var i = 0; i < scope.posiblesCampos.length; i++) {
                                if (scope.posiblesCampos[i].campo == 'tipoDocumento') {
                                    //array.splice(index, 1);
                                    for (var x = 0; x < scope.tiposDeDocumentos.length; x++) {
                                        if (VistasDnt[$stateParams.app][$stateParams.categoria][$stateParams.vista].documentosDisponibles.indexOf(parseInt(scope.tiposDeDocumentos[x].valor)) >= 0) {
                                            scope.posiblesCampos[i].opciones.push(scope.tiposDeDocumentos[x]);
                                        }
                                    }
                                }
                            }
                        }
                        scope.agregarDocumentosPermitidos();



                        scope.obtenerNombreCampo = function (campo) {
                            for (var i = 0; i < scope.posiblesCampos.length; i++) {
                                if (scope.posiblesCampos[i].campo == campo) {
                                    return scope.posiblesCampos[i];
                                }
                            }
                            return "";
                        };



                        scope.filtros = [];
                        scope.ordenes = [];
                        scope.toggleConfigFiltros = function ($event, quien) {
                            $event.preventDefault();
                            $('.uk-tooltip').hide();
                            if (scope.activo == quien || !$rootScope.configFiltrosActivo) {
                                $rootScope.configFiltrosActivo = !$rootScope.configFiltrosActivo;
                            }
                            if (!$rootScope.configFiltrosActivo) {
                                scope.activo = '';
                            } else {
                                scope.activo = quien;
                            }
                        };
                        scope.esconderFiltros = function () {
                            $rootScope.configFiltrosActivo = false;
                        }
                        scope.yaSeEstaFiltrando = function (campo) {
                            for (var i = 0; i < scope.filtros.length; i++) {
                                if (scope.filtros[i].campo == campo.campo) {
                                    return true;
                                }
                            }
                            return false;
                        }
                        
                        scope.yaSeEstaOrdenado = function (campo) {
                            for (var i = 0; i < scope.ordenes.length; i++) {
                                if (scope.ordenes[i].campo == campo.campo) {
                                    return true;
                                }
                            }
                            return false;
                        }
                        scope.agregarFiltro = function (campo) {
                            if (scope.yaSeEstaFiltrando(campo)) {
                                console.log("Ya se esta filtrnado por ", campo);
                            } else {
                                var filtro = {
                                    'nombre': campo.nombre,
                                    'campo': campo.campo,
                                    'valor': "",
                                    'tipoFiltro': campo.tipoFiltro,
                                    'configurando': true,
                                    'manual': true

                                }
                                if (typeof campo.opciones != 'undefined') {
                                    filtro.opciones = campo.opciones;
                                } else {
                                    filtro.opciones = [];
                                }
                                if (filtro.tipoFiltro == 'fecha') {
                                    filtro.valor = {};
                                    filtro.valor.desde = "";
                                    filtro.valor.hasta = "";
                                    filtro.valor.texto = "losUltimos15Dias";
                                    filtro.manual = false;
                                }
                                scope.filtros.push(filtro);
                            }
                        }
                        scope.agregarOrden = function (campo) {
                            var tipo = '-';
                            var orden = {
                                'nombre': campo.nombre,
                                'campo': campo.campo,
                                'orden': tipo
                            }
                            scope.ordenes.push(orden);
                        }
                        scope.eliminarFiltro = function (filtro) {
                            var posicion = -1;
                            for (var i = 0; i < scope.filtros.length; i++) {
                                if (scope.filtros[i].campo == filtro.campo) {
                                    posicion = i;
                                    break;
                                }
                            }
                            if (posicion >= 0) {
                                scope.filtros.splice(posicion, 1);
                            }
                        }
                        scope.eliminarOrden = function (orden) {
                            var posicion = -1;
                            for (var i = 0; i < scope.ordenes.length; i++) {
                                if (scope.ordenes[i].campo == orden.campo) {
                                    posicion = i;
                                    break;
                                }
                            }
                            if (posicion >= 0) {
                                scope.ordenes.splice(posicion, 1);
                            }
                        }
                        scope.configurar = function (filtro) {
                            for (var i = 0; i < scope.filtros.length; i++) {
                                if (typeof filtro != 'undefined' && scope.filtros[i].campo == filtro.campo) {
                                    scope.filtros[i].configurando = true;
                                    if (scope.filtros[i].tipoFiltro == 'fecha') {
                                        if (typeof scope.filtros[i].valor == 'string') {
                                            scope.filtros[i].valor = {};
                                            scope.filtros[i].valor.desde = "";
                                            scope.filtros[i].valor.hasta = "";
                                            scope.filtros[i].valor.texto = "losUltimos15Dias";
                                            scope.filtros[i].manual = false;
                                        }
                                    }
                                } else {
                                    scope.filtros[i].configurando = false;
                                    //scope.filtros[i].manual = false;
                                }
                            }

                        }
                        
                        scope.toggleOrden = function (orden) {
                            orden.orden = orden.orden == '-' ? '+' : '-';
                        }
                        scope.generarQueryParams = function () {
                            var filtros = [];
                            for (var i = 0; i < scope.filtros.length; i++) {
                                if (scope.filtros[i].tipoFiltro == 'fecha') {
                                    if (scope.filtros[i].manual) {
                                        filtros.push(scope.filtros[i].campo + ":" + scope.filtros[i].valor.desde + "--" + scope.filtros[i].valor.hasta);
                                    } else {
                                        filtros.push(scope.filtros[i].campo + ":" + scope.filtros[i].valor.texto);
                                    }
                                } else {
                                    filtros.push(scope.filtros[i].campo + ":" + scope.filtros[i].valor);
                                }
                            }

                            var ordenes = [];
                            for (var i = 0; i < scope.ordenes.length; i++) {
                                ordenes.push(scope.ordenes[i].orden + scope.ordenes[i].campo);
                            }
                            var queryParams = {
                                "pagina": $location.search().pagina,
                                "itemsPorPagina": $location.search().itemsPorPagina,
                                "orden": ordenes.join(","),
                                "filtros": filtros.join("|")
                            }
                            return queryParams;
                        }
                        scope.aplicar = function () {
                            var queryParams = scope.generarQueryParams();
                            $location.search(queryParams);
                            $state.go('restringido.dnt',{
                                'app':$stateParams.app,
                                'categoria':$stateParams.categoria,
                                'vista':$stateParams.vista,
                                'filtros':queryParams.filtros,
                                'orden':queryParams.orden,
                                'pagina':1,
                                'itemsPorPagina':$stateParams.itemsPorPagina
                            });
                            $state.reload();
                        }

                        scope.nombreFiltroPredefinido = "";
                        scope.guardar = function () {
                            var queryParams = scope.generarQueryParams();
                            UIkit.modal.prompt("Con que nombre quieres guardar esta vista?", scope.nombreFiltroPredefinido, function (nombre) {
                                console.log("guardando con el nombre:", nombre);
                                SesionFebos().filtrosDnt[$stateParams.categoria].push({
                                    'nombre': nombre,
                                    'params': queryParams
                                });
                                UIkit.notify("Se ha guardado la vista '" + nombre + "' <a class='notify-action'>[X]</a> ", {"status": "success", timeout: 1500});
                            }, {labels: {'Ok': 'Guardar', 'Cancel': 'Mejor no, me arrepentí'}});
                        }
                        scope.seleccionarVista = function (filtro) {
                            console.log("VISTA SELECCIONADA", filtro);
                            UIkit.notify("Cargando la vista '" + filtro.nombre + "' <a class='notify-action'>[X]</a> ", {"status": "success", timeout: 1500});
                            $location.search(filtro.params);
                            $state.go('restringido.dnt',{
                                'app':$stateParams.app,
                                'categoria':$stateParams.categoria,
                                'vista':$stateParams.vista,
                                'filtros':filtro.params.filtros,
                                'orden':filtro.params.orden,
                                'pagina':1,
                                'itemsPorPagina':$stateParams.itemsPorPagina
                            });
                            $state.reload();

                        }
                        scope.eliminarFiltroGuardado = function (filtro) {
                            var indiceParaEliminar = -1;
                            for (var i = 0; i <= SesionFebos().filtrosDnt[$stateParams.categoria].length; i++) {
                                if (SesionFebos().filtrosDnt[$stateParams.categoria][i].nombre == filtro.nombre) {
                                    indiceParaEliminar = i;
                                    break;
                                }
                            }
                            if (indiceParaEliminar >= 0) {
                                UIkit.modal.confirm("Estas a punto de eliminar la vista '" + filtro.nombre + "', confirmas?", function () {
                                    SesionFebos().filtrosDnt[$stateParams.categoria].splice(indiceParaEliminar, 1);
                                    UIkit.notify("Se ha eliminado la vista '" + filtro.nombre + "' <a class='notify-action'>[X]</a> ", {"status": "success", timeout: 1500});
                                    $rootScope.$broadcast('refrescarTituloDnt');
                                }, function () {}, {labels: {'Ok': 'Si, eliminar', 'Cancel': 'Mejor no, me arrepentí'}});
                            }
                        }
                        
                        // MINI SIDEBAR
                        scope.mini_sidebar_toggle = false;
                        $rootScope.miniSidebarActive = false;

                        // change input's state to checked if mini sidebar is active
                        if ((localStorage.getItem("altair_sidebar_mini") !== null && localStorage.getItem("altair_sidebar_mini") == '1')) {
                            $rootScope.miniSidebarActive = true;
                            scope.mini_sidebar_toggle = true;
                        }

                        // toggle mini sidebar
                        $('#style_sidebar_mini')
                                .on('ifChecked', function (event) {
                                    localStorage.setItem("altair_sidebar_mini", '1');
                                    localStorage.removeItem('altair_sidebar_slim');
                                    $rootScope.configFiltrosActivo = false;
                                    $timeout(function () {
                                        $rootScope.miniSidebarActive = true;
                                        $state.go($state.current, {}, {reload: true});
                                    })
                                })
                                .on('ifUnchecked', function (event) {
                                    localStorage.removeItem('altair_sidebar_mini');
                                    $rootScope.configFiltrosActivo = false;
                                    $timeout(function () {
                                        $rootScope.miniSidebarActive = false;
                                        $state.go($state.current, {}, {reload: true});
                                    })
                                });

                        // SLIM SIDEBAR
                        scope.slim_sidebar_toggle = false;
                        $rootScope.slimSidebarActive = false;

                        // change input's state to checked if mini sidebar is active
                        if ((localStorage.getItem("altair_sidebar_slim") !== null && localStorage.getItem("altair_sidebar_slim") == '1')) {
                            $rootScope.slimSidebarActive = true;
                            scope.slim_sidebar_toggle = true;
                        }

                        // toggle mini sidebar
                        $('#style_sidebar_slim')
                                .on('ifChecked', function (event) {
                                    localStorage.removeItem('altair_sidebar_mini');
                                    localStorage.setItem("altair_sidebar_slim", '1');
                                    $rootScope.configFiltrosActivo = false;
                                    $timeout(function () {
                                        $rootScope.slimSidebarActive = true;
                                        $state.go($state.current, {}, {reload: true});
                                    })
                                })
                                .on('ifUnchecked', function (event) {
                                    localStorage.removeItem('altair_sidebar_slim');
                                    $rootScope.configFiltrosActivo = false;
                                    $timeout(function () {
                                        $rootScope.slimSidebarActive = false;
                                        $state.go($state.current, {}, {reload: true});
                                    })
                                });

                        // BOXED LAYOUT
                        scope.boxed_layout_toggle = false;

                        // change input's state to checked if boxed layout is active
                        if ((localStorage.getItem("altair_boxed_layout") !== null && localStorage.getItem("altair_boxed_layout") == '1')) {
                            $rootScope.boxedLayoutActive = true;
                            scope.boxed_layout_toggle = true;
                        }
                    
                        // toggle mini sidebar
                        $('#style_layout_boxed')
                                .on('ifChecked', function (event) {
                                    localStorage.setItem("altair_boxed_layout", '1');
                                    $rootScope.configFiltrosActivo = false;
                                    $timeout(function () {
                                        $rootScope.boxedLayoutActive = true;
                                        $state.go($state.current, {}, {reload: true});
                                    })
                                })
                                .on('ifUnchecked', function (event) {
                                    localStorage.removeItem('altair_boxed_layout');
                                    $rootScope.configFiltrosActivo = false;
                                    $timeout(function () {
                                        $rootScope.boxedLayoutActive = false;
                                        $state.go($state.current, {}, {reload: true});
                                    })
                                });

                        // main menu accordion mode
                        var $accordion_mode_toggle = $('#accordion_mode_main_menu');

                        if ($rootScope.menuAccordionMode) {
                            $accordion_mode_toggle.iCheck('check');
                        }

                        $accordion_mode_toggle
                                .on('ifChecked', function () {
                                    $rootScope.menuAccordionMode = true;
                                })
                                .on('ifUnchecked', function () {
                                    $rootScope.menuAccordionMode = false;
                                });


                        // check which theme is active
                        if (localStorage.getItem("altair_theme") !== null) {
                            $rootScope.main_theme = localStorage.getItem("altair_theme");
                        } else {
                            $rootScope.main_theme = "default_theme"
                        }

                        
                        //aciones a realizar una vez todo listo
                        for (var param in query) {
                            var campo = scope.obtenerNombreCampo(param);
                            campo.configurando = false;
                            var filtro = {
                                'nombre': campo.nombre,
                                'campo': campo.campo,
                                'tipoFiltro': campo.tipoFiltro,
                                'configurando': false,
                                'manual': false

                            }
                            if (filtro.tipoFiltro == 'fecha') {
                                filtro.valor = {};
                                if (query[param].substring(0, 1) == '[') {
                                    filtro.valor.desde = "";
                                    filtro.valor.hasta = "";
                                    filtro.valor.texto = query[param];
                                    filtro.manual = false;
                                } else {
                                    filtro.valor.desde = query[param].split("--")[0];
                                    filtro.valor.hasta = query[param].split("--")[1];
                                    filtro.valor.texto = "";
                                    filtro.manual = true;
                                }
                            } else {
                                try{
                                    filtro.valor = (query[param].includes(",")) ? query[param].split(",") : query[param];
                                }catch(e){
                                    filtro.valor=query[param];
                                }
                            }
                            if (typeof campo.opciones != 'undefined') {
                                filtro.opciones = campo.opciones;
                            } else {
                                filtro.opciones = [];
                            }
                            scope.filtros.push(filtro);
                        }
                        for (var i = 0; i < ordenes.length; i++) {
                            var orden = ordenes[i];
                            var campo = scope.obtenerNombreCampo(orden.substring(1));
                            var tipo = orden.substring(0, 1);
                            var orden = {
                                'nombre': campo.nombre,
                                'campo': campo.campo,
                                'orden': tipo

                            }
                            scope.ordenes.push(orden);
                        }
                        
//                        console.log("filtros aplicados; ",scope.filtros);

                    }
                };
            }])
        ;
