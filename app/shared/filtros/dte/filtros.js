angular.module('febosApp').directive('filtrosDte', [
  '$rootScope',
  '$document',
  '$state',
  '$timeout',
  '$location',
  'ComplementoDte',
  '$stateParams',
  'SesionFebos',
  'VistasDte',
  function(
    $rootScope,
    $document,
    $state,
    $timeout,
    $location,
    ComplementoDte,
    $stateParams,
    SesionFebos,
    VistasDte
  ) {
    return {
      restrict: 'E',
      templateUrl: 'app/shared/filtros/dte/filtros.html',
      controller: function($scope) {},
      link: function(scope, elem, attr) {
        //console.log("Documentos disponibles para la vista", $rootScope.vistaDTE.documentosDisponibles);
        //console.log("H",$location.search($rootScope.locationSearch));
        //console.log("H2",$rootScope.locationSearch);

        //los campos bloqueados son los que tienen filtros fijos
        scope.campoBloqueado = function(campo) {
          var fijos =
            VistasDte[$stateParams.app][$stateParams.categoria][$stateParams.vista].filtrosFijos;
          for (var i = 0; i < fijos.length; i++) {
            if (campo == fijos[i].campo) {
              return true;
            }
          }
          return false;
        };
        var ordenes = $location.search().orden.split(',');
        var query = ComplementoDte.convertirFiltrosEnObjeto($location.search().filtros);
        if (typeof SesionFebos().filtrosDte == 'undefined') {
          SesionFebos().filtrosDte = {
            emitidos: [],
            recibidos: []
          };
        }
        scope.filtrosGuardados = SesionFebos().filtrosDte[$stateParams.categoria];
        scope.hayFiltroaGuardados = scope.filtrosGuardados.length > 0;
        scope.activo = '';
        scope.selectize_config = {
          maxItems: null,
          valueField: 'valor',
          labelField: 'nombre',
          searchField: 'nombre',
          create: true,
          dropdownParent: 'body',
          render: {
            option: function(data, escape) {
              return (
                '<div class="option">' +
                '<span class="title">' +
                escape(data.nombre) +
                '</span>' +
                '</div>'
              );
            },
            item: function(data, escape) {
              return '<div class="item">' + escape(data.nombre) + '</div>';
            },
            option_create: function(data, escape) {
              return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
            }
          }
        };

        scope.seleccionManual = function(evt) {
          if (evt == 'manual') {
            //buscar la fecha que se esta editando
            for (var i = 0; i < scope.filtros.length; i++) {
              if (scope.filtros[i].configurando == true) {
                console.log('este es el campo que se esta configurando: ', scope.filtros[i]);
                scope.filtros[i].manual = true;
              }
            }
          }
        };
        scope.selectize_fecha_config = {
          maxItems: 1,
          valueField: 'valor',
          labelField: 'nombre',
          searchField: 'nombre',
          create: false,
          dropdownParent: 'body',
          onChange: scope.seleccionManual,
          render: {
            option: function(data, escape) {
              return (
                '<div class="option">' +
                '<span class="title">' +
                escape(data.nombre) +
                '</span>' +
                '</div>'
              );
            },
            item: function(data, escape) {
              return '<div class="item">' + escape(data.nombre) + '</div>';
            },
            option_create: function(data, escape) {
              return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
            }
          }
        };

        var fechasSimples = [
          { nombre: 'Sin Fecha', valor: 'NULL' },
          { nombre: 'Hoy', valor: '[hoy' },
          { nombre: 'Ayer y hoy', valor: '[ayerYhoy]' },
          { nombre: 'Esta semana', valor: '[estaSemana]' },
          { nombre: 'Las últimas 2 semanas', valor: '[las2UltimasSemanas]' },
          { nombre: 'Las últimas 3 semanas', valor: '[las3UltimasSemanas]' },
          { nombre: 'Los últimos 5 días', valor: '[losUltimos5Dias]' },
          { nombre: 'Los últimos 10 días', valor: '[losUltimos10Dias]' },
          { nombre: 'Los últimos 15 días', valor: '[losUltimos15Dias]' },
          { nombre: 'Los últimos 20 días', valor: '[losUltimos20Dias]' },
          { nombre: 'Los últimos 30 días', valor: '[losUltimos30Dias]' },
          { nombre: 'Este mes', valor: '[esteMes]' },
          { nombre: 'El mes pasado', valor: '[elMesPasado]' },
          { nombre: 'El mes antes pasado', valor: '[elMesAntesPasado]' },
          { nombre: 'Los últimos 2 meses', valor: '[losUltimos2Meses]' },
          { nombre: 'Los últimos 3 meses', valor: '[losUltimos3Meses]' },
          { nombre: 'Los últimos 4 meses', valor: '[losUltimos4Meses]' },
          { nombre: 'Los últimos 5 meses', valor: '[losUltimos5Meses]' },
          { nombre: 'Los últimos 6 meses', valor: '[losUltimos6Meses]' }
        ];
        scope.convertirFechaEnProsa = function(valor) {
          for (var i = 0; i < fechasSimples.length; i++) {
            if (fechasSimples[i].valor == valor) return fechasSimples[i].nombre;
          }
          return 'Fecha inválida';
        };
        scope.tiposDeDocumentos = [
          { valor: '33', nombre: 'Factura (E) Afecta' },
          { valor: '34', nombre: 'Factura (E) Exenta' },
          { valor: '39', nombre: 'Boleta (E)' },
          { valor: '41', nombre: 'Boleta (E) Exenta' },
          { valor: '43', nombre: 'Liquidación de Factura (E)' },
          { valor: '46', nombre: 'Factura de Compra (E)' },
          { valor: '52', nombre: 'Guía de Despacho (E)' },
          { valor: '56', nombre: 'Nota de Débito (E)' },
          { valor: '61', nombre: 'Nota de Crédito (E)' },
          { valor: '66', nombre: 'Boleta (E) de honorarios' },
          { valor: '110', nombre: 'Factura de Exportación (E)' },
          { valor: '111', nombre: 'Nota de Débito de Exp. (E)' },
          { valor: '112', nombre: 'Nota de Crédito de Exp. (E)' }
        ];
        scope.filtroCategoriaDte = function(item) {
          if (typeof item['categoria'] == 'undefined') {
            return true;
          }
          if (item['categoria'].indexOf(scope.categoria) >= 0) {
            return true;
          }
          return false;
        };
        scope.posiblesCampos = [
          { campo: 'tipoDocumento', nombre: 'Tipo de documento', tipoFiltro: 'tag', opciones: [] },
          { campo: 'folio', nombre: 'Folio', tipoFiltro: 'tag' },
          { campo: 'rutEmisor', nombre: 'Rut Emisor', tipoFiltro: 'tag' },
          { campo: 'razonSocialEmisor', nombre: 'Razón Social Emisor', tipoFiltro: 'texto' },
          { campo: 'rutReceptor', nombre: 'Rut Receptor', tipoFiltro: 'tag' },
          { campo: 'razonSocialReceptor', nombre: 'Razón Social Receptor', tipoFiltro: 'texto' },
          { campo: 'rutCesionario', nombre: 'Rut Cesionario', tipoFiltro: 'tag' },
          {
            campo: 'razonSocialCesionario',
            nombre: 'Razón Social Cesionario',
            tipoFiltro: 'texto'
          },
          {
            campo: 'fechaCesion',
            nombre: 'Fecha de Cesión',
            tipoFiltro: 'fecha',
            opciones: fechasSimples
          },
          {
            campo: 'codigoSii',
            nombre: 'Codigo SII',
            tipoFiltro: 'tag',
            opciones: [
              { nombre: 'Folio anulado', valor: 'FAN' },
              { nombre: 'TMD', valor: 'Texto modificado con ND' },
              { nombre: 'TMC', valor: 'Texto modificado con NC' },
              { nombre: 'MMD', valor: 'Monto modificado con ND' },
              { nombre: 'MMC', valor: 'Monto modificado con ND' },
              { nombre: 'AND', valor: 'Anulado con ND' },
              { nombre: 'ANC', valor: 'Anulado con ND' }
            ]
          },
          {
            campo: 'fechaEmision',
            nombre: 'Fecha de Emisión',
            tipoFiltro: 'fecha',
            opciones: fechasSimples
          },
          {
            campo: 'fechaCreacion',
            nombre: 'Fecha Creación Febos',
            tipoFiltro: 'fecha',
            opciones: fechasSimples
          },
          {
            campo: 'fechaRecepcion',
            nombre: 'Fecha Recepción Febos',
            tipoFiltro: 'fecha',
            opciones: fechasSimples
          },
          {
            campo: 'fechaRecepcionSii',
            nombre: 'Fecha Recepción SII',
            tipoFiltro: 'fecha',
            opciones: fechasSimples
          },
          {
            campo: 'estadoComercial',
            nombre: 'Estado Comercial',
            tipoFiltro: 'tag',
            opciones: [
              { nombre: 'Sin Acción', valor: '0' },
              { nombre: 'Aceptado', valor: '1' },
              { nombre: 'Aceptado SII', valor: '2' },
              { nombre: 'Pre rechazado', valor: '3' },
              { nombre: 'Rechazado SII', valor: '4' },
              { nombre: 'Reclamo parcial SII', valor: '5' },
              { nombre: 'Reclamo total SII', valor: '6' },
              { nombre: 'Recibo Mercaderías SII', valor: '7' }
            ]
          },
          {
            campo: 'estadoSii',
            nombre: 'Estado SII',
            tipoFiltro: 'tag',
            opciones: [
              { nombre: 'Pendiente de envío al SII', valor: '1' },
              { nombre: 'Enviado al SII', valor: '2' },
              { nombre: 'Error al enviar', valor: '3' },
              { nombre: 'Aceptado por el SII', valor: '4' },
              { nombre: 'Aceptado con reparos por el SII', valor: '5' },
              { nombre: 'Rechazado por el SII', valor: '6' },
              { nombre: 'Pendiente de consulta en el SII', valor: '7' },
              { nombre: 'Error al consultar en el SII', valor: '8' }
            ]
          },
          {
            campo: 'fechaReciboMercaderia',
            nombre: 'Fecha Recibo Mercaderías',
            tipoFiltro: 'fecha',
            opciones: fechasSimples
          },
          {
            campo: 'formaDePago',
            nombre: 'Forma de pago',
            tipoFiltro: 'tag',
            opciones: [
              { nombre: 'Contado', valor: '1' },
              { nombre: 'Crédito', valor: '2' },
              { nombre: 'Sin costo', valor: '3' }
            ]
          },
          { campo: 'contacto', nombre: 'Contacto', tipoFiltro: 'texto' },
          { campo: 'correoReceptor', nombre: 'Correo receptor', tipoFiltro: 'texto' },
          {
            campo: 'fechaCesion',
            nombre: 'Fecha de Cesión',
            tipoFiltro: 'fecha',
            opciones: fechasSimples
          },
          { campo: 'incompleto', nombre: 'No recibido', tipoFiltro: 'texo' },
          { campo: 'codigoItem', nombre: 'Código de item', tipoFiltro: 'texo' },
          {
            campo: 'tipo',
            nombre: 'Estado pago prov.',
            tipoFiltro: 'tag',
            opciones: [
              { nombre: 'Sin acción', valor: '0' },
              { nombre: 'En Proceso', valor: '1' },
              { nombre: 'Pago Enviado', valor: '2' },
              { nombre: 'Pagado', valor: '3' },
              { nombre: 'Cobrado', valor: '4' },
              { nombre: 'Pago no efectuado', valor: '5' }
            ]
          },
          { campo: 'monto', nombre: 'Monto pago prov.', tipoFiltro: 'texo' },
          { campo: 'lugar', nombre: 'Lugar pago prov.', tipoFiltro: 'texo' },
          { campo: 'comentario', nombre: 'Comentario pago prov.', tipoFiltro: 'texo' },
          { campo: 'fecha', nombre: 'Fecha pago prov.', tipoFiltro: 'texo' },
          {
            campo: 'medio',
            nombre: 'Medio pago prov.',
            tipoFiltro: 'tag',
            opciones: [
              { nombre: 'Cheque', valor: 'CH' },
              { nombre: 'Cheque  a fecha', valor: 'CF' },
              { nombre: 'Letra', valor: 'LT' },
              { nombre: 'Efectivo', valor: 'EF' },
              { nombre: 'Pago a Cta. Cte.', valor: 'PE' },
              { nombre: 'Tarjeta Crédito', valor: 'TC' },
              { nombre: 'Vale Vista', valor: 'VV' },
              { nombre: 'Confirming', valor: 'CO' },
              { nombre: 'Otro', valor: 'OT' }
            ]
          },
          {
            campo: 'tpoTranCompra',
            nombre: 'Tipo de Compra',
            categoria: 'recibidos',
            tipoFiltro: 'tag',
            opciones: [
              { nombre: 'Del Giro', valor: '1' },
              { nombre: 'Supermercados', valor: '2' },
              { nombre: 'Bien Raíz', valor: '3' },
              { nombre: 'Activo Fijo', valor: '4' },
              { nombre: 'IVA Uso Común', valor: '5' },
              { nombre: 'Sin derecho', valor: '6' },
              { nombre: 'No Inculir', valor: '7' }
            ]
          },
          {
            campo: 'tpoTraVenta',
            nombre: 'Tipo de Venta',
            categoria: 'emitidos',
            tipoFiltro: 'tag',
            opciones: [
              { nombre: 'Ventas del Giro', valor: '1' },
              { nombre: 'Venta Bien Raíz', valor: '2' },
              { nombre: 'Venta Activo Fijo', valor: '3' }
            ]
          }
        ];

        scope.agregarDocumentosPermitidos = function() {
          for (var i = 0; i < scope.posiblesCampos.length; i++) {
            if (scope.posiblesCampos[i].campo == 'tipoDocumento') {
              //array.splice(index, 1);
              for (var x = 0; x < scope.tiposDeDocumentos.length; x++) {
                if (
                  VistasDte[$stateParams.app][$stateParams.categoria][
                    $stateParams.vista
                  ].documentosDisponibles.indexOf(parseInt(scope.tiposDeDocumentos[x].valor)) >= 0
                ) {
                  scope.posiblesCampos[i].opciones.push(scope.tiposDeDocumentos[x]);
                }
              }
            }
          }
        };
        scope.agregarDocumentosPermitidos();

        scope.obtenerNombreCampo = function(campo) {
          for (var i = 0; i < scope.posiblesCampos.length; i++) {
            if (scope.posiblesCampos[i].campo == campo) {
              return scope.posiblesCampos[i];
            }
          }
          return '';
        };

        scope.filtros = [];
        scope.ordenes = [];
        scope.toggleConfigFiltros = function($event, quien) {
          $event.preventDefault();
          $('.uk-tooltip').hide();
          if (scope.activo == quien || !$rootScope.configFiltrosActivo) {
            $rootScope.configFiltrosActivo = !$rootScope.configFiltrosActivo;
          }
          if (!$rootScope.configFiltrosActivo) {
            scope.activo = '';
          } else {
            scope.activo = quien;
          }
        };
        scope.esconderFiltros = function() {
          $rootScope.configFiltrosActivo = false;
        };
        scope.yaSeEstaFiltrando = function(campo) {
          for (var i = 0; i < scope.filtros.length; i++) {
            if (scope.filtros[i].campo == campo.campo) {
              return true;
            }
          }
          return false;
        };

        scope.yaSeEstaOrdenado = function(campo) {
          for (var i = 0; i < scope.ordenes.length; i++) {
            if (scope.ordenes[i].campo == campo.campo) {
              return true;
            }
          }
          return false;
        };
        scope.agregarFiltro = function(campo) {
          if (scope.yaSeEstaFiltrando(campo)) {
            console.log('Ya se esta filtrnado por ', campo);
          } else {
            var filtro = {
              nombre: campo.nombre,
              campo: campo.campo,
              valor: '',
              tipoFiltro: campo.tipoFiltro,
              configurando: true,
              manual: true
            };
            if (typeof campo.opciones != 'undefined') {
              filtro.opciones = campo.opciones;
            } else {
              filtro.opciones = [];
            }
            if (filtro.tipoFiltro == 'fecha') {
              filtro.valor = {};
              filtro.valor.desde = '';
              filtro.valor.hasta = '';
              filtro.valor.texto = 'losUltimos30Dias';
              filtro.manual = false;
            }
            scope.filtros.push(filtro);
          }
        };
        scope.agregarOrden = function(campo) {
          var tipo = '-';
          var orden = {
            nombre: campo.nombre,
            campo: campo.campo,
            orden: tipo
          };
          scope.ordenes.push(orden);
        };
        scope.eliminarFiltro = function(filtro) {
          var posicion = -1;
          for (var i = 0; i < scope.filtros.length; i++) {
            if (scope.filtros[i].campo == filtro.campo) {
              posicion = i;
              break;
            }
          }
          if (posicion >= 0) {
            scope.filtros.splice(posicion, 1);
          }
        };
        scope.eliminarOrden = function(orden) {
          var posicion = -1;
          for (var i = 0; i < scope.ordenes.length; i++) {
            if (scope.ordenes[i].campo == orden.campo) {
              posicion = i;
              break;
            }
          }
          if (posicion >= 0) {
            scope.ordenes.splice(posicion, 1);
          }
        };
        scope.configurar = function(filtro) {
          for (var i = 0; i < scope.filtros.length; i++) {
            if (typeof filtro != 'undefined' && scope.filtros[i].campo == filtro.campo) {
              scope.filtros[i].configurando = true;
              if (scope.filtros[i].tipoFiltro == 'fecha') {
                if (typeof scope.filtros[i].valor == 'string') {
                  scope.filtros[i].valor = {};
                  scope.filtros[i].valor.desde = '';
                  scope.filtros[i].valor.hasta = '';
                  scope.filtros[i].valor.texto = 'losUltimos30Dias';
                  scope.filtros[i].manual = false;
                }
              }
            } else {
              scope.filtros[i].configurando = false;
              //scope.filtros[i].manual = false;
            }
          }
        };

        scope.toggleOrden = function(orden) {
          orden.orden = orden.orden == '-' ? '+' : '-';
        };
        scope.generarQueryParams = function() {
          var filtros = [];
          for (var i = 0; i < scope.filtros.length; i++) {
            if (scope.filtros[i].tipoFiltro == 'fecha') {
              if (scope.filtros[i].manual) {
                filtros.push(
                  scope.filtros[i].campo +
                    ':' +
                    scope.filtros[i].valor.desde +
                    '--' +
                    scope.filtros[i].valor.hasta
                );
              } else {
                filtros.push(scope.filtros[i].campo + ':' + scope.filtros[i].valor.texto);
              }
            } else {
              filtros.push(scope.filtros[i].campo + ':' + scope.filtros[i].valor);
            }
          }

          var ordenes = [];
          for (var i = 0; i < scope.ordenes.length; i++) {
            ordenes.push(scope.ordenes[i].orden + scope.ordenes[i].campo);
          }
          var queryParams = {
            pagina: $location.search().pagina,
            itemsPorPagina: $location.search().itemsPorPagina,
            orden: ordenes.join(','),
            filtros: filtros.join('|')
          };
          return queryParams;
        };
        scope.aplicar = function() {
          var queryParams = scope.generarQueryParams();
          $location.search(queryParams);
          $state.go('restringido.dte', {
            app: $stateParams.app,
            categoria: $stateParams.categoria,
            vista: $stateParams.vista,
            filtros: queryParams.filtros,
            orden: queryParams.orden,
            pagina: 1,
            itemsPorPagina: $stateParams.itemsPorPagina
          });
          $state.reload();
        };

        scope.nombreFiltroPredefinido = '';
        scope.guardar = function() {
          var queryParams = scope.generarQueryParams();
          UIkit.modal.prompt(
            'Con que nombre quieres guardar esta vista?',
            scope.nombreFiltroPredefinido,
            function(nombre) {
              console.log('guardando con el nombre:', nombre);
              SesionFebos().filtrosDte[$stateParams.categoria].push({
                nombre: nombre,
                params: queryParams
              });
              UIkit.notify(
                "Se ha guardado la vista '" + nombre + "' <a class='notify-action'>[X]</a> ",
                { status: 'success', timeout: 1500 }
              );
            },
            { labels: { Ok: 'Guardar', Cancel: 'Mejor no, me arrepentí' } }
          );
        };
        scope.seleccionarVista = function(filtro) {
          console.log('VISTA SELECCIONADA', filtro);
          UIkit.notify(
            "Cargando la vista '" + filtro.nombre + "' <a class='notify-action'>[X]</a> ",
            { status: 'success', timeout: 1500 }
          );
          $location.search(filtro.params);
          $state.go('restringido.dte', {
            app: $stateParams.app,
            categoria: $stateParams.categoria,
            vista: $stateParams.vista,
            filtros: filtro.params.filtros,
            orden: filtro.params.orden,
            pagina: 1,
            itemsPorPagina: $stateParams.itemsPorPagina
          });
          $state.reload();
        };
        scope.eliminarFiltroGuardado = function(filtro) {
          var indiceParaEliminar = -1;
          for (var i = 0; i <= SesionFebos().filtrosDte[$stateParams.categoria].length; i++) {
            if (SesionFebos().filtrosDte[$stateParams.categoria][i].nombre == filtro.nombre) {
              indiceParaEliminar = i;
              break;
            }
          }
          if (indiceParaEliminar >= 0) {
            UIkit.modal.confirm(
              "Estas a punto de eliminar la vista '" + filtro.nombre + "', confirmas?",
              function() {
                SesionFebos().filtrosDte[$stateParams.categoria].splice(indiceParaEliminar, 1);
                UIkit.notify(
                  "Se ha eliminado la vista '" +
                    filtro.nombre +
                    "' <a class='notify-action'>[X]</a> ",
                  { status: 'success', timeout: 1500 }
                );
                $rootScope.$broadcast('refrescarTituloDte');
              },
              function() {},
              { labels: { Ok: 'Si, eliminar', Cancel: 'Mejor no, me arrepentí' } }
            );
          }
        };

        // MINI SIDEBAR
        scope.mini_sidebar_toggle = false;
        $rootScope.miniSidebarActive = false;

        // change input's state to checked if mini sidebar is active
        if (
          localStorage.getItem('altair_sidebar_mini') !== null &&
          localStorage.getItem('altair_sidebar_mini') == '1'
        ) {
          $rootScope.miniSidebarActive = true;
          scope.mini_sidebar_toggle = true;
        }

        // toggle mini sidebar
        $('#style_sidebar_mini')
          .on('ifChecked', function(event) {
            localStorage.setItem('altair_sidebar_mini', '1');
            localStorage.removeItem('altair_sidebar_slim');
            $rootScope.configFiltrosActivo = false;
            $timeout(function() {
              $rootScope.miniSidebarActive = true;
              $state.go($state.current, {}, { reload: true });
            });
          })
          .on('ifUnchecked', function(event) {
            localStorage.removeItem('altair_sidebar_mini');
            $rootScope.configFiltrosActivo = false;
            $timeout(function() {
              $rootScope.miniSidebarActive = false;
              $state.go($state.current, {}, { reload: true });
            });
          });

        // SLIM SIDEBAR
        scope.slim_sidebar_toggle = false;
        $rootScope.slimSidebarActive = false;

        // change input's state to checked if mini sidebar is active
        if (
          localStorage.getItem('altair_sidebar_slim') !== null &&
          localStorage.getItem('altair_sidebar_slim') == '1'
        ) {
          $rootScope.slimSidebarActive = true;
          scope.slim_sidebar_toggle = true;
        }

        // toggle mini sidebar
        $('#style_sidebar_slim')
          .on('ifChecked', function(event) {
            localStorage.removeItem('altair_sidebar_mini');
            localStorage.setItem('altair_sidebar_slim', '1');
            $rootScope.configFiltrosActivo = false;
            $timeout(function() {
              $rootScope.slimSidebarActive = true;
              $state.go($state.current, {}, { reload: true });
            });
          })
          .on('ifUnchecked', function(event) {
            localStorage.removeItem('altair_sidebar_slim');
            $rootScope.configFiltrosActivo = false;
            $timeout(function() {
              $rootScope.slimSidebarActive = false;
              $state.go($state.current, {}, { reload: true });
            });
          });

        // BOXED LAYOUT
        scope.boxed_layout_toggle = false;

        // change input's state to checked if boxed layout is active
        if (
          localStorage.getItem('altair_boxed_layout') !== null &&
          localStorage.getItem('altair_boxed_layout') == '1'
        ) {
          $rootScope.boxedLayoutActive = true;
          scope.boxed_layout_toggle = true;
        }

        // toggle mini sidebar
        $('#style_layout_boxed')
          .on('ifChecked', function(event) {
            localStorage.setItem('altair_boxed_layout', '1');
            $rootScope.configFiltrosActivo = false;
            $timeout(function() {
              $rootScope.boxedLayoutActive = true;
              $state.go($state.current, {}, { reload: true });
            });
          })
          .on('ifUnchecked', function(event) {
            localStorage.removeItem('altair_boxed_layout');
            $rootScope.configFiltrosActivo = false;
            $timeout(function() {
              $rootScope.boxedLayoutActive = false;
              $state.go($state.current, {}, { reload: true });
            });
          });

        // main menu accordion mode
        var $accordion_mode_toggle = $('#accordion_mode_main_menu');

        if ($rootScope.menuAccordionMode) {
          $accordion_mode_toggle.iCheck('check');
        }

        $accordion_mode_toggle
          .on('ifChecked', function() {
            $rootScope.menuAccordionMode = true;
          })
          .on('ifUnchecked', function() {
            $rootScope.menuAccordionMode = false;
          });

        // check which theme is active
        if (localStorage.getItem('altair_theme') !== null) {
          $rootScope.main_theme = localStorage.getItem('altair_theme');
        } else {
          $rootScope.main_theme = 'default_theme';
        }

        //aciones a realizar una vez todo listo
        for (var param in query) {
          var campo = scope.obtenerNombreCampo(param);
          campo.configurando = false;
          var filtro = {
            nombre: campo.nombre,
            campo: campo.campo,
            tipoFiltro: campo.tipoFiltro,
            configurando: false,
            manual: false
          };
          if (filtro.tipoFiltro == 'fecha') {
            filtro.valor = {};
            if (query[param].substring(0, 1) == '[') {
              filtro.valor.desde = '';
              filtro.valor.hasta = '';
              filtro.valor.texto = query[param];
              filtro.manual = false;
            } else {
              filtro.valor.desde = query[param].split('--')[0];
              filtro.valor.hasta = query[param].split('--')[1];
              filtro.valor.texto = '';
              filtro.manual = true;
            }
          } else {
            try {
              filtro.valor = query[param].includes(',') ? query[param].split(',') : query[param];
            } catch (e) {
              filtro.valor = query[param];
            }
          }
          if (typeof campo.opciones != 'undefined') {
            filtro.opciones = campo.opciones;
          } else {
            filtro.opciones = [];
          }
          scope.filtros.push(filtro);
        }
        for (var i = 0; i < ordenes.length; i++) {
          var orden = ordenes[i];
          var campo = scope.obtenerNombreCampo(orden.substring(1));
          var tipo = orden.substring(0, 1);
          var orden = {
            nombre: campo.nombre,
            campo: campo.campo,
            orden: tipo
          };
          scope.ordenes.push(orden);
        }

        //                        console.log("filtros aplicados; ",scope.filtros);
      }
    };
  }
]);
