febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $locationProvider.hashPrefix('');

                // Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
                $urlRouterProvider
                        .when('/', '/cloud/ingreso')
                        //.otherwise('/error/404');
                        .otherwise('/cloud/ingreso');
                        

                $stateProvider
                        // -- ERROR PAGES --
                        .state("error", {
                            url: "/error",
                            templateUrl: 'app/views/error.html',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit'
                                        ]);
                                    }]
                            }
                        })
                        .state("error.404", {
                            url: "/404",
                            templateUrl: 'app/components/pages/error_404View.html'
                        })
                        .state("error.500", {
                            url: "/500",
                            templateUrl: 'app/components/pages/error_500View.html'
                        })

// -- RESTRICTED --
                        .state("restricted", {
                            abstract: true,
                            url: "",
                            templateUrl: 'app/views/restricted.html',
                            resolve: {

                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_selectizeJS',
                                            'lazy_switchery',
                                            'lazy_prismJS',
                                            'lazy_autosize',
                                            'lazy_iCheck',
                                            'lazy_themes',
                                            'lazy_idle_timeout',
                                            'app/febos/ayuda/ayudaController.js'
                                        ]);
                                    }]
                            }
                        })

                        .state("restringido", {
                            abstract: true,
                            url: "",
                            templateUrl: 'app/views/restringido.html',
                            resolve: {

                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_selectizeJS',
                                            'lazy_switchery',
                                            'lazy_prismJS',
                                            'lazy_autosize',
                                            'lazy_iCheck',
                                            'lazy_themes',
                                            'lazy_idle_timeout',
                                            'app/febos/ayuda/ayudaController.js',
                                            'app/shared/footer/footerController.js'
                                        ]);
                                    }]
                            }
                        })

                        // vista "vacia" para motivos especiales (como el selector de empresas)
                        .state("especial", {
                            abstract: true,
                            url: "",
                            views: {
                                '': {
                                    templateUrl: 'app/views/especial.html'
                                }
                            },
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_selectizeJS',
                                            'lazy_switchery',
                                            'lazy_prismJS',
                                            'lazy_autosize',
                                            'lazy_iCheck',
                                            'lazy_themes',
                                            'lazy_idle_timeout',
                                            'app/shared/footer/footerController.js'
                                        ]);
                                    }]
                            }
                        })

/*
*/
/*
                        .state("restricted.listado_dte", {
                            url: "/documentos/electronicos?:pagina&:itemsPorPagina&:filtros&:orden",
                            templateUrl: 'app/febos/documentos/documentosView.html',
                            controller: 'documentosCtrl',
                            reloadOnSearch:false,
                            // runGuardsAndResolvers: 'paramsOrQueryParamsChange',
                            params:{
                              pagina:"",
                              itemsPorPagina:"" ,
                              filtros:"",
                              orden:""
                            },
                            resolve: {
                                EstadoAnterior: [
                                    "$state",
                                    function ($state) {
                                        var currentStateData = {
                                            estado: $state.current.name,
                                            parametros: $state.params,
                                            url: $state.href($state.current.name, $state.params)
                                        };
                                        return currentStateData;
                                    }
                                ],
                                Autorizacion: ['Autorizacion', function (Autorizacion) {
                                        try {
                                            Autorizacion.verificar();
                                        } catch (e) {
                                            console.log("Error al autenticar", e);
                                        }
                                        return;
                                    }],
                                Datos: ['FebosAPI','$location','$rootScope', function (FebosAPI,$location,$rootScope) {
                                        //'filtros': "rutReceptor:76179952-5|fechaRecepcion:2018-04-01--2018-05-05",
                                        return FebosAPI.cl_listar_dte({
                                            'pagina': $location.search().pagina,
                                            'itemsPorPagina': $location.search().itemsPorPagina,
                                            'campos': "tipoDocumento,folio,rutEmisor,razonSocialEmisor,rutReceptor,razonSocialReceptor,rutCesionario,razonSocialCesionario,fechaCesion,codigoSii,fechaEmision,fechaRecepcion,fechaRecepcionSii,plazo,estadoComercial,estadoSii,fechaReciboMercaderia,formaDePago,montoTotal,contacto,correoReceptor,codigoSii,fechaCesion",
                                            'filtros': $location.search().filtros,
                                            'orden': $location.search().orden
                                        }).then(function (response) {
                                            return response.data;
                                        });
                                        //return [];
                                    }],
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/febos/documentos/documentosController.js',
                                        ]);
                                    }]
                            }
                        })
*/


                        // -- PERSONALIZACION PORTAL --
                        .state("restricted.personalizacion", {
                            url: "/personalizacion",
                            templateUrl: 'app/febos/configuracion/personalizacion/personalizacionView.html',
                            controller: 'personalizacionCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/personalizacion.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            // ocLazyLoad config (app/app.js)
                                            'app/febos/configuracion/personalizacion/personalizacionController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Dashboard'
                            },
                            ncyBreadcrumb: {
                                label: 'Home'
                            }
                        })
                        //Bola de cristal
                       /* .state("restricted.bolaDeCristal", {
                            url: "/soporte/bolaDeCristal",
                            templateUrl: 'app/febos/bola_de_cristal/bolaDeCristalView.html',
                            controller: 'bolaDeCristalCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/bolaDeCristal.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            // ocLazyLoad config (app/app.js)
                                            'app/febos/bola_de_cristal/bolaDeCristalController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Bola de cristal'
                            },
                            ncyBreadcrumb: {
                                label: 'Home'
                            }
                        })*/
                        // GESTION DE LISTAS
                        .state("restricted.listas", {
                            url: "/empresa/listas",
                            templateUrl: 'app/febos/listas/gestionListasView.html',
                            controller: 'gestionListasCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/listas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            // ocLazyLoad config (app/app.js)
                                            'app/febos/listas/gestionListasController.js'
                                        ], {serie: true});
                                    }]
                            }
                        })

                        
                        .state("especial.cambioClave", {
                            url: "/:app/cambio_clave",
                            templateUrl: 'app/febos/usuarios/cambio_clave/cambioClaveView.html',
                            controller: 'cambioClaveCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/usuarios/cambio_clave/cambioClaveController.js'
                                        ]);
                                    }]
                            }
                        })

                        

                        // -- GESTIÓN DOCUMENTOS MANUALES (FM) --
                        .state("restricted.recibidos_fm", {
                            url: "/documentos/manuales/recibidos",
                            templateUrl: 'app/febos/documentos/manuales/recibidos/documentosView.html',
                            controller: 'documentosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/recibidosManuales.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/manuales/recibidos/documentosController.js'
                                        ]);
                                    }]
                            }
                        })
                        // --

                        // -- GESTIÓN DOCUMENTOS ELECTRÓNICOS (FE) --
                        .state("restricted.recibidos_bh", {
                            url: "/documentos/electronicos/honorarios/recibidos",
                            templateUrl: 'app/febos/documentos/electronicos/honorarios/documentosView.html',
                            controller: 'honorariosRecibidosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/honorariosRecibidos.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/honorarios/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })


                        .state("restricted.recibidos_fe", {
                            url: "/documentos/electronicos/recibidos",
                            templateUrl: 'app/febos/documentos/electronicos/recibidos/documentosView.html',
                            controller: 'documentosRecibidosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/documentosRecibidos.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/recibidos/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        // -- GESTIÓN DOCUMENTOS ELECTRÓNICOS PAGADOS --
                        .state("restricted.pagados", {
                            url: "/documentos/electronicos/pagados",
                            templateUrl: 'app/febos/documentos/electronicos/pagados/documentosView.html',
                            controller: 'documentosPagadosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/documentosPagados.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/pagados/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        // -- GESTIÓN DOCUMENTOS CEDIDOS --
                        .state("restricted.cedidos", {
                            url: "/documentos/electronicos/cedidos",
                            templateUrl: 'app/febos/documentos/electronicos/cedidos/documentosView.html',
                            controller: 'documentosCedidosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/documentosCedidos.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/cedidos/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        // -- GESTION DOCUMENTOS GUIAS RECIBIDAS
                        .state("restricted.recibidos_guias", {
                            url: "/documentos/electronicos/guias",
                            templateUrl: 'app/febos/documentos/electronicos/guias/documentosView.html',
                            controller: 'documentosGuiasCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/recibidosGuias.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/guias/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("restricted.recibidos_pendientes_sii", {
                            url: "/documentos/electronicos/pendientes_sii",
                            templateUrl: 'app/febos/documentos/electronicos/pendientes_sii/documentosView.html',
                            controller: 'documentosPendientesSiiCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/recibidosPendientesSii.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/pendientes_sii/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("restricted.misrecibidos", {
                            url: "/documentos/electronicos/mis_recibidos",
                            templateUrl: 'app/febos/documentos/electronicos/mis_recibidos/documentosView.html',
                            controller: 'documentosMisRecibidosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/misRecibidos.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/mis_recibidos/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("restricted.norecibidos", {
                            url: "/documentos/electronicos/no_recibidos",
                            templateUrl: 'app/febos/documentos/electronicos/no_recibidos/documentosView.html',
                            controller: 'documentosNoRecibidosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/noRecibidos.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/solicitarDte.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/no_recibidos/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("restricted.reporte", {
                            url: "/documentos/electronicos/reportes",
                            templateUrl: 'app/febos/documentos/electronicos/reportes/reporteDte.html',
                            controller: 'reporteDteCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/solicitarDte.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/reportes/documentosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })

                        // -- GESTIÓN RECIBIDOS ORDEN DE COMPRA --
                        .state("restricted.emitidos_oc", {//  Listado de ordenes de compra recibidas
                            url: "/documentos/emitidos_oc",
                            templateUrl: 'app/febos/documentos/emitidos_oc/documentosView.html',
                            controller: 'emitidos_ocCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/ocEmitidas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/bitacoraDnt.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            'app/febos/documentos/emitidos_oc/documentosController.js'
                                        ]);
                                    }]
                            }
                        })
                        // --

                        // -- GESTIÓN RECIBIDOS HES --
                        .state("restricted.emitidos_hes", {
                            url: "/documentos/emitidos_hes",
                            templateUrl: 'app/febos/documentos/emitidos_hes/documentosView.html',
                            controller: 'emitidos_hesCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/hesEmitidas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/bitacoraDnt.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            'app/febos/documentos/emitidos_hes/documentosController.js'
                                        ]);
                                    }]
                            }
                        })
                        // --

                        // -- GESTIÓN RECIBIDOS HEM --
                        .state("restricted.emitidos_hem", {
                            url: "/documentos/emitidos_hem",
                            templateUrl: 'app/febos/documentos/emitidos_hem/documentosView.html',
                            controller: 'emitidos_hemCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/hemEmitidas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/bitacoraDnt.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            'app/febos/documentos/emitidos_hem/documentosController.js'
                                        ]);
                                    }]
                            }
                        })
                        // --

                        // flujos


                       
                        // --
                        // -- GESTIÓN DOCUMENTOS ELECTRÓNICOS MERCADO PUBLICO --
                        .state("restricted.emitidos_oc_mp", {
                            url: "/documentos/mercado_publico",
                            templateUrl: 'app/febos/documentos/mercado_publico/documentosView.html',
                            controller: 'mercadoPublicoCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/ocMercadoPublicoEmitidas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/bitacoraDnt.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/actualizarOCMP.js',
                                            'app/febos/documentos/mercado_publico/documentosController.js'
                                        ]);
                                    }]
                            }
                        })
                        // -- GESTIÓN DOCUMENTOS ELECTRÓNICOS MIS OC DE MERCADO PUBLICO --
                        .state("restricted.mis_ocmp_emitidos", {
                            url: "/documentos/mi_mercado_publico",
                            templateUrl: 'app/febos/documentos/mis_oc_mercado_publico/documentosView.html',
                            controller: 'misOcMercadoPublicoCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/ocMercadoPublicoEmitidas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/estadoCesion.js',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/bitacoraDnt.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/reciboMercaderiaMasivo.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            'app/febos/documentos/mis_oc_mercado_publico/documentosController.js'
                                        ]);
                                    }]
                            }
                        })
                        // -- GESTIÓN DOCUMENTOS ELECTRÓNICOS (FE) --
                        .state("restricted.rechazados_fe", {
                            url: "/documentos/electronicos/rechazados",
                            templateUrl: 'app/febos/documentos/electronicos/rechazados/rechazadosView.html',
                            controller: 'rechazadosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/documentosRechazados.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/rechazados/rechazadosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        // -- GESTIÓN DOCUMENTOS ELECTRÓNICOS (FE) --
                        .state("restricted.pre_rechazados_fe", {
                            url: "/documentos/electronicos/pre_rechazados",
                            templateUrl: 'app/febos/documentos/electronicos/pre_rechazados/preRechazadosView.html',
                            controller: 'preRechazadosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/documentosPreRechazados.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/pre_rechazados/preRechazadosController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })
                        // - DTEs Boleta Honorarios
                        .state("restricted.boleta_honorarios", {
                            url: "/documentos/boleta_honorarios",
                            templateUrl: 'app/febos/documentos/boleta_honorarios/boletaView.html',
                            controller: 'boletaHonorariosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/boltasHonorarios.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/documentos/boleta_honorarios/boletaController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })

                        // - DTEs Guia de Despacho
                        .state("restricted.guia_despacho", {
                            url: "/documentos/guia_despacho",
                            templateUrl: 'app/febos/documentos/guia_despacho/guiaDespachoView.html',
                            controller: 'guiaDespachoCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/guiasDespachoRecibidas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/documentos/guia_despacho/guiaDespachoController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })

                        // --
                        //
                        // - DTEs Cesiones
                        .state("restricted.cesiones", {
                            url: "/documentos/cesiones",
                            templateUrl: 'app/febos/documentos/cesiones/cesionesView.html',
                            controller: 'cesionesCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/recibidosCedidos.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/documentos/cesiones/cesionesController.js',
                                            '../comun/documentos/acciones/suscripcionDte.js'
                                        ]);
                                    }]
                            }
                        })

                        //
                        // -- Gestión de empresas
                        /*.state("restringido.empresas", {
                            url: "/:app/empresas",
                            templateUrl: 'app/febos/empresas/gestionEmpresasView.html',
                            controller: 'gestionEmpresasCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/gestionEmpresas.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/empresas/gestionEmpresasController.js'
                                        ]);
                                    }]
                            }
                        })
                        */

                        // -- Gestión de sucursales
                        .state("restricted.sucursales", {
                            url: "/sucursales",
                            templateUrl: 'app/febos/sucursales/gestionSucursalesView.html',
                            controller: 'gestionSucursalesCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/gestionSucursales.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/sucursales/gestionSucursalesController.js'
                                        ]);
                                    }]
                            }
                        })


                        // -- Gestión de certificado digital
                        .state("restricted.certificado", {
                            url: "/operacion/certificado",
                            templateUrl: 'app/febos/operacion/certificado_digital/certificadoDigitalView.html',
                            controller: 'certificadoDigitalCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/certificadoDigital.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/operacion/certificado_digital/certificadoDigitalController.js'
                                        ]);
                                    }]
                            }
                        })

                        // -- Gestión de CAFs
                        .state("restricted.rangos", {
                            url: "/operacion/rangos",
                            templateUrl: 'app/febos/operacion/rangos/rangosView.html',
                            controller: 'rangosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/cafs.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/operacion/rangos/rangosController.js'
                                        ]);
                                    }]
                            }
                        })

                        // SEGURIDAD

                        // -- Roles
                        

                        /*
                        .state("restricted.logs", {
                            url: "/seguridad/logs?seguimientoId",
                            templateUrl: 'app/febos/seguridad/registros/registrosView.html',
                            controller: 'registrosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/logs.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/seguridad/registros/registrosController.js'
                                        ]);
                                    }]
                            }
                        })*/


                        // -- Actividades Usuario
                        .state("restricted.actividad", {
                            url: "/usuarios/actividad/actividad?usuarioId",
                            templateUrl: 'app/febos/usuarios/actividad/actividadView.html',
                            controller: 'actividadCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/actividad.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/usuarios/actividad/actividadController.js'
                                        ]);
                                    }]
                            }
                        })

                        // -- Tokens Activos
                        .state("restricted.tokens", {
                            url: "/seguridad/integraciones",
                            templateUrl: 'app/febos/seguridad/tokens/tokensView.html',
                            controller: 'tokensCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/tokens.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/seguridad/tokens/tokensController.js'
                                        ]);
                                    }]
                            }
                        })

                        // -- Usuarios Activos
                        .state("restricted.usuarios", {
                            url: "/usuarios",
                            templateUrl: 'app/febos/usuarios/gestion/gestionUsuariosView.html',
                            controller: 'gestionUsuariosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/gestionUsuarios.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'lazy_selectizeJS',
                                            'app/febos/usuarios/gestion/gestionUsuariosController.js'
                                        ]);
                                    }]
                            }
                        })

                        // -- Perfil
                        .state("restricted.perfil_usuario", {
                            url: "/usuarios/:usuarioId",
                            templateUrl: 'app/febos/usuarios/perfil/perfilView.html',
                            controller: 'perfilCtrl',
                            //
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/perfil.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_KendoUI',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/usuarios/perfil/perfilController.js'
                                        ]);
                                    }]
                            }
                        })
                      

                        // -- configuracion : edicion de archivos
                        .state("restricted.editor_archivos", {
                            url: "/configuracion/editor",
                            templateUrl: 'app/febos/configuracion/editor/editorView.html',
                            controller: 'editorCtrl',
                            params: {
                                objHtmlEdit: null
                            },
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/editorArchivos.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/configuracion/editor/editorController.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("restricted.layouts", {
                            url: "/configuracion/layouts",
                            templateUrl: 'app/febos/configuracion/layouts/layoutsView.html',
                            controller: 'layoutsCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/layouts.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/configuracion/layouts/layoutsController.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("restricted.parametros", {
                            url: "/configuracion/parametros",
                            templateUrl: 'app/febos/configuracion/parametros/parametrosView.html',
                            controller: 'parametrosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/parametros.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/configuracion/parametros/parametrosController.js'
                                        ]);
                                    }]
                            }
                        })

                        .state("restricted.gestorMenus", {
                            url: "/menu",
                            templateUrl: 'app/febos/menu/gestionMenusView.html',
                            controller: 'gestionMenusCtrl',
                            params: {

                            },
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/menus.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/menu/gestionMenusController.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("restricted.webhooks", {
                            url: "/webhooks",
                            templateUrl: 'app/febos/webhook/webhookView.html',
                            controller: 'webhookCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/webhook.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/webhook/webhookController.js'
                                        ]);
                                    }]
                            }
                        })
                        // -- Usuarios Activos de Proveedores
                        .state("restricted.listar_usuarios", {
                            url: "/usuarios/activos",
                            templateUrl: 'app/febos/usuarios/activos/gestionUsuariosProveedorView.html',
                            controller: 'gestionUsuariosProveedorCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/usuarios/activos/gestionUsuariosProveedorController.js'
                                        ]);
                                    }]
                            }
                        })
                        // - Emitidos (desde proveedores a cloud)
                        // -- Receptores Electrónicos

                        .state("restricted.emitidos_cloud", {
                            url: "/emitidos",
                            templateUrl: 'app/febos/documentos/electronicos/emitidos/emitidosView.html',
                            controller: 'emitidosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/receptores.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/emitidos/emitidosController.js'
                                        ]);
                                    }]
                            }
                        })

                        //Rechazados desde proveedores a cloud
                        .state("restricted.rechazados_cloud", {
                            url: "/rechazados",
                            templateUrl: 'app/febos/documentos/electronicos/rechazados/rechazadosView.html',
                            controller: 'rechazadosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/receptores.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/rechazados/rechazadosController.js'
                                        ]);
                                    }]
                            }
                        })
                        //PreRechazados desde proveedores a cloud
                        .state("restricted.pre_rechazados_cloud", {
                            url: "/preRechazados",
                            templateUrl: 'app/febos/documentos/electronicos/pre_rechazados/preRechazadosView.html',
                            controller: 'preRechazadosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/receptores.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/pre_rechazados/preRechazadosController.js'
                                        ]);
                                    }]
                            }
                        })

                        //Pagados desde proveedores a cloud
                        .state("restricted.pagados_cloud", {
                            url: "/pagados",
                            templateUrl: 'app/febos/documentos/electronicos/pagados_em/pagadosEmView.html',
                            controller: 'pagadosEmCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/receptores.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/pagados_em/pagadosEmController.js'
                                        ]);
                                    }]
                            }
                        })

                        //No entregados desde proveedores a cloud
                        .state("restricted.no_entregados_cloud", {
                            url: "/no_entregados",
                            templateUrl: 'app/febos/documentos/electronicos/no_entregados/noEntregadosView.html',
                            controller: 'noEntregadosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/receptores.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/no_entregados/noEntregadosController.js'
                                        ]);
                                    }]
                            }
                        })

                        //cedidos emitidos desde proveedores a cloud
                        .state("restricted.cedidos_cloud", {
                            url: "/cedidosEm",
                            templateUrl: 'app/febos/documentos/electronicos/cedidos_em/cedidosEmView.html',
                            controller: 'cedidosEmCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/receptores.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/css/bitacora.css',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            '../comun/documentos/acciones/bitacora.js',
                                            '../comun/documentos/acciones/verDetalles.js',
                                            '../comun/documentos/acciones/verHTML.js',
                                            '../comun/documentos/acciones/aceptarMasivamente.js',
                                            '../comun/documentos/acciones/rechazarMasivamente.js',
                                            '../comun/documentos/acciones/verInformacionPago.js',
                                            '../comun/documentos/acciones/enviarDte.js',
                                            'app/febos/documentos/electronicos/cedidos_em/cedidosEmController.js'
                                        ]);
                                    }]
                            }
                        })


// ESTADOS DEL TEMPLATE - SOLO PARA EJEMPLOS

                        // -- LOGIN PAGE --
                        .state("login", {
                            url: "/login",
                            templateUrl: 'app/components/pages/loginView.html',
                            controller: 'loginCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/components/pages/loginController.js'
                                        ]);
                                    }]
                            }
                        })
                        .state("login_v2", {
                            url: "/login_v2",
                            templateUrl: 'app/components/pages/login_v2View.html',
                            controller: 'login_v2Ctrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/components/pages/login_v2Controller.js'
                                        ]);
                                    }]
                            }
                        })





                        // -- DASHBOARD --
                        .state("restricted.dashboard", {
                            url: "/dashboard",
                            templateUrl: 'app/components/dashboard/dashboardView.html',
                            controller: 'dashboardCtrl',
                            resolve: {
                                Autorizacion: ['Autorizacion', function (Autorizacion) {
                                        try {
                                            Autorizacion.verificar();
                                        } catch (e) {
                                            console.log("Error al autenticar", e);
                                        }
                                        return;
                                    }],
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        console.log("RESOLVIENDO DEPENDECIAS EN EL DASHBOARD!");
                                        return $ocLazyLoad.load([
                                            // ocLazyLoad config (app/app.js)
                                            'lazy_countUp',
                                            'lazy_charts_peity',
                                            'lazy_charts_easypiechart',
                                            'lazy_charts_metricsgraphics',
                                            'lazy_charts_chartist',
                                            'lazy_weathericons',
                                            'lazy_clndr',
                                            'lazy_google_maps',
                                            'app/components/dashboard/dashboardController.js'
                                        ], {serie: true});
                                    }],
                                sale_chart_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/mg_dashboard_chart.min.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                user_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/user_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Dashboard'
                            },
                            ncyBreadcrumb: {
                                label: 'Home'
                            }
                        })
                        // -- FORMS --
                        .state("restricted.forms", {
                            url: "/forms",
                            template: '<div ui-view autoscroll="false"/>',
                            abstract: true
                        })
                        .state("restricted.forms.regular", {
                            url: "/regular",
                            templateUrl: 'app/components/forms/regularView.html',
                            controller: 'regularCtrl',
                            resolve: {
                                Autorizacion: ['Autorizacion', function (Autorizacion) {
                                        try {
                                            Autorizacion.verificar();
                                        } catch (e) {
                                            console.log("Error al autenticar", e);
                                        }
                                        return;
                                    }],
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/forms/regularController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Regular Elements'
                            }
                        })
                        .state("restricted.forms.advanced", {
                            url: "/advanced",
                            templateUrl: 'app/components/forms/advancedView.html',
                            controller: 'advancedCtrl',
                            resolve: {
                                Autorizacion: ['Autorizacion', function (Autorizacion) {
                                        try {
                                            Autorizacion.verificar();
                                        } catch (e) {
                                            console.log("Error al autenticar", e);
                                        }
                                        return;
                                    }],
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_ionRangeSlider',
                                            'lazy_masked_inputs',
                                            'lazy_character_counter',
                                            'bower_components/jquery-ui/jquery-ui.min.js',
                                            'lazy_uiSelect',
                                            'app/components/forms/advancedController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Advanced Elements'
                            }
                        })
                        .state("restricted.forms.dynamic", {
                            url: "/dynamic",
                            templateUrl: 'app/components/forms/dynamicView.html',
                            controller: 'dynamicCtrl',
                            resolve: {
                                Autorizacion: ['Autorizacion', function (Autorizacion) {
                                        try {
                                            Autorizacion.verificar();
                                        } catch (e) {
                                            console.log("Error al autenticar", e);
                                        }
                                        return;
                                    }],
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/forms/dynamicController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Dynamic Elements'
                            }
                        })
                        .state("restricted.forms.file_input", {
                            url: "/file_input",
                            templateUrl: 'app/components/forms/file_inputView.html',
                            controller: 'file_inputCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_dropify',
                                            'app/components/forms/file_inputController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'File Input (dropify)'
                            }
                        })
                        .state("restricted.forms.file_upload", {
                            url: "/file_upload",
                            templateUrl: 'app/components/forms/file_uploadView.html',
                            controller: 'file_uploadCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/forms/file_uploadController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'File Upload'
                            }
                        })
                        .state("restricted.forms.validation", {
                            url: "/validation",
                            templateUrl: 'app/components/forms/validationView.html',
                            controller: 'validationCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_parsleyjs',
                                            'app/components/forms/validationController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Validation'
                            }
                        })
                        .state("restricted.forms.wizard", {
                            url: "/wizard",
                            templateUrl: 'app/components/forms/wizardView.html',
                            controller: 'wizardCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_parsleyjs',
                                            'lazy_wizard',
                                            'app/components/forms/wizardController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Wizard'
                            }
                        })
                        .state("restricted.forms.wysiwyg_ckeditor", {
                            url: "/wysiwyg_ckeditor",
                            templateUrl: 'app/components/forms/wysiwyg_ckeditorView.html',
                            controller: 'ckeditorCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_ckeditor',
                                            'app/components/forms/wysiwyg_ckeditorController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'ckEditor'
                            }
                        })
                        .state("restricted.forms.wysiwyg_tinymce", {
                            url: "/wysiwyg_tinymce",
                            templateUrl: 'app/components/forms/wysiwyg_tinymceView.html',
                            controller: 'tinymceCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_tinymce',
                                            'app/components/forms/wysiwyg_tinymceController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'tinyMCE'
                            }
                        })
                        .state("restricted.forms.wysiwyg_inline", {
                            url: "/wysiwyg_inline",
                            templateUrl: 'app/components/forms/wysiwyg_ckeditorInlineView.html',
                            controller: 'ckeditorInlineCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_ckeditor',
                                            'app/components/forms/wysiwyg_ckeditorInlineController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'ckEditor inline'
                            }
                        })

                        // -- LAYOUT --
                        .state("restricted.layout", {
                            url: "/layout",
                            template: '<div ui-view autoscroll="false"/>',
                            abstract: true
                        })
                        .state("restricted.layout.top_menu", {
                            url: "/top_menu",
                            templateUrl: 'app/components/layout/top_menuView.html',
                            controller: 'top_menuCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/layout/top_menuController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Top Menu'
                            }
                        })
                        .state("restricted.layout.full_header", {
                            url: "/full_header",
                            templateUrl: 'app/components/layout/full_headerView.html',
                            controller: 'full_headerCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/layout/full_headerController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Full Header'
                            }
                        })

                        // -- KENDO UI --
                        .state("restricted.kendoui", {
                            url: "/kendoui",
                            template: '<div ui-view autoscroll="false"/>',
                            abstract: true,
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('lazy_KendoUI');
                                    }]
                            }
                        })
                        .state("restricted.kendoui.autocomplete", {
                            url: "/autocomplete",
                            templateUrl: 'app/components/kendoUI/autocompleteView.html',
                            controller: 'autocompleteCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/autocompleteController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Autocomplete (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.calendar", {
                            url: "/calendar",
                            templateUrl: 'app/components/kendoUI/calendarView.html',
                            controller: 'calendarCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/calendarController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Calendar (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.colorpicker", {
                            url: "/colorPicker",
                            templateUrl: 'app/components/kendoUI/colorPickerView.html',
                            data: {
                                pageTitle: 'ColorPicker (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.combobox", {
                            url: "/combobox",
                            templateUrl: 'app/components/kendoUI/comboboxView.html',
                            controller: 'comboboxCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/comboboxController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Combobox (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.datepicker", {
                            url: "/datepicker",
                            templateUrl: 'app/components/kendoUI/datepickerView.html',
                            controller: 'datepickerCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/datepickerController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Datepicker (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.datetimepicker", {
                            url: "/datetimepicker",
                            templateUrl: 'app/components/kendoUI/datetimepickerView.html',
                            controller: 'datetimepickerCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/datetimepickerController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Datetimepicker (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.dropdown_list", {
                            url: "/dropdown_list",
                            templateUrl: 'app/components/kendoUI/dropdown_listView.html',
                            controller: 'dropdownListCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/dropdown_listController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Dropdown List (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.masked_input", {
                            url: "/masked_input",
                            templateUrl: 'app/components/kendoUI/masked_inputView.html',
                            controller: 'maskedInputCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/masked_inputController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Masked Input (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.menu", {
                            url: "/menu",
                            templateUrl: 'app/components/kendoUI/menuView.html',
                            controller: 'menuCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/menuController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Menu (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.multiselect", {
                            url: "/multiselect",
                            templateUrl: 'app/components/kendoUI/multiselectView.html',
                            controller: 'multiselectCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/multiselectController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Multiselect (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.numeric_textbox", {
                            url: "/numeric_textbox",
                            templateUrl: 'app/components/kendoUI/numeric_textboxView.html',
                            controller: 'numericTextboxCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/numeric_textboxController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Numeric textbox (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.panelbar", {
                            url: "/panelbar",
                            templateUrl: 'app/components/kendoUI/panelbarView.html',
                            data: {
                                pageTitle: 'PanelBar (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.timepicker", {
                            url: "/timepicker",
                            templateUrl: 'app/components/kendoUI/timepickerView.html',
                            data: {
                                pageTitle: 'Timepicker (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.toolbar", {
                            url: "/toolbar",
                            templateUrl: 'app/components/kendoUI/toolbarView.html',
                            controller: 'toolbarCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/toolbarController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Toolbar (kendoUI)'
                            }
                        })
                        .state("restricted.kendoui.window", {
                            url: "/window",
                            templateUrl: 'app/components/kendoUI/windowView.html',
                            controller: 'windowCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/kendoUI/windowController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Window (kendoUI)'
                            }
                        })
                        // -- COMPONENTS --
                        .state("restricted.components", {
                            url: "/components",
                            template: '<div ui-view autoscroll="false" ng-class="{ \'uk-height-1-1\': page_full_height }"/>',
                            abstract: true,
                            ncyBreadcrumb: {
                                label: 'Components'
                            }
                        })
                        .state("restricted.components.accordion", {
                            url: "/accordion",
                            controller: 'accordionCtrl',
                            templateUrl: 'app/components/components/accordionView.html',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/accordionController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Accordion (components)'
                            }
                        })
                        .state("restricted.components.animations", {
                            url: "/animations",
                            templateUrl: 'app/components/components/animationsView.html',
                            controller: 'animationsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/animationsController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Animations (MD)'
                            }
                        })
                        .state("restricted.components.autocomplete", {
                            url: "/autocomplete",
                            templateUrl: 'app/components/components/autocompleteView.html',
                            controller: 'autocompleteCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/autocompleteController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Autocomplete (components)'
                            }
                        })
                        .state("restricted.components.breadcrumbs", {
                            url: "/breadcrumbs",
                            templateUrl: 'app/components/components/breadcrumbsView.html',
                            controller: 'breadcrumbsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/breadcrumbsController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Breadcrumbs (components)'
                            },
                            ncyBreadcrumb: {
                                label: 'Breadcrumbs'
                            }})
                        .state("restricted.components.buttons", {
                            url: "/buttons",
                            templateUrl: 'app/components/components/buttonsView.html',
                            data: {
                                pageTitle: 'Buttons (components)'
                            }
                        })
                        .state("restricted.components.buttons_fab", {
                            url: "/buttons_fab",
                            templateUrl: 'app/components/components/fabView.html',
                            data: {
                                pageTitle: 'Buttons FAB (components)'
                            }
                        })
                        .state("restricted.components.cards", {
                            url: "/cards",
                            templateUrl: 'app/components/components/cardsView.html',
                            controller: 'cardsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/cardsController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Cards (components)'
                            }
                        })
                        .state("restricted.components.colors", {
                            url: "/colors",
                            templateUrl: 'app/components/components/colorsView.html',
                            data: {
                                pageTitle: 'Colors (components)'
                            }
                        })
                        .state("restricted.components.common", {
                            url: "/common",
                            templateUrl: 'app/components/components/commonView.html',
                            data: {
                                pageTitle: 'Common (components)'
                            }
                        })
                        .state("restricted.components.dropdowns", {
                            url: "/dropdowns",
                            templateUrl: 'app/components/components/dropdownsView.html',
                            data: {
                                pageTitle: 'Dropdowns (components)'
                            }
                        })
                        .state("restricted.components.dynamic_grid", {
                            url: "/dynamic_grid",
                            templateUrl: 'app/components/components/dynamic_gridView.html',
                            data: {
                                pageTitle: 'Dynamic Grid (components)'
                            }
                        })
                        .state("restricted.components.footer", {
                            url: "/footer",
                            templateUrl: 'app/components/components/footerView.html',
                            controller: 'footerCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/footerController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Footer (components)'
                            }
                        })
                        .state("restricted.components.grid", {
                            url: "/grid",
                            templateUrl: 'app/components/components/gridView.html',
                            data: {
                                pageTitle: 'Grid (components)'
                            }
                        })
                        .state("restricted.components.icons", {
                            url: "/icons",
                            templateUrl: 'app/components/components/iconsView.html',
                            data: {
                                pageTitle: 'Icons (components)'
                            }
                        })
                        .state("restricted.components.list_grid_view", {
                            url: "/list_grid_view",
                            templateUrl: 'app/components/components/list_gridView.html',
                            controller: 'list_gridCtrl',
                            resolve: {
                                products_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/ecommerce_products.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/list_gridController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Lists/Grid View (components)'
                            }
                        })
                        .state("restricted.components.lists", {
                            url: "/lists",
                            templateUrl: 'app/components/components/listsView.html',
                            data: {
                                pageTitle: 'Lists (components)'
                            }
                        })
                        .state("restricted.components.modal", {
                            url: "/modal",
                            templateUrl: 'app/components/components/modalView.html',
                            data: {
                                pageTitle: 'Modals/Lightboxes (components)'
                            }
                        })
                        .state("restricted.components.nestable", {
                            url: "/nestable",
                            templateUrl: 'app/components/components/nestableView.html',
                            controller: 'nestableCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/nestableController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Nestable (components)'
                            }
                        })
                        .state("restricted.components.notifications", {
                            url: "/notifications",
                            templateUrl: 'app/components/components/notificationsView.html',
                            controller: 'notificationsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/notificationsController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Notifications (components)'
                            }
                        })
                        .state("restricted.components.panels", {
                            url: "/panels",
                            templateUrl: 'app/components/components/panelsView.html',
                            data: {
                                pageTitle: 'Panels (components)'
                            }
                        })
                        .state("restricted.components.preloaders", {
                            url: "/preloaders",
                            templateUrl: 'app/components/components/preloadersView.html',
                            controller: 'preloadersCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/components/preloadersController.js');
                                    }]
                            },
                            data: {
                                pageTitle: 'Preloaders (components)'
                            }
                        })
                        .state("restricted.components.slider", {
                            url: "/slider",
                            templateUrl: 'app/components/components/sliderView.html',
                            data: {
                                pageTitle: 'Slider (components)'
                            }
                        })
                        .state("restricted.components.slideshow", {
                            url: "/slideshow",
                            templateUrl: 'app/components/components/slideshowView.html',
                            data: {
                                pageTitle: 'Slideshow (components)'
                            }
                        })
                        .state("restricted.components.smooth_scroll", {
                            url: "/smooth_scroll",
                            templateUrl: 'app/components/components/smooth_scrollView.html',
                            data: {
                                pageTitle: 'Smooth Scroll (components)'
                            }
                        })
                        .state("restricted.components.sortable", {
                            url: "/sortable",
                            templateUrl: 'app/components/components/sortableView.html',
                            controller: 'sortableCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_dragula',
                                            'app/components/components/sortableController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Sortable (components)'
                            }
                        })
                        .state("restricted.components.switcher", {
                            url: "/switcher",
                            templateUrl: 'app/components/components/switcherView.html',
                            data: {
                                pageTitle: 'Switcher (components)'
                            }
                        })
                        .state("restricted.components.tables_examples", {
                            url: "/tables_examples",
                            templateUrl: 'app/components/components/tables_examplesView.html',
                            controller: 'tables_examplesCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/components/tables_examplesController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Tables Examples (components)'
                            }
                        })
                        .state("restricted.components.tables", {
                            url: "/tables",
                            templateUrl: 'app/components/components/tablesView.html',
                            data: {
                                pageTitle: 'Tables (components)'
                            }
                        })
                        .state("restricted.components.tabs", {
                            url: "/tabs",
                            templateUrl: 'app/components/components/tabsView.html',
                            data: {
                                pageTitle: 'Tabs (components)'
                            }
                        })
                        .state("restricted.components.timeline", {
                            url: "/timeline",
                            templateUrl: 'app/components/components/timelineView.html',
                            controller: 'timelineCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/components/timelineController.js'
                                        ]);
                                    }],
                                user_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/user_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Timeline'
                            }
                        })
                        .state("restricted.components.tooltips", {
                            url: "/tooltips",
                            templateUrl: 'app/components/components/tooltipsView.html',
                            data: {
                                pageTitle: 'Tooltips (components)'
                            }
                        })
                        .state("restricted.components.typography", {
                            url: "/typography",
                            templateUrl: 'app/components/components/typographyView.html',
                            data: {
                                pageTitle: 'Typography (components)'
                            }
                        })
                        // -- E-COMMERCE --
                        .state("restricted.ecommerce", {
                            url: "/ecommerce",
                            template: '<div ui-view autoscroll="false"/>',
                            abstract: true
                        })
                        .state("restricted.ecommerce.payment_page", {
                            url: "/payment_page",
                            templateUrl: 'app/components/ecommerce/paymentView.html',
                            controller: 'paymentCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/ecommerce/paymentController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Product Details'
                            }
                        })
                        .state("restricted.ecommerce.product_details", {
                            url: "/product_details",
                            templateUrl: 'app/components/ecommerce/product_detailsView.html',
                            controller: 'productCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/ecommerce/productController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Product Details'
                            }
                        })
                        .state("restricted.ecommerce.product_edit", {
                            url: "/product_edit",
                            templateUrl: 'app/components/ecommerce/product_editView.html',
                            controller: 'productCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/ecommerce/productController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Product Edit'
                            }
                        })
                        .state("restricted.ecommerce.products_list", {
                            url: "/products_list",
                            templateUrl: 'app/components/ecommerce/products_listView.html',
                            controller: 'products_listCtrl',
                            resolve: {
                                products_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/ecommerce_products.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_pagination',
                                            'app/components/ecommerce/products_listController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Products List'
                            }
                        })
                        .state("restricted.ecommerce.products_grid", {
                            url: "/products_grid",
                            templateUrl: 'app/components/ecommerce/products_gridView.html',
                            controller: 'products_gridCtrl',
                            resolve: {
                                products_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/ecommerce_products.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/ecommerce/products_gridController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Products Grid'
                            }
                        })
                        // -- PLUGINS --
                        .state("restricted.plugins", {
                            url: "/plugins",
                            template: '<div ui-view autoscroll="false"/>',
                            abstract: true
                        })
                        .state("restricted.plugins.calendar", {
                            url: "/calendar",
                            templateUrl: 'app/components/plugins/calendarView.html',
                            controller: 'calendarCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_fullcalendar',
                                            'app/components/plugins/calendarController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Calendar'
                            }
                        })
                        .state("restricted.plugins.code_editor", {
                            url: "/code_editor",
                            templateUrl: 'app/components/plugins/code_editorView.html',
                            controller: 'code_editorCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_codemirror',
                                            'app/components/plugins/code_editorController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Code Editor'
                            }
                        })
                        .state("restricted.plugins.charts", {
                            url: "/charts",
                            templateUrl: 'app/components/plugins/chartsView.html',
                            controller: 'chartsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_charts_chartist',
                                            'lazy_charts_metricsgraphics',
                                            'lazy_charts_c3',
                                            'app/components/plugins/chartsController.js'
                                        ], {serie: true});
                                    }],
                                mg_chart_linked_1: function ($http) {
                                    return $http({method: 'GET', url: 'data/mg_brief-1.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                mg_chart_linked_2: function ($http) {
                                    return $http({method: 'GET', url: 'data/mg_brief-2.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                mg_confidence_band: function ($http) {
                                    return $http({method: 'GET', url: 'data/mg_confidence_band.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                mg_currency: function ($http) {
                                    return $http({method: 'GET', url: 'data/mg_some_currency.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Charts'
                            }
                        })
                        .state("restricted.plugins.charts_echarts", {
                            url: "/echarts",
                            templateUrl: 'app/components/plugins/charts_echartsView.html',
                            controller: 'charts_echartsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_echarts',
                                            'app/components/plugins/charts_echartsController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Charts (echarts)'
                            }
                        })
                        .state("restricted.plugins.crud_table", {
                            url: "/crud_table",
                            templateUrl: 'app/components/plugins/crud_tableView.html',
                            controller: 'crud_tableCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_masked_inputs',
                                            'app/components/plugins/crud_tableController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Context Menu'
                            }
                        })
                        .state("restricted.plugins.context_menu", {
                            url: "/context_menu",
                            templateUrl: 'app/components/plugins/context_menuView.html',
                            controller: 'context_menuCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_context_menu',
                                            'app/components/plugins/context_menuController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Context Menu'
                            }
                        })
                        .state("restricted.plugins.datatables", {
                            url: "/datatables",
                            templateUrl: 'app/components/plugins/datatablesView.html',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'bower_components/angular-resource/angular-resource.min.js',
                                            'lazy_datatables',
                                            'app/components/plugins/datatablesController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Datatables'
                            }
                        })
                        .state("restricted.plugins.diff_view", {
                            url: "/diff_view",
                            templateUrl: 'app/components/plugins/diff_viewView.html',
                            controller: 'diff_viewCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_diff',
                                            'app/components/plugins/diff_viewController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Diff View'
                            }
                        })
                        .state("restricted.plugins.filemanager", {
                            url: "/filemanager",
                            controller: 'filemanagerCtrl',
                            templateUrl: 'app/components/plugins/filemanagerView.html',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_filemanager',
                                            'app/components/plugins/filemanagerController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'FileManager'
                            }
                        })
                        .state("restricted.plugins.gantt_chart", {
                            url: "/gantt_chart",
                            controller: 'gantt_chartCtrl',
                            templateUrl: 'app/components/plugins/gantt_chartView.html',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_gantt_chart',
                                            'app/components/plugins/gantt_chartController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Gantt Chart'
                            }
                        })
                        .state("restricted.plugins.google_maps", {
                            url: "/google_maps",
                            templateUrl: 'app/components/plugins/google_mapsView.html',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_google_maps',
                                            'app/components/plugins/google_mapsController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Google Maps'
                            }
                        })
                        .state("restricted.plugins.image_cropper", {
                            url: "/image_cropper",
                            templateUrl: 'app/components/plugins/image_cropperView.html',
                            controller: 'image_cropperCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_image_cropper',
                                            'app/components/plugins/image_cropperController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Google Maps'
                            }
                        })
                        .state("restricted.plugins.idle_timeout", {
                            url: "/idle_timeout",
                            templateUrl: 'app/components/plugins/idle_timeoutView.html',
                            controller: 'idle_timeoutCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_idle_timeout',
                                            'app/components/plugins/idle_timeoutController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Idle Timeout'
                            }
                        })
                        .state("restricted.plugins.push_notifications", {
                            url: "/push_notifications",
                            templateUrl: 'app/components/plugins/push_notificationsView.html',
                            controller: 'push_notificationsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            // push.js
                                            'bower_components/push.js/bin/push.min.js',
                                            'app/components/plugins/push_notificationsController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Push Notifications'
                            }
                        })
                        .state("restricted.plugins.tablesorter", {
                            url: "/tablesorter",
                            templateUrl: 'app/components/plugins/tablesorterView.html',
                            controller: 'tablesorterCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_ionRangeSlider',
                                            'lazy_tablesorter',
                                            'app/components/plugins/tablesorterController.js'
                                        ], {serie: true});
                                    }],
                                ts_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/tablesorter.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Tablesorter'
                            }
                        })
                        .state("restricted.plugins.tour", {
                            url: "/tour",
                            templateUrl: 'app/components/plugins/tourView.html',
                            controller: 'tourCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_tour',
                                            'app/components/plugins/tourController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Tour'
                            }
                        })
                        .state("restricted.plugins.tree", {
                            url: "/tree",
                            templateUrl: 'app/components/plugins/treeView.html',
                            controller: 'treeCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_iCheck',
                                            'lazy_tree',
                                            'app/components/plugins/treeController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Tree'
                            }
                        })
                        .state("restricted.plugins.vector_maps", {
                            url: "/vector_maps",
                            templateUrl: 'app/components/plugins/vector_mapsView.html',
                            controller: 'vector_mapsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_vector_maps',
                                            'app/components/plugins/vector_mapsController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Vector Maps'
                            }
                        })

                        // -- PAGES --
                        .state("restricted.pages", {
                            url: "/pages",
                            template: '<div ui-view autoscroll="false" ng-class="{ \'uk-height-1-1\': page_full_height }" />',
                            abstract: true
                        })
                        .state("restricted.pages.blank", {
                            url: "/blank",
                            templateUrl: 'app/components/pages/blankView.html',
                            data: {
                                pageTitle: 'Blank Page'
                            }
                        })
                        .state("restricted.pages.chat", {
                            url: "/chat",
                            templateUrl: 'app/components/pages/chatView.html',
                            controller: 'chatCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/chatController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Chat'
                            }
                        })
                        .state("restricted.pages.chatboxes", {
                            url: "/chatboxes",
                            templateUrl: 'app/components/pages/chatboxesView.html',
                            controller: 'chatboxesCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/chatboxesController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Chatboxes'
                            }
                        })
                        .state("restricted.pages.contact_list", {
                            url: "/contact_list",
                            templateUrl: 'app/components/pages/contact_listView.html',
                            controller: 'contact_listCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/contact_listController.js'
                                        ], {serie: true});
                                    }],
                                contact_list: function ($http) {
                                    return $http({method: 'GET', url: 'data/contact_list.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Contact List'
                            }
                        })
                        .state("restricted.pages.contact_list_horizontal", {
                            url: "/contact_list_horizontal",
                            templateUrl: 'app/components/pages/contact_list_horizontalView.html',
                            controller: 'contact_list_horizontalCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/contact_list_horizontalController.js'
                                        ], {serie: true});
                                    }],
                                contact_list: function ($http) {
                                    return $http({method: 'GET', url: 'data/contact_list.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Contact List Horizontal'
                            }
                        })
                        .state("restricted.pages.contact_list_v2", {
                            url: "/contact_list_v2",
                            templateUrl: 'app/components/pages/contact_list_v2View.html',
                            controller: 'contact_list_v2Ctrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_listNav',
                                            'app/components/pages/contact_list_v2Controller.js'
                                        ], {serie: true});
                                    }],
                                contact_list: function ($http) {
                                    return $http({method: 'GET', url: 'data/contact_list.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Contact List v2'
                            }
                        })
                        .state("restricted.pages.gallery", {
                            url: "/gallery",
                            templateUrl: 'app/components/pages/galleryView.html',
                            controller: 'galleryCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/galleryController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Gallery'
                            }
                        })
                        .state("restricted.pages.gallery_v2", {
                            url: "/gallery_v2",
                            templateUrl: 'app/components/pages/gallery_v2View.html',
                            controller: 'gallery_v2Ctrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/gallery_v2Controller.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Gallery'
                            }
                        })
                        .state("restricted.pages.help", {
                            url: "/help",
                            templateUrl: 'app/components/pages/helpView.html',
                            controller: 'helpCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/helpController.js'
                                        ], {serie: true});
                                    }],
                                help_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/help_faq.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Help Center'
                            }
                        })
                        .state("restricted.pages.invoices", {
                            url: "/invoices",
                            abstract: true,
                            templateUrl: 'app/components/pages/invoices_listView.html',
                            controller: 'invoicesCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/pages/invoicesController.js');
                                    }],
                                invoices_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/invoices_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            }
                        })
                        .state("restricted.pages.invoices.list", {
                            url: "/list",
                            views: {
                                'ivoice_content': {
                                    templateUrl: 'app/components/pages/invoices_blankView.html',
                                    controller: 'invoicesCtrl'
                                }
                            },
                            data: {
                                pageTitle: 'Invoices'
                            }
                        })
                        .state("restricted.pages.invoices.details", {
                            url: "/details/{invoiceId:[0-9]{1,4}}",
                            views: {
                                'ivoice_content': {
                                    templateUrl: 'app/components/pages/invoices_detailView.html',
                                    controller: 'invoicesCtrl'
                                }
                            },
                            params: {hidePreloader: true}
                        })
                        .state("restricted.pages.invoices.add", {
                            url: "/add",
                            views: {
                                'ivoice_content': {
                                    templateUrl: 'app/components/pages/invoices_addView.html',
                                    controller: 'invoicesCtrl'
                                }
                            },
                            params: {hidePreloader: true}
                        })
                        .state("restricted.pages.mailbox", {
                            url: "/mailbox",
                            templateUrl: 'app/components/pages/mailboxView.html',
                            controller: 'mailboxCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/pages/mailboxController.js');
                                    }],
                                messages: function ($http) {
                                    return $http({method: 'GET', url: 'data/mailbox_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Mailbox'
                            }
                        })
                        .state("restricted.pages.mailbox_v2", {
                            url: "/mailbox_v2",
                            templateUrl: 'app/components/pages/mailbox_v2View.html',
                            controller: 'mailbox_v2Ctrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/pages/mailbox_v2Controller.js');
                                    }],
                                messages: function ($http) {
                                    return $http({method: 'GET', url: 'data/mailbox_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Mailbox v2'
                            }
                        })
                        .state("restricted.pages.notes", {
                            url: "/notes",
                            templateUrl: 'app/components/pages/notesView.html',
                            controller: 'notesCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/notesController.js'
                                        ]);
                                    }],
                                notes_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/notes_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Notes'
                            }
                        })
                        .state("restricted.pages.pricing_tables", {
                            url: "/pricing_tables",
                            templateUrl: 'app/components/pages/pricing_tablesView.html',
                            data: {
                                pageTitle: 'Pricing Tables'
                            }
                        })
                        .state("restricted.pages.pricing_tables_v2", {
                            url: "/pricing_tables_v2",
                            templateUrl: 'app/components/pages/pricing_tables_v2View.html',
                            data: {
                                pageTitle: 'Pricing Tables v2'
                            }
                        })
                        .state("restricted.pages.scrum_board", {
                            url: "/scrum_board",
                            templateUrl: 'app/components/pages/scrum_boardView.html',
                            controller: 'scrum_boardCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_dragula',
                                            'app/components/pages/scrum_boardController.js'
                                        ], {serie: true});
                                    }],
                                tasks_list: function ($http) {
                                    return $http({method: 'GET', url: 'data/tasks_list.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Scrum Board'
                            }
                        })
                        .state("restricted.pages.search_results", {
                            url: "/search_results",
                            templateUrl: 'app/components/pages/search_resultsView.html',
                            controller: 'search_resultsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'js!https://maps.google.com/maps/api/js',
                                            'lazy_google_maps',
                                            'app/components/pages/search_resultsController.js'
                                        ], {serie: true})
                                    }]
                            },
                            data: {
                                pageTitle: 'Search Results'
                            }
                        })
                        .state("restricted.pages.sticky_notes", {
                            url: "/sticky_notes",
                            templateUrl: 'app/components/pages/sticky_notesView.html',
                            controller: 'sticky_notesCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/sticky_notesController.js'
                                        ], {serie: true})
                                    }]
                            },
                            data: {
                                pageTitle: 'Sticky Notes'
                            }
                        })
                        .state("restricted.pages.settings", {
                            url: "/settings",
                            templateUrl: 'app/components/pages/settingsView.html',
                            controller: 'settingsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load('app/components/pages/settingsController.js')
                                    }]
                            },
                            data: {
                                pageTitle: 'Settings'
                            }
                        })
                        .state("restricted.pages.snippets", {
                            url: "/snippets",
                            templateUrl: 'app/components/pages/snippetsView.html',
                            controller: 'snippetsCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/snippetsController.js'
                                        ]);
                                    }],
                                snippets_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/snippets.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'Snippets'
                            }
                        })
                        .state("restricted.pages.todo", {
                            url: "/todo",
                            templateUrl: 'app/components/pages/todoView.html',
                            controller: 'todoCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/todoController.js'
                                        ]);
                                    }],
                                todo_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/todo_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'User profile'
                            }
                        })
                        .state("restricted.pages.user_profile", {
                            url: "/user_profile",
                            templateUrl: 'app/components/pages/user_profileView.html',
                            controller: 'user_profileCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/user_profileController.js'
                                        ]);
                                    }],
                                user_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/user_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'User profile'
                            }
                        })
                        .state("restricted.pages.user_edit", {
                            url: "/user_edit",
                            templateUrl: 'app/components/pages/user_editView.html',
                            controller: 'user_editCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'assets/js/custom/uikit_fileinput.min.js',
                                            'app/components/pages/user_editController.js'
                                        ], {serie: true});
                                    }],
                                user_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/user_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                },
                                groups_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/groups_data.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            data: {
                                pageTitle: 'User edit'
                            }
                        })
                        .state("restricted.pages.quiz", {
                            url: "/quiz",
                            templateUrl: 'app/components/pages/quizView.html',
                            controller: 'quizCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_quiz',
                                            'app/components/pages/quizController.js'
                                        ]);
                                    }]
                            },
                            data: {
                                pageTitle: 'Quiz'
                            }
                        })
                        .state("restricted.pages.issues", {
                            url: "/issues",
                            abstract: true,
                            template: '<div ui-view autoscroll="false" ng-class="{ \'uk-height-1-1\': page_full_height }" />',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_tablesorter',
                                            'app/components/pages/issuesController.js'
                                        ]);
                                    }],
                                issues_data: function ($http) {
                                    return $http({method: 'GET', url: 'data/issues.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            }
                        })
                        .state("restricted.pages.issues.list", {
                            url: "/list",
                            templateUrl: 'app/components/pages/issues_listView.html',
                            controller: 'issuesCtrl',
                            data: {
                                pageTitle: 'Issues List'
                            }
                        })
                        .state("restricted.pages.issues.details", {
                            url: "/details/{issueId:[0-9]{1,4}}",
                            controller: 'issuesCtrl',
                            templateUrl: 'app/components/pages/issue_detailsView.html',
                            data: {
                                pageTitle: 'Issue Details'
                            }
                        })
                        .state("restricted.pages.blog", {
                            url: "/blog",
                            template: '<div ui-view autoscroll="false" ng-class="{ \'uk-height-1-1\': page_full_height }" />',
                            controller: 'blogCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'app/components/pages/blogController.js'
                                        ]);
                                    }],
                                blog_articles: function ($http) {
                                    return $http({method: 'GET', url: 'data/blog_articles.json'})
                                            .then(function (data) {
                                                return data.data;
                                            });
                                }
                            },
                            abstract: true
                        })
                        .state("restricted.pages.blog.list", {
                            url: "/list",
                            controller: 'blogCtrl',
                            templateUrl: 'app/components/pages/blog_listView.html',
                            data: {
                                pageTitle: 'Blog List'
                            }
                        })
                        .state("restricted.pages.blog.article", {
                            url: "/article/{articleId:[0-9]{1,4}}",
                            controller: 'blogCtrl',
                            templateUrl: 'app/components/pages/blog_articleView.html',
                            data: {
                                pageTitle: 'Blog Article'
                            }
                        })
            }
        ]);
