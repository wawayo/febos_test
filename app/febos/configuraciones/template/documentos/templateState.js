febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.configuracion_templates', {
      url: '/:app/configuraciones/template/documentos',
      templateUrl: 'app/febos/configuraciones/template/documentos/templateView.html',
      controller: 'configurarTemplateCtrl',
      params: {
        app: 'cloud'
      },
      reloadOnSearch: false,
      resolve: {
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          'FebosAPI',
          function(FebosAPI) {
            return FebosAPI.cl_adm_configuracion_listar({ parametros: 'layout.tipos' }).then(
              function(response) {
                if (response.data.codigo == 10 && response.data.configuraciones.length > 0) {
                  //console.log(response.data.configuraciones[0].valor);
                  return { tipos: response.data.configuraciones[0].valor };
                } else {
                  return { tipos: '33,34,39,41' };
                }
              }
            );
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load(
              [
                // ocLazyLoad config (app/app.js)
                'bower_components/jquery-ui/jquery-ui.min.js',
                'lazy_uiSelect',
                'lazy_selectizeJS',
                'app/febos/configuraciones/template/documentos/templateController.js'
              ],
              { serie: true }
            );
          }
        ]
      },
      data: {
        pageTitle: 'Configuracion de Template Documentos'
      },
      ncyBreadcrumb: {
        label: 'Templates'
      }
    });
  }
]);
