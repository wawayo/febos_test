angular.module('febosApp').controller('configurarTemplateCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$http',
  'utils',
  '$state',
  '$location',
  'ContextoFebos',
  'SesionFebos',
  '$sce',
  'FebosAPI',
  '$stateParams',
  'Datos',
  'FebosUtil',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $http,
    utils,
    $state,
    $location,
    ContextoFebos,
    SesionFebos,
    $sce,
    FebosAPI,
    $stateParams,
    Datos,
    FebosUtil
  ) {
    $scope.tiposDocumentoEmpresa = Datos.tipos;
    $scope.tipos = [];
    ($scope.tiposDocumentoEmpresa.split(',') || []).forEach(function(item, index) {
      $scope.tipos.push({ tipo: item, desc: FebosUtil.tipoDocumento(parseInt(item)) });
    });
    $scope.buscar = '';
    $scope.revicion = new Date().getTime();
    $scope.actual;
    $scope.configuraciones = {};
    $scope.cargando = false;
    $scope.ambiente = FebosUtil.obtenerAmbiente();
    $scope.empresa = ContextoFebos.sesiones[ContextoFebos.usuarioActual].empresa.iut;
    console.log('ambiente  ', $rootScope.ambiente, $scope.ambiente, $scope.empresa);
    $scope.actual_tipo_pagina_configuracion = {
      maxItems: 1,
      plugins: {
        remove_button: {
          label: ''
        }
      },
      render: {
        option: function(langData, escape) {
          return (
            '<div class="option">' +
            '<i class="item-icon flag-' +
            escape(langData.value).toUpperCase() +
            '"></i>' +
            '<span>' +
            escape(langData.title) +
            '</span>' +
            '</div>'
          );
        },
        item: function(langData, escape) {
          return (
            '<div class="item"><i class="item-icon flag-' +
            escape(langData.value).toUpperCase() +
            '"></i>' +
            escape(langData.title) +
            '</div>'
          );
        }
      },
      valueField: 'value',
      labelField: 'title',
      searchField: 'title',
      create: false,
      placeholder: 'Seleccione Tipo de hoja...'
    };
    $scope.actual_tipo_pagina__options = [
      { id: 1, title: 'Letter (215.9 x 279.4 mm)', value: 'Letter' },
      { id: 2, title: 'A4 (210 x 297  mm)', value: 'A4' }
    ];

    $scope.modales = FebosUtil.modales(['configuracion_probar']);

    $scope.configurarTipo = function(seleccionado) {
      if ($scope.actual && $scope.actual.tipo == seleccionado.tipo) {
      } else {
        $scope.revicion = new Date().getTime();
        $scope.actual = JSON.parse(JSON.stringify(seleccionado));
        $scope.actual.uppie = {};
        $scope.actual.upcuerpo = {};
        $scope.actual.upcabeza = {};
        $scope.actual.multiple = true;
        console.log($scope.actual.tipo);
        if ($scope.configuraciones['conf-' + $scope.actual.tipo] == undefined) {
          $scope.cargando = true;
          var filtro = obtenerConfiguracionesPorTipoDocumento($scope.actual.tipo);
          FebosAPI.cl_adm_configuracion_listar({ parametros: filtro })
            .then(function(response) {
              console.log('?? === >>>>  ', response);
              $scope.configuraciones['conf-' + $scope.actual.tipo] = response.data;
              $scope.cargarConfiguracionesActual(
                $scope.configuraciones['conf-' + $scope.actual.tipo],
                $scope.actual.tipo
              );
              $scope.cargando = false;
            })
            .catch(function(reason) {
              $scope.cargando = false;
              console.log('error', reason);
            });
        } else {
          $scope.cargarConfiguracionesActual(
            $scope.configuraciones['conf-' + $scope.actual.tipo],
            $scope.actual.tipo
          );
        }
      }
    };
    $scope.probando = {};
    $scope.probar = function(plantilla) {
      $scope.modales.configuracionProbar.show();
      $scope.probando.cargando = true;
      var solicitud = { xml: 'febos-io/global/dummy.xml', plantilla: plantilla };
      FebosAPI.cl_obtener_documento_test(solicitud)
        .then(function(response) {
          console.log('?? === >>>>  ', response);
          $scope.iframe = $sce.trustAsResourceUrl(
            'https://docs.google.com/gview?url=' + response.data.imagenLink + '&embedded=true'
          );
          $scope.probando.imagenLink = response.data.imagenLink;
          $scope.probando.cargando = false;
        })
        .catch(function(reason) {
          console.log('error', reason);
          $scope.probando.cargando = false;
          $scope.probando.error = true;
        });
    };
    $scope.guardar = function() {
      $scope.guardando = true;
      var tipo = $scope.actual.tipo;

      function asignarConfiguracion(item, index) {
        item.ultimaActualizacion = '';
        switch (item.parametroId) {
          case 'layout.' + tipo + '.tipoHoja.0':
            item.cambio = item.valor == $scope.actual.hoja ? 'no' : 'si';
            item.valor = $scope.actual.hoja;
            break;
          case 'layout.' + tipo + '.margenSuperior.0':
            item.cambio = item.valor == $scope.actual.margen_superior ? 'no' : 'si';
            item.valor = $scope.actual.margen_superior;
            break;
          case 'layout.' + tipo + '.margenInferior.0':
            item.cambio = item.valor == $scope.actual.margen_inferior ? 'no' : 'si';
            item.valor = $scope.actual.margen_inferior;
            break;
          case 'layout.' + tipo + '.header.0':
            item.cambio = item.valor == $scope.actual.cabecera ? 'no' : 'si';
            item.valor = $scope.actual.cabecera;
            break;
          case 'layout.' + tipo + '.file.0':
            item.cambio = item.valor == $scope.actual.cuerpo ? 'no' : 'si';
            item.valor = $scope.actual.cuerpo;
            break;
          case 'layout.' + tipo + '.file.2':
            item.cambio = item.valor == $scope.actual.cuerpo.cedible ? 'no' : 'si';
            item.valor = $scope.actual.cuerpo.cedible;
            break;
          case 'layout.' + tipo + '.footer.0':
            item.cambio = item.valor == $scope.actual.pie ? 'no' : 'si';
            item.valor = $scope.actual.pie;
            break;
          default:
            item.cambio = 'no';
            break;
        }
      }

      var configuraciones = JSON.parse(
        JSON.stringify($scope.configuraciones['conf-' + $scope.actual.tipo])
      );
      console.log('cargamosConfiguraciones', configuraciones);
      configuraciones.configuraciones.forEach(asignarConfiguracion);

      var cuerpoRequest = {
        origen: 'plantilla_documentos',
        configuraciones: configuraciones.configuraciones
      };
      // //console.log("guardar" ,JSON.stringify(cuerpoRequest));
      FebosAPI.cl_adm_configuracion_guardar({}, cuerpoRequest)
        .then(function(response) {
          console.log('?? === >>>>  ', response);
          $scope.configuraciones['conf-' + $scope.actual.tipo] = {
            configuraciones: response.data.configuraciones
          };
          console.log('OK  ', $scope.configuraciones['conf-' + $scope.actual.tipo]);
          $scope.cargarConfiguracionesActual(
            $scope.configuraciones['conf-' + $scope.actual.tipo],
            $scope.actual.tipo
          );
        })
        .catch(function(reason) {
          console.log('error', reason);
        })
        .finally(function() {
          $scope.guardando = false;
        });
    };
    $scope.cargarConfiguracionesActual = function(configuraciones, tipo) {
      function asignarConfiguracion(item, index) {
        switch (item.parametroId) {
          case 'layout.' + tipo + '.tipoHoja.0':
            $scope.actual.hoja = item.valor; //actual.hoja
            break;
          case 'layout.' + tipo + '.margenSuperior.0':
            $scope.actual.margen_superior = item.valor; //
            break;
          case 'layout.' + tipo + '.margenInferior.0':
            $scope.actual.margen_inferior = item.valor; //
            break;
          case 'layout.' + tipo + '.header.0':
            $scope.actual.cabecera = item.valor; //
            break;
          case 'layout.' + tipo + '.file.0':
            $scope.actual.cuerpo = item.valor; //
            break;
          case 'layout.' + tipo + '.file.2':
            $scope.actual.cedible = item.valor; //
            break;
          case 'layout.' + tipo + '.footer.0':
            $scope.actual.pie = item.valor; //
            break;
        }
      }

      console.log('cargamosConfiguraciones', configuraciones);
      configuraciones.configuraciones.forEach(asignarConfiguracion);
    };
    $scope.esActual = function(comparar) {
      if ($scope.actual) {
        if (comparar) {
          if ($scope.actual.tipo == comparar.tipo) {
            return true;
          }
        }
      }
      return false;
    };

    $scope.cargarDocumentoS3 = function(pro, st) {
      console.log('p', pro);
      st = {
        width: pro + '%'
      };
      console.log('st', st);
    };
    $scope.descargarDocumentoS3 = function(documento, nombre) {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Descargando archivo..." +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_obtener_archivo_privado(
        {
          path: documento,
          nombre: nombre,
          descargable: 'si'
        },
        undefined,
        false,
        true
      )
        .then(function(response) {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
          if (response['data']['codigo'] != 10) {
          } else {
            /* var a = document.createElement("a");
               a.href = response['data']['url']+';charset=utf-8,';
               a.target = '_blank';
               a.download = nombre+'';
               document.body.appendChild(a);
               a.click();*/
            window.open(
              'https://s3.amazonaws.com/' +
                response['data']['url'].replace('http://', '').replace('https://', ''),
              true
            );
          }
        })
        .catch(function(reason) {
          console.log('error', reason);
        })
        .finally(function() {
          $rootScope.blockModal.hide();
        });
    };

    function obtenerConfiguracionesPorTipoDocumento(tipo) {
      var patt1 = /\[\[tipo\]\]/gm;

      var configuraciones =
        'layout.[[tipo]].header.0' +
        '|' +
        'layout.[[tipo]].file.0' +
        '|' +
        'layout.[[tipo]].file.2' +
        '|' + //cedible
        'layout.[[tipo]].footer.0' +
        '|' +
        'layout.[[tipo]].tipoHoja.0' +
        '|' +
        'layout.[[tipo]].margenSuperior.0' +
        '|' +
        'layout.[[tipo]].margenInferior.0';
      return configuraciones.replace(patt1, tipo);
    }
  }
]);
