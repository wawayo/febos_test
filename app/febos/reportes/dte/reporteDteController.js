angular.module('febosApp').controller('reporteDteCtrl', [
    '$sce',
    '$rootScope',
    '$scope',
    '$interval',
    '$timeout',
    '$location',
    'ServicioDocumentos',
    '$http',
    'utils',
    '$state',
    '$window',
    'SesionFebos',
    'FebosAPI',
    'FebosUtil',
    function ($sce, $rootScope, $scope, $interval, $timeout, $location, ServicioDocumentos, $http, utils, $state, $window, SesionFebos, FebosAPI, FebosUtil) {
        var hoy = new Date();
        var hace60Dias = new Date();
        hace60Dias.setDate(hoy.getDate() - 60);
        var desde = FebosUtil.formatearFecha(hace60Dias);
        var hasta = FebosUtil.formatearFecha(hoy);

        if (typeof funcionesDte == 'undefined') {
            var funcionesDte = {};
        }

        funcionesDte.scope = $scope;
        funcionesDte.rootScope = $rootScope;
        funcionesDte.http = $http;
        funcionesDte.generandoReporte = "false";

        $scope.funcionesDte = funcionesDte;
        //$scope.arregloSeleccion;
        var camposSeleccionados;

        var form = $("#example-advanced-form").show();

        $scope.rut;

        // -- función que activa modal para cargar nuevo documento XML
        $scope.mostrarModalCargaDocumento = function () {
           UIkit.modal('#mdFormularioComentarios-2').show();
        },

        $scope.config = {
            "ambito": "reportes",

            "filtrosPorDefecto": {
                "fechaRecepcion": desde + "--" + hasta,
                "tipoDocumento": "33,34,43,46,56,61"
            },
            "filtrosEstaticos": {
                "rutReceptor": SesionFebos().empresa.iut
            },
            filtros: {},
            "orden": ["-fechaEmision"],
            "tiposDeDocumentosHabilitados":
                [   
                    {valor: '33', nombre: 'Factura (E) Afecta'},
                    {valor: '34', nombre: 'Factura (E) Exenta'},
                    {valor: '39', nombre: 'Boleta (E) Afecta'},
                    {valor: '41', nombre: 'Boleta (E) Exenta'},
                    {valor: '43', nombre: 'Facturas (E) de Liquidación'},
                    {valor: '46', nombre: 'Facturas (E) de Compra'},
                    {valor: '52', nombre: 'Guía (E) de Despacho'},
                    {valor: '56', nombre: 'Nota (E) Débito'},
                    {valor: '61', nombre: 'Nota (E) Crédito'} ,
                    {valor: '110', nombre: 'Factura (E) de Exportación'},
                    {valor: '111', nombre: 'Factura (E) de Débito de Exportación'},
                    {valor: '112', nombre: 'Factura (E) de Crédito de Exportación'}
                ],
            "tiposCuentasPago":[
                {valor:'CT',nombre:'Cta corriente'},
                {valor:'AH',nombre:'Cta ahorro'},
                {valor:'OT',nombre:'Otra'}
            ],
            "tiposDespacho":[
                {valor:'1',nombre:'Despacho por cuenta del receptor del documento'},
                {valor:'2',nombre:'Despacho por cuenta del emisor a instalaciones del cliente'},
                {valor:'3',nombre:'Despacho por cuentas del emisor a otras instalaciones'}
            ],
            "tiposImpresion":[
                {valor:'N',nombre:'Normal'},
                {valor:'T',nombre:'Ticket'}
            ],
            "cedidos":[
                {valor:'si',nombre:'Si'},
                {valor:'no',nombre:'No'}
            ],
            "tiposImpuesto":[
                {valor:'14',nombre:'IVA de margen de comercialización'},
                {valor:'15',nombre:'IVA retenido total'},
                {valor:'17',nombre:'IVA anticipado faenamiento carne'},
                {valor:'18',nombre:'IVA anticipado carne'},
                {valor:'19',nombre:'IVA anticipado harina'},
                {valor:'23',nombre:'Impuesto adicional Art 37Letras a,b,c'},
                {valor:'24',nombre:'Impuesto articulo 42 Ley 825/74 letra a'},
                {valor:'25',nombre:'Impuesto articulo 42 letra c'},
                {valor:'26',nombre:'Impuesto articulo 42 letra c'},
                {valor:'27',nombre:'Impuesto articulo 42 letra d y e'},
                {valor:'28',nombre:'Impuesto especifico diesel'},
                {valor:'30',nombre:'IVA retenido legumbres'},
                {valor:'31',nombre:'IVA retenido silvestres'},
                {valor:'32',nombre:'IVA retenido ganado'},
                {valor:'33',nombre:'IVA retenido madera'},
                {valor:'34',nombre:'IVA retenido trigo'},
                {valor:'35',nombre:'Impuesto especifico gasolina'},
                {valor:'36',nombre:'IVA retenido arroz'},
                {valor:'37',nombre:'IVA retenido hidrobiologicos'},
                {valor:'38',nombre:'IVA retenido chatarra'},
                {valor:'39',nombre:'IVA retenido ppa'},
                {valor:'41',nombre:'IVA retenido contruccion'},
                {valor:'44',nombre:'IMPUESTO ADICIONAL Art 37 Letras e, h, I, l '},
                {valor:'45',nombre:'IMPUESTO ADICIONAL Art 37 Letras j '},
                {valor:'46',nombre:'IVA retenido oro'},
                {valor:'47',nombre:'IVA retenido cartones'},
                {valor:'48',nombre:'IVA retenido frambruesas '},
                {valor:'49',nombre:'FACTURA DE COMPRA SIN RETENCIÓN (hoy utilizada sólo por Bolsa de Productos de Chile, lo cual es validado por el sistema) '},
                {valor:'50',nombre:'IVA de margen de comercialización de instrumentos de prepago '},
                {valor:'51',nombre:'Impuesto gas natural comprimido; 1,93 UTM/KM3, Art. 1°, Ley N° 20.052 '},
                {valor:'52',nombre:'Impuesto gas licuado de petróleo; 1,40 UTM/M3, Art 1°, Ley N° 20.052 '},
                {valor:'53',nombre:'Impuesto Retenido Suplementeros Art 74 N°5 Ley de la Renta '}

            ],
            "tiposDescuento": [
                {valor:'pesos',nombre:'pesos'},
                {valor:'porcentaje',nombre:'porcentaje'},
            ],
            "tiposRecargo": [
                {valor:'$',nombre:'$'},
                {valor:'%',nombre:'%'}
            ],
            "tiposMovimiento": [
                {valor:'C',nombre:'Comisión'},
                {valor:'O',nombre:'Otros cargos'}
            ],
            "estadosSii": [
                {valor:'1',nombre:'Pendiente de envio al sii'},
                {valor:'2',nombre:'Enviado al sii'},
                {valor:'3',nombre:'Error al enviar'},
                {valor:'4',nombre:'Aceptado por el sii'},
                {valor:'5',nombre:'Aceptado con reparos por el sii'},
                {valor:'6',nombre:'Rechazado por el sii'},
                {valor:'7',nombre:'Pendiente de consulta en el sii'},
                {valor:'8',nombre:'Error al consultar en el sii'}
            ],
            "estadosComerciales": [
                {valor:'0',nombre:'Sin Acción'},
                {valor:'1',nombre:'Aceptado'},
                {valor:'2',nombre:'Aceptado en el SII'},
                {valor:'3',nombre:'Pre-Rechazado'},
                {valor:'4',nombre:'Rechazado en el SII'},
                {valor:'5',nombre:'Reclamo parcial en el SII'},
                {valor:'6',nombre:'Reclamo total en el SII'},
                {valor:'7',nombre:'Recibo de mercaderias en el SII'}
            ],
            "campos": [],
            "selectize": {
                plugins: {
                    'remove_button': {
                        label: ''
                    }
                },
                maxItems: null,
                valueField: 'valor',
                labelField: 'nombre',
                searchField: ['valor', 'nombre'],
                create: false,
                render: {
                    option: function (data, escape) {
                        return  '<div class="option">' +
                            '<span class="title">' + escape(data.nombre) + '</span>' +
                            '</div>';
                    },
                    item: function (data, escape) {
                        return '<div class="item">' + escape(data.nombre) + '</a></div>';
                    }
                }
            }
        };

        //estructura del arbol
        $("#tree3").fancytree({
            checkbox: true,
            selectMode: 2,
            icon: false, // Display node icons
            generateIds: true, // Generate id attributes like <span id='fancytree-id-KEY'>
            idPrefix: "data",
            expanded:"true",
            source: {
                url: "app/febos/reportes/dte/arbolSeleccionDte.json",
                cache: false
            },
            init: function(event, data) {
                // Set key from first part of title (just for this demo output)
                data.tree.visit(function(n) {
                    n.key = n.title.split(" ")[0];
                });
                $scope.rut = SesionFebos().empresa.iut;
            },
            select: function(event, data) {
                // Display list of selected nodes
                var selNodes = data.tree.getSelectedNodes();
                // convert to title/key array
                var selKeys = $.map(selNodes, function(node){
                    return node.data.id;
                });
                $("#echoSelection3").text(selKeys.join(","));
                camposSeleccionados = selKeys;

            },
        });

        /*
        $("#example-basic").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true
        });
*/


        $scope.confirmaGeneracionReporte = function (index) {
            modal = UIkit.modal.confirm('Desea generar el reporte?', function(){
                $scope.generaReporte(index);
            });
        };

        $scope.limpiarFiltros = function(tipoFiltro){
            if(tipoFiltro == 'fechas'){
                $scope.config.filtros.fechaRecepcion.desde = "";
                $scope.config.filtros.fechaRecepcion.hasta = "";
                $scope.config.filtros.fechaEmision.desde = "";
                $scope.config.filtros.fechaEmision.hasta = "";
                delete $scope.config.filtros.fechaRecepcion;
                delete $scope.config.filtros.fechaEmision;
            }

            if(tipoFiltro == 'tipos'){
                $scope.config.filtros.tipoDocumento = "";
                $scope.config.filtros.estadoSii = "";
                $scope.config.filtros.estadoComercial = "";
                $scope.config.filtros.rutCesionario = "";
                delete $scope.config.filtros.tipoDocumento;
                delete $scope.config.filtros.estadoSii;
                delete $scope.config.filtros.estadoComercial;
                delete $scope.config.filtros.rutCesionario;
            }

            if(tipoFiltro == 'rut'){
                $scope.config.filtros.rutEmisor[0] = "";
                delete $scope.config.filtros.rutEmisor;
            }

        }

        $scope.generaReporte = function(index){
            //$scope.config
            $scope.config.pagina=index;
            $scope.config.campos = camposSeleccionados;
            funcionesDte.generandoReporte = "true";
            ServicioDocumentos.generarDocumentosReporte($scope.config); //< llama función listar documentos
            funcionesDte.generandoReporte = "false";
        };

    }]);


