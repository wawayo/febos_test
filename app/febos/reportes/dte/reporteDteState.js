febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.reporte", {
                            url: "/:app/reportes/dte",
                            templateUrl: 'app/febos/reportes/dte/reporteDte.html',
                            controller: 'reporteDteCtrl',
                            resolve: {
                                EstadoAnterior: [
                                    "$state",
                                    function ($state) {
                                        var currentStateData = {
                                            estado: $state.current.name,
                                            parametros: $state.params,
                                            url: $state.href($state.current.name, $state.params)
                                        };
                                        return currentStateData;
                                    }
                                ],
                                Autorizacion: ['Autorizacion', function (Autorizacion) {
                                        try {
                                            Autorizacion.verificar();
                                        } catch (e) {
                                            console.log("Error al autenticar", e);
                                        }
                                        return;
                                    }],
                                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_countUp',
                                            'lazy_charts_peity',
                                            'lazy_charts_easypiechart',
                                            'lazy_charts_metricsgraphics',
                                            'lazy_charts_chartist',
                                            'lazy_weathericons',
                                            'lazy_google_maps',
                                            'lazy_clndr',
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'lazy_tree',
                                            'lazy_wizard',
                                            'app/febos/reportes/dte/reporteDteController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Dashboard'
                            },
                            ncyBreadcrumb: {
                                label: 'Home'
                            }
                        })

            }]);
