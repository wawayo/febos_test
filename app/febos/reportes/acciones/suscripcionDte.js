

if (typeof funcionesDte == 'undefined') {
    var funcionesDte = {};
}

funcionesDte.suscripcionData = {
    "tiposDeEvento": [   
        {"nombre": 'RECEPCION_ARCHIVO_INTEGRACION_TXT', "valor": '1'},
        {"nombre": 'RECEPCION_ARCHIVO_INTEGRACION_XML', "valor": '2'},
        {"nombre": 'ERROR_INTEGRACION', "valor": '3'},
        {"nombre": 'DOCUMENTO_PREPROCESADO', "valor": '4'},
        {"nombre": 'DOCUMENTO_FIRMADO', "valor": '5'},
        {"nombre": 'DOCUMENTO_ENVIADO_EAI', "valor": '6'},
        {"nombre": 'DOCUMENTO_RECEPCIONADO_EAI', "valor": '7'},
        {"nombre": 'ERROR_AL_ENVIAR_A_EAI', "valor": '8'},
        {"nombre": 'ERROR_RESPUESTA_EAI', "valor": '9'},
        {"nombre": 'DOCUMENTO_ACEPTADO_EAI', "valor": '10'},
        {"nombre": 'DOCUMENTO_ACEPTADO_REPAROS_EAI', "valor": '11'},
        {"nombre": 'DOCUMENTO_RECHAZADO_EAI', "valor": '12'},
        {"nombre": 'DOCUMENTO_ENVIADO_A_RECEPTOR', "valor": '13'},
        {"nombre": 'DOCUMENTO_ENVIADO_A_DIRECCION_PERSONALIZADA', "valor": '14'},
        {"nombre": 'DOCUMENTO_RECIBIDO_POR_RECEPTOR', "valor": '15'},
        {"nombre": 'ENVIO_RECHAZADO_POR_RECEPTOR', "valor": '16'},
        {"nombre": 'DOCUMENTO_ACEPTADO_COMERCIALEMENTE_POR_RECEPTOR', "valor": '17'},
        {"nombre": 'DOCUMENTO_ACEPTADO_REPAROS_COMERCIALEMENTE_POR_RECEPTOR', "valor": '18'},
        {"nombre": 'DOCUMENTO_RECHAZADO_COMERCIALEMENTE_POR_RECEPTOR', "valor": '19'},
        {"nombre": 'ENVIO_RECIBO_DE_MERCADERIAS', "valor": '20'},
        {"nombre": 'ENVIO_RECIBO_DE_MERCADERIAS_PARCIAL', "valor": '21'},
        {"nombre": 'DEVOLUCION_TOTAL_DE_MERCADERIAS', "valor": '22'},
        {"nombre": 'DEVOLUCION_PARCIAL_DE_MERCADERIAS', "valor": '23'},
        {"nombre": 'COMENTARIO_DE_USUARIO', "valor": '24'},
        {"nombre": 'ASIGNACION_FOLIO', "valor": '25'},
        {"nombre": 'ASIGNACION_CAF', "valor": '26'},
        {"nombre": 'ALMACENAMIENTO_XML_DOCUMENTO', "valor": '27'},
        {"nombre": 'VISUALIZACION_XML', "valor": '28'},
        {"nombre": 'VISUALIZACION_BASICA', "valor": '29'},
        {"nombre": 'VISUALIZACION_IMAGEN_TIPO_0', "valor": '30'},
        {"nombre": 'VISUALIZACION_IMAGEN_TIPO_1', "valor": '31'},
        {"nombre": 'VISUALIZACION_IMAGEN_TIPO_2', "valor": '32'},
        {"nombre": 'VISUALIZACION_IMAGEN_TIPO_3', "valor": '33'},
        {"nombre": 'VISUALIZACION_IMAGEN_TIPO_4', "valor": '34'},
        {"nombre": 'VISUALIZACION_IMAGEN_TIPO_5', "valor": '35'},
        {"nombre": 'VISUALIZACION_IMAGEN_TIPO_6', "valor": '36'},
        {"nombre": 'VISUALIZACION_IMAGEN_TIPO_7', "valor": '37'},
        {"nombre": 'RECEPCION_DTE_SIN_VERIFICAR_EN_SII', "valor": '38'},
        {"nombre": 'DTE_RECIBIDO_ACEPTADO_POR_EL_SII', "valor": '39'},
        {"nombre": 'DTE_RECIBIDO_RECHAZADO_POR_EL_SII', "valor": '40'},
        {"nombre": 'ALMACENAMIENTO_ADJUNTO_DOCUMENTO', "valor": '41'},
        {"nombre": 'DOCUMENTO_CONSULTADO_EAI', "valor": '42'},
        {"nombre": 'SOLICITUD_DTE_NO_RECIBIDO', "valor": '43'},
        {"nombre": 'DOCUMENTO_CEDIDO', "valor": '44'},
        {"nombre": 'CESION_RECHAZADA', "valor": '45'},
        {"nombre": 'INFORMACION_DE_PAGO', "valor": '46'},
        {"nombre": 'DTE_RECIBIDO_EN_VALIDACION', "valor": '47'}            
    ],
    "usuarioNombre": "",
    "verboEnviar": "Enviar",
    "cargandoSuscripciones": false,
    "cargandoEnviarSuscripciones": false,
    "suscripciones": {
        "ninterno": false,
        "ncorreo": false,
        "ntelegram": false
    },
    "tipo": "",
    "documento": {},
    "enviar": true,
    "webhookId": "",
    "selectize": {
        plugins: {
            'remove_button': {
                label: ''
            }
        },
        maxItems: 1,
        valueField: 'valor',
        labelField: 'nombre',
        searchField: ['valor', 'nombre'],
        create: false,
        render: {
            option: function (data, escape) {
                return  '<div class="option">' +
                        '<span class="title">' + escape(data.nombre) + '</span>' +
                        '</div>';
            },
            item: function (data, escape) {
                return '<div class="item">' + escape(data.nombre) + '</a></div>';
            }
        }
    }    
};

funcionesDte.suscripcion = [];

funcionesDte.suscripcionModal = function (documento) {
    console.log("dentre a suscripcion");
    funcionesDte.suscripcionData.enviar = true;
    funcionesDte.suscripcionData.webhookId = "";
    funcionesDte.suscripcionData.ntelegram = false;
    funcionesDte.suscripcionData.ncorreo = false;
    funcionesDte.suscripcionData.ninterno = false;
    funcionesDte.suscripcionData.tipo = 0;     
    funcionesDte.suscripcionData.cargandoSuscripciones = true;
    funcionesDte.suscripcionData.documento = documento;
    funcionesDte.suscripcionData.usuarioNombre = funcionesDte.febosSingleton.usuario.nombre 
    console.log("funcionesDte.suscripcionData.documento:");
    console.log(funcionesDte.suscripcionData.documento);
    var req = {
        method: 'GET',
        url: funcionesDte.febosSingleton.api + "/webhook/"+funcionesDte.suscripcionData.documento.febosId+"/suscripciones/usuario"
    };
    funcionesDte.http(req).then(function (response) {
        console.log("response /webhook/{id}/suscripciones/usuario:", response);
        funcionesDte.suscripcionData.cargandoSuscripciones = false;
        funcionesDte.suscripcion = response.data.suscripciones;
        if (funcionesDte.suscripcion.length >0){
            funcionesDte.suscripcionData.webhookId = funcionesDte.suscripcion[0].webhookId;
            funcionesDte.suscripcionData.enviar = false;
            funcionesDte.suscripcionData.ntelegram = funcionesDte.suscripcion[0].notificarPorTelegram==="Y"?true:false;
            funcionesDte.suscripcionData.ncorreo = funcionesDte.suscripcion[0].notificarPorCorreo==="Y"?true:false;
            funcionesDte.suscripcionData.ninterno = funcionesDte.suscripcion[0].notificarPorInterno==="Y"?true:false;
            funcionesDte.suscripcionData.tipo = response.data.tipo;            
        }
    }, function (response) {
        console.log(response);

    });

/*    for(var i=0;i<funcionesDte.suscripcion.length;i++){
        funcionesDte.suscripcionData.ninterno = funcionesDte.suscripcion.
    }
*/
    UIkit.modal('#modalSuscripciones').show();   

};

funcionesDte.cancelarSuscripcion = function () {
    UIkit.modal('#modalSuscripciones').hide();   
}

funcionesDte.enviarSuscripcion = function (){

    funcionesDte.suscripcionData.verboEnviar = "Enviando";
    funcionesDte.suscripcionData.cargandoEnviarSuscripciones = true;
    console.log("funcionesDte.febosSingleton:");
    console.log(funcionesDte.febosSingleton);

    if (funcionesDte.suscripcionData.enviar){
        console.log("crear");
        var req = {
            method: 'POST',
            url: funcionesDte.febosSingleton.api + "/webhook/",
            data: {
                "nombre": funcionesDte.suscripcionData.documento.febosId,
                "tipoEvento": funcionesDte.suscripcionData.tipo,
                "url": "",
                "tokenWebhook": "",
                "usuario": funcionesDte.febosSingleton.usuario.id,
                "clave": "",
                "flujo": "",
                "activo": "si",
                "reintentos": 3,
               // "xml": $scope.datos.modificarWebhook.,
                "correos": funcionesDte.febosSingleton.usuario.correo,
                "notificarPorCorreo": funcionesDte.suscripcionData.ncorreo?"si":"no",
                "notificarPorInterno": funcionesDte.suscripcionData.ninterno?"si":"no",
                "notificarPorTelegram": funcionesDte.suscripcionData.ntelegram?"si":"no"
            }
        };
        console.log("funcionesDte.suscripcionData.documento:")
        console.log(funcionesDte.suscripcionData.documento)
        funcionesDte.http(req).then(function (response) {
            console.log("response:", response);
            webhookId = response.data.webhookId;
            console.log("webhookid: "+webhookId);
            
            //crear regla
            var req2 = {
                method: 'POST',
                url: funcionesDte.febosSingleton.api + "/webhook/"+webhookId+"/reglas",
                data: {
                    "nombre": "Regla de un documento tributario electrónico",
                    "condicion": funcionesDte.febosSingleton.base64.encode("dte.Encabezado.IdDoc.TipoDTE == "+funcionesDte.suscripcionData.documento.tipoDocumento+" && dte.Encabezado.IdDoc.Folio == "+funcionesDte.suscripcionData.documento.folio+" && dte.Encabezado.Emisor.RUTEmisor == '"+funcionesDte.suscripcionData.documento.rutEmisor+"'"),
                    "continuar": "no",  
                    "activa": "si",
                    "prioridad": "5",
                    "descripcion": "Regla de suscripcion a un documento" 
                }
            };
            funcionesDte.http(req2).then(function (response2) {
                console.log("response2:", response2);
                    //$scope.config.verboCrear = "Crear";
                    UIkit.modal('#modalSuscripciones').hide();   
                    UIkit.notify("<i class=‘uk-icon-check’></i> Suscripcion realizada!", {status: 'success', timeout: 5000, pos: 'top-center'});
                    funcionesDte.suscripcionData.verboEnviar = "Enviar";
                    funcionesDte.suscripcionData.cargandoEnviarSuscripciones = false;

            }, function (response2) {});
        }, function (response) {});         
    }
    else {
        console.log("modificar");

        var req = {
            method: 'POST',
            url: funcionesDte.febosSingleton.api + "/webhook/"+funcionesDte.suscripcionData.webhookId,
            data: {
                "tipoEvento": funcionesDte.suscripcionData.tipo
            }
        };
        console.log("funcionesDte.suscripcionData.documento:")
        console.log(funcionesDte.suscripcionData.documento)
        funcionesDte.http(req).then(function (response) {
            console.log("response:", response);
            webhookId = response.data.webhookId;
            console.log("webhookid: "+webhookId);
            
            //modificar suscripcion

            var req = {
                method: 'POST',
                url: funcionesDte.febosSingleton.api + "/webhook/"+funcionesDte.suscripcionData.webhookId+"/suscripciones",
                data: {
                    "notificarPorCorreo": funcionesDte.suscripcionData.ncorreo?"si":"no",
                    "notificarPorInterno": funcionesDte.suscripcionData.ninterno?"si":"no",
                    "notificarPorTelegram": funcionesDte.suscripcionData.ntelegram?"si":"no"
                }
            };
            console.log("funcionesDte.suscripcionData.documento:")
            console.log(funcionesDte.suscripcionData.documento)
            funcionesDte.http(req).then(function (response) {
                UIkit.modal('#modalSuscripciones').hide();   
                UIkit.notify("<i class=‘uk-icon-check’></i> Suscripcion realizada!", {status: 'success', timeout: 5000, pos: 'top-center'});
                funcionesDte.suscripcionData.verboEnviar = "Enviar";
                funcionesDte.suscripcionData.cargandoEnviarSuscripciones = false;
                
            }, function (response) {});   

        }, function (response) {});  
    }
       
}