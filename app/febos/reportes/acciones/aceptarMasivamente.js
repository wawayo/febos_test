if (typeof funcionesDte == 'undefined') {
    var funcionesDte = {};
}
funcionesDte.scope.vincularSii = false;
 
funcionesDte.aceptarMasivamente = function () {
    console.log("aceptarMasivamente");
    funcionesDte.scope.loadingAceptarMasivoBoton = false;

    setTimeout(function () {
        modal = UIkit.modal(angular.element('#mdlAceptarMasivo'), {bgclose: false});
        modal.show();
    }, 50);

}


funcionesDte.aceptarMasivamenteAccion = function (seleccionFilas) {
    console.log("aceptarMasivamenteAccion");
    console.log(funcionesDte.scope.vincularSii);
    funcionesDte.scope.loadingAceptarMasivoBoton = true;

    var objetoAuxiliarPayload = [];

    //convertir seleccionFiles a json
    for(nombreIndice in seleccionFilas) {
        objetoAuxiliarPayload.push({"febosId":nombreIndice});         
    }

    var req = {
        method: 'POST',
        url: funcionesDte.febosSingleton.api + "/herramientas/masivas",
        data: {
            'accion': "cl_generar_intercambio_electronico",
            'nombre': 'Aprobar Documento en SII',
            'comunes': {    
                'tipoAccion': "ACD",
                'vincularAlSii': funcionesDte.scope.vincularSii ? 'si' : 'no'
                },
            'payload': objetoAuxiliarPayload
        }
    };


    funcionesDte.http(req).then(function (response) {
        funcionesDte.scope.loadingAceptarMasivoBoton = false;
        modal = UIkit.modal(angular.element('#mdlAceptarMasivo'), {bgclose: false});
        modal.hide();

        UIkit.notify("<i class='uk-icon-check'></i> Aceptación masiva exitosa", {
                status: 'success',
                timeout: 3000,
                pos: 'top-center'
            });
    }, function (response) {
        console.log(response);
        funcionesDte.rootScope.ultimoSeguimientoId = response.data.seguimientoId;
    });    


}

