if (typeof funcionesDte == 'undefined') {
    var funcionesDte = {};
}
funcionesDte.verBitacoraComoLista = false;
funcionesDte.seguimiento = "";
funcionesDte.trazaCorreo = {
    "destino": "",
    "eventos": []
}
funcionesDte.bitacora = [];
funcionesDte.adjuntos = [];
funcionesDte.url_xml = [];
funcionesDte.textoBitacora = 'Cargando...';
funcionesDte.bitacora = [];
funcionesDte.adjuntos = [];
funcionesDte.scope = {};
funcionesDte.rootScope = {};
funcionesDte.cargando = false;
funcionesDte.documentoInfo = {};
funcionesDte.trazaCorreoError=false;
funcionesDte.consultarCorreo = function (mailId) {
    console.log("CONSULTANDO " + mailId);
    funcionesDte.trazaCorreo = {
        "destino": "",
        "eventos": []
    };
    UIkit.modal('#mdTraza', {modal: false}).show();
    var url = "https://api.febos.io/produccion/correo?mailid=" + mailId;
    var req = {
        method: 'GET',
        url: url
    }
    funcionesDte.http(req).then(function (response) {
        funcionesDte.trazaCorreoError=false;
        funcionesDte.trazaCorreo = {
            "destino": response.data.para,
            "eventos": response.data.registros
        };
    }, function (response) {
        funcionesDte.trazaCorreoError=true;
        console.log("RESPUESTA CORREO", response);
    });
}

funcionesDte.verBitacora = function (documento) {
    console.log(funcionesDte.scope.DOCUMENTOS);
    console.log(documento);
    var id = documento.febosId;
    funcionesDte.documentoInfo = documento;
    console.log(documento);
    funcionesDte.bitacora = [];
    funcionesDte.adjuntos = [];
    funcionesDte.url_xml = [];
    funcionesDte.textoBitacora = 'Cargando...';
    funcionesDte.cargando = true;
    funcionesDte.adjuntos = [];

    UIkit.modal('#mdBitacora', {modal: false}).show();

    var req = {
        method: 'GET',
        url: funcionesDte.febosSingleton.api + "/documentos/" + id + "/bitacora",
        params: {
            "pagina": '1',
            "filas": '100'
        }
    }

    funcionesDte.http(req).then(function (response) {
        console.log(response);
        //$rootScope.ultimoSeguimientoId = response.data.seguimientoId;

        try {
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                funcionesDte.textoBitacora = 'BITACORA';

                for (var i = 0; i < response.data.bitacora.length; i++) {
                    var row = {
                        usuario: '',
                        mensaje: '',
                        fecha: '',
                        seguimientoId: '',
                        externalId: '',
                        bitacoraId: '',
                        documentoId: '',
                        tipo: '',
                        correoId: ''

                    }

                    row.usuario = (typeof response.data.bitacora[i].usuarioNombre != "undefined" ? response.data.bitacora[i].usuarioNombre : "?");
                    row.mensaje = (typeof response.data.bitacora[i].mensaje != "undefined" ? response.data.bitacora[i].mensaje : "?");
                    row.fecha = (typeof response.data.bitacora[i].fecha != "undefined" ? response.data.bitacora[i].fecha : "?");
                    row.seguimientoId = (typeof response.data.bitacora[i].seguimientoId != "undefined" ? response.data.bitacora[i].seguimientoId : "?");
                    row.externalId = (typeof response.data.bitacora[i].externalId != "undefined" ? response.data.bitacora[i].externalId : "?");
                    row.bitacoraId = (typeof response.data.bitacora[i].bitacoraId != "undefined" ? response.data.bitacora[i].bitacoraId : "?");
                    row.documentoId = (typeof response.data.bitacora[i].documentoId != "undefined" ? response.data.bitacora[i].documentoId : "?");
                    row.tipo = response.data.bitacora[i].tipo;
                    row.correoId = (typeof response.data.bitacora[i].correoId != "undefined" ? response.data.bitacora[i].correoId : "?");

                    funcionesDte.bitacora.push(row);
                }

                var req2 = {//< request de documentos adjuntos
                    method: 'GET',
                    url: funcionesDte.febosSingleton.api + "/documentos/" + id + "/adjuntos",
                    params: {
                        //'link': 'si'
                    }
                };

                funcionesDte.http(req2).then(function (response2) {
                    console.log(response2);
                    //$rootScope.ultimoSeguimientoId = response2.data.seguimientoId;
                    funcionesDte.cargando = false;

                    try {
                        if (response2.data.codigo === 10 || typeof response2.data.codigo === '10') {
                            funcionesDte.adjuntos = [];
                            funcionesDte.textoBitacora = 'BITACORA';

                            for (var i = 0; i < response2.data.adjuntos.length; i++) {
                                var row = {
                                    documentoId: '',
                                    tipo: '',
                                    nombre: '',
                                    dteArchivoId: ''
                                };

                                row.documentoId = (typeof response2.data.adjuntos[i].documentoId != "undefined" ? response2.data.adjuntos[i].documentoId : "?");
                                row.tipo = (typeof response2.data.adjuntos[i].tipo != "undefined" ? response2.data.adjuntos[i].tipo : "?");
                                row.nombre = (typeof response2.data.adjuntos[i].nombre != "undefined" ? response2.data.adjuntos[i].nombre : "?");
                                row.dteArchivoId = (typeof response2.data.adjuntos[i].dteArchivoId != "undefined" ? response2.data.adjuntos[i].dteArchivoId : "?");

                                funcionesDte.adjuntos.push(row);
                            }

                        } else {
                            funcionesDte.textoBitacora = 'BITACORA';
                            funcionesDte.febosSingleton.error(response2.data.codigo, response2.data.mensaje, response2.data.seguimientoId);
                        }

                    } catch (e) {
                        console.log(e);
                        funcionesDte.textoBitacora = 'BITACORA';
                        funcionesDte.cargando = false;
                        funcionesDte.febosSingleton.error(response2.data.codigo, e.message, response2.data.seguimientoId);
                    }

                }, function (response2) {
                    console.log(response2);
                    funcionesDte.funcionesDte.textoBitacora = 'BITACORA';
                    //$rootScope.ultimoSeguimientoId = response2.data.seguimientoId;
                    funcionesDte.cargando = false;
                    funcionesDte.febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');
                });


            } else {
                funcionesDte.textoBitacora = 'BITACORA';
                funcionesDte.febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
            }

        } catch (e) {
            console.log(e);
            funcionesDte.textoBitacora = 'BITACORA';
            funcionesDte.febosSingleton.error(response.data.codigo, e.message, response.data.seguimientoId);
        }

    }, function (response) {
        console.log(response);
        funcionesDte.textoBitacora = 'BITACORA';
        //$rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        funcionesDte.febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');
    });
};

funcionesDte.verFormularioBitacora = function(){
        UIkit.modal('#mdFormularioComentarios').show();
}