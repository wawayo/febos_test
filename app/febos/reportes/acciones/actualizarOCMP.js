if (typeof funcionesDte == 'undefined') {
    var funcionesDte = {};
}

funcionesDte.actualizarOCMP = function (documento) {


    modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Actualizando Orden de Compra desde Mercado Público...<br/><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');
    modal.show();
    var req = {
        method: 'PUT',
        url: funcionesDte.febosSingleton.api + "/mercadopublico",
        params: {
            'codigoMP': documento.numero
        }
    }

    funcionesDte.http(req).then(function (response) {
        modal.hide();

        funcionesDte.rootScope.ultimoSeguimientoId = response.data.seguimientoId;

        try {
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {

                UIkit.notify("<i class='uk-icon-check'></i> "+ response.data.mensaje, {
                   status: 'success',
                   timeout: 5000,
                   pos: 'top-center'
                });

            } else {
                funcionesDte.febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
            }

        } catch (e) {
            console.log(e);
            funcionesDte.febosSingleton.error(response.data.codigo, e.message, response.data.seguimientoId);
        }

    }, function (response) {
        funcionesDte.consultandoSii = false;
        console.log(response);
        funcionesDte.rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        funcionesDte.febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');
    });

}