if (typeof funcionesDte == 'undefined') {
    var funcionesDte = {};
}
funcionesDte.scope.vincularSii = false;
 
funcionesDte.rechazarMasivamente = function () {
    console.log("rechazarMasivamente");
    funcionesDte.scope.loadingRechazarMasivoBoton = false;

    setTimeout(function () {
        modal = UIkit.modal(angular.element('#mdlRechazarMasivo'), {bgclose: false});
        modal.show();
    }, 50);

}


funcionesDte.rechazarMasivamenteAccion = function (seleccionFilas) {
    console.log("rechazarMasivamenteAccion");
    console.log(funcionesDte.scope.vincularSii);
    funcionesDte.scope.loadingRechazarMasivoBoton = true;

    var objetoAuxiliarPayload = [];

    //convertir seleccionFiles a json
    for(nombreIndice in seleccionFilas) {
        objetoAuxiliarPayload.push({"febosId":nombreIndice});         
    }

    var req = {
        method: 'POST',
        url: funcionesDte.febosSingleton.api + "/herramientas/masivas",
        data: {
            'accion': "cl_generar_intercambio_electronico",
            'nombre': 'Rechazar Documento en SII',
            'comunes': {    
                'tipoAccion': funcionesDte.scope.reclamo,
                'vincularAlSii': funcionesDte.scope.vincularSii ? 'si' : 'no',
                'motivo': funcionesDte.scope.motivo
                //
                },
            'payload': objetoAuxiliarPayload
        }
    };


    funcionesDte.http(req).then(function (response) {
        funcionesDte.scope.loadingRechazarMasivoBoton = false;
        modal = UIkit.modal(angular.element('#mdlRechazarMasivo'), {bgclose: false});
        modal.hide();

        UIkit.notify("<i class='uk-icon-check'></i> Rechazo masivo exitoso", {
                status: 'success',
                timeout: 3000,
                pos: 'top-center'
            });
    }, function (response) {
        console.log(response);
        funcionesDte.rootScope.ultimoSeguimientoId = response.data.seguimientoId;
    });    


}

