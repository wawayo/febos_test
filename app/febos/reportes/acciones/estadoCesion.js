if (typeof funcionesDte == 'undefined') {
    var funcionesDte = {};
}

funcionesDte.consultarEstadoCesionEnSii = function (documento) {
    funcionesDte.historialSii = [];

    funcionesDte.consultandoSii = true;
    UIkit.modal('#mdlConsultaCesionSii').show();
    var req = {
        method: 'GET',
        url: funcionesDte.febosSingleton.api + "/sii/dte/eventos",
        params: {
            'febosId': documento.febosId
        }
    }

    console.log(req);
    funcionesDte.estaCedido=false;
    funcionesDte.http(req).then(function (response) {
        console.log(response);
        funcionesDte.consultandoSii = false;
        funcionesDte.rootScope.ultimoSeguimientoId = response.data.seguimientoId;

        try {
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                if (typeof response.data.eventos != 'undefined') {
                    funcionesDte.historialSii = response.data.eventos;
                } else {
                    funcionesDte.historialSii = [];
                }
                
                for(i=0;i>funcionesDte.historialSii.length;i++){
                    if(funcionesDte.historialSii[i].codEvento==='CED'){
                        funcionesDte.estaCedido=true;
                    }
                }

            } else {
                funcionesDte.febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
            }

        } catch (e) {
            console.log(e);
            funcionesDte.febosSingleton.error(response.data.codigo, e.message, response.data.seguimientoId);
        }

    }, function (response) {
        funcionesDte.consultandoSii = false;
        console.log(response);
        funcionesDte.rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        funcionesDte.febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');
    });

}