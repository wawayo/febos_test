funcionesDte.suscripcion = {
    "usuarioNombre": funcionesDte.febosSingleton.usuario.nombre,
    "verboEnviar": "Enviar",
    "cargandoEnviarSuscripciones": false,
    "suscripciones": {
        "ninterno": false,
        "ncorreo": false,
        "ntelegram": false
    }
};

if (typeof funcionesDte == 'undefined') {
    var funcionesDte = {};
}
funcionesDte.suscripcion = function (documento) {


    UIkit.modal('#modalSuscripciones').show();   

};

funcionesDte.enviarSuscripcion = function (){

    funcionesDte.suscripcion.verboEnviar = "Enviando";
    funcionesDte.suscripcion.cargandoEnviarSuscripciones = true;

    funcionesDte.documento=documento;
    console.log(documento);

    var req = {
        method: 'POST',
        url: funcionesDte.febosSingleton.api + "/webhook/",
        data: {
            "nombre": "Suscripcion a documento ",
            "tipoEvento": 0,
            "url": "",
            "tokenWebhook": "",
            "usuario": "",
            "clave": "",
            "flujo": "",
            "activo": "si",
            "reintentos": 3,
            "formato": "JSON",
           // "xml": $scope.datos.modificarWebhook.,
            "correos": funcionesDte.febosSingleton.usuario.correo,
            "notificarPorCorreo": funcionesDte.suscripcion.ncorreo?"si":"no",
            "notificarPorInterno": funcionesDte.suscripcion.ninterno?"si":"no",
            "notificarPorTelegram": funcionesDte.suscripcion.ntelegram?"si":"no"
        }
    };
    funcionesDte.http(req).then(function (response) {
        console.log("response:", response);
        $scope.limpiarDatosModificarWebhook();
        webhookId = response.data.webhookId;
        console.log("webhookid: "+webhookId);
        
        //crear regla
        var req2 = {
            method: 'POST',
            url: funcionesDte.febosSingleton.api + "/webhook/"+webhookId+"/reglas",
            data: {
                "nombre": "Suscripcion a un documento",
                "condicion": "",
                "continuar": "no",
                "activa": "si",
                "prioridad": "5",
                "descripcion": "Regla de suscripcion a un documento" 
            }
        };
        funcionesDte.http(req2).then(function (response2) {
            console.log("response2:", response2);
                //$scope.config.verboCrear = "Crear";
                UIkit.modal('#modalSuscripciones').hide();   
                UIkit.notify("<i class=‘uk-icon-check’></i> Suscripcion realizada!", {status: 'success', timeout: 5000, pos: 'top-center'});
                $scope.suscripcion.verboEnviar = "Enviar";
                $scope.suscripcion.cargandoEnviarSuscripciones = false;

        }, function (response2) {});
    }, function (response) {});        
}