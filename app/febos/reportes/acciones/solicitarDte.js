if (typeof funcionesDte == 'undefined') {
    var funcionesDte = {};
}

funcionesDte.solicitarDteModal = function (documento) {
    funcionesDte.documento = documento;
    UIkit.modal('#mdlSolicitarDte').show();
}
funcionesDte.cargando = false;
funcionesDte.datos={
    contacto:"",
    copias:"",
    recibirCopia:"no",
    mensaje:""
};
funcionesDte.solicitarDte = function () {
    funcionesDte.cargando = true;
    var req = {
        method: 'PUT',
        url: funcionesDte.febosSingleton.api + "/documentos/" + funcionesDte.documento.febosId + "/solicitudes",
        params: {
            'contacto': funcionesDte.datos.contacto,
            'copias': funcionesDte.datos.copias,
            'recibirCopia': funcionesDte.datos.recibirCopia,
            'mensaje': btoa(funcionesDte.datos.mensaje)
        }
    }

    console.log(req);

    funcionesDte.http(req).then(function (response) {
        console.log(response);
        funcionesDte.cargando = false;
        funcionesDte.rootScope.ultimoSeguimientoId = response.data.seguimientoId;

        try {
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                UIkit.modal('#mdlSolicitarDte').hide();
                UIkit.modal.alert("DTE Solicitado!");
            } else {
                funcionesDte.febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
            }
        } catch (e) {
            console.log(e);
            funcionesDte.febosSingleton.error(response.data.codigo, e.message, response.data.seguimientoId);
        }

    }, function (response) {
        console.log(response);
        funcionesDte.cargando = false;
        funcionesDte.rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');
    });
    
    
funcionesDte.datos={
    contacto:"",
    copias:"",
    recibirCopia:"no",
    mensaje:""
};

}