if (typeof funcionesDte == 'undefined') {
    var funcionesDte = {};
}
funcionesDte.scope.vincularSii = false;
 
funcionesDte.recibirMercaderiaMasivamente = function () {
    console.log("recibirMercaderia");
    funcionesDte.scope.loadingRecibirMercaderiaMasivoBoton = false;

    setTimeout(function () {
        modal = UIkit.modal(angular.element('#mdlReciboMercaderiaMasivo'), {bgclose: false});
        modal.show();
    }, 50);

}


funcionesDte.recibirMercaderiaMasivamenteAccion = function (seleccionFilas) {
    console.log("recibirMercaderiaMasivamenteAccion");
    console.log(funcionesDte.scope.vincularSii);
    funcionesDte.scope.loadingRecibirMercaderiaMasivoBoton = true;

    var objetoAuxiliarPayload = [];

    //convertir seleccionFiles a json
    for(nombreIndice in seleccionFilas) {
        objetoAuxiliarPayload.push({"febosId":nombreIndice});         
    }

    var req = {
        method: 'POST',
        url: funcionesDte.febosSingleton.api + "/herramientas/masivas",
        data: {
            'accion': "cl_generar_intercambio_electronico",
            'nombre': 'Rechazar Documento en SII',
            'comunes': {    
                'tipoAccion': "ERM",
                'vincularAlSii': funcionesDte.scope.vincularSii ? 'si' : 'no',
                'recinto': funcionesDte.scope.recinto
                //
                },
            'payload': objetoAuxiliarPayload
        }
    };


    funcionesDte.http(req).then(function (response) {
        funcionesDte.scope.loadingRecibirMercaderiaMasivoBoton = false;
        modal = UIkit.modal(angular.element('#mdlReciboMercaderiaMasivo'), {bgclose: false});
        modal.hide();

        UIkit.notify("<i class='uk-icon-check'></i> Rechazo masivo exitoso", {
                status: 'success',
                timeout: 3000,
                pos: 'top-center'
            });
    }, function (response) {
        console.log(response);
        funcionesDte.rootScope.ultimoSeguimientoId = response.data.seguimientoId;
    });    


}

