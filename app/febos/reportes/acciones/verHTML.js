if (typeof funcionesDte == 'undefined') {
    var funcionesDte = {};
}

funcionesDte.verHtml = function (id) {
    console.log(id);
    console.log('ver Html');

    funcionesDte.url_pdf = [];
    funcionesDte.cargando.e = true;
    funcionesDte.textoVerPdf = 'Cargando ...';

    var req = {
        method: 'GET',
        url: funcionesDte.febosSingleton.api + "/documentos/" + id,
        params: {
            "imagen": "si",
            "regenerar": "si",
            "incrustar": "no",
            "tipoImagen":1
        }
    };
    funcionesDte.block = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Generando representación impresa...<br/><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');
    funcionesDte.http(req).then(function (response) {
        funcionesDte.block.hide();
        console.log(response);
        funcionesDte.rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        funcionesDte.cargando.e = false;

        try {
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                window.open(response.data.imagenLink,"_blank");
                funcionesDte.textoVerPdf = 'PDF';
            } else {
                funcionesDte.textoVerPdf = 'PDF';
                funcionesDte.febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
            }

        } catch (e) {
            funcionesDte.textoVerPdf = 'PDF';
            funcionesDte.febosSingleton.error(response.data.codigo, e.message, response.data.seguimientoId);
        }

    }, function (response) {
        funcionesDte.block.hide();
        console.log(response);
        funcionesDte.textoVerPdf = 'PDF';
        funcionesDte.rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        funcionesDte.cargando.e = false;
        funcionesDte.febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');
    });
};
// --

funcionesDte.verHtmlDnt = function (id) {
    console.log(id);
    console.log('ver Html');

    funcionesDte.url_pdf = [];
    funcionesDte.cargando.e = true;
    funcionesDte.textoVerPdf = 'Cargando ...';

    var req = {
        method: 'GET',
        url: funcionesDte.febosSingleton.api + "/notributarios/" + id,
        params: {
            "imagen": "si",
            "regenerar": "si",
            "incrustar": "no",
            "tipoImagen":1
        }
    };
    funcionesDte.block = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Generando representación impresa...<br/><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');
    funcionesDte.http(req).then(function (response) {
        funcionesDte.block.hide();
        console.log(response);
        funcionesDte.rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        funcionesDte.cargando.e = false;

        try {
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                window.open(response.data.imagenLink,"_blank");
                funcionesDte.textoVerPdf = 'PDF';
            } else {
                funcionesDte.textoVerPdf = 'PDF';
                funcionesDte.febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
            }

        } catch (e) {
            funcionesDte.textoVerPdf = 'PDF';
            funcionesDte.febosSingleton.error(response.data.codigo, e.message, response.data.seguimientoId);
        }

    }, function (response) {
        funcionesDte.block.hide();
        console.log(response);
        funcionesDte.textoVerPdf = 'PDF';
        funcionesDte.rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        funcionesDte.cargando.e = false;
        funcionesDte.febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');
    });
};
