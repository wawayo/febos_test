if (typeof funcionesDte == 'undefined') {
    var funcionesDte = {};
}

funcionesDte.enviarDteModal = function (documento) {
    console.log("enviarDteModal");
    funcionesDte.documento = documento;
    UIkit.modal('#mdlEnviarDte').show();
}
funcionesDte.cargando = false;
funcionesDte.datos={
    contacto:"",
    copias:"",
    recibirCopia:"no",
    mensaje:""
};
funcionesDte.enviarDte = function () {
    funcionesDte.cargando = true;
    var req = {
        method: 'POST',
        url: funcionesDte.febosSingleton.api + "/documentos/" + funcionesDte.documento.febosId + "/envio",
        data: {
            'destinatario': funcionesDte.datos.destinatario,
            'copias': funcionesDte.datos.copias,
            'recibirCopia': funcionesDte.datos.recibirCopia,
            'mensaje': btoa(funcionesDte.datos.mensaje),
            'adjuntarXml': funcionesDte.datos.adjuntarXML,
            'adjuntarPdf': funcionesDte.datos.adjuntarPDF
        }
    }

    console.log(req);

    funcionesDte.http(req).then(function (response) {
        console.log(response);
        funcionesDte.cargando = false;
        funcionesDte.rootScope.ultimoSeguimientoId = response.data.seguimientoId;

        try {
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                UIkit.modal('#mdlEnviarDte').hide();
                UIkit.modal.alert("Documento enviado!");
            } else {
                funcionesDte.febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
            }
        } catch (e) {
            console.log(e);
            funcionesDte.febosSingleton.error(response.data.codigo, e.message, response.data.seguimientoId);
        }

    }, function (response) {
        console.log(response);
        funcionesDte.cargando = false;
        funcionesDte.rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');
    });
    
    
funcionesDte.datos={
    contacto:"",
    copias:"",
    recibirCopia:"no",
    mensaje:""
};

}