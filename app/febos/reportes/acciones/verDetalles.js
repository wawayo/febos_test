    if (typeof funcionesDte == 'undefined') {
    var funcionesDte = {};
}
funcionesDte.modalVerDetalles = function (documento) {
    
    funcionesDte.documento=documento;
    funcionesDte.loadingReferencias = true;
    
    //formateo de rut
    var nuevoRut = "";
    var largo = funcionesDte.documento.rutEmisor.length;
    if(largo == 9){
        nuevoRut = funcionesDte.documento.rutEmisor.substring(0,1)+"."+funcionesDte.documento.rutEmisor.substring(1,4)+"."+funcionesDte.documento.rutEmisor.substring(4);
    }
    if (largo == 10){
        nuevoRut = funcionesDte.documento.rutEmisor.substring(0,2)+"."+funcionesDte.documento.rutEmisor.substring(2,5)+"."+funcionesDte.documento.rutEmisor.substring(5);       
    }
    if (largo == 11){
        nuevoRut = funcionesDte.documento.rutEmisor.substring(0,3)+"."+funcionesDte.documento.rutEmisor.substring(3,6)+"."+funcionesDte.documento.rutEmisor.substring(6);       
    }
    funcionesDte.documento.rutEmisor = nuevoRut;
    
    //formateo monto

    funcionesDte.documento.montoTotal = funcionesDte.documento.montoTotal.toLocaleString('de-DE');

    
    console.log(documento);

    var req = {
        method: 'GET',
        url: funcionesDte.febosSingleton.api + "/documentos/" + documento.febosId + "/referencias"
    };

    funcionesDte.http(req).then(function (response) {
        console.log("referencia:");
        console.log(response.data);
        console.log("config:");
        console.log(funcionesDte.scope.config);

        funcionesDte.documento.referenciasDte=response.data.referenciasTipoDte;
        funcionesDte.documento.referenciasDnt=response.data.referenciasTipoDnt;

        funcionesDte.loadingReferencias = false;
        //$rootScope.ultimoSeguimientoId = response.data.seguimientoId;
    });

        
    UIkit.modal('#mdVerDetalles').show();
};
