if (typeof funcionesDte == 'undefined') {
    var funcionesDte = {};
}
funcionesDte.verBitacoraComoLista = false;
funcionesDte.seguimiento = "";
funcionesDte.trazaCorreo = {
    "destino": "",
    "eventos": []
}
funcionesDte.bitacora = [];
funcionesDte.adjuntos = [];
funcionesDte.url_xml = [];
funcionesDte.textoBitacora = 'Cargando...';
funcionesDte.bitacora = [];
funcionesDte.adjuntos = [];
funcionesDte.scope = {};
funcionesDte.rootScope = {};
funcionesDte.cargando = false;
funcionesDte.documentoInfo = {};
funcionesDte.trazaCorreoError=false;
funcionesDte.tienepermiso = false;
funcionesDte.cargandoInformacion = false;
funcionesDte.actualizandoInformacion = false;
funcionesDte.consultarCorreo = function (mailId) {
    console.log("CONSULTANDO " + mailId);
    funcionesDte.trazaCorreo = {
        "destino": "",
        "eventos": []
    };
    UIkit.modal('#mdTraza', {modal: false}).show();
    var url = "https://api.febos.io/produccion/correo?mailid=" + mailId;
    var req = {
        method: 'GET',
        url: url
    }
    funcionesDte.http(req).then(function (response) {
        funcionesDte.trazaCorreoError=false;
        funcionesDte.trazaCorreo = {
            "destino": response.data.para,
            "eventos": response.data.registros
        };
    }, function (response) {
        funcionesDte.trazaCorreoError=true;
        console.log("RESPUESTA CORREO", response);
    });
}

funcionesDte.verInfoPago = function (documento) {
    funcionesDte.cargandoInformacion = true;
    console.log(funcionesDte.scope.DOCUMENTOS);
    console.log(documento);
    var id = documento.febosId;
    funcionesDte.documentoInfo = documento;
    console.log(documento);
    funcionesDte.bitacora = [];
    funcionesDte.adjuntos = [];
    funcionesDte.url_xml = [];
    funcionesDte.textoBitacora = 'Cargando...';
    funcionesDte.cargando = true;
    funcionesDte.adjuntos = [];
    var cosa = Number("123123");
    console.log("MONTO1:");
    console.log(cosa.toLocaleString('de-DE'));

    UIkit.modal('#mdVerInformacionPago', {modal: false}).show();

    for(var i = 0; i < funcionesDte.febosSingleton.permiso.length; i++){
        if(funcionesDte.febosSingleton.permiso[i] == 'DTE19'){
            funcionesDte.tienePermiso = true;
        }
    }

    var req = {
        method: 'GET',
        url: funcionesDte.febosSingleton.api + "/documentos/" + id + "/pagos", //TODO id solo de prueba
        params: {
            "pagina": '1',
            "filas": '100'
        }
    }


    funcionesDte.http(req).then(function (response) {
        console.log(response);
        
        //$rootScope.ultimoSeguimientoId = response.data.seguimientoId;

        try {
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                if(response.data.totalElementos > 0){
                    funcionesDte.documentoInfo.tipo = (typeof response.data.infoPago[0].tipo != "undefined" ? response.data.infoPago[0].tipo : "?");
                    funcionesDte.documentoInfo.montoInfoPago = (typeof response.data.infoPago[0].monto != "undefined" ? Number(response.data.infoPago[0].monto) : "?");
                    funcionesDte.documentoInfo.lugar = (typeof response.data.infoPago[0].lugar != "undefined" ? response.data.infoPago[0].lugar : "?");
                    funcionesDte.documentoInfo.comentario = (typeof response.data.infoPago[0].comentario != "undefined" ? response.data.infoPago[0].comentario : "?");
                    funcionesDte.documentoInfo.fecha = (typeof response.data.infoPago[0].fecha != "undefined" ? response.data.infoPago[0].fecha : "?");
                    funcionesDte.documentoInfo.medio = (typeof response.data.infoPago[0].medio != "undefined" ? response.data.infoPago[0].medio : "?");
                    funcionesDte.cargandoInformacion = false;
                }else{
                    funcionesDte.cargandoInformacion = false;
                }
            } else {
                funcionesDte.febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                funcionesDte.cargandoInformacion = false;
            }

        } catch (e) {
            console.log(e);
            funcionesDte.febosSingleton.error(response.data.codigo, e.message, response.data.seguimientoId);
            funcionesDte.cargandoInformacion = false;
        }

    }, function (response) {
        console.log(response);
        funcionesDte.febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');
        funcionesDte.cargandoInformacion = false;
    });
};

funcionesDte.verFormularioActualizaInfoPago = function(){
        UIkit.modal('#mdFormularioActualizaInfoPago').show();
}