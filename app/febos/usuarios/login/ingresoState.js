febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("ingreso", {
                    url: "/:app/ingreso",
                    templateUrl: 'app/febos/usuarios/login/ingresoView.html',
                    controller: 'ingresoCtrl',
                    reloadOnSearch: false,
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                console.log("ingreso!");
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_uikit',
                                    'lazy_iCheck',
                                    'app/febos/usuarios/login/ingresoController.js'
                                ]);
                            }]
                    },
                    data: {
                        pageTitle: 'Ingreso'
                    }
                })

            }]);