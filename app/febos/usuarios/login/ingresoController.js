angular.module('febosApp').controller('ingresoCtrl', [
  '$scope',
  '$rootScope',
  'utils',
  'FebosAPI',
  'FebosUtil',
  'SesionFebos',
  'FebosQueueAPI',
  'ContextoFebos',
  '$location',
  '$stateParams',
  '$window',
  '$http',
  '$state',
  function(
    $scope,
    $rootScope,
    utils,
    FebosAPI,
    FebosUtil,
    SesionFebos,
    FebosQueueAPI,
    ContextoFebos,
    $location,
    $stateParams,
    $window,
    $http,
    $state
  ) {
    if (window.location.href.includes('#/undefined')) {
      $state.go('ingreso', { app: 'cloud' });
    }

    $scope.intentoLogin = false;

    $scope.registerFormActive = false;
    $scope.datos = {
      correo: '',
      clave: ''
    };

    if ($location.absUrl().includes('code=')) {
      var code = $location.absUrl() ? $location.absUrl().match(/code=([^&|^#]*)/) : null;
      $scope.code = code && code.length > 0 ? code[1] : null;

      var state = $location.absUrl() ? $location.absUrl().match(/state=([^&|^#]*)/)[1] : null;
      $scope.state = state && state.length > 0 ? state[1] : null;

      var url = '';
      if (FebosUtil.obtenerDominio() === 'localhost') {
        url = '/#' + $location.path();
      } else {
        url = '/' + FebosUtil.obtenerAmbiente() + '/web#' + $location.path();
      }
      //window.history.replaceState("", "Febos :: Ingreso", url);
    }

    // $http.get("http://febos.cl/feed/").then(function(response){
    //     console.log(response);
    // });

    if ($scope.state && $scope.code) {
      var body = {
        accion: 'obtenerTokenAutenticacion',
        codigoClaveUnica: $scope.code,
        tokenEstado: $scope.state
      };

      $location.search({});
      FebosAPI.cl_clave_unica({}, body, true, true).then(
        function(response) {
          if (response.data.codigo == 10) {
            FebosUtil.cargarInformacionDeIngreso(response.data);
            SesionFebos().usuario.ultimoPingValido = new Date();
            SesionFebos().proveedor = {};
            SesionFebos().cliente = {};

            if (
              SesionFebos().usuario.permisos == 'undefined' ||
              SesionFebos().usuario.permisos.length == 0
            ) {
              var sPageURL = $location.path();
              var new_url = sPageURL.substring(0, sPageURL.indexOf('?'));
              // alert($location.absUrl().replace(/(\?)(.*)(\#)+/g,"#"))
              // $window.location.href =$location.absUrl().replace(/(\?)(.*)(\#)+/g,"#");
              $window.location.href =
                $location.absUrl().replace(/(\?)(.*)+/g, '#') +
                '/' +
                $stateParams.app +
                '/cargando';
              $location.path(new_url + '/' + $stateParams.app + '/cargando');
            } else {
              //recarga de permisos en background
              FebosQueueAPI('cl_listar_permiso_usuario', {}, {}).then(function(response) {
                SesionFebos().usuario.permisos = response.data.permisos;
              });
              FebosQueueAPI(
                'cl_generar_menu',
                {
                  dominio: FebosUtil.obtenerDominio(),
                  app: FebosUtil.obtenerAmbito()
                },
                {}
              ).then(function(response) {
                SesionFebos().menu[FebosUtil.obtenerAmbito() + '-' + FebosUtil.obtenerDominio()] =
                  response.data.menu;
              });
              FebosQueueAPI(
                'cl_listar_empresas',
                {
                  pagina: 1,
                  filas: 10000,
                  filtro: $scope.filtro,
                  busquedaSimple: 'si',
                  ambito: FebosUtil.obtenerAmbito()
                },
                {}
              ).then(function(response) {
                SesionFebos().misEmpresas = response.data.empresas;
              });
              //$rootScope.blockModal.hide();
              //$locations.search({});

              $location.path('/' + $stateParams.app + '/empresas/seleccion');
            }
          } else {
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        } /*, function(err){
                        //console.log("error en la llamada cl_clave_unica",err.data.codigo, err.data.mensaje, err.data.seguimientoId);
                        SesionFebos().error(err.data.codigo, err.data.mensaje, err.data.seguimientoId);
                  }*/
      );
    }

    var $login_card = $('#login_card'),
      $login_form = $('#login_form'),
      $login_help = $('#login_help'),
      $login_error = $('#login_error'),
      $register_form = $('#register_form'),
      $login_password_reset_ok = $('#login_password_reset_ok'),
      $login_password_reset = $('#login_password_reset');

    // show login form (hide other forms)
    var login_form_show = function() {
      $login_form
        .show()
        .siblings()
        .hide();
    };

    // show register form (hide other forms)
    var register_form_show = function() {
      $register_form
        .show()
        .siblings()
        .hide();
    };

    // show login help (hide other forms)
    var login_help_show = function() {
      $login_help
        .show()
        .siblings()
        .hide();
    };

    // show password reset form (hide other forms)
    var password_reset_show = function() {
      $login_password_reset
        .show()
        .siblings()
        .hide();
    };

    $scope.loginClaveUnica = function($event) {
      var img =
        "<img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>";
      $rootScope.blockModal = UIkit.modal.blockUI(
        '<div id="login-modal" class=\'uk-text-center\'>Redireccionando a login clave única...<br/>' +
          img +
          '</div>'
      );
      var body = {
        accion: 'generarToken'
      };
      FebosAPI.cl_clave_unica({}, body, true, false).then(function(response) {
        if (response.data.codigo == 10) {
          $window.location.href = response.data.url.replace('#', '%23');
        }
      });
    };

    $scope.loginHelp = function($event) {
      $event.preventDefault();
      utils.card_show_hide($login_card, undefined, login_help_show, undefined);
    };

    $scope.backToLogin = function($event) {
      $event.preventDefault();
      $scope.registerFormActive = false;
      utils.card_show_hide($login_card, undefined, login_form_show, undefined);
    };

    $scope.registerForm = function($event) {
      $event.preventDefault();
      $scope.registerFormActive = true;
      utils.card_show_hide($login_card, undefined, register_form_show, undefined);
    };

    $scope.passwordReset = function($event) {
      $event.preventDefault();
      utils.card_show_hide($login_card, undefined, password_reset_show, undefined);
    };

    var password_reset_show = function() {
      $login_password_reset
        .show()
        .siblings()
        .hide();
    };

    var login_error_show = function() {
      $login_error
        .show()
        .siblings()
        .hide();
    };

    var login_password_reset_ok = function() {
      $login_password_reset_ok
        .show()
        .siblings()
        .hide();
    };

    $scope.passwordResetOk = function(mensaje) {
      $scope.datos.ok = mensaje + '. El correo para restaurar su contraseña ha sido enviado.';
      utils.card_show_hide($login_card, undefined, login_password_reset_ok, undefined);
      return false;
    };

    $scope.camposVacios = function() {
      $scope.datos.correo == '';
      $scope.datos.clave == '';
      $scope.datos.correo_reset == '';
    };

    $scope.construirMensajeDeAvance = function(nombre, paso) {
      var cuerpo =
        'Hola <b>' +
        nombre +
        '</b>!<br/>por favor danos unos segundos para hacer que tu experiencia aquí sea mas agradable..<br/><br/>';
      var img =
        "<img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>";
      var enCurso = '<i class="material-icons">&#xE315;</i>';
      var procesado = '<i style="color: green" class="material-icons">&#xE86C;</i>';
      var pasos = [
        'Verificando tus permisos',
        'Revisando configuraciones locales',
        'Recuperando empresas a las que perteneces'
      ];
      for (var i = 0; i < 3; i++) {
        if (i < paso) {
          cuerpo += '<span>' + procesado + pasos[i] + '</span><br/>';
        } else if (i == paso) {
          cuerpo += '<span>' + enCurso + pasos[i] + '</span><br/>';
        } else {
          cuerpo += '<span>' + '&nbsp;&nbsp;&nbsp;' + pasos[i] + '</span><br/>';
        }
      }
      return cuerpo + img;
    };

    $scope.login = function() {
      var img =
        "<img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>";

      if ($scope.intentoLogin) {
        return;
      }
      $scope.intentoLogin = true;

      $scope.blockModalLogin = UIkit.modal.blockUI(
        '<div id="login-modal2" class=\'uk-text-center\'>Verificando credenciales...<br/>' +
          img +
          '</div>'
      );

      FebosAPI.cl_login(
        {},
        {
          correo: new String($scope.datos.correo).trim(),
          clave: $scope.datos.clave
        },
        false,
        true
      ).then(function(response) {
        if (response.data.codigo == 10) {
          FebosUtil.cargarInformacionDeIngreso(response.data);
          SesionFebos().usuario.ultimoPingValido = new Date();
          SesionFebos().proveedor = {};
          SesionFebos().cliente = {};
          $scope.intentoLogin = false;
          $scope.blockModalLogin.hide();
          // UIkit.modal('#login-modal2').hide();

          if (
            (SesionFebos().usuario.permisos == 'undefined' ||
              SesionFebos().usuario.permisos.length == 0) &&
            !response.data.usuario.cambiarClave
          ) {
            $location.path('/' + $stateParams.app + '/cargando');
          } else {
            //recarga de permisos en background
            FebosQueueAPI('cl_listar_permiso_usuario', {}, {}).then(function(response) {
              SesionFebos().usuario.permisos = response.data.permisos;
            });
            FebosQueueAPI(
              'cl_generar_menu',
              {
                dominio: FebosUtil.obtenerDominio(),
                app: FebosUtil.obtenerAmbito()
              },
              {}
            ).then(function(response) {
              SesionFebos().menu[FebosUtil.obtenerAmbito() + '-' + FebosUtil.obtenerDominio()] =
                response.data.menu;
            });
            FebosQueueAPI(
              'cl_listar_empresas',
              {
                pagina: 1,
                filas: 10000,
                filtro: $scope.filtro,
                busquedaSimple: 'si',
                ambito: 'cloud' //,FebosUtil.obtenerAmbito()
              },
              {}
            ).then(function(response) {
              SesionFebos().misEmpresas = response.data.empresas;
            });
            if (response.data.usuario.cambiarClave) {
              $state.go('especial.cambioClave', { app: $stateParams.app });
            } else {
              $location.path('/' + $stateParams.app + '/empresas/seleccion');
            }
          }
        } else {
          $scope.blockModalLogin.hide();
          UIkit.modal.alert(response.data.mensaje);
          $scope.intentoLogin = false;
        }
      });
    };

    //triggers a ejecutar antes de mostrar la pagina
    if (Object.keys($location.search()).length != 0) {
      $rootScope.logout();
    }
    $scope.datos = {
      correo: '',
      clave: '',
      correo_reset: '',
      error: '',
      ok: '',
      textoLogin: 'ENTRAR',
      textoResetear: 'RESETEAR CONTRASEÑA'
    };
    // -- función recuperar contraseña, envía los datos a la API, el cual retorna un objeto como resultado
    $scope.recuperar = function() {
      if ($scope.datos.textoResetear != 'RESETEAR CONTRASEÑA') {
        return;
      }
      if (!$scope.validarReset()) {
        return;
      }

      $scope.datos.textoResetear = 'ENVIANDO';
      FebosAPI.cl_recuperar_clave(
        {},
        {
          correo: $scope.datos.correo_reset,
          dominio: window.location.href.split('/')[2].split('.')[0],
          portal: 'cloud'
        },
        true,
        true
      ).then(function(response) {
        try {
          if (response.data.codigo == 10) {
            $scope.passwordResetOk(response.data.mensaje);
            $scope.camposVacios();
            $scope.datos.textoResetear = 'RESETEAR CONTRASEÑA';
          } else {
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
            $scope.datos.textoResetear = 'RESETEAR CONTRASEÑA';
            $scope.camposVacios();
          }
        } catch (e) {
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId
          );
          $scope.datos.textoResetear = 'RESETEAR CONTRASEÑA';
          $scope.camposVacios();
        }
      });
    };

    // -- función validar campos 'email' en el formuario de recuperar contraseña
    $scope.validarReset = function() {
      var errores = '';

      if ($scope.datos.correo_reset === '') {
        errores += '\nDebe ingresar un correo electrónico válido.';
      }

      if (errores !== '') {
        $scope.datos.error = errores;
        utils.card_show_hide($login_card, undefined, login_error_show, undefined);

        return false;
      } else {
        return true;
      }
    };
  }
]);
