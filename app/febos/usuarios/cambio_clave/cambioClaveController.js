angular
  .module('febosApp')
  .controller('cambioClaveCtrl', function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    FebosAPI,
    $http,
    utils,
    $state,
    $location,
    $stateParams
  ) {
    var $crear_card = $('#crear_card');
    var $crear_form = $('#crear_form');
    var $crear_error = $('#crear_error');
    var $crear_ok = $('#crear_ok');
    $scope.nivel = '';
    $scope.porcentaje = 0;
    var crear_form_show = function() {
      $crear_form
        .show()
        .siblings()
        .hide();
    };

    var crear_error_show = function() {
      $crear_error
        .show()
        .siblings()
        .hide();
    };

    var crear_ok_show = function() {
      $crear_ok
        .show()
        .siblings()
        .hide();
    };

    $scope.backToCrear = function($event) {
      $event.preventDefault();
      utils.card_show_hide($crear_form, undefined, crear_form_show, undefined);
    };

    $scope.datos = {
      iut: '',
      nombre: '',
      apellidos: '',
      alias: '',
      correo: '',
      clave: '',
      reclave: '',
      empresas: '',
      textoCrear: 'Cambiar',
      mensaje_error: '',
      mensaje_ok: ''
    };

    $scope.crear = function() {
      if ($scope.datos.clave != $scope.datos.reclave) {
        UIkit.modal.alert('Las contraseñas no coinciden!');
        return;
      }
      if ($scope.datos.textoCrear != 'Cambiar') return;
      if ($scope.datos.clave !== '') {
        $scope.datos.textoCrear = 'Modificando contraseña ...';
        /*
                    req = {
                        method: 'PUT',
                        url: febosSingleton.api + "/usuarios/login/recuperar",
                        data: {
                            "claveActual": $scope.datos.claveActual,
                            "claveNueva": $scope.datos.clave
                        }
                    };*/

        FebosAPI.cl_modificar_contrasena(
          {},
          {
            claveActual: $scope.datos.claveActual,
            claveNueva: $scope.datos.clave
          },
          false,
          false
        ).then(
          function(response) {
            if (response.data.codigo == 10) {
              $scope.datos.textoCrear = 'Cambiar';
              $scope.datos.mensaje_ok = response.data.mensaje;

              //utils.card_show_hide($crear_form, undefined, crear_ok_show, undefined);
              //UIkit.modal.alert("Su contraseña fue modificada correctamente");

              $location.path('/' + $stateParams.app + '/empresas/seleccion');
            } else {
              $scope.datos.textoCrear = 'Cambiar';
              //$scope.datos.mensaje_error = response.data.mensaje;
              //utils.card_show_hide($crear_form, undefined, crear_error_show, undefined);
              UIkit.modal.alert(response.data.mensaje);
            }
          },
          function(error) {
            // hacer algo con el error
            console.log(error);
            $scope.datos.textoCrear = 'Cambiar';
            // $scope.datos.mensaje_error = response.data.mensaje;
            UIkit.modal.alert(response.data.mensaje);
            // utils.card_show_hide($crear_form, undefined, crear_error_show, undefined);
          }
        );

        /*
                    $http(req).then(function (response) {
                        console.log(response);
                        try {
                            if (response.data.codigo == 10) {
                                $scope.datos.textoCrear = "Cambiar";
                                $scope.datos.mensaje_ok = response.data.mensaje;
                                $cookies.put("usuario.cambiarClave",false);
                                //utils.card_show_hide($crear_form, undefined, crear_ok_show, undefined);
                                //UIkit.modal.alert("Su contraseña fue modificada correctamente");
                                setTimeout(function(){
                                    $state.go('especial.empresa');
                                },100);
                                
                            } else {
                                $scope.datos.textoCrear = "Cambiar";
                                $scope.datos.mensaje_error = response.data.mensaje;
                                utils.card_show_hide($crear_form, undefined, crear_error_show, undefined);
                            }

                        } catch (e) {
                            console.log(e);
                            $scope.datos.textoCrear = "Cambiar";
                            $scope.datos.mensaje_error = response.data.mensaje;
                            utils.card_show_hide($crear_form, undefined, crear_error_show, undefined);
                        }

                    }, function (response) {
                        console.log(response);
                        $scope.datos.textoCrear = "Cambiar";
                        $scope.datos.mensaje_error = response.data.mensaje;
                        utils.card_show_hide($crear_form, undefined, crear_error_show, undefined);
                    });

                    */
      } else {
        UIkit.modal.alert('Favor ingrese una nueva contraseña');
      }
    };

    String.prototype.strReverse = function() {
      var newstring = '';
      for (var s = 0; s < this.length; s++) {
        newstring = this.charAt(s) + newstring;
      }
      return newstring;
      //strOrig = ' texttotrim ';
      //strReversed = strOrig.revstring();
    };

    $scope.chkPass = function(pwd) {
      console.log('analizando: ' + pwd);
      var oScorebar = $('scorebar');
      var oScore = $('score');
      var oComplexity = $('complexity');
      // Simultaneous variable declaration and value assignment aren't supported in IE apparently
      // so I'm forced to assign the same value individually per var to support a crappy browser *sigh*
      var nScore = 0,
        nLength = 0,
        nAlphaUC = 0,
        nAlphaLC = 0,
        nNumber = 0,
        nSymbol = 0,
        nMidChar = 0,
        nRequirements = 0,
        nAlphasOnly = 0,
        nNumbersOnly = 0,
        nUnqChar = 0,
        nRepChar = 0,
        nRepInc = 0,
        nConsecAlphaUC = 0,
        nConsecAlphaLC = 0,
        nConsecNumber = 0,
        nConsecSymbol = 0,
        nConsecCharType = 0,
        nSeqAlpha = 0,
        nSeqNumber = 0,
        nSeqSymbol = 0,
        nSeqChar = 0,
        nReqChar = 0,
        nMultConsecCharType = 0;
      var nMultRepChar = 1,
        nMultConsecSymbol = 1;
      var nMultMidChar = 2,
        nMultRequirements = 2,
        nMultConsecAlphaUC = 2,
        nMultConsecAlphaLC = 2,
        nMultConsecNumber = 2;
      var nReqCharType = 3,
        nMultAlphaUC = 3,
        nMultAlphaLC = 3,
        nMultSeqAlpha = 3,
        nMultSeqNumber = 3,
        nMultSeqSymbol = 3;
      var nMultLength = 4,
        nMultNumber = 4;
      var nMultSymbol = 6;
      var nTmpAlphaUC = '',
        nTmpAlphaLC = '',
        nTmpNumber = '',
        nTmpSymbol = '';
      var sAlphaUC = '0',
        sAlphaLC = '0',
        sNumber = '0',
        sSymbol = '0',
        sMidChar = '0',
        sRequirements = '0',
        sAlphasOnly = '0',
        sNumbersOnly = '0',
        sRepChar = '0',
        sConsecAlphaUC = '0',
        sConsecAlphaLC = '0',
        sConsecNumber = '0',
        sSeqAlpha = '0',
        sSeqNumber = '0',
        sSeqSymbol = '0';
      var sAlphas = 'abcdefghijklmnopqrstuvwxyz';
      var sNumerics = '01234567890';
      var sSymbols = ')!@#$%^&*()';
      var sComplexity = 'Too Short';
      var sStandards = 'Below';
      var nMinPwdLen = 8;
      if (document.all) {
        var nd = 0;
      } else {
        var nd = 1;
      }
      if (pwd) {
        nScore = parseInt(pwd.length * nMultLength);
        nLength = pwd.length;
        var arrPwd = pwd.replace(/\s+/g, '').split(/\s*/);
        var arrPwdLen = arrPwd.length;

        /* Loop through password to check for Symbol, Numeric, Lowercase and Uppercase pattern matches */
        for (var a = 0; a < arrPwdLen; a++) {
          if (arrPwd[a].match(/[A-Z]/g)) {
            if (nTmpAlphaUC !== '') {
              if (nTmpAlphaUC + 1 == a) {
                nConsecAlphaUC++;
                nConsecCharType++;
              }
            }
            nTmpAlphaUC = a;
            nAlphaUC++;
          } else if (arrPwd[a].match(/[a-z]/g)) {
            if (nTmpAlphaLC !== '') {
              if (nTmpAlphaLC + 1 == a) {
                nConsecAlphaLC++;
                nConsecCharType++;
              }
            }
            nTmpAlphaLC = a;
            nAlphaLC++;
          } else if (arrPwd[a].match(/[0-9]/g)) {
            if (a > 0 && a < arrPwdLen - 1) {
              nMidChar++;
            }
            if (nTmpNumber !== '') {
              if (nTmpNumber + 1 == a) {
                nConsecNumber++;
                nConsecCharType++;
              }
            }
            nTmpNumber = a;
            nNumber++;
          } else if (arrPwd[a].match(/[^a-zA-Z0-9_]/g)) {
            if (a > 0 && a < arrPwdLen - 1) {
              nMidChar++;
            }
            if (nTmpSymbol !== '') {
              if (nTmpSymbol + 1 == a) {
                nConsecSymbol++;
                nConsecCharType++;
              }
            }
            nTmpSymbol = a;
            nSymbol++;
          }
          /* Internal loop through password to check for repeat characters */
          var bCharExists = false;
          for (var b = 0; b < arrPwdLen; b++) {
            if (arrPwd[a] == arrPwd[b] && a != b) {
              /* repeat character exists */
              bCharExists = true;
              /* 
                                 Calculate icrement deduction based on proximity to identical characters
                                 Deduction is incremented each time a new match is discovered
                                 Deduction amount is based on total password length divided by the
                                 difference of distance between currently selected match
                                 */
              nRepInc += Math.abs(arrPwdLen / (b - a));
            }
          }
          if (bCharExists) {
            nRepChar++;
            nUnqChar = arrPwdLen - nRepChar;
            nRepInc = nUnqChar ? Math.ceil(nRepInc / nUnqChar) : Math.ceil(nRepInc);
          }
        }

        /* Check for sequential alpha string patterns (forward and reverse) */
        for (var s = 0; s < 23; s++) {
          var sFwd = sAlphas.substring(s, parseInt(s + 3));
          var sRev = sFwd.strReverse();
          if (pwd.toLowerCase().indexOf(sFwd) != -1 || pwd.toLowerCase().indexOf(sRev) != -1) {
            nSeqAlpha++;
            nSeqChar++;
          }
        }

        /* Check for sequential numeric string patterns (forward and reverse) */
        for (var s = 0; s < 8; s++) {
          var sFwd = sNumerics.substring(s, parseInt(s + 3));
          var sRev = sFwd.strReverse();
          if (pwd.toLowerCase().indexOf(sFwd) != -1 || pwd.toLowerCase().indexOf(sRev) != -1) {
            nSeqNumber++;
            nSeqChar++;
          }
        }

        /* Check for sequential symbol string patterns (forward and reverse) */
        for (var s = 0; s < 8; s++) {
          var sFwd = sSymbols.substring(s, parseInt(s + 3));
          var sRev = sFwd.strReverse();
          if (pwd.toLowerCase().indexOf(sFwd) != -1 || pwd.toLowerCase().indexOf(sRev) != -1) {
            nSeqSymbol++;
            nSeqChar++;
          }
        }

        /* Modify overall score value based on usage vs requirements */

        /* General point assignment */
        //$("nLengthBonus").innerHTML = "+ " + nScore;
        if (nAlphaUC > 0 && nAlphaUC < nLength) {
          nScore = parseInt(nScore + (nLength - nAlphaUC) * 2);
          sAlphaUC = '+ ' + parseInt((nLength - nAlphaUC) * 2);
        }
        if (nAlphaLC > 0 && nAlphaLC < nLength) {
          nScore = parseInt(nScore + (nLength - nAlphaLC) * 2);
          sAlphaLC = '+ ' + parseInt((nLength - nAlphaLC) * 2);
        }
        if (nNumber > 0 && nNumber < nLength) {
          nScore = parseInt(nScore + nNumber * nMultNumber);
          sNumber = '+ ' + parseInt(nNumber * nMultNumber);
        }
        if (nSymbol > 0) {
          nScore = parseInt(nScore + nSymbol * nMultSymbol);
          sSymbol = '+ ' + parseInt(nSymbol * nMultSymbol);
        }
        if (nMidChar > 0) {
          nScore = parseInt(nScore + nMidChar * nMultMidChar);
          sMidChar = '+ ' + parseInt(nMidChar * nMultMidChar);
        }

        /* Point deductions for poor practices */
        if ((nAlphaLC > 0 || nAlphaUC > 0) && nSymbol === 0 && nNumber === 0) {
          // Only Letters
          nScore = parseInt(nScore - nLength);
          nAlphasOnly = nLength;
          sAlphasOnly = '- ' + nLength;
        }
        if (nAlphaLC === 0 && nAlphaUC === 0 && nSymbol === 0 && nNumber > 0) {
          // Only Numbers
          nScore = parseInt(nScore - nLength);
          nNumbersOnly = nLength;
          sNumbersOnly = '- ' + nLength;
        }
        if (nRepChar > 0) {
          // Same character exists more than once
          nScore = parseInt(nScore - nRepInc);
          sRepChar = '- ' + nRepInc;
        }
        if (nConsecAlphaUC > 0) {
          // Consecutive Uppercase Letters exist
          nScore = parseInt(nScore - nConsecAlphaUC * nMultConsecAlphaUC);
          sConsecAlphaUC = '- ' + parseInt(nConsecAlphaUC * nMultConsecAlphaUC);
        }
        if (nConsecAlphaLC > 0) {
          // Consecutive Lowercase Letters exist
          nScore = parseInt(nScore - nConsecAlphaLC * nMultConsecAlphaLC);
          sConsecAlphaLC = '- ' + parseInt(nConsecAlphaLC * nMultConsecAlphaLC);
        }
        if (nConsecNumber > 0) {
          // Consecutive Numbers exist
          nScore = parseInt(nScore - nConsecNumber * nMultConsecNumber);
          sConsecNumber = '- ' + parseInt(nConsecNumber * nMultConsecNumber);
        }
        if (nSeqAlpha > 0) {
          // Sequential alpha strings exist (3 characters or more)
          nScore = parseInt(nScore - nSeqAlpha * nMultSeqAlpha);
          sSeqAlpha = '- ' + parseInt(nSeqAlpha * nMultSeqAlpha);
        }
        if (nSeqNumber > 0) {
          // Sequential numeric strings exist (3 characters or more)
          nScore = parseInt(nScore - nSeqNumber * nMultSeqNumber);
          sSeqNumber = '- ' + parseInt(nSeqNumber * nMultSeqNumber);
        }
        if (nSeqSymbol > 0) {
          // Sequential symbol strings exist (3 characters or more)
          nScore = parseInt(nScore - nSeqSymbol * nMultSeqSymbol);
          sSeqSymbol = '- ' + parseInt(nSeqSymbol * nMultSeqSymbol);
        }

        /* Determine if mandatory requirements have been met and set image indicators accordingly */
        var arrChars = [nLength, nAlphaUC, nAlphaLC, nNumber, nSymbol];
        var arrCharsIds = ['nLength', 'nAlphaUC', 'nAlphaLC', 'nNumber', 'nSymbol'];
        var arrCharsLen = arrChars.length;

        nRequirements = nReqChar;
        if (pwd.length >= nMinPwdLen) {
          var nMinReqChars = 3;
        } else {
          var nMinReqChars = 4;
        }
        if (nRequirements > nMinReqChars) {
          // One or more required characters exist
          nScore = parseInt(nScore + nRequirements * 2);
          sRequirements = '+ ' + parseInt(nRequirements * 2);
        }

        /* Determine if additional bonuses need to be applied and set image indicators accordingly */
        var arrChars = [nMidChar, nRequirements];
        var arrCharsIds = ['nMidChar', 'nRequirements'];
        var arrCharsLen = arrChars.length;

        /* Determine if suggested requirements have been met and set image indicators accordingly */
        var arrChars = [
          nAlphasOnly,
          nNumbersOnly,
          nRepChar,
          nConsecAlphaUC,
          nConsecAlphaLC,
          nConsecNumber,
          nSeqAlpha,
          nSeqNumber,
          nSeqSymbol
        ];
        var arrCharsIds = [
          'nAlphasOnly',
          'nNumbersOnly',
          'nRepChar',
          'nConsecAlphaUC',
          'nConsecAlphaLC',
          'nConsecNumber',
          'nSeqAlpha',
          'nSeqNumber',
          'nSeqSymbol'
        ];
        var arrCharsLen = arrChars.length;
        if (nScore > 100) {
          nScore = 100;
        } else if (nScore < 0) {
          nScore = 0;
        }
        if (nScore >= 0 && nScore < 20) {
          $scope.nivel = 'Muy mala contraseña, por tu seguridad usa otra';
          $('#progressbar div').css({ width: nScore + '%', 'background-color': '#ed5565' });
        } else if (nScore >= 20 && nScore < 40) {
          $scope.nivel = 'Mala contraseña, por tu seguridad usa otra';
          $('#progressbar div').css({ width: nScore + '%', 'background-color': '#f7b000' });
        } else if (nScore >= 40 && nScore < 60) {
          $scope.nivel = 'Sirve, no es mala, pero tampoco es buena';
          $('#progressbar div').css({ width: nScore + '%', 'background-color': '#ffd900' });
        } else if (nScore >= 60 && nScore < 80) {
          $scope.nivel = 'Buena contraseña, aunque podría ser vulnerada';
          $('#progressbar div').css({ width: nScore + '%', 'background-color': '#00b0ff' });
        } else if (nScore >= 80 && nScore <= 100) {
          $scope.nivel = 'Muy buena contraseña!';
          $('#progressbar div').css({ width: nScore + '%', 'background-color': '#449d44' });
        }
        $scope.porcentaje = nScore;

        /* Display updated score criteria to client */
      } else {
        /* Display default score criteria to client */
        //initPwdChk();
        //oScore.innerHTML = nScore + "%";
        //oComplexity.innerHTML = sComplexity;
      }
    };
  });
