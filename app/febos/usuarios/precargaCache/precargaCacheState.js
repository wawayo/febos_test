febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("especial.precargaCache", {
                    url: "/:app/cargando",
                    templateUrl: 'app/febos/usuarios/precargaCache/precargaCacheView.html',
                    controller: 'precargaCacheCtrl',
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_uikit',
                                    'lazy_iCheck',
                                    'app/febos/usuarios/precargaCache/precargaCacheController.js'
                                ]);
                            }]
                    },
                    data: {
                        pageTitle: 'Cargando...'
                    }
                })

            }]);