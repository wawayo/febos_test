angular
        .module('febosApp')
        .controller('precargaCacheCtrl', [
            '$scope',
            '$rootScope',
            'utils',
            'FebosAPI',
            'FebosUtil',
            'SesionFebos',
            'FebosQueueAPI',
            'ContextoFebos',
            '$location',
            '$stateParams',
            '$timeout',
            function ($scope, $rootScope, utils, FebosAPI, FebosUtil, SesionFebos, FebosQueueAPI, ContextoFebos, $location, $stateParams,$timeout) {
                $scope.sesion = SesionFebos();
                $scope.etapas = [
                    {estado: 0, nombre: "Cargando permisos"},
                    {estado: 0, nombre: "Verificando accesos"},
                    {estado: 0, nombre: "Revisando relaciones con empresas"}
                ];
                $scope.etapas[0].estado = 1;
                FebosAPI.cl_listar_permiso_usuario({},{},false, false).then(function (response) {
                    SesionFebos().usuario.permisos = response.data.permisos;
                    $scope.etapas[0].estado = 2;
                    $scope.etapas[1].estado = 1;
                    //$rootScope.blockModal = UIkit.modal.blockUI('<div id="login-modal" class=\'uk-text-center\'><ul>Hola <b>' + SesionFebos().usuario.nombre + '</b>! estamos verificando tus configuraciones, danos unos segundos...</ul><img width="30"class=\'uk-margin-top\' src=\'assets/img/logos/FEBOS_LOADER.svg\' alt=\'\'>');
                    FebosAPI.cl_generar_menu({'dominio': FebosUtil.obtenerDominio(), 'app': FebosUtil.obtenerAmbito()},{}, false, false).then(function (response) {
                        SesionFebos().menu[FebosUtil.obtenerAmbito()+"-"+FebosUtil.obtenerDominio()] = response.data.menu;
                        $scope.etapas[1].estado = 2;
                        $scope.etapas[2].estado = 1;
                        //$("#login-modal").html($scope.construirMensajeDeAvance(SesionFebos().usuario.nombre, 2));
                        FebosAPI.cl_listar_empresas({
                            'pagina': 1,
                            'filas': 10000,
                            'busquedaSimple': "si",
                            'ambito': 'cloud'//FebosUtil.obtenerAmbito() // siempre listar a las que pertenece
                        },{}, true, false).then(function (response) {
                            $scope.etapas[2].estado = 2;
                            $scope.etapas.push({"estado":0,"nombre":"redireccionando..."});
                            SesionFebos().misEmpresas = response.data.empresas;
                            $timeout(function(){
                                $location.path("/" + $stateParams.app + "/empresas/seleccion");
                            },700);
                        });
                    });
                });

            }
        ]);