angular.module('febosApp').controller('perfilCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$http',
  'utils',
  '$state',
  '$location',
  'SesionFebos',
  'FebosAPI',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $http,
    utils,
    $state,
    $location,
    SesionFebos,
    FebosAPI
  ) {
    $scope.backToCrear = function($event) {
      $scope.mensaje = 'none';
    };

    // Variables scope

    $scope.listando = false;
    $scope.cargando = false;
    $scope.mensaje = 'none';

    //Pestaña Informacion

    // -- data del usuario para cargar vista del perfil
    $scope.usuario = {
      rut: SesionFebos().usuario.iut,
      nombre: SesionFebos().usuario.nombre,
      alias: SesionFebos().usuario.alias,
      correo: SesionFebos().usuario.correo,
      id: SesionFebos().usuario.id,
      avatar: SesionFebos().usuario.avatar,
      clave: '', //SesionFebos().usuario.clave,
      textoPerfil: 'ACTUALIZAR PERFIL'
    };
    // --

    // -- función que envia datos del usuario a la API para ser modificado el perfil
    $scope.modificaPerfil = function() {
      $scope.usuario.textoPerfil = 'MODIFICANDO...';
      var idUsuario = $scope.usuario.id;

      if (!$scope.validarModificarPerfil())
        //< valida compos obligatorios
        return false;

      FebosAPI.cl_modificar_usuario(
        {
          usuarioId: $scope.usuario.id,
          simular: 'no'
        },
        {
          nombre: $scope.usuario.nombre,
          alias: $scope.usuario.alias,
          correo: $scope.usuario.correo,
          avatar: $scope.usuario.avatar
        },
        true,
        false
      ).then(function(response) {
        $scope.usuario.textoPerfil = 'ACTUALIZAR PERFIL';
        console.log(response);

        try {
          if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
            console.log('OK');
            $scope.mensaje = 'block';
            //ingresar();
          } else {
            console.log('no es codigo 10');
          }
        } catch (e) {
          console.log(e);
        }
      });

      $scope.usuario.textoPerfil = 'ACTUALIZAR PERFIL';
    };

    // -- función que activa el modal  modifica la contraseña de un usuario 'perfil'
    $scope.modificaClave = function() {
      console.log('cambio clave');
      $scope.contrasena = {
        claveA: '', //< antigua pass
        claveB: '', //< nueva pass
        claveC: '', //< repite nueva pass
        textoClave: 'ACTUALIZAR CONTRASEÑA'
      };

      var modal = UIkit.modal('#mdModificarClave');
      modal.show();
    };
    // --

    // -- función que envía la data de contraseñas para ser modificadas
    $scope.modificarContrasena = function() {
      console.log('modifica contraseña!');
      $scope.contrasena.textoClave = 'MODIFICANDO...';
      var idUsuario = SesionFebos().usuario.id;

      if ($scope.contrasena.claveC == $scope.contrasena.claveB) {
        if (!$scope.validarModificarContrasena())
          //< valida compos obligatorios
          return false;

        FebosAPI.cl_modificar_contrasena(
          {
            simular: 'no'
          },
          {
            claveActual: $scope.contrasena.claveA,
            claveNueva: $scope.contrasena.claveC
          },
          true,
          false
        ).then(function(response) {
          $scope.contrasena.textoClave = 'ACTUALIZAR CONTRASEÑA';
          console.log(response);

          try {
            if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
              console.log('OK');
              $scope.mensaje = 'block';
            } else {
              console.log('no es codigo 10');
            }
          } catch (e) {
            console.log(e);
          }
        });
      } else {
        $scope.contrasena.textoClave = 'ACTUALIZAR CONTRASEÑA';
        alert('ERROR: La nueva contraseña debe coincidir');
        return false;
      }
    };
    // --

    //Pestaña Avatar
    //"https://api.adorable.io/avatars/face/eyes5/nose3/mouth10/2cfc07"
    $scope.avatar =
      typeof SesionFebos().usuario.avatar == 'undefined' || SesionFebos().usuario.avatar == ''
        ? avatarPorDefecto
        : SesionFebos().usuario.avatar;
    $scope.work =
      typeof SesionFebos().usuario.avatar == 'undefined' || SesionFebos().usuario.avatar == ''
        ? avatarPorDefecto
        : SesionFebos().usuario.avatar;

    if ($scope.avatar != null) {
      var res = $scope.avatar.split('/');
      $scope.color = '#' + res[8];
    } else {
      $scope.color = '#2cfc07';
    }

    $scope.eye = 0;
    $scope.nose = 0;
    $scope.mouth = 0;

    var avatar_list = {
      face: {
        eyes: ['eyes1', 'eyes10', 'eyes2', 'eyes3', 'eyes4', 'eyes5', 'eyes6', 'eyes7', 'eyes9'],
        nose: ['nose2', 'nose3', 'nose4', 'nose5', 'nose6', 'nose7', 'nose8', 'nose9'],
        mouth: ['mouth1', 'mouth10', 'mouth11', 'mouth3', 'mouth5', 'mouth6', 'mouth7', 'mouth9']
      }
    };
    var avatarPorDefecto = 'https://api.adorable.io/avatars/350/' + SesionFebos().usuario.avatar;

    $scope.$watch('color', function(newValue, oldValue) {
      if (newValue != oldValue) {
        $scope.updateAvatarBuilder();
      }
    });

    $scope.updateAvatarBuilder = function() {
      var eye = avatar_list.face.eyes[$scope.eye];
      var mouth = avatar_list.face.mouth[$scope.mouth];
      var nose = avatar_list.face.nose[$scope.nose];

      $scope.avatar =
        'https://api.adorable.io/avatars/face/' +
        eye +
        '/' +
        nose +
        '/' +
        mouth +
        '/' +
        $scope.color.replace('#', '');
    };

    $scope.ojoSiguiente = function() {
      $scope.eye = $scope.eye + 1;

      if ($scope.eye >= avatar_list.face.eyes.length) {
        $scope.eye = 0;
      }

      $scope.updateAvatarBuilder();
    };

    $scope.ojoAnterior = function() {
      $scope.eye = $scope.eye - 1;

      if ($scope.eye < 0) {
        $scope.eye = avatar_list.face.eyes.length - 1;
      }

      $scope.updateAvatarBuilder();
    };

    $scope.bocaSiguiente = function() {
      $scope.mouth = $scope.mouth + 1;

      if ($scope.mouth >= avatar_list.face.mouth.length) {
        $scope.mouth = 0;
      }

      $scope.updateAvatarBuilder();
    };

    $scope.bocaAnterior = function() {
      $scope.mouth = $scope.mouth - 1;

      if ($scope.mouth < 0) {
        $scope.mouth = avatar_list.face.mouth.length - 1;
      }

      $scope.updateAvatarBuilder();
    };

    $scope.narizSiguiente = function() {
      $scope.nose = $scope.nose + 1;

      if ($scope.nose >= avatar_list.face.nose.length) {
        $scope.nose = 0;
      }

      $scope.updateAvatarBuilder();
    };

    $scope.narizAnterior = function() {
      $scope.nose = $scope.nose - 1;

      if ($scope.nose < 0) {
        $scope.nose = avatar_list.face.nose.length - 1;
      }

      $scope.updateAvatarBuilder();
    };

    $scope.thumbnail = {
      dataUrl: ''
    };

    $scope.fileReaderSupported = window.FileReader != null;
    $scope.photoChanged = function(files) {
      if (files != null) {
        var file = files[0];

        if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
          $timeout(function() {
            var fileReader = new FileReader();
            fileReader.readAsDataURL(file);
            fileReader.onload = function(e) {
              $timeout(function() {
                $scope.thumbnail.dataUrl = e.target.result;
              });
            };
          });
        }
      }
    };

    $scope.add = function() {
      var f = document.getElementById('file').files[0],
        r = new FileReader();

      r.onloadend = function(e) {
        var data = e.target.result;
        //send your binary data via $http or $resource or do anything else with it
      };

      r.readAsBinaryString(f);
    };

    //Pestaña Actividad
    $scope.filtros = [];
    $scope.filtros.paginas = [];
    $scope.filtros.fechaDesde = '';
    $scope.filtros.fechaHasta = '';
    $scope.filtros.usuarios = [];
    $scope.filtros.acciones = [];
    $scope.filtros.direccionIp = '';
    $scope.filtros.completado = '';
    $scope.filtros.pagina = '1';
    $scope.filtros.itemsPorPagina = '10';
    $scope.filtros.numeroDePaginas = 9; //< número de páginas máximo mostradas en la parte de cambio de página
    $scope.totalElementos = 0;
    $scope.totalPaginas = 0;

    $scope.actividades = new Array();

    $scope.filtros.usuariosOptions = [];
    $scope.filtros.accionesOptions = [];
    $scope.filtros.completadoOptions = [];

    // -- función que limpia los filtros de las actividades
    $scope.limpiarFiltros = function() {
      $scope.filtros.fechaDesde = '';
      $scope.filtros.fechaHasta = '';
      $scope.filtros.usuarios = [];
      $scope.filtros.acciones = [];
      $scope.filtros.direccionIp = '';
      $scope.filtros.completado = '';
    };
    // --

    $scope.filtros.usuariosConfig = {
      plugins: {
        remove_button: {
          label: ''
        }
      },
      maxItems: null,
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      create: false,
      render: {
        option: function(planets_data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(planets_data.title) +
            '</span>' +
            '</div>'
          );
        },
        item: function(planets_data, escape) {
          return '<div class="item">' + escape(planets_data.title) + '</div>';
        }
      }
    };

    $scope.filtros.accionesConfig = {
      plugins: {
        remove_button: {
          label: ''
        }
      },
      maxItems: null,
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      create: false,
      render: {
        option: function(planets_data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(planets_data.title) +
            '</span>' +
            '</div>'
          );
        },
        item: function(planets_data, escape) {
          return '<div class="item">' + escape(planets_data.title) + '</div>';
        }
      }
    };

    $scope.filtros.completadoConfig = {
      create: false,
      maxItems: 1,
      valueField: 'id',
      labelField: 'title',
      searchField: 'title'
    };

    // -- función que lista las actividades del usuario
    $scope.listarActividades = function() {
      if ($scope.listando == true) {
        return;
      }

      $scope.listando = true;
      $scope.actividades = [];

      FebosAPI.cl_listar_actividades(
        {
          simular: 'no',
          debug: 'si',
          pagina: $scope.filtros.pagina,
          itemsPorPagina: $scope.filtros.itemsPorPagina,
          fechaDesde: $scope.filtros.fechaDesde,
          fechaHasta: $scope.filtros.fechaHasta,
          usuarios: $scope.filtros.usuarios.join(','),
          acciones: $scope.filtros.acciones.join(','),
          direccionIp: $scope.filtros.direccionIp,
          completado: $scope.filtros.completado,
          usuarioId: SesionFebos().usuario.id
        },
        {},
        true,
        false
      ).then(function(response) {
        console.log(response);
        $scope.listando = false;

        try {
          $scope.actividades = [];
          if (response.data.codigo == 10 || response.data.codigo == '10') {
            for (var i = 0; i < response.data.actividades.length; i++) {
              var row = {
                actividadId: '',
                usuario: '',
                nombreUsuario: '',
                accion: '',
                ip: '',
                completado: '',
                fecha: '',
                hora: '',
                duracion: '',
                resultado: ''
              };

              row.actividadId =
                typeof response.data.actividades[i].actividadId != 'undefined'
                  ? response.data.actividades[i].actividadId
                  : '?';
              row.usuario =
                typeof response.data.actividades[i].usuario != 'undefined'
                  ? response.data.actividades[i].usuario
                  : '?';
              row.nombreUsuario =
                typeof response.data.actividades[i].nombreUsuario != 'undefined'
                  ? response.data.actividades[i].nombreUsuario
                  : '?';
              row.accion =
                typeof response.data.actividades[i].accion != 'undefined'
                  ? response.data.actividades[i].accion
                  : '?';
              row.ip =
                typeof response.data.actividades[i].ip != 'undefined'
                  ? response.data.actividades[i].ip
                  : '?';
              row.completado =
                typeof response.data.actividades[i].completado != 'undefined'
                  ? response.data.actividades[i].completado
                  : '?';
              row.fecha =
                typeof response.data.actividades[i].fecha != 'undefined'
                  ? response.data.actividades[i].fecha
                  : '?';
              row.hora =
                typeof response.data.actividades[i].hora != 'undefined'
                  ? response.data.actividades[i].hora
                  : '?';
              row.duracion =
                typeof response.data.actividades[i].duracion != 'undefined'
                  ? response.data.actividades[i].duracion
                  : '?';
              row.resultado =
                typeof response.data.actividades[i].resultado != 'undefined'
                  ? response.data.actividades[i].resultado
                  : '?';

              if (row.actividadId !== '?') $scope.actividades.push(row);
            }

            if (typeof response.data.totalElementos == 'undefined') {
              $scope.totalElementos = 0;
            } else {
              $scope.totalElementos = parseInt(response.data.totalElementos);
            }

            if (($scope.totalElementos = 0)) {
              $scope.filtros.paginas = [];
            } else {
              $scope.calcularPaginas();
            }
          } else {
            console.log('Error 1');
          }
        } catch (e) {
          console.log('Error 2');
        }
      });
    };
    // --

    // -- función que permite cambiar de página en un mantenedor
    $scope.cambiarPagina = function(index) {
      $scope.filtros.numeroDePaginas = Math.ceil($scope.filtros.numeroDePaginas);
      $scope.totalElementos = Math.ceil($scope.totalElementos);

      if (index == 'inicio') {
        $scope.filtros.pagina = 1;
      } else {
        if (index == 'final') {
          $scope.filtros.pagina = $scope.totalPaginas;
        } else {
          if (
            index !== '' &&
            typeof index != 'undefined' &&
            typeof $scope.filtros.paginas[index] != 'undefined'
          ) {
            $scope.filtros.pagina = $scope.filtros.paginas[index].numero;
          } else {
            $scope.filtros.pagina = 1;
          }
        }
      }

      $scope.listarActividades();
    };
    // --

    // -- función que calcula las páginas a mostrar en el mantenedor
    $scope.calcularPaginas = function() {
      $scope.totalPaginas = Math.ceil($scope.totalElementos / $scope.filtros.itemsPorPagina);

      var resta = -(Math.ceil($scope.filtros.numeroDePaginas) % 2 == 0
        ? Math.ceil($scope.filtros.numeroDePaginas / 2 - 1)
        : Math.ceil($scope.filtros.numeroDePaginas / 2 - 1));
      var suma =
        Math.ceil($scope.filtros.numeroDePaginas) % 2 == 0
          ? Math.ceil($scope.filtros.numeroDePaginas / 2)
          : Math.ceil($scope.filtros.numeroDePaginas / 2 - 1);
      var first = false;

      if (Math.ceil($scope.filtros.pagina) + resta <= 0) {
        first = true;
        suma -= Math.ceil($scope.filtros.pagina) + (resta - 1);
        resta -= Math.ceil($scope.filtros.pagina) + (resta - 1);
      }

      console.log($scope.totalPaginas);
      if ($scope.totalPaginas <= 0) suma = $scope.filtros.numeroDePaginas + resta - 1;

      if (
        Math.ceil($scope.filtros.pagina) + suma > $scope.totalPaginas &&
        $scope.totalPaginas > 0
      ) {
        if (first) {
          suma -= Math.ceil($scope.filtros.pagina + suma) - $scope.totalPaginas;
        } else {
          suma -= Math.ceil($scope.filtros.pagina + suma) - $scope.totalPaginas;
        }
      }

      var cont = 0;
      $scope.filtros.paginas = [];

      for (var i = $scope.filtros.pagina + resta; i <= $scope.filtros.pagina + suma; i++) {
        var nuevaPagina = {
          id: cont,
          numero: i
        };

        $scope.filtros.paginas.push(nuevaPagina);
        cont++;
      }
    };
    // --

    // -- función que valida los datos del formulario que modifica data a un perfil usuario
    $scope.validarModificarPerfil = function() {
      var errores = '';

      if ($scope.usuario.nombre == '') {
        errores += '\nDebe ingresar un Nombre.';
      }

      if ($scope.usuario.alias == '') {
        errores += '\nDebe ingresar un Alias.';
      }

      if ($scope.usuario.correo == '') {
        errores += '\nDebe ingresar un Correo Electrónico.';
      }

      if (errores != '') {
        $scope.usuario.textoPerfil = 'ACTUALIZAR PERFIL';

        alert('Errores encontrados campos obligatorios vacíos :\n' + errores);
        return false;
      } else {
        return true;
      }
    };
    // --

    // -- función que valida los datos del formulario que modifica la contraseña de un usuario 'perfil'
    $scope.validarModificarContrasena = function() {
      var errores = '';

      if ($scope.contrasena.claveA == '') {
        errores += '\nDebe ingresar un Nombre.';
      }

      if ($scope.contrasena.claveB == '') {
        errores += '\nDebe ingresar un Alias.';
      }

      if ($scope.contrasena.claveC == '') {
        errores += '\nDebe ingresar un Correo Electrónico.';
      }

      if (errores != '') {
        $scope.contrasena.textoClave = 'ACTUALIZAR CONTRASEÑA';

        alert('Errores encontrados campos obligatorios vacíos :\n' + errores);
        return false;
      } else {
        return true;
      }
    };
    // --

    // -- función que carga los permisos del usuario
    $scope.cargarElementos = function() {
      $scope.filtros.usuariosOptions = [];
      $scope.filtros.accionesOptions = [];

      $scope.filtros.completadoOptions = [
        {
          id: 'todos',
          title: 'Todos'
        },
        {
          id: 'si',
          title: 'Si'
        },
        {
          id: 'no',
          title: 'No'
        }
      ];

      FebosAPI.cl_listar_permisos(
        {
          simular: 'no',
          debug: 'si'
        },
        {},
        true,
        false
      ).then(function(response) {
        console.log(response);
        $scope.listando = false;
        try {
          $scope.filtros.accionesOptions = [];

          if (response.data.codigo == 10 || response.data.codigo == '10') {
            for (var i = 0; i < response.data.permisos.length; i++) {
              var row = {
                id: '',
                title: ''
              };

              row.id =
                typeof response.data.permisos[i].codigo != 'undefined'
                  ? response.data.permisos[i].codigo
                  : '?';
              row.title =
                typeof response.data.permisos[i].nombre != 'undefined'
                  ? response.data.permisos[i].nombre
                  : '?';

              if (row.id !== '?') $scope.filtros.accionesOptions.push(row);
            }
          } else {
            console.log('Error 1');
          }
        } catch (e) {
          console.log('Error 2');
        }
      });

      FebosAPI.cl_listar_usuarios(
        {
          simular: 'no',
          debug: 'si',
          pagina: '1',
          filas: '100' //< limite de 100 usuarios para el select.
        },
        {},
        true,
        false
      ).then(function(response) {
        console.log(response);
        $scope.listando = false;

        try {
          $scope.filtros.usuariosOptions = [];
          if (response.data.codigo == 10 || response.data.codigo == '10') {
            for (var i = 0; i < response.data.usuarios.length; i++) {
              var row = {
                id: '',
                title: ''
              };

              row.id =
                typeof response.data.usuarios[i].id != 'undefined'
                  ? response.data.usuarios[i].id
                  : '?';
              row.title =
                typeof response.data.usuarios[i].alias != 'undefined' &&
                typeof response.data.usuarios[i].nombre != 'undefined'
                  ? response.data.usuarios[i].alias + ' (' + response.data.usuarios[i].nombre + ')'
                  : '?';

              if (row.id !== '?') $scope.filtros.usuariosOptions.push(row);
            }
          } else {
            console.log('Error 1');
          }
        } catch (e) {
          console.log('Error 2');
        }
      });

      $scope.filtros.completado = 'todos';
    };
    // --

    $scope.cargarElementos(); //< carga los permisos del usuario 'select'

    $scope.cambiarPagina('inicio'); //< paginación el listado de actividades
  }
]);
