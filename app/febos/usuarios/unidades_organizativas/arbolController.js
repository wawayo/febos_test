/*globals angular*/
var ARBOL;
//console.log("CONTROLLER fuera");
angular.module('febosApp').controller('arbolUsuariosCtrl', [
  '$sce',
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$location',
  '$http',
  'utils',
  '$state',
  '$window',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  function(
    $sce,
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    $http,
    utils,
    $state,
    $window,
    SesionFebos,
    FebosAPI,
    FebosUtil
  ) {
    $scope.imprimir = function() {
      //console.log("imprime esto!!!");
    };

    //$scope.AYUDA = AYUDA;
    $scope.arbolDeGrupos = []; //arbol
    $scope.listadoDeGrupos = [];
    $scope.todosLosUsuarios = [];
    $scope.usuariosDisponibles = [];
    $scope.usuariosSeleccionados = [];
    $scope.usuariosDelGrupo = [];
    $scope.lideres = [];
    $scope.query = '';
    $scope.query2 = '';
    $scope.query3 = '';
    $scope.loaders = {
      usuarios: false,
      grupos: false,
      creandoGrupo: false,
      modificandoGrupo: false
    };
    $scope.usuarioActual = {};
    $scope.grupoActual = { grupoId: 'root' };
    $scope.grupoEditar = {};
    $scope.grupoNuevo = {};
    $scope.raiz = [];

    console.log($('#arbol-grupos'));

    ARBOL = $('#arbol-grupos').fancytree({
      checkbox: false,
      selectMode: 2,
      icon: true, // Display node icons
      generateIds: true, // Generate id attributes like <span id='fancytree-id-KEY'>
      idPrefix: 'data',
      source: [
        {
          title: SesionFebos().empresa.fantasia + ' (Todos los usuarios)',
          key: '1',
          lazy: true,
          grupoId: 'root',
          icon: 'assets/icons/others/home.svg'
        }
      ],
      init: function(event, data) {
        // Set key from first part of title (just for this demo output)
        data.tree.visit(function(n) {
          n.key = n.title.split(' ')[0];
        });
      },
      activate: function(event, data) {
        $scope.grupoActual = data.node.data;
        $scope.cargarUsuariosDeGrupo($scope.grupoActual.grupoId);
      },
      lazyLoad: function(event, data) {
        var dfd = new $.Deferred();
        data.result = dfd.promise();
        dfd.resolve($scope.raiz);
      },
      icon: function(event, data) {
        if (data.node.data.grupoId == 'root') {
          return 'assets/icons/others/home.svg';
        } else {
          return 'assets/icons/others/icon-group.svg';
        }
      },
      // The following options are only required, if we have more than one tree on one page:
      cookieId: 'fancytree-Cb2',
      idPrefix: 'fancytree-Cb2-'
    });

    $scope.cargarUsuarios = function() {
      FebosAPI.cl_listar_arbol_usuarios(
        {
          empresaId: SesionFebos().empresa.iut
        },
        true,
        false
      ).then(function(response) {
        //console.log("usuarios", response);
        $scope.loaders.usuarios = true;
        $scope.todosLosUsuarios = response.data.usuarios;
        for (var i = 0; i < $scope.todosLosUsuarios.length; i++) {
          if (typeof $scope.todosLosUsuarios[i].usuario.avatar === 'undefined') {
            $scope.todosLosUsuarios[i].usuario.avatar =
              'https://api.adorable.io/avatars/' + $scope.todosLosUsuarios[i].usuario.correo;
          }
          var ultimoIngreso = '';
          var row = {
            id: '',
            rut: '',
            nombre: '',
            alias: '',
            correo: '',
            proveedor: '',
            estado: '',
            ultimoIngreso: ''
          };

          if (
            response.data.usuarios[i].usuario.ultimoIngreso == 'null' ||
            response.data.usuarios[i].usuario.ultimoIngreso == null ||
            response.data.usuarios[i].usuario.ultimoIngreso == 'undefined' ||
            response.data.usuarios[i].usuario.ultimoIngreso == undefined
          ) {
            ultimoIngreso = '-';
          } else {
            var ingreso = response.data.usuarios[i].usuario.ultimoIngreso;
            var splitAcceso = ingreso.split(' ');
            var formatoAcceso = splitAcceso[0].split('-');
            var formato = formatoAcceso[2] + '-' + formatoAcceso[1] + '-' + formatoAcceso[0];

            ultimoIngreso = formato;
          }
          row.id = response.data.usuarios[i].usuario.id;
          row.rut = response.data.usuarios[i].usuario.iut;
          row.nombre = response.data.usuarios[i].usuario.nombre;
          row.alias = response.data.usuarios[i].usuario.alias;
          row.correo = response.data.usuarios[i].usuario.correo;
          row.proveedor = response.data.usuarios[i].usuario.proveedor;
          row.estado = response.data.usuarios[i].usuario.estado;
          row.ultimoIngreso = ultimoIngreso;

          //$scope.usuariox.push(row);
        }
        $scope.cargarUsuariosDeGrupo('root');
      });
    };

    $scope.crearGrupo = function() {
      if ($scope.loaders.creandoGrupo) return;
      $scope.loaders.creandoGrupo = true;
      if ($scope.grupoActual.grupoId != 'root') {
        $scope.grupoNuevo.padreId = $scope.grupoActual.grupoId;
      }

      FebosAPI.cl_crear_grupo(
        {
          empresaId: SesionFebos().empresa.iut
        },
        $scope.grupoNuevo,
        true,
        false
      ).then(function(response) {
        var nuevo = {
          title: $scope.grupoNuevo.nombre,
          grupoId: response.data.grupoId,
          nombre: $scope.grupoNuevo.nombre,
          codigo: $scope.grupoNuevo.codigo,
          descripcion: $scope.grupoNuevo.descripcion
        };
        $scope.listadoDeGrupos.push(nuevo);
        $scope.agregarHijoAlFancyTree(nuevo);
        UIkit.modal('#mdlCrearGrupoArbol').hide();
        UIkit.modal.alert('Muy bien! el grupo fue creado exitosamente!', {
          labels: { Ok: 'Listo!' }
        });
        $scope.loaders.creandoGrupo = false;
      });
    };
    $scope.modificarGrupo = function() {
      if ($scope.loaders.modificandoGrupo) {
        return;
      }
      $scope.loaders.modificandoGrupo = true;
      if ($scope.grupoActual.grupoId != 'root') {
        $scope.grupoNuevo.padreId = $scope.grupoActual.grupoId;
      }

      var query = {};
      query.empresaId = SesionFebos().empresa.iut;
      query.grupoId = $scope.grupoActual.grupoId;
      FebosAPI.cl_modificar_grupo(query, $scope.grupoEditar, true, false).then(function(response) {
        $scope.grupoActual.nombre = $scope.grupoEditar.nombre;
        $scope.grupoActual.codigo = $scope.grupoEditar.codigo;
        $scope.grupoActual.descripcion = $scope.grupoEditar.descripcion;
        var activeNode = $('#arbol-grupos').fancytree('getActiveNode');
        activeNode.setTitle($scope.grupoActual.nombre);
        activeNode.data.nombre = $scope.grupoActual.nombre;
        activeNode.data.codigo = $scope.grupoActual.codigo;
        activeNode.data.descripcion = $scope.grupoActual.descripcion;
        UIkit.modal('#mdlEditarGrupoArbol').hide();
        UIkit.modal.alert('Muy bien! el grupo fue modificado exitosamente!', {
          labels: { Ok: 'Listo!' }
        });
        $scope.loaders.modificandoGrupo = false;
      });
    };
    $scope.eliminarGrupoSeleccionado = function() {
      if ($scope.grupoActual.grupoId == 'root') {
        UIkit.modal.alert('Esto no es un grupo, es su empresa', { labels: { Ok: 'Entendido' } });
        return;
      }
      UIkit.modal.confirm(
        'Esta seguro que desea eliminar el grupo <b>' + $scope.grupoActual.nombre + '</b>?',
        function() {
          modal = UIkit.modal.blockUI(
            "<div class='uk-text-center'>Eliminando grupo...<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
          );

          var query = {};
          query.empresaId = SesionFebos().empresa.iut;
          query.grupoId = $scope.grupoActual.grupoId;
          FebosAPI.cl_eliminar_grupo(query, true, false).then(function(response) {
            modal.hide();

            $rootScope.ultimoSeguimientoId = response.data.seguimientoId;

            if (response.data.codigo == 10) {
              var activeNode = $('#arbol-grupos').fancytree('getActiveNode');
              activeNode.remove();
              UIkit.modal.alert('Grupo eliminado', { labels: { Ok: 'Listo!' } });
            }
          });
        },
        { labels: { Ok: 'Si, eliminar', Cancel: 'No, me acabo de arrepentir' } }
      );
    };

    $scope.agregarHijoAlFancyTree = function(hijo) {
      var activeNode = $('#arbol-grupos').fancytree('getActiveNode');
      if (activeNode == null || typeof activeNode == 'undefined') {
        var tree = $('#arbol-grupos').fancytree('getTree');
        var node = tree.getNodeByKey('1');
        node.setSelected(true);
        node.setActive(true);
      }
      var activeNode = $('#arbol-grupos').fancytree('getActiveNode');
      activeNode.addChildren(hijo);
    };
    $scope.verUsuario = function(el) {
      $scope.usuarioActual = el;
      //console.log("usuario actual", el);
      UIkit.modal('#mdlVerUsuarioArbol').show();
    };
    $scope.modalModificarUsuario = function() {
      UIkit.modal('#mdlVerUsuarioArbol').hide();
      UIkit.modal('#mdlModificarUsuarioArbol').show();
    };
    $scope.modalEditarGrupo = function() {
      if ($scope.grupoActual.grupoId == 'root') {
        UIkit.modal.alert('Esto no es un grupo, es su empresa', { labels: { Ok: 'Entendido' } });
        return;
      }

      $scope.grupoEditar.nombre = $scope.grupoActual.nombre;
      $scope.grupoEditar.grupoId = $scope.grupoActual.grupoId;
      $scope.grupoEditar.codigo = $scope.grupoActual.codigo;
      $scope.grupoEditar.descripcion = $scope.grupoActual.descripcion;
      UIkit.modal('#mdlEditarGrupoArbol').show();
    };
    $scope.modalCrearGrupo = function() {
      $scope.grupoNuevo = {
        nombre: '',
        codigo: '',
        descripcion: ''
      };
      UIkit.modal('#mdlCrearGrupoArbol').show();
    };
    $scope.modalTraerAlGrupo = function() {
      if ($scope.grupoActual.grupoId == 'root') {
        UIkit.modal.alert('Esto no es un grupo, es su empresa', { labels: { Ok: 'Entendido' } });
        return;
      }
      $scope.usuariosDisponibles = [];
      $scope.usuariosSeleccionados = [];

      $scope.lideres = {};
      for (var i = 0; i < $scope.todosLosUsuarios.length; i++) {
        if (
          !$scope.usuarioPerteneceAlGrupo(
            $scope.todosLosUsuarios[i].usuario.usuarioId,
            $scope.grupoActual.grupoId
          )
        ) {
          var item = jQuery.extend(true, {}, $scope.todosLosUsuarios[i]);
          var estaYaSeleccionado = false;
          for (var j = 0; j < $scope.usuariosDelGrupo.length; j++) {
            if ($scope.usuariosDelGrupo[j].usuario.id == item.usuario.id) {
              estaYaSeleccionado = true;
              if (typeof item.gruposLiderados != 'undefined') {
                for (var x = 0; x < item.gruposLiderados.length; x++) {
                  if (item.gruposLiderados[x] == $scope.grupoActual.grupoId) {
                    $scope.lideres[item.usuario.id] = true;
                  }
                }
              }
            }
          }
          if (!estaYaSeleccionado) {
            $scope.usuariosDisponibles.push(item);
            if (typeof $scope.todosLosUsuarios[i].gruposLiderados != 'undefined') {
              for (var x = 0; x < $scope.todosLosUsuarios[i].gruposLiderados.length; x++) {
                if ($scope.todosLosUsuarios[i].gruposLiderados[x] == $scope.grupoActual.grupoId) {
                  $scope.lideres[$scope.todosLosUsuarios[i].usuario.usuarioId] = true;
                }
              }
            }
          }
        }
      }
      for (var i = 0; i < $scope.usuariosDelGrupo.length; i++) {
        var item = jQuery.extend(true, {}, $scope.usuariosDelGrupo[i]);
        $scope.usuariosSeleccionados.push(item);
      }
      UIkit.modal('#mdlTraerAlGrupoArbol').show();
    };
    $scope.cancelarTraerUsuarios = function() {
      UIkit.modal('#mdlTraerAlGrupoArbol').hide();
    };
    $scope.aplicarGrupos = function(el) {
      UIkit.modal('#mdlTraerAlGrupoArbol').hide();
      modal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Configurando grupo...<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
      );
      //console.log("miembros", $scope.usuariosSeleccionados);
      //console.log("lideres", $scope.lideres);
      var lideres = [];
      var usuarios = [];
      for (var i = 0; i < $scope.usuariosSeleccionados.length; i++) {
        usuarios.push($scope.usuariosSeleccionados[i].usuario.id);
      }
      for (key in $scope.lideres) {
        if ($scope.lideres[key]) {
          lideres.push(key);
        }
      }
      console.log(lideres, usuarios);

      var query = {
        lideres: lideres.join(','),
        usuarios: usuarios.join(',')
      };
      query.empresaId = SesionFebos().empresa.iut;
      query.grupoId = $scope.grupoActual.grupoId;
      FebosAPI.cl_actualizar_usuarios_grupo(query, true, false).then(function(response) {
        //console.log(" respuesta solicitud al actualizar usuarios ",response);

        modal.hide();
        modal = UIkit.modal.blockUI(
          "<div class='uk-text-center'>Actualizando listado de usuarios...<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
        );

        FebosAPI.cl_listar_arbol_usuarios(
          {
            empresaId: SesionFebos().empresa.iut
          },
          true,
          false
        ).then(function(response) {
          //console.log("usuarios", response);
          $scope.loaders.usuarios = true;
          $scope.todosLosUsuarios = response.data.usuarios;
          for (var i = 0; i < $scope.todosLosUsuarios.length; i++) {
            if (typeof $scope.todosLosUsuarios[i].usuario.avatar === 'undefined') {
              $scope.todosLosUsuarios[i].usuario.avatar =
                'https://api.adorable.io/avatars/' + $scope.todosLosUsuarios[i].usuario.correo;
            }
          }
          modal.hide();
          $scope.cargarUsuariosDeGrupo($scope.grupoActual.grupoId);
          UIkit.modal.alert('El grupo fue configurado exitosamente!', {
            labels: { Ok: 'Entendido' }
          });
        });

        /*$http(req).then(function (response) {
                            //console.log("usuarios", response);
                            $scope.loaders.usuarios = true;
                            $scope.todosLosUsuarios = response.data.usuarios;
                            for (var i = 0; i < $scope.todosLosUsuarios.length; i++) {
                                if (typeof $scope.todosLosUsuarios[i].usuario.avatar === 'undefined') {
                                    $scope.todosLosUsuarios[i].usuario.avatar = "https://api.adorable.io/avatars/" + $scope.todosLosUsuarios[i].usuario.correo;
                                }
                            }
                            modal.hide();
                            $scope.cargarUsuariosDeGrupo($scope.grupoActual.grupoId);
                            UIkit.modal.alert('El grupo fue configurado exitosamente!', {labels: {'Ok': 'Entendido'}});
                        });
                        */
      });
    };
    function obtenerGruposHijos(padreId) {
      ////console.log("Buscando hijos para: "+padreId);
      var hijos = [];
      for (var i = 0; i < $scope.listadoDeGrupos.length; i++) {
        if (typeof padreId == 'undefined' || padreId == 'null' || padreId == '') {
          if (typeof $scope.listadoDeGrupos[i].padreId == 'undefined') {
            hijos.push({
              title: $scope.listadoDeGrupos[i].nombre,
              nombre: $scope.listadoDeGrupos[i].nombre,
              grupoId: $scope.listadoDeGrupos[i].id,
              codigo: $scope.listadoDeGrupos[i].codigo,
              descripcion: $scope.listadoDeGrupos[i].descripcion,
              lazy: true
            });
          }
        } else {
          if ($scope.listadoDeGrupos[i].codigo.indexOf('1000-002-') > 0)
            if ($scope.listadoDeGrupos[i].padreId == padreId) {
              //console.log("el padre del grupo "+$scope.listadoDeGrupos[i].codigo+" es :"+$scope.listadoDeGrupos[i].padreId,$scope.listadoDeGrupos[i].padreId == padreId);
              hijos.push({
                title: $scope.listadoDeGrupos[i].nombre,
                nombre: $scope.listadoDeGrupos[i].nombre,
                grupoId: $scope.listadoDeGrupos[i].id,
                codigo: $scope.listadoDeGrupos[i].codigo,
                descripcion: $scope.listadoDeGrupos[i].descripcion
              });
            }
        }
      }
      return hijos;
    }
    $scope.armarArbol = function() {
      var raiz = obtenerGruposHijos('');
      for (var i = 0; i < raiz.length; i++) {
        raiz[i].children = obtenerGruposHijos(raiz[i].grupoId);
        for (var w = 0; w < raiz[i].children.length; w++) {
          raiz[i].children[w].children = obtenerGruposHijos(raiz[i].children[w].grupoId);
          for (var x = 0; x < raiz[i].children[w].children.length; x++) {
            raiz[i].children[w].children[x].children = obtenerGruposHijos(
              raiz[i].children[w].children[x].grupoId
            );
            for (var y = 0; y < raiz[i].children[w].children[x].children.length; y++) {
              raiz[i].children[w].children[x].children[y].children = obtenerGruposHijos(
                raiz[i].children[w].children[x].children[y].grupoId
              );
              for (
                var z = 0;
                z < raiz[i].children[w].children[x].children[y].children.length;
                z++
              ) {
                raiz[i].children[w].children[x].children[y].children[
                  z
                ].children = obtenerGruposHijos(
                  raiz[i].children[w].children[x].children[y].children[z].grupoId
                );
              }
            }
          }
        }
      }
      //console.log("ARBOL",raiz);
      return raiz;
    };
    $scope.cargarArbolDeGrupos = function(promesa) {
      FebosAPI.cl_listar_grupos(
        {
          empresaId: SesionFebos().empresa.iut
        },
        true,
        false
      ).then(function(response) {
        $scope.loaders.grupos = true;
        $scope.listadoDeGrupos = response.data.grupos;
        //console.log("GRUPOS",$scope.listadoDeGrupos);
        $scope.raiz = $scope.armarArbol();
        var tree = $('#arbol-grupos').fancytree('getTree');
        tree.reload();
      });
    };
    $scope.cargarUsuariosDeGrupo = function(grupoId) {
      $scope.usuariosDelGrupo = [];
      $scope.lideres = {};
      if (grupoId == 'root') {
        for (var i = 0; i < $scope.todosLosUsuarios.length; i++) {
          $scope.usuariosDelGrupo.push($scope.todosLosUsuarios[i]);
        }
      } else {
        for (var i = 0; i < $scope.todosLosUsuarios.length; i++) {
          if (typeof $scope.todosLosUsuarios[i].grupos == 'undefined') {
            continue;
          }
          for (var j = 0; j < $scope.todosLosUsuarios[i].grupos.length; j++) {
            if ($scope.todosLosUsuarios[i].grupos[j].id == grupoId) {
              var item = jQuery.extend(true, {}, $scope.todosLosUsuarios[i]);
              $scope.usuariosDelGrupo.push(item);

              if (typeof item.gruposLiderados != 'undefined') {
                for (var x = 0; x < item.gruposLiderados.length; x++) {
                  if (item.gruposLiderados[x] == $scope.grupoActual.grupoId) {
                    $scope.lideres[item.usuario.id] = true;
                  }
                }
              }
            }
          }
        }
      }
    };
    $scope.usuarioPerteneceAlGrupo = function(usuarioId, grupoId) {
      for (var i = 0; i < $scope.todosLosUsuarios.length; i++) {
        if ($scope.todosLosUsuarios[i].usuario.usuarioId == usuarioId) {
          if (typeof $scope.todosLosUsuarios[i].grupos != 'undefined') {
            for (var j = 0; j < $scope.todosLosUsuarios[i].grupos.length; j++) {
              if ($scope.todosLosUsuarios[i].grupos[j] == grupoId) {
                return true;
              }
            }
          }
        }
      }
      return false;
    };
    $scope.incluiEnElGrupo = function(el) {
      //alert(1);
      $scope.query = '';
      $scope.usuariosSeleccionados.push(jQuery.extend(true, {}, el));
      $scope.lideres[el.usuario.id] = false;
      var i = 0;
      for (; i < $scope.usuariosDisponibles.length; i++) {
        if ($scope.usuariosDisponibles[i].usuario.id == el.usuario.id) {
          break;
        }
      }
      $scope.usuariosDisponibles.splice(i, 1);
    };

    $scope.sacarDelGrupo = function(el) {
      //alert(2);
      ////console.log("elemento",el);
      $scope.query2 = '';
      //console.log('usuariosDisponibles',$scope.usuariosDisponibles)
      $scope.usuariosDisponibles.push(jQuery.extend(true, {}, el));
      var i = 0;
      for (; i < $scope.usuariosSeleccionados.length; i++) {
        if ($scope.usuariosSeleccionados[i].usuario.id == el.usuario.id) {
          break;
        }
      }
      $scope.usuariosSeleccionados.splice(i, 1);
    };
    $scope.buscarUsuario = function(item) {
      if ($scope.query == '') return true;
      if (item.usuario.nombre.toLowerCase().indexOf($scope.query.toLowerCase()) >= 0) {
        return true;
      }
      return false;
    };
    $scope.buscarUsuario2 = function(item) {
      if ($scope.query2 == '') return true;
      if (item.usuario.nombre.toLowerCase().indexOf($scope.query2.toLowerCase()) >= 0) {
        return true;
      }
      return false;
    };
    $scope.buscarUsuario3 = function(item) {
      if ($scope.query3 == '') return true;
      if (item.usuario.nombre.toLowerCase().indexOf($scope.query3.toLowerCase()) >= 0) {
        return true;
      }
      return false;
    };
    var $crear_card = $('#crear_card');
    var $crear_form = $('#crear_form');
    var $crear_error = $('#crear_error');
    var $crear_ok = $('#crear_ok');

    var $modificar_card = $('#modificar_card');
    var $modificar_form = $('#modificar_form');
    var $modificar_error = $('#modificar_error');
    var $modificar_ok = $('#modificar_ok');

    var $asignar_card = $('#asignar_card');
    var $asignar_form = $('#asignar_form');
    var $asignar_error = $('#asignar_error');
    var $asignar_ok = $('#asignar_ok');

    var crear_form_show = function() {
      $crear_form
        .show()
        .siblings()
        .hide();
    };

    var crear_error_show = function() {
      $crear_error
        .show()
        .siblings()
        .hide();
    };

    var crear_ok_show = function() {
      $crear_ok
        .show()
        .siblings()
        .hide();
    };

    $scope.backToCrear = function($event) {
      $event.preventDefault();
      utils.card_show_hide($crear_form, undefined, crear_form_show, undefined);
    };

    var modificar_form_show = function() {
      $modificar_form
        .show()
        .siblings()
        .hide();
    };

    var modificar_error_show = function() {
      $modificar_error
        .show()
        .siblings()
        .hide();
    };

    var modificar_ok_show = function() {
      $modificar_ok
        .show()
        .siblings()
        .hide();
    };

    $scope.backToModificar = function($event) {
      $event.preventDefault();
      utils.card_show_hide($modificar_form, undefined, modificar_form_show, undefined);
    };

    var asignar_form_show = function() {
      $asignar_form
        .show()
        .siblings()
        .hide();
    };

    var asignar_error_show = function() {
      $asignar_error
        .show()
        .siblings()
        .hide();
    };

    var asignar_ok_show = function() {
      $asignar_ok
        .show()
        .siblings()
        .hide();
    };

    $scope.backToAsignar = function($event) {
      console.log($event);
      $event.preventDefault();
      utils.card_show_hide($asignar_form, undefined, asignar_form_show, undefined);
    };

    $scope.loadingEmpresas = true;
    $scope.filtros = [];
    $scope.filtros.paginas = [];
    $scope.filtros.usuario_rut = '';
    $scope.filtros.usuario_nombre = '';
    $scope.filtros.usuario_correo = '';
    $scope.filtros.pagina = '1';
    $scope.filtros.itemsPorPagina = '10';
    $scope.filtros.numeroDePaginas = 9; //< número de páginas máximo mostradas en la parte de cambio de página

    $scope.totalElementos = 0;
    $scope.totalPaginas = 0;
    $scope.index_usuario_eliminar = '';
    $scope.index_usuario_roles = '';
    $scope.texto_roles = 'Obteniendo Roles...';

    $scope.cargando = false;

    $scope.usuarioSeleccionado = new Array(); //< arreglo de usuarios

    // -- selectize roles del usuario:
    $scope.roles_usuario = [];
    $scope.roles_options = [];
    $scope.roles_config = {
      plugins: {
        remove_button: {
          label: ''
        }
      },
      maxItems: null,
      valueField: 'rolId',
      labelField: 'nombre',
      searchField: 'nombre',
      create: false,
      render: {
        option: function(permiso, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(permiso.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(permiso, escape) {
          return '<div class="item">' + escape(permiso.nombre) + '</div>';
        }
      }
    };
    // --
    // -- configuración de empresas
    /*var empresas = $scope.empresas_options = [];

             $scope.empresas_config = {
             plugins: {
             'remove_button': {
             label: ''
             }
             },
             maxItems: null,
             valueField: 'id',
             labelField: 'title',
             searchField: 'title',
             create: false,
             render: {
             option: function (planets_data, escape) {
             return  '<div class="option">' +
             '<span class="title">' + escape(planets_data.title) + '</span>' +
             '</div>';
             },
             item: function (planets_data, escape) {
             return '<div class="item"><a href="' + escape(planets_data.url) + '" target="_blank">' + escape(planets_data.title) + '</a></div>';
             }
             }
             };
             */

    // -- función que lista a los usuarios de la empresa logeada
    $scope.listar = function() {
      $scope.cargando = true;
      $scope.usuarioSeleccionado = [];
      /*
                req = {
                    method: 'GET',
                    url: febosSingleton.api + "/usuarios",
                    params: {
                        "pagina": $scope.filtros.pagina,
                        "filas": $scope.filtros.itemsPorPagina,
                        "filtro": $scope.filtros.usuario_rut.replace(/\,/g, '') + "," + $scope.filtros.usuario_nombre.replace(/\,/g, '') + "," + $scope.filtros.usuario_correo.replace(/\,/g, ''),
                        "simular": "no",
                        "debug": "si"
                    }
                };

                $http(req).then(function (response) {
                    console.log(response);
                    $scope.cargando = false;

                    try {
                        if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
                            $scope.usuarioSeleccionado = [];

                            for (var i = 0; i < response.data.usuarios.length; i++) {
                                var ultimoIngreso = '';
                                var row = {
                                    id: '',
                                    rut: '',
                                    nombre: '',
                                    alias: '',
                                    correo: '',
                                    proveedor: '',
                                    estado: '',
                                    ultimoIngreso: ''
                                }

                                if (response.data.usuarios[i].ultimoIngreso == 'null' || response.data.usuarios[i].ultimoIngreso == null || response.data.usuarios[i].ultimoIngreso == 'undefined' || response.data.usuarios[i].ultimoIngreso == undefined) {
                                    ultimoIngreso = '-';
                                } else {
                                    var ingreso = response.data.usuarios[i].ultimoIngreso;
                                    var splitAcceso = ingreso.split(' ');
                                    var formatoAcceso = splitAcceso[0].split('-');
                                    var formato = formatoAcceso[2] + '-' + formatoAcceso[1] + '-' + formatoAcceso[0];

                                    ultimoIngreso = formato;
                                }

                                row.id = response.data.usuarios[i].id;
                                row.rut = response.data.usuarios[i].iut;
                                row.nombre = response.data.usuarios[i].nombre;
                                row.alias = response.data.usuarios[i].alias;
                                row.correo = response.data.usuarios[i].correo;
                                row.proveedor = response.data.usuarios[i].proveedor;
                                row.estado = response.data.usuarios[i].estado;
                                row.ultimoIngreso = ultimoIngreso;

                               // $scope.usuarioSeleccionado.push(row);
                            }

                            if (response.data.totalElementos == undefined || typeof response.data.totalElementos == "undefined") {
                                $scope.totalElementos = 0;
                            } else {
                                $scope.totalElementos = parseInt(response.data.totalElementos);
                            }

                            $scope.calcularPaginas();

                        } else {
                            febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                        }

                    } catch (e) {
                        febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                    }

                }, function (response) {
                    console.log(response);
                    febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
                });
                */
      FebosAPI.cl_listar_usuarios(
        {
          pagina: $scope.filtros.pagina,
          filas: $scope.filtros.itemsPorPagina,
          filtro:
            $scope.filtros.usuario_rut.replace(/\,/g, '') +
            ',' +
            $scope.filtros.usuario_nombre.replace(/\,/g, '') +
            ',' +
            $scope.filtros.usuario_correo.replace(/\,/g, ''),
          simular: 'no',
          debug: 'si'
        },
        true,
        false
      ).then(function(response) {
        console.log(response);
        $scope.cargando = false;

        if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
          $scope.usuarioSeleccionado = [];

          for (var i = 0; i < response.data.usuarios.length; i++) {
            var ultimoIngreso = '';
            var row = {
              id: '',
              rut: '',
              nombre: '',
              alias: '',
              correo: '',
              proveedor: '',
              estado: '',
              ultimoIngreso: ''
            };

            if (
              response.data.usuarios[i].ultimoIngreso == 'null' ||
              response.data.usuarios[i].ultimoIngreso == null ||
              response.data.usuarios[i].ultimoIngreso == 'undefined' ||
              response.data.usuarios[i].ultimoIngreso == undefined
            ) {
              ultimoIngreso = '-';
            } else {
              var ingreso = response.data.usuarios[i].ultimoIngreso;
              var splitAcceso = ingreso.split(' ');
              var formatoAcceso = splitAcceso[0].split('-');
              var formato = formatoAcceso[2] + '-' + formatoAcceso[1] + '-' + formatoAcceso[0];

              ultimoIngreso = formato;
            }

            row.id = response.data.usuarios[i].id;
            row.rut = response.data.usuarios[i].iut;
            row.nombre = response.data.usuarios[i].nombre;
            row.alias = response.data.usuarios[i].alias;
            row.correo = response.data.usuarios[i].correo;
            row.proveedor = response.data.usuarios[i].proveedor;
            row.estado = response.data.usuarios[i].estado;
            row.ultimoIngreso = ultimoIngreso;

            // $scope.usuarioSeleccionado.push(row);
          }

          if (
            response.data.totalElementos == undefined ||
            typeof response.data.totalElementos == 'undefined'
          ) {
            $scope.totalElementos = 0;
          } else {
            $scope.totalElementos = parseInt(response.data.totalElementos);
          }

          $scope.calcularPaginas();
        }
      });
    };
    // --

    // -- función que permite cambiar de página en un mantenedor
    $scope.cambiarPagina = function(index) {
      $scope.filtros.numeroDePaginas = Math.ceil($scope.filtros.numeroDePaginas);
      $scope.totalElementos = Math.ceil($scope.totalElementos);

      if (index == 'inicio') {
        $scope.filtros.pagina = 1;
      } else {
        if (index == 'final') {
          $scope.filtros.pagina = $scope.totalPaginas;
        } else {
          if (
            index !== '' &&
            (index != undefined || typeof index != 'undefined') &&
            ($scope.filtros.paginas[index] != undefined ||
              typeof $scope.filtros.paginas[index] != 'undefined')
          ) {
            $scope.filtros.pagina = $scope.filtros.paginas[index].numero;
          } else {
            $scope.filtros.pagina = 1;
          }
        }
      }

      $scope.listar(); //< llama función listar usuarios, para mostrar data
    };
    // --

    // -- función que calcula las páginas a mostrar en el mantenedor
    $scope.calcularPaginas = function() {
      $scope.totalPaginas = Math.ceil($scope.totalElementos / $scope.filtros.itemsPorPagina);

      var resta = -(Math.ceil($scope.filtros.numeroDePaginas) % 2 == 0
        ? Math.ceil($scope.filtros.numeroDePaginas / 2 - 1)
        : Math.ceil($scope.filtros.numeroDePaginas / 2 - 1));
      var suma =
        Math.ceil($scope.filtros.numeroDePaginas) % 2 == 0
          ? Math.ceil($scope.filtros.numeroDePaginas / 2)
          : Math.ceil($scope.filtros.numeroDePaginas / 2 - 1);
      var first = false;

      if (Math.ceil($scope.filtros.pagina) + resta <= 0) {
        first = true;
        suma -= Math.ceil($scope.filtros.pagina) + (resta - 1);
        resta -= Math.ceil($scope.filtros.pagina) + (resta - 1);
      }

      if ($scope.totalPaginas <= 0) suma = $scope.filtros.numeroDePaginas + resta - 1;

      if (
        Math.ceil($scope.filtros.pagina) + suma > $scope.totalPaginas &&
        $scope.totalPaginas > 0
      ) {
        if (first) {
          suma -= Math.ceil($scope.filtros.pagina + suma) - $scope.totalPaginas;
        } else {
          suma -= Math.ceil($scope.filtros.pagina + suma) - $scope.totalPaginas;
        }
      }

      var cont = 0;
      $scope.filtros.paginas = [];

      for (var i = $scope.filtros.pagina + resta; i <= $scope.filtros.pagina + suma; i++) {
        var nuevaPagina = {
          id: cont,
          numero: i
        };

        $scope.filtros.paginas.push(nuevaPagina);
        cont++;
      }
    };
    // --

    // -- función que limpia los input de los filtros
    $scope.limpiarFiltros = function() {
      $scope.filtros.usuario_rut = '';
      $scope.filtros.usuario_nombre = '';
      $scope.filtros.usuario_correo = '';
    };
    // --

    $scope.datas = {
      m_empresa: '',
      m_iut: '',
      m_razon: '',
      m_fantasia: '',
      textoModificar: 'MODIFICAR',
      modificar_error: '',
      modificar_ok: ''
    };

    $scope.usuario_id = '';
    $scope.usuario_rut = '';
    $scope.usuario_nombre = '';
    $scope.usuario_alias = '';
    $scope.usuario_correo = '';
    $scope.usuario_clave = '';
    $scope.loadingRoles = true;

    // -- función que activa el modal de modificar un usuario
    $scope.modificar = function(el) {
      $scope.datas.textoModificar = 'MODIFICAR';

      $scope.usuarioSeleccionado = el.usuario;
      //$scope.usuario_clave = "";

      var modal = UIkit.modal('#mdModificarUsuario');
      modal.show();
    };
    // --

    // -- función que gurda un usuario modificado
    $scope.guardarModificacion = function() {
      $scope.datas.textoModificar = 'modificando ...';

      if (!$scope.validarModificar())
        //< valida compos obligatorios
        return false;

      var id = $scope.usuarioSeleccionado.id;
      console.log(id);
      /*
                var reqx = {
                    method: 'PUT',
                    url: febosSingleton.api + "/usuarios/" + id,
                    data: {
                        "nombre": $scope.usuarioSeleccionado.nombre,
                        "alias": $scope.usuarioSeleccionado.alias,
                        "correo": $scope.usuarioSeleccionado.correo
                    },
                    params: {
                        'simular': 'no'
                    }
                }

                $http(reqx).then(function (response) {
                    $scope.datas.textoModificar = "MODIFICAR";
                    console.log(response);

                    try {
                        if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
                            console.log('OK');
                            var modal = UIkit.modal("#mdModificarUsuario");
                            modal.hide();
                            $scope.listar();
                        } else {
                            console.log('no es codigo 10');
                            febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                        }

                    } catch (e) {
                        console.log(e);
                        febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                    }

                }, function (response) {
                    $scope.datas.textoModificar = "MODIFICAR";
                    febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
                });
                */
      FebosAPI.cl_modificar_usuario(
        {
          usuarioId: id,
          simular: 'no'
        },
        {
          nombre: $scope.usuarioSeleccionado.nombre,
          alias: $scope.usuarioSeleccionado.alias,
          correo: $scope.usuarioSeleccionado.correo
        },
        true,
        false
      ).then(function(response) {
        $scope.datas.textoModificar = 'MODIFICAR';
        console.log(response);
        if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
          console.log('OK');
          var modal = UIkit.modal('#mdModificarUsuario');
          modal.hide();
          $scope.listar();
        }
      });
    };
    // --

    // -- función que valida los datos del formulario que modifica data a un usuario
    $scope.validarModificar = function() {
      var errores = '';

      if ($scope.usuarioSeleccionado.nombre == '') {
        errores += '\nDebe ingresar un Nombre.';
      }

      if ($scope.usuarioSeleccionado.alias == '') {
        errores += '\nDebe ingresar un Alias.';
      }

      if ($scope.usuarioSeleccionado.correo == '') {
        errores += '\nDebe ingresar un Correo Electrónico.';
      }

      if (errores != '') {
        alert('Errores encontrados:' + errores);
        return false;
      } else {
        return true;
      }
    };
    // --

    // -- función que activa el modal crear un nuevo usuario
    $scope.mostrarModalCrear = function() {
      //console.log("mostrar el modal");
      $scope.datos = {
        rut: '',
        nombre: '',
        apellidos: '',
        alias: '',
        correo: '',
        clave: '',
        empresas: '',
        textoCrear: 'CREAR',
        mensaje_error: '',
        mensaje_ok: ''
      };
      console.log($scope.datos);
      UIkit.modal('#mdCrearUsuario').show();
    };
    // --

    // -- función que crea un nuevo usuario
    $scope.crear = function() {
      if ($scope.datos.textoCrear != 'CREAR') return;

      if (!$scope.validarCrear())
        //< valida compos obligatorios
        return false;

      $scope.datos.textoCrear = 'Creando ...';
      /*
                var reqs = {
                    method: 'PUT',
                    url: febosSingleton.api + "/usuarios",
                    data: {
                        "iut": $scope.datos.rut,
                        "nombre": $scope.datos.nombre + ' ' + $scope.datos.apellidos,
                        "alias": $scope.datos.alias,
                        "correo": $scope.datos.correo.trim(),
                        "clave": $scope.datos.clave,
                        "nivel": 1,
                        "cambiarClave": "si",
                        "accesoWeb": "si"
                    },
                    params: {
                        'simular': 'no',
                        "debug": "si"
                    }
                };

                $http(reqs).then(function (response) {
                    console.log(response);
                    try {
                        if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
                            $scope.datos.textoCrear = "CREAR";

                            var modal = UIkit.modal("#mdCrearUsuario");
                            modal.hide();

                            $scope.cargarUsuarios();
                            $scope.cambiarPagina('inicio');

                        } else {
                            $scope.datos.textoCrear = "CREAR";
                            febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                        }

                    } catch (e) {
                        console.log(e);
                        $scope.datos.textoCrear = "CREAR";
                        febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                    }

                }, function (response) {
                    console.log(response);
                    $scope.datos.textoCrear = "CREAR";
                    febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                });
                */
      var body = {
        iut: formatRut($scope.datos.rut),
        nombre: $scope.datos.nombre + ' ' + $scope.datos.apellidos,
        alias: $scope.datos.alias,
        correo: $scope.datos.correo.trim(),
        clave: $scope.datos.clave,
        nivel: 1,
        cambiarClave: 'si',
        accesoWeb: 'si'
      };
      var query = {
        simular: 'no',
        debug: 'si'
      };
      FebosAPI.cl_crear_usuario(query, body, true, false).then(function(response) {
        if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
          $scope.datos.textoCrear = 'CREAR';

          var modal = UIkit.modal('#mdCrearUsuario');
          modal.hide();

          $scope.cargarUsuarios();
          $scope.cambiarPagina('inicio');
        }
      });
    };
    // --

    // -- función que valida los datos del formulario que crea un nuevo usuario
    $scope.validarCrear = function() {
      var errores = '';

      if (!isRut(formatRut($scope.datos.rut))) {
        errores += '\nEl RUT ingresado no es válido.';
      }

      if ($scope.datos.nombre == '') {
        errores += '\nDebe ingresar un Nombre.';
      }

      if ($scope.datos.apellidos == '') {
        errores += '\nDebe ingresar Apellidos.';
      }

      if ($scope.datos.alias == '') {
        errores += '\nDebe ingresar un Alias.';
      }

      if ($scope.datos.correo == '') {
        errores += '\nDebe ingresar un Correo Electrónico.';
      }

      if (errores != '') {
        alert('Errores encontrados:' + errores);
        return false;
      } else {
        return true;
      }
    };
    // --

    // -- función que activa modal para seleccionar roles y asignarlos a un usuario
    $scope.abrirRoles = function(el) {
      $scope.usuarioSeleccionado = el.usuario;
      $scope.texto_roles = 'Asignar Roles';

      var modal = UIkit.modal('#mdRoles');
      modal.show();
      $scope.obtenerRoles();
    };
    // --
    $scope.cargandoRoles = false;
    $scope.rolesTemporales = [];
    $scope.rol = [];
    $scope.rol.filtro_rol = '';
    $scope.rol.filtro_roltemp = '';
    // -- función que obtiene los roles
    $scope.obtenerRoles = function() {
      $scope.rolesTemporales = [];
      $scope.roles_usuario = [];
      $scope.roles_options = [];
      $scope.rol.filtro_rol = '';
      $scope.rol.filtro_roltemp = '';
      $scope.cargandoRoles = true;
      /*
                var req = {
                    method: 'GET',
                    url: febosSingleton.api + "/usuarios/" + $scope.usuarioSeleccionado.id + '/roles',
                    params: {
                        "pagina": "1",
                        "simular": "no",
                        "debug": "si"
                    }
                };

                $http(req).then(function (response) {
                    $scope.cargandoRoles = false;
                    console.log(JSON.stringify(response));

                    try {
                        if (response.data.codigo === 10 || typeof response.data.codigo === '10') {

                            var respuesta = response.data.roles;
                            $scope.rolesTemporales = [];
                            $scope.roles_options = [];

                            for (var i = 0; i < respuesta.length; i++) {
                                var elemento = {};
                                var temp={};
                                console.log(respuesta[i]);
                                if (respuesta[i].rolId != undefined || typeof respuesta[i].rolId != "undefined")
                                    elemento.rolId = respuesta[i].rolId;
                                if (respuesta[i].nombre != undefined || typeof respuesta[i].nombre != "undefined")
                                    elemento.nombre = respuesta[i].nombre;
                                    elemento.descripcion = respuesta[i].descripcion;
                                if (respuesta[i].asignado != undefined || typeof respuesta[i].asignado != "undefined")
                                    elemento.asignado = respuesta[i].asignado;
                                if ((respuesta[i].rolId != undefined || typeof respuesta[i].rolId != "undefined") && (respuesta[i].asignado != undefined || typeof respuesta[i].asignado != "undefined") && respuesta[i].asignado == "si") {
                                    //$scope.roles_usuario.push(respuesta[i].rolId);
                                    temp.rolId=respuesta[i].rolId;
                                    temp.nombre = respuesta[i].nombre;
                                    temp.descripcion = respuesta[i].descripcion;
                                    $scope.rolesTemporales.push(temp);
                                    ////console.log("AAAA"+JSON.stringify(respuesta));
                                }
                                if(respuesta[i].asignado == "no")
                                {
                                    $scope.roles_options.push(elemento);
                                }
                            }
                            $scope.loadingRoles=false;
                            setTimeout(function () {
                                $scope.roles_usuario = [];
                                //$scope.desplegarRoles();
                            }, 10);
                            $scope.texto_roles = 'Asignar Roles';

                        } else {
                            $scope.texto_roles = 'Asignar Roles';
                            febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                        }

                    } catch (e) {
                        $scope.texto_roles = 'Asignar Roles';
                        febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                    }
                }, function (response) {
                    $scope.cargandoRoles = false;
                    $scope.texto_roles = 'Asignar Roles';
                    febosSingleton.error(1, "Error: no se pudo enviar la solicitud.", "no hay id de seguimiento", "");
                });
                */

      var query = {
        pagina: '1',
        simular: 'no',
        debug: 'si',
        usuarioId: $scope.usuarioSeleccionado.id,
        ambito: FebosUtil.obtenerAmbito()
      };
      FebosAPI.cl_listar_roles_usuario(query, true, false).then(function(response) {
        $scope.cargandoRoles = false;
        console.log(JSON.stringify(response));
        if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
          var respuesta = response.data.roles;
          $scope.rolesTemporales = [];
          $scope.roles_options = [];

          for (var i = 0; i < respuesta.length; i++) {
            var elemento = {};
            var temp = {};
            console.log(respuesta[i]);
            if (respuesta[i].rolId != undefined || typeof respuesta[i].rolId != 'undefined')
              elemento.rolId = respuesta[i].rolId;
            if (respuesta[i].nombre != undefined || typeof respuesta[i].nombre != 'undefined')
              elemento.nombre = respuesta[i].nombre;
            elemento.descripcion = respuesta[i].descripcion;
            if (respuesta[i].asignado != undefined || typeof respuesta[i].asignado != 'undefined')
              elemento.asignado = respuesta[i].asignado;
            if (
              (respuesta[i].rolId != undefined || typeof respuesta[i].rolId != 'undefined') &&
              (respuesta[i].asignado != undefined || typeof respuesta[i].asignado != 'undefined') &&
              respuesta[i].asignado == 'si'
            ) {
              //$scope.roles_usuario.push(respuesta[i].rolId);
              temp.rolId = respuesta[i].rolId;
              temp.nombre = respuesta[i].nombre;
              temp.descripcion = respuesta[i].descripcion;
              $scope.rolesTemporales.push(temp);
              ////console.log("AAAA"+JSON.stringify(respuesta));
            }
            if (respuesta[i].asignado == 'no') {
              $scope.roles_options.push(elemento);
            }
          }
          $scope.loadingRoles = false;
          setTimeout(function() {
            $scope.roles_usuario = [];
            //$scope.desplegarRoles();
          }, 10);
          $scope.texto_roles = 'Asignar Roles';
        }
      });
    };
    $scope.tareas = [];
    $scope.agregarRoles = function(index) {
      ////console.log("agregado"+index);
      for (var i = 0; i < $scope.roles_options.length; i++) {
        if ($scope.roles_options[i].rolId == index) {
          $scope.rolesTemporales.push($scope.roles_options[i]);
          $scope.roles_options.splice(i, 1);
          //console.log(JSON.stringify($scope.rolesTemporales));
        }
      }
      ////console.log("$scope.roles_usuario: "+$scope.roles_usuario);
    };
    $scope.quitarRoles = function(index) {
      ////console.log("quitado"+index);
      for (var i = 0; i < $scope.rolesTemporales.length; i++) {
        if ($scope.rolesTemporales[i].rolId == index) {
          $scope.roles_options.push($scope.rolesTemporales[i]);
          $scope.rolesTemporales.splice(i, 1);
        }
      }
    };
    $scope.aplicarRoles = function() {
      for (var i = 0; i < $scope.rolesTemporales.length; i++) {
        if ($scope.rolesTemporales[i].rolId != '') {
          $scope.roles_usuario.push($scope.rolesTemporales[i].rolId);
        }
      }
      $scope.asignarRoles();
    };
    // --
    /* $scope.desplegarRoles = function () {
                //console.log("$scope.roles_usuario: "+$scope.roles_usuario);
                if ($scope.rolesTemporales.length > 0) {
                    $scope.roles_usuario.push($scope.rolesTemporales.splice(0, 1));
                    setTimeout(function () {
                        $scope.desplegarRoles();
                    }, 10);
                } else {
                    $scope.$apply();
                }

            }*/
    // -- función que asigna roles
    $scope.asociandorol = false;
    $scope.asignarRoles = function() {
      $scope.asociandorol = true;
      $scope.texto_roles = 'Asignando Roles...';

      /*
                var req = {
                    method: 'POST',
                    url: febosSingleton.api + "/usuarios/" + $scope.usuarioSeleccionado.id + '/roles',
                    params: {
                        "pagina": "1",
                        "simular": "no",
                        "debug": "si"
                    },
                    data: {
                        "roles": ($scope.roles_usuario.constructor === Array ? $scope.roles_usuario.join(",") : $scope.roles_usuario)
                    }
                };

                $http(req).then(function (response) {
                    console.log(response);
                    $scope.obtenerRoles();
                    try {
                        if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                            $scope.texto_roles = 'Asignar Roles';
                            utils.card_show_hide($asignar_form, undefined, asignar_ok_show, undefined);
                            $scope.asociandorol = false;
                            UIkit.modal.alert('Muy bien! el rol fue asignado exitosamente!', {labels: {'Ok': 'Listo!'}});
                            //$scope.cerrarRoles();
                            //$scope.abrirRoles($scope.usuarioSeleccionado.id);
                        } else {
                            $scope.texto_roles = 'Asignar Roles';
                            febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                        }

                        $scope.texto_roles = 'Asignar Roles';

                    } catch (e) {
                        $scope.texto_roles = 'Asignar Roles';
                        febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                    }

                }, function (response) {
                    $scope.texto_roles = 'Asignar Roles';
                    febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
                });
                */

      var query = {
        pagina: '1',
        simular: 'no',
        debug: 'si',
        usuarioId: $scope.usuarioSeleccionado.id
      };
      var body = {
        roles:
          $scope.roles_usuario.constructor === Array
            ? $scope.roles_usuario.join(',')
            : $scope.roles_usuario
      };
      FebosAPI.cl_modificar_roles_usuario(query, body, true, false).then(function(response) {
        console.log(response);
        $scope.obtenerRoles();

        if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
          $scope.texto_roles = 'Asignar Roles';
          utils.card_show_hide($asignar_form, undefined, asignar_ok_show, undefined);
          $scope.asociandorol = false;
          UIkit.modal.alert('Muy bien! el rol fue asignado exitosamente!', {
            labels: { Ok: 'Listo!' }
          });
        }
        $scope.texto_roles = 'Asignar Roles';
      });
    };

    // --

    // -- función que cierra roles
    $scope.cerrarRoles = function() {
      $scope.informacion['obtenerPermisos'].index = '';
      $scope.informacion['asignarPermisos'].index = '';

      var modal = UIkit.modal('#mdRoles');
      modal.hide();
    };
    // --
    $scope.cargandoEliminar = false;
    $scope.texto_eliminar = 'ELIMINAR';

    // -- función que elimina un usuario
    $scope.eliminaUsuario = function() {
      $scope.texto_eliminar = 'ELIMINANDO...';
      $scope.cargandoEliminar = true;
      var id = $scope.usuarioSeleccionado.id;
      console.log(id);
      /*
                var req = {
                    method: 'DELETE',
                    url: febosSingleton.api + "/usuarios/" + id
                }

                $http(req).then(function (response) {
                    console.log(response);
                $scope.cargandoEliminar = false;
                $scope.texto_eliminar = "ELIMINAR";

                    try {
                        if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
                            console.log('OK');
                            $scope.cancelarEliminarUsuario();
                            $scope.cargarUsuarios();
                            $scope.cambiarPagina('inicio');

                        } else {
                            febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                        }

                    } catch (e) {
                        febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                        console.log(e);
                    }

                }, function (response) {
                    console.log(response);
                    febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
                });
                */

      var query = {
        usuarioId: $scope.usuarioSeleccionado.id
      };

      FebosAPI.cl_eliminar_usuario(query, true, false).then(function(response) {
        console.log(response);
        $scope.cargandoEliminar = false;
        $scope.texto_eliminar = 'ELIMINAR';

        if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
          console.log('OK');
          $scope.cancelarEliminarUsuario();
          $scope.cargarUsuarios();
          $scope.cambiarPagina('inicio');
        }
      });
    };
    // --

    // -- función que confirma la eliminación de un usuario
    $scope.confirmarEliminarUsuario = function(el) {
      $scope.usuarioSeleccionado = el.usuario;
      var modal = UIkit.modal('#mdConfirmar');
      modal.show();
    };
    // --

    // -- función que cancela la eliminación de un usuario
    $scope.cancelarEliminarUsuario = function() {
      $scope.index_usuario_eliminar = '';

      var modal = UIkit.modal('#mdConfirmar');
      modal.hide();
    };
    // --

    // -- función que valida el rut 11111111-1
    function isRut(rutCompleto) {
      //console.log("VALIDAMOS RUT",rutCompleto);
      if (!/^[0-9]+-[0-9kK]{1}$/.test(rutCompleto)) {
        return false;
      }

      var tmp = rutCompleto.split('-');
      var digv = tmp[1];
      var rut = tmp[0];

      if (digv == 'K') {
        digv = 'k';
      }

      var digesto = isDv(rut);

      if (digesto == digv) {
        return true;
      } else {
        return false;
      }
    }
    // --

    // -- función que valida el DV del rut
    function isDv(T) {
      var M = 0;
      var S = 1;

      for (; T; T = Math.floor(T / 10)) {
        S = (S + (T % 10) * (9 - (M++ % 6))) % 11;
      }

      return S ? S - 1 : 'k';
    }
    // --
    $scope.usuario_id = '';
    $scope.usuario_rut = '';
    //$scope.empresas_asociar = [];

    var empresas_config = {
      plugins: {
        remove_button: {
          label: ''
        }
      },
      maxItems: null,
      valueField: 'id',
      labelField: 'title',
      searchField: ['title', 'rut'],
      create: false,
      render: {
        option: function(empresa, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(empresa.title) +
            '</span>' +
            '</div>'
          );
        },
        item: function(empresa, escape) {
          return '<div class="item">' + escape(empresa.title) + '</div>';
        }
      }
    };
    $scope.empresas = {
      asignadas: [],
      disponibles: [],
      disponiblesTemporal: [],
      configuracion: empresas_config
    };
    $scope.empresa_ids = [];
    // -- función para asignar empresas
    $scope.asignarUsuariosModal = function(el) {
      //$scope.usuario= el.usuario;
      $scope.empresas.asignadas = [];
      $scope.empresas.asignadasTemporal = [];
      $scope.empresas.disponibles = [];

      $scope.usuariosSeleccionadosCopiarRoles = [];

      //Clono arreglo
      $scope.usuariosElegibles = $scope.usuariosDelGrupo.slice(0);

      $scope.usuario_id = el.usuario.id;
      $scope.nombre = el.usuario.nombre;
      $scope.alias = el.usuario.alias;

      $scope.queryCopiarRoles = {
        queryUsuarioDisponible: '',
        queryUsuarioAgregado: ''
      };

      $scope.loadingEmpresas = true;
      var modal = UIkit.modal('#mdAsignarUsuarios');
      modal.show();

      var query = {
        busquedaSimple: 'si',
        filas: 9000,
        pagina: 1
      };

      FebosAPI.cl_listar_empresas(query, true, false).then(function(response) {
        $scope.empresas.disponibles = [];
        for (var i = 0; i < response.data.empresas.length; i++) {
          $scope.empresas.disponibles.push({
            id: response.data.empresas[i].id,
            title: response.data.empresas[i].razonSocial,
            rut: response.data.empresas[i].iut
          });
        }
        //console.log("EMPRESAS DISPONIBLES", $scope.empresas.disponibles);

        var query = {
          busquedaSimple: 'si',
          filas: 9000,
          pagina: 1,
          usuarioId: $scope.usuario_id
        };
        FebosAPI.cl_listar_empresas_usuario(query, true, false).then(function(response) {
          $scope.loadingEmpresas = false;
          //se asignan options a un selectize
          var respuestaEmpresas = response.data.empresas;
          //elemento se debe agregar como value
          var aux = false;
          var empresa_dis = $scope.empresas.disponibles;
          $scope.empresa_ids = [];
          for (var i = 0; i < response.data.empresas.length; i++) {
            $scope.empresas.asignadasTemporal.push({
              id: respuestaEmpresas[i].id,
              title: respuestaEmpresas[i].razonSocial
            });
            for (var j = 0; j < empresa_dis.length; j++) {
              if (empresa_dis[j].id == respuestaEmpresas[i].id) {
                $scope.empresa_ids.push(empresa_dis[j].id);
              }
            }
          }
          for (var i = 0; i < empresa_dis.length; i++) {
            for (var j = 0; j < $scope.empresa_ids.length; j++) {
              if (empresa_dis[i].id == $scope.empresa_ids[j]) {
                $scope.empresas.disponibles.splice(i, 1);
              }
            }
          }
          //console.log("todas "+JSON.stringify($scope.empresa_ids));
        });
      });
    };

    $scope.asignarEmpresasModal = function(el) {
      $scope.empresas.asignadas = [];
      $scope.empresas.asignadasTemporal = [];
      $scope.empresas.disponibles = [];
      $scope.usuario_id = el.usuario.id;
      $scope.loadingEmpresas = true;
      var modal = UIkit.modal('#mdAsignarEmpresas');
      modal.show();

      //obtiene empresas del usuario
      /*
                var req = {
                    method: 'GET',
                    //url: febosSingleton.api + "/usuarios/" + $scope.usuario_id + "/empresas",
                    url: febosSingleton.api + "/empresas",
                    params: {
                        busquedaSimple: 'si',
                        filas: 9000,
                        pagina: 1
                    }
                };

                $http(req).then(function (response) {
                    $scope.empresas.disponibles = [];
                    for (var i = 0; i < response.data.empresas.length; i++) {
                       // console.log(i, {id: response.data.empresas[i].id, title: response.data.empresas[i].razonSocial});
                        $scope.empresas.disponibles.push({id: response.data.empresas[i].id, title: response.data.empresas[i].razonSocial, rut:response.data.empresas[i].iut});
                    }
                    ////console.log("todas "+JSON.stringify(response));
                    ////console.log("todas "+JSON.stringify($scope.empresas.disponibles));
                    //console.log("EMPRESAS DISPONIBLES", $scope.empresas.disponibles);
                    var req2 = {
                        method: 'GET',
                        //url: febosSingleton.api + "/empresas",
                        url: febosSingleton.api + "/usuarios/" + $scope.usuario_id + "/empresas",
                        params: {
                            busquedaSimple: 'si',
                            filas: 9000,
                            pagina: 1
                        }
                    };



                    $http(req2).then(function (response2) {
                        ////console.log("estaaaa : "+JSON.stringify(response2));
                        $scope.loadingEmpresas = false;
                        //se asignan options a un selectize
                        var respuestaEmpresas = response2.data.empresas;
                        //elemento se debe agregar como value
                        var aux=false;
                        var empresa_dis=$scope.empresas.disponibles;
                        $scope.empresa_ids=[];
                        for (var i = 0; i < response2.data.empresas.length; i++) {
                            $scope.empresas.asignadasTemporal.push({id: respuestaEmpresas[i].id, title: respuestaEmpresas[i].razonSocial});
                            for (var j = 0; j < empresa_dis.length; j++) {
                                if(empresa_dis[j].id==respuestaEmpresas[i].id)
                                {
                                    $scope.empresa_ids.push(empresa_dis[j].id);
                                }
                            }
                        }
                        for (var i = 0; i < empresa_dis.length; i++) {
                            for (var j = 0; j < $scope.empresa_ids.length; j++) {
                                if(empresa_dis[i].id==$scope.empresa_ids[j])
                                {
                                    $scope.empresas.disponibles.splice(i,1);
                                }
                            }
                        }
                        //console.log("todas "+JSON.stringify($scope.empresa_ids));


                    });

                });
                */

      var query = {
        busquedaSimple: 'si',
        filas: 9000,
        pagina: 1
      };

      FebosAPI.cl_listar_empresas(query, true, false).then(function(response) {
        $scope.empresas.disponibles = [];
        for (var i = 0; i < response.data.empresas.length; i++) {
          $scope.empresas.disponibles.push({
            id: response.data.empresas[i].id,
            title: response.data.empresas[i].razonSocial,
            rut: response.data.empresas[i].iut
          });
        }
        //console.log("EMPRESAS DISPONIBLES", $scope.empresas.disponibles);

        /*
                            var req2 = {
                                method: 'GET',
                                //url: febosSingleton.api + "/empresas",
                                url: febosSingleton.api + "/usuarios/" + $scope.usuario_id + "/empresas",
                                params: {
                                    busquedaSimple: 'si',
                                    filas: 9000,
                                    pagina: 1
                                }
                            };



                            $http(req2).then(function (response2) {
                                ////console.log("estaaaa : "+JSON.stringify(response2));
                                $scope.loadingEmpresas = false;
                                //se asignan options a un selectize
                                var respuestaEmpresas = response2.data.empresas;
                                //elemento se debe agregar como value
                                var aux=false;
                                var empresa_dis=$scope.empresas.disponibles;
                                $scope.empresa_ids=[];
                                for (var i = 0; i < response2.data.empresas.length; i++) {
                                    $scope.empresas.asignadasTemporal.push({id: respuestaEmpresas[i].id, title: respuestaEmpresas[i].razonSocial});
                                    for (var j = 0; j < empresa_dis.length; j++) {
                                        if(empresa_dis[j].id==respuestaEmpresas[i].id)
                                        {
                                            $scope.empresa_ids.push(empresa_dis[j].id);
                                        }
                                    }
                                }
                                for (var i = 0; i < empresa_dis.length; i++) {
                                    for (var j = 0; j < $scope.empresa_ids.length; j++) {
                                        if(empresa_dis[i].id==$scope.empresa_ids[j])
                                        {
                                            $scope.empresas.disponibles.splice(i,1);
                                        }
                                    }
                                }
                                //console.log("todas "+JSON.stringify($scope.empresa_ids));


                            });
                            */

        var query = {
          busquedaSimple: 'si',
          filas: 9000,
          pagina: 1,
          usuarioId: $scope.usuario_id
        };
        FebosAPI.cl_listar_empresas_usuario(query, true, false).then(function(response) {
          $scope.loadingEmpresas = false;
          //se asignan options a un selectize
          var respuestaEmpresas = response.data.empresas;
          //elemento se debe agregar como value
          var aux = false;
          var empresa_dis = $scope.empresas.disponibles;
          $scope.empresa_ids = [];
          for (var i = 0; i < response.data.empresas.length; i++) {
            $scope.empresas.asignadasTemporal.push({
              id: respuestaEmpresas[i].id,
              title: respuestaEmpresas[i].razonSocial
            });
            for (var j = 0; j < empresa_dis.length; j++) {
              if (empresa_dis[j].id == respuestaEmpresas[i].id) {
                $scope.empresa_ids.push(empresa_dis[j].id);
              }
            }
          }
          for (var i = 0; i < empresa_dis.length; i++) {
            for (var j = 0; j < $scope.empresa_ids.length; j++) {
              if (empresa_dis[i].id == $scope.empresa_ids[j]) {
                $scope.empresas.disponibles.splice(i, 1);
              }
            }
          }
          //console.log("todas "+JSON.stringify($scope.empresa_ids));
        });
      });
    };

    /*$scope.agregarEmpresaAsignadaAlSelect = function () {
                if ($scope.empresas.asignadasTemporal.length > 0) {
                    var id = $scope.empresas.asignadasTemporal.splice(0, 1);
                    $scope.empresas.asignadas.push(id);
                    setTimeout(function () {
                        $scope.agregarEmpresaAsignadaAlSelect();
                    }, 10);
                } else {
                    $scope.$apply();
                }
            };*/

    $scope.copiarRoles = function() {
      var query = {
        usuarioId: $scope.usuario_id
      };

      var jsonIds = [];

      for (var i = 0; i < $scope.usuariosSeleccionadosCopiarRoles.length; i++) {
        jsonIds.push($scope.usuariosSeleccionadosCopiarRoles[i].usuario.id);
      }

      var body = {
        usuarioId: $scope.usuario_id,
        listaUsuariosRecibir: JSON.stringify(jsonIds)
      };

      $scope.loadingCopiandoRoles = true;

      FebosAPI.cl_copiar_roles_usuario_a_usuarios(query, body, true, false).then(function(
        response
      ) {
        $scope.loadingCopiandoRoles = false;
        UIkit.modal.alert('Roles traspasados exitosamente', { labels: { Ok: 'Listo!' } });
      });
    };

    $scope.filtraUsuarioDisponible = function(item) {
      if ($scope.usuario_id == item.usuario.id) return false;

      if ($scope.queryUsuarioDisponible == '') return true;
      if (
        item.usuario.nombre
          .toLowerCase()
          .indexOf($scope.queryCopiarRoles.queryUsuarioDisponible.toLowerCase()) >= 0
      ) {
        return true;
      }
      return false;
    };

    $scope.filtraUsuarioAgregado = function(item) {
      if ($scope.queryCopiarRoles.queryUsuarioAgregado == '') return true;
      if (
        item.usuario.nombre
          .toLowerCase()
          .indexOf($scope.queryCopiarRoles.queryUsuarioAgregado.toLowerCase()) >= 0
      ) {
        return true;
      }
      return false;
    };

    function buscaIndexEnArrayUsuario(array, id) {
      for (var z = 0; z < array.length; z++) {
        if (array[z].usuario.id === id) {
          return z;
        }
      }
    }

    $scope.agregarUsuarioAgregarRol = function(index) {
      $scope.usuariosSeleccionadosCopiarRoles.push(index);

      //var indexUsuarioBuscado=$scope.usuariosElegibles.findIndex((objectUser)=>objectUser.usuario.id===index.usuario.id);
      var indexUsuarioBuscado = buscaIndexEnArrayUsuario(
        $scope.usuariosElegibles,
        index.usuario.id
      );
      $scope.usuariosElegibles.splice(indexUsuarioBuscado, 1);
    };

    $scope.deseleccionarUsuario = function(index) {
      $scope.usuariosElegibles.push(index);
      //var indexUsuarioBuscado=$scope.usuariosSeleccionadosCopiarRoles.findIndex((objectUser)=>objectUser.usuario.id===index.usuario.id);
      var indexUsuarioBuscado = buscaIndexEnArrayUsuario(
        $scope.usuariosSeleccionadosCopiarRoles,
        index.usuario.id
      );
      $scope.usuariosSeleccionadosCopiarRoles.splice(indexUsuarioBuscado, 1);
    };

    $scope.agregarEmpresa = function(index) {
      //console.log("agregado"+index);
      var empresa_dis = $scope.empresas.disponibles;
      for (var i = 0; i < empresa_dis.length; i++) {
        if (empresa_dis[i].id == index) {
          $scope.empresas.asignadasTemporal.push(empresa_dis[i]);
          $scope.empresas.disponibles.splice(i, 1);
        }
      }
    };
    $scope.quitarEmpresa = function(index) {
      //console.log("quitado"+index);
      var empresa_temp = $scope.empresas.asignadasTemporal;
      for (var i = 0; i < empresa_temp.length; i++) {
        if (empresa_temp[i].id == index) {
          $scope.empresas.disponibles.push(empresa_temp[i]);
          $scope.empresas.asignadasTemporal.splice(i, 1);
        }
      }
    };
    $scope.aplicarEmpresa = function() {
      var empresa_temp = $scope.empresas.asignadasTemporal;
      for (var i = 0; i < empresa_temp.length; i++) {
        if (empresa_temp[i].id != '') {
          $scope.empresas.asignadas.push(empresa_temp[i].id);
        }
      }
      $scope.asignarEmpresas($scope.empresas.asignadas);
    };

    $scope.asociando = false;
    $scope.asignarEmpresas = function(empresas_arg) {
      //  $scope.empresas_asociar = "AAAAA";
      //console.log("empresas_arg:");
      $scope.asociando = true;
      console.log(empresas_arg);
      /*
                var req = {
                    method: 'POST',
                    url: febosSingleton.api + "/usuarios/" + $scope.usuario_id + "/empresas",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        empresas: empresas_arg.join(",")
                    }
                };

                $http(req).then(function (response) {
                    //console.log("resultado asignacion empresas", response);
                    $scope.asociando = false;
                    var modal = UIkit.modal("#mdAsignarEmpresas");
                    modal.hide();
                });
                */
      var query = {
        usuarioId: $scope.usuario_id
      };
      var body = {
        empresas: empresas_arg.join(',')
      };
      FebosAPI.cl_asignar_empresa_usuario(query, body, true, false).then(function(response) {
        //console.log("resultado asignacion empresas", response);
        $scope.asociando = false;
        var modal = UIkit.modal('#mdAsignarEmpresas');
        modal.hide();
      });
    };

    $scope.cargarArbolDeGrupos();
    $scope.cargarUsuarios();
  }
]);
