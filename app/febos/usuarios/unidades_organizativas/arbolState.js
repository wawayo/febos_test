febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {
                $stateProvider.state("restringido.arbol_grupos", {
                    url: "/:app/usuarios/organizacion",
                    templateUrl: 'app/febos/usuarios/unidades_organizativas/arbolView.html',
                    controller: 'arbolUsuariosCtrl',
                    resolve: {
                        ath:[
                            function(){
                                console.log("AQUI!");
                            }
                        ],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_uikit', 
                                    'lazy_iCheck',
                                    'lazy_tree',                                
                                    'app/febos/usuarios/unidades_organizativas/arbolController.js'
                                ]);
                            }]
                    }
                })
            }]);

