febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.usuarios_invitar', {
      url: '/:app/usuarios/invitar/:vista',
      templateUrl: 'app/febos/usuarios/invitar/invitarUsuariosView.html',
      controller: 'invitarUsuariosCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/usuarios/invitar/invitarUsuariosController.js'
            ]);
          }
        ],
        verificacionDeParametros: [
          '$stateParams',
          '$state',
          function($stateParams, $state) {
            var st = {};
            st.vista = $stateParams.vista;

            return st;
          }
        ]
      }
    });
  }
]);
