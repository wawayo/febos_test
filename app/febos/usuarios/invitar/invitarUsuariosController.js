angular
  .module('febosApp')
  .controller('invitarUsuariosCtrl', function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $http,
    utils,
    FebosUtil,
    $state,
    $stateParams,
    $location,
    FebosAPI,
    SesionFebos
  ) {
    $scope.paso = 1;
    $scope.buscando = false;
    $scope.creando = false;
    $scope.invitacionesOk = 0;
    $scope.invitacionesTotales = 0;
    $scope.region_options = [];
    $scope.provincia_options = [];
    $scope.comuna_options = [];
    $scope.vista = $stateParams.vista;
    $scope.datos = {
      empresa: {
        rut: '',
        nombre: '',
        fantasia: '',
        mensaje: '',
        telefono: '',
        correoElectronico: '',
        municipioId: '',
        region: '',
        provincia: '',
        comuna: '',
        regionAnterior: '',
        provinciaAnterior: '',
        comunaAnterior: ''
      },
      usuarios: [
        {
          nombre: '',
          correo: '',
          estado: 0
        }
      ]
    };

    $scope.region_config = {
      create: false,
      maxItems: 1,
      placeholder: 'Seleccione Región',
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      onChange: function() {
        $scope.actualizarLugar('provincia');
      }
    };

    $scope.provincia_config = {
      create: false,
      maxItems: 1,
      placeholder: 'Seleccione Provincia',
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      onChange: function() {
        $scope.actualizarLugar('comuna');
      }
    };

    $scope.comuna_config = {
      create: false,
      maxItems: 1,
      placeholder: 'Seleccione Comuna',
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      onChange: function() {
        $scope.actualizarLugar('municipio');
      }
    };

    // --
    $scope.agregar = function() {
      $scope.datos.usuarios.push({
        nombre: '',
        correo: '',
        estado: 0
      });
    };
    // --

    // --
    $scope.eliminar = function(idx) {
      $scope.datos.usuarios.splice(idx, 1);
    };
    // --

    // --
    $scope.verificar = function() {
      for (var i = 0; i < $scope.datos.usuarios.length; i++) {
        if ($scope.datos.usuarios[i].nombre == '' && $scope.datos.usuarios[i].correo == '') {
          $scope.datos.usuarios.splice(i, 1);
        }
      }

      for (var i = 0; i < $scope.datos.usuarios.length; i++) {
        if ($scope.datos.usuarios[i].nombre == '' || $scope.datos.usuarios[i].correo == '') {
          febosSingleton.error(
            'N/A',
            'Debe completar el nombre y correo de todos los invitados',
            1
          );
          return false;
        }

        if (!validarEmail($scope.datos.usuarios[i].correo)) {
          febosSingleton.error('N/A', 'Una o mas direcciones de correo no son válidas', 1);
          return false;
        }
      }

      return true;
    };
    // --

    // --
    $scope.invitar = function() {
      if (!$scope.verificar()) {
        return;
      }

      for (var i = 0; i < $scope.datos.usuarios.length; i++) {
        $scope.invitarUsuario($scope.datos.usuarios[i].nombre, $scope.datos.usuarios[i].correo, i);
      }
    };
    // --

    // --
    String.prototype.escapeSpecialChars = function() {
      return this.replace(/\\n/g, '\\n')
        .replace(/\\'/g, "\\'")
        .replace(/\\"/g, '\\"')
        .replace(/\\&/g, '\\&')
        .replace(/\\r/g, '\\r')
        .replace(/\\t/g, '\\t')
        .replace(/\\b/g, '\\b')
        .replace(/\\f/g, '\\f');
    };
    // --

    // -- función que invita a un usuario proveedor, para que ingrese por el portal proveedores
    $scope.invitarUsuario = function(nombre, correo, index) {
      $scope.datos.usuarios[index].estado = 1;
      $scope.invitacionesTotales++;

      /*
        var req = { //< request
            method: 'POST',
            url: febosSingleton.api + "/empresas/" + febosSingleton.empresa.iut + "/proveedores",
            params: {},
            data: {
                "correo": correo,
                "nombre": nombre,
                "rutEmpresaProveedora": $scope.datos.empresa.rut,
                "mensaje": $scope.datos.empresa.mensaje.escapeSpecialChars(),
                "dominio": window.location.host
            }
        }

        console.log(req);

        $http(req).then(function (response) {
            console.log(response);

            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                $scope.datos.usuarios[index].estado = 2;
                $scope.invitacionesOk++;
            } else {
                $scope.datos.usuarios[index].estado = 3;
            }

        }, function (response) {
            $scope.datos.usuarios[index].estado = 3;
            console.log(response);  //provinciaAnterior
            febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
        });
        */

      $scope.ambito = FebosUtil.obtenerAmbito();

      var body = {
        correo: correo,
        nombre: nombre,
        rutEmpresaProveedora: $scope.datos.empresa.rut,
        mensaje: $scope.datos.empresa.mensaje.escapeSpecialChars(),
        dominio: window.location.host,
        vista: $scope.vista
      };
      FebosAPI.cl_invitar_usuario_proveedor(
        {
          empresaId: SesionFebos().empresa.iut,
          ambito: $scope.ambito
        },
        body,
        true,
        false
      ).then(function(response) {
        if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
          $scope.datos.usuarios[index].estado = 2;
          $scope.invitacionesOk++;
        } else {
          $scope.datos.usuarios[index].estado = 3;
        }
      });
    };
    // --

    // --
    $scope.reset = function() {
      $scope.paso = 1;
      $scope.datos = {
        empresa: {
          rut: '',
          nombre: '',
          fantasia: '',
          mensaje: '',
          telefono: '',
          correoElectronico: '',
          municipioId: '',
          region: '',
          provincia: '',
          comuna: '',
          regionAnterior: '',
          provinciaAnterior: '',
          comunaAnterior: ''
        },
        usuarios: [
          {
            nombre: '',
            correo: '',
            estado: 0
          }
        ]
      };
    };
    // --
    // -- busca empresa al momento que ingresa el rut para invitar a un proveedor, si existe retorna los datos
    $scope.buscarEmpresa = function() {
      if (!$scope.datos.empresa.rut || !isRut($scope.datos.empresa.rut)) {
        febosSingleton.error('N/A', 'Debe especificar un RUT válido', 1);
        return;
      }

      $scope.buscando = true;
      /*
        var req = { //< request
            method: 'GET',
            url: febosSingleton.api + "/empresas/" + $scope.datos.empresa.rut,
            params: {
                pagina: 1,
                filas: 100
            },
            data: {
            }
        }

        console.log(req);

        $http(req).then(function (response) {
            console.log(response);
            $scope.buscando = false;

            try {
                if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                    $scope.paso = 3;
                    $scope.datos.empresa.nombre = response.data.razonSocial;
                    $scope.datos.empresa.fantasia = response.data.fantasia;
                } else if (response.data.codigo == 151) {
                    $scope.paso = 2;
                } else {
                    febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
                }

            } catch (e) {
                console.log(e);
                febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
            }

        }, function (response) {
            $scope.buscando = false;
            console.log(response);
            febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
        });
        */

      FebosAPI.cl_info_empresa(
        {
          empresaId: $scope.datos.empresa.rut,
          pagina: 1,
          filas: 100
        },
        {},
        false,
        false
      ).then(
        function(response) {
          $scope.buscando = false;
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            $scope.paso = 3;
            $scope.datos.empresa.nombre = response.data.razonSocial;
            $scope.datos.empresa.fantasia = response.data.fantasia;
          } else if (response.data.codigo == 151) {
            $scope.paso = 2;
          }
        },
        function(err) {
          $scope.buscando = false;
          console.log(err);
        }
      );
    };
    // --

    // -- función que crea una empresa si no existe
    $scope.crearEmpresa = function() {
      $scope.creando = true;
      /*
        var req = { //< request
            method: 'POST',
            url: febosSingleton.api + "/empresas/",
            data: {
               "razonSocial": $scope.datos.empresa.nombre,
                "fantasia": $scope.datos.empresa.nombre,
                "iut": $scope.datos.empresa.rut,
                "direccion": $scope.datos.empresa.direccion,
                "municipioId": $scope.datos.empresa.municipioId,
                "telefono": $scope.datos.empresa.telefono,
                "correoElectronico": $scope.datos.empresa.correoElectronico,
                "regimen": ""
            }
        }

        console.log(req);

        $http(req).then(function (response) {
            console.log(response);
            $scope.buscando = false;

            try {
                if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                    $scope.paso = 3;
                    $scope.creando = false;
                } else if (response.data.codigo == 151) {
                    $scope.paso = 2;
                    $scope.creando = false;
                } else {
                    febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
                    $scope.creando = false;
                }

            } catch (e) {
                console.log(e);
                febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
                $scope.creando = false;
            }

        }, function (response) {
            $scope.buscando = false;
            console.log(response);
            febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
            $scope.creando = false;
        });
        */
      var body = {
        razonSocial: $scope.datos.empresa.nombre,
        fantasia: $scope.datos.empresa.nombre,
        iut: $scope.datos.empresa.rut,
        direccion: $scope.datos.empresa.direccion,
        municipioId: $scope.datos.empresa.municipioId,
        telefono: $scope.datos.empresa.telefono,
        correoElectronico: $scope.datos.empresa.correoElectronico,
        regimen: ''
      };
      FebosAPI.cl_crear_empresa({}, body, true, false).then(function(response) {
        $scope.buscando = false;
        if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
          $scope.paso = 3;
          $scope.creando = false;
        } else if (response.data.codigo == 151) {
          $scope.paso = 2;
          $scope.creando = false;
        }
      });
    };
    // --

    // -- función que valida correo electrónico, por medio de una expresión regular
    function validarEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }
    // --

    // -- función que valida el RUT
    function isRut(rutCompleto) {
      if (!/^[0-9]+-[0-9kK]{1}$/.test(rutCompleto)) {
        return false;
      }

      var tmp = rutCompleto.split('-');
      var digv = tmp[1];
      var rut = tmp[0];

      if (digv == 'K') {
        digv = 'k';
      }

      var digesto = isDv(rut);

      if (digesto == digv) {
        return true;
      } else {
        return false;
      }
    }
    // --

    // -- función que valida el DV del rut
    function isDv(T) {
      var M = 0;
      var S = 1;

      for (; T; T = Math.floor(T / 10)) {
        S = (S + (T % 10) * (9 - (M++ % 6))) % 11;
      }

      return S ? S - 1 : 'k';
    }
    // --

    // --
    $scope.actualizarLugar = function(lugar) {
      var enviar = false;

      if (lugar == 'region') {
        var req = {
          //< request
          method: 'GET',
          url: febosSingleton.api + '/regiones'
        };

        enviar = true;
      }

      if (
        lugar == 'provincia' &&
        $scope.datos.empresa.region != $scope.datos.empresa.regionAnterior
      ) {
        var req = {
          //< request
          method: 'GET',
          url: febosSingleton.api + '/regiones/' + $scope.datos.empresa.region + '/provincias'
        };

        enviar = true;
      }

      if (
        lugar == 'comuna' &&
        $scope.datos.empresa.provincia != $scope.datos.empresa.provinciaAnterior
      ) {
        var req = {
          //< request
          method: 'GET',
          url:
            febosSingleton.api +
            '/regiones/' +
            $scope.datos.empresa.region +
            '/provincias/' +
            $scope.datos.empresa.provincia +
            '/comunas'
        };

        enviar = true;
      }

      if (
        lugar == 'municipio' &&
        $scope.datos.empresa.comuna != $scope.datos.empresa.comunaAnterior
      ) {
        $scope.datos.empresa.municipioId = $scope.datos.empresa.comuna;
      }

      if (enviar) {
        $scope.datos.empresa[lugar + 'Anterior'] = '';
        $scope.datos.empresa[lugar] = '';
        $scope[lugar + '_options'] = ['Cargando...'];

        if (lugar == 'region') {
          $scope.datos.empresa['provinciaAnterior'] = '';
          $scope.datos.empresa['provincia'] = '';
          $scope['provincia_options'] = ['Cargando...'];
        }

        if (lugar == 'provincia' || lugar == 'region') {
          $scope.datos.empresa['comunaAnterior'] = '';
          $scope.datos.empresa['comuna'] = '';
          $scope['comuna_options'] = ['Cargando...'];
        }

        $http(req).then(
          function(response) {
            try {
              if (response.data.codigo == 10) {
                $scope.datos.empresa[lugar + 'Anterior'] = '';
                $scope.datos.empresa[lugar] = '';
                $scope[lugar + '_options'] = response.data.lugares;

                if (lugar == 'region') {
                  $scope.datos.empresa['provinciaAnterior'] = '';
                  $scope.datos.empresa['provincia'] = '';
                  $scope['provincia_options'] = [];
                }

                if (lugar == 'provincia' || lugar == 'region') {
                  $scope.datos.empresa['comunaAnterior'] = '';
                  $scope.datos.empresa['comuna'] = '';
                  $scope['comuna_options'] = [];
                }
              } else {
                febosSingleton.error(
                  response.data.codigo,
                  response.data.mensaje,
                  response.data.seguimientoId,
                  response.data.errores
                );
              }
            } catch (e) {
              febosSingleton.error(
                response.data.codigo,
                response.data.mensaje,
                response.data.seguimientoId,
                response.data.errores
              );
            }
          },
          function(response) {
            febosSingleton.error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId,
              response.data.errores
            );
          }
        );
      }
    };
    // --

    //$scope.actualizarLugar("region");
  });
