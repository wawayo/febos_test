febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.boletasgarantia', {
      url: '/:app/documentos/portafolio/boletas/garantias/:categoria/:vista',
      templateUrl: 'app/febos/documentos/portafolio/boletasgarantia/documentosView.html',
      controller: 'boletaGarantiaCtrl',
      //reloadOnSearch: false,
      params: {
        codigo: '',
        responsable: ''
      },
      data: {
        pageTitle: 'Boletas de Garantía'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            console.log('ANTERIOR');
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            console.log('NUEVO');
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            console.log('LISTAR', $stateParams);
            return FebosAPI.cl_listar_boleta_garantia(
              { tipo: $stateParams.categoria },
              {
                codigo: $stateParams.codigo,
                responsable: $stateParams.responsable
              },
              true,
              true
            ).then(function(response) {
              console.log('listados');
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            console.log('laz');
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/aprobaciones/aprobacionesService.js',
              'app/febos/documentos/portafolio/boletasgarantia/documentosController.js'
            ]);
          }
        ]
      }
    });
  }
]);
