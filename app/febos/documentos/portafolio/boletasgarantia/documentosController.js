angular.module('febosApp').controller('boletaGarantiaCtrl', [
  '$rootScope',
  '$scope',
  '$timeout',
  'variables',
  '$location',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  'MONEDAS',
  'TIPOSGARANTIA',
  'Datos',
  '$state',
  '$stateParams',
  'VistasPortafolio',
  '$http',
  'ComplementoPortafolio',
  'AprobacionService',
  function(
    $rootScope,
    $scope,
    $timeout,
    variables,
    $location,
    SesionFebos,
    FebosAPI,
    FebosUtil,
    _MONEDAS,
    _TIPOSGARANTIA,
    Datos,
    $state,
    $stateParams,
    VistasPortafolio,
    $http,
    ComplementoPortafolio,
    AprobacionService
  ) {
    FebosAPI['cl_aprobaciones_ejecucion_crear'] = FebosAPI.generarFuncionParaLlamadaAPI(
      'cl_aprobaciones_ejecucion_crear',
      'POST',
      "'/aprobaciones/ejecucion'"
    );

    $scope.ambiente = '';
    $scope.cargando = false;
    try {
      var partes = window.location.href
        .replace('https://', '')
        .replace('http://', '')
        .split('/');
      $scope.ambiente = partes[1].match(posiblesAmbientes) != null ? partes[1] : 'desarrollo';
    } catch (e) {
      $scope.ambiente = 'desarrollo';
    }
    $.datepicker.setDefaults({
      dateFormat: 'YYYY-MM-DD',
      i18n: {
        months: [
          'Enero',
          'Febrero',
          'Marzo',
          'Abril',
          'Mayo',
          'Junio',
          'Julio',
          'Agosto',
          'Septiembre',
          'Octubre',
          'Noviembre',
          'Diciembre'
        ],
        weekdays: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
      }
    });
    $scope.app = $stateParams.app;
    $scope.categoria = $stateParams.categoria;
    $scope.vista = $stateParams.vista;
    var query = {};
    $scope.tituloVista = 'Garantía';
    $location.search(query);

    $scope.mdlEnviarBoleta = UIkit.modal('#mdlEnviarBoleta');
    $scope.mdlEnviarBoletaCustodia = UIkit.modal('#mdlEnviarBoletaCustodia');
    $scope.mdlDocumentosBoleta = UIkit.modal('#mdlDocumentosBoleta');
    $scope.mdlRenovarBoleta = UIkit.modal('#mdlRenovarBoleta');
    $scope.mdlBitacoraBoleta = UIkit.modal('#mdlBitacoraBoleta');
    $scope.mdlComentarBoleta = UIkit.modal('#mdlComentarBoleta');
    $scope.mdlContactosBoleta = UIkit.modal('#mdlContactosBoleta');
    $scope.mdlFlujoAsignar = UIkit.modal('#mdlFlujoAsignar');
    $scope.mdlFlujoEjecucion = UIkit.modal('#mdlFlujoEjecucion');
    $scope.modales = FebosUtil.modales(['portafolio_modales_proyectos_crear']);
    //$scope.modales.portafolioModalesProyectosCrear.show();

    $scope.proyecto = {
      topicos: []
    };
    $scope.FebosUtil = FebosUtil;

    $scope.transformarFechaReverso = function(fecha) {
      try {
        var d = new Date(fecha);
        var m = d.getMonth() + 1;
        var dia = d.getDate();
        m = m < 10 ? '0' + m : m;
        ddia = dia < 10 ? '0' + d : d;
        return d.getFullYear() + '-' + m + '-' + dia;
      } catch (e) {
        console.log(e);
        return '';
      }
    };
    $scope.conceptos = [
      { nombre: 'Boleta', arreglo: 'boletas' },
      { nombre: 'Anticipo', arreglo: 'anticipos' },
      { nombre: 'Poliza', arreglo: 'polizas' },
      { nombre: 'Estado', arreglo: 'estados' },
      { nombre: 'Contacto', arreglo: 'contactos' }
    ];

    $scope.documentos = Datos.boletas;

    /* $scope.proyectos = Datos.proyectos;

         for (var i = 0; i < $scope.proyectos.length; i++) {
             for (var x = 0; x < $scope.proyectos[i].topicos.length; x++) {
                 try {
                     var items = $scope.proyectos[i].topicos[x].boletas;
                     for (var b = 0; b < items.length; b++) {
                         var item = items[b];
                         item.proyecto = JSON.parse(JSON.stringify($scope.proyectos[i]));
                         $scope.documentos.push(item);
                     }
                 } catch (e) {
                 }
             }
         }*/

    $scope.parametros = $location.search();
    $scope.variablesPaginacion = {
      pagina: parseInt($location.search().pagina),
      itemsPorPagina: parseInt($location.search().itemsPorPagina)
      //itemsPorPagina: Datos.documentos.length,
    };

    $scope.paginacion = {
      //elementoDesde: (((parseInt($location.search().pagina) - 1) * parseInt($location.search().itemsPorPagina)) + 1),
      //elementoHasta: (((parseInt($location.search().pagina) - 1) * parseInt($location.search().itemsPorPagina)) + Datos.documentos.length),
      //total: Datos.totalElementos,
      //paginas: parseInt(Datos.totalPaginas)
    };
    var $documentos = $('#documentos');

    if (typeof SesionFebos().documentosNoTributariosSeleccionados == 'undefined') {
      SesionFebos().documentosNoTributariosSeleccionados = [];
    }
    $scope.documentosSeleccionados = SesionFebos().documentosNoTributariosSeleccionados;
    $scope.estaSeleccionado = function(id) {
      if ($.inArray(id, $scope.documentosSeleccionados) >= 0) {
        return true;
      } else {
        return false;
      }
    };

    $scope.listarSeleccionados = function() {
      var params = $location.search();
      params.filtros = 'febosId:' + $scope.documentosSeleccionados.join(',');
      params.pagina = 1;
      $location.search(params);
      $state.go('restringido.dnt', {
        app: $stateParams.app,
        categoria: $stateParams.categoria,
        vista: $stateParams.vista,
        filtros: params.filtros,
        orden: $stateParams.orden,
        pagina: params.pagina,
        itemsPorPagina: params.itemsPorPagina
      });
      $state.reload();
    };
    $scope.deseleccionarTodo = function() {
      var cantidad = $scope.documentosSeleccionados.length;
      $timeout(function() {
        $documentos.find('.select_message').each(function() {
          if ($(this).is(':checked')) {
            $(this).iCheck('uncheck');
          }
        });
        while (typeof $scope.documentosSeleccionados.pop() != 'undefined');
      }, 100);

      var mensaje =
        cantidad == 1
          ? 'Se deseleccionó 1 documento'
          : 'Se deseleccionaron ' + cantidad + ' documentos';
      UIkit.notify(mensaje + "<a class='notify-action'>[X]</a> ", {
        status: 'success',
        timeout: 3000
      });

      // UIkit.notify(mensaje + "<a class='notify-action'>[X]</a> ", {status: "success", timeout: 3000});
    };
    $scope.escanearDocumentosSeleccionados = function() {
      //revisar si habian documentos guardados en la sesion
      var seleccionados = [];
      var deseleccionados = [];
      $documentos.find('.select_message').each(function(index, elemento) {
        var febosId = $(elemento)
          .attr('id')
          .split('_')[1];
        if ($(elemento).is(':checked')) {
          seleccionados.push(febosId);
        } else {
          deseleccionados.push(febosId);
        }
      });
      //insertar seleccionados
      $.each(seleccionados, function(i, el) {
        if ($.inArray(el, $scope.documentosSeleccionados) === -1) {
          $scope.documentosSeleccionados.push(el);
        }
      });
      var eliminar = [];
      $.each($scope.documentosSeleccionados, function(i, el) {
        if ($.inArray(el, deseleccionados) >= 0) {
          eliminar.push(i);
        }
      });
      //eliminar indices deseleccionados
      for (var i = eliminar.length - 1; i >= 0; i--) {
        $scope.documentosSeleccionados.splice(eliminar[i], 1);
      }
      setTimeout(function() {
        $scope.$apply();
      }, 200);
    };
    // seleccionar un documento
    $documentos.on('ifChanged', '.select_message', function() {
      $(this).is(':checked')
        ? $(this)
            .closest('li')
            .addClass('md-card-list-item-selected')
        : $(this)
            .closest('li')
            .removeClass('md-card-list-item-selected');
      $scope.escanearDocumentosSeleccionados();
    });

    // seleccionar todos los documentos
    $('#documentos_seleccionar_todos').on('ifChanged', function() {
      var $this = $(this);
      $documentos.find('.select_message').each(function() {
        $this.is(':checked') ? $(this).iCheck('check') : $(this).iCheck('uncheck');
      });
      $scope.escanearDocumentosSeleccionados();
    });

    // ver el detalle de un documento
    $scope.hayUnDetalleDesplegado = false;
    $documentos.on('click', '.md-card-list ul > li.seleccionable > div.lista-item-head', function(
      e
    ) {
      if (
        !$(e.target).closest('lista-item-head').length &&
        !$(e.target).closest('.md-card-list-item-select').length
      ) {
        var $this = $(this).parent();
        if (!$this.hasClass('item-shown')) {
          // obtener la altura del detalle del documento
          var el_min_height =
            $this.height() + $this.children('.md-card-list-item-content-wrapper').actual('height');
          // esconder el contenido del mensaje
          $documentos.find('.item-shown').velocity('reverse', {
            begin: function(elements) {
              $(elements)
                .removeClass('item-shown')
                .children('.md-card-list-item-content-wrapper')
                .hide()
                .velocity('reverse');
            }
          });
          // mostrar el mensaje
          $this.velocity(
            {
              marginTop: 40,
              marginBottom: 40,
              marginLeft: 0,
              marginRight: 0,
              minHeight: el_min_height
            },
            {
              duration: 300,
              easing: variables.easing_swiftOut,
              begin: function(elements) {
                $(elements).addClass('item-shown');
              },
              complete: function(elements) {
                // show: message content, reply form
                $(elements)
                  .children('.md-card-list-item-content-wrapper')
                  .show()
                  .velocity({
                    opacity: 1
                  });
                // scroll to message
                var container = $('body'),
                  scrollTo = $(elements);
                container.animate(
                  {
                    scrollTop: scrollTo.offset().top - $('#page_content').offset().top - 8
                  },
                  1000,
                  variables.bez_easing_swiftOut
                );
                $scope.hayUnDetalleDesplegado = true;
              }
            }
          );
        }
      }
    });
    // esconder el detalle del documento al: hacer click afuera, apretar escape
    $(document).on('click keydown', function(e) {
      if (!$(e.target).closest('li.item-shown').length || e.which == 27) {
        $documentos.find('.item-shown').velocity('reverse', {
          begin: function(elements) {
            $(elements)
              .removeClass('item-shown')
              .children('.md-card-list-item-content-wrapper')
              .hide()
              .velocity('reverse');
          }
        });
      }
    });

    $scope.avanzarPagina = function() {
      var params = verificacionDeParametros;
      var siguientePagina = parseInt(params.pagina) + 1;
      if (siguientePagina > $scope.paginacion.paginas) {
        UIkit.notify("No hay mas páginas <a class='notify-action'>[X]</a> ", {
          status: 'danger',
          timeout: 2000
        });
      } else {
        $location.search('pagina', siguientePagina);
        $state.go('restringido.dnt', {
          app: $stateParams.app,
          categoria: $stateParams.categoria,
          vista: $stateParams.vista,
          filtros: $stateParams.filtros,
          orden: $stateParams.orden,
          pagina: siguientePagina,
          itemsPorPagina: $stateParams.itemsPorPagina
        });
        $state.reload();
      }
    };
    $scope.retrocederPagina = function() {
      var params = verificacionDeParametros.pagina;
      var siguientePagina = parseInt(params.pagina) - 1;
      if (siguientePagina < 1) {
        UIkit.notify(
          "Ésta es la primera página, no se puede <br/>retroceder más =( <a class='notify-action'>[X]</a> ",
          {
            status: 'danger',
            timeout: 2000
          }
        );
      } else {
        $location.search('pagina', siguientePagina);
        $state.go('restringido.dnt', {
          app: $stateParams.app,
          categoria: $stateParams.categoria,
          vista: $stateParams.vista,
          filtros: $stateParams.filtros,
          orden: $stateParams.orden,
          pagina: siguientePagina,
          itemsPorPagina: $stateParams.itemsPorPagina
        });
        $state.reload();
      }
    };
    $scope.mostrarInputPagina = false;
    $scope.paginaIngresada = 1;
    $scope.irPagina = function(mostrarInputPagina, valor) {
      $scope.mostrarInputPagina = mostrarInputPagina;
      if (mostrarInputPagina) {
        $scope.paginaIngresada = parseInt($location.search().pagina);
      } else {
        var pagina = parseInt($('#paginaManual')[0].value);
        if (pagina > $scope.paginacion.paginas || pagina <= 0) {
          UIkit.notify(
            'Imposible ir la página ' +
              pagina +
              ", ya que no existe <a class='notify-action'>[X]</a> ",
            {
              status: 'danger',
              timeout: 2000
            }
          );
        } else {
          $location.search('pagina', pagina);
          $state.go('restringido.dnt', {
            app: $stateParams.app,
            categoria: $stateParams.categoria,
            vista: $stateParams.vista,
            filtros: $stateParams.filtros,
            orden: $stateParams.orden,
            pagina: pagina,
            itemsPorPagina: $stateParams.itemsPorPagina
          });
          $state.reload();
        }
      }
      //var params = $location.search();
      //$location.search("pagina", parseInt(params.pagina) + 1);
    };

    $scope.comprarSiLosFiltrosSonIguales = function(A, B) {
      ////console.log("comparando", A, B);
      if (A.filtros == B.filtros && A.orden == B.orden) {
        return true;
      }
      return false;
    };
    $scope.detectarTitulo = function() {
      var query = $location.search();
      if (typeof SesionFebos().filtrosDnt != 'undefined') {
        for (var i = 0; i < SesionFebos().filtrosDnt[$stateParams.categoria].length; i++) {
          var parametros = SesionFebos().filtrosDnt[$stateParams.categoria][i];
          if ($scope.comprarSiLosFiltrosSonIguales(query, parametros.params)) {
            return (
              VistasPortafolio[$stateParams.app][$stateParams.categoria][$stateParams.vista]
                .titulo +
              ' aplicando mi vista: ' +
              SesionFebos().filtrosDnt[$stateParams.categoria][i].nombre
            );
          }
        }
      }
      return VistasPortafolio[$stateParams.app][$stateParams.categoria][$stateParams.vista].titulo;
    };
    //$scope.tituloVista = $scope.detectarTitulo();
    //$rootScope.$on('refrescarTituloDnt', function () {
    //    $scope.tituloVista = $scope.detectarTitulo();
    //})
    $scope.formatearFecha = function(fechaComoNumero) {
      if (typeof fechaComoNumero != 'undefined' && fechaComoNumero != '' && fechaComoNumero > 0) {
        var fecha = new Date(fechaComoNumero);
        var dia = fecha.getDate();
        dia = dia < 10 ? '0' + dia : dia;
        var mes = fecha.getMonth() + 1;
        mes = mes < 10 ? '0' + mes : mes;
        return dia + '-' + mes + '-' + fecha.getFullYear();
      }
      return '';
    };
    $scope.formatearMonto = function(numero, simbolo) {
      if (typeof numero != 'undefined') {
        var partes = numero.toString().split('.');
        partes[0] = partes[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
        return simbolo + ' ' + partes.join(',');
      } else {
        return '';
      }
    };
    $scope.obtenerMonto = function(documento) {
      if (documento.montoDecimal * 1 > 0) {
        return documento.montoDecimal;
      }
      return documento.monto;
    };
    $scope.calcularAvanceProyecto = function(proyecto) {
      /* var estadosTotales = 0;
             var estadosCompletados = 0;
             for (var i = 0; i < proyecto.topicos.length; i++) {
                 var estados = proyecto.topicos[i].estados.length;
                 estados = isNaN(estados) ? 0 : estados;
                 estadosTotales += estados;
                 if (proyecto.topicos[i].estados.length > 0) {
                     for (var j = 0; j < proyecto.topicos[i].estados.length; j++) {
                         if (proyecto.topicos[i].estados[j].estado == 1 || proyecto.topicos[i].estados[j].estado == 2) {
                             estadosCompletados++;
                         }
                     }
                 }
             }
             try {
                 var t=Math.round(estadosCompletados * 100 / estadosTotales);
                 return isNaN(t)?0:t;
             } catch (e) {
                 return 0;
             }*/
    };

    $scope.agregarHito = function() {
      $scope.proyecto.topicos.push({});
    };
    $scope.eliminarHito = function(indice) {
      $scope.proyecto.topicos.splice(indice, 1);
    };

    $scope.crearProyecto = function() {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Guardando proyecto...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_crear_proyecto({}, { proyecto: $scope.proyecto }).then(function(response) {
        console.log(response);
      });
    };
    $scope.buscar = '';
    $scope.busqueda = function(item) {
      //if($scope.buscar == "")return true;
      if (
        JSON.stringify(item)
          .toLowerCase()
          .includes($scope.buscar.toLowerCase())
      ) {
        return true;
      }
      return false;
    };
    $scope.uploadArchivo = function(id, mime, url) {
      FebosAPI.cl_obtener_url_prefirmada({}, {}, true, true).then(function(response) {
        console.log(response);
        var url = response.data.url;
      });
    };

    $scope.abritBitacora = function(boleta) {
      console.log(boleta);
      $scope.bitacorascargando = true;
      $scope.mdlBitacoraBoleta.show();
      $scope.bitacoras = [];
      FebosAPI.cl_listar_bitacora_portafolio(
        {
          documentoId: boleta.boletaId,
          pagina: 1,
          filas: 200
        },
        {},
        true,
        false
      ).then(function(response) {
        $scope.bitacorascargando = false;
        $scope.bitacoras = response.data.bitacora;
      });
    };
    $scope.modalAgregarBoleta = function() {
      $scope.inserar = true;
      $scope.ModalBoletaTitle = 'Insertar ';
      $scope.mensajeActualizacion = 'Insertada ';
      $scope.renovacion = {};
      $scope.renovacion.boletaId = new Date().valueOf() + '-' + FebosUtil.uuid();
      $scope.mdlRenovarBoleta.show();
    };
    $scope.modalEnviar = function(boletaActual) {
      $scope.mdlEnviarBoleta.show();
      $scope.boletaActual = boletaActual;
    };
    $scope.modalEnviarCustodia = function(boletaActual) {
      $scope.mdlEnviarBoletaCustodia.show();
      $scope.boletaActual = boletaActual;
    };
    $scope.enviarCustodia = function() {
      $scope.mdlEnviarBoletaCustodia.hide();
      $scope.boletaActual.enCustodia = 1;
      $scope.guardarBoletaActual();
    };

    $scope.modalDocumentos = function(boletaActual) {
      boletaActual.adjuntos = boletaActual.adjuntos || new Array();
      $scope.boletaActual = boletaActual;
      console.log('BOLETA ', boletaActual);
      $scope.mdlDocumentosBoleta.show();
    };

    $scope.modalContacto = function(boletaActual) {
      $scope.boletaActual = boletaActual;
      $scope.nuevocontacto = {
        nombre: '',
        correo: '',
        telefono: '',
        referencia: ''
      };
      $scope.mdlContactosBoleta.show();
    };
    $scope.agregarContactoLista = function(boletaActual) {
      $scope.boletaActual.contactos = $scope.boletaActual.contactos || new Array();
      $scope.boletaActual.contactos.push(JSON.parse(JSON.stringify($scope.nuevocontacto)));
      $scope.nuevocontacto = {};
    };
    $scope.modalObservacion = function(boletaActual) {
      $scope.boletaActual = boletaActual;

      $scope.nuevocomentario = {};
      $scope.mdlComentarBoleta.show();
    };
    $scope.agregarComentarioLista = function(boletaActual) {
      console.log('usuario ', SesionFebos().usuario);
      $scope.nuevocomentario.fecha = new Date().getTime();
      $scope.nuevocomentario.autor = SesionFebos().usuario;
      $scope.boletaActual.comentarios = $scope.boletaActual.comentarios || new Array();
      $scope.boletaActual.comentarios.push(JSON.parse(JSON.stringify($scope.nuevocomentario)));
      $scope.nuevocomentario = {};
    };
    $scope.guardarBoletaActual = function() {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Guardando cambios en garantía...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_crear_documento_portafolio(
        {},
        {
          boleta: $scope.boletaActual
        },
        true,
        true
      ).then(function(response) {
        UIkit.notify(
          "<i style='color:white' class='uk-icon-check'></i> Garantía " +
            ($scope.mensajeActualizacion || 'Actualizada') +
            '!',
          {
            status: 'success',
            timeout: 5000,
            pos: 'top-center'
          }
        );
        try {
          $scope.mdlRenovarBoleta.hide();
          $scope.mdlContactosBoleta.hide();
        } catch (e) {}
      });
    };
    $scope.modalRenovar = function(boletaActual) {
      $scope.inserar = false;
      $scope.ModalBoletaTitle = 'Renobar ';
      $scope.mensajeActualizacion = 'Renobada ';

      $scope.boletaActual = boletaActual;
      console.log('RENOVACION  ', $scope.boletaActual);
      $scope.mdlRenovarBoleta.show();
      $scope.renovacion = {};
      $scope.renovacion.boletaId = new Date().valueOf() + '-' + FebosUtil.uuid();
      $scope.renovacion.proyectoId = $scope.boletaActual.proyectoId;
      $scope.renovacion.topicoId = $scope.boletaActual.topicoId;
      $scope.renovacion.empresa = $scope.boletaActual.empresa;
      $scope.renovacion.boletaGarantiaReferenciaId = $scope.boletaActual.boletaId;
    };
    $scope.enviar = function(mensaje, copias, destinatario) {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Enviando garantía...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      console.log('enviando boleta', $scope.boletaActual);
      FebosAPI.cl_correo_envio_portafolio(
        {
          febosId: $scope.boletaActual.boletaId,
          tipo: 'boleta',
          destinatario: destinatario,
          copias: copias == undefined ? '' : copias
        },
        { mensaje: mensaje == undefined ? '' : FebosUtil.encode(mensaje) },
        true,
        true
      ).then(function(response) {
        $scope.mdlEnviarBoleta.hide();
        UIkit.notify("<i style='color:white' class='uk-icon-check'></i> Garantía enviada!", {
          status: 'success',
          timeout: 5000,
          pos: 'top-center'
        });
        for (var i = 0; i < $scope.documentos.length; i++) {
          var doc = $scope.documentos[i];
          if (doc.boletaId == $scope.boletaActual.boletaId) {
            $scope.documentos = $scope.boletaActual;
          }
        }
      });

      $scope.mensaje = '';
      $scope.copias = '';
      $scope.destinatario = '';
    };

    $scope.renovar = function(boleta) {
      var b = JSON.parse(JSON.stringify(boleta));
      for (var key in b) {
        if (b.hasOwnProperty(key)) {
          if (key.includes('fecha')) {
            b[key] = new Date(b[key]).getTime();
          }
        }
      }

      b.estado = 1;
      b.vigente = 1;
      b.usuarioGestorNombre = SesionFebos().usuario.nombre;
      b.usuarioGestorCorreo = SesionFebos().usuario.correo;
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Renovando boleta de garantía...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_crear_documento_portafolio(
        {},
        {
          boleta: b
        },
        true,
        true
      ).then(function(response) {
        $scope.mdlRenovarBoleta.hide();
        UIkit.notify(
          "<i style='color:white' class='uk-icon-check'></i> Boleta " +
            $scope.mensajeActualizacion +
            '!',
          {
            status: 'success',
            timeout: 5000,
            pos: 'top-center'
          }
        );
        if ($scope.inserar) {
          $scope.documentos.push(b);
        }
      });
    };
    $scope.monedasOptions = JSON.parse(JSON.stringify(_MONEDAS));
    $scope.tiposGarantiaOptions = JSON.parse(JSON.stringify(_TIPOSGARANTIA));

    $scope.selectize_config_id_title = {
      maxItems: 1,
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      create: false,
      // dropdownParent: "body",
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.title) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.title) + '</div>';
        }
        //,
        //option_create: function (data, escape) {
        //    return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
        // }
      }
    };
    $scope.conceptosOptions = [
      //Seriedad de la Oferta, Fiel cumplimiento y anticipo de Fondos
      { value: 'Seriedad de la Oferta' },
      { value: 'Fiel cumplimiento' },
      { value: 'Anticipo de Fondos' }
    ];
    $scope.selectize_config_concepto = {
      maxItems: 1,
      valueField: 'value',
      labelField: 'value',
      searchField: 'value',
      create: true,
      dropdownParent: 'body',
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.value) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.value) + '</div>';
        }
      }
    };

    $scope.cargandoProyectos = true;
    $scope.proyectos = [];

    function cargarProyectos() {
      return new Promise(function(resolve, reject) {
        return FebosAPI.cl_listar_proyectos_cabecera({}, {}, true, true).then(function(response) {
          $scope.cargandoProyectos = false;
          $scope.proyectos = response.data.proyectos;
          $scope.proyectosOptions = JSON.parse(JSON.stringify(response.data.proyectos));
          resolve(response.data.proyectos);
        });
      });
    }

    $scope.obtenerDescripcionProyecto = function(doc) {
      if (doc.proyectoId) {
        for (var i = 0; i < $scope.proyectos.length; i++) {
          var proyecto = $scope.proyectos[i];
          if (proyecto.proyectoId == doc.proyectoId) {
            return '(' + proyecto.codigo + ') ' + proyecto.nombre;
          }
        }
      }
      return '-';
    };
    $scope.proyectosOptions = [];

    $scope.selectize_config_proyecto = {
      maxItems: 1,
      valueField: 'proyectoId',
      labelField: 'codigo',
      create: false,
      render: {
        option: function(data, escape) {
          return (
            '<div class="option" title="' +
            escape(data.codigo) +
            '" data-uk-tooltip="{cls:\'long-text\'}">' +
            '<span class="title" >(' +
            escape(data.codigo) +
            ') ' +
            escape(data.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return (
            '<div class="item" title="' +
            escape(data.codigo) +
            '" data-uk-tooltip="{cls:\'long-text\'}">' +
            escape(data.codigo) +
            '</div>'
          );
        }
      }
    };
    $scope.aprobacionesOptions = [];

    $scope.cargarAprobaciones = function() {
      return new Promise(function(resolve, reject) {
        if ($scope.aprobacionesOptions.length == 0) {
          FebosAPI.cl_aprobaciones_listar({ estado: true }, {}, true, true).then(function(data) {
            $scope.flujoEjecucionLlamada.cargandoejecuciones = false;
            $scope.aprobacionesOptions = data.data.aprobaciones;
            resolve(data.data.aprobaciones);
          });
        } else {
          resolve($scope.aprobacionesOptions);
        }
      });
    };
    $scope.selectize_config_aprovaciones = {
      maxItems: 1,
      valueField: 'id',
      labelField: 'nombre',
      create: false,
      render: {
        option: function(data, escape) {
          return (
            '<div class="option" title="' +
            escape(data.descripcion) +
            '" data-uk-tooltip="{cls:\'long-text\'}">' +
            '<span class="title" >' +
            escape(data.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return (
            '<div class="item" title="' +
            escape(data.descripcion) +
            '" data-uk-tooltip="{cls:\'long-text\'}">' +
            escape(data.nombre) +
            '</div>'
          );
        }
      }
    };
    cargarProyectos().then(function(data) {
      console.log('PROYECTOS ', data);
      return data;
    });

    $scope.modalFlujoAsignar = function(boletaActual) {
      $scope.boletaActual = boletaActual || {};
      $scope.flujoEjecucionLlamada = {
        cargandoejecuciones: true,
        ejecucion: {
          nombre: 'Flujo Garantia',
          descripcion: 'Flujo de aprobacion Garantia',
          aprobacionesReferenciaId: undefined,
          reversible: true
        },
        documentos: [
          {
            objetoId: $scope.boletaActual.boletaId,
            objetoNombre: 'GARANTIA: ',
            tipoObjetoId: 'BOLETA_GARANTIA',
            tipoObjetoTexto: 'BG',
            descripcion:
              'Garantia ' +
              ($scope.boletaActual.folio || 0) +
              '(' +
              ($scope.boletaActual.concepto || '-') +
              ')'
          }
        ]
      };
      $scope.flujoEjecucionLlamada.documentos[0].objetoNombre =
        'GARANTIA: ' +
        ($scope.boletaActual.tipoGarantia || '-').toString().toLocaleUpperCase() +
        ' Nº: ' +
        ($scope.boletaActual.folio || '-');
      $scope.flujoEjecucionLlamada.cargandoejecuciones = true;
      $scope.cargarAprobaciones().then(function(data) {
        console.log('APROBACIONES 5', data);
        $scope.flujoEjecucionLlamada.cargandoejecuciones = false;
      });
      $scope.mdlFlujoAsignar.show();
    };
    $scope.modalFlujoAsignarGuardar = function() {
      var guardar = JSON.parse(JSON.stringify($scope.flujoEjecucionLlamada));
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Guardando...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      console.log('GUARDAR ', guardar);
      FebosAPI.cl_aprobaciones_ejecucion_crear({}, guardar, true, true).then(function(response) {
        $scope.boletaActual.flujoAprobacionId = response.data.idAprobacion;
        $scope.guardarBoletaActual();
        $scope.mdlFlujoAsignar.hide();
      });
    };
    $scope.ejecucionActual = {};
    $scope.modalFlujoEjecucion = function(boletaActual) {
      $scope.boletaActual = boletaActual;
      $scope.ejecucionActual = { cargando: true };
      $scope.flujoEjecucionCargar($scope.boletaActual.flujoAprobacionId);
      $scope.mdlFlujoEjecucion.show();
    };

    $scope.flujoEjecucionCargar = function(ejecicionId) {
      FebosAPI.cl_ejecuciones_estado_actual({ ejecucion: ejecicionId }, {}).then(function(data) {
        $scope.ejecucionActual = data.data.detalle;
        $scope.ejecucionActual.cargando = false;
        $scope.mdlFlujoAsignar.hide();
      });
    };

    $scope.estadoAprobacion = function(observador) {
      for (var i = 0; i < $scope.ejecucionActual.pasos.length; i++) {
        var paso = $scope.ejecucionActual.pasos[i];
        if (paso.paso.tipoValorResponsableId == observador.observadorUsuarioId) {
          var estado = '-';
          console.log('paso.aprobacionesPasoEstadoId ' + paso.aprobacionesPasoEstadoId);
          switch (paso.aprobacionesPasoEstadoId * 1) {
            case 1:
              estado = 'DESESTIMADA';
              break;
            case 2:
              estado = 'PREPARADA';
              break;
            case 3:
              estado = 'INICIADA';
              break;
            case 4:
              estado = 'APROBADA';
              break;
            case 5:
              estado = 'RECHAZADA';
              break;
          }
          return estado;
        }
      }
      return '-';
    };
  }
]);
