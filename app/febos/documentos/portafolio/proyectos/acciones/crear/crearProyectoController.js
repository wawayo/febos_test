angular.module('febosApp').controller('crearProyectoCtrl', [
  '$rootScope',
  '$scope',
  'variables',
  '$location',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  'MONEDAS',
  'TIPOSGARANTIA',
  'Datos',
  'Provincias',
  'UnidadesOrganizativas',
  '$state',
  '$stateParams',
  '$http',
  '$timeout',
  '$interval',
  'SubTitulos',
  function(
    $rootScope,
    $scope,
    variables,
    $location,
    SesionFebos,
    FebosAPI,
    FebosUtil,
    _MONEDAS,
    _TIPOSGARANTIA,
    Datos,
    Provincias,
    UnidadesOrganizativas,
    $state,
    $stateParams,
    $http,
    $timeout,
    $interval,
    SubTitulos
  ) {
    $scope.provincias = Provincias;
    $scope.comunas = [];
    $scope.unidades = UnidadesOrganizativas;
    $scope.generarId = function() {
      return new Date().valueOf() + '-' + FebosUtil.uuid();
    };

    $scope.persistirTopico = function() {
      ////console.log("buscando topico", $scope.topico);
      for (var i = 0; i < $scope.proyecto.topicos.length; i++) {
        if ($scope.proyecto.topicos[i].topicoId == $scope.topico.topicoId) {
          ////console.log("topico encontrado!");
          $scope.proyecto.topicos[i] = JSON.parse(JSON.stringify($scope.topico));
        }
      }
    };
    $scope.changeProveedorRut = function() {
      ////console.log("cambio probeedor", $scope.topico);
      $scope.topico.proveedorRut = formatRut($scope.topico.proveedorRut || '0-0');
      ////console.log("cambiado probeedor", $scope.topico);
    };
    $scope.resetearProyecto = function() {
      ////console.log("reseteando proyecto");
      SesionFebos().proyecto = {
        proyectoId: $scope.generarId(),
        topicos: [],
        comentarios: [{}]
      };
      SesionFebos().topico = {};
      $scope.proyecto = SesionFebos().proyecto;
      $scope.topico = SesionFebos().topico;
      $scope.topico = {};
      $scope.prepararNuevoProyecto();
    };

    $scope.nuevoTopico = function() {
      ////console.log("creando nuevo topico");
      var indice = $scope.proyecto.topicos.push({
        proyectoId: $scope.proyecto.proyectoId,
        topicoId: $scope.generarId(),
        numero: $scope.proyecto.topicos.length + 1,
        anticipos: [],
        boletas: [],
        polizas: [],
        estados: [],
        contactos: []
      });
      $scope.persistirTopico();
      return indice;
    };
    $scope.transformarFechaReverso = function(fecha) {
      try {
        var d = new Date(fecha);
        var m = d.getMonth() + 1;
        var dia = d.getDate();
        m = m < 10 ? '0' + m : m;
        ddia = dia < 10 ? '0' + dia : dia;
        return d.getFullYear() + '-' + m + '-' + ddia;
      } catch (e) {
        //console.log(e);
        return '';
      }
    };
    $scope.prepararNuevoProyecto = function() {
      ////console.log("preparando nuevo proyecto");
      $scope.proyecto = {
        proyectoId: $scope.generarId(),
        topicos: [],
        comentarios: [{}]
      };
      $scope.proyecto.fechaInicio = $scope.transformarFechaReverso(new Date().getTime());
      //$scope.nuevoTopico();
    };

    $scope.resetearProyecto();

    $scope.modales = FebosUtil.modales([
      'portafolio_modales_proyectos_crear_configurar_topico',
      'portafolio_modales_proyectos_modificar_topico'
    ]);

    $scope.conceptos = [
      { nombre: 'Contrato', arreglo: 'contratos' },
      { nombre: 'Boleta', arreglo: 'boletas' },
      { nombre: 'Modificaciones', arreglo: 'modificacionesMandato' },
      { nombre: 'Anticipo', arreglo: 'anticipos' },
      { nombre: 'Devolucion', arreglo: 'devolucion' },
      { nombre: 'Poliza', arreglo: 'polizas' },
      { nombre: 'Estado', arreglo: 'estados' },
      { nombre: 'Contacto', arreglo: 'contactos' }
    ];
    //fabricar funciones de agregar y eliminar conceptor de los arreglos
    var buildAgregar = function(concepto) {
      return function() {
        var obj = {};
        obj['proyectoId'] = $scope.proyecto.proyectoId;
        var conceptoSingular = concepto.endsWith('s')
          ? concepto.substring(0, concepto.length - 1)
          : concepto;
        if (
          conceptoSingular == 'boleta' ||
          conceptoSingular == 'poliza' ||
          conceptoSingular == 'estado'
        ) {
          obj.estado = 1;
          obj.vigente = 1;
          obj.usuarioGestorNombre = SesionFebos().usuario.nombre;
          obj.usuarioGestorCorreo = SesionFebos().usuario.correo;
        }
        if (conceptoSingular == 'estado') {
          conceptoSingular = 'estadoPago';
        }
        obj[conceptoSingular + 'Id'] = $scope.generarId();
        obj['topicoId'] = $scope.topico.topicoId;
        if ($scope[conceptoSingular + 'Validador']) {
          $scope[conceptoSingular + 'Validador'](obj);
        }
        if ($scope.topico[concepto] == undefined) {
          $scope.topico[concepto] = [];
        }
        var indice = $scope.topico[concepto].push({});
        obj['indice'] = indice;
        $scope.topico[concepto][indice - 1] = obj;
        ////console.log("creando nuevo elemento: ", obj);
        if ($scope[conceptoSingular + 'Interceptor']) {
          $scope[conceptoSingular + 'Interceptor'](obj);
        }
      };
    };
    var buildEliminar = function(concepto) {
      return function(indice) {
        $scope.topico[concepto].splice(indice, 1);
      };
    };
    for (var i = 0; i < $scope.conceptos.length; i++) {
      var eliminar = 'eliminar' + $scope.conceptos[i].nombre;
      $scope['agregar' + $scope.conceptos[i].nombre] = buildAgregar($scope.conceptos[i].arreglo);
      $scope['eliminar' + $scope.conceptos[i].nombre] = buildEliminar($scope.conceptos[i].arreglo);
    }

    $scope.eliminarTopico = function(topico) {
      $scope.proyecto.topicos.splice(topico.numero - 1, 1);
      for (var i = 0; i < $scope.proyecto.topicos.length; i++) {
        $scope.proyecto.topicos[i].numero = i + 1;
      }
      $scope.persistirTopico();
    };

    $scope.base = {
      total: {
        avance: 0,
        devAnticipo: 0,
        saldoAnticipo: 0,
        subtotal: 0,
        retencionInterna: 0,
        multa: 0,
        pago: 0,
        porcentaje: 0
      }
    };
    $scope.modalConfigurarTopico = function(topico) {
      $scope.persistirTopico();
      $scope.modales.portafolioModalesProyectosCrearConfigurarTopico.show();
      $scope.topico = topico;
      /*if ($scope.topico.anticipos == undefined || $scope.topico.anticipos.length == 0) {
                            $scope.agregarAnticipo();
                        }
                        ;*/
      $scope.topico.total = $scope.topico.total
        ? Object.assign(JSON.parse(JSON.stringify($scope.base.total)), $scope.topico.total)
        : Object.assign(JSON.parse(JSON.stringify($scope.base.total)), {
            topicoTotalId: $scope.generarId()
          });
      ////console.log("topico actual", topico);
    };

    $scope.subir = function(input) {
      //console.log(input);
      //console.log($(input).parent());
    };

    $scope.guardar = function() {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Guardando proyecto...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      try {
        $scope.persistirTopico();
        var proyecto = JSON.parse(JSON.stringify($scope.proyecto));
        proyecto.fechaInicio = $scope.transformarFecha($scope.proyecto.fechaInicio);
        proyecto.fechaTermino = $scope.transformarFecha($scope.proyecto.fechaTermino);
        proyecto = $scope.normalizarProyecto(proyecto);
        var subtitulo = '00';
        var subtituloNombre = '';
        var item = '00';
        var itemNombre = '';
        try {
          subtitulo = JSON.parse($scope.subtitulos.subtituloActual).numero;
          subtituloNombre = JSON.parse($scope.subtitulos.subtituloActual).nombre;
          item = JSON.parse($scope.subtitulos.itemActual).item;
          itemNombre = JSON.parse($scope.subtitulos.itemActual).nombre;
        } catch (e) {
          ////console.log("ERROR al obtener ITEMS");
        }
        ////console.log("Concepto: "+$scope.conceptos[i].arreglo);
        for (var z = 0; z < proyecto.topicos.length; z++) {
          for (var i = 0; i < $scope.conceptos.length; i++) {
            if (typeof proyecto.topicos[z][$scope.conceptos[i].arreglo] != 'undefined') {
              ////console.log("propiedad "+$scope.conceptos[i].arreglo+ " encontrada");
              for (var j = 0; j < proyecto.topicos[z][$scope.conceptos[i].arreglo].length; j++) {
                for (var property in proyecto.topicos[z][$scope.conceptos[i].arreglo][j]) {
                  if (
                    (property.includes('fecha') || property.includes('vigencia')) &&
                    proyecto.topicos[z][$scope.conceptos[i].arreglo][j].hasOwnProperty(property)
                  ) {
                    var valor = proyecto.topicos[z][$scope.conceptos[i].arreglo][j][property];
                    //console.log(property+ " = "+valor);
                    proyecto.topicos[z][$scope.conceptos[i].arreglo][j][property] = new Date(
                      valor
                    ).getTime();
                    // //console.log(property+ " = "+valor);
                    // ////console.log("========");
                  }
                  var val = proyecto.topicos[z][$scope.conceptos[i].arreglo][j][property];
                  if (typeof val != 'string' && isNaN(val) && !Array.isArray(val)) {
                    proyecto.topicos[z][$scope.conceptos[i].arreglo][j][property] = 0;
                  }
                }
              }
            }
          }
          try {
            for (var i = 0; i < proyecto.fiscalizaciones.length; i++) {
              proyecto.fiscalizaciones[i].fecha = $scope.transformarFecha(
                proyecto.fiscalizaciones[i].fechaS
              );
            }
          } catch (e) {}
          if (typeof proyecto.topicos[z]['fechaInicio'] != 'undefined') {
            proyecto.topicos[z]['fechaInicio'] = new Date(
              proyecto.topicos[z]['fechaInicio']
            ).getTime();
          }
          if (typeof proyecto.topicos[z]['fechaTermino'] != 'undefined') {
            proyecto.topicos[z]['fechaTermino'] = new Date(
              proyecto.topicos[z]['fechaTermino']
            ).getTime();
          }
        }
        //.replace(/\D+/g, '');
        proyecto.subtitulo = subtitulo;
        proyecto.subtituloNombre = subtituloNombre;
        proyecto.item = item;
        proyecto.itemNombre = itemNombre;
        $scope.normalizarMontos(proyecto);
        var proyectoEnviar = { proyecto: proyecto };
        console.log('proyecto', JSON.stringify(proyectoEnviar));
        $scope.validarGuardarProyecto(proyecto);
        FebosAPI.cl_crear_proyecto({}, proyectoEnviar).then(function(response) {
          //console.log(response);
          UIkit.modal.alert('El proyecto fue creado exitosamente!', {
            labels: { Ok: 'Entendido' }
          });
        });
      } catch (e) {
        console.error(e);
        if (typeof $rootScope.blockModal != 'undefined') $rootScope.blockModal.hide();
      }
    };
    $scope.validarGuardarProyecto = function(proyecto) {
      function valorNuloOindefinidoOEmpy(val) {
        if (val == undefined || val == null || val.toString().trim().length == 0) {
          return true;
        }
        return false;
      }

      function valorNuloOindefinidoOEmpyOCERO(val) {
        if (val == undefined || val == null || val.toString().trim().length == 0 || val * 1 == 0) {
          return true;
        }
        return false;
      }

      if (true) {
        return;
      }

      var invalidos = false;
      var mensajeDeAlerta = 'Uno o mas campos no son  valido <br/><ul>';

      if (valorNuloOindefinidoOEmpy(proyecto.codigo)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>CODIGO</b> es requerido</li>';
      }
      if (valorNuloOindefinidoOEmpy(proyecto.nombre)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>NOMBRE</b> es requerido</li>';
      }
      if (valorNuloOindefinidoOEmpyOCERO(proyecto.montoOriginal)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>MONTO INICIAL</b> es requerido</li>';
      }

      if (valorNuloOindefinidoOEmpyOCERO(proyecto.subtitulo)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>SUBTITULO</b> es requerido</li>';
      }
      if (valorNuloOindefinidoOEmpyOCERO(proyecto.item)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>ITEM</b> es requerido</li>';
      }
      if (valorNuloOindefinidoOEmpyOCERO(proyecto.montoTotal)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>MONTO FINAL</b> es requerido</li>';
      }
      if (valorNuloOindefinidoOEmpy(proyecto.fechaInicio)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>FECHA INICIO</b> es requerido</li>';
      }
      if (valorNuloOindefinidoOEmpy(proyecto.fechaTermino)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>FECHA TERMINO</b> es requerido</li>';
      }
      if (
        proyecto.responsable == undefined ||
        valorNuloOindefinidoOEmpy(proyecto.responsable.nombre)
      ) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>RESPONSABLE NOMBRE</b> es requerido</li>';
      }
      if (
        proyecto.responsable == undefined ||
        valorNuloOindefinidoOEmpy(proyecto.responsable.correo)
      ) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>RESPONSABLE CORREO</b> es requerido</li>';
      }

      if (invalidos) {
        mensajeDeAlerta += '</ul>';
        UIkit.modal.alert(mensajeDeAlerta, { labels: { Ok: 'Ok' } });
        throw 'PROYECTO_NO_VALIDO';
      }
      mensajeDeAlerta = '';
      //Fin validaciones campos de proyecto
      for (var z = 0; z < proyecto.topicos.length; z++) {
        var topico = proyecto.topicos[z];
        /*
                                                if (valorNuloOindefinidoOEmpyOCERO(topico.montoMandato)) {
                                                    invalidos = true;
                                                    mensajeDeAlerta += "<li>Campo <b>MONTO MANDATO</b> es requerido</li>";
                                                }
                                                if (valorNuloOindefinidoOEmpyOCERO(topico.montoContratado)) {
                                                    invalidos = true;
                                                    mensajeDeAlerta += "<li>Campo <b>MONTO CONTRATO</b> es requerido</li>";
                                                }*/
        if (invalidos) {
          mensajeDeAlerta =
            'Uno o mas campos no son  valido<br/>   #' + topico.numero + '<ul>' + mensajeDeAlerta;
          mensajeDeAlerta += '</ul>';
          UIkit.modal.alert(mensajeDeAlerta, { labels: { Ok: 'Ok' } });
          throw 'PROYECTO_NO_VALIDO';
        }
      }
    };

    $scope.normalizarMontos = function(proyecto) {
      for (var property in proyecto) {
        if (
          (property.includes('monto') || property.includes('saldo')) &&
          proyecto.hasOwnProperty(property)
        ) {
          var valor = proyecto[property];
          try {
            //console.log(property+ " = "+valor);
            proyecto[property] = valor.replace(/\D+/g, '');
          } catch (e) {
            ////console.log("No se puede normalizar " + property + " = " + valor);
          }
          // //console.log(property+ " = "+valor);
          // ////console.log("========");
        }
      }
    };
    $scope.normalizarProyecto = function(proyecto) {
      proyecto.topicos = proyecto.topicos || [];
      for (var _topico = 0; _topico < proyecto.topicos.length; _topico++) {
        proyecto.topicos[_topico].total =
          proyecto.topicos[_topico].total ||
          Object.assign(JSON.parse(JSON.stringify($scope.base.total)), {
            topicoTotalId: $scope.generarId()
          }); //;
        proyecto.topicos[_topico].estados = proyecto.topicos[_topico].estados || [];
        for (var _estado = 0; _estado < proyecto.topicos[_topico].estados.length; _estado++) {
          proyecto.topicos[_topico].estados[_estado].documentos =
            proyecto.topicos[_topico].estados[_estado].documentos || [];
          for (
            var _documento = 0;
            _documento < proyecto.topicos[_topico].estados[_estado].documentos.length.length;
            _documento++
          ) {
            proyecto.topicos[_topico].estados[_topico].documentos[_documento].adjunto =
              proyecto.topicos[_topico].estados[_estado].documentos[_documento].adjunto || [];
          }
        }
      }
      return proyecto;
    };
    $scope.replicarMontos = function(proyecto) {
      proyecto.montoTotal = proyecto.montoOriginal;
      proyecto.montoDisponible = proyecto.montoOriginal;
    };
    $scope.transformarFecha = function(fecha) {
      try {
        return new Date(fecha).getTime();
      } catch (e) {
        return '';
      }
    };

    $scope.estado = {};
    $scope.configurarEstado = function(indice) {
      $scope.estado.indice = indice;
      //console.log(indice);
      $scope.estado.estado = $scope.topico['estados'][indice];
      //console.log(JSON.stringify($scope.estado.estado));
      // $scope.modales.portafolioModalesProyectosCrearConfigurarTopicoEstado.show();
    };

    $scope.agregarDocumento = function(indiceEstado) {
      var nuevoDoc = { documentoId: $scope.generarId(), nombreDocumento: 'Título' };
      $scope.topico['estados'][indiceEstado]['documentos'] =
        $scope.topico['estados'][indiceEstado]['documentos'] || [];
      $scope.topico['estados'][indiceEstado]['documentos'].push(nuevoDoc);
    };
    $scope.habilitarEdicion = function(edit) {
      //console.log(edit);
      edit = !edit;
    };
    $scope.descargarAdjunto = function(adjunto) {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Descargando archivo adjunto..." +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_obtener_archivo_privado(
        {
          path: adjunto.adjuntoUrl,
          nombre: adjunto.adjuntoNombre
        },
        undefined,
        false,
        true
      )
        .then(function(response) {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
          if (response['data']['codigo'] != 10) {
          } else {
            window.open(response['data']['url']);
          }
        })
        .finally(function() {
          $rootScope.blockModal.hide();
        });
    };
    $scope.agregarAdjunto = function(documento) {
      //console.log(JSON.stringify(documento));
      var nuevoAdjunto = { adjuntoId: $scope.generarId(), adjuntoNombre: '' };
      documento.adjunto = documento.adjunto || [];
      documento.adjunto.push(nuevoAdjunto);
    };
    $scope.estados = [
      { value: 0, descripcion: 'Sin gestión' },
      { value: 1, descripcion: 'Estado de Pago Generado' },
      { value: 2, descripcion: 'Estado de Pago Completo' }
    ];

    // ////console.log("SUBTITULOS CONFIGURADOS "+JSON.stringify($scope.subtitulos));
    $scope.subtitulos = {};
    $scope.subtitulos.subtituloActual = null;
    $scope.subtitulos.itemActual = null;
    $scope.subtitulos.subtitulosOpciones = SubTitulos;
    $scope.subtitulos.items = [];
    $scope.subtituloChange = function() {
      try {
        $scope.subtitulos.cambiando = true;
        var actual = $scope.subtitulos.subtituloActual;
        try {
          actual = JSON.parse(actual);
        } catch (e) {
          console.err('Error al pasar actual');
        }
        ////console.log("SUB -- ", actual);
        ////console.log("NOM -- ", actual.nombre);
        $scope.subtitulos.items = actual['item'];
        ////console.log("items  1", actual ["item"]);
        ////console.log("items  2", $scope.subtitulos.items);
        // $scope.generarTopicos();
      } finally {
        $scope.subtitulos.cambiando = false;
      }
    };
    $scope.subtituloItemChange = function() {
      var asignaciones = [];
      var actual = $scope.subtitulos.itemActual;
      if ($scope.subtitulos.cambiando) {
      } else {
        try {
          asignaciones = JSON.parse(actual).asignacion;
        } catch (e) {
          console.error('Error al pasar actual', e);
        }
        try {
          asignaciones = JSON.parse(asignaciones);
        } catch (e) {}

        $scope.generarTopicos(asignaciones);
      }
      ////console.log("Modificando $scope.subtitulos.cambiando", $scope.subtitulos.cambiando, asignaciones);
    };
    $scope.generarTopicos = function(asignaciones) {
      $scope.proyecto.topicos = [];
      ////console.log("GENERAR TOPICO", $scope.proyecto.topicos, $scope.subtitulos.items);
      asignaciones.forEach(function(item, index) {
        var indice = $scope.nuevoTopico();
        //console.log(indice);
        $scope.proyecto.topicos[indice - 1].numero = item.numero;
        $scope.proyecto.topicos[indice - 1].asunto = item.nombre;
        $scope.proyecto.topicos[indice - 1].monto = 0;
      });
      ////console.log("TOPICO GENERADO", $scope.proyecto.topicos);
    };

    // masked inputs
    var $maskedInput = $('input[data-inputmask]');
    if ($maskedInput.length) {
      ////console.log("INIT MASK", $maskedInput)
      $maskedInput.inputmask();
    }

    $scope.obtenerMontoAnticipo = function(topicosEnviado) {
      var topico = topicosEnviado || $scope.topico;
      if ((topico.anticipos || []).length > 0) {
        return num(topico.anticipos[0].monto);
      }
      return 0;
    };

    $scope.getMontoTotalEstadoPago = function(topico) {
      var totales = JSON.parse(JSON.stringify($scope.base.total));
      for (var i = 0; i < topico.estados.length; i++) {
        totales['avance'] += num(topico.estados[i]['avance'], 'avance');
        totales['devAnticipo'] += num(topico.estados[i]['devAnticipo'], 'devAnticipo');
        totales['subtotal'] += num(topico.estados[i]['subtotal']);
        totales['retencionInterna'] += num(
          topico.estados[i]['retencionInterna'],
          'retencionInterna'
        );
        totales['multa'] += num(topico.estados[i]['multa'], 'multa');
        totales['pago'] += num(topico.estados[i]['pago'], 'pago');
        try {
          topico.estados[i]['avancePorcentual'] =
            Math.round((num(topico.estados[i]['pago']) / num(topico.montoContratado)) * 100) +
            ($scope.topico.estados[i - 1] != undefined
              ? num($scope.topico.estados[i - 1]['avancePorcentual'])
              : 0);
        } catch (e) {
          console.error(e, topico);
        }
        totales['porcentaje'] = topico.estados[i]['avancePorcentual'] || 0;
      }
      totales['saldoAnticipo'] = num(
        $scope.obtenerMontoAnticipo() - totales['devAnticipo'],
        'saldoAnticipo'
      );
      topico.total = Object.assign({ topicoTotalId: $scope.generarId() }, topico.total, totales);
      topico.total.restante =
        num(topico.montoContratado, 'montoCntratado') -
        num(totales['avance'], 'avance') -
        num(totales['saldoAnticipo'], 'saldo'); //monto contrato, menos avance total, menos anticopo saldo;
    };

    $scope.getTotalesEstadoPago = function(estado) {
      estado['subtotal'] = num(estado['avance']) - num(estado['devAnticipo']);
      estado['pago'] =
        num(estado['subtotal']) - num(estado['retencionInterna']) - num(estado['multa']);

      $scope.getMontoTotalEstadoPago();
    };
    $scope.calcularTotalesTopico2 = function() {
      calcularTotalesTopico().then();
    };

    function calcularTotalesTopico(topicosEnviado) {
      return new Promise(function(resolve, reject) {
        var topico = topicosEnviado || $scope.topico;
        ////console.log("calculando topico", topico);
        $scope.getMontoTotalEstadoPago(topico);
        resolve(topico);
      });
    }

    function calcularTotalEstadoPago(estado) {
      return new Promise(function(resolve, reject) {
        estado['subtotal'] = num(estado['avance']) - num(estado['devAnticipo']);
        estado['pago'] =
          num(estado['subtotal']) - num(estado['retencionInterna']) /* - num(estado['multa'])*/;
        estado['pagoCMulta'] =
          num(estado['subtotal']) - num(estado['retencionInterna']) - num(estado['multa']);
        resolve(estado);
      });
    }

    $scope.calcularTotalYSaldoMandato = function(topico) {
      calcularTotalYSaldo(topico).then(function() {
        calcularTotalesTopico(topico).then(function() {
          carcularSaldoTotal($scope.proyecto.topicos).then(function(saldoTotal) {
            $scope.proyecto.montoDisponible = saldoTotal.saldoTotal;
            $scope.proyecto.montoOriginal = saldoTotal.montoTotalMandato;
            $scope.proyecto.montoTotal = saldoTotal.montoContratadoTotalMandato;
          });
        });
      });
    };

    function calcularTotalYSaldo(topico) {
      return new Promise(function(resolve, reject) {
        topico.montoTotalMandato = num(topico.montoMandato) - num(topico.modificacionMontoMandato);
        topico.saldoMandato = num(topico.montoTotalMandato) - num(topico.montoContratado);
        resolve(topico);
      });
    }

    function carcularSaldoTotal(topicos) {
      return new Promise(function(resolve, reject) {
        var saldoTotal = 0;
        var montoTotalMandato = 0,
          montoContratadoTotalMandato = 0;
        for (var i = 0; i < topicos.length; i++) {
          saldoTotal += num(topicos[i]['saldoMandato']);
          montoTotalMandato += num(topicos[i]['montoTotalMandato']);
          montoContratadoTotalMandato += num(topicos[i]['montoContratado']);
        }
        resolve({
          saldoTotal: saldoTotal,
          montoTotalMandato: montoTotalMandato,
          montoContratadoTotalMandato: montoContratadoTotalMandato
        });
      });
    }

    $scope.estadoPagoInterceptor = function(obj) {
      ////console.log("estado agregado");
      $scope.getMontoTotalEstadoPago();
    };

    $scope.anticipoValidador = function(obj) {
      if ($scope.topico.anticipos.length > 0) {
        UIkit.modal.alert('Solo se permite 1 anticipo por proyecto', {
          labels: { Ok: 'Entendido' }
        });
        throw 'PROYECTO_NO_VALIDO_MAS_UNO_TOPICO';
      }
    };

    $scope.devolucionInterceptor = function(devolucion) {
      $scope.topico['devolucion'] = $scope.topico['devolucion'] || [];
      var valorRetencionTotal = num($scope.topico.total['retencionInterna']);
      var retencionDevuelto = 0;
      for (var i = 0; i < $scope.topico['devolucion'].length; i++) {
        retencionDevuelto += num($scope.topico['devolucion']['monto']);
      }
      devolucion['monto'] = valorRetencionTotal - retencionDevuelto;
      devolucion['asunto'] = 'Devolución de retención';
      devolucion['fecha'] = $scope.transformarFechaReverso(new Date().getTime());
      console.log(valorRetencionTotal, retencionDevuelto, devolucion);
    };
    $scope.asignarBoleta = function(devolucion, boleta) {
      devolucion.editando = false;
      console.log('dev', devolucion, boleta);
      devolucion.boletaReferenciaId = boleta.boletaId;
    };

    $scope.obtenerDescripcionBoleta = function(boletaReferenciaId, boletas) {
      var desc = '';
      for (var i = 0; i < boletas.length; i++) {
        if (boletas[i].boletaId == boletaReferenciaId) {
          desc = boletas[i].folio;
          return desc;
        }
      }
      return desc;
    };
    $scope.scrollTo = function(hash) {
      console.log('to ' + hash);
      $location.hash(hash);
    };

    $scope.agregarPlazo = function(topico, plazo) {
      if (plazo == 0) {
        return;
      }
      topico.plazoModificacion = topico.plazoModificacion || [];
      topico.plazoModificacion.push({ plazo: plazo, fecha: new Date().getTime() });
      ////console.log("plazo agregado", topico)
    };
    $scope.calcularPlazoTopico = function(topico) {
      var fechaInicio =
        topico.fechaInicio == undefined || new String(topico.fechaInicio).trim().length == 0
          ? moment().format('YYYY-MM-DD')
          : topico.fechaInicio;
      topico.plazoOriginal = topico.plazoOriginal || 0;
      var plazo = topico.plazoOriginal || 0;
      var modificaciones = topico.plazoModificacion || [];
      var fechaTermino = moment(fechaInicio).add(plazo, 'day');
      ////console.log("modificaciones plazo", fechaTermino, modificaciones);
      for (var x = 0; x < modificaciones.length; x++) {
        fechaTermino.add(modificaciones[x].plazo, 'day');
        plazo += modificaciones[x].plazo;
        ////console.log("modificaciones plazo", fechaTermino, modificaciones[x]);
      }
      topico.plazo = plazo;
      topico.fechaTermino = fechaTermino.format('YYYY-MM-DD');
      topico.fechaInicio = fechaInicio;
      ////console.log("plazo agregado 2", topico)
    };

    $scope.provinciaChange = function(provincia) {
      $scope.provinciaChangePromise(provincia).then(function(d) {
        console.log('comunas cambiadas ', d);
      });
    };
    $scope.provinciaChangePromise = function(provincia) {
      return new Promise(function(resolve, reject) {
        if (provincia != undefined && provincia != null) {
          $scope.comunasCargando = true;
          $scope.proyecto.comuna = 0;
          resolve(
            FebosAPI.cl_listar_comunas(
              {
                regionid: $stateParams.region,
                provinciaid: provincia
              },
              {},
              true,
              true
            ).then(function(response) {
              $scope.comunasCargando = false;

              $scope.comunas = [];
              $scope.comunas = response.data.lugares;
              $scope.proyecto.comuna = 0;
              return $scope.comunas;
            })
          );
        } else {
          resolve([]);
        }
      });
    };
    $scope.selectize_config_unidades = {
      maxItems: 1,
      valueField: 'iut',
      labelField: 'razonSocial',
      searchField: 'razonSocial',
      create: false,
      dropdownParent: 'body',
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.iut) +
            '  -  ' +
            escape(data.razonSocial) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.razonSocial) + '</div>';
        }
      }
    };
    $scope.selectize_config_provincia = {
      maxItems: 1,
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      create: false,
      dropdownParent: 'body',
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.id) +
            '  -  ' +
            escape(data.title) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.title) + '</div>';
        }
      }
    };
    $scope.selectize_config_comuna = {
      maxItems: 1,
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      create: false,
      dropdownParent: 'body',
      render: {
        option: function(data, escape) {
          if (!data.disable) {
            return (
              '<div class="option">' +
              '<span class="title">' +
              escape(data.id) +
              '  -  ' +
              escape(data.title) +
              '</span>' +
              '</div>'
            );
          } else {
            return (
              '<div class="option option-disabled" style="pointer-events: none; color: #aaa;">' +
              escape(data.title) +
              '</div>'
            );
          }
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.title) + '</div>';
        }
      }
    };

    $scope.obtenerFechaAdjunto = function(adjunto) {
      if (adjunto.fecha == undefined || adjunto.fecha.toString().trim().length == 0) {
        adjunto.fecha = moment().format('YYYY-MM-DD HH:MM:SS');
      }
      return adjunto.fecha;
    };
    $scope.adjuntoContratoFiltro = function(visibleOcultos) {
      return function(adjunto) {
        var visible =
          adjunto.estado == undefined ||
          adjunto.estado == 0 ||
          adjunto.estado == 1 ||
          visibleOcultos == 1;
        return visible;
      };
    };
    $scope.modalModificacionesTopico = function(topico) {
      $scope.topico = topico;
      $scope.modales.portafolioModalesProyectosModificarTopico.show();
    };

    $scope.cancelarModificacion = function() {
      $scope.modificacionNuevaEditando = false;
    };
    $scope.guardarModificacion = function() {
      console.log('guardando', $scope.topico.modificacionNueva);
      $scope.modificacionNuevaEditando = false;
      $scope.topico.modificacionesMandato = $scope.topico.modificacionesMandato || [];
      $scope.topico.modificacionesMandato.push(
        JSON.parse(JSON.stringify($scope.topico.modificacionNueva))
      );
      var montoModificaciones = 0;
      for (var i = 0; i < $scope.topico.modificacionesMandato.length; i++) {
        montoModificaciones += num($scope.topico.modificacionesMandato[i].monto);
      }
      $scope.topico.modificacionMontoMandato = montoModificaciones;
      $scope.calcularTotalYSaldoMandato($scope.topico);
    };
    $scope.agregarModificacion = function() {
      $scope.topico.modificacionNueva = {
        modificacionId: $scope.generarId(),
        fecha: $scope.transformarFechaReverso(new Date().getTime()),
        monto: 0,
        asunto: '--'
      };
      console.log('agregando modificacion', $scope.topico.modificacionNueva);
      $scope.modificacionNuevaEditando = true;
    };

    $scope.agregarComentarioEstadoPago = function(estado) {
      estado.comentarios = estado.comentarios || [];
      console.log('USUARIO ');
      var comentario = JSON.parse(JSON.stringify(SesionFebos().usuario));
      comentario.fecha = new Date().getTime();
      estado.comentarios.push(comentario);
    };

    function num(val) {
      return (val || 0) * 1;
    }

    $scope.calcularPlazo = function(fechaInicio, fechaFin) {
      var s = 0;
      try {
        var inicio = moment(fechaInicio, 'YYYY-MM-DD');
        var fin = moment(fechaFin, 'YYYY-MM-DD');
        var ms = fin.diff(inicio, 'days');
        s = ms;
      } catch (e) {
        console.error(e);
      }
      return isNaN(s) ? 0 : s;
    };

    $scope.monedasOptions = JSON.parse(JSON.stringify(_MONEDAS));
    $scope.tiposGarantiaOptions = JSON.parse(JSON.stringify(_TIPOSGARANTIA));
    console.log('tiposGarantiaOptions', $scope.tiposGarantiaOptions);

    $scope.conceptosOptions = [
      //Seriedad de la Oferta, Fiel cumplimiento y anticipo de Fondos
      { value: 'Seriedad de la Oferta' },
      { value: 'Fiel cumplimiento' },
      { value: 'Anticipo de Fondos' }
    ];
    $scope.selectize_config_concepto = {
      maxItems: 1,
      valueField: 'value',
      labelField: 'value',
      searchField: 'value',
      create: true,
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.value) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.value) + '</div>';
        }
        //,
        //option_create: function (data, escape) {
        //    return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
        // }
      }
    };

    $scope.selectize_config_moneda = {
      maxItems: 1,
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      create: true,
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.title) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.title) + '</div>';
        }
        //,
        //option_create: function (data, escape) {
        //    return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
        // }
      }
    };

    $scope.selectize_config_tipo_garantia = {
      maxItems: 1,
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      create: true,
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.title) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.title) + '</div>';
        }
        //,
        //option_create: function (data, escape) {
        //    return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
        // }
      }
    };

    $scope.selectize_config_id_title = {
      maxItems: 1,
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      create: false,
      dropdownParent: 'body',
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.title) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.title) + '</div>';
        }
        //,
        //option_create: function (data, escape) {
        //    return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
        // }
      }
    };
  }
]);
