febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.portafolio-crear-proyecto', {
      url: '/:app/portafolio/proyectos/nuevo',
      templateUrl:
        'app/febos/documentos/portafolio/proyectos/acciones/crear/crearProyectoView.html',
      controller: 'crearProyectoCtrl',
      data: {
        pageTitle: 'Nuevo Proyecto'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            console.log('AAA ==>', currentStateData);
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            console.log('B');
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            return {};
          }
        ],
        Provincias: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            try {
              console.log('CONSULTANDO CONFIGURACION');
              $stateParams.region = 0;
              return FebosAPI.cl_adm_configuracion_listar({
                parametros: 'proyecto.provincia.listar'
              }).then(function(response) {
                try {
                  $stateParams.region = response.data.configuraciones[0].valor;
                } catch (e) {
                  console.log('ERROR');
                }
                console.log('RESPUESTA API CONFIGURACION', response);
                return FebosAPI.cl_listar_provincias(
                  { regionid: $stateParams.region },
                  {},
                  true,
                  true
                ).then(function(response) {
                  console.log('respuesta api provincias', response);
                  return response.data.lugares;
                });
              });
            } catch (e) {
              console.log(e);
            }
          }
        ],
        UnidadesOrganizativas: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            try {
              console.log('CONSULTANDO CONFIGURACION');
              $stateParams.region = 0;
              return FebosAPI.cl_listar_unidades_tecnicas({}).then(function(response) {
                return response.data.listado;
              });
            } catch (e) {
              console.log(e);
            }
          }
        ],
        SubTitulos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            try {
              return [
                {
                  numero: '22',
                  nombre: 'SALDO INICIAL DE CAJA',
                  item: [
                    {
                      item: '11',
                      nombre: 'Estudios Propios del giro',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Compensaciones por Daños a Terceros y/o a la Propiedad'
                        }
                      ]
                    }
                  ]
                },
                {
                  numero: '24',
                  nombre: 'TRANSFERENCIAS CORRIENTES',
                  item: [
                    {
                      item: '01',
                      nombre: 'Al Sector Privado',
                      asignacion: [
                        {
                          numero: '100',
                          nombre: 'Aplicación Numeral 2.1 Glosa 02 Común para Gobiernos Regionales'
                        },
                        {
                          numero: '101',
                          nombre: 'Cultura'
                        },
                        {
                          numero: '102',
                          nombre: 'Deporte'
                        },
                        {
                          numero: '103',
                          nombre: 'Seguridad Ciudadna'
                        },
                        {
                          numero: '999',
                          nombre: 'Otros'
                        }
                      ]
                    },
                    {
                      item: '03',
                      nombre: 'A Otras Entidades Públicas',
                      asignacion: [
                        {
                          numero: '100',
                          nombre: 'Aplicación Numeral 2.1 Glosa 02 Común para Gobiernos Regionales'
                        },
                        {
                          numero: '101',
                          nombre: 'Cultura'
                        },
                        {
                          numero: '102',
                          nombre: 'Deporte'
                        },
                        {
                          numero: '103',
                          nombre: 'Seguridad Ciudadna'
                        },
                        {
                          numero: '999',
                          nombre: 'Otros'
                        }
                      ]
                    }
                  ]
                },
                {
                  numero: '26',
                  nombre: 'INICIATIVAS DE INVERSIÓN',
                  item: [
                    {
                      item: '02',
                      nombre: 'Compensaciones por Daños a Terceros y/o a la Propiedad',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Compensaciones por Daños a Terceros y/o a la Propiedad'
                        }
                      ]
                    }
                  ]
                },
                {
                  numero: '29',
                  nombre: 'ADQUISICIÓN DE ACTIVOS NO FINANCIEROS',
                  item: [
                    {
                      item: '01',
                      nombre: 'Terreno',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Terreno'
                        }
                      ]
                    },
                    {
                      item: '02',
                      nombre: 'Edificios',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Edificios'
                        }
                      ]
                    },
                    {
                      item: '03',
                      nombre: 'Vehículos',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Vehículos'
                        }
                      ]
                    },
                    {
                      item: '04',
                      nombre: 'Mobiliario y Otros',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Mobiliario y Otros'
                        }
                      ]
                    },
                    {
                      item: '05',
                      nombre: 'Máquinas y Equipos',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Máquinas y Equipos'
                        }
                      ]
                    },
                    {
                      item: '06',
                      nombre: 'Equipos Informáticos',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Equipos Informáticos'
                        }
                      ]
                    },
                    {
                      item: '07',
                      nombre: 'Programas Informáticos',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Programas Informáticos'
                        },
                        {
                          numero: '002',
                          nombre: 'Sistema de Información '
                        }
                      ]
                    },
                    {
                      item: '99',
                      nombre: 'Otros Activos no Financieros',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Otros Activos no Financieros'
                        }
                      ]
                    }
                  ]
                },
                {
                  numero: '31',
                  nombre: 'INICIATIVAS DE INVERSIÓN',
                  item: [
                    {
                      item: '01',
                      nombre: 'Estudios Básicos',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Gasto administrativo'
                        },
                        {
                          numero: '002',
                          nombre: 'Consultoria'
                        }
                      ]
                    },
                    {
                      item: '02',
                      nombre: 'Proyectos',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Gasto administrativo'
                        },
                        {
                          numero: '002',
                          nombre: 'Consultoria'
                        },
                        {
                          numero: '003',
                          nombre: 'Terreno'
                        },
                        {
                          numero: '004',
                          nombre: 'Obras Civiles'
                        },
                        {
                          numero: '005',
                          nombre: 'Equipamiento'
                        },
                        {
                          numero: '006',
                          nombre: 'Equipos'
                        },
                        {
                          numero: '007',
                          nombre: 'Vehiculos'
                        },
                        {
                          numero: '008',
                          nombre: 'otros'
                        }
                      ]
                    },
                    {
                      item: '03',
                      nombre: 'Programas de Inversión',
                      asignacion: [
                        {
                          numero: '001',
                          nombre: 'Gasto administrativo'
                        },
                        {
                          numero: '002',
                          nombre: 'Consultoria'
                        },
                        {
                          numero: '003',
                          nombre: 'Programa'
                        }
                      ]
                    }
                  ]
                },
                {
                  numero: '33',
                  nombre: 'TRANSFERENCIAS DE CAPITAL',
                  item: [
                    {
                      item: '01',
                      nombre: 'Al Sector Privado',
                      asignacion: [
                        {
                          numero: '004',
                          nombre: 'Junta Nacional del Cuerpo de Bomberos de Chile'
                        },
                        {
                          numero: '010',
                          nombre: 'Aplicación Letra a) Artículo Cuarto Transitorio Ley N°20.378'
                        }
                      ]
                    },
                    {
                      item: '02',
                      nombre: 'Al Gobierno  Central',
                      asignacion: [
                        {
                          numero: '039',
                          nombre: 'Servicio de Salud Valparaíso-San Antonio - FAR'
                        },
                        {
                          numero: '040',
                          nombre: 'SERVIU V REGIÓN'
                        }
                      ]
                    },
                    {
                      item: '03',
                      nombre: 'A Otras Entidades Públicas',
                      asignacion: [
                        {
                          numero: '125',
                          nombre: 'Municipalidades (Fondo Regional de Iniciativa Local)'
                        },
                        {
                          numero: '206',
                          nombre:
                            'SEREMI de Minería - Programa Regularización y Fomento Pequeña Minería Región de Valparaíso (30396032-0)'
                        },
                        {
                          numero: '207',
                          nombre:
                            'INDAP-Transferencia tecnológica para obras civiles de riego IV (30470441-0)'
                        },
                        {
                          numero: '208',
                          nombre:
                            'SEREMI de Minería - Regularización y fomento pequeña minería II Etapa (30458145-0)'
                        },
                        {
                          numero: '209',
                          nombre:
                            'Subsecretaría de Pesca (FAP) - Fomento y desarrollo productivo para el sector pesquero artesanal (30483822-0)'
                        }
                      ]
                    }
                  ]
                }
              ];
            } catch (e) {
              console.log(e);
            }
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'lazy_masked_inputs',
              'app/febos/documentos/portafolio/proyectos/acciones/crear/crearProyectoController.js'
            ]);
          }
        ]
      }
    });
  }
]);
