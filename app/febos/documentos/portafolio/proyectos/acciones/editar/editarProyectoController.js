angular.module('febosApp').controller('editarProyectoCtrl', [
  '$rootScope',
  '$scope',
  'variables',
  '$location',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  'MONEDAS',
  'TIPOSGARANTIA',
  'Datos',
  'Provincias',
  'UnidadesOrganizativas',
  '$state',
  '$stateParams',
  '$http',
  '$timeout',
  '$interval',
  'SubTitulos',
  function(
    $rootScope,
    $scope,
    variables,
    $location,
    SesionFebos,
    FebosAPI,
    FebosUtil,
    _MONEDAS,
    _TIPOSGARANTIA,
    Datos,
    Provincias,
    UnidadesOrganizativas,
    $state,
    $stateParams,
    $http,
    $timeout,
    $interval,
    SubTitulos
  ) {
    $scope.app = $stateParams.app;
    $scope.region = $stateParams.region;
    $scope.ambiente = FebosUtil.obtenerAmbiente();
    $scope.provincias = Provincias;
    $scope.comunas = [];
    $scope.unidades = UnidadesOrganizativas;
    ////console.log("DENTRO DEL CONTROLLER " + $scope.ambiente);
    $scope.proyecto = Datos;
    $scope.proyecto.codigoVerificador = $scope.proyecto.codigoVerificador || 0;
    $scope.proyecto.topicos = sortObjectByKey($scope.proyecto.topicos, 'numero', true);
    // for (var i = 0; i < $scope.proyecto.topicos.length; i++) {
    //   calcularTotalYSaldo($scope.proyecto.topicos[i]);
    // }
    carcularSaldoTotal($scope.proyecto.topicos).then(function(saldoTotal) {
      $scope.proyecto.montoDisponible = saldoTotal.saldoTotal;
      $scope.proyecto.montoTotalMandato = saldoTotal.montoTotalMandato;
    });
    //console.log('provincia', $scope.proyecto.provincia);
    //console.log('comuna', $scope.proyecto.comuna);
    $scope.generarId = function() {
      return new Date().valueOf() + '-' + FebosUtil.uuid();
    };
    $scope.topico = {};
    $scope.persistirTopico = function() {
      ////console.log("buscando topico", $scope.topico);
      for (var i = 0; i < $scope.proyecto.topicos.length; i++) {
        if ($scope.proyecto.topicos[i].topicoId == $scope.topico.topicoId) {
          ////console.log("topico encontrado!");
          $scope.proyecto.topicos[i] = JSON.parse(JSON.stringify($scope.topico));
        }
      }
    };
    $scope.nuevaFiscalizacion = function() {
      $scope.proyecto.fiscalizaciones = $scope.proyecto.fiscalizaciones || [];
      $scope.proyecto.fiscalizaciones.push({ fiscalizacionId: $scope.generarId() });
    };
    $scope.nuevoTopico = function() {
      ////console.log("creando nuevo topico");
      var indice = $scope.proyecto.topicos.push({
        proyectoId: $scope.proyecto.proyectoId,
        topicoId: $scope.generarId(),
        item: null,
        numero: $scope.proyecto.topicos.length + 1,
        anticipos: [],
        boletas: [],
        polizas: [],
        estados: [],
        contactos: []
      });
      $scope.persistirTopico();
      return indice;
    };

    $scope.modales = FebosUtil.modales([
      'portafolio_modales_proyectos_crear_configurar_topico',
      'portafolio_modales_proyectos_modificar_topico'
    ]);

    $scope.conceptos = [
      { nombre: 'Contrato', arreglo: 'contratos' },
      { nombre: 'Boleta', arreglo: 'boletas' },
      { nombre: 'Modificaciones', arreglo: 'modificacionesMandato' },
      { nombre: 'Anticipo', arreglo: 'anticipos' },
      { nombre: 'Devolucion', arreglo: 'devolucion' },
      { nombre: 'Poliza', arreglo: 'polizas' },
      { nombre: 'Estado', arreglo: 'estados' },
      { nombre: 'Contacto', arreglo: 'contactos' }
    ];
    //fabricar funciones de agregar y eliminar conceptor de los arreglos
    var buildAgregar = function(concepto) {
      return function() {
        var obj = {};
        obj['proyectoId'] = $scope.proyecto.proyectoId;
        var conceptoSingular = concepto.endsWith('s')
          ? concepto.substring(0, concepto.length - 1)
          : concepto;
        if (
          conceptoSingular == 'boleta' ||
          conceptoSingular == 'poliza' ||
          conceptoSingular == 'estado'
        ) {
          obj.estado = 1;
          obj.vigente = 1;
          obj.usuarioGestorNombre = SesionFebos().usuario.nombre;
          obj.usuarioGestorCorreo = SesionFebos().usuario.correo;
        }
        if (conceptoSingular == 'estado') {
          conceptoSingular = 'estadoPago';
        }
        obj[conceptoSingular + 'Id'] = $scope.generarId();
        obj['topicoId'] = $scope.topico.topicoId;
        if ($scope[conceptoSingular + 'Validador']) {
          $scope[conceptoSingular + 'Validador'](obj);
        }
        if ($scope.topico[concepto] == undefined) {
          $scope.topico[concepto] = [];
        }
        var indice = $scope.topico[concepto].push({});
        obj['indice'] = indice;
        $scope.topico[concepto][indice - 1] = obj;
        if ($scope[conceptoSingular + 'Interceptor']) {
          $scope[conceptoSingular + 'Interceptor'](obj);
        }
      };
    };
    var buildEliminar = function(concepto) {
      return function(indice) {
        $scope.topico[concepto].splice(indice, 1);
      };
    };

    $scope.eliminarFiscalizacion = function(indice) {
      $scope.proyecto.fiscalizaciones.splice(indice, 1);
    };
    for (var i = 0; i < $scope.conceptos.length; i++) {
      $scope['agregar' + $scope.conceptos[i].nombre] = buildAgregar($scope.conceptos[i].arreglo);
      $scope['eliminar' + $scope.conceptos[i].nombre] = buildEliminar($scope.conceptos[i].arreglo);
    }

    $scope.eliminarTopico = function(topico) {
      $scope.proyecto.topicos.splice(topico.numero - 1, 1);
      for (var i = 0; i < $scope.proyecto.topicos.length; i++) {
        $scope.proyecto.topicos[i].numero = i + 1;
      }
      $scope.persistirTopico();
    };
    $scope.base = {
      total: {
        avance: 0,
        devAnticipo: 0,
        saldoAnticipo: 0,
        subtotal: 0,
        retencionInterna: 0,
        multa: 0,
        pago: 0,
        porcentaje: 0
      }
    };
    $scope.modalConfigurarTopico = function(topico) {
      $scope.persistirTopico();
      $scope.topico = topico;
      /*if ($scope.topico.anticipos == undefined || $scope.topico.anticipos.length == 0) {
                                                    $scope.agregarAnticipo();
                                                }
                                                ;*/
      $scope.topico.total =
        $scope.topico.total ||
        Object.assign(JSON.parse(JSON.stringify($scope.base.total)), {
          topicoTotalId: $scope.generarId()
        });
      $scope.topico.total = $scope.topico.total
        ? Object.assign(JSON.parse(JSON.stringify($scope.base.total)), $scope.topico.total)
        : Object.assign(JSON.parse(JSON.stringify($scope.base.total)), {
            topicoTotalId: $scope.generarId()
          });
      $scope.topico.estados = sortObjectByKey($scope.topico.estados, 'avancePorcentual', true);
      ////console.log("topico actual", topico);
      // calcularTotalesTopico().then(function(response) {
      //   ////console.log("calculo al abrir el modal ", response);
      // });
      $scope.modales.portafolioModalesProyectosCrearConfigurarTopico.show();
    };

    $scope.guardar = function() {
      try {
        // //console.log("proyecto en memoria antes de persistir",JSON.stringify($scope.proyecto));
        $scope.persistirTopico();
        // //console.log("proyecto en memoria despues de persistir",JSON.stringify($scope.proyecto));

        var proyecto = JSON.parse(JSON.stringify($scope.proyecto));
        // //console.log("proyecto en memoria ", JSON.stringify($scope.proyecto));
        proyecto.fechaInicio = $scope.transformarFecha($scope.proyecto.fechaInicio);
        proyecto.fechaTermino = $scope.transformarFecha($scope.proyecto.fechaTermino);
        proyecto.fechaCreacion = $scope.transformarFecha($scope.proyecto.fechaCreacion);
        proyecto = $scope.normalizarProyecto(proyecto);
        ////console.log("Concepto: "+$scope.conceptos[i].arreglo);
        for (var z = 0; z < proyecto.topicos.length; z++) {
          for (var i = 0; i < $scope.conceptos.length; i++) {
            if (typeof proyecto.topicos[z][$scope.conceptos[i].arreglo] != 'undefined') {
              //console.log('propiedad ' + $scope.conceptos[i].arreglo + ' encontrada');
              for (var j = 0; j < proyecto.topicos[z][$scope.conceptos[i].arreglo].length; j++) {
                for (var property in proyecto.topicos[z][$scope.conceptos[i].arreglo][j]) {
                  if (
                    (property.includes('fecha') || property.includes('vigencia')) &&
                    proyecto.topicos[z][$scope.conceptos[i].arreglo][j].hasOwnProperty(property)
                  ) {
                    var valor = proyecto.topicos[z][$scope.conceptos[i].arreglo][j][property];
                    //console.log(property+ " = "+valor);
                    proyecto.topicos[z][$scope.conceptos[i].arreglo][j][property] = new Date(
                      valor
                    ).getTime();
                    // console.log(property+ " = "+valor);
                    // //console.log("========");
                    continue;
                  }
                  var val = proyecto.topicos[z][$scope.conceptos[i].arreglo][j][property];
                  if (new String(val) == 'true' || new String(val) == 'false') {
                    proyecto.topicos[z][$scope.conceptos[i].arreglo][j][property] =
                      new String(val) == 'true' ? 1 : 0;
                    continue;
                  }
                  if (typeof val != 'string' && isNaN(val) && !Array.isArray(val)) {
                    proyecto.topicos[z][$scope.conceptos[i].arreglo][j][property] = 0;
                  }
                }
              }
            }
          }
          try {
            for (var i = 0; i < proyecto.fiscalizaciones.length; i++) {
              proyecto.fiscalizaciones[i].fecha = $scope.transformarFecha(
                proyecto.fiscalizaciones[i].fechaS
              );
            }
          } catch (e) {}
          if (typeof proyecto.topicos[z]['fechaInicio'] != 'undefined') {
            proyecto.topicos[z]['fechaInicio'] = new Date(
              proyecto.topicos[z]['fechaInicio']
            ).getTime();
          }
          if (typeof proyecto.topicos[z]['fechaTermino'] != 'undefined') {
            proyecto.topicos[z]['fechaTermino'] = new Date(
              proyecto.topicos[z]['fechaTermino']
            ).getTime();
          }
        }
        $scope.normalizarMontos(proyecto);

        var proyectoEnviar = { proyecto: proyecto };
        //console.log('proyecto', JSON.stringify(proyectoEnviar));
        $rootScope.blockModal = UIkit.modal.blockUI(
          "<div class='uk-text-center'>Modificando proyecto...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
        );
        $scope.validarGuardarProyecto(proyecto);
        FebosAPI.cl_crear_proyecto({}, proyectoEnviar).then(function(response) {
          //console.log(response);
          UIkit.modal.alert('El proyecto fue modificado exitosamente!', {
            labels: { Ok: 'Entendido' }
          });
        });
      } catch (e) {
        console.error(e);
        if (typeof $rootScope.blockModal != 'undefined') $rootScope.blockModal.hide();
      }
    };
    $scope.validarGuardarProyecto = function(proyecto) {
      function valorNuloOindefinidoOEmpy(val) {
        if (val == undefined || val == null || val.toString().trim().length == 0) {
          return true;
        }
        return false;
      }

      function valorNuloOindefinidoOEmpyOCERO(val) {
        if (val == undefined || val == null || val.toString().trim().length == 0 || val * 1 == 0) {
          return true;
        }
        return false;
      }

      if (true) {
        return;
      }
      var invalidos = false;
      var mensajeDeAlerta = 'Uno o mas campos no son  valido <br/><ul>';

      if (valorNuloOindefinidoOEmpy(proyecto.codigo)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>CODIGO</b> es requerido</li>';
      }
      if (valorNuloOindefinidoOEmpy(proyecto.nombre)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>NOMBRE</b> es requerido</li>';
      }
      if (valorNuloOindefinidoOEmpyOCERO(proyecto.montoOriginal)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>MONTO INICIAL</b> es requerido</li>';
      }

      if (valorNuloOindefinidoOEmpyOCERO(proyecto.subtitulo)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>SUBTITULO</b> es requerido</li>';
      }
      if (valorNuloOindefinidoOEmpyOCERO(proyecto.item)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>ITEM</b> es requerido</li>';
      }
      if (valorNuloOindefinidoOEmpyOCERO(proyecto.montoTotal)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>MONTO FINAL</b> es requerido</li>';
      }
      if (valorNuloOindefinidoOEmpy(proyecto.fechaInicio)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>FECHA INICIO</b> es requerido</li>';
      }
      if (valorNuloOindefinidoOEmpy(proyecto.fechaTermino)) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>FECHA TERMINO</b> es requerido</li>';
      }
      if (
        proyecto.responsable == undefined ||
        valorNuloOindefinidoOEmpy(proyecto.responsable.nombre)
      ) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>RESPONSABLE NOMBRE</b> es requerido</li>';
      }
      if (
        proyecto.responsable == undefined ||
        valorNuloOindefinidoOEmpy(proyecto.responsable.correo)
      ) {
        invalidos = true;
        mensajeDeAlerta += '<li>Campo <b>RESPONSABLE CORREO</b> es requerido</li>';
      }

      if (invalidos) {
        mensajeDeAlerta += '</ul>';
        UIkit.modal.alert(mensajeDeAlerta, { labels: { Ok: 'Ok' } });
        throw 'PROYECTO_NO_VALIDO';
      }
      mensajeDeAlerta = '';
      //Fin validaciones campos de proyecto
      for (var z = 0; z < proyecto.topicos.length; z++) {
        var topico = proyecto.topicos[z];
        /*
                                                                                if (valorNuloOindefinidoOEmpyOCERO(topico.montoMandato)) {
                                                                                    invalidos = true;
                                                                                    mensajeDeAlerta += "<li>Campo <b>MONTO MANDATO</b> es requerido</li>";
                                                                                }
                                                                                if (valorNuloOindefinidoOEmpyOCERO(topico.montoContratado)) {
                                                                                    invalidos = true;
                                                                                    mensajeDeAlerta += "<li>Campo <b>MONTO CONTRATO</b> es requerido</li>";
                                                                                }*/
        if (invalidos) {
          mensajeDeAlerta =
            'Uno o mas campos no son  valido<br/>   #' + topico.numero + '<ul>' + mensajeDeAlerta;
          mensajeDeAlerta += '</ul>';
          UIkit.modal.alert(mensajeDeAlerta, { labels: { Ok: 'Ok' } });
          throw 'PROYECTO_NO_VALIDO';
        }
      }
    };
    $scope.normalizarMontos = function(proyecto) {
      for (var property in proyecto) {
        if (
          (property.includes('monto') || property.includes('saldo')) &&
          proyecto.hasOwnProperty(property)
        ) {
          var valor = proyecto[property];
          try {
            //console.log(property+ " = "+valor);
            proyecto[property] = valor.replace(/\D+/g, '');
          } catch (e) {
            ////console.log("No se puede normalizar " + property + " = " + valor);
          }
          // console.log(property+ " = "+valor);
          // //console.log("========");
        }
      }
    };
    $scope.normalizarProyecto = function(proyecto) {
      proyecto.topicos = proyecto.topicos || [];
      for (var _topico = 0; _topico < proyecto.topicos.length; _topico++) {
        proyecto.topicos[_topico].total =
          proyecto.topicos[_topico].total ||
          Object.assign(JSON.parse(JSON.stringify($scope.base.total)), {
            topicoTotalId: $scope.generarId()
          }); //;
        proyecto.topicos[_topico].estados = proyecto.topicos[_topico].estados || [];
        for (var _estado = 0; _estado < proyecto.topicos[_topico].estados.length; _estado++) {
          proyecto.topicos[_topico].estados[_estado].documentos =
            proyecto.topicos[_topico].estados[_estado].documentos || [];
          for (
            var _documento = 0;
            _documento < proyecto.topicos[_topico].estados[_estado].documentos.length.length;
            _documento++
          ) {
            proyecto.topicos[_topico].estados[_topico].documentos[_documento].adjunto =
              proyecto.topicos[_topico].estados[_estado].documentos[_documento].adjunto || [];
          }
        }
      }
      return proyecto;
    };
    $scope.transformarFecha = function(fecha) {
      try {
        return new Date(fecha).getTime();
      } catch (e) {
        return '';
      }
    };

    $scope.transformarFechaReverso = function(fecha) {
      try {
        var d = new Date(fecha);
        var m = d.getMonth() + 1;
        var dia = d.getDate();
        m = m < 10 ? '0' + m : m;
        ddia = dia < 10 ? '0' + dia : dia;
        return d.getFullYear() + '-' + m + '-' + ddia;
      } catch (e) {
        //console.log(e);
        return '';
      }
    };

    $scope.proyecto.fechaCreacion = $scope.transformarFechaReverso($scope.proyecto.fechaCreacion);
    $scope.proyecto.fechaInicio = $scope.transformarFechaReverso($scope.proyecto.fechaInicio);
    $scope.proyecto.fechaTermino = $scope.transformarFechaReverso($scope.proyecto.fechaTermino);
    $scope.proyecto.fechaCreacion = $scope.transformarFechaReverso($scope.proyecto.fechaCreacion);

    for (var z = 0; z < $scope.proyecto.topicos.length; z++) {
      for (var i = 0; i < $scope.conceptos.length; i++) {
        if (typeof $scope.proyecto.topicos[z][$scope.conceptos[i].arreglo] != 'undefined') {
          ////console.log("propiedad "+$scope.conceptos[i].arreglo+ " encontrada");
          for (var j = 0; j < $scope.proyecto.topicos[z][$scope.conceptos[i].arreglo].length; j++) {
            for (var property in $scope.proyecto.topicos[z][$scope.conceptos[i].arreglo][j]) {
              if (
                (property.includes('fecha') || property.includes('vigencia')) &&
                $scope.proyecto.topicos[z][$scope.conceptos[i].arreglo][j].hasOwnProperty(property)
              ) {
                var valor = $scope.proyecto.topicos[z][$scope.conceptos[i].arreglo][j][property];
                //console.log(property+ " = "+valor);
                $scope.proyecto.topicos[z][$scope.conceptos[i].arreglo][j][
                  property
                ] = $scope.transformarFechaReverso(valor);
              }
              if (property.includes('endoso')) {
                $scope.proyecto.topicos[z][$scope.conceptos[i].arreglo][j][property] =
                  $scope.proyecto.topicos[z][$scope.conceptos[i].arreglo][j][property] * 1 == 1
                    ? true
                    : false;
                continue;
              }
              var val = $scope.proyecto.topicos[z][$scope.conceptos[i].arreglo][j][property];
              if (typeof val != 'string' && isNaN(val) && !Array.isArray(val)) {
                $scope.proyecto.topicos[z][$scope.conceptos[i].arreglo][j][property] = 0;
              }
            }
          }
        }
      }
      if (typeof $scope.proyecto.topicos[z]['fechaInicio'] != 'undefined') {
        $scope.proyecto.topicos[z]['fechaInicio'] = $scope.transformarFechaReverso(
          $scope.proyecto.topicos[z]['fechaInicio']
        );
      }
      if (typeof $scope.proyecto.topicos[z]['fechaTermino'] != 'undefined') {
        $scope.proyecto.topicos[z]['fechaTermino'] = $scope.transformarFechaReverso(
          $scope.proyecto.topicos[z]['fechaTermino']
        );
      }
    }
    ////console.log("Resultado", $scope.proyecto);

    $scope.estado = {};
    $scope.configurarEstado = function(indice) {
      $scope.estado.indice = indice;
      //console.log(indice);
      $scope.estado.estado = $scope.topico['estados'][indice];
      //console.log(JSON.stringify($scope.estado.estado));
      // $scope.modales.portafolioModalesProyectosCrearConfigurarTopicoEstado.show();
    };

    $scope.agregarDocumento = function(indiceEstado) {
      var nuevoDoc = { documentoId: $scope.generarId(), nombreDocumento: 'Sin Título' };
      $scope.topico['estados'][indiceEstado]['documentos'] =
        $scope.topico['estados'][indiceEstado]['documentos'] || [];
      $scope.topico['estados'][indiceEstado]['documentos'].push(nuevoDoc);
    };
    $scope.habilitarEdicion = function(edit) {
      edit = !edit;
    };
    $scope.descargarAdjunto = function(adjunto) {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Descargando archivo adjunto..." +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_obtener_archivo_privado(
        {
          path: adjunto.adjuntoUrl,
          nombre: adjunto.adjuntoNombre
        },
        undefined,
        false,
        true
      )
        .then(function(response) {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
          if (response['data']['codigo'] != 10) {
          } else {
            window.open(response['data']['url']);
          }
        })
        .finally(function() {
          $rootScope.blockModal.hide();
        });
    };
    $scope.agregarAdjunto = function(documento) {
      //console.log(JSON.stringify(documento));
      var nuevoAdjunto = { adjuntoId: $scope.generarId(), adjuntoNombre: '' };
      documento.adjunto = documento.adjunto || [];
      documento.adjunto.push(nuevoAdjunto);
    };
    $scope.estados = [
      { value: 0, descripcion: 'Sin gestión' },
      { value: 1, descripcion: 'Estado de Pago Generado' },
      { value: 2, descripcion: 'Estado de Pago Completo' }
    ];

    // //console.log("SUBTITULOS CONFIGURADOS "+JSON.stringify($scope.subtitulos));
    $scope.subtitulos = {};
    $scope.subtitulos.subtituloActual = null;
    $scope.subtitulos.itemActual = null;
    $scope.subtitulos.subtitulosOpciones = SubTitulos;
    $scope.subtitulos.items = [];
    $scope.subtituloChange = function() {
      var actual = $scope.subtitulos.subtituloActual;
      try {
        actual = JSON.parse(actual);
      } catch (e) {
        console.err('Error al pasar actual');
      }
      ////console.log("SUB -- ", actual);
      ////console.log("NOM -- ", actual.nombre);
      $scope.subtitulos.items = actual['item'];
      ////console.log("items  1", actual ["item"]);
      ////console.log("items  2", $scope.subtitulos.items);
      $scope.generarTopicos();
    };
    $scope.generarTopicos = function() {
      $scope.proyecto.topicos = [];
      ////console.log("GENERAR TOPICO", $scope.proyecto.topicos, $scope.subtitulos.items);
      $scope.subtitulos.items.forEach(function(item, index) {
        var indice = $scope.nuevoTopico();
        //console.log(indice);
        $scope.proyecto.topicos[indice - 1].numero = item.item;
        $scope.proyecto.topicos[indice - 1].asunto = item.nombre;
        $scope.proyecto.topicos[indice - 1].monto = 0;
      });
      ////console.log("TOPICO GENERADO", $scope.proyecto.topicos);
    };

    // masked inputs
    var $maskedInput = $('input[data-inputmask]');
    if ($maskedInput.length) {
      ////console.log("INIT MASK", $maskedInput)
      $maskedInput.inputmask();
    }

    $scope.obtenerMontoAnticipo = function(topicosEnviado) {
      var topico = topicosEnviado || $scope.topico;
      topico.anticipos = topico.anticipos || [];
      if (topico.anticipos.length > 0) {
        return num(topico.anticipos[0].monto);
      }
      return 0;
    };
    $scope.getTotalesEstadoPago = function(estado) {
      calcularTotalEstadoPago(estado).then(function() {
        calcularTotalesTopico().then(function(response) {
          ////console.log("total calculado despues de cambio en un valor");
        });
      });
    };
    $scope.getMontoTotalEstadoPago = function(topico) {
      var totales = JSON.parse(JSON.stringify($scope.base.total));
      for (var i = 0; i < topico.estados.length; i++) {
        totales['avance'] += num(topico.estados[i]['avance'], 'avance');
        totales['devAnticipo'] += num(topico.estados[i]['devAnticipo'], 'devAnticipo');
        totales['subtotal'] += num(topico.estados[i]['subtotal']);
        totales['retencionInterna'] += num(
          topico.estados[i]['retencionInterna'],
          'retencionInterna'
        );
        totales['multa'] += num(topico.estados[i]['multa'], 'multa');
        totales['pago'] += num(topico.estados[i]['pago'], 'pago');
        try {
          topico.estados[i]['avancePorcentual'] =
            Math.round((num(topico.estados[i]['pago']) / num(topico.montoContratado)) * 100) +
            ($scope.topico.estados[i - 1] != undefined
              ? num($scope.topico.estados[i - 1]['avancePorcentual'])
              : 0);
        } catch (e) {
          if (i > 0) {
            console.error(e, i, topico);
          }
        }
        totales['porcentaje'] = topico.estados[i]['avancePorcentual'] || 0;
      }
      totales['saldoAnticipo'] = num(
        $scope.obtenerMontoAnticipo() - totales['devAnticipo'],
        'saldoAnticipo'
      );
      topico.total = Object.assign({ topicoTotalId: $scope.generarId() }, topico.total, totales);
      topico.total.restante =
        num(topico.montoContratado, 'montoCntratado') -
        num(totales['avance'], 'avance') -
        num(totales['saldoAnticipo'], 'saldo'); //monto contrato, menos avance total, menos anticopo saldo;
    };

    $scope.calcularTotalesTopico2 = function() {
      calcularTotalesTopico().then();
    };

    function calcularTotalesTopico(topicosEnviado) {
      return new Promise(function(resolve, reject) {
        var topico = topicosEnviado || $scope.topico;
        ////console.log("calculando topico", topico);
        $scope.getMontoTotalEstadoPago(topico);

        resolve(topico);
      });
    }

    function calcularTotalEstadoPago(estado) {
      return new Promise(function(resolve, reject) {
        estado['subtotal'] = num(estado['avance']) - num(estado['devAnticipo']);
        estado['pago'] =
          num(estado['subtotal']) - num(estado['retencionInterna']) /* - num(estado['multa'])*/;
        estado['pagoCMulta'] =
          num(estado['subtotal']) - num(estado['retencionInterna']) - num(estado['multa']);
        resolve(estado);
      });
    }

    $scope.calcularTotalYSaldoMandato = function(topico) {
      calcularTotalYSaldo(topico).then(function() {
        calcularTotalesTopico(topico).then(function() {
          carcularSaldoTotal($scope.proyecto.topicos).then(function(saldoTotal) {
            $scope.proyecto.montoDisponible = saldoTotal.saldoTotal;
            $scope.proyecto.montoOriginal = saldoTotal.montoTotalMandato;
            $scope.proyecto.montoTotal = saldoTotal.montoContratadoTotalMandato;
          });
        });
      });
    };

    function carcularSaldoTotal(topicos) {
      return new Promise(function(resolve, reject) {
        var saldoTotal = 0;
        var montoTotalMandato = 0,
          montoContratadoTotalMandato = 0;
        for (var i = 0; i < topicos.length; i++) {
          saldoTotal += num(topicos[i]['saldoMandato']);
          montoTotalMandato += num(topicos[i]['montoTotalMandato']);
          montoContratadoTotalMandato += num(topicos[i]['montoContratado']);
        }
        resolve({
          saldoTotal: saldoTotal,
          montoTotalMandato: montoTotalMandato,
          montoContratadoTotalMandato: montoContratadoTotalMandato
        });
      });
    }

    function calcularTotalYSaldo(topico) {
      return new Promise(function(resolve, reject) {
        topico.montoTotalMandato = num(topico.montoMandato) - num(topico.modificacionMontoMandato);
        topico.saldoMandato = num(topico.montoTotalMandato) - num(topico.montoContratado);
        resolve(topico);
      });
    }

    $scope.estadoPagoInterceptor = function(obj) {
      ////console.log("estado agregado");
      calcularTotalesTopico();
    };

    $scope.anticipoValidador = function(obj) {
      if ($scope.topico.anticipos.length > 0) {
        UIkit.modal.alert('Solo se permite 1 anticipo por proyecto', {
          labels: { Ok: 'Entendido' }
        });
        throw 'PROYECTO_NO_VALIDO_MAS_UNO_TOPICO';
      }
    };
    $scope.getPlazoTitulo = function(topico) {
      var mensaje = '';
      mensaje += '';
      var modificaciones = topico.plazoModificacion || [];

      if (modificaciones.length > 0) {
        mensaje += 'modificaciones (';

        for (var x = 0; x < modificaciones.length; x++) {
          if (x > 0) {
            mensaje += ' , ';
          }
          mensaje += '' + modificaciones[x].plazo;
        }
        mensaje += ')';
      }
      return mensaje;
    };
    $scope.agregarPlazo = function(topico, plazo) {
      if (plazo == 0) {
        return;
      }
      topico.plazoModificacion = topico.plazoModificacion || [];
      topico.plazoModificacion.push({ plazo: plazo, fecha: new Date().getTime() });
      ////console.log("plazo agregado", topico)
    };
    $scope.calcularPlazoTopico = function(topico) {
      var fechaInicio =
        topico.fechaInicio == undefined || new String(topico.fechaInicio).trim().length == 0
          ? moment().format('YYYY-MM-DD')
          : topico.fechaInicio;
      topico.plazoOriginal = topico.plazoOriginal || 0;
      var plazo = topico.plazoOriginal || 0;
      var modificaciones = topico.plazoModificacion || [];
      var fechaTermino = moment(fechaInicio).add(plazo, 'day');
      ////console.log("modificaciones plazo", fechaTermino, modificaciones);
      for (var x = 0; x < modificaciones.length; x++) {
        fechaTermino.add(modificaciones[x].plazo, 'day');
        plazo += modificaciones[x].plazo;
        ////console.log("modificaciones plazo", fechaTermino, modificaciones[x]);
      }
      topico.plazo = plazo;
      topico.fechaTermino = fechaTermino.format('YYYY-MM-DD');
      topico.fechaInicio = fechaInicio;
      ////console.log("plazo agregado 2", topico)
    };

    $scope.contratoInterceptor = function(contrato) {
      contrato['fechaInicio'] = $scope.transformarFechaReverso(new Date().getTime());
      contrato['plazoOriginal'] = 1;
      contrato.fechaTermino = moment(contrato['fechaInicio']).add(contrato['plazoOriginal'], 'day');
      $scope.calcularPlazoTopico(contrato);
    };
    $scope.devolucionInterceptor = function(devolucion) {
      $scope.topico['devolucion'] = $scope.topico['devolucion'] || [];
      var valorRetencionTotal = num($scope.topico.total['retencionInterna']);
      var retencionDevuelto = 0;
      for (var i = 0; i < $scope.topico['devolucion'].length; i++) {
        retencionDevuelto += num($scope.topico['devolucion']['monto']);
      }
      devolucion['monto'] = valorRetencionTotal - retencionDevuelto;
      devolucion['asunto'] = 'Devolución de retención';
      devolucion['fecha'] = $scope.transformarFechaReverso(new Date().getTime());
      //console.log(valorRetencionTotal, retencionDevuelto, devolucion);
    };
    $scope.asignarBoleta = function(devolucion, boleta) {
      devolucion.editando = false;
      //console.log('dev', devolucion, boleta);
      devolucion.boletaReferenciaId = boleta.boletaId;
    };

    $scope.modalModificacionesTopico = function(topico) {
      $scope.topico = topico;
      $scope.modales.portafolioModalesProyectosModificarTopico.show();
    };

    $scope.cancelarModificacion = function() {
      $scope.modificacionNuevaEditando = false;
    };
    $scope.guardarModificacion = function() {
      //console.log('guardando', $scope.topico.modificacionNueva);
      $scope.modificacionNuevaEditando = false;
      $scope.topico.modificacionesMandato = $scope.topico.modificacionesMandato || [];
      $scope.topico.modificacionesMandato.push(
        JSON.parse(JSON.stringify($scope.topico.modificacionNueva))
      );
      var montoModificaciones = 0;
      for (var i = 0; i < $scope.topico.modificacionesMandato.length; i++) {
        montoModificaciones += num($scope.topico.modificacionesMandato[i].monto);
      }
      $scope.topico.modificacionMontoMandato = montoModificaciones;
      $scope.calcularTotalYSaldoMandato($scope.topico);
    };
    $scope.agregarModificacion = function() {
      $scope.topico.modificacionNueva = {
        modificacionId: $scope.generarId(),
        fecha: $scope.transformarFechaReverso(new Date().getTime()),
        monto: 0,
        asunto: '--'
      };
      //console.log('agregando modificacion', $scope.topico.modificacionNueva);
      $scope.modificacionNuevaEditando = true;
    };

    $scope.obtenerDescripcionBoleta = function(boletaReferenciaId, boletas) {
      var desc = '';
      for (var i = 0; i < boletas.length; i++) {
        if (boletas[i].boletaId == boletaReferenciaId) {
          desc = boletas[i].folio;
          return desc;
        }
      }
      return desc;
    };
    $scope.scrollTo = function(hash) {
      //console.log('to ' + hash);
      $location.hash(hash);
    };

    $scope.provinciaChange = function(provincia) {
      $scope.provinciaChangePromise(provincia).then(function(d) {
        //console.log('comunas cambiadas ', d);
      });
    };
    $scope.provinciaChangePromise = function(provincia) {
      return new Promise(function(resolve, reject) {
        if (provincia != undefined && provincia != null) {
          $scope.comunasCargando = true;
          $scope.proyecto.comuna = 0;
          resolve(
            FebosAPI.cl_listar_comunas(
              {
                regionid: $stateParams.region,
                provinciaid: provincia
              },
              {},
              true,
              true
            ).then(function(response) {
              $scope.comunasCargando = false;

              $scope.comunas = [];
              $scope.comunas = response.data.lugares;
              $scope.proyecto.comuna = 0;
              return $scope.comunas;
            })
          );
        } else {
          resolve([]);
        }
      });
    };
    var iniciaSelecctablesProyecto = function(proyecto) {
      var unidadTecnica = proyecto.unidadTecnica || 0;
      var provincia = proyecto.provincia || 0;
      var comuna = proyecto.comuna || 0;
      proyecto.comuna = 0;
      $scope.comunas = [];
      //console.log('Se inica proyecto', proyecto);
      //console.log('provincia', provincia);
      //console.log('comuna', comuna);
      //console.log('unidad', unidadTecnica);
      proyecto.unidadTecnica = unidadTecnica;
      $scope.provinciaChangePromise(proyecto.provincia).then(function(comunas) {
        //console.log(comunas);
        proyecto.comuna = comuna;
      });
    };
    $scope.selectize_config_unidades = {
      maxItems: 1,
      valueField: 'iut',
      labelField: 'razonSocial',
      searchField: 'razonSocial',
      create: false,
      dropdownParent: 'body',
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.iut) +
            '  -  ' +
            escape(data.razonSocial) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.razonSocial) + '</div>';
        }
      }
    };
    $scope.selectize_config_provincia = {
      maxItems: 1,
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      create: false,
      dropdownParent: 'body',
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.id) +
            '  -  ' +
            escape(data.title) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.title) + '</div>';
        }
      }
    };
    $scope.selectize_config_comuna = {
      maxItems: 1,
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      create: false,
      dropdownParent: 'body',
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.id) +
            '  -  ' +
            escape(data.title) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.title) + '</div>';
        }
      }
    };

    $scope.obtenerFechaAdjunto = function(adjunto) {
      if (adjunto.fecha == undefined || adjunto.fecha.toString().trim().length == 0) {
        adjunto.fecha = moment().format('YYYY-MM-DD HH:MM:SS');
      }
      return adjunto.fecha;
    };
    $scope.adjuntoContratoFiltro = function(visibleOcultos) {
      return function(adjunto) {
        var visible =
          adjunto.estado == undefined ||
          adjunto.estado == 0 ||
          adjunto.estado == 1 ||
          visibleOcultos == 1;
        return visible;
      };
    };

    $scope.agregarComentarioEstadoPago = function(estado) {
      estado.comentarios = estado.comentarios || [];
      console.log('USUARIO ');
      var comentario = JSON.parse(JSON.stringify(SesionFebos().usuario));
      comentario.fecha = new Date().getTime();
      estado.comentarios.push(comentario);
    };
    iniciaSelecctablesProyecto($scope.proyecto);

    function num(val, key) {
      var valorRetornado = (val || 0) * 1;
      return valorRetornado;
    }

    function sortObjectByKey(objOriginal, key, asc) {
      function sortByKeyDesc(array, key) {
        return array.sort(function(a, b) {
          var x = a[key];
          var y = b[key];
          return x > y ? -1 : x < y ? 1 : 0;
        });
      }

      function sortByKeyAsc(array, key) {
        return array.sort(function(a, b) {
          var x = a[key];
          var y = b[key];
          return x < y ? -1 : x > y ? 1 : 0;
        });
      }

      if (asc) {
        return sortByKeyAsc(objOriginal, key);
      } else {
        return sortByKeyDesc(objOriginal, key);
      }
    }

    $scope.concat = function(val1, val2, val3, val4) {
      return (val1 || '') + (val2 || '') + (val3 || '') + (val4 || '');
    };
    $scope.calcularPlazo = function(fechaInicio, fechaFin) {
      var s = 0;
      try {
        var inicio = moment(fechaInicio, 'YYYY-MM-DD');
        var fin = moment(fechaFin, 'YYYY-MM-DD');
        var ms = fin.diff(inicio, 'days');
        s = ms;
      } catch (e) {
        console.error(e);
      }
      return isNaN(s) ? 0 : s;
    };

    $scope.monedasOptions = JSON.parse(JSON.stringify(_MONEDAS));
    $scope.tiposGarantiaOptions = JSON.parse(JSON.stringify(_TIPOSGARANTIA));

    $scope.selectize_config_id_title = {
      maxItems: 1,
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      create: false,
      dropdownParent: 'body',
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.title) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.title) + '</div>';
        }
        //,
        //option_create: function (data, escape) {
        //    return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
        // }
      }
    };

    $scope.selectize_config_tipo_garantia = {
      maxItems: 1,
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      create: true,
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.title) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.title) + '</div>';
        }
        //,
        //option_create: function (data, escape) {
        //    return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
        // }
      }
    };

    $scope.conceptosOptions = [
      //Seriedad de la Oferta, Fiel cumplimiento y anticipo de Fondos
      { value: 'Seriedad de la Oferta' },
      { value: 'Fiel cumplimiento' },
      { value: 'Anticipo de Fondos' }
    ];
    $scope.selectize_config_concepto = {
      maxItems: 1,
      valueField: 'value',
      labelField: 'value',
      searchField: 'value',
      create: true,
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.value) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.value) + '</div>';
        }
        //,
        //option_create: function (data, escape) {
        //    return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
        // }
      }
    };

    $scope.selectize_config_moneda = {
      maxItems: 1,
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      create: true,
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.title) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.title) + '</div>';
        }
        //,
        //option_create: function (data, escape) {
        //    return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
        // }
      }
    };

    console.log('End file');
  }
]);
