febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.anticipos', {
      url: '/:app/documentos/portafolio/:categoria/anticipos?pagina&itemsPorPagina&orden&filtros',
      templateUrl: 'app/febos/documentos/portafolio/anticipos/documentosView.html',
      controller: 'anticiposCtrl',
      //reloadOnSearch: false,
      params: {
        codigo: '',
        responsable: ''
      },
      data: {
        pageTitle: 'Anticipos'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            console.log('A');
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            console.log('B');
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            return FebosAPI.cl_listar_proyectos(
              {},
              {
                codigo: $stateParams.codigo,
                responsable: $stateParams.responsable
              },
              true,
              true
            ).then(function(response) {
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/documentos/portafolio/anticipos/documentosController.js'
            ]);
          }
        ]
      }
    });
  }
]);
