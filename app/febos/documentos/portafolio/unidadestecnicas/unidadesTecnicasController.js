angular.module('febosApp').controller('unidadesController', [
  '$rootScope',
  '$scope',
  '$timeout',
  '$location',
  'SesionFebos',
  'FebosAPI',
  'Datos',
  '$state',
  '$stateParams',
  '$http',
  function(
    $rootScope,
    $scope,
    $timeout,
    $location,
    SesionFebos,
    FebosAPI,
    Datos,
    $state,
    $stateParams,
    $http
  ) {
    $scope.ambiente = '';
    try {
      var partes = window.location.href
        .replace('https://', '')
        .replace('http://', '')
        .split('/');
      $scope.ambiente = partes[1].match(posiblesAmbientes) != null ? partes[1] : 'desarrollo';
    } catch (e) {
      $scope.ambiente = 'desarrollo';
    }
    $scope.app = $stateParams.app;
    $scope.categoria = $stateParams.categoria;
    $scope.vista = $stateParams.vista;
    var query = {};
    $scope.tituloVista = 'Unidades Tecnicas';
    $scope.unidades = Datos;
    //console.log("unidades ", $scope.unidades)
    $scope.generales = {
      cargandoUsuarios: true
    };
    /* $scope.usuarios = [];
         FebosAPI.cl_listar_usuarios({
             "pagina": 1,
             "filas": 400000
         }, true, false).then(function (response) {
                 $scope.usuarios = response.data.usuarios;
                 $scope.generales.cargandoUsuarios = false;
                 //console.log("USUARIOS R", response, $scope.usuarios);
             }
         );*/
    $scope.unidadActual = undefined;
    $scope.configurarUnidad = function(seleccionado) {
      if ($scope.unidadActual != undefined && $scope.unidadActual.iut == seleccionado.iut) {
        //console.log("1");
      } else {
        $scope.cargando = false;
        $scope.unidadActual = seleccionado;
        //console.log("2 ", $scope.unidadActual, $scope.cargando);
      }
    };
    $scope.esActual = function(comparar) {
      if ($scope.unidadActual) {
        if (comparar) {
          if ($scope.unidadActual.iut == comparar.iut) {
            return true;
          }
        }
      }
      return false;
    };
    $scope.mdlNuevaUnidad = UIkit.modal('#mdlNuevaUnidad');

    $scope.guardarUnidad = function(iut, razonSocial) {
      var unidad = { iut: iut, razonSocial: razonSocial };

      FebosAPI.cl_crear_unidades_tecnicas({}, { unidad: unidad })
        .then(function(response) {
          //console.log("REPONSE SAVE", respose);
        })
        .finally(function() {
          $scope.mdlNuevaUnidad.hide();
        });
    };
    $scope.modalAgregarUnidad = function() {
      $scope.mdlNuevaUnidad.show();
    };
  }
]);
