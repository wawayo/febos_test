febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.unidadestecnicas', {
      url: '/:app/documentos/portafolio/unidad/tecnica/?pagina&itemsPorPagina&orden&filtros',
      templateUrl: 'app/febos/documentos/portafolio/unidadestecnicas/unidadesTecnicas.html',
      controller: 'unidadesController',
      //reloadOnSearch: false,
      params: {
        codigo: '',
        responsable: ''
      },
      data: {
        pageTitle: 'Unidades Tecnicas'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            console.log('ANTERIOR');
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            console.log('NUEVO');
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        Datos: [
          '$stateParams',
          'FebosAPI',
          function($stateParams, FebosAPI) {
            try {
              console.log('CONSULTANDO CONFIGURACION');
              $stateParams.region = 0;
              return FebosAPI.cl_listar_unidades_tecnicas({}).then(function(response) {
                return response.data.listado;
              });
            } catch (e) {
              console.log(e);
            }
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            console.log('laz');
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/documentos/portafolio/unidadestecnicas/unidadesTecnicasController.js'
            ]);
          }
        ]
      }
    });
  }
]);
