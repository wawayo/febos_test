angular.module('febosApp').controller('polizasCtrl', [
  '$rootScope',
  '$scope',
  '$timeout',
  'variables',
  '$location',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  'Datos',
  'MONEDAS',
  '$state',
  '$stateParams',
  'VistasPortafolio',
  '$http',
  'ComplementoPortafolio',
  function(
    $rootScope,
    $scope,
    $timeout,
    variables,
    $location,
    SesionFebos,
    FebosAPI,
    FebosUtil,
    Datos,
    _MONEDAS,
    $state,
    $stateParams,
    VistasPortafolio,
    $http,
    ComplementoPortafolio
  ) {
    $scope.ambiente = '';
    try {
      var partes = window.location.href
        .replace('https://', '')
        .replace('http://', '')
        .split('/');
      $scope.ambiente = partes[1].match(posiblesAmbientes) != null ? partes[1] : 'desarrollo';
    } catch (e) {
      $scope.ambiente = 'desarrollo';
    }
    $.datepicker.setDefaults({
      dateFormat: 'YYYY-MM-DD',
      i18n: {
        months: [
          'Enero',
          'Febrero',
          'Marzo',
          'Abril',
          'Mayo',
          'Junio',
          'Julio',
          'Agosto',
          'Septiembre',
          'Octubre',
          'Noviembre',
          'Diciembre'
        ],
        weekdays: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
      }
    });

    var modales = $('.uk-modal');
    for (var i = 0; i < modales.length; i++) {
      var modal = modales[i];
      $scope[modal.id] = UIkit.modal('#' + modal.id);
    }

    $scope.app = $stateParams.app;
    $scope.categoria = $stateParams.categoria;
    $scope.vista = $stateParams.vista;
    var query = {};
    $scope.tituloVista = 'Pólizas';
    $location.search(query);

    $scope.modales = FebosUtil.modales(['portafolio_modales_proyectos_crear']);
    //$scope.modales.portafolioModalesProyectosCrear.show();

    $scope.proyecto = {
      topicos: []
    };
    $scope.FebosUtil = FebosUtil;

    $scope.transformarFechaReverso = function(fecha) {
      try {
        var d = new Date(fecha);
        var m = d.getMonth() + 1;
        var dia = d.getDate();
        m = m < 10 ? '0' + m : m;
        ddia = dia < 10 ? '0' + d : d;
        return d.getFullYear() + '-' + m + '-' + dia;
      } catch (e) {
        console.log(e);
        return '';
      }
    };
    $scope.conceptos = [
      { nombre: 'Boleta', arreglo: 'boletas' },
      { nombre: 'Anticipo', arreglo: 'anticipos' },
      { nombre: 'Poliza', arreglo: 'polizas' },
      { nombre: 'Estado', arreglo: 'estados' },
      { nombre: 'Contacto', arreglo: 'contactos' }
    ];

    $scope.documentos = Datos.polizas;

    $scope.parametros = $location.search();
    $scope.variablesPaginacion = {
      pagina: parseInt($location.search().pagina),
      itemsPorPagina: parseInt($location.search().itemsPorPagina)
      //itemsPorPagina: Datos.documentos.length,
    };

    $scope.paginacion = {
      //elementoDesde: (((parseInt($location.search().pagina) - 1) * parseInt($location.search().itemsPorPagina)) + 1),
      //elementoHasta: (((parseInt($location.search().pagina) - 1) * parseInt($location.search().itemsPorPagina)) + Datos.documentos.length),
      //total: Datos.totalElementos,
      //paginas: parseInt(Datos.totalPaginas)
    };

    var $documentos = $('#documentos');

    if (typeof SesionFebos().documentosNoTributariosSeleccionados == 'undefined') {
      SesionFebos().documentosNoTributariosSeleccionados = [];
    }
    $scope.documentosSeleccionados = SesionFebos().documentosNoTributariosSeleccionados;
    $scope.estaSeleccionado = function(id) {
      if ($.inArray(id, $scope.documentosSeleccionados) >= 0) {
        return true;
      } else {
        return false;
      }
    };

    $scope.listarSeleccionados = function() {
      var params = $location.search();
      params.filtros = 'febosId:' + $scope.documentosSeleccionados.join(',');
      params.pagina = 1;
      $location.search(params);
      $state.go('restringido.dnt', {
        app: $stateParams.app,
        categoria: $stateParams.categoria,
        vista: $stateParams.vista,
        filtros: params.filtros,
        orden: $stateParams.orden,
        pagina: params.pagina,
        itemsPorPagina: params.itemsPorPagina
      });
      $state.reload();
    };
    $scope.deseleccionarTodo = function() {
      var cantidad = $scope.documentosSeleccionados.length;
      $timeout(function() {
        $documentos.find('.select_message').each(function() {
          if ($(this).is(':checked')) {
            $(this).iCheck('uncheck');
          }
        });
        while (typeof $scope.documentosSeleccionados.pop() != 'undefined');
      }, 100);

      var mensaje =
        cantidad == 1
          ? 'Se deseleccionó 1 documento'
          : 'Se deseleccionaron ' + cantidad + ' documentos';
      UIkit.notify(mensaje + "<a class='notify-action'>[X]</a> ", {
        status: 'success',
        timeout: 3000
      });

      // UIkit.notify(mensaje + "<a class='notify-action'>[X]</a> ", {status: "success", timeout: 3000});
    };
    $scope.escanearDocumentosSeleccionados = function() {
      //revisar si habian documentos guardados en la sesion
      var seleccionados = [];
      var deseleccionados = [];
      $documentos.find('.select_message').each(function(index, elemento) {
        var febosId = $(elemento)
          .attr('id')
          .split('_')[1];
        if ($(elemento).is(':checked')) {
          seleccionados.push(febosId);
        } else {
          deseleccionados.push(febosId);
        }
      });
      //insertar seleccionados
      $.each(seleccionados, function(i, el) {
        if ($.inArray(el, $scope.documentosSeleccionados) === -1) {
          $scope.documentosSeleccionados.push(el);
        }
      });
      var eliminar = [];
      $.each($scope.documentosSeleccionados, function(i, el) {
        if ($.inArray(el, deseleccionados) >= 0) {
          eliminar.push(i);
        }
      });
      //eliminar indices deseleccionados
      for (var i = eliminar.length - 1; i >= 0; i--) {
        $scope.documentosSeleccionados.splice(eliminar[i], 1);
      }
      setTimeout(function() {
        $scope.$apply();
      }, 200);
    };
    // seleccionar un documento
    $documentos.on('ifChanged', '.select_message', function() {
      $(this).is(':checked')
        ? $(this)
            .closest('li')
            .addClass('md-card-list-item-selected')
        : $(this)
            .closest('li')
            .removeClass('md-card-list-item-selected');
      $scope.escanearDocumentosSeleccionados();
    });

    // seleccionar todos los documentos
    $('#documentos_seleccionar_todos').on('ifChanged', function() {
      var $this = $(this);
      $documentos.find('.select_message').each(function() {
        $this.is(':checked') ? $(this).iCheck('check') : $(this).iCheck('uncheck');
      });
      $scope.escanearDocumentosSeleccionados();
    });

    // ver el detalle de un documento
    $scope.hayUnDetalleDesplegado = false;
    $documentos.on('click', '.md-card-list ul > li.seleccionable', function(e) {
      if (
        !$(e.target).closest('.md-card-list-item-menu').length &&
        !$(e.target).closest('.md-card-list-item-select').length
      ) {
        var $this = $(this);
        if (!$this.hasClass('item-shown')) {
          // obtener la altura del detalle del documento
          var el_min_height =
            $this.height() + $this.children('.md-card-list-item-content-wrapper').actual('height');
          // esconder el contenido del mensaje
          $documentos.find('.item-shown').velocity('reverse', {
            begin: function(elements) {
              $(elements)
                .removeClass('item-shown')
                .children('.md-card-list-item-content-wrapper')
                .hide()
                .velocity('reverse');
            }
          });
          // mostrar el mensaje
          $this.velocity(
            {
              marginTop: 40,
              marginBottom: 40,
              marginLeft: 0,
              marginRight: 0,
              minHeight: el_min_height
            },
            {
              duration: 300,
              easing: variables.easing_swiftOut,
              begin: function(elements) {
                $(elements).addClass('item-shown');
              },
              complete: function(elements) {
                // show: message content, reply form
                $(elements)
                  .children('.md-card-list-item-content-wrapper')
                  .show()
                  .velocity({
                    opacity: 1
                  });
                // scroll to message
                var container = $('body'),
                  scrollTo = $(elements);
                container.animate(
                  {
                    scrollTop: scrollTo.offset().top - $('#page_content').offset().top - 8
                  },
                  1000,
                  variables.bez_easing_swiftOut
                );
                $scope.hayUnDetalleDesplegado = true;
              }
            }
          );
        }
      }
    });
    // esconder el detalle del documento al: hacer click afuera, apretar escape
    $(document).on('click keydown', function(e) {
      if (!$(e.target).closest('li.item-shown').length || e.which == 27) {
        $documentos.find('.item-shown').velocity('reverse', {
          begin: function(elements) {
            $(elements)
              .removeClass('item-shown')
              .children('.md-card-list-item-content-wrapper')
              .hide()
              .velocity('reverse');
          }
        });
      }
    });

    $scope.avanzarPagina = function() {
      var params = verificacionDeParametros;
      var siguientePagina = parseInt(params.pagina) + 1;
      if (siguientePagina > $scope.paginacion.paginas) {
        UIkit.notify("No hay mas páginas <a class='notify-action'>[X]</a> ", {
          status: 'danger',
          timeout: 2000
        });
      } else {
        $location.search('pagina', siguientePagina);
        $state.go('restringido.dnt', {
          app: $stateParams.app,
          categoria: $stateParams.categoria,
          vista: $stateParams.vista,
          filtros: $stateParams.filtros,
          orden: $stateParams.orden,
          pagina: siguientePagina,
          itemsPorPagina: $stateParams.itemsPorPagina
        });
        $state.reload();
      }
    };
    $scope.retrocederPagina = function() {
      var params = verificacionDeParametros.pagina;
      var siguientePagina = parseInt(params.pagina) - 1;
      if (siguientePagina < 1) {
        UIkit.notify(
          "Ésta es la primera página, no se puede <br/>retroceder más =( <a class='notify-action'>[X]</a> ",
          {
            status: 'danger',
            timeout: 2000
          }
        );
      } else {
        $location.search('pagina', siguientePagina);
        $state.go('restringido.dnt', {
          app: $stateParams.app,
          categoria: $stateParams.categoria,
          vista: $stateParams.vista,
          filtros: $stateParams.filtros,
          orden: $stateParams.orden,
          pagina: siguientePagina,
          itemsPorPagina: $stateParams.itemsPorPagina
        });
        $state.reload();
      }
    };
    $scope.mostrarInputPagina = false;
    $scope.paginaIngresada = 1;
    $scope.irPagina = function(mostrarInputPagina, valor) {
      $scope.mostrarInputPagina = mostrarInputPagina;
      if (mostrarInputPagina) {
        $scope.paginaIngresada = parseInt($location.search().pagina);
      } else {
        var pagina = parseInt($('#paginaManual')[0].value);
        if (pagina > $scope.paginacion.paginas || pagina <= 0) {
          UIkit.notify(
            'Imposible ir la página ' +
              pagina +
              ", ya que no existe <a class='notify-action'>[X]</a> ",
            {
              status: 'danger',
              timeout: 2000
            }
          );
        } else {
          $location.search('pagina', pagina);
          $state.go('restringido.dnt', {
            app: $stateParams.app,
            categoria: $stateParams.categoria,
            vista: $stateParams.vista,
            filtros: $stateParams.filtros,
            orden: $stateParams.orden,
            pagina: pagina,
            itemsPorPagina: $stateParams.itemsPorPagina
          });
          $state.reload();
        }
      }
      //var params = $location.search();
      //$location.search("pagina", parseInt(params.pagina) + 1);
    };

    $scope.comprarSiLosFiltrosSonIguales = function(A, B) {
      ////console.log("comparando", A, B);
      if (A.filtros == B.filtros && A.orden == B.orden) {
        return true;
      }
      return false;
    };
    $scope.detectarTitulo = function() {
      var query = $location.search();
      if (typeof SesionFebos().filtrosDnt != 'undefined') {
        for (var i = 0; i < SesionFebos().filtrosDnt[$stateParams.categoria].length; i++) {
          var parametros = SesionFebos().filtrosDnt[$stateParams.categoria][i];
          if ($scope.comprarSiLosFiltrosSonIguales(query, parametros.params)) {
            return (
              VistasPortafolio[$stateParams.app][$stateParams.categoria][$stateParams.vista]
                .titulo +
              ' aplicando mi vista: ' +
              SesionFebos().filtrosDnt[$stateParams.categoria][i].nombre
            );
          }
        }
      }
      return VistasPortafolio[$stateParams.app][$stateParams.categoria][$stateParams.vista].titulo;
    };
    //$scope.tituloVista = $scope.detectarTitulo();
    //$rootScope.$on('refrescarTituloDnt', function () {
    //    $scope.tituloVista = $scope.detectarTitulo();
    //})
    $scope.formatearFecha = function(fechaComoNumero) {
      if (typeof fechaComoNumero != 'undefined' && fechaComoNumero != '' && fechaComoNumero > 0) {
        var fecha = new Date(fechaComoNumero);
        var dia = fecha.getDate();
        dia = dia < 10 ? '0' + dia : dia;
        var mes = fecha.getMonth() + 1;
        mes = mes < 10 ? '0' + mes : mes;
        return dia + '-' + mes + '-' + fecha.getFullYear();
      }
      return '';
    };
    $scope.formatearMonto = function(numero, simbolo) {
      if (typeof numero != 'undefined') {
        var partes = numero.toString().split('.');
        partes[0] = partes[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
        return simbolo + ' ' + partes.join(',');
      } else {
        return '';
      }
    };

    $scope.calcularAvanceProyecto = function(proyecto) {
      /* var estadosTotales = 0;
       var estadosCompletados = 0;
       for (var i = 0; i < proyecto.topicos.length; i++) {
           var estados = proyecto.topicos[i].estados.length;
           estados = isNaN(estados) ? 0 : estados;
           estadosTotales += estados;
           if (proyecto.topicos[i].estados.length > 0) {
               for (var j = 0; j < proyecto.topicos[i].estados.length; j++) {
                   if (proyecto.topicos[i].estados[j].estado == 1 || proyecto.topicos[i].estados[j].estado == 2) {
                       estadosCompletados++;
                   }
               }
           }
       }
       try {
           var t=Math.round(estadosCompletados * 100 / estadosTotales);
           return isNaN(t)?0:t;
       } catch (e) {
           return 0;
       }*/
    };

    $scope.agregarHito = function() {
      $scope.proyecto.topicos.push({});
    };
    $scope.eliminarHito = function(indice) {
      $scope.proyecto.topicos.splice(indice, 1);
    };

    $scope.crearProyecto = function() {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Guardando proyecto...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_crear_proyecto({}, { proyecto: $scope.proyecto }).then(function(response) {
        console.log(response);
      });
    };
    $scope.buscar = '';
    $scope.busqueda = function(item) {
      //if($scope.buscar == "")return true;
      if (
        JSON.stringify(item)
          .toLowerCase()
          .includes($scope.buscar.toLowerCase())
      ) {
        return true;
      }
      return false;
    };
    $scope.uploadArchivo = function(id, mime, url) {
      FebosAPI.cl_obtener_url_prefirmada({}, {}, true, true).then(function(response) {
        console.log(response);
        var url = response.data.url;
      });
    };

    $scope.mdlEnviarPoliza = UIkit.modal('#mdlEnviarPoliza');
    $scope.mdlRenovarPoliza = UIkit.modal('#mdlRenovarPoliza');
    $scope.modalEnviar = function(boletaActual) {
      $scope.mdlEnviarPoliza.show();
      $scope.polizaActual = boletaActual;
    };
    $scope.obtenerMonto = function(documento) {
      if (documento.montoDecimal * 1 > 0) {
        return documento.montoDecimal;
      }
      return documento.monto;
    };
    $scope.modalRenovar = function(boletaActual) {
      $scope.mdlRenovarPoliza.show();
      $scope.polizaActual = boletaActual;
      $scope.renovacion = {};
      $scope.renovacion.polizaId = new Date().valueOf() + '-' + FebosUtil.uuid();
      $scope.renovacion.proyectoId = $scope.polizaActual.proyectoId;
      $scope.renovacion.topicoId = $scope.polizaActual.topicoId;
      $scope.renovacion.empresa = $scope.polizaActual.empresa;
      $scope.renovacion.polizaReferenciaId = $scope.polizaActual.polizaId;
    };
    $scope.enviar = function(mensaje, copias, destinatario) {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Enviando póliza...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      console.log('enviando poliza', $scope.polizaActual);
      FebosAPI.cl_correo_envio_portafolio(
        {
          febosId: $scope.polizaActual.boletaGarantiaId,
          tipo: 'poliza',
          destinatario: destinatario,
          copias: copias == undefined ? '' : copias
        },
        { mensaje: mensaje == undefined ? '' : FebosUtil.encode(mensaje) },
        true,
        true
      ).then(function(response) {
        $scope.mdlEnviarPoliza.hide();
        UIkit.notify("<i style='color:white' class='uk-icon-check'></i> Póliza enviada!", {
          status: 'success',
          timeout: 5000,
          pos: 'top-center'
        });
      });

      $scope.mensaje = '';
      $scope.copias = '';
      $scope.destinatario = '';
    };
    $scope.renovarPoliza = function(poliza) {
      $scope.inserar = false;
      var b = JSON.parse(JSON.stringify(poliza));
      for (var key in b) {
        if (b.hasOwnProperty(key)) {
          if (key.includes('fecha')) {
            b[key] = new Date(b[key]).getTime();
          }
        }
      }
      b.estado = 1;
      b.vigente = 1;
      b.endoso = new String(b.endoso) == 'true' ? 1 : 0;
      b.usuarioGestorNombre = SesionFebos().usuario.nombre;
      b.usuarioGestorCorreo = SesionFebos().usuario.correo;
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_crear_documento_portafolio(
        {},
        {
          poliza: b
        },
        true,
        true
      ).then(function(response) {
        $scope.mdlRenovarPoliza.hide();
        UIkit.notify("<i style='color:white' class='uk-icon-check'></i> !", {
          status: 'success',
          timeout: 5000,
          pos: 'top-center'
        });
        $scope.documentos.push(poliza);
      });
    };
    $scope.monedasOptions = JSON.parse(JSON.stringify(_MONEDAS));
    $scope.selectize_config_id_title = {
      maxItems: 1,
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      create: false,
      // dropdownParent: "body",
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.title) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.title) + '</div>';
        }
        //,
        //option_create: function (data, escape) {
        //    return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
        // }
      }
    };

    $scope.cargandoProyectos = true;
    $scope.proyectos = [];

    function cargarProyectos() {
      return FebosAPI.cl_listar_proyectos_cabecera({}, {}, true, true).then(function(response) {
        $scope.cargandoProyectos = false;
        $scope.proyectos = response.data.proyectos;
        $scope.proyectosOptions = JSON.parse(JSON.stringify(response.data.proyectos));
        return response.data.proyectos;
      });
    }

    $scope.obtenerDescripcionProyecto = function(doc) {
      if (doc.proyectoId) {
        for (var i = 0; i < $scope.proyectos.length; i++) {
          var proyecto = $scope.proyectos[i];
          if (proyecto.proyectoId == doc.proyectoId) {
            return '(' + proyecto.codigo + ') ' + proyecto.nombre;
          }
        }
      }
      return '-';
    };
    $scope.proyectosOptions = [];

    $scope.selectize_config_proyecto = {
      maxItems: 1,
      valueField: 'proyectoId',
      labelField: 'codigo',
      create: false,
      render: {
        option: function(data, escape) {
          return (
            '<div class="option" title="' +
            escape(data.codigo) +
            '" data-uk-tooltip="{cls:\'long-text\'}">' +
            '<span class="title" >(' +
            escape(data.codigo) +
            ') ' +
            escape(data.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return (
            '<div class="item" title="' +
            escape(data.codigo) +
            '" data-uk-tooltip="{cls:\'long-text\'}">' +
            escape(data.codigo) +
            '</div>'
          );
        }
      }
    };
    cargarProyectos();

    $scope.modalAgregarPoliza = function() {
      $scope.inserar = true;
      $scope.ModalBoletaTitle = 'Insertar ';
      $scope.mensajeActualizacion = 'Insertada ';
      $scope.renovacion = {};
      $scope.renovacion.polizaId = new Date().valueOf() + '-' + FebosUtil.uuid();
      $scope.mdlRenovarPoliza.show();
    };
  }
]);
