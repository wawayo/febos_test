febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.dnt', {
      url: '/:app/documentos/no_tributarios/:categoria/:vista?pagina&itemsPorPagina&orden&filtros',
      templateUrl: 'app/febos/documentos/no_tributarios/documentosView.html',
      controller: 'documentosNoTributariosCtrl',
      reloadOnSearch: false,
      params: {
        pagina: '',
        itemsPorPagina: '',
        filtros: '',
        orden: ''
      },
      data: {
        pageTitle: 'Documentos No Tributarios'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        verificacionDeParametros: [
          'ComplementoDnt',
          '$location',
          '$stateParams',
          'FebosUtil',
          '$state',
          'SesionFebos',
          '$rootScope',
          function(
            ComplementoDnt,
            $location,
            $stateParams,
            FebosUtil,
            $state,
            SesionFebos,
            $rootScope
          ) {
            var categoria = $stateParams.categoria || 'recibidos';
            var pagina = $stateParams.filtros != '' ? $location.search().pagina || 1 : 1;
            var itemsPorPagina =
              $stateParams.filtros != '' ? $location.search().itemsPorPagina || 20 : 20;
            var orden =
              $stateParams.filtros != ''
                ? typeof $location.search().orden == 'undefined' || $location.search().orden == ''
                  ? categoria == 'emitidos'
                    ? '-fechaEmision'
                    : '-fechaEmision'
                  : $location.search().orden
                : '-fechaEmision';
            //objeto para reemplazar en la ruta URL
            var queryParams = {
              pagina: pagina,
              itemsPorPagina: itemsPorPagina,
              orden: orden
            };

            queryParams.filtros = ComplementoDnt.prepararFiltrosDnt(
              {
                pagina: $stateParams.pagina,
                itemsPorPagina: $stateParams.itemsPorPagina,
                filtros: $stateParams.filtros,
                orden: $stateParams.orden
              },
              $stateParams.app,
              $stateParams.categoria,
              $stateParams.vista
            );
            $rootScope.queryParams = $location.search();

            var st = {};
            st.pagina = queryParams.pagina;
            st.itemsPorPagina = queryParams.itemsPorPagina;
            st.orden = queryParams.orden;
            st.filtros = queryParams.filtros;
            st.app = $stateParams.app;
            st.categoria = $stateParams.categoria;
            st.vista = $stateParams.vista;

            return st;
          }
        ],
        Datos: [
          'FebosAPI',
          '$location',
          'ComplementoDnt',
          '$rootScope',
          'verificacionDeParametros',
          function(FebosAPI, $location, ComplementoDnt, $rootScope, verificacionDeParametros) {
            //$stateParams.app;  // cloud, proveedores, clientes
            //$stateParams.sentido; // emitidos, recibidos
            //'filtros': "rutReceptor:76179952-5|fechaRecepcion:2018-04-01--2018-05-05",
            //seteo valores por defecto si no vienen
            console.log('A');
            $rootScope.configFiltrosActivo = false;
            var filtros = verificacionDeParametros.filtros;
            //var filtros = verificacionDeParametros.filtros;
            var rangos = Object.keys(ComplementoDnt.atajosDeRangosDeFecha);
            for (var i = 0; i < rangos.length; i++) {
              var rango = rangos[i];
              if (filtros.includes(rango)) {
                filtros = filtros.replace(
                  rango,
                  ComplementoDnt.atajosDeRangosDeFecha[rango].desde +
                    '--' +
                    ComplementoDnt.atajosDeRangosDeFecha[rango].hasta
                );
                i--;
              }
            }

            /*
                            var reemplazosDeBusquedaLike=['rutCesionario','razonSocialEmisor','razonSocialReceptor','razonSocialCesionario','comentario','lugar'];
                            for(var i=0;i<reemplazosDeBusquedaLike.length;i++){
                                if(filtros.includes(reemplazosDeBusquedaLike[i])){
                                    filtros = filtros.replace(reemplazosDeBusquedaLike[i]+":",reemplazosDeBusquedaLike[i]+"$");
                                }
                            }
                            */
            return FebosAPI.cl_listar_dnt({
              pagina: verificacionDeParametros.pagina,
              itemsPorPagina: verificacionDeParametros.itemsPorPagina,
              campos:
                'tipo,numero,receptorRut,fechaEmision,totalPrecioTotalNeto,totalMonedaTipo,fechaActualizacion,compradorNombre,compradorArea,emisorRut,emisorRazonSocial,receptorRazonSocial,leido',
              //'filtros': $location.search().filtros,
              filtros: filtros,
              orden: verificacionDeParametros.orden
            }).then(function(response) {
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/documentos/no_tributarios/documentosController.js'
            ]);
          }
        ]
      }
    });
  }
]);
