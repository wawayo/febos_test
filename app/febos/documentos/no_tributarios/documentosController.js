angular.module('febosApp').controller('documentosNoTributariosCtrl', [
  '$rootScope',
  '$scope',
  '$timeout',
  'variables',
  '$location',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  'Datos',
  '$state',
  '$stateParams',
  'VistasDnt',
  'verificacionDeParametros',
  '$http',
  'ComplementoDnt',
  function(
    $rootScope,
    $scope,
    $timeout,
    variables,
    $location,
    SesionFebos,
    FebosAPI,
    FebosUtil,
    Datos,
    $state,
    $stateParams,
    VistasDnt,
    verificacionDeParametros,
    $http,
    ComplementoDnt
  ) {
    //'$location','$timeout','$rootScope',
    //function ($location,$timeout,$rootScope) {
    //$location.search($rootScope.queryParams);
    // $scope.$on('$locationChangeSuccess', function () {
    //    $state.reload();
    //});
    $scope.app = $stateParams.app;
    $scope.categoria = $stateParams.categoria;
    var query = {
      pagina: verificacionDeParametros.pagina,
      itemsPorPagina: verificacionDeParametros.itemsPorPagina,
      orden: verificacionDeParametros.orden,
      filtros: verificacionDeParametros.filtros
    };
    $location.search(query);

    $scope.FebosUtil = FebosUtil;
    $scope.documentos = Datos.documentos;
    $scope.parametros = $location.search();
    $scope.variablesPaginacion = {
      pagina: parseInt($location.search().pagina),
      itemsPorPagina: parseInt($location.search().itemsPorPagina),
      itemsPorPagina: Datos.documentos.length
    };

    $scope.paginacion = {
      elementoDesde:
        (parseInt($location.search().pagina) - 1) * parseInt($location.search().itemsPorPagina) + 1,
      elementoHasta:
        (parseInt($location.search().pagina) - 1) * parseInt($location.search().itemsPorPagina) +
        Datos.documentos.length,
      total: Datos.totalElementos,
      paginas: parseInt(Datos.totalPaginas)
    };
    $scope.fechaEnProsa = function(fecha, conHora) {
      if (typeof fecha == 'undefined') return '';
      var meses = [
        '',
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre'
      ];

      conHora = typeof conHora == 'undefined' ? false : conHora;
      var partes = fecha.split(' ');
      var fracciones = partes[0].split('-');
      var anio = fracciones[0].length == 4 ? fracciones[0] : fracciones[2];
      var mes = fracciones[1];
      var dia = fracciones[0].length == 4 ? fracciones[2] : fracciones[0];
      var anioActual = new Date().getFullYear();
      var diferenciaEnAnios = anioActual - anio;
      var anioEnProsa = '';
      switch (diferenciaEnAnios) {
        case 0:
          anioEnProsa = ' de este año';
          break;
        case 1:
          anioEnProsa = ' del año pasado';
          break;
        case 2:
          anioEnProsa = ' del año antepasado';
          break;
        default:
          anioEnProsa = ' del año ' + anio;
      }

      var prosa = parseInt(dia) + ' de ' + meses[parseInt(mes)] + anioEnProsa;

      if (partes.length == 2 && conHora) {
        var fraccionHora = partes[1].split(':');
        var hora = parseInt(fraccionHora[0]);
        var minuto = parseInt(fraccionHora[1]);

        prosa += ', ';
        if (minuto == 0) {
          if (hora < 11) {
            prosa += 'a las ' + hora + ' en punto de la mañana';
          } else if (hora == 12) {
            prosa += 'justo a medio día';
          } else if (hora == 0) {
            prosa += 'justo a media noche';
          } else if (hora > 12 && hora <= 19) {
            prosa += 'a las ' + (hora - 12) + ' en punto de la tarde';
          } else if (hora > 19 && hora <= 23) {
            prosa += 'a las ' + (hora - 12) + ' en punto de la noche';
          }
        } else if (minuto == 15) {
          if (hora <= 12) {
            prosa += 'a las ' + hora + ' y cuarto de la mañana';
          } else if (hora >= 12 && hora <= 19) {
            prosa += 'a las ' + (hora - 12) + ' y cuarto de la tarde';
          } else if (hora > 19 && hora <= 23) {
            prosa += 'a las ' + (hora - 12) + ' y cuarto de la noche';
          }
        } else if (minuto == 30) {
          if (hora <= 12) {
            prosa += 'a las ' + hora + ' y media de la mañana';
          } else if (hora >= 12 && hora <= 19) {
            prosa += 'a las ' + (hora - 12) + ' y media de la tarde';
          } else if (hora > 19 && hora <= 23) {
            prosa += 'a las ' + (hora - 12) + ' y media de la noche';
          }
        } else if (minuto == 45) {
          if (hora <= 11) {
            prosa += 'un cuarto para las ' + hora + 1 + ' de la mañana';
          }
          if (hora == 12) {
            prosa += 'un cuarto para las 1 de la tarde';
          } else if (hora > 12 && hora <= 19) {
            prosa += 'un cuarto para las ' + (hora - 11) + ' de la tarde';
          } else if (hora > 19 && hora <= 23) {
            prosa += 'un cuarto para las ' + (hora - 11) + ' de la noche';
          }
        } else {
          if (hora <= 12) {
            prosa +=
              'a las ' +
              hora +
              ' de la mañana con ' +
              minuto +
              ' minuto' +
              (minuto == 1 ? '' : 's');
          } else if (hora >= 12 && hora <= 19) {
            prosa +=
              'a las ' +
              (hora - 12) +
              ' de la tarde con ' +
              minuto +
              ' minuto' +
              (minuto == 1 ? '' : 's');
          } else if (hora > 19 && hora <= 23) {
            prosa +=
              'a las ' +
              (hora - 12) +
              ' de la noche con ' +
              minuto +
              ' minuto' +
              (minuto == 1 ? '' : 's');
          }
        }
      }
      return prosa;
    };
    $scope.plazoEnProsa = function(documento) {
      if (
        typeof documento.estadoComercial == 'undefined' ||
        documento.estadoComercial == 0 ||
        documento.estadoComercial == 1 ||
        documento.estadoComercial == 3
      ) {
        if (
          ['0', '1', '3'].indexOf(documento.estadoComercial.toString()) >= 0 &&
          documento.plazo !== undefined &&
          ['33', '34', '43'].indexOf(documento.tipoDocumento.toString()) >= 0 &&
          documento.formaDePago != 1 &&
          documento.formaDePago != 3
        ) {
          if (documento.plazo <= 0) {
            return ', al cual se le <b>ha vencido el plazo para realizar la aceptación o reclamo</b>, por lo que se entiende como <b>aceptada comercialmente</b> según las instrucciones dadas por el <b>Servicio de Impuestos Internos</b>';
          }
          if (documento.plazo == 1) {
            return ', al cual sólo le queda <b>1 día disponible<b/> para realizar la aceptación o reclamo en el <b>Servicio de Impuestos Internos</b>, de lo contrario, se entenderá como <b>aceptada comercialmente</b>';
          } else if (documento.plazo == 2 || documento.plazo == 3) {
            return (
              ', al cual sólo le quedan <b>' +
              documento.plazo +
              ' días disponibles<b/> para realizar la aceptación o reclamo en el <b>Servicio de Impuestos Internos</b>, de lo contrario, se entenderá como <b>aceptada comercialmente</b>'
            );
          } else if (documento.plazo >= 4) {
            return (
              ', al cual le quedan <b>' +
              documento.plazo +
              ' días disponibles<b/> para realizar la aceptación o reclamo en el <b>Servicio de Impuestos Internos</b>'
            );
          }
        } else if (
          documento.plazo == undefined &&
          ['33', '34', '43'].indexOf(documento.tipoDocumento.toString()) >= 0 &&
          documento.formaDePago != 1 &&
          documento.formaDePago != 3
        ) {
          return ', del cual, al no tener la fecha de recepción en el <b>Servicio de Impuestos Internos</b>, no es posible calcular el plazo de vencimiento para su aceptación o reclamo';
        } else {
          return ', el cual, por instrucción del <b>Servicio de Impuestos Internos</b>, y debido a las características mismas del documento, no se encuentra afecto al registro de aceptación o reclamo';
        }
      } else {
        return '';
      }
    };
    $scope.extraSiiEnProsa = function(documento) {
      switch (documento.codigoSii) {
        case 'FAN':
          return '. Posteriormente el <b>folio</b> de éste documento <b>fue anulado por el emisor del documento</b>, por lo que el documento <b>no es legalmente válido</b>';
        case 'TMD':
          return ". Posteriormente el <b>texto</b> de éste documento fue <b>modificado con una nota de débito</b> (puedes consultar las referencias de este documento, con el botón de 'Ver Referencias')";
        case 'TMC':
          return ". Posteriormente el <b>texto</b> de éste documento fue <b>modificado con una nota de crédito</b> (puedes consultar las referencias de este documento, con el botón de 'Ver Referencias')";
        case 'MMD':
          return ". Posteriormente el <b>monto</b> de éste documento fue <b>modificado con una nota de débito</b> (puedes consultar las referencias de este documento, con el botón de 'Ver Referencias')";
        case 'MMC':
          return ". Posteriormente el <b>monto</b> de éste documento fue <b>modificado con una nota de crédito</b> (puedes consultar las referencias de este documento, con el botón de 'Ver Referencias')";
        case 'AND':
          return ". Posteriormente éste documento fue <b>anulado</b> con una nota de débito</b> (puedes consultar las referencias de este documento, con el botón de 'Ver Referencias')";
        case 'ANC':
          return ". Posteriormente éste documento fue <b>anulado</b> con una nota de crédito</b> (puedes consultar las referencias de este documento, con el botón de 'Ver Referencias')";
        default:
          return '';
      }
    };

    $scope.cedidoEnProsa = function(documento) {
      if (
        typeof documento.rutCesionario == 'undefined' ||
        documento.rutCesionario == '' ||
        documento.rutCesionario == 'NULL'
      )
        return '';
      if (documento.rutCesionario.length > 5) {
        return (
          '. Es importante destacar que éste documento <b>fue cedido</b> a la empresa <b>' +
          documento.razonSocialCesionario +
          '</b>, R.U.T. ' +
          documento.rutCesionario +
          ' el día ' +
          $scope.fechaEnProsa(documento.fechaCesion)
        );
      } else {
        return '. Es importante destacar que éste documento <b>fue cedido</b>, sin embargo, no contamos con el detalle de ésta información por ahora';
      }
    };

    $scope.prosa = function(documento) {
      var descripcionDocumento =
        '<b>' +
        $scope.tipoDocumento(documento.tipoDocumento, false) +
        '</b> folio <b>' +
        documento.folio +
        '</b> emitida por <b>' +
        documento.razonSocialEmisor +
        '</b> el ' +
        $scope.fechaEnProsa(documento.fechaEmision) +
        ', por un monto total de <b>' +
        $scope.formatearMonto(documento.montoTotal, '$') +
        '</b>';
      var recepcionadaEnSii =
        typeof documento.fechaRecepcionSii == 'undefined'
          ? ', de la cual aún no tenemos el registro de la fecha en que el <b>Servicio de Impuestos Internos</b> la recibió'
          : ', recibida por el <b>Servicio de Impuestos Internos</b> el ' +
            $scope.fechaEnProsa(documento.fechaRecepcionSii, true);
      var recepcionadaEnFebos =
        typeof documento.fechaRecepcionSii == 'undefined'
          ? ''
          : ' y recibida en <b>Febos</b> el ' + $scope.fechaEnProsa(documento.fechaRecepcion, true);
      var plazoEnProsa = $scope.plazoEnProsa(documento);
      var siiEnProsa = $scope.extraSiiEnProsa(documento);
      var cedidoEnProsa = $scope.cedidoEnProsa(documento);
      return (
        descripcionDocumento +
        recepcionadaEnSii +
        recepcionadaEnFebos +
        plazoEnProsa +
        siiEnProsa +
        cedidoEnProsa +
        '.'
      );
    };

    $scope.leida = function(leido) {
      var leida = leido == true || leido == 'Y' || leido == 'si' ? 1 : 2;
      switch (leida) {
        case 1:
          return { color: 'green', icono: 'description', descripcion: 'Ya fue leída' };
        case 2:
          return { color: 'gray', icono: 'mail', descripcion: 'No ha sido leída aún' };
      }
    };
    $scope.estadoComercial = function(estado) {
      switch (estado) {
        case 1:
          return {
            color: 'teal',
            icono: 'thumb_up',
            descripcion: 'Inteción de aceptación comercial'
          };
        case 2:
          return {
            color: 'light-green',
            icono: 'done',
            descripcion: 'Aceptado comercialmente en el SII'
          };
        case 3:
          return {
            color: 'orange',
            icono: 'thumb_down',
            descripcion: 'Intención de accionAutomatica comercial'
          };
        case 4:
          return {
            color: 'red',
            icono: 'pan_tool',
            descripcion: 'Rechazado comercialmente en el SII'
          };
        case 5:
          return {
            color: 'red',
            icono: 'pan_tool',
            descripcion: 'Reclamado parcialmente en el SII'
          };
        case 6:
          return { color: 'red', icono: 'pan_tool', descripcion: 'Reclamado totalmente en el SII' };
        case 7:
          return {
            color: 'green',
            icono: 'done_all',
            descripcion: 'Con aceptación de mercaderías y/o servicios en el SII'
          };
        default:
          return { color: 'gray', icono: 'visibility', descripcion: 'Sin acción comercial aún' };
      }
    };
    $scope.tipoDocumento = function(a, b, c) {
      return FebosUtil.tipoDocumento(a, b, c);
    };
    $scope.formatearMonto = function(numero, simbolo) {
      if (typeof numero != 'undefined') {
        var partes = numero.toString().split('.');
        partes[0] = partes[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
        return simbolo + ' ' + partes.join(',');
      } else {
        return '';
      }
    };

    var $documentos = $('#documentos');

    if (typeof SesionFebos().documentosNoTributariosSeleccionados == 'undefined') {
      SesionFebos().documentosNoTributariosSeleccionados = [];
    }
    $scope.documentosSeleccionados = SesionFebos().documentosNoTributariosSeleccionados;
    $scope.estaSeleccionado = function(id) {
      if ($.inArray(id, $scope.documentosSeleccionados) >= 0) {
        return true;
      } else {
        return false;
      }
    };

    $scope.listarSeleccionados = function() {
      var params = $location.search();
      params.filtros = 'febosId:' + $scope.documentosSeleccionados.join(',');
      params.pagina = 1;
      $location.search(params);
      $state.go('restringido.dnt', {
        app: $stateParams.app,
        categoria: $stateParams.categoria,
        vista: $stateParams.vista,
        filtros: params.filtros,
        orden: $stateParams.orden,
        pagina: params.pagina,
        itemsPorPagina: params.itemsPorPagina
      });
      $state.reload();
    };
    $scope.deseleccionarTodo = function() {
      var cantidad = $scope.documentosSeleccionados.length;
      $timeout(function() {
        $documentos.find('.select_message').each(function() {
          if ($(this).is(':checked')) {
            $(this).iCheck('uncheck');
          }
        });
        while (typeof $scope.documentosSeleccionados.pop() != 'undefined');
      }, 100);

      var mensaje =
        cantidad == 1
          ? 'Se deseleccionó 1 documento'
          : 'Se deseleccionaron ' + cantidad + ' documentos';
      UIkit.notify(mensaje + "<a class='notify-action'>[X]</a> ", {
        status: 'success',
        timeout: 3000
      });

      // UIkit.notify(mensaje + "<a class='notify-action'>[X]</a> ", {status: "success", timeout: 3000});
    };
    $scope.escanearDocumentosSeleccionados = function() {
      //revisar si habian documentos guardados en la sesion
      var seleccionados = [];
      var deseleccionados = [];
      $documentos.find('.select_message').each(function(index, elemento) {
        var febosId = $(elemento)
          .attr('id')
          .split('_')[1];
        if ($(elemento).is(':checked')) {
          seleccionados.push(febosId);
        } else {
          deseleccionados.push(febosId);
        }
      });
      //insertar seleccionados
      $.each(seleccionados, function(i, el) {
        if ($.inArray(el, $scope.documentosSeleccionados) === -1) {
          $scope.documentosSeleccionados.push(el);
        }
      });
      var eliminar = [];
      $.each($scope.documentosSeleccionados, function(i, el) {
        if ($.inArray(el, deseleccionados) >= 0) {
          eliminar.push(i);
        }
      });
      //eliminar indices deseleccionados
      for (var i = eliminar.length - 1; i >= 0; i--) {
        $scope.documentosSeleccionados.splice(eliminar[i], 1);
      }
      setTimeout(function() {
        $scope.$apply();
      }, 200);
    };
    // seleccionar un documento
    $documentos.on('ifChanged', '.select_message', function() {
      $(this).is(':checked')
        ? $(this)
            .closest('li')
            .addClass('md-card-list-item-selected')
        : $(this)
            .closest('li')
            .removeClass('md-card-list-item-selected');
      $scope.escanearDocumentosSeleccionados();
    });

    // seleccionar todos los documentos
    $('#documentos_seleccionar_todos').on('ifChanged', function() {
      var $this = $(this);
      $documentos.find('.select_message').each(function() {
        $this.is(':checked') ? $(this).iCheck('check') : $(this).iCheck('uncheck');
      });
      $scope.escanearDocumentosSeleccionados();
    });

    // ver el detalle de un documento
    $scope.hayUnDetalleDesplegado = false;
    $documentos.on('click', '.md-card-list ul > li.seleccionable', function(e) {
      if (
        !$(e.target).closest('.md-card-list-item-menu').length &&
        !$(e.target).closest('.md-card-list-item-select').length
      ) {
        var $this = $(this);
        if (!$this.hasClass('item-shown')) {
          // obtener la altura del detalle del documento
          var el_min_height =
            $this.height() + $this.children('.md-card-list-item-content-wrapper').actual('height');
          // esconder el contenido del mensaje
          $documentos.find('.item-shown').velocity('reverse', {
            begin: function(elements) {
              $(elements)
                .removeClass('item-shown')
                .children('.md-card-list-item-content-wrapper')
                .hide()
                .velocity('reverse');
            }
          });
          // mostrar el mensaje
          $this.velocity(
            {
              marginTop: 40,
              marginBottom: 40,
              marginLeft: 0,
              marginRight: 0,
              minHeight: el_min_height
            },
            {
              duration: 300,
              easing: variables.easing_swiftOut,
              begin: function(elements) {
                $(elements).addClass('item-shown');
              },
              complete: function(elements) {
                // show: message content, reply form
                $(elements)
                  .children('.md-card-list-item-content-wrapper')
                  .show()
                  .velocity({
                    opacity: 1
                  });
                // scroll to message
                var container = $('body'),
                  scrollTo = $(elements);
                container.animate(
                  {
                    scrollTop: scrollTo.offset().top - $('#page_content').offset().top - 8
                  },
                  1000,
                  variables.bez_easing_swiftOut
                );
                $scope.hayUnDetalleDesplegado = true;
              }
            }
          );
        }
      }
    });
    // esconder el detalle del documento al: hacer click afuera, apretar escape
    $(document).on('click keydown', function(e) {
      if (!$(e.target).closest('li.item-shown').length || e.which == 27) {
        $documentos.find('.item-shown').velocity('reverse', {
          begin: function(elements) {
            $(elements)
              .removeClass('item-shown')
              .children('.md-card-list-item-content-wrapper')
              .hide()
              .velocity('reverse');
          }
        });
      }
    });

    $scope.revisarCaracteresRaros = function() {
      var hayCaracteresRaros = 0;
      for (var i = 0; i < $scope.documentos.length; i++) {
        var json = JSON.stringify($scope.documentos[i]);
        if (json.includes('??')) {
          hayCaracteresRaros++;
          if (hayCaracteresRaros > 3) {
            break;
          }
        }
      }
      if (hayCaracteresRaros > 0) {
        var mensaje = "<i class='material-icons'>&#xE002;</i>";
        var opciones = {
          status: 'warning',
          timeout: 10000
        };
        switch (hayCaracteresRaros) {
          case 1:
            mensaje +=
              ' Detectamos que un documento de éste listado contiene caracteres no válidos, por lo que algunas palabras podrían no verse escritas correctamente. Esto se debe a que el documento viene así desde su origen.';
            break;
          case 2:
            mensaje +=
              ' Detectamos que dos documentos de éste listado contienen caracteres no válidos, por lo que algunas palabras podrían no verse escritas correctamente. Esto se debe a que los documentos vienen así desde su origen.';
            break;
          default:
            mensaje +=
              ' Detectamos que varios documentos de éste listado contienen caracteres no válidos, por lo que algunas palabras podrían no verse escritas correctamente. Esto se debe a que los documentos vienen así desde su origen.';
            break;
        }
        UIkit.notify(mensaje + "<a class='notify-action'>[X]</a> ", opciones);
      }
    };

    $scope.avanzarPagina = function() {
      var params = verificacionDeParametros;
      var siguientePagina = parseInt(params.pagina) + 1;
      if (siguientePagina > $scope.paginacion.paginas) {
        UIkit.notify("No hay mas páginas <a class='notify-action'>[X]</a> ", {
          status: 'danger',
          timeout: 2000
        });
      } else {
        $location.search('pagina', siguientePagina);
        $state.go('restringido.dnt', {
          app: $stateParams.app,
          categoria: $stateParams.categoria,
          vista: $stateParams.vista,
          filtros: $stateParams.filtros,
          orden: $stateParams.orden,
          pagina: siguientePagina,
          itemsPorPagina: $stateParams.itemsPorPagina
        });
        $state.reload();
      }
    };
    $scope.retrocederPagina = function() {
      var params = verificacionDeParametros.pagina;
      var siguientePagina = parseInt(params.pagina) - 1;
      if (siguientePagina < 1) {
        UIkit.notify(
          "Ésta es la primera página, no se puede <br/>retroceder más =( <a class='notify-action'>[X]</a> ",
          { status: 'danger', timeout: 2000 }
        );
      } else {
        $location.search('pagina', siguientePagina);
        $state.go('restringido.dnt', {
          app: $stateParams.app,
          categoria: $stateParams.categoria,
          vista: $stateParams.vista,
          filtros: $stateParams.filtros,
          orden: $stateParams.orden,
          pagina: siguientePagina,
          itemsPorPagina: $stateParams.itemsPorPagina
        });
        $state.reload();
      }
    };
    $scope.mostrarInputPagina = false;
    $scope.paginaIngresada = 1;
    $scope.irPagina = function(mostrarInputPagina, valor) {
      $scope.mostrarInputPagina = mostrarInputPagina;
      if (mostrarInputPagina) {
        $scope.paginaIngresada = parseInt($location.search().pagina);
      } else {
        var pagina = parseInt($('#paginaManual')[0].value);
        if (pagina > $scope.paginacion.paginas || pagina <= 0) {
          UIkit.notify(
            'Imposible ir la página ' +
              pagina +
              ", ya que no existe <a class='notify-action'>[X]</a> ",
            { status: 'danger', timeout: 2000 }
          );
        } else {
          $location.search('pagina', pagina);
          $state.go('restringido.dnt', {
            app: $stateParams.app,
            categoria: $stateParams.categoria,
            vista: $stateParams.vista,
            filtros: $stateParams.filtros,
            orden: $stateParams.orden,
            pagina: pagina,
            itemsPorPagina: $stateParams.itemsPorPagina
          });
          $state.reload();
        }
      }
      //var params = $location.search();
      //$location.search("pagina", parseInt(params.pagina) + 1);
    };

    $scope.comprarSiLosFiltrosSonIguales = function(A, B) {
      console.log('comparando', A, B);
      if (A.filtros == B.filtros && A.orden == B.orden) {
        return true;
      }
      return false;
    };
    $scope.detectarTitulo = function() {
      var query = $location.search();
      if (typeof SesionFebos().filtrosDnt != 'undefined') {
        for (var i = 0; i < SesionFebos().filtrosDnt[$stateParams.categoria].length; i++) {
          var parametros = SesionFebos().filtrosDnt[$stateParams.categoria][i];
          if ($scope.comprarSiLosFiltrosSonIguales(query, parametros.params)) {
            return (
              VistasDnt[$stateParams.app][$stateParams.categoria][$stateParams.vista].titulo +
              ' aplicando mi vista: ' +
              SesionFebos().filtrosDnt[$stateParams.categoria][i].nombre
            );
          }
        }
      }
      return VistasDnt[$stateParams.app][$stateParams.categoria][$stateParams.vista].titulo;
    };
    $scope.tituloVista = $scope.detectarTitulo();
    $rootScope.$on('refrescarTituloDnt', function() {
      $scope.tituloVista = $scope.detectarTitulo();
    });

    //trigger automaticos al iniciar pagina
    $scope.revisarCaracteresRaros();

    //Descarga PDF
    $scope.cargando = false;
    $scope.cargando.e = false;

    $scope.referencias = {};
    var modalReferencias = UIkit.modal('#modalReferencias');

    $scope.verReferencias = function(documento) {
      $scope.documentoActual = documento;

      if (documento.tipo == '801') {
        $scope.tipoDocumentoSeleccionado = 'Orden de compra';
      } else {
        $scope.tipoDocumentoSeleccionado = documento.tipo;
      }
      $scope.referencias.loadingReferencias = true;
      FebosAPI.cl_listar_referencias_dnt(
        {
          febosId: documento.febosId
        },
        {},
        false,
        true
      ).then(function(response) {
        //$scope.block.hide();
        console.log(response);

        $scope.referencias.referenciasDte = response.data.referenciasTipoDte;
        $scope.referencias.referenciasDnt = response.data.referenciasTipoDnt;
        $scope.referencias.referenciadosDte = response.data.referenciadosTipoDte;
        console.log($scope.referencias.referenciadosDte);
        $scope.referencias.loadingReferencias = false;
      });
      modalReferencias.show();
    };

    $scope.verPDFFactura = function(id, cedible) {
      console.log(id);
      console.log('ver PDF');

      $scope.url_pdf = [];
      $scope.cargando.e = true;
      $scope.textoVerPdf = 'Cargando ...';
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Generando representación impresa...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      FebosAPI.cl_obtener_documento(
        {
          cedible: 'NO',
          febosId: id,
          imagen: 'si',
          regenerar: 'no',
          incrustar: 'no',
          tipoImagen: 0
        },
        {},
        true,
        true
      ).then(function(response) {
        //$scope.block.hide();
        console.log(response);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        $scope.cargando.e = false;

        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            window.open(response.data.imagenLink);
            $scope.textoVerPdf = 'PDF';
          } else {
            $scope.textoVerPdf = 'PDF';
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        } catch (e) {
          $scope.textoVerPdf = 'PDF';
          SesionFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
        }
      });
    };

    // -- función que obtiene y muestra un PDF, consumiendo la API
    $scope.verPDF = function(id) {
      console.log(id);
      console.log('ver PDF');

      $scope.url_pdf = [];
      $scope.cargando.e = true;
      $scope.textoVerPdf = 'Cargando ...';
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Generando representación impresa...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      FebosAPI.cl_obtener_dnt(
        {
          febosId: id,
          imagen: 'si',
          regenerar: 'no',
          incrustar: 'no',
          tipoImagen: 0
        },
        {},
        true,
        true
      ).then(function(response) {
        //$scope.block.hide();
        console.log(response);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        $scope.cargando.e = false;

        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            window.open(response.data.imagenLink);
            $scope.textoVerPdf = 'PDF';
          } else {
            $scope.textoVerPdf = 'PDF';
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        } catch (e) {
          $scope.textoVerPdf = 'PDF';
          SesionFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
        }
      });
    };

    $scope.modalAccionComercial = UIkit.modal('#accion_comercial');
    $scope.modalInfoPago = UIkit.modal('#info_pago');
    $scope.documentoActual = {};
    //$scope.vincularSii='';
    $scope.motivo = '';
    $scope.recinto = '';
    $scope.masivo = 'no';
    $scope.tipoGestionComercial = 'RCD';
    $scope.popUpAccionComercial = function(documento) {
      $scope.vincularSii = 'no';
      $scope.motivo = '';
      $scope.recinto = '';
      $scope.tipoGestionComercial = 'RCD';
      $scope.documentoActual = documento;
      $scope.masivo = 'no';
      $scope.modalAccionComercial.show();
    };
    $scope.popUpAccionComercialMasivo = function(documentosSeleccionados) {
      $scope.vincularSii = 'no';
      $scope.motivo = '';
      $scope.recinto = '';
      $scope.tipoGestionComercial = 'RCD';
      $scope.documentosSeleccionados = documentosSeleccionados;
      $scope.masivo = 'si';
      $scope.modalAccionComercial.show();
    };
    $scope.popUpInfoPago = function(documento) {
      $scope.documentoActual = documento;
      $scope.modalInfoPago.show();
    };
    $scope.nuevoEstadoDocumento = 0;
    $scope.gestionComercial = function(tipoAccion, vincular) {
      var mensaje = '';
      $scope.modalAccionComercial.hide();
      switch (tipoAccion) {
        case 'ACD':
          $scope.nuevoEstadoDocumento = 1;
          if (vincular == 'si') {
            $scope.nuevoEstadoDocumento = 2;
          }
          mensaje = 'Aceptando documento' + (vincular == 'si' ? ' y notificando al SII' : '');
          break;
        case 'RCD':
          if ($scope.motivo == '') {
            UIkit.notify(
              "Debe especificar un motivo de accionAutomatica <a class='notify-action'>[X]</a> ",
              { status: 'danger', timeout: 2000 }
            );
            return;
          }
          $scope.nuevoEstadoDocumento = 3;
          if (vincular == 'si') {
            $scope.nuevoEstadoDocumento = 4;
          }
          mensaje = 'Rechazando documento' + (vincular == 'si' ? ' y notificando al SII' : '');
          break;
        case 'ERM':
          if ($scope.recinto == '') {
            UIkit.notify(
              "Debe especificar un recinto de recepción <a class='notify-action'>[X]</a> ",
              { status: 'danger', timeout: 2000 }
            );
            return;
          }

          $scope.nuevoEstadoDocumento = 7;
          mensaje =
            'Generando recibo de mercaderías para el documento' +
            (vincular == 'si' ? ' y notificando al SII' : '');
          break;
        case 'RFT':
          if ($scope.motivo == '') {
            UIkit.notify(
              "Debe especificar un motivo de reclamo <a class='notify-action'>[X]</a> ",
              { status: 'danger', timeout: 2000 }
            );
            return;
          }
          $scope.nuevoEstadoDocumento = 3;
          if (vincular == 'si') {
            $scope.nuevoEstadoDocumento = 6;
          }
          mensaje = 'Reclamando documento' + (vincular == 'si' ? ' y notificando al SII' : '');
          break;
        case 'RFP':
          if ($scope.motivo == '') {
            UIkit.notify(
              "Debe especificar un motivo de reclamo <a class='notify-action'>[X]</a> ",
              { status: 'danger', timeout: 2000 }
            );
            return;
          }
          $scope.nuevoEstadoDocumento = 3;
          if (vincular == 'si') {
            $scope.nuevoEstadoDocumento = 5;
          }
          mensaje = 'Reclamando documento' + (vincular == 'si' ? ' y notificando al SII' : '');
          break;
        default:
          console.log('OPERACION', tipoAccion);
          UIkit.notify("Debe seleccionar un tipo de Rechazo <a class='notify-action'>[X]</a> ", {
            status: 'danger',
            timeout: 2000
          });
          return;
      }
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>" +
          mensaje +
          "<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      if ($scope.masivo == 'si') {
        var objetoAuxiliarPayload = [];
        console.log($scope.documentosSeleccionados);

        for (var i = 0; i < $scope.documentosSeleccionados.length; i++) {
          objetoAuxiliarPayload.push({ febosId: $scope.documentosSeleccionados[i] });
        }

        console.log(objetoAuxiliarPayload);
        FebosAPI.cl_realizar_accion_masiva(
          {},
          {
            accion: 'cl_generar_intercambio_electronico',
            nombre: mensaje,
            comunes: {
              tipoAccion: tipoAccion,
              vincualAlSii: vincular,
              recinto: tipoAccion == 'ERM' ? $scope.recinto : '',
              motivo:
                tipoAccion == 'RCD' || tipoAccion == 'RFP' || tipoAccion == 'RFT'
                  ? $scope.motivo
                  : ''
            },
            payload: objetoAuxiliarPayload
          },
          true,
          true
        ).then(function() {
          UIkit.notify(
            "Operación efectuada correctamente! Se te avisará por correo cuando termine la acción.<a class='notify-action'>[X]</a> ",
            { status: 'success', timeout: 2000 }
          );
        });
      } else {
        FebosAPI.cl_generar_intercambio_electronico(
          {
            febosId: $scope.documentoActual.febosId,
            tipoAccion: tipoAccion,
            vincualAlSii: vincular,
            recinto: tipoAccion == 'ERM' ? $scope.recinto : '',
            motivo:
              tipoAccion == 'RCD' || tipoAccion == 'RFP' || tipoAccion == 'RFT' ? $scope.motivo : ''
          },
          {},
          true,
          true
        ).then(function() {
          for (var i = 0; i < $scope.documentos.length; i++) {
            if ($scope.documentos[i].febosId == $scope.documentoActual.febosId) {
              $scope.documentos[i].metadata.estadoComercial = $scope.estadoComercial(
                $scope.nuevoEstadoDocumento
              );
              break;
            }
          }
          UIkit.notify("Operación efectuada correctamente!<a class='notify-action'>[X]</a> ", {
            status: 'success',
            timeout: 2000
          });
        });
      }
    };
    $scope.textoCargar = 'CARGAR DOCUMENTO';
    $scope.mostrarModalCargaDocumento = function() {
      console.log('modal carga documento');
      $scope.textoCargar = 'CARGAR DOCUMENTO';
      UIkit.modal('#mdCargaDocumento').show();
    };
    function uuid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    $scope.cargarDocumento = function() {
      if ($scope.textoCargar != 'CARGAR DOCUMENTO') return;

      $scope.textoCargar = 'Cargando documento...';

      var files = document.getElementById('file').files;
      console.log(files);

      if (files.length > 0) {
        if (files[0].type === 'text/xml') {
          var reader = new FileReader();
          reader.readAsDataURL(files[0]);

          console.log(reader);

          reader.onload = function() {
            var base64 = reader.result;
            var documento = base64.split(',');
            var payload = documento[1];
            //console.log(payload);
            var ambiente = FebosUtil.obtenerAmbiente();
            var sufijo = ambiente.charAt(0).toUpperCase() + ambiente.slice(1);
            var req = {
              method: 'POST',
              url:
                'https://api.febos.cl/' +
                ambiente +
                '/empresas/' +
                SesionFebos().empresa.iut +
                '/flujos/RecepcionDte' +
                sufijo +
                '/ejecuciones',
              headers: {
                token: SesionFebos().usuario.token,
                empresa: SesionFebos().empresa.iut,
                'Content-Type': 'application/json'
              },
              data: {
                nombreArchivo: 'Archivo-' + SesionFebos().empresa.iut + '-' + uuid() + '.xml',
                payload: payload
              }
            };
            console.log('RESQUEST LOAD DTE', req);
            $http(req).then(
              function(response) {
                console.log('RESPONSE LOAD DTE', response);
                try {
                  //if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                  $scope.textoCargar = 'CARGAR DOCUMENTO';
                  $scope.mensaje_ok = response.data.mensaje;
                  $scope.mensaje_api = response.data.mensajeResultado;
                  $scope.seguimiento = response.data.seguimientoId;
                  UIkit.modal.alert(
                    response.data.mensaje + '<br/>' + response.data.mensajeResultado
                  );
                } catch (e) {
                  console.log(e);
                  $rootScope.$broadcast('febosError', response);
                }
              },
              function(response) {
                console.log(response);
                febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');
              }
            );
          };

          reader.onerror = function(error) {
            console.log('Error: ', error);
            febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');
          };
        } else {
          console.log('No soporta el tipo de archivo ' + files[0].type);
          //febosSingleton.error('1', 'No soporta el tipo de archivo' + files[0].type, '');
          UIkit.modal.alert('No soporta el tipo de archivo' + files[0].type);
          $scope.textoCargar = 'CARGAR DOCUMENTO';
          return false;
        }
      } else {
        //febosSingleton.error('1', 'El documento es obligatorio y en formato XML', '');
        UIkit.modal.alert('El documento es obligatorio y en formato XML');
        $scope.textoCargar = 'CARGAR DOCUMENTO';
        return false;
      }
    };

    $scope.descargarCSV = function() {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Verificando filtros aplicados...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      var filtros = $location.search().filtros;
      var rangos = Object.keys(ComplementoDnt.atajosDeRangosDeFecha);
      for (var i = 0; i < rangos.length; i++) {
        var rango = rangos[i];
        if (filtros.includes(rango)) {
          filtros = filtros.replace(
            rango,
            ComplementoDnt.atajosDeRangosDeFecha[rango].desde +
              '--' +
              ComplementoDnt.atajosDeRangosDeFecha[rango].hasta
          );
          i--;
        }
      }

      var payload = {
        pagina: 1,
        itemsPorPagina: 999999999,
        campos:
          'tipoDocumento,folio,rutEmisor,razonSocialEmisor,rutReceptor,razonSocialReceptor,rutCesionario,razonSocialCesionario,fechaCesion,codigoSii,fechaEmision,fechaRecepcion,fechaRecepcionSii,plazo,estadoComercial,estadoSii,fechaReciboMercaderia,formaDePago,montoTotal,contacto,correoReceptor,codigoSii,fechaCesion,tipo,monto,lugar,comentario,fecha,medio',
        filtros: filtros,
        orden: $location.search().orden
      };
      console.log('TAREA A ENCOLAR', payload);
      var payload64 = btoa(unescape(encodeURIComponent(JSON.stringify(payload))));

      FebosAPI.cl_encolar_tarea_background(
        {
          proceso: 'cl_listar_dnt'
        },
        {
          payload: payload64
        },
        false,
        false
      ).then(function(response) {
        $rootScope.blockModal.hide();
        $rootScope.blockModal = UIkit.modal.blockUI(
          "<div class='uk-text-center'>Descargando listado de documentos,<br/>esto podria tardar varios segundos...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
        );
        $timeout(function() {
          $scope.verificarCsv(response.data.tareaId);
        }, 1000);
      });
    };
    $scope.verificarCsv = function(tareaId) {
      FebosAPI.cl_consulta_estado_tarea_background(
        {
          tareaId: tareaId
        },
        {},
        false,
        false
      ).then(function(response) {
        switch (response.data.estado) {
          case 2:
            $timeout(function() {
              $scope.verificarCsv(tareaId);
            }, 3000);
            break;
          case 3:
            $http.get(response.data.resultado).then(function(data) {
              $scope.armarCSV(data);
            });
            break;
          case 4:
            var r = {
              codigo: '',
              mensaje: 'Ocurrio un error al intentar exportar el listado de documentos',
              seguimientoId: response.data.seguimientoId
            };
            $rootScope.$broadcast('febosError', r);
        }
      });
    };
    $scope.armarCSV = function(csv) {
      var docs = csv.data.documentos;
      var separador = ';';
      var especiales = 'tipoDocumento,codigoSii,estadoComercial,estadoSii,formaDePago,tipo,medio'.split(
        ','
      );
      var titulos = $scope.titulosCsv();
      var textoCSV = [];
      //poniendo titulos
      for (var i = 0; i < titulos.length; i++) {
        textoCSV.push('"' + titulos[i].titulo + '"');
      }
      textoCSV = textoCSV.join(separador) + '\n';
      for (var i = 0; i < docs.length; i++) {
        var linea = [];
        for (var t = 0; t < titulos.length; t++) {
          if (typeof docs[i][titulos[t].campo] != 'undefined') {
            if (especiales.indexOf(titulos[t].campo) >= 0) {
              //es un campo especial
              switch (titulos[t].campo) {
                case 'tipoDocumento':
                  linea.push('"' + FebosUtil.tipoDocumento(docs[i].tipoDocumento) + '"');
                  break;
                case 'estadoSii':
                  linea.push('"' + $scope.estadoSii(docs[i].estadoSii).descripcion + '"');
                  break;
                case 'estadoComercial':
                  linea.push(
                    '"' + $scope.estadoComercial(docs[i].estadoComercial).descripcion + '"'
                  );
                  break;
                case 'formaPago':
                  switch (parseInt(docs[i].formaDePago)) {
                    case 1:
                      linea.push('"Contado"');
                      break;
                    case 2:
                      linea.push('"Crédito"');
                      break;
                    case 3:
                      linea.push('"Sin costo"');
                      break;
                    default:
                      linea.push('"No especificado"');
                      break;
                  }
                  break;
                case 'tipo':
                  switch (parseInt(docs[i].tipo)) {
                    case 0:
                      linea.push('"Sin Acción"');
                      break;
                    case 1:
                      linea.push('"En proceso"');
                      break;
                    case 2:
                      linea.push('"Pago enviado"');
                      break;
                    case 3:
                      linea.push('"Pagado"');
                      break;
                    case 4:
                      linea.push('"Cobrado"');
                      break;
                    case 5:
                      linea.push('"Pago no efectuado"');
                      break;
                    default:
                      linea.push('"No especificado"');
                      break;
                  }
                  break;
                case 'medio':
                  switch (parseInt(docs[i].medio)) {
                    case 'CH':
                      linea.push('"Cheque"');
                      break;
                    case 'CF':
                      linea.push('"Cheque a fecha"');
                      break;
                    case 'LT':
                      linea.push('"Letra"');
                      break;
                    case 'EF':
                      linea.push('"Efectivo"');
                      break;
                    case 'PE':
                      linea.push('"Pago a Cta. Cte."');
                      break;
                    case 'TC':
                      linea.push('"Tarjeta de crédito"');
                      break;
                    case 'VV':
                      linea.push('"Vale vista"');
                      break;
                    case 'CO':
                      linea.push('"Confirming"');
                      break;
                    case 'OT':
                      linea.push('"Otro"');
                      break;
                    default:
                      linea.push('"No especificado"');
                      break;
                  }
                  break;
                case 'codigoSii':
                  var desc = null;
                  switch (docs[i].codigoSii) {
                    case 'DOK':
                      desc = '"Documento coincide con los registros del SII"';
                      break;
                    case 'AND':
                      desc = '"Folio anulado"';
                      break;
                    case 'TMD':
                      desc = '"Texto modificado con ND"';
                      break;
                    case 'TMC':
                      desc = '"Texto modificado con NC"';
                      break;
                    case 'MMD':
                      desc = '"Monto modificado con ND"';
                      break;
                    case 'MMC':
                      desc = '"Monto modificado con NC"';
                      break;
                    case 'AND':
                      desc = '"DTE Anulado con ND"';
                      break;
                    case 'ANC':
                      desc = '"DTE Anulado con NC"';
                      break;
                  }
                  linea.push(desc);
                  break;
                default:
                  linea.push('"' + docs[i][titulos[t].campo] + '"');
              }

              if (typeof docs[i][titulos[t + 1].campo] == 'string') {
                linea.push('"' + docs[i][titulos[t + 1].campo] + '"');
              } else {
                linea.push(docs[i][titulos[t + 1].campo]);
              }
              t++;
            } else {
              //es un campo normal
              if (typeof docs[i][titulos[t].campo] == 'string') {
                linea.push('"' + docs[i][titulos[t].campo] + '"');
              } else {
                linea.push(docs[i][titulos[t].campo]);
              }
            }
          } else {
            linea.push(null);
          }
        }
        textoCSV += linea.join(separador) + '\n';
      }
      $rootScope.blockModal.hide();
      UIkit.notify("Listado de documentos descargado! <a class='notify-action'>[X]</a> ", {
        status: 'success',
        timeout: 2000
      });
      console.log(textoCSV);
      var link = document.createElement('a');
      link.href = 'data:text/csv;charset=utf-8;base64,' + btoa(textoCSV);
      link.download = 'ExportadoFebos.csv';
      document.body.appendChild(link);
      try {
        setTimeout(function() {
          link.click();
        }, 150);
      } catch (e) {}
    };
    $scope.titulosCsv = function() {
      var campos = 'febosId,tipoDocumento,tipoDocumento,folio,rutEmisor,razonSocialEmisor,rutReceptor,razonSocialReceptor,rutCesionario,razonSocialCesionario,fechaCesion,codigoSii,codigoSii,fechaEmision,fechaRecepcion,fechaRecepcionSii,plazo,estadoComercial,estadoComercial,estadoSii,estadoSii,fechaReciboMercaderia,formaDePago,formaDePago,montoTotal,contacto,correoReceptor,fechaCesion,tipo,tipo,monto,lugar,comentario,fecha,medio,medio'.split(
        ','
      );
      var titulos = 'Febos ID,Tipo Documento,Tipo Documento Código,Folio,Rut Emisor,Razón Social Emisor,Rut Receptor,Razón Social Receptor,Rut Cesionario,Razón Social Cesionario,Fecha Cesión,Código SII Prosa,Código SII,Fecha de Emisión,Fecha de Recepción Febos,Fecha de Recepcion SII,Plazo para gestión comercial,Estado Comercial,Estado Comercial código,Estado SII,Estado SII código,Fecha de Recibo de Mercaderías,Forma de Pago,Forma de Pago código,Monto Total,Contacto,Correo Receptor,Fecha de Cesión,Estado de pago proveedor,Estado de pago código,Monto pago proveedor,Lugar de pago proveedor,Comentario de pago proveedor,Fecha de pago proveedor,Medio de pago proveedor,Medio de pago proveedor código'.split(
        ','
      );

      var resultado = [];
      for (var i = 0; i < titulos.length; i++) {
        resultado.push({
          campo: campos[i],
          titulo: titulos[i]
        });
      }
      return resultado;
    };

    //agregar meta data a cada documento sobre sus estados (colores, descripciones e iconos)
    $scope.documentos = Datos.documentos;
    for (var i = 0; i < $scope.documentos.length; i++) {
      $scope.documentos[i].metadata = {
        leido: $scope.leida($scope.documentos[i].leido)
        //,estadoComercial: $scope.estadoComercial($scope.documentos[i].estadoComercial)
      };
    }

    $scope.modalConsultarHistorialSii = function(documento) {
      var modal = UIkit.modal('#consultar_historial_sii');
      $scope.documentoActual = documento;
      modal.show();
    };

    $scope.consultarHistorialSii = function() {
      UIkit.modal('#consultar_historial_sii').hide();
      //var resp={"codigoSii":15,"descripcionSii":"Listado de eventos del documento","eventos":[{"codEvento":"CED","descEvento":"DTE Cedido","rutResponsable":"14172951","dvResponsable":"9","fechaEvento":"25-01-2018 11:00:09"},{"codEvento":"RCD","descEvento":"Reclama Contenido del Documento","rutResponsable":"61704000","dvResponsable":"K","fechaEvento":"30-01-2018 18:37:11"},{"codEvento":"NCA","descEvento":"Recepción de NC de Anulación que Referencia Documento","rutResponsable":"14172951","dvResponsable":"9","fechaEvento":"07-02-2018 14:55:39"}],"codigo":10,"mensaje":"Acción realizada satisfactoriamente","seguimientoId":"e38478df8d8e10465759cc8-e4dc0ae1a230","duracion":10653,"hora":"2018-05-20 16:47:44.125"};
      console.log($scope.documentoActual);
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Consultando documento el SII..." +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_sii_consulta_historial_eventos(
        { febosId: $scope.documentoActual.febosId },
        {},
        true,
        true
      ).then(function(response) {
        $scope.documentoActual.eventos_sii = response.data.eventos;
        UIkit.modal('#historial_sii').show();
      });
    };

    //Consultar fecha de recepcion en el SII, se actualiza en DB y en el arreglo de documentos que estan siendo mostrados
    $scope.modalConsultarFecha = function(documento) {
      var modal = UIkit.modal('#consultar_fecha_sii');
      $scope.documentoActual = documento;
      modal.show();
    };

    $scope.consultarFechaRecepcionSii = function(documento) {
      UIkit.modal('#consultar_fecha_sii').hide();
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Consultando fecha de recepción en el SII..." +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      FebosAPI.cl_consulta_fecha_recepcion_sii(
        {
          febosId: documento.febosId,
          rutEmisor: documento.rutEmisor,
          tipoDte: documento.tipoDocumento,
          folio: documento.folio
        },
        {},
        true,
        true
      ).then(function(response) {
        console.log(response);

        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}

        $scope.cargando = false;

        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            $scope.fechaRecepcionSii = response.data.fechaRecepcion;
            //Actualizo la fecha en la variable de ese mismo documento, para evitar que la vista quede desactualizada y evitar que se deba refrescar
            for (var i = 0; i < $scope.documentos.length; i++) {
              if ($scope.documentos[i].febosId == $scope.documentoActual.febosId) {
                $scope.documentos[i].fechaRecepcionSii = response.data.fechaRecepcion;
                break;
              }
            }
            //Recarga vista ya que la consulta de la fecha de recepcion actualiza en la base de datos
            $scope.textoRespuesta =
              'La ' +
              $scope.tipoDocumento(documento.tipoDocumento) +
              ', folio #' +
              documento.folio +
              ' fue recibida por el SII en ';
            var modal = UIkit.modal('#mostrar_fecha_recepcion');
            modal.show();
          } else {
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        } catch (e) {
          console.log(e);
          SesionFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
        }
      });
    };

    $scope.formularioEdicionPago = false;
    $scope.selectize_config_pagos = {
      maxItems: 1,
      valueField: 'valor',
      labelField: 'nombre',
      searchField: 'nombre',
      create: false,
      dropdownParent: 'body',
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.nombre) + '</div>';
        }
        //,
        //option_create: function (data, escape) {
        //    return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
        // }
      }
    };
    $scope.estadoPagoActual = {
      tipo: 0,
      monto: 0,
      lugar: '',
      comentario: '',
      medio: '',
      fecha: ''
    };
    $scope.estadoPagoTransicion = {
      tipo: 0,
      monto: 0,
      lugar: '',
      comentario: '',
      medio: '',
      fecha: ''
    };
    $scope.estadosDePago = [
      { valor: '0', nombre: 'Sin gestión' },
      { valor: '1', nombre: 'En vía de pago' },
      { valor: '2', nombre: 'Pago enviado' },
      { valor: '3', nombre: 'Pagado' },
      { valor: '4', nombre: 'Cobrado' },
      { valor: '5', nombre: 'No pagado' }
    ];
    $scope.mediosDePago = [
      { valor: '', nombre: 'Sin gestión' },
      { valor: 'CH', nombre: 'Cheque' },
      { valor: 'CF', nombre: 'Cheque a fecha' },
      { valor: 'LT', nombre: 'Letra' },
      { valor: 'EF', nombre: 'Efectivo' },
      { valor: 'PE', nombre: 'Pago a Cta. Cte.' },
      { valor: 'TC', nombre: 'Tarjeta de crédito' },
      { valor: 'VV', nombre: 'Vale vista' },
      { valor: 'CO', nombre: 'Confirming' },
      { valor: 'OT', nombre: 'Otro' }
    ];
    var modalInfoPago = UIkit.modal('#info_pago');
    $scope.modalInfoPago = function(documento) {
      $scope.formularioEdicionPago = false;
      $scope.estadoPagoActual = {
        tipo: documento.tipo,
        monto: documento.monto,
        lugar: documento.lugar,
        comentario: documento.comentario,
        medio: documento.medio,
        fecha: documento.fecha
      };

      $scope.documentoActual = documento;
      modalInfoPago.show();
    };
    $scope.editarInfoPago = function() {
      $scope.estadoPagoTransicion = {
        tipo: $scope.estadoPagoActual.tipo,
        monto: $scope.estadoPagoActual.monto,
        lugar: $scope.estadoPagoActual.lugar,
        comentario: $scope.estadoPagoActual.comentario,
        medio: $scope.estadoPagoActual.medio,
        fecha: $scope.estadoPagoActual.fecha
      };
      $scope.formularioEdicionPago = true;
    };
    $scope.actualizarInfoPago = function() {
      modalInfoPago.hide();
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Actualizando estado de pago" +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_crea_info_pago(
        { febosId: $scope.documentoActual.febosId },
        $scope.estadoPagoTransicion,
        true,
        true
      ).then(function(response) {
        for (var i = 0; i < $scope.documentos.length; i++) {
          if ($scope.documentos[i].febosId == $scope.documentoActual.febosId) {
            $scope.documentos[i].tipo = $scope.estadoPagoTransicion.tipo;
            $scope.documentos[i].monto = $scope.estadoPagoTransicion.monto;
            $scope.documentos[i].lugar = $scope.estadoPagoTransicion.lugar;
            $scope.documentos[i].comentario = $scope.estadoPagoTransicion.comentario;
            $scope.documentos[i].medio = $scope.estadoPagoTransicion.medio;
            $scope.documentos[i].fecha = $scope.estadoPagoTransicion.fecha;
            break;
          }
          UIkit.notify("Estado de pago actualizado <a class='notify-action'>[X]</a> ", {
            status: 'success',
            timeout: 2000
          });
        }
      });
    };

    $scope.modalEnviarDte = UIkit.modal('#mdlEnviarDte');

    $scope.popUpEnviarDte = function(documento) {
      $scope.documentoActual = documento;
      $scope.destinatario = '';
      $scope.copias = '';
      $scope.recibirCopia = 'no';
      $scope.mensaje = '';
      $scope.adjuntarXML = 'si';
      $scope.adjuntarPDF = 'si';
      $scope.cargando = 'no';
      $scope.modalEnviarDte.show();
    };

    $scope.enviarDte = function() {
      $scope.cargando = 'si';
      FebosAPI.cl_correo_envio_dte(
        {
          febosId: $scope.documentoActual.febosId
        },
        {
          destinatario: $scope.destinatario,
          copias: $scope.copias,
          recibirCopia: $scope.recibirCopia,
          mensaje: btoa($scope.mensaje),
          adjuntarXml: $scope.adjuntarXML,
          adjuntarPdf: $scope.adjuntarPDF
        },
        true,
        false
      ).then(function(response) {
        console.log(response);
        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}

        //$rootScope.blockModal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Enviando bits a destino...<br/><img width="30"class=\'uk-margin-top\' src=\'assets/img/logos/FEBOS_LOADER.svg\' alt=\'\'>')

        $scope.cargando = 'no';
        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            UIkit.modal('#mdlEnviarDte').hide();
            UIkit.notify("Documento enviado!<a class='notify-action'>[X]</a> ", {
              status: 'success',
              timeout: 3000
            });
          } else {
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        } catch (e) {
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId
          );
        }
      });
    };

    var $crear_card = $('#crear_card'),
      $crear_form = $('#crear_form'),
      $crear_error = $('#crear_error'),
      $crear_ok = $('#crear_ok');
    var $crear_cardOC = $('#crear_cardOC'),
      $crear_formOC = $('#crear_formOC'),
      $crear_errorOC = $('#crear_errorOC'),
      $crear_okOC = $('#crear_okOC');
    var $crear_cardHES = $('#crear_cardHES'),
      $crear_formHES = $('#crear_formHES'),
      $crear_errorHES = $('#crear_errorHES'),
      $crear_okHES = $('#crear_okHES');
    var $crear_cardRM = $('#crear_cardRM'),
      $crear_formRM = $('#crear_formRM'),
      $crear_errorRM = $('#crear_errorRM'),
      $crear_okRM = $('#crear_okRM');

    $scope.modalCargaHes = function(id) {
      console.log('modal carga HES');
      $scope.cargando.l = true;
      $scope.textoCargarHES = 'CARGAR DOCUMENTO';
      $scope.febosId = id;

      $scope.cargando.l = false;
      UIkit.modal('#mdFrmCargaHes').show();
    };

    $scope.backToCrear = function($event) {
      $event.preventDefault();
      utils.card_show_hide($crear_form, undefined, crear_form_show, undefined);
      utils.card_show_hide($crear_formOC, undefined, crear_form_show, undefined);
      utils.card_show_hide($crear_formHES, undefined, crear_form_show, undefined);
      utils.card_show_hide($crear_formRM, undefined, crear_form_show, undefined);

      $scope.textoDetalle = 'DETALLES';
      $scope.textoVerPdf = 'PDF';
      $scope.textoVerXml = 'XML';
      $scope.textoOrCom = 'OC';
      $scope.textoBitacora = 'BITACORA';
    };

    $scope.cargarHes = function() {
      if ($scope.textoCargarHES != 'CARGAR DOCUMENTO') return;

      $scope.textoCargarHES = 'Cargando documento...';

      var filesHES = document.getElementById('fileHES').files;

      if (filesHES.length > 0) {
        if (filesHES[0].type === 'text/xml' || filesHES[0].type === 'application/pdf') {
          var reader = new FileReader();
          reader.readAsDataURL(filesHES[0]);

          reader.onload = function() {
            var data = reader.result;
            var payload = data.replace(/^[^,]*,/, '');

            FebosAPI.cl_adjuntar_documento(
              {
                token: SesionFebos().usuario.token,
                empresa: SesionFebos().empresa.iut,
                grupo: SesionFebos().grupo.id,
                debug: '',
                simular: ''
              },
              {
                tipo: '2',
                nombreArchivo: filesHES[0].name,
                tipoOrigen: '2',
                payload: payload,
                folioRef: document.getElementById('folioRef').value,
                fechaRef: document.getElementById('fechaRef').value,
                razonRef: document.getElementById('razonRef').value
              },
              true,
              false
            ).then(function(response) {
              console.log(response);
              try {
                if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                  $scope.cargando = false;
                  $scope.textoCargarHES = 'CARGAR DOCUMENTO';
                  $scope.mensaje_okHES = response.data.mensaje;

                  utils.card_show_hide($crear_formHES, undefined, crear_ok_show, undefined);
                } else {
                  SesionFebos().error(
                    response.data.codigo,
                    response.data.mensaje,
                    response.data.seguimientoId
                  );
                }
              } catch (e) {
                console.log(e);
                SesionFebos().error(
                  response.data.codigo,
                  response.data.mensaje,
                  response.data.seguimientoId
                );
              }
            });
          };
          reader.onerror = function(error) {
            console.log('Error: ', error);
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          };
        } else {
          console.log('No soporta el tipo de archivo ' + filesHES[0].type);
          //febosSingleton.error('1', 'No soporta el tipo de archivo' + filesHES[0].type, '');
          alert('No soporta el tipo de archivo' + filesHES[0].type);
          $scope.textoCargarHES = 'CARGAR DOCUMENTO';
          return false;
        }
      } else {
        //febosSingleton.error('1', 'El documento es obligatorio y en formato XML o PDF', '');
        alert('El documento es obligatorio y en formato XML o PDF');
        $scope.textoCargarHES = 'CARGAR DOCUMENTO';
        return false;
      }
    };

    $scope.modalCargaRecepcionMateriales = function(id) {
      console.log('modal carga recepción de materiales');
      $scope.cargando.m = true;
      $scope.textoCargarRM = 'CARGAR DOCUMENTO';
      febosId = id;

      $scope.cargando.m = false;
      UIkit.modal('#mdFrmCargaRecepcionMateriales').show();
    };

    $scope.cargarRecepcionMateriales = function() {
      if ($scope.textoCargarRM != 'CARGAR DOCUMENTO') return;

      $scope.textoCargarRM = 'Cargando documento...';

      var filesRM = document.getElementById('fileRM').files;

      if (filesRM.length > 0) {
        if (filesRM[0].type === 'text/xml' || filesRM[0].type === 'application/pdf') {
          var reader = new FileReader();
          reader.readAsDataURL(filesRM[0]);

          reader.onload = function() {
            var data = reader.result;
            var payload = data.replace(/^[^,]*,/, '');

            $scope.cargando = true;

            FebosAPI.cl_adjuntar_documento(
              {
                token: SesionFebos().usuario.token,
                empresa: SesionFebos().empresa.iut,
                grupo: SesionFebos().grupo.id,
                debug: '',
                simular: ''
              },
              {
                tipo: '3',
                nombreArchivo: filesRM[0].name,
                tipoOrigen: '2',
                payload: payload,
                folioRef: document.getElementById('folioRef').value,
                fechaRef: document.getElementById('fechaRef').value,
                razonRef: document.getElementById('razonRef').value
              },
              true,
              false
            ).then(function(response) {
              console.log(response);
              try {
                if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                  $scope.cargando = false;
                  $scope.textoCargarRM = 'CARGAR DOCUMENTO';
                  $scope.mensaje_okRM = response.data.mensaje;

                  utils.card_show_hide($crear_formRM, undefined, crear_ok_show, undefined);
                } else {
                  SesionFebos().error(
                    response.data.codigo,
                    response.data.mensaje,
                    response.data.seguimientoId
                  );
                }
              } catch (e) {
                console.log(e);
                SesionFebos().error(
                  response.data.codigo,
                  response.data.mensaje,
                  response.data.seguimientoId
                );
              }
            });
          };

          reader.onerror = function(error) {
            console.log('Error: ', error);
            febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');
          };
        } else {
          console.log('No soporta el tipo de archivo ' + filesRM[0].type);
          //febosSingleton.error('1', 'No soporta el tipo de archivo' + filesRM[0].type, '');
          alert('No soporta el tipo de archivo' + filesRM[0].type);
          $scope.textoCargarRM = 'CARGAR DOCUMENTO';
          return false;
        }
      } else {
        //febosSingleton.error('1', 'El documento es obligatorio y en formato XML o PDF', '');
        alert('El documento es obligatorio y en formato XML o PDF');
        $scope.textoCargarRM = 'CARGAR DOCUMENTO';
        return false;
      }
    };
  }
]);
