febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {
                $stateProvider.state("restringido.bitacora_dnt", {
                    url: "/:app/documentos/no_tributarios/bitacora/:febosid",
                    templateUrl: 'app/febos/documentos/no_tributarios/acciones/bitacora/bitacoraDntView.html',
                    controller: 'bitacoraDntCtrl',
                    reloadOnSearch: true,
                    data: {
                        pageTitle: 'Bitácora'
                    },
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                console.log("a");
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                            console.log("a");
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                            }],
                        bitacora:['FebosAPI','$stateParams',function(FebosAPI,$stateParams){
                            console.log("b");
                                return FebosAPI.cl_listar_bitacora_dnt({
                                    febosId:$stateParams.febosid,
                                    pagina:1,
                                    filas:200
                                },{},true,false).then(function (response) {
                                    return response.data;
                                });
                        }],
                        dte:['FebosAPI','$stateParams',function(FebosAPI,$stateParams){
                            console.log("c");
                                return FebosAPI.cl_obtener_dnt({
                                    febosId:$stateParams.febosid,
                                    incrustar:'si',
                                    imagen:'si',
                                    tipoImagen:'1'
                                },{},true,false).then(function (response) {
                                    return response.data;
                                });
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            console.log("d");
                                return $ocLazyLoad.load([
                                    'bower_components/jquery-ui/jquery-ui.min.js',
                                    'lazy_uiSelect',
                                    'app/febos/documentos/no_tributarios/acciones/bitacora/bitacoraDntController.js',
                                ]);
                            }]
                    }
                })

            }]);
