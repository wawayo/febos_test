angular.module('febosApp').controller('bitacoraDntCtrl', [
  '$rootScope',
  '$scope',
  '$timeout',
  'variables',
  '$location',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  '$state',
  '$stateParams',
  'bitacora',
  'dte',
  '$sce',
  '$window',
  function(
    $rootScope,
    $scope,
    $timeout,
    variables,
    $location,
    SesionFebos,
    FebosAPI,
    FebosUtil,
    $state,
    $stateParams,
    bitacora,
    dte,
    $sce,
    $window
  ) {
    $rootScope.page_full_height = true;
    $rootScope.headerDoubleHeightActive = true;

    $scope.$on('$destroy', function() {
      $rootScope.page_full_height = false;
      $rootScope.headerDoubleHeightActive = false;
    });
    //console.log("bitacora", bitacora);
    $scope.dte = dte;
    $scope.tipo = '';
    switch (dte.tipo) {
      case '801':
        $scope.tipo = 'Orden de Compra';
        break;
      case 'MP':
        $scope.tipo = 'Orden de Compra';
        break;
      case 'HES':
        $scope.tipo = 'Hoja de entrega de Servicios';
        break;
      case 'HEM':
        $scope.tipo = 'Hoja de entrega de Materiales';
        break;
    }

    $scope.html = FebosUtil.decode(dte.imagenData);
    //$scope.iframe=$sce.trustAsResourceUrl('data:application/pdf;base64,' +dte.imagenData);
    $scope.iframe = $sce.trustAsResourceUrl('data:text/html;base64,' + dte.imagenData);
    //$scope.invoices_data = invoices_data;
    $scope.bitacoras = bitacora.bitacora;
    $scope.getMonth = function(bitacora, $index) {
      var prev_date = $scope.bitacoras[$index - 1],
        this_date = bitacora.fecha;
      if (prev_date) {
        return (
          moment(this_date, 'MM-DD-YYYY').format('MM') !==
          moment(prev_date.date, 'MM-DD-YYYY').format('MM')
        );
      } else if ($index == 0) {
        return true;
      }
    };
    $scope.query = '';
    var w = angular.element($window);
    $scope.getWindowDimensions = function() {
      return {
        h: w.height(),
        w: w.width()
      };
    };
    $scope.$watch(
      $scope.getWindowDimensions,
      function(newValue, oldValue) {
        $scope.windowHeight = newValue.h;
        $scope.windowWidth = newValue.w;
      },
      true
    );
    $scope.buscar = function(item) {
      if (
        item.fecha.includes($scope.query.toLowerCase()) ||
        item.mensaje.toLowerCase().includes($scope.query.toLowerCase()) ||
        item.usuarioNombre.toLowerCase().includes($scope.query.toLowerCase()) ||
        item.usuarioCorreo.toLowerCase().includes($scope.query.toLowerCase())
      ) {
        return true;
      }
      return false;
    };
    //var iframe=$("#documento").contentWindow.document;

    // invoice details
    //Descargar adjunto bitacora

    $scope.descargarAdjunto = function(bitacoraId) {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Descargando archivo adjunto..." +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      //cl_obtener_adjunto_bitacora
      console.log(bitacoraId);
      FebosAPI.cl_obtener_adjunto_bitacora(
        {
          link: 'si',
          idBitacora: bitacoraId
        },
        {},
        true,
        true
      ).then(function(response) {
        console.log(response);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;

        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            window.open(response.data.archivo);
          } else {
            SesiobFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        } catch (e) {
          console.log(e);
          SesiobFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
        }
      });
    };

    $scope.cargando = false;
    // -- función que obtiene y muestra un PDF, consumiendo la API
    $scope.verPDF = function(febosId) {
      console.log(febosId);

      $scope.url_pdf = [];
      $scope.cargando = true;
      $scope.textoVerPdf = 'Cargando ...';
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Generando representación impresa...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      FebosAPI.cl_obtener_dnt(
        {
          febosId: febosId,
          imagen: 'si',
          regenerar: 'no',
          incrustar: 'no',
          tipoImagen: 0
        },
        {},
        true,
        true
      ).then(function(response) {
        //$scope.block.hide();
        console.log(response);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        $scope.cargando = false;

        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            window.open(response.data.imagenLink);
            $scope.textoVerPdf = 'PDF';
          } else {
            $scope.textoVerPdf = 'PDF';
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        } catch (e) {
          $scope.textoVerPdf = 'PDF';
          SesionFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
        }
      });
    };
  }
]);
