febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.emisionWeb', {
      url: '/:app/documentos/emitir/:tipo',
      templateUrl: 'app/febos/documentos/emisionWeb/emitirView.html',
      controller: 'emitirCtrl',
      //reloadOnSearch: false,
      params: {
        tipo: ''
      },
      data: {
        pageTitle: 'Emisión de Documentos Electrónicos'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        DatosEmisor: [
          'FebosAPI',
          '$location',
          '$rootScope',
          'SesionFebos',
          function(FebosAPI, $location, $rootScope, SesionFebos) {
            return FebosAPI.cl_info_empresa(
              {
                empresaId: SesionFebos().empresa.iut,
                debug: '',
                simular: '',
                token: SesionFebos().token,
                empresa: SesionFebos().empresa.iut,
                grupo: SesionFebos().grupo.id
              },
              {},
              true,
              false
            ).then(function(response) {
              return response.data;
            });
          }
        ],

        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_selectizeJS',
              'lazy_uiSelect',
              'lazy_KendoUI',
              'lazy_uikit',
              'lazy_masked_inputs',
              'app/febos/documentos/emisionWeb/emitirController.js'
            ]);
          }
        ]
      }
    });
  }
]);
