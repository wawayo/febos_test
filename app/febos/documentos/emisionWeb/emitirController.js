angular.module('febosApp').controller('emitirCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$location',
  '$http',
  '$state',
  'SesionFebos',
  'FebosAPI',
  '$stateParams',
  'FebosUtil',
  'VistasEmisionWeb',
  'DatosEmisor',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    $http,
    $state,
    SesionFebos,
    FebosAPI,
    $stateParams,
    FebosUtil,
    VistasEmisionWeb,
    DatosEmisor
  ) {
    $scope.global = false;
    $scope.otros = false;
    $scope.datos = {};
    $scope.datos.acteco = DatosEmisor.acteco;
    for (var i = 0; i < DatosEmisor.sucursales.length; i++) {
      if (DatosEmisor.sucursales[i].casaMatriz) {
        $scope.datos.empresa_direccion = DatosEmisor.sucursales[i].direccion;
        $scope.datos.empresa_telefono = DatosEmisor.sucursales[i].telefono;
        $scope.datos.empresa_correo = DatosEmisor.sucursales[i].correoElectronico;
        $scope.datos.empresa_ciudad = DatosEmisor.sucursales[i].ciudad;
        $scope.datos.empresa_comuna = DatosEmisor.sucursales[i].comuna;
        break;
      }
    }
    $scope.paises = paises();
    $scope.regiones = regiones();
    $scope.provincias = [];
    $scope.seleccionarCiudad = function(region) {
      $scope.provincias = seleccionarCiudad(region);
    };

    $scope.comunas = [];
    $scope.seleccionarComuna = function(provincia) {
      $scope.comunas = seleccionarComuna(provincia);
    };

    $scope.tiposDocumento = tipoDocumento();
    $scope.tiposMoneda = tipoMoneda();

    $scope.tiposImpuesto = tipoImpuesto();

    $scope.unidadMedida = unidadMedida();

    $scope.tiposBulto = tipoBulto();

    $scope.tiposClausula = tipoClausula();
    $scope.seleccionarPuertoEmision = function(pais) {
      $scope.puertosE = puerto(pais);
    };

    $scope.seleccionarPuertoReceptor = function(pais) {
      $scope.puertosR = puerto(pais);
    };
    $scope.obtenerPuertoSegunCodigo = function(codigo, tipo) {
      if (tipo == 'E') {
        for (var i = 0; i < $scope.puertosE.length; i++) {
          if ($scope.puertosE[i].codigo == codigo) {
            $scope.dte.transporte.ptoEmbarque = $scope.puertosE[i].descripcion;
            break;
          }
        }
      }
      if (tipo == 'D') {
        for (var i = 0; i < $scope.puertosR.length; i++) {
          if ($scope.puertosR[i].codigo == codigo) {
            $scope.dte.transporte.ptoDestino = $scope.puertosR[i].descripcion;
            break;
          }
        }
      }
      return '';
    };

    $scope.obtenerPaisSegunCodigo = function(codigo) {
      for (var i = 0; i < $scope.paises.length; i++) {
        if ($scope.paises[i].codigo == codigo) {
          return $scope.paises[i].descripcion;
        }
      }
      return 'Extranjero';
    };
    $scope.obtenerRegionSegunCodigo = function(codigo) {
      for (var i = 0; i < $scope.regiones.length; i++) {
        if ($scope.regiones[i].codigo == codigo) {
          return $scope.regiones[i].descripcion;
        }
      }
      return 'Región Metropolitana de Santiago';
    };
    $scope.obtenerRegionSegunNombre = function(nombre) {
      for (var i = 0; i < $scope.regiones.length; i++) {
        if ($scope.regiones[i].descripcion.toUpperCase() == nombre.toUpperCase()) {
          return $scope.regiones[i].codigo;
        }
      }
      return '';
    };
    $scope.obtenerCiudadSegunCodigo = function(codigo) {
      for (var i = 0; i < $scope.provincias.length; i++) {
        if ($scope.provincias[i].codigo == codigo) {
          return $scope.provincias[i].descripcion;
        }
      }
      return 'Región Metropolitana de Santiago';
    };
    $scope.obtenerCiudadSegunNombre = function(nombre) {
      for (var i = 0; i < $scope.provincias.length; i++) {
        if ($scope.provincias[i].descripcion.toUpperCase() == nombre.toUpperCase()) {
          return $scope.provincias[i].codigo;
        }
      }
      return '';
    };
    $scope.obtenerComunaSegunCodigo = function(codigo) {
      for (var i = 0; i < $scope.comunas.length; i++) {
        if ($scope.comunas[i].codigo == codigo) {
          return $scope.comunas[i].descripcion;
        }
      }
      return 'Santiago';
    };
    $scope.obtenerComunaSegunNombre = function(nombre) {
      for (var i = 0; i < $scope.comunas.length; i++) {
        if ($scope.comunas[i].descripcion.toUpperCase() == nombre.toUpperCase()) {
          return $scope.comunas[i].codigo;
        }
      }
      return '';
    };
    $scope.obtenerImpuestoSegunCodigo = function(codigo) {
      for (var i = 0; i < $scope.tiposImpuesto.length; i++) {
        if ($scope.tiposImpuesto[i].codigo == codigo) {
          return $scope.tiposImpuesto[i].descripcion;
        }
      }
      return 'Sin Adicional';
    };
    $scope.obtenerPorcImpuestoSegunCodigo = function(codigo) {
      for (var i = 0; i < $scope.tiposImpuesto.length; i++) {
        if ($scope.tiposImpuesto[i].codigo == codigo) {
          return $scope.tiposImpuesto[i].porcentaje;
        }
      }
      return 'Sin Adicional';
    };

    $scope.setFolio = function(indice, global) {
      if (global) {
        $scope.dte.referencias[indice].folio = 0;
      } else {
        $scope.dte.referencias[indice].folio = '';
      }
    };
    $scope.setOtros = function(otros, indice) {
      if (otros == '0' || otros == '-1') {
        $scope.dte.referencias[indice].otros = true;
      } else {
        $scope.dte.referencias[indice].otros = false;
        $scope.dte.referencias[indice].documento = '';
      }
    };

    var hoy = new Date();
    $scope.tipoCodigo = '';
    $scope.tipoProsa = '';

    $scope.indicadoresEconomicos = function() {
      $http({ method: 'GET', url: 'https://mindicador.cl/api' }).then(
        function(data) {
          console.log(data);
          $scope.indicadores = data.data;
        },
        function(data) {
          console.log(data);
        }
      );
    };

    $scope.indicadoresEconomicos();

    switch ($stateParams.tipo) {
      case 'venta':
        $scope.tipoProsa = 'Factura Electronica';
        $scope.tipoCodigo = '33';
        break;
      case 'ventaExenta':
        $scope.tipoProsa = 'Factura Exenta Electrónica';
        $scope.tipoCodigo = '34';
        break;
      case 'notaCredito':
        $scope.tipoProsa = 'Nota de Débito';
        $scope.tipoCodigo = '56';
        break;
      case 'notaDebito':
        $scope.tipoProsa = 'Nota de Crédito';
        $scope.tipoCodigo = '61';
        break;
      case 'guiaDespacho':
        $scope.tipoProsa = 'Guia de Despacho';
        $scope.tipoCodigo = '52';
        break;
      case 'boleta':
        $scope.tipoProsa = 'Boleta Electronica';
        $scope.tipoCodigo = '39';
        break;
      case 'boletaExenta':
        $scope.tipoProsa = 'Boleta Exenta Electrónica';
        $scope.tipoCodigo = '41';
        break;
      case 'exportacion':
        $scope.tipoProsa = 'Factura de Exportacion';
        $scope.tipoCodigo = '110';
        break;
      case 'notaDebitoExportacion':
        $scope.tipoProsa = 'Nota de Débito de Exportacion';
        $scope.tipoCodigo = '111';
        break;
      case 'notaCreditoExportacion':
        $scope.tipoProsa = 'Nota de Crédito de Exportacion';
        $scope.tipoCodigo = '112';
        break;
      case 'compra':
        $scope.tipoProsa = 'Factura de Compra Electrónica';
        $scope.tipoCodigo = '46';
        break;
      default:
        break;
    }
    $scope.dte = {};
    $scope.limpiarCampos = function() {
      var tpoEmpresa = SesionFebos().empresa.tipoEmpresa;
      if (tpoEmpresa == undefined || tpoEmpresa == null) {
        tpoEmpresa = 1;
      }
      $scope.dte = {
        tipo: $scope.tipoCodigo,
        folio: '',
        fechaEmision:
          hoy.getDate() +
          ' / ' +
          (hoy.getMonth() + 1 < 10 ? '0' + (hoy.getMonth() + 1) : hoy.getMonth() + 1) +
          ' / ' +
          hoy.getFullYear(),
        horaEmision: '',
        moneda: 'CLP',
        factorConversion: 1,
        formaPago: '',
        tipoDespacho: '',
        fechaVencimiento:
          hoy.getDate() +
          ' / ' +
          (hoy.getMonth() + 1 < 10 ? '0' + (hoy.getMonth() + 1) : hoy.getMonth() + 1) +
          ' / ' +
          hoy.getFullYear(),
        indTraslado: '',
        tipoServicio: '',
        usuario: SesionFebos().usuario.nombre,
        notas: '',
        sucursal: '',
        codSucursal: '',
        emisor: {
          tipoEmpresa: '1',
          iutEmisor: SesionFebos().empresa.iut,
          iutMandante: '',
          giro: '',
          razonSocial: SesionFebos().empresa.razonSocial,
          nombre: '',
          apellidoPaterno: '',
          apellidoMaterno: '',
          codigoPais: 'CL',
          pais: 'Chile',
          region: '',
          provincia: $scope.datos.empresa_ciudad,
          comuna: $scope.datos.empresa_comuna,
          direccion: $scope.datos.empresa_direccion,
          correo: $scope.datos.empresa_correo,
          acteco: $scope.datos.acteco
        },
        receptor: {
          tipoEmpresa: '1',
          iutReceptor: '',
          iutSolicitante: '',
          giro: '',
          razonSocial: '',
          nombre: '',
          apellidoPaterno: '',
          apellidoMaterno: '',
          codigoPais: 'CL',
          pais: 'Chile',
          region: '',
          provincia: '',
          comuna: '',
          direccion: '',
          correo: ''
        },
        transporte: {
          rutChofer: '',
          rutTrasnporte: '',
          tipoServicio: '',
          modalidadVenta: '',
          tipoClausula: '',
          totalClausulaV: '',
          nombreTransportista: '',
          rutTransportista: '',
          ciaTransportista: '',
          nomChofer: '',
          paisEmisor: '',
          codPtoEmbarque: '',
          ptoEmbarque: '',
          paisDestino: '',
          codPtoDestino: '',
          ptoDestino: '',
          dirDestino: '',
          ciuDestino: '',
          locDestino: '',
          patente: '',
          booking: '',
          operador: '',
          montoFlete: '',
          montoSeguro: '',
          container: '',
          sello: '',
          emiSello: '',
          tara: '',
          umTara: '',
          totalPB: '',
          umPB: '',
          totalPN: '',
          umPN: '',
          totalItems: '',
          totalBulto: '',
          tipoBulto: '',
          cantBulto: ''
        },
        detalles: [],
        referencias: [],
        descRecargos: []
      };
    };
    $scope.limpiarCampos();

    $scope.agregarDetalle = function() {
      if ($scope.dte.detalles.length < 61) {
        $scope.dte.detalles.push({
          moneda: $scope.dte.moneda,
          tipoCodigo: 'INT',
          codigo: '',
          indEx: false,
          nombre: '',
          cantidad: 0,
          uniMedida: '',
          precio: 0,
          total: 0,
          hayAdicional: false,
          adicional: '',
          descuentoPorc: 0,
          descuentoVal: 0,
          recargoPorc: 0,
          recargoVal: 0,
          descripcion: ''
        });
      } else {
        UIkit.notify(
          "¡Ups! No puedes añadir mas de 60 artículos. <a class='notify-action'>[X]</a> ",
          {
            status: 'success',
            timeout: 3000
          }
        );
      }
    };
    $scope.agregarReferencia = function() {
      if ($scope.dte.referencias.length < 41) {
        $scope.dte.referencias.push({
          tipoDocumento: '',
          documento: '',
          global: false,
          folio: '',
          fecha: '',
          tipoMotivo: '',
          motivo: '',
          otros: false
        });
      } else {
        UIkit.notify(
          "¡Ups! No puedes añadir mas de 40 referencias. <a class='notify-action'>[X]</a> ",
          {
            status: 'success',
            timeout: 3000
          }
        );
      }
    };
    $scope.agregarDescuentoRecargo = function() {
      if ($scope.dte.descRecargos.length < 21) {
        $scope.dte.descRecargos.push({
          tipo: '',
          exento: '',
          tipoValor: '1',
          valor: 0,
          valorOtraMoneda: '',
          descripcion: ''
        });
      } else {
        UIkit.notify(
          "¡Ups! No puedes añadir mas de 20 líneas de Descuento o Recargo. <a class='notify-action'>[X]</a> ",
          {
            status: 'success',
            timeout: 3000
          }
        );
      }
    };

    $scope.eliminarDetalle = function(indice) {
      if (
        indice == 0 &&
        ($scope.dte.tipo == '56' ||
          $scope.dte.tipo == '61' ||
          $scope.dte.tipo == '111' ||
          $scope.dte.tipo == '112')
      ) {
        UIkit.notify("¡Ups! Debes dejar al menos un detalle. <a class='notify-action'>[X]</a> ", {
          status: 'success',
          timeout: 3000
        });
      } else {
        $scope.dte.detalles.splice(indice, 1);
        $scope.calcularTotalera();
      }
    };
    $scope.eliminarReferencia = function(indice) {
      if (
        indice == 0 &&
        ($scope.dte.tipo == '56' ||
          $scope.dte.tipo == '61' ||
          $scope.dte.tipo == '111' ||
          $scope.dte.tipo == '112')
      ) {
        UIkit.notify(
          "¡Ups! Debes dejar una referencia en la Nota <a class='notify-action'>[X]</a> ",
          {
            status: 'success',
            timeout: 3000
          }
        );
      } else {
        $scope.dte.referencias.splice(indice, 1);
      }
    };
    $scope.eliminarDescuentoRecargo = function(indice) {
      $scope.dte.descRecargos.splice(indice, 1);
    };
    $scope.adicionales = function(indice) {
      if (!$scope.dte.detalles[indice].hayAdicional) {
        $scope.dte.detalles[indice].adicional = '';
        $scope.calcularTotalera();
      }
    };

    $scope.totalImpuestos = {};
    $scope.totalera = {
      subtotal: 0,
      neto: 0,
      exento: 0,
      descuento: 0,
      recargo: 0,
      iva: 0,
      adicionales: [],
      totalAdicionales: 0,
      total: 0
    };
    $scope.formatearMonto = function(numero, simbolo) {
      try {
        var partes = numero.toString().split('.');
        partes[0] = partes[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
        return simbolo + ' ' + partes.join(',');
      } catch (e) {
        return 0;
      }
    };

    var $maskedInput = $('.masked_input');
    if ($maskedInput.length) {
      $maskedInput.inputmask();
    }

    $scope.redondear = function(num) {
      return Math.round(num);
    };

    $scope.calcular = function(linea) {
      $scope.iva = 19;

      linea.cantidad = (linea.cantidad + '').replace(/^0+/, '');
      if (linea.precio !== 0) {
        linea.precio = (linea.precio + '').replace(/[^\d.]/g, '');
      } else {
        linea.precio = 0;
      }

      $scope.subtotal = linea.precio * linea.cantidad;

      if (linea.descuentoPorc != 0) {
        linea.descuentoPorc = (linea.descuentoPorc + '').replace(/^0+/, '');
        linea.descuentoVal = $scope.redondear(($scope.subtotal * linea.descuentoPorc) / 100);
      } else {
        linea.descuentoVal = 0;
      }
      if (linea.recargoPorc != 0) {
        linea.recargoPorc = (linea.recargoPorc + '').replace(/^0+/, '');
        linea.recargoVal = $scope.redondear(($scope.subtotal * linea.recargoPorc) / 100);
      } else {
        linea.recargoVal = 0;
      }

      linea.total = $scope.redondear(
        linea.precio * linea.cantidad - linea.descuentoVal + linea.recargoVal
      );

      $scope.calcularTotalera();
    };

    $scope.calcularTotalera = function() {
      $scope.totalera.subtotal = 0;
      $scope.totalera.neto = 0;
      $scope.totalera.exento = 0;
      $scope.totalera.descuento = 0;
      $scope.totalera.recargo = 0;
      $scope.totalera.iva = 0;
      $scope.totalera.adicionales = [];
      $scope.totalera.totalAdicionales = 0;
      $scope.totalera.total = 0;
      $scope.impPorc = 0;

      for (var i = 0; i < $scope.dte.detalles.length; i++) {
        if (
          $scope.dte.tipo == '34' ||
          $scope.dte.tipo == '41' ||
          $scope.dte.tipo == '110' ||
          $scope.dte.tipo == '111' ||
          $scope.dte.tipo == '112'
        ) {
          $scope.totalera.exento += $scope.dte.detalles[i].total;
        } else {
          if (!$scope.dte.detalles[i].indEx) {
            $scope.totalera.neto += $scope.dte.detalles[i].total;
            $scope.totalera.iva = $scope.redondear(($scope.totalera.neto * $scope.iva) / 100);
          } else {
            $scope.totalera.exento += $scope.dte.detalles[i].total;
          }
        }

        if ($scope.dte.detalles[i].hayAdicional) {
          $scope.impPorc = $scope.obtenerPorcImpuestoSegunCodigo($scope.dte.detalles[i].adicional);

          if ($scope.impPorc > 0) {
            $scope.a = false;
            if ($scope.totalera.adicionales.length > 0) {
              for (var j = 0; j < $scope.totalera.adicionales.length; j++) {
                if ($scope.totalera.adicionales[j].codigo == $scope.dte.detalles[i].adicional) {
                  $scope.totalera.adicionales[j].total +=
                    ($scope.dte.detalles[i].cantidad *
                      $scope.dte.detalles[i].precio *
                      $scope.impPorc) /
                    100;
                  $scope.a = true;
                }
              }
            }
            if (!$scope.a) {
              $scope.totalera.adicionales.push({
                codigo: $scope.dte.detalles[i].adicional,
                total:
                  ($scope.dte.detalles[i].cantidad *
                    $scope.dte.detalles[i].precio *
                    $scope.impPorc) /
                  100
              });
            }
          }

          $scope.diesel = 0;
          if ($scope.dte.detalles[i].adicional == '11') {
            switch ($scope.dte.detalles[i].uniMedida.toUpperCase()) {
              case 'M3':
                $scope.diesel = $scope.dte.detalles[i].cantidad;
                break;
              case 'L':
                $scope.diesel = $scope.dte.detalles[i].cantidad / 1000;
                break;
              case 'LT':
                $scope.diesel = $scope.dte.detalles[i].cantidad / 1000;
                break;
              case 'LTS':
                $scope.diesel = $scope.dte.detalles[i].cantidad / 1000;
                break;
            }
            if ($scope.diesel > 0) {
              $scope.a = false;
              if ($scope.totalera.adicionales.length > 0) {
                for (var j = 0; j < $scope.totalera.adicionales.length; j++) {
                  if ($scope.totalera.adicionales[j].codigo == $scope.dte.detalles[i].adicional) {
                    $scope.totalera.adicionales[j].total +=
                      $scope.indicadores.utm.valor * 1.5 * $scope.diesel;
                    $scope.a = true;
                  }
                }
              }
              if (!$scope.a) {
                $scope.totalera.adicionales.push({
                  codigo: $scope.dte.detalles[i].adicional,
                  total: $scope.indicadores.utm.valor * 1.5 * $scope.diesel
                });
              }
            }
          }

          $scope.gasolina = 0;
          if ($scope.dte.detalles[i].adicional == '12') {
            switch ($scope.dte.detalles[i].uniMedida.toUpperCase()) {
              case 'M3':
                $scope.gasolina = $scope.dte.detalles[i].cantidad;
                break;
              case 'L':
                $scope.gasolina = $scope.dte.detalles[i].cantidad / 1000;
                break;
              case 'LT':
                $scope.gasolina = $scope.dte.detalles[i].cantidad / 1000;
                break;
              case 'LTS':
                $scope.gasolina = $scope.dte.detalles[i].cantidad / 1000;
                break;
            }
            if ($scope.gasolina > 0) {
              $scope.a = false;
              if ($scope.totalera.adicionales.length > 0) {
                for (var j = 0; j < $scope.totalera.adicionales.length; j++) {
                  if ($scope.totalera.adicionales[j].codigo == $scope.dte.detalles[i].adicional) {
                    $scope.totalera.adicionales[j].total +=
                      $scope.indicadores.utm.valor * 6 * $scope.gasolina;
                    $scope.a = true;
                  }
                }
              }
              if (!$scope.a) {
                $scope.totalera.adicionales.push({
                  codigo: $scope.dte.detalles[i].adicional,
                  total: $scope.indicadores.utm.valor * 6 * $scope.gasolina
                });
              }
            }
          }
        }

        $scope.totalera.subtotal += $scope.dte.detalles[i].total;
      }
      for (var i = 0; i < $scope.totalera.adicionales.length; i++) {
        $scope.totalera.totalAdicionales += $scope.totalera.adicionales[i].total;
      }

      for (var i = 0; i < $scope.dte.descRecargos.length; i++) {
        console.log($scope.dte.descRecargos[i]);
        $scope.val = ($scope.dte.descRecargos[i].valor + '').replace(/^0+/, '');
        if ($scope.dte.tipo == '110' || $scope.dte.tipo == '111' || $scope.dte.tipo == '112') {
          $scope.dte.descRecargos[i].valorOtraMoneda = $scope.redondear(
            $scope.val * $scope.totalera.factorConversion
          );
        }
        if ($scope.dte.descRecargos[i].tipo == 'D') {
          //descuento
          switch ($scope.dte.descRecargos[i].tipoValor) {
            case '%':
              console.log($scope.totalera.subtotal * $scope.val);
              $scope.totalera.descuento += $scope.redondear(
                ($scope.totalera.subtotal * $scope.val) / 100
              );
              break;
            case '$':
              console.log($scope.val);
              $scope.totalera.descuento += $scope.val * 1;
              break;
          }
          if ($scope.dte.descRecargos[i].exento == '') {
            $scope.totalera.neto -= $scope.totalera.descuento;
            $scope.totalera.iva -= $scope.redondear(($scope.totalera.descuento * 19) / 100);
          } else {
            $scope.totalera.exento -= $scope.totalera.descuento;
          }
        } else {
          //recargo
          switch ($scope.dte.descRecargos[i].tipoValor) {
            case '%':
              console.log($scope.totalera.subtotal * $scope.val);
              $scope.totalera.recargo += $scope.redondear(
                ($scope.totalera.subtotal * $scope.val) / 100
              );
              break;
            case '$':
              console.log($scope.val);
              $scope.totalera.recargo += $scope.val * 1;
              break;
          }
          if ($scope.dte.descRecargos[i].exento == '') {
            $scope.totalera.neto += $scope.totalera.recargo;
            $scope.totalera.iva += $scope.redondear(($scope.totalera.recargo * 19) / 100);
          } else {
            $scope.totalera.exento += $scope.totalera.recargo;
          }
        }
      }

      $scope.totalera.total = $scope.totalera.subtotal; //sumo el bruto del item
      $scope.totalera.total += $scope.totalera.iva;
      $scope.totalera.total -= $scope.totalera.descuento;
      $scope.totalera.total += $scope.totalera.recargo;
      $scope.totalera.total += $scope.totalera.totalAdicionales;
      $scope.totalera.total = $scope.redondear($scope.totalera.total);
      $scope.montoEnPalabras = NumeroALetras($scope.totalera.total, $scope.dte.moneda);
    };

    $scope.actualizarHora = $interval(function() {
      var ahora = new Date();
      var horas = ahora.getHours();
      var minutos = ahora.getMinutes();
      var segundos = ahora.getSeconds();
      var tp = horas >= 12 ? 'pm' : 'am';
      if (horas >= 13) horas -= 12;
      $scope.dte.horaEmision =
        (horas < 10 ? '0' + horas : horas) +
        ':' +
        (minutos < 10 ? '0' + minutos : minutos) +
        ':' +
        (segundos < 10 ? '0' + segundos : segundos) +
        ' ' +
        tp;
    }, 1000);
    $scope.detenerHora = function() {
      $interval.cancel($scope.actualizarHora);
    };

    $scope.agregarDetalle();
    if (
      $scope.dte.tipo == '56' ||
      $scope.dte.tipo == '61' ||
      $scope.dte.tipo == '112' ||
      $scope.dte.tipo == '111'
    ) {
      $scope.agregarReferencia();
    }

    $scope.construirArchivoIntegracion = function() {
      var archivoIntegracion = '';
      archivoIntegracion += $scope.construirLinea('@TIPO_DOC', 1, [
        $scope.tipoCodigo == '110' || $scope.tipoCodigo == '111' || $scope.tipoCodigo == '112'
          ? 'Exportaciones'
          : 'Documento'
      ]);
      archivoIntegracion += $scope.construirLinea('@CABECERA', 8, [
        $scope.dte.tipo,
        $scope.dte.folio,
        convertirFecha($scope.dte.fechaEmision),
        $scope.dte.tipoDespacho,
        $scope.dte.indTraslado,
        $scope.dte.tipoServicio,
        $scope.dte.formaPago,
        $scope.dte.fechaVencimiento
      ]); //]);

      archivoIntegracion += $scope.construirLinea('@EMISOR', 10, [
        $scope.dte.emisor.iutEmisor,
        $scope.dte.emisor.tipoEmpresa == '1'
          ? $scope.dte.emisor.razonSocial
          : $scope.dte.emisor.nombre +
            ' ' +
            $scope.dte.emisor.apellidoPaterno +
            ' ' +
            $scope.dte.emisor.apellidoMaterno,
        $scope.dte.emisor.giro,
        $scope.dte.emisor.telefono,
        $scope.dte.emisor.correoElectronico,
        $scope.dte.emisor.acteco,
        $scope.dte.sucursal,
        $scope.dte.codSucursal,
        $scope.dte.emisor.direccion,
        isNaN($scope.dte.emisor.comuna)
          ? $scope.dte.emisor.comuna
          : $scope.obtenerComunaSegunCodigo($scope.dte.emisor.comuna),
        isNaN($scope.dte.emisor.provincia)
          ? $scope.dte.emisor.provincia
          : $scope.obtenerCiudadSegunCodigo($scope.dte.emisor.provincia)
      ]);
      if ($scope.tipoCodigo != '110' && $scope.tipoCodigo != '111' && $scope.tipoCodigo != '112') {
        archivoIntegracion += $scope.construirLinea('@EMISOR_MANDANTE', 1, [
          $scope.dte.emisor.iutMandante
        ]);
      }

      archivoIntegracion += $scope.construirLinea('@RECEPTOR', 8, [
        $scope.dte.receptor.iutReceptor,
        $scope.dte.receptor.tipoEmpresa == '1'
          ? $scope.dte.receptor.razonSocial
          : $scope.dte.receptor.nombre +
            ' ' +
            $scope.dte.receptor.apellidoPaterno +
            ' ' +
            $scope.dte.receptor.apellidoMaterno,
        $scope.dte.receptor.giro,
        $scope.dte.receptor.telefono,
        $scope.dte.receptor.correoElectronico,
        $scope.dte.receptor.direccion,
        isNaN($scope.dte.receptor.comuna)
          ? $scope.dte.receptor.comuna
          : $scope.obtenerComunaSegunCodigo($scope.dte.receptor.comuna),
        isNaN($scope.dte.receptor.provincia)
          ? $scope.dte.receptor.provincia
          : $scope.obtenerCiudadSegunCodigo($scope.dte.receptor.provincia)
      ]);
      if ($scope.tipoCodigo != '110' && $scope.tipoCodigo != '111' && $scope.tipoCodigo != '112') {
        archivoIntegracion += $scope.construirLinea('@RECEPTOR_SOLICITANTE', 1, [
          $scope.dte.receptor.iutSolicitante
        ]);
      }
      if ($scope.tipoCodigo == '110' || $scope.tipoCodigo == '111' || $scope.tipoCodigo == '112') {
        archivoIntegracion += $scope.construirLinea('@CONDICIONES_ENTREGA', 34, [
          $scope.dte.transporte.patente,
          $scope.dte.transporte.rutTransportista,
          $scope.dte.transporte.rutChofer,
          $scope.dte.transporte.nomChofer,
          $scope.dte.transporte.dirDestino,
          $scope.dte.transporte.locDestino,
          $scope.dte.transporte.ciuDestino,
          $scope.dte.transporte.modalidadVenta,
          $scope.dte.transporte.tipoClausula,
          $scope.dte.transporte.totalClausulaV,
          $scope.dte.transporte.ciaTransportista,
          $scope.dte.transporte.booking,
          $scope.dte.transporte.operador,
          $scope.dte.transporte.codPtoEmbarque,
          $scope.dte.transporte.ptoEmbarque,
          $scope.dte.transporte.codPtoDestino,
          $scope.dte.transporte.ptoDestino,
          $scope.dte.transporte.tara,
          $scope.dte.transporte.umTara,
          $scope.dte.transporte.totalPB,
          $scope.dte.transporte.umPB,
          $scope.dte.transporte.totalPN,
          $scope.dte.transporte.umPN,
          $scope.dte.transporte.totalItems,
          $scope.dte.transporte.totalBulto,
          $scope.dte.transporte.tipoBulto,
          $scope.dte.transporte.cantBulto,
          $scope.dte.transporte.container,
          $scope.dte.transporte.sello,
          $scope.dte.transporte.emiSello,
          $scope.dte.transporte.montoFlete,
          $scope.dte.transporte.montoSeguro,
          $scope.dte.transporte.paisEmisor,
          $scope.dte.transporte.paisDestino
        ]);
      }

      if ($scope.tipoCodigo == '110' || $scope.tipoCodigo == '111' || $scope.tipoCodigo == '112') {
        archivoIntegracion += $scope.construirLinea('@TOTALES', 6, [
          '',
          $scope.redondear($scope.totalera.exento),
          '',
          '',
          '',
          $scope.redondear($scope.totalera.total),
          $scope.moneda
        ]);
      } else {
        archivoIntegracion += $scope.construirLinea('@TOTALES', 6, [
          $scope.redondear($scope.totalera.neto),
          $scope.redondear($scope.totalera.exento),
          '19',
          $scope.redondear($scope.totalera.iva),
          $scope.redondear($scope.totalera.subtotal),
          $scope.redondear($scope.totalera.total),
          ''
        ]);
      }

      for (var i = 0; i < $scope.totalera.adicionales.length; i++) {
        archivoIntegracion += $scope.construirLinea('@TOTAL_ADICIONALES', 3, [
          $scope.totalera.adicionales[i].codigo,
          $scope.obtenerPorcImpuestoSegunCodigo($scope.totalera.adicionales[i].codigo),
          $scope.totalera.adicionales[i].total
        ]);
      }

      if ($scope.tipoCodigo == '110' || $scope.tipoCodigo == '111' || $scope.tipoCodigo == '112') {
        archivoIntegracion += $scope.construirLinea('@CONDICIONES_CAMBIO', 6, [
          $scope.dte.moneda,
          $scope.dte.factorConversion,
          $scope.redondear($scope.totalera.exento * $scope.dte.factorConversion),
          ,
          $scope.redondear($scope.totalera.total * $scope.dte.factorConversion)
        ]);
      }

      for (var i = 0; i < $scope.dte.detalles.length; i++) {
        if ($scope.dte.detalles[i].precio != null && $scope.dte.detalles[i].precio != 0) {
        } else {
          $scope.dte.detalles[i].precio = 0;
        }
        archivoIntegracion += $scope.construirLinea('@DETALLE', 18, [
          i + 1,
          $scope.dte.detalles[i].tipoCodigo,
          $scope.dte.detalles[i].codigo,
          $scope.dte.detalles[i].indEx ? '1' : '',
          $scope.dte.detalles[i].nombre,
          $scope.dte.detalles[i].descripcion,
          $scope.dte.detalles[i].cantidad,
          $scope.dte.detalles[i].uniMedida,
          $scope.dte.detalles[i].precio,
          $scope.tipoCodigo == '110' || $scope.tipoCodigo == '111' || $scope.tipoCodigo == '112'
            ? $scope.dte.detalles[i].precio * $scope.dte.factorConversion
            : '',
          $scope.tipoCodigo == '110' || $scope.tipoCodigo == '111' || $scope.tipoCodigo == '112'
            ? 'CLP'
            : '',
          $scope.tipoCodigo == '110' || $scope.tipoCodigo == '111' || $scope.tipoCodigo == '112'
            ? $scope.dte.factorConversion
            : '',
          ($scope.tipoCodigo == '110' ||
            $scope.tipoCodigo == '111' ||
            $scope.tipoCodigo == '112') &&
          $scope.dte.detalles[i].descuentoVal > 0
            ? $scope.dte.detalles[i].descuentoVal * $scope.dte.factorConversion
            : '',
          ($scope.tipoCodigo == '110' ||
            $scope.tipoCodigo == '111' ||
            $scope.tipoCodigo == '112') &&
          $scope.dte.detalles[i].recargoVal > 0
            ? $scope.dte.detalles[i].recargoVal * $scope.dte.factorConversion
            : '',
          $scope.tipoCodigo == '110' || $scope.tipoCodigo == '111' || $scope.tipoCodigo == '112'
            ? $scope.dte.detalles[i].total * $scope.dte.factorConversion
            : '',
          $scope.dte.detalles[i].descuentoPorc,
          $scope.dte.detalles[i].descuentoVal,
          $scope.dte.detalles[i].recargoPorc,
          $scope.dte.detalles[i].recargoVal,
          $scope.dte.detalles[i].adicional,
          $scope.dte.detalles[i].total
        ]);
      }

      for (var i = 0; i < $scope.dte.descRecargos.length; i++) {
        archivoIntegracion += $scope.construirLinea('@DESC_RECAR', 7, [
          i + 1,
          $scope.dte.descRecargos[i].tipo,
          $scope.dte.descRecargos[i].descripcion,
          $scope.dte.descRecargos[i].tipoValor,
          $scope.dte.descRecargos[i].valor,
          $scope.dte.descRecargos[i].valorOtraMoneda,
          $scope.dte.descRecargos[i].exento
        ]);
      }

      for (var i = 0; i < $scope.dte.referencias.length; i++) {
        archivoIntegracion += $scope.construirLinea('@REFERENCIA', 2, [
          i + 1,
          $scope.dte.referencias[i].tipoReferencia,
          $scope.dte.referencias[i].global ? '1' : '',
          $scope.dte.referencias[i].folio,
          $scope.dte.referencias[i].documento,
          $scope.dte.referencias[i].fecha,
          $scope.dte.referencias[i].tipoMotivo,
          $scope.dte.referencias[i].motivo
        ]);
      }

      return archivoIntegracion;
    };
    $scope.construirLinea = function(etiqueta, camposTotales, valores) {
      if (valores.length == 0) return '';
      var camposVacios = true;
      for (var i = 0; i < valores.length; i++) {
        if (valores[i] != '') {
          camposVacios = false;
        }
      }
      if (camposVacios) return '';

      var linea = etiqueta;
      for (var i = 0; i < camposTotales; i++) {
        linea += ';';
        try {
          if (typeof valores[i] == 'undefined') continue;
          if (typeof valores[i] == 'string') {
            linea += valores[i].replace('\n', '**');
          } else {
            linea += valores[i];
          }
        } catch (e) {}
      }
      linea += '\n';
      return linea;
    };

    function convertirHora(hora) {
      var partes = hora.split(' ');
      console.log('hora', partes);
      var segmentos = partes[0].split(':');
      if (partes[1] == 'pm') {
        if (segmentos[0] == 12) {
          return (
            dobleDigito(segmentos[0]) +
            ':' +
            dobleDigito(segmentos[1]) +
            ':' +
            dobleDigito(segmentos[2])
          );
        } else {
          return (
            dobleDigito(parseInt(segmentos[0]) + 12) +
            ':' +
            dobleDigito(segmentos[1]) +
            ':' +
            dobleDigito(segmentos[2])
          );
        }
      } else {
        return (
          dobleDigito(segmentos[0]) +
          ':' +
          dobleDigito(segmentos[1]) +
          ':' +
          dobleDigito(segmentos[2])
        );
      }
    }

    function convertirFecha(fecha) {
      var partes = fecha.split(' / ');
      console.log('fecha', partes);
      return dobleDigito(partes[2]) + '-' + dobleDigito(partes[1]) + '-' + dobleDigito(partes[0]);
    }

    function dobleDigito(numero) {
      if (numero < 10) return '0' + parseInt(numero);
      return numero;
    }

    $scope.validarCampos = function() {
      var texto = '';
      var textoFin = '';

      //Emisor
      if ($scope.dte.emisor.acteco == '') {
        texto += 'Acteco Emisor; ';
      }
      if ($scope.dte.emisor.iutEmisor == '') {
        texto += 'Rut Emisor; ';
      }
      if ($scope.dte.emisor.giro == '' && $scope.dte.emisor.tipoEmpresa == '1') {
        texto += 'Giro Emisor; ';
      }
      if ($scope.dte.emisor.razonSocial == '' && $scope.dte.emisor.tipoEmpresa == '1') {
        texto += 'Razón Social Emisor; ';
      }
      if ($scope.dte.emisor.nombre == '' && $scope.dte.emisor.tipoEmpresa == '2') {
        texto += 'Nombre Emisor; ';
      }
      if (
        $scope.dte.emisor.comuna == '' &&
        ($scope.dte.tipo == '33' || $scope.dte.tipo == '34' || $scope.dte.tipo != '52')
      ) {
        texto += 'Comuna; ';
      }
      if (
        $scope.dte.emisor.direccion == '' &&
        ($scope.dte.tipo != '61' &&
          $scope.dte.tipo != '56' &&
          $scope.dte.tipo != '111' &&
          $scope.dte.tipo != '112')
      ) {
        texto += 'Dirección; ';
      }

      if (texto != '') {
        textoFin += '<b>Información de Emisor:</b><br/> &emsp;' + texto + '<br/> ';
        texto = '';
      }

      //Documento
      if ($scope.dte.factorConversion == '' && dte.moneda != 'CLP') {
        texto += 'Factor de Conversión; ';
      }
      if ($scope.dte.sucursal == '') {
        texto += 'Código de Sucursal; ';
      }

      if (texto != '') {
        textoFin += '<b>Información de Documento:</b><br/> &emsp;' + texto + '<br/> ';
        texto = '';
      }

      //Receptor
      if ($scope.dte.receptor.iutReceptor == '') {
        texto += 'Rut Receptor; ';
      }
      if ($scope.dte.receptor.giro == '' && $scope.dte.receptor.tipoEmpresa == '1') {
        texto += 'Giro Receptor; ';
      }
      if ($scope.dte.receptor.razonSocial == '' && $scope.dte.receptor.tipoEmpresa == '1') {
        texto += 'Razón Social Receptor; ';
      }
      if ($scope.dte.receptor.nombre == '' && $scope.dte.receptor.tipoEmpresa == '2') {
        texto += 'Nombre Receptor; ';
      }
      if (
        $scope.dte.receptor.comuna == '' &&
        ($scope.dte.tipo == '33' || $scope.dte.tipo == '34' || $scope.dte.tipo != '52')
      ) {
        texto += 'Comuna; ';
      }
      if (
        $scope.dte.receptor.direccion == '' &&
        ($scope.dte.tipo != '61' &&
          $scope.dte.tipo != '56' &&
          $scope.dte.tipo != '111' &&
          $scope.dte.tipo != '112')
      ) {
        texto += 'Dirección; ';
      }

      if (texto != '') {
        textoFin += '<b>Información de Receptor:</b><br/>&emsp; ' + texto + '<br/> ';
        texto = '';
      }

      for (var i = 0; i < $scope.dte.referencias.length; i++) {
        if ($scope.dte.referencias[i].tipoDocumento == '') {
          texto += 'Tipo Documento; ';
        }
        if (
          $scope.dte.referencias[i].tipoDocumento == '0' &&
          $scope.dte.referencias[i].documento == ''
        ) {
          texto += 'Documento Ref; ';
        }
        if (
          ($scope.dte.tipo == '56' ||
            $scope.dte.tipo == '61' ||
            $scope.dte.tipo == '112' ||
            $scope.dte.tipo == '111') &&
          $scope.dte.referencias[i].tipoMotivo == ''
        ) {
          texto += 'Tipo Motivo.; ';
        }
        if ($scope.dte.referencias[i].fecha == '') {
          texto += 'Fech Ref; ';
        }
        if (texto != '') {
          textoFin += '<b>Referencia num ' + (i + 1) + ':</b><br/> &emsp;' + texto + '<br/> ';
          texto = '';
        }
      }

      for (var i = 0; i < $scope.dte.descRecargos.length; i++) {
        if ($scope.dte.descRecargos[i].tipo == '') {
          texto += 'Tipo; ';
        }
        if ($scope.dte.descRecargos[i].tipoValor == '') {
          texto += 'Tipo de Valor; ';
        }
        if ($scope.dte.descRecargos[i].valor == '') {
          texto += 'Valor; ';
        }
        if (texto != '') {
          textoFin += '<b>Desc/Recargo num ' + (i + 1) + ':</b><br/> &emsp;' + texto + '<br/> ';
          texto = '';
        }
      }

      for (var i = 0; i < $scope.dte.detalles.length; i++) {
        if ($scope.dte.detalles[i].tipoCodigo == '') {
          texto += 'Tipo; ';
        }
        if ($scope.dte.detalles[i].codigo == '') {
          texto += 'Valor; ';
        }
        if ($scope.dte.detalles[i].nombre == '') {
          texto += 'Nombre; ';
        }
        if ($scope.dte.detalles[i].cantidad == '') {
          texto += 'Cantidad; ';
        }
        if ($scope.dte.detalles[i].precio == '') {
          texto += 'Precio; ';
        }
        if (texto != '') {
          textoFin += '<b>Detalle num ' + (i + 1) + ':</b><br/> &emsp;' + texto + '<br/> ';
          texto = '';
        }
      }

      return textoFin;
    };

    $scope.emitir = function() {
      var texto = $scope.validarCampos();
      if (texto != '') {
        UIkit.modal.alert(
          '¡Epa! No podemos crear tu documento porque nos falta lo siguiente:<br/><br/> <div style="font-size: 14px;">' +
            texto +
            '</div>',
          { labels: { Ok: 'Vale, lo arreglo!' } }
        );
      } else {
        var img =
          "<img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>";
        $rootScope.blockModal = UIkit.modal.blockUI(
          "<div class='uk-text-center'>Generando documento...<br/>" + img + '</div>'
        );
        var archivoIntegracion = $scope.construirArchivoIntegracion();
        console.log(archivoIntegracion);
        console.log(FebosUtil.encodeIso8859_1(archivoIntegracion));
        FebosAPI.cl_crear_dte(
          {},
          {
            // "debug": "si",
            //"simular": "no",
            entrada: 'txt',
            tipo: $scope.tipoCodigo,
            foliar: 'si',
            firmar: 'si',
            obtenerXml: 'no',
            obtenerPdf: 'si',
            //"notificarError": "milton@febos.cl",
            //"notificarOk": "milton@febos.cl",
            empresa: SesionFebos().empresa.iut,
            payload: Base64Encode(archivoIntegracion)
          },
          true,
          true
        ).then(
          function(response) {
            console.log(response);
            try {
              if (response.data.codigo === 10) {
                console.log('10!');
                $scope.limpiarCampos();
                $scope.texto_guardar = 'GUARDAR';

                var folio = response.data.folio;

                UIkit.notify(
                  'Se ha emitido la ' +
                    $scope.tipoProsa +
                    ' con Folio #' +
                    folio +
                    "!<a class='notify-action'>[X]</a> ",
                  { status: 'success', timeout: 3000 }
                );
                $scope.verPDF(response.data.febosID);
              } else {
                console.log('otro!');
                $scope.texto_guardar = 'GUARDAR';
                $rootScope.$broadcast('febosError', response);
              }
            } catch (e) {
              console.log(e);
              $scope.texto_guardar = 'GUARDAR';
              $rootScope.$broadcast('febosError', response);
            }
          },
          function(response) {
            //ahora se tratara de cargar el pdf...10 segundos mas tarde, para que termine el proceso.
            console.log(JSON.stringify(response));
            $timeout(llamarVerPDF, 10000);
          }
        );
      }
    };

    $scope.verPDF = function(id) {
      $scope.url_pdf = [];
      var img =
        "<img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>";
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Recuperando representación impresa...<br/>" + img + '</div>'
      );
      FebosAPI.cl_obtener_documento(
        {
          febosId: id,
          imagen: 'si',
          regenerar: 'no',
          incrustar: 'no',
          tipoImagen: 0
        },
        {},
        true,
        true
      ).then(function(response) {
        //$scope.block.hide();
        console.log(response);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;

        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            window.open(response.data.imagenLink);
            $scope.textoVerPdf = 'PDF';
          } else {
            $scope.textoVerPdf = 'PDF';
            $rootScope.$broadcast('febosError', response);
          }
        } catch (e) {
          $scope.textoVerPdf = 'PDF';
          $rootScope.$broadcast('febosError', response);
        }
      });
    };

    /**
     * Encode string into Base64, as defined by RFC 4648 [http://tools.ietf.org/html/rfc4648].
     * As per RFC 4648, no newlines are added.
     *
     * Characters in str must be within ISO-8859-1 with Unicode code point <= 256.
     *
     * Can be achieved JavaScript with btoa(), but this approach may be useful in other languages.
     *
     * @param {string} str ASCII/ISO-8859-1 string to be encoded as base-64.
     * @returns {string} Base64-encoded string.
     */
    function Base64Encode(str) {
      if (/([^\u0000-\u00ff])/.test(str)) throw Error('String must be ASCII');

      var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
      var o1,
        o2,
        o3,
        bits,
        h1,
        h2,
        h3,
        h4,
        e = [],
        pad = '',
        c;

      c = str.length % 3; // pad string to length of multiple of 3
      if (c > 0) {
        while (c++ < 3) {
          pad += '=';
          str += '\0';
        }
      }
      // note: doing padding here saves us doing special-case packing for trailing 1 or 2 chars

      for (c = 0; c < str.length; c += 3) {
        // pack three octets into four hexets
        o1 = str.charCodeAt(c);
        o2 = str.charCodeAt(c + 1);
        o3 = str.charCodeAt(c + 2);

        bits = (o1 << 16) | (o2 << 8) | o3;

        h1 = (bits >> 18) & 0x3f;
        h2 = (bits >> 12) & 0x3f;
        h3 = (bits >> 6) & 0x3f;
        h4 = bits & 0x3f;

        // use hextets to index into code string
        e[c / 3] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
      }
      str = e.join(''); // use Array.join() for better performance than repeated string appends

      // replace 'A's from padded nulls with '='s
      str = str.slice(0, str.length - pad.length) + pad;

      return str;
    }
  }
]);
