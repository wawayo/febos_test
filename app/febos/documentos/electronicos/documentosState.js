febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.dte', {
      url: '/:app/documentos/electronicos/:categoria/:vista?pagina&itemsPorPagina&orden&filtros',
      templateUrl: 'app/febos/documentos/electronicos/documentosView.html',
      controller: 'documentosCtrl',
      reloadOnSearch: false,
      params: {
        pagina: '',
        itemsPorPagina: '',
        filtros: '',
        orden: ''
      },
      data: {
        pageTitle: 'Documentos Electrónicos Emitidos'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        verificacionDeParametros: [
          'ComplementoDte',
          '$location',
          '$stateParams',
          'FebosUtil',
          '$state',
          'SesionFebos',
          '$rootScope',
          function(
            ComplementoDte,
            $location,
            $stateParams,
            FebosUtil,
            $state,
            SesionFebos,
            $rootScope
          ) {
            var categoria = $stateParams.categoria || 'recibidos';
            var pagina = $stateParams.filtros != '' ? $location.search().pagina || 1 : 1;
            var itemsPorPagina =
              $stateParams.filtros != '' ? $location.search().itemsPorPagina || 20 : 20;
            var orden =
              $stateParams.filtros != ''
                ? typeof $location.search().orden == 'undefined' || $location.search().orden == ''
                  ? categoria == 'emitidos'
                    ? '-fechaCreacion'
                    : '-fechaRecepcion'
                  : $location.search().orden
                : categoria == 'emitidos'
                  ? '-fechaCreacion'
                  : '-fechaRecepcion';
            //objeto para reemplazar en la ruta URL
            console.log(orden);
            if (
              (orden == '-fechaCreacion' || orden == '-fechaRecepcion') &&
              $stateParams.vista == 'noRecibidos'
            ) {
              orden = '-fechaRecepcionSii';
            }
            console.log(orden);

            var queryParams = {
              pagina: pagina,
              itemsPorPagina: itemsPorPagina,
              orden: orden
            };
            console.log({
              pagina: $stateParams.pagina,
              itemsPorPagina: $stateParams.itemsPorPagina,
              filtros: $stateParams.filtros,
              orden: $stateParams.orden
            });
            console.log($stateParams.app, $stateParams.categoria, $stateParams.vista);

            queryParams.filtros = ComplementoDte.prepararFiltrosDte(
              {
                pagina: $stateParams.pagina,
                itemsPorPagina: $stateParams.itemsPorPagina,
                filtros: $stateParams.filtros,
                orden: $stateParams.orden
              },
              $stateParams.app,
              $stateParams.categoria,
              $stateParams.vista
            );
            console.log('queryParams.filtros', queryParams.filtros);
            console.log('roberto2');
            $rootScope.queryParams = $location.search();

            var st = {};
            st.pagina = queryParams.pagina;
            st.itemsPorPagina = queryParams.itemsPorPagina;
            st.orden = queryParams.orden;
            st.filtros = queryParams.filtros;
            st.app = $stateParams.app;
            st.categoria = $stateParams.categoria;
            st.vista = $stateParams.vista;

            console.log('st', st);
            return st;
          }
        ],
        Datos: [
          'FebosAPI',
          '$location',
          'ComplementoDte',
          '$rootScope',
          'verificacionDeParametros',
          function(FebosAPI, $location, ComplementoDte, $rootScope, verificacionDeParametros) {
            //$stateParams.app;  // cloud, proveedores, clientes
            //$stateParams.sentido; // emitidos, recibidos
            //'filtros': "rutReceptor:76179952-5|fechaRecepcion:2018-04-01--2018-05-05",
            //seteo valores por defecto si no vienen

            $rootScope.configFiltrosActivo = false;
            var filtros = verificacionDeParametros.filtros;
            //var filtros = verificacionDeParametros.filtros;
            var rangos = Object.keys(ComplementoDte.atajosDeRangosDeFecha);
            for (var i = 0; i < rangos.length; i++) {
              var rango = rangos[i];
              if (filtros.includes(rango)) {
                filtros = filtros.replace(
                  rango,
                  ComplementoDte.atajosDeRangosDeFecha[rango].desde +
                    '--' +
                    ComplementoDte.atajosDeRangosDeFecha[rango].hasta
                );
                i--;
              }
            }

            var reemplazosDeBusquedaLike = [
              'rutCesionario',
              'razonSocialEmisor',
              'razonSocialReceptor',
              'razonSocialCesionario',
              'comentario',
              'lugar'
            ];
            for (var i = 0; i < reemplazosDeBusquedaLike.length; i++) {
              if (filtros.includes(reemplazosDeBusquedaLike[i])) {
                filtros = filtros.replace(
                  reemplazosDeBusquedaLike[i] + ':',
                  reemplazosDeBusquedaLike[i] + '$'
                );
              }
            }

            return FebosAPI.cl_listar_dte({
              pagina: verificacionDeParametros.pagina,
              itemsPorPagina: verificacionDeParametros.itemsPorPagina,
              campos:
                'trackId,tipoDocumento,folio,rutEmisor,razonSocialEmisor,rutReceptor,razonSocialReceptor,rutCesionario,razonSocialCesionario,indicadorDeTraslado,fechaCesion,codigoSii,fechaEmision,fechaRecepcion,fechaRecepcionSii,plazo,estadoComercial,estadoSii,fechaReciboMercaderia,formaDePago,montoTotal,iva,contacto,correoReceptor,fechaCesion,tipo,monto,lugar,comentario,fecha,medio,tpoTraVenta,tpoTranCompra,tpoTranCompraCodIva,tieneNc,tieneNd',
              filtros: filtros,
              orden: verificacionDeParametros.orden
            }).then(function(response) {
              console.log('?? Datos');
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          '$location',
          function($ocLazyLoad, $location) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'assets/js/encoding-japanese/encoding.js',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'app/febos/documentos/electronicos/documentosController.js'
            ]);
          }
        ]
      }
    });
  }
]);
