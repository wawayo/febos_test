febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.bitacora_dte', {
      url: '/:app/documentos/bitacora/:febosid?esBoleta&tipo',
      templateUrl: 'app/febos/documentos/electronicos/acciones/bitacora/bitacoraDteView.html',
      params: {
        esBoleta: '',
        tipo: ''
      },
      controller: 'bitacoraDteCtrl',
      reloadOnSearch: true,
      data: {
        pageTitle: 'Bitácora'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        bitacora: [
          'FebosAPI',
          '$stateParams',
          function(FebosAPI, $stateParams) {
            if ($stateParams.febosid.length > 40) {
              return FebosAPI.cl_listar_bitacora_portafolio(
                {
                  documentoId: $stateParams.febosid
                },
                {},
                true,
                false
              ).then(function(response) {
                return response.data;
              });
            } else {
              return FebosAPI.cl_listar_bitacora_dte(
                {
                  febosId: $stateParams.febosid,
                  pagina: 1,
                  filas: 200
                },
                {},
                true,
                false
              ).then(function(response) {
                return response.data;
              });
            }
          }
        ],
        esBoleta: [
          '$stateParams',
          function($stateParams) {
            console.log('validando si es boleta');
            console.log($stateParams);
            return $stateParams.esBoleta == 'si';
          }
        ],
        dte: [
          'FebosAPI',
          '$stateParams',
          function(FebosAPI, $stateParams) {
            if ($stateParams.febosid.length > 40) {
              return FebosAPI.cl_obtener_documento_portafolio(
                {
                  documentoId: $stateParams.febosid,
                  //incrustar:$stateParams.esBoleta=='si'?'no':'si',
                  incrustar: 'no',
                  imagen: 'si',
                  tipo: $stateParams.tipo
                },
                {},
                false,
                false
              ).then(
                function(response) {
                  return response.data;
                },
                function(err) {
                  return {};
                }
              );
            } else {
              return FebosAPI.cl_obtener_documento(
                {
                  febosId: $stateParams.febosid,
                  //incrustar:$stateParams.esBoleta=='si'?'no':'si',
                  incrustar: 'no',
                  imagen: 'si',
                  regenerar: 'si',
                  tipoImagen: $stateParams.esBoleta == 'si' ? '0' : '1'
                },
                {},
                false,
                false
              ).then(
                function(response) {
                  return response.data;
                },
                function(err) {
                  return {};
                }
              );
            }
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'bower_components/jquery-ui/jquery-ui.min.js',
              'lazy_uiSelect',
              'app/febos/documentos/electronicos/acciones/bitacora/bitacoraDteController.js'
            ]);
          }
        ]
      }
    });
  }
]);
