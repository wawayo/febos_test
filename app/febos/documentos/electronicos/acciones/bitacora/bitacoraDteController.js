angular.module('febosApp').controller('bitacoraDteCtrl', [
  '$rootScope',
  '$scope',
  '$timeout',
  'variables',
  '$location',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  '$state',
  '$stateParams',
  'bitacora',
  'dte',
  '$sce',
  '$window',
  '$http',
  'esBoleta',
  function(
    $rootScope,
    $scope,
    $timeout,
    variables,
    $location,
    SesionFebos,
    FebosAPI,
    FebosUtil,
    $state,
    $stateParams,
    bitacora,
    dte,
    $sce,
    $window,
    $http,
    esBoleta
  ) {
    $rootScope.page_full_height = true;
    $rootScope.headerDoubleHeightActive = true;
    $scope.esBoleta = esBoleta;
    $scope.$on('$destroy', function() {
      $rootScope.page_full_height = false;
      $rootScope.headerDoubleHeightActive = false;
    });
    //console.log("bitacora", bitacora);
    $scope.dte = dte;
    $scope.tipo = FebosUtil.tipoDocumento(parseInt(dte.tipo));

    //$scope.html = FebosUtil.decode(dte.imagenData);
    //$scope.iframe=$sce.trustAsResourceUrl('data:application/pdf;base64,' +dte.imagenData);
    var mime = esBoleta ? '' : 'data:text/html;base64,';
    var dteDeFebos2 = undefined;
    //console.log(" DTE IMAGEN LINK dteDeFebos2 ===>",dteDeFebos2);

    if (typeof dte.imagenLink != 'undefined') {
      //console.log(" DTE IMAGEN LINK ===>",dte.imagenLink);
      var dteDeFebos2 = dte.imagenLink.includes('.pdf');
      //console.log(" DTE IMAGEN LINK dteDeFebos2 ===>",dteDeFebos2);
    }

    console.log(mime);
    //$scope.iframe =  mime + (esBoleta?dte.imagenLink:dte.imagenData));
    try {
      //console.log(" DTE IMAGEN LINK dteDeFebos2 ===>",dteDeFebos2);

      $scope.iframe =
        esBoleta || dteDeFebos2
          ? $sce.trustAsResourceUrl(
              'https://docs.google.com/gview?url=' + dte.imagenLink + '&embedded=true'
            )
          : $sce.trustAsResourceUrl(dte.imagenLink.replace('http:', 'https:'));
    } catch (e) {
      $scope.iframe = '';
    }
    //$scope.invoices_data = invoices_data;
    $scope.bitacoras = bitacora.bitacora;
    $scope.getMonth = function(bitacora, $index) {
      try {
        ////console.log("bitacora",bitacora);
        ////console.log("bitacora index",$scope.bitacoras[$index - 1]);
        ////console.log("index",$index);
        ////console.log("bitacoraS",$scope.bitacoras);
        var prev_date = $scope.bitacoras[$index - 1].fecha;
        var this_date = bitacora.fecha;
        if (prev_date) {
          return this_date.split(' ')[0] !== prev_date.split(' ')[0];
        } else if ($index == 0) {
          return true;
        }
      } catch (e) {
        //console.log(e);
        return true;
      }
    };
    $scope.imprimir = function() {
      //myWindow=window.open(,'','width=1,height=1');
      $window.open(dte.imagenLink + '?imprimir=si', '_blank');
      //myWindow.print();
    };
    $scope.query = '';
    var w = angular.element($window);
    $scope.getWindowDimensions = function() {
      return {
        h: w.height(),
        w: w.width()
      };
    };
    $scope.$watch(
      $scope.getWindowDimensions,
      function(newValue, oldValue) {
        $scope.windowHeight = newValue.h;
        $scope.windowWidth = newValue.w;
      },
      true
    );
    $scope.buscar = function(item) {
      if (
        item.fecha.includes($scope.query.toLowerCase()) ||
        item.mensaje.toLowerCase().includes($scope.query.toLowerCase()) ||
        item.usuarioNombre.toLowerCase().includes($scope.query.toLowerCase()) ||
        item.usuarioCorreo.toLowerCase().includes($scope.query.toLowerCase())
      ) {
        return true;
      }
      return false;
    };
    //var iframe=$("#documento").contentWindow.document;

    // invoice details
    //Descargar adjunto bitacora

    $scope.descargarAdjunto = function(bitacoraId) {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Descargando archivo adjunto..." +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_obtener_adjunto_bitacora(
        {
          link: 'si',
          idBitacora: bitacoraId
        },
        {},
        true,
        true
      ).then(function(response) {
        console.log(response);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;

        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            window.open(response.data.archivo);
          } else {
            SesiobFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        } catch (e) {
          console.log(e);
          SesiobFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
        }
      });
    };

    $scope.cargando = false;
    // -- función que obtiene y muestra un PDF, consumiendo la API
    $scope.verPDF = function(febosId, regenerarPdf) {
      console.log(febosId);
      $scope.url_pdf = [];
      $scope.cargando = true;
      $scope.textoVerPdf = 'Cargando ...';
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Generando representación impresa...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      FebosAPI.cl_obtener_documento(
        {
          febosId: febosId,
          imagen: 'si',
          regenerar: regenerarPdf ? 'si' : 'no',
          incrustar: 'no',
          tipoImagen: 0
        },
        {},
        true,
        true
      ).then(function(response) {
        //$scope.block.hide();
        console.log(response);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        $scope.cargando = false;

        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            window.open(response.data.imagenLink);
            $scope.textoVerPdf = 'PDF';
          } else {
            $scope.textoVerPdf = 'PDF';
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        } catch (e) {
          $scope.textoVerPdf = 'PDF';
          SesionFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
        }
      });
    };

    $scope.consultarTrazaCorreo = function(mailId) {
      //console.log("CONSULTANDO " + mailId);
      $scope.trazaCorreo = {
        destino: '',
        eventos: []
      };
      $scope.trazaCorreoError = false;
      UIkit.modal('#mdTraza', { modal: false }).show();
      var url = 'https://api.febos.io/produccion/correo?mailid=' + mailId;
      var req = {
        method: 'GET',
        url: url
      };
      $http(req).then(
        function(response) {
          //console.log("RESPUESTA CORREO", response);
          $scope.trazaCorreo = {
            destino: response.data.para,
            eventos: response.data.registros
          };
        },
        function(response) {
          //console.log("RESPUESTA CORREO ERROR", response);
          $scope.trazaCorreoError = true;
        }
      );
    };
  }
]);
