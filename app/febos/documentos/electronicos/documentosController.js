angular.module('febosApp').controller('documentosCtrl', [
  '$rootScope',
  '$scope',
  '$timeout',
  'variables',
  '$location',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  'Datos',
  '$state',
  '$stateParams',
  'VistasDte',
  'verificacionDeParametros',
  '$http',
  'ComplementoDte',
  'utils',
  '$window',
  function(
    $rootScope,
    $scope,
    $timeout,
    variables,
    $location,
    SesionFebos,
    FebosAPI,
    FebosUtil,
    Datos,
    $state,
    $stateParams,
    VistasDte,
    verificacionDeParametros,
    $http,
    ComplementoDte,
    utils,
    $window
  ) {
    //'$location','$timeout','$rootScope',
    //function ($location,$timeout,$rootScope) {
    //$location.search($rootScope.queryParams);
    // $scope.$on('$locationChangeSuccess', function () {
    //    $state.reload();
    //});
    $timeout(function() {}, 3000);

    $scope.app = $stateParams.app;
    $scope.vista = $stateParams.vista;

    $scope.ipp = parseInt(verificacionDeParametros.itemsPorPagina);
    $scope.categoria = $stateParams.categoria;

    //validacion para mostrar/ocultar accion masiva Fecha Recepcion SII FB-384
    $scope.esAppProveedores = $scope.app == 'proveedores';
    $scope.esAppCloud = $scope.app == 'cloud';
    // Menú Emitidos, Mis Emitidos, Mis Emitidos Unidad, Guías Emitidas, Guias Recibidas
    $scope.esCategoriaEmitidoVistaTodos = $scope.categoria == 'emitidos' && $scope.vista == 'todos';
    $scope.esCategoriaEmitidoVistaMisEmitidos =
      $scope.categoria == 'emitidos' && $scope.vista == 'misEmitidos';
    $scope.esCategoriaEmitidoVistaMisEmitidosUnidad =
      $scope.categoria == 'emitidos' && $scope.vista == 'misEmitidosUnidad';
    $scope.esCategoriaEmitidoVistaGuiasEmitidas =
      $scope.categoria == 'emitidos' && $scope.vista == 'guias';
    $scope.esCategoriaRecibidoVistaGuiasRecibidas =
      $scope.categoria == 'recibidos' && $scope.vista == 'guias';
    //validacion para mostrar/ocultar accion masiva Fecha Recepcion SII FB-384

    $scope.referencias = {};
    var query = {
      pagina: verificacionDeParametros.pagina,
      itemsPorPagina: verificacionDeParametros.itemsPorPagina,
      orden: verificacionDeParametros.orden,
      filtros: verificacionDeParametros.filtros
    };
    $location.search(query);

    $scope.FebosUtil = FebosUtil;
    $scope.documentos = Datos.documentos;
    $scope.parametros = $location.search();
    $scope.variablesPaginacion = {
      pagina: parseInt($location.search().pagina),
      itemsPorPagina: parseInt($location.search().itemsPorPagina),
      itemsPorPagina: Datos.documentos.length
    };

    $scope.paginacion = {
      elementoDesde:
        (parseInt($location.search().pagina) - 1) * parseInt($location.search().itemsPorPagina) + 1,
      elementoHasta:
        (parseInt($location.search().pagina) - 1) * parseInt($location.search().itemsPorPagina) +
        Datos.documentos.length,
      total: Datos.totalElementos,
      paginas: parseInt(Datos.totalPaginas)
    };
    $scope.fechaEnProsa = function(fecha, conHora) {
      if (typeof fecha == 'undefined') return '';
      var meses = [
        '',
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre'
      ];

      conHora = typeof conHora == 'undefined' ? false : conHora;
      var partes = fecha.split(' ');
      var fracciones = partes[0].split('-');
      var anio = fracciones[0].length == 4 ? fracciones[0] : fracciones[2];
      var mes = fracciones[1];
      var dia = fracciones[0].length == 4 ? fracciones[2] : fracciones[0];
      var anioActual = new Date().getFullYear();
      var diferenciaEnAnios = anioActual - anio;
      var anioEnProsa = '';
      switch (diferenciaEnAnios) {
        case 0:
          anioEnProsa = ' de este año';
          break;
        case 1:
          anioEnProsa = ' del año pasado';
          break;
        case 2:
          anioEnProsa = ' del año antepasado';
          break;
        default:
          anioEnProsa = ' del año ' + anio;
      }

      var prosa = parseInt(dia) + ' de ' + meses[parseInt(mes)] + anioEnProsa;

      if (partes.length == 2 && conHora) {
        var fraccionHora = partes[1].split(':');
        var hora = parseInt(fraccionHora[0]);
        var minuto = parseInt(fraccionHora[1]);

        prosa += ', ';
        if (minuto == 0) {
          if (hora < 11) {
            prosa += 'a las ' + hora + ' en punto de la mañana';
          } else if (hora == 12) {
            prosa += 'justo a medio día';
          } else if (hora == 0) {
            prosa += 'justo a media noche';
          } else if (hora > 12 && hora <= 19) {
            prosa += 'a las ' + (hora - 12) + ' en punto de la tarde';
          } else if (hora > 19 && hora <= 23) {
            prosa += 'a las ' + (hora - 12) + ' en punto de la noche';
          }
        } else if (minuto == 15) {
          if (hora <= 12) {
            prosa += 'a las ' + hora + ' y cuarto de la mañana';
          } else if (hora >= 12 && hora <= 19) {
            prosa += 'a las ' + (hora - 12) + ' y cuarto de la tarde';
          } else if (hora > 19 && hora <= 23) {
            prosa += 'a las ' + (hora - 12) + ' y cuarto de la noche';
          }
        } else if (minuto == 30) {
          if (hora <= 12) {
            prosa += 'a las ' + hora + ' y media de la mañana';
          } else if (hora >= 12 && hora <= 19) {
            prosa += 'a las ' + (hora - 12) + ' y media de la tarde';
          } else if (hora > 19 && hora <= 23) {
            prosa += 'a las ' + (hora - 12) + ' y media de la noche';
          }
        } else if (minuto == 45) {
          if (hora <= 11) {
            prosa += 'un cuarto para las ' + hora + 1 + ' de la mañana';
          }
          if (hora == 12) {
            prosa += 'un cuarto para las 1 de la tarde';
          } else if (hora > 12 && hora <= 19) {
            prosa += 'un cuarto para las ' + (hora - 11) + ' de la tarde';
          } else if (hora > 19 && hora <= 23) {
            prosa += 'un cuarto para las ' + (hora - 11) + ' de la noche';
          }
        } else {
          if (hora <= 12) {
            prosa +=
              'a las ' +
              hora +
              ' de la mañana con ' +
              minuto +
              ' minuto' +
              (minuto == 1 ? '' : 's');
          } else if (hora >= 12 && hora <= 19) {
            prosa +=
              'a las ' +
              (hora - 12) +
              ' de la tarde con ' +
              minuto +
              ' minuto' +
              (minuto == 1 ? '' : 's');
          } else if (hora > 19 && hora <= 23) {
            prosa +=
              'a las ' +
              (hora - 12) +
              ' de la noche con ' +
              minuto +
              ' minuto' +
              (minuto == 1 ? '' : 's');
          }
        }
      }
      return prosa;
    };
    $scope.plazoEnProsa = function(documento) {
      if (
        typeof documento.estadoComercial == 'undefined' ||
        documento.estadoComercial == 0 ||
        documento.estadoComercial == 1 ||
        documento.estadoComercial == 3
      ) {
        if (
          ['0', '1', '3'].indexOf(documento.estadoComercial.toString()) >= 0 &&
          documento.plazo !== undefined &&
          ['33', '34', '43'].indexOf(documento.tipoDocumento.toString()) >= 0 &&
          documento.formaDePago != 1 &&
          documento.formaDePago != 3
        ) {
          if (documento.plazo <= 0) {
            return ', a la cual se le <b>ha vencido el plazo para realizar la aceptación o reclamo</b>, por lo que se entiende como <b>aceptada comercialmente</b> según las instrucciones dadas por el <b>Servicio de Impuestos Internos</b>';
          }
          if (documento.plazo == 1) {
            return ', a la cual sólo le queda <b>1 día disponible<b/> para realizar la aceptación o reclamo en el <b>Servicio de Impuestos Internos</b>, de lo contrario, se entenderá como <b>aceptada comercialmente</b>';
          } else if (documento.plazo == 2 || documento.plazo == 3) {
            return (
              ', a la cual sólo le quedan <b>' +
              documento.plazo +
              ' días disponibles<b/> para realizar la aceptación o reclamo en el <b>Servicio de Impuestos Internos</b>, de lo contrario, se entenderá como <b>aceptada comercialmente</b>'
            );
          } else if (documento.plazo >= 4) {
            return (
              ', a la cual le quedan <b>' +
              documento.plazo +
              ' días disponibles<b/> para realizar la aceptación o reclamo en el <b>Servicio de Impuestos Internos</b>'
            );
          }
        } else if (
          documento.plazo == undefined &&
          ['33', '34', '43'].indexOf(documento.tipoDocumento.toString()) >= 0 &&
          documento.formaDePago != 1 &&
          documento.formaDePago != 3
        ) {
          return ', de la cual, al no tener la fecha de recepción en el <b>Servicio de Impuestos Internos</b>, no es posible calcular el plazo de vencimiento para su aceptación o reclamo';
        } else {
          return ', la cual, por instrucción del <b>Servicio de Impuestos Internos</b>, y debido a las características mismas del documento, no se encuentra afecta al registro de aceptación o reclamo';
        }
      } else {
        return '';
      }
    };
    $scope.extraSiiEnProsa = function(documento) {
      switch (documento.codigoSii) {
        case 'FAN':
          return '. Posteriormente el <b>folio</b> de éste documento <b>fue anulado por el emisor del documento</b>, por lo que el documento <b>no es legalmente válido</b>';
        case 'TMD':
          return ". Posteriormente el <b>texto</b> de éste documento fue <b>modificado con una nota de débito</b> (puedes consultar las referencias de este documento, con el botón de 'Ver Referencias')";
        case 'TMC':
          return ". Posteriormente el <b>texto</b> de éste documento fue <b>modificado con una nota de crédito</b> (puedes consultar las referencias de este documento, con el botón de 'Ver Referencias')";
        case 'MMD':
          return ". Posteriormente el <b>monto</b> de éste documento fue <b>modificado con una nota de débito</b> (puedes consultar las referencias de este documento, con el botón de 'Ver Referencias')";
        case 'MMC':
          return ". Posteriormente el <b>monto</b> de éste documento fue <b>modificado con una nota de crédito</b> (puedes consultar las referencias de este documento, con el botón de 'Ver Referencias')";
        case 'AND':
          return ". Posteriormente éste documento fue <b>anulado</b> con una nota de débito</b> (puedes consultar las referencias de este documento, con el botón de 'Ver Referencias')";
        case 'ANC':
          return ". Posteriormente éste documento fue <b>anulado</b> con una nota de crédito</b> (puedes consultar las referencias de este documento, con el botón de 'Ver Referencias')";
        default:
          return '';
      }
    };

    $scope.cedidoEnProsa = function(documento) {
      if (
        typeof documento.rutCesionario == 'undefined' ||
        documento.rutCesionario == '' ||
        documento.rutCesionario == 'NULL'
      )
        return '';
      if (documento.rutCesionario.length > 5) {
        return (
          '. Es importante destacar que éste documento <b>fue cedido</b> a la empresa <b>' +
          documento.razonSocialCesionario +
          '</b>, R.U.T. ' +
          documento.rutCesionario +
          ' el día ' +
          $scope.fechaEnProsa(documento.fechaCesion)
        );
      } else {
        return '. Es importante destacar que éste documento <b>fue cedido</b>, sin embargo, no contamos con el detalle de ésta información por ahora';
      }
    };

    $scope.prosa = function(documento) {
      var descripcionDocumento =
        '<b>' +
        $scope.tipoDocumento(documento.tipoDocumento, false) +
        '</b> folio <b>' +
        documento.folio +
        '</b> emitida por <b>' +
        documento.razonSocialEmisor +
        '</b> el ' +
        $scope.fechaEnProsa(documento.fechaEmision) +
        ', por un monto total de <b>' +
        $scope.formatearMonto(documento.montoTotal, '$') +
        '</b>';
      var recepcionadaEnSii =
        typeof documento.fechaRecepcionSii == 'undefined'
          ? ', de la cual aún no tenemos el registro de la fecha en que el <b>Servicio de Impuestos Internos</b> la recibió'
          : ', recibida por el <b>Servicio de Impuestos Internos</b> el ' +
            $scope.fechaEnProsa(documento.fechaRecepcionSii, true);
      var recepcionadaEnFebos =
        typeof documento.fechaRecepcion == 'undefined'
          ? ' y <b>no recepcionada aún en febos</b>'
          : ' y recibida en <b>Febos</b> el ' + $scope.fechaEnProsa(documento.fechaRecepcion, true);
      var plazoEnProsa = $scope.plazoEnProsa(documento);
      var siiEnProsa = $scope.extraSiiEnProsa(documento);
      var cedidoEnProsa = $scope.cedidoEnProsa(documento);
      return (
        descripcionDocumento +
        recepcionadaEnSii +
        recepcionadaEnFebos +
        plazoEnProsa +
        siiEnProsa +
        cedidoEnProsa +
        '.'
      );
    };

    $scope.estadoSii = function(estado) {
      switch (estado) {
        case 1:
          return { color: 'yellow', icono: 'alarm', descripcion: 'Pendiente de envio al SII' };
        case 2:
          return { color: 'blue', icono: 'send', descripcion: 'Enviado al SII' };
        case 3:
          return { color: 'red', icono: 'bug_report', descripcion: 'Error al enviar' };
        case 4:
          return {
            color: 'green',
            icono: 'done_all',
            descripcion: $scope.vista == 'honorarios' ? 'Vigente' : 'Aceptado por el SII'
          };
        case 5:
          return { color: 'yellow', icono: 'done', descripcion: 'Aceptado con reparos' };
        case 6:
          return {
            color: 'red',
            icono: 'clear',
            descripcion: $scope.vista == 'honorarios' ? 'Anulada' : 'Rechazado por el SII'
          };
        case 7:
          return { color: 'blue', icono: 'alarm', descripcion: 'Pendiente de consulta en el SII' };
        case 8:
          return { color: 'blue', icono: 'alarm', descripcion: 'Pendiente de consulta en el SII' };
        default:
          return {
            color: 'gray',
            icono: 'sentiment_very_dissatisfied',
            descripcion: 'Desconocido'
          };
      }
    };
    $scope.estadoComercial = function(estado) {
      switch (estado) {
        case 1:
          return {
            color: 'teal',
            icono: 'thumb_up',
            descripcion: 'Inteción de aceptación comercial'
          };
        case 2:
          return {
            color: 'light-green',
            icono: 'done',
            descripcion: 'Aceptado comercialmente en el SII'
          };
        case 3:
          return {
            color: 'orange',
            icono: 'thumb_down',
            descripcion: 'Intención de accionAutomatica comercial'
          };
        case 4:
          return {
            color: 'red',
            icono: 'pan_tool',
            descripcion: 'Rechazado comercialmente en el SII'
          };
        case 5:
          return {
            color: 'red',
            icono: 'pan_tool',
            descripcion: 'Reclamado parcialmente en el SII'
          };
        case 6:
          return { color: 'red', icono: 'pan_tool', descripcion: 'Reclamado totalmente en el SII' };
        case 7:
          return {
            color: 'green',
            icono: 'done_all',
            descripcion: 'Con aceptación de mercaderías y/o servicios en el SII'
          };
        default:
          return { color: 'gray', icono: 'visibility', descripcion: 'Sin acción comercial aún' };
      }
    };

    $scope.tipoDocumentoReferencia = function(tipo_dte, mostrar_codigo, version_compacta) {
      var tipoDocRef = FebosUtil.tipoDocumento(
        parseInt(tipo_dte),
        mostrar_codigo,
        version_compacta
      );
      if (tipoDocRef.length == 0) {
        return tipo_dte;
      } else {
        return tipoDocRef;
      }
    };

    $scope.tipoDocumento = function(a, b, c) {
      return FebosUtil.tipoDocumento(a, b, c);
    };
    $scope.formatearMonto = function(numero, simbolo) {
      if (typeof numero != 'undefined') {
        var partes = numero.toString().split('.');
        partes[0] = partes[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
        return simbolo + ' ' + partes.join(',');
      } else {
        return '';
      }
    };

    var $documentos = $('#documentos');

    if (typeof SesionFebos().documentosSeleccionados == 'undefined') {
      SesionFebos().documentosSeleccionados = [];
    }
    $scope.documentosSeleccionados = SesionFebos().documentosSeleccionados;
    $scope.estaSeleccionado = function(id) {
      if ($.inArray(id, $scope.documentosSeleccionados) >= 0) {
        return true;
      } else {
        return false;
      }
    };

    $scope.listarSeleccionados = function() {
      var params = $location.search();
      console.log('seleccionados', $scope.documentosSeleccionados);
      if (
        $scope.documentosSeleccionados != undefined &&
        $scope.documentosSeleccionados.length > 0
      ) {
        params.filtros = 'febosId:' + $scope.documentosSeleccionados.join(',');
      } else {
        params.filtros = '';
      }
      params.pagina = 1;
      $location.search(params);
      $state.go('restringido.dte', {
        app: $stateParams.app,
        categoria: $stateParams.categoria,
        vista: $stateParams.vista,
        filtros: params.filtros,
        orden: $stateParams.orden,
        pagina: params.pagina,
        itemsPorPagina: params.itemsPorPagina
      });
      $state.reload();
    };
    $scope.deseleccionarTodo = function() {
      var cantidad = $scope.documentosSeleccionados.length;
      $timeout(function() {
        $documentos.find('.select_message').each(function() {
          if ($(this).is(':checked')) {
            $(this).iCheck('uncheck');
          }
        });
        while (typeof $scope.documentosSeleccionados.pop() != 'undefined');
      }, 100);

      var mensaje =
        cantidad == 1
          ? 'Se deseleccionó 1 documento'
          : 'Se deseleccionaron ' + cantidad + ' documentos';
      UIkit.notify(mensaje + "<a class='notify-action'>[X]</a> ", {
        status: 'success',
        timeout: 3000
      });

      // UIkit.notify(mensaje + "<a class='notify-action'>[X]</a> ", {status: "success", timeout: 3000});
    };
    $scope.escanearDocumentosSeleccionados = function() {
      //revisar si habian documentos guardados en la sesion
      var seleccionados = [];
      var deseleccionados = [];
      $documentos.find('.select_message').each(function(index, elemento) {
        var febosId = $(elemento)
          .attr('id')
          .split('_')[1];
        if ($(elemento).is(':checked')) {
          seleccionados.push(febosId);
        } else {
          deseleccionados.push(febosId);
        }
      });
      //insertar seleccionados
      $.each(seleccionados, function(i, el) {
        if ($.inArray(el, $scope.documentosSeleccionados) === -1) {
          $scope.documentosSeleccionados.push(el);
        }
      });
      var eliminar = [];
      $.each($scope.documentosSeleccionados, function(i, el) {
        if ($.inArray(el, deseleccionados) >= 0) {
          eliminar.push(i);
        }
      });
      //eliminar indices deseleccionados
      for (var i = eliminar.length - 1; i >= 0; i--) {
        $scope.documentosSeleccionados.splice(eliminar[i], 1);
      }
      setTimeout(function() {
        $scope.$apply();
      }, 200);
    };
    // seleccionar un documento
    $documentos.on('ifChanged', '.select_message', function() {
      $(this).is(':checked')
        ? $(this)
            .closest('li')
            .addClass('md-card-list-item-selected')
        : $(this)
            .closest('li')
            .removeClass('md-card-list-item-selected');
      $scope.escanearDocumentosSeleccionados();
    });

    // seleccionar todos los documentos
    $('#documentos_seleccionar_todos').on('ifChanged', function() {
      var $this = $(this);
      $documentos.find('.select_message').each(function() {
        $this.is(':checked') ? $(this).iCheck('check') : $(this).iCheck('uncheck');
      });
      $scope.escanearDocumentosSeleccionados();
    });

    // ver el detalle de un documento
    $scope.hayUnDetalleDesplegado = false;
    $documentos.on('click', '.md-card-list ul > li.seleccionable', function(e) {
      if (
        !$(e.target).closest('.md-card-list-item-menu').length &&
        !$(e.target).closest('.md-card-list-item-select').length
      ) {
        var $this = $(this);
        if (!$this.hasClass('item-shown')) {
          // obtener la altura del detalle del documento
          var el_min_height =
            $this.height() + $this.children('.md-card-list-item-content-wrapper').actual('height');
          // esconder el contenido del mensaje
          $documentos.find('.item-shown').velocity('reverse', {
            begin: function(elements) {
              $(elements)
                .removeClass('item-shown')
                .children('.md-card-list-item-content-wrapper')
                .hide()
                .velocity('reverse');
            }
          });
          // mostrar el mensaje
          $this.velocity(
            {
              marginTop: 40,
              marginBottom: 40,
              marginLeft: 0,
              marginRight: 0,
              minHeight: el_min_height
            },
            {
              duration: 300,
              easing: variables.easing_swiftOut,
              begin: function(elements) {
                $(elements).addClass('item-shown');
              },
              complete: function(elements) {
                // show: message content, reply form
                $(elements)
                  .children('.md-card-list-item-content-wrapper')
                  .show()
                  .velocity({
                    opacity: 1
                  });
                // scroll to message
                var container = $('body'),
                  scrollTo = $(elements);
                container.animate(
                  {
                    scrollTop: scrollTo.offset().top - $('#page_content').offset().top - 8
                  },
                  1000,
                  variables.bez_easing_swiftOut
                );
                $scope.hayUnDetalleDesplegado = true;
              }
            }
          );
        }
      }
    });
    // esconder el detalle del documento al: hacer click afuera, apretar escape
    $(document).on('click keydown', function(e) {
      if (!$(e.target).closest('li.item-shown').length || e.which == 27) {
        $documentos.find('.item-shown').velocity('reverse', {
          begin: function(elements) {
            $(elements)
              .removeClass('item-shown')
              .children('.md-card-list-item-content-wrapper')
              .hide()
              .velocity('reverse');
          }
        });
      }
    });

    $scope.revisarCaracteresRaros = function() {
      var hayCaracteresRaros = 0;
      for (var i = 0; i < $scope.documentos.length; i++) {
        var json = JSON.stringify($scope.documentos[i]);
        if (json.includes('??')) {
          hayCaracteresRaros++;
          if (hayCaracteresRaros > 3) {
            break;
          }
        }
      }
      if (hayCaracteresRaros > 0) {
        var mensaje = "<i class='material-icons'>&#xE002;</i>";
        var opciones = {
          status: 'warning',
          timeout: 10000
        };
        switch (hayCaracteresRaros) {
          case 1:
            mensaje +=
              ' Detectamos que un documento de éste listado contiene caracteres no válidos, por lo que algunas palabras podrían no verse escritas correctamente. Esto se debe a que el documento viene así desde su origen.';
            break;
          case 2:
            mensaje +=
              ' Detectamos que dos documentos de éste listado contienen caracteres no válidos, por lo que algunas palabras podrían no verse escritas correctamente. Esto se debe a que los documentos vienen así desde su origen.';
            break;
          default:
            mensaje +=
              ' Detectamos que varios documentos de éste listado contienen caracteres no válidos, por lo que algunas palabras podrían no verse escritas correctamente. Esto se debe a que los documentos vienen así desde su origen.';
            break;
        }
        UIkit.notify(mensaje + "<a class='notify-action'>[X]</a> ", opciones);
      }
    };

    $scope.avanzarPagina = function() {
      var params = $location.search();
      var siguientePagina = parseInt(params.pagina) + 1;
      if (siguientePagina > $scope.paginacion.paginas) {
        UIkit.notify("No hay mas páginas <a class='notify-action'>[X]</a> ", {
          status: 'danger',
          timeout: 2000
        });
      } else {
        $location.search('pagina', siguientePagina);
        $state.go('restringido.dte', {
          app: $stateParams.app,
          categoria: $stateParams.categoria,
          vista: $stateParams.vista,
          filtros: $stateParams.filtros,
          orden: $stateParams.orden,
          pagina: siguientePagina,
          itemsPorPagina: $stateParams.itemsPorPagina
        });
        $state.reload();
      }
    };
    $scope.retrocederPagina = function() {
      var params = $location.search();
      var siguientePagina = parseInt(params.pagina) - 1;
      if (siguientePagina < 1) {
        UIkit.notify(
          "Ésta es la primera página, no se puede <br/>retroceder más =( <a class='notify-action'>[X]</a> ",
          {
            status: 'danger',
            timeout: 2000
          }
        );
      } else {
        $location.search('pagina', siguientePagina);
        $state.go('restringido.dte', {
          app: $stateParams.app,
          categoria: $stateParams.categoria,
          vista: $stateParams.vista,
          filtros: $stateParams.filtros,
          orden: $stateParams.orden,
          pagina: siguientePagina,
          itemsPorPagina: $stateParams.itemsPorPagina
        });
        $state.reload();
      }
    };
    $scope.mostrarInputPagina = false;
    $scope.paginaIngresada = 1;
    $scope.irPagina = function(mostrarInputPagina, valor) {
      $scope.mostrarInputPagina = mostrarInputPagina;
      if (mostrarInputPagina) {
        $scope.paginaIngresada = parseInt($location.search().pagina);
      } else {
        var pagina = parseInt($('#paginaManual')[0].value);
        if (pagina > $scope.paginacion.paginas || pagina <= 0) {
          UIkit.notify(
            'Imposible ir la página ' +
              pagina +
              ", ya que no existe <a class='notify-action'>[X]</a> ",
            {
              status: 'danger',
              timeout: 2000
            }
          );
        } else {
          $location.search('pagina', pagina);
          $state.go('restringido.dte', {
            app: $stateParams.app,
            categoria: $stateParams.categoria,
            vista: $stateParams.vista,
            filtros: $stateParams.filtros,
            orden: $stateParams.orden,
            pagina: pagina,
            itemsPorPagina: $stateParams.itemsPorPagina
          });
          $state.reload();
        }
      }
      //var params = $location.search();
      //$location.search("pagina", parseInt(params.pagina) + 1);
    };
    $scope.actualizarPagina = function() {
      if (verificacionDeParametros.itemsPorPagina != $scope.ipp) {
        $state.go('restringido.dte', {
          app: $stateParams.app,
          categoria: $stateParams.categoria,
          vista: $stateParams.vista,
          filtros: $stateParams.filtros,
          orden: $stateParams.orden,
          pagina: 1,
          itemsPorPagina: $scope.ipp
        });
        $state.reload();
      }
    };

    $scope.comprarSiLosFiltrosSonIguales = function(A, B) {
      console.log('comparando', A, B);
      if (A.filtros == B.filtros && A.orden == B.orden) {
        return true;
      }
      return false;
    };
    $scope.detectarTitulo = function() {
      var query = $location.search();
      if (typeof SesionFebos().filtrosDte != 'undefined') {
        for (var i = 0; i < SesionFebos().filtrosDte[$stateParams.categoria].length; i++) {
          var parametros = SesionFebos().filtrosDte[$stateParams.categoria][i];
          if ($scope.comprarSiLosFiltrosSonIguales(query, parametros.params)) {
            return (
              VistasDte[$stateParams.app][$stateParams.categoria][$stateParams.vista].titulo +
              ' aplicando mi vista: ' +
              SesionFebos().filtrosDte[$stateParams.categoria][i].nombre
            );
          }
        }
      }
      return VistasDte[$stateParams.app][$stateParams.categoria][$stateParams.vista].titulo;
    };
    $scope.tituloVista = $scope.detectarTitulo();
    $rootScope.$on('refrescarTituloDte', function() {
      $scope.tituloVista = $scope.detectarTitulo();
    });

    //trigger automaticos al iniciar pagina
    $scope.revisarCaracteresRaros();

    //Descarga PDF
    $scope.cargando = false;
    $scope.cargando.e = false;
    // -- función que obtiene y muestra un PDF, consumiendo la API
    $scope.verPDF = function(id, cedible) {
      console.log(id);
      console.log('ver PDF');

      $scope.url_pdf = [];
      $scope.cargando.e = true;
      $scope.textoVerPdf = 'Cargando ...';
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Generando representación impresa...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      FebosAPI.cl_obtener_documento(
        {
          febosId: id,
          imagen: 'si',
          regenerar: 'no',
          incrustar: 'no',
          tipoImagen: 0,
          cedible: cedible
        },
        {},
        true,
        true
      ).then(function(response) {
        //$scope.block.hide();
        console.log(response);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        $scope.cargando.e = false;

        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            window.open(response.data.imagenLink);
            $scope.textoVerPdf = 'PDF';
          } else {
            $scope.textoVerPdf = 'PDF';
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        } catch (e) {
          $scope.textoVerPdf = 'PDF';
          SesionFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
        }
      });
    };

    $scope.modalAccionComercial = UIkit.modal('#accion_comercial');
    $scope.modalInfoPago = UIkit.modal('#info_pago');
    $scope.documentoActual = {};
    //$scope.vincularSii='';
    $scope.motivo = '';
    $scope.recinto = '';
    $scope.masivo = 'no';
    $scope.tipoGestionComercial = 'RCD';
    $scope.popUpAccionComercial = function(documento) {
      $scope.vincularAlSii = 'no';
      $scope.motivo = '';
      $scope.recinto = '';
      $scope.tipoGestionComercial = 'RCD';
      $scope.documentoActual = documento;
      $scope.masivo = 'no';
      $scope.modalAccionComercial.show();
    };
    $scope.popUpAccionComercialMasivo = function(documentosSeleccionados) {
      $scope.vincularAlSii = 'no';
      $scope.motivo = '';
      $scope.recinto = '';
      $scope.tipoGestionComercial = 'RCD';
      $scope.documentosSeleccionados = documentosSeleccionados;
      $scope.masivo = 'si';
      $scope.modalAccionComercial.show();
    };
    $scope.popUpInfoPago = function(documento) {
      $scope.documentoActual = documento;
      $scope.modalInfoPago.show();
    };
    $scope.nuevoEstadoDocumento = 0;
    $scope.gestionComercial = function(tipoAccion, vincular) {
      var mensaje = '';
      $scope.modalAccionComercial.hide();
      switch (tipoAccion) {
        case 'ACD':
          $scope.nuevoEstadoDocumento = 1;
          if (vincular == 'si') {
            $scope.nuevoEstadoDocumento = 2;
          }
          mensaje = 'Aceptando documento' + (vincular == 'si' ? ' y notificando al SII' : '');
          break;
        case 'RCD':
          if ($scope.motivo == '') {
            UIkit.notify(
              "Debe especificar un motivo de accionAutomatica <a class='notify-action'>[X]</a> ",
              {
                status: 'danger',
                timeout: 2000
              }
            );
            return;
          }
          $scope.nuevoEstadoDocumento = 3;
          if (vincular == 'si') {
            $scope.nuevoEstadoDocumento = 4;
          }
          mensaje = 'Rechazando documento' + (vincular == 'si' ? ' y notificando al SII' : '');
          break;
        case 'ERM':
          if ($scope.recinto == '') {
            UIkit.notify(
              "Debe especificar un recinto de recepción <a class='notify-action'>[X]</a> ",
              {
                status: 'danger',
                timeout: 2000
              }
            );
            return;
          }

          $scope.nuevoEstadoDocumento = 7;
          mensaje =
            'Generando recibo de mercaderías para el documento' +
            (vincular == 'si' ? ' y notificando al SII' : '');
          break;
        case 'RFT':
          if ($scope.motivo == '') {
            UIkit.notify(
              "Debe especificar un motivo de reclamo <a class='notify-action'>[X]</a> ",
              {
                status: 'danger',
                timeout: 2000
              }
            );
            return;
          }
          $scope.nuevoEstadoDocumento = 3;
          if (vincular == 'si') {
            $scope.nuevoEstadoDocumento = 6;
          }
          mensaje = 'Reclamando documento' + (vincular == 'si' ? ' y notificando al SII' : '');
          break;
        case 'RFP':
          if ($scope.motivo == '') {
            UIkit.notify(
              "Debe especificar un motivo de reclamo <a class='notify-action'>[X]</a> ",
              {
                status: 'danger',
                timeout: 2000
              }
            );
            return;
          }
          $scope.nuevoEstadoDocumento = 3;
          if (vincular == 'si') {
            $scope.nuevoEstadoDocumento = 5;
          }
          mensaje = 'Reclamando documento' + (vincular == 'si' ? ' y notificando al SII' : '');
          break;
        default:
          console.log('OPERACION', tipoAccion);
          UIkit.notify("Debe seleccionar un tipo de Rechazo <a class='notify-action'>[X]</a> ", {
            status: 'danger',
            timeout: 2000
          });
          return;
      }
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>" +
          mensaje +
          "<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      if ($scope.masivo == 'si') {
        var objetoAuxiliarPayload = [];
        console.log($scope.documentosSeleccionados);

        for (var i = 0; i < $scope.documentosSeleccionados.length; i++) {
          objetoAuxiliarPayload.push({ febosId: $scope.documentosSeleccionados[i] });
        }

        console.log(objetoAuxiliarPayload);
        FebosAPI.cl_realizar_accion_masiva(
          {},
          {
            accion: 'cl_generar_intercambio_electronico',
            nombre: mensaje,
            metodo: 'SERIE',
            comunes: {
              tipoAccion: tipoAccion,
              vincularAlSii: vincular,
              recinto: tipoAccion == 'ERM' ? $scope.recinto : '',
              motivo:
                tipoAccion == 'RCD' || tipoAccion == 'RFP' || tipoAccion == 'RFT'
                  ? $scope.motivo
                  : ''
            },
            payload: objetoAuxiliarPayload
          },
          true,
          true
        ).then(function() {
          UIkit.notify(
            "Operación efectuada correctamente! Se te avisará por correo cuando termine el proceso completo.<a class='notify-action'>[X]</a> ",
            {
              status: 'success',
              timeout: 2000
            }
          );
        });
      } else {
        FebosAPI.cl_generar_intercambio_electronico(
          {
            febosId: $scope.documentoActual.febosId,
            tipoAccion: tipoAccion,
            vincularAlSii: vincular,
            recinto: tipoAccion == 'ERM' ? $scope.recinto : '',
            motivo:
              tipoAccion == 'RCD' || tipoAccion == 'RFP' || tipoAccion == 'RFT' ? $scope.motivo : ''
          },
          {},
          true,
          true
        ).then(function() {
          for (var i = 0; i < $scope.documentos.length; i++) {
            if ($scope.documentos[i].febosId == $scope.documentoActual.febosId) {
              $scope.documentos[i].metadata.estadoComercial = $scope.estadoComercial(
                $scope.nuevoEstadoDocumento
              );
              break;
            }
          }
          UIkit.notify("Operación efectuada correctamente!<a class='notify-action'>[X]</a> ", {
            status: 'success',
            timeout: 2000
          });
        });
      }
    };
    $scope.textoCargar = 'CARGAR DOCUMENTO';
    $scope.mostrarModalCargaDocumento = function() {
      console.log('modal carga documento');
      $scope.textoCargar = 'CARGAR DOCUMENTO';
      UIkit.modal('#mdCargaDocumento').show();
    };

    function uuid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }

      return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    $scope.cargarDocumento = function() {
      if ($scope.textoCargar != 'CARGAR DOCUMENTO') return;

      $scope.textoCargar = 'Cargando documento...';

      var files = document.getElementById('file').files;
      console.log(files);

      if (files.length > 0) {
        if ($scope.vista == 'honorarios') {
          if (files[0].type === 'application/pdf') {
            var reader = new FileReader();
            reader.readAsDataURL(files[0]);

            console.log(reader);

            reader.onload = function() {
              var base64 = reader.result;
              var documento = base64.split(',');
              var payload = documento[1];
              //console.log(payload);
              var ambiente = FebosUtil.obtenerAmbiente();
              var sufijo = ambiente.charAt(0).toUpperCase() + ambiente.slice(1);
              var req = {
                method: 'POST',
                url:
                  'https://api.febos.cl/' +
                  ambiente +
                  '/empresas/' +
                  SesionFebos().empresa.iut +
                  '/flujos/CargarBH' +
                  sufijo +
                  '/ejecuciones',
                headers: {
                  token: SesionFebos().usuario.token,
                  empresa: SesionFebos().empresa.iut,
                  'Content-Type': 'application/json'
                },
                data: {
                  nombreArchivo: 'Archivo-' + SesionFebos().empresa.iut + '-' + uuid() + '.pdf',
                  payload: payload
                }
              };
              console.log('RESQUEST LOAD DTE', req);
              $http(req).then(
                function(response) {
                  console.log('RESPONSE LOAD DTE', response);
                  $rootScope.ultimoSeguimientoId = response.data.seguimientoId;

                  try {
                    if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                      $scope.textoCargar = 'CARGAR DOCUMENTO';
                      $scope.mensaje_ok = response.data.mensaje;
                      $scope.mensaje_api = response.data.mensajeResultado;
                      $scope.seguimiento = response.data.seguimientoId;
                      UIkit.modal.alert(
                        response.data.mensaje + '<br/>' + response.data.mensajeResultado,
                        { labels: { Ok: 'Ok' } }
                      );
                    } else {
                      $scope.textoCargar = 'CARGAR DOCUMENTO';
                      UIkit.modal.alert(
                        'Ocurrió un error al cargar el documento, por favor reintentar.',
                        { labels: { Ok: 'Entiendo' } }
                      );
                    }
                  } catch (e) {
                    console.log(e);
                    //febosSingleton.error(response.data.codigo, e.message, response.data.seguimientoId);
                    $rootScope.$broadcast('febosError', response);

                    $scope.textoCargar = 'CARGAR DOCUMENTO';
                    UIkit.modal.alert(
                      'Ocurrió un error al cargar el documento, por favor reintentar.'
                    );
                  }
                },
                function(response) {
                  console.log(response);
                  $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
                  febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');

                  $scope.textoCargar = 'CARGAR DOCUMENTO';
                  UIkit.modal.alert(
                    'Ocurrió un error al cargar el documento, por favor reintentar.'
                  );
                }
              );
            };

            reader.onerror = function(error) {
              console.log('Error: ', error);
              febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');

              $scope.textoCargar = 'CARGAR DOCUMENTO';
              UIkit.modal.alert('Ocurrió un error al cargar el documento, por favor reintentar.');
            };
          } else {
            console.log('No soporta el tipo de archivo ' + files[0].type);
            //febosSingleton.error('1', 'No soporta el tipo de archivo' + files[0].type, '');
            UIkit.modal.alert('No soporta el tipo de archivo ' + files[0].type);
            $scope.textoCargar = 'CARGAR DOCUMENTO';
            return false;
          }
        } else if (files[0].type === 'text/xml') {
          var reader = new FileReader();
          reader.readAsDataURL(files[0]);

          console.log(reader);

          reader.onload = function() {
            var base64 = reader.result;
            var documento = base64.split(',');
            var payload = documento[1];
            //console.log(payload);
            var ambiente = FebosUtil.obtenerAmbiente();
            var sufijo = ambiente.charAt(0).toUpperCase() + ambiente.slice(1);
            var req = {
              method: 'POST',
              url:
                'https://api.febos.cl/' +
                ambiente +
                '/empresas/' +
                SesionFebos().empresa.iut +
                '/flujos/RecepcionDte' +
                sufijo +
                '/ejecuciones',
              headers: {
                token: SesionFebos().usuario.token,
                empresa: SesionFebos().empresa.iut,
                'Content-Type': 'application/json'
              },
              data: {
                nombreArchivo: 'Archivo-' + SesionFebos().empresa.iut + '-' + uuid() + '.xml',
                payload: payload
              }
            };
            console.log('RESQUEST LOAD DTE', req);
            $http(req).then(
              function(response) {
                console.log('RESPONSE LOAD DTE', response);
                try {
                  if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                    $scope.textoCargar = 'CARGAR DOCUMENTO';
                    $scope.mensaje_ok = response.data.mensaje;
                    $scope.mensaje_api = response.data.mensajeResultado;
                    $scope.seguimiento = response.data.seguimientoId;
                    UIkit.modal.alert(
                      response.data.mensaje + '<br/>' + response.data.mensajeResultado
                    );
                  } else {
                    $scope.textoCargar = 'CARGAR DOCUMENTO';
                    UIkit.modal.alert(
                      'Ocurrió un error al cargar el documento, por favor reintentar.'
                    );
                  }
                } catch (e) {
                  console.log(e);
                  $rootScope.$broadcast('febosError', response);

                  $scope.textoCargar = 'CARGAR DOCUMENTO';
                  UIkit.modal.alert(
                    'Ocurrió un error al cargar el documento, por favor reintentar.'
                  );
                }
              },
              function(response) {
                console.log(response);
                febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');

                $scope.textoCargar = 'CARGAR DOCUMENTO';
                UIkit.modal.alert('Ocurrió un error al cargar el documento, por favor reintentar.');
              }
            );
          };

          reader.onerror = function(error) {
            console.log('Error: ', error);
            febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');

            $scope.textoCargar = 'CARGAR DOCUMENTO';
            UIkit.modal.alert('Ocurrió un error al cargar el documento, por favor reintentar.');
          };
        } else {
          console.log('No soporta el tipo de archivo ' + files[0].type);
          //febosSingleton.error('1', 'No soporta el tipo de archivo' + files[0].type, '');
          UIkit.modal.alert('No soporta el tipo de archivo' + files[0].type);
          $scope.textoCargar = 'CARGAR DOCUMENTO';
          return false;
        }
      } else {
        //febosSingleton.error('1', 'El documento es obligatorio y en formato XML', '');
        UIkit.modal.alert('El documento es obligatorio y en formato XML');
        $scope.textoCargar = 'CARGAR DOCUMENTO';
        return false;
      }
    };

    $scope.descargarCSV = function() {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Verificando filtros aplicados...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      var filtros = $location.search().filtros;
      var rangos = Object.keys(ComplementoDte.atajosDeRangosDeFecha);
      for (var i = 0; i < rangos.length; i++) {
        var rango = rangos[i];
        if (filtros.includes(rango)) {
          filtros = filtros.replace(
            rango,
            ComplementoDte.atajosDeRangosDeFecha[rango].desde +
              '--' +
              ComplementoDte.atajosDeRangosDeFecha[rango].hasta
          );
          i--;
        }
      }

      // Debe reemplazar en los filtros el operador : (=) por $ (like)
      var reemplazosDeBusquedaLike = [
        'rutCesionario',
        'razonSocialEmisor',
        'razonSocialReceptor',
        'razonSocialCesionario',
        'comentario',
        'lugar'
      ];
      for (var i = 0; i < reemplazosDeBusquedaLike.length; i++) {
        if (filtros.includes(reemplazosDeBusquedaLike[i])) {
          filtros = filtros.replace(
            reemplazosDeBusquedaLike[i] + ':',
            reemplazosDeBusquedaLike[i] + '$'
          );
        }
      }

      var payload = {
        pagina: 1,
        itemsPorPagina: 999999999,
        campos:
          'tipoDocumento,folio,rutEmisor,razonSocialEmisor,rutReceptor,razonSocialReceptor,rutCesionario,razonSocialCesionario,fechaCesion,codigoSii,fechaEmision,fechaRecepcion,fechaRecepcionSii,plazo,estadoComercial,estadoSii,fechaReciboMercaderia,formaDePago,montoTotal,contacto,correoReceptor,codigoSii,fechaCesion,tipo,monto,lugar,comentario,fecha,medio,tpoTraVenta,tpoTranCompra',
        filtros: filtros,
        orden: $location.search().orden
      };
      console.log('TAREA A ENCOLAR', payload);
      var payload64 = btoa(unescape(encodeURIComponent(JSON.stringify(payload))));

      FebosAPI.cl_encolar_tarea_background(
        {
          proceso: 'cl_listar_dte'
        },
        {
          payload: payload64
        },
        false,
        false
      ).then(function(response) {
        $rootScope.blockModal.hide();
        $rootScope.blockModal = UIkit.modal.blockUI(
          "<div class='uk-text-center'>Descargando listado de documentos,<br/>esto podria tardar varios segundos...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
        );
        $timeout(function() {
          $scope.verificarCsv(response.data.tareaId);
        }, 1000);
      });
    };

    $scope.verificarCsv = function(tareaId, funcionArmarCSVCallBack) {
      FebosAPI.cl_consulta_estado_tarea_background(
        {
          tareaId: tareaId
        },
        {},
        false,
        false
      ).then(function(response) {
        switch (response.data.estado) {
          case 2:
            $timeout(function() {
              $scope.verificarCsv(tareaId, funcionArmarCSVCallBack);
            }, 3000);
            break;
          case 3:
            $http.get(response.data.resultado).then(function(data) {
              if (funcionArmarCSVCallBack != undefined) {
                funcionArmarCSVCallBack(data);
              } else {
                $scope.armarCSV(data);
              }
            });
            break;
          case 4:
            var r = {
              codigo: '',
              mensaje: 'Ocurrio un error al intentar exportar el listado de documentos',
              seguimientoId: response.data.seguimientoId
            };
            $rootScope.$broadcast('febosError', r);
        }
      });
    };
    $scope.armarCSV = function(csv) {
      var docs = csv.data.documentos;
      var separador = ';';
      var especiales = 'tipoDocumento,codigoSii,estadoComercial,estadoSii,formaDePago,tipo,medio'.split(
        ','
      );
      var titulos = $scope.titulosCsv();
      var textoCSV = [];
      //poniendo titulos
      for (var i = 0; i < titulos.length; i++) {
        textoCSV.push('"' + titulos[i].titulo + '"');
      }
      textoCSV = textoCSV.join(separador) + '\n';
      for (var i = 0; i < docs.length; i++) {
        var linea = [];
        for (var t = 0; t < titulos.length; t++) {
          if (typeof docs[i][titulos[t].campo] != 'undefined') {
            if (especiales.indexOf(titulos[t].campo) >= 0) {
              //es un campo especial
              switch (titulos[t].campo) {
                case 'tipoDocumento':
                  linea.push('"' + FebosUtil.tipoDocumento(docs[i].tipoDocumento) + '"');
                  break;
                case 'estadoSii':
                  linea.push('"' + $scope.estadoSii(docs[i].estadoSii).descripcion + '"');
                  break;
                case 'estadoComercial':
                  linea.push(
                    '"' + $scope.estadoComercial(docs[i].estadoComercial).descripcion + '"'
                  );
                  break;
                case 'formaDePago':
                  switch (parseInt(docs[i].formaDePago)) {
                    case 1:
                      linea.push('"Contado"');
                      break;
                    case 2:
                      linea.push('"Crédito"');
                      break;
                    case 3:
                      linea.push('"Sin costo"');
                      break;
                    default:
                      linea.push('"No especificado"');
                      break;
                  }
                  break;
                case 'tipo':
                  switch (parseInt(docs[i].tipo)) {
                    case 0:
                      linea.push('"Sin Acción"');
                      break;
                    case 1:
                      linea.push('"En proceso"');
                      break;
                    case 2:
                      linea.push('"Pago enviado"');
                      break;
                    case 3:
                      linea.push('"Pagado"');
                      break;
                    case 4:
                      linea.push('"Cobrado"');
                      break;
                    case 5:
                      linea.push('"Pago no efectuado"');
                      break;
                    default:
                      linea.push('"No especificado"');
                      break;
                  }
                  break;
                case 'medio':
                  switch (parseInt(docs[i].medio)) {
                    case 'CH':
                      linea.push('"Cheque"');
                      break;
                    case 'CF':
                      linea.push('"Cheque a fecha"');
                      break;
                    case 'LT':
                      linea.push('"Letra"');
                      break;
                    case 'EF':
                      linea.push('"Efectivo"');
                      break;
                    case 'PE':
                      linea.push('"Pago a Cta. Cte."');
                      break;
                    case 'TC':
                      linea.push('"Tarjeta de crédito"');
                      break;
                    case 'VV':
                      linea.push('"Vale vista"');
                      break;
                    case 'CO':
                      linea.push('"Confirming"');
                      break;
                    case 'OT':
                      linea.push('"Otro"');
                      break;
                    default:
                      linea.push('"No especificado"');
                      break;
                  }
                  break;
                case 'codigoSii':
                  var desc = null;
                  switch (docs[i].codigoSii) {
                    case 'DOK':
                      desc = '"Documento coincide con los registros del SII"';
                      break;
                    case 'AND':
                      desc = '"Folio anulado"';
                      break;
                    case 'TMD':
                      desc = '"Texto modificado con ND"';
                      break;
                    case 'TMC':
                      desc = '"Texto modificado con NC"';
                      break;
                    case 'MMD':
                      desc = '"Monto modificado con ND"';
                      break;
                    case 'MMC':
                      desc = '"Monto modificado con NC"';
                      break;
                    case 'AND':
                      desc = '"DTE Anulado con ND"';
                      break;
                    case 'ANC':
                      desc = '"DTE Anulado con NC"';
                      break;
                  }
                  linea.push(desc);
                  break;
                default:
                  linea.push('"' + docs[i][titulos[t].campo] + '"');
              }

              if (typeof docs[i][titulos[t + 1].campo] == 'string') {
                linea.push('"' + docs[i][titulos[t + 1].campo] + '"');
              } else {
                linea.push(docs[i][titulos[t + 1].campo]);
              }
              t++;
            } else {
              //es un campo normal
              switch (titulos[t].campo) {
                case 'plazo': {
                  if (
                    ['0', '1', '3'].indexOf(docs[i].estadoComercial.toString()) >= 0 &&
                    docs[i].plazo !== undefined &&
                    docs[i].plazo >= 0 &&
                    ['33', '34', '43'].indexOf(docs[i].tipoDocumento.toString()) >= 0 &&
                    docs[i].formaDePago != 1 &&
                    docs[i].formaDePago != 3
                  ) {
                    linea.push('"' + docs[i][titulos[t].plazo] + '"');
                  } else if (
                    ['0', '1', '3'].indexOf(docs[i].estadoComercial.toString()) >= 0 &&
                    docs[i].plazo !== undefined &&
                    docs[i].plazo < 0 &&
                    ['33', '34', '43'].indexOf(docs[i].tipoDocumento.toString()) >= 0 &&
                    docs[i].formaDePago != 1 &&
                    docs[i].formaDePago != 3
                  ) {
                    linea.push('"Vencido"');
                  } else if (
                    ['2', '4', '5', '6', '7'].indexOf(docs[i].estadoComercial.toString()) >= 0 &&
                    ['33', '34', '43'].indexOf(docs[i].tipoDocumento.toString()) >= 0 &&
                    docs[i].formaDePago != 1 &&
                    docs[i].formaDePago != 3
                  ) {
                    linea.push('"Gestionado"');
                  } else if (
                    ['0', '1', '3'].indexOf(docs[i].estadoComercial.toString()) >= 0 &&
                    docs[i].plazo == undefined &&
                    ['33', '34', '43'].indexOf(docs[i].tipoDocumento.toString()) >= 0 &&
                    docs[i].formaDePago != 1 &&
                    docs[i].formaDePago != 3
                  ) {
                    linea.push('"Falta Información"');
                  } else {
                    linea.push('"No Aplica"');
                  }
                  break;
                }
                default:
                  if (typeof docs[i][titulos[t].campo] == 'string') {
                    linea.push('"' + docs[i][titulos[t].campo] + '"');
                  } else {
                    linea.push(docs[i][titulos[t].campo]);
                  }
              }
            }
          } else {
            linea.push(null);
          }
        }
        textoCSV += linea.join(separador) + '\n';
      }
      $rootScope.blockModal.hide();
      UIkit.notify("Listado de documentos descargado! <a class='notify-action'>[X]</a> ", {
        status: 'success',
        timeout: 2000
      });
      console.log(textoCSV);
      var link = document.createElement('a');
      link.href = 'data:text/csv;charset=utf-8;base64,' + btoa(textoCSV);
      link.download = 'ExportadoFebos.csv';
      document.body.appendChild(link);
      try {
        setTimeout(function() {
          link.click();
        }, 150);
      } catch (e) {}
    };
    $scope.titulosCsv = function() {
      var campos = 'febosId,tipoDocumento,tipoDocumento,folio,rutEmisor,razonSocialEmisor,rutReceptor,razonSocialReceptor,rutCesionario,razonSocialCesionario,fechaCesion,codigoSii,codigoSii,fechaEmision,fechaRecepcion,fechaRecepcionSii,plazo,estadoComercial,estadoComercial,estadoSii,estadoSii,fechaReciboMercaderia,formaDePago,formaDePago,montoTotal,contacto,correoReceptor,fechaCesion,tipo,tipo,monto,lugar,comentario,fecha,medio,medio'.split(
        ','
      );
      var titulos = 'Febos ID,Tipo Documento,Tipo Documento Código,Folio,Rut Emisor,Razón Social Emisor,Rut Receptor,Razón Social Receptor,Rut Cesionario,Razón Social Cesionario,Fecha Cesión,Código SII Prosa,Código SII,Fecha de Emisión,Fecha de Recepción Febos,Fecha de Recepcion SII,Plazo para gestión comercial,Estado Comercial,Estado Comercial código,Estado SII,Estado SII código,Fecha de Recibo de Mercaderías,Forma de Pago,Forma de Pago código,Monto Total,Contacto,Correo Receptor,Fecha de Cesión,Estado de pago proveedor,Estado de pago código,Monto pago proveedor,Lugar de pago proveedor,Comentario de pago proveedor,Fecha de pago proveedor,Medio de pago proveedor,Medio de pago proveedor código'.split(
        ','
      );

      var resultado = [];
      for (var i = 0; i < titulos.length; i++) {
        resultado.push({
          campo: campos[i],
          titulo: titulos[i]
        });
      }
      return resultado;
    };
    $scope.descargarCSVRegistroCompra = function() {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Verificando filtros aplicados...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      var filtros = $location.search().filtros;
      var rangos = Object.keys(ComplementoDte.atajosDeRangosDeFecha);
      for (var i = 0; i < rangos.length; i++) {
        var rango = rangos[i];
        if (filtros.includes(rango)) {
          filtros = filtros.replace(
            rango,
            ComplementoDte.atajosDeRangosDeFecha[rango].desde +
              '--' +
              ComplementoDte.atajosDeRangosDeFecha[rango].hasta
          );
          i--;
        }
      }
      //solo permite  tipoDocumento = 33,34,43,46,56,61
      var filtrosAux = filtros.split('|');
      for (var i = 0; i < filtrosAux.length; i++) {
        if (filtrosAux[i].indexOf('tipoDocumento') >= 0) {
          filtrosAux[i] = 'tipoDocumento:33,34,43,46,56,61';
        }
      }
      filtrosAux.push('tpoTranCompra:1,2,3,4,5,6,7');
      filtros = filtrosAux.join('|');
      // Debe reemplazar en los filtros el operador : (=) por $ (like) tipoDocumento
      var reemplazosDeBusquedaLike = [
        'rutCesionario',
        'razonSocialEmisor',
        'razonSocialReceptor',
        'razonSocialCesionario',
        'comentario',
        'lugar'
      ];
      for (var i = 0; i < reemplazosDeBusquedaLike.length; i++) {
        if (filtros.includes(reemplazosDeBusquedaLike[i])) {
          filtros = filtros.replace(
            reemplazosDeBusquedaLike[i] + ':',
            reemplazosDeBusquedaLike[i] + '$'
          );
        }
      }

      var payload = {
        pagina: 1,
        itemsPorPagina: 999999999,
        campos: 'tipoDocumento,folio,rutEmisor,tpoTranCompra,tpoTranCompraCodIva', //,tpoTraVenta
        filtros: filtros,
        orden: $location.search().orden
      };
      console.log('TAREA A ENCOLAR', payload);
      var payload64 = btoa(unescape(encodeURIComponent(JSON.stringify(payload))));

      FebosAPI.cl_encolar_tarea_background(
        {
          proceso: 'cl_listar_dte'
        },
        {
          payload: payload64
        },
        false,
        false
      ).then(function(response) {
        $rootScope.blockModal.hide();
        $rootScope.blockModal = UIkit.modal.blockUI(
          "<div class='uk-text-center'>Descargando listado de documentos,<br/>esto podria tardar varios segundos...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
        );
        $timeout(function() {
          $scope.verificarCsv(response.data.tareaId, $scope.armarCSVCompra);
        }, 1000);
      });
    };
    $scope.armarCSVCompra = function(data) {
      var docs = data.data.documentos;
      var separador = ';';
      var campos =
        'rutEmisor|Rut-DV' +
        ',tipoDocumento|Codigo_Tipo_Doc' +
        ',folio|Folio_Doc' +
        ',tpoTranCompra|TpoTranCompra' +
        ',tpoTranCompraCodIva|Codigo_IVA_E_Imptos';
      var titulos = $scope.titulosCSVCompra(campos);
      console.log(titulos);
      var textoCSV = [];
      //poniendo titulos
      for (var i = 0; i < titulos.length; i++) {
        textoCSV.push(titulos[i].titulo);
      }
      textoCSV = textoCSV.join(separador) + '\n';

      for (var i = 0; i < docs.length; i++) {
        var linea = $scope.obtenerLineaCompraCsv(docs[i], titulos);
        textoCSV += linea.join(separador) + '\n';
      }
      console.log(textoCSV);

      $rootScope.blockModal.hide();
      UIkit.notify("Listado de documentos descargado! <a class='notify-action'>[X]</a> ", {
        status: 'success',
        timeout: 2000
      });
      var link = document.createElement('a');
      link.href = 'data:text/csv;charset=utf-8;base64,' + btoa(textoCSV);
      link.download = 'ExportadoFebosRegistroCompra.csv';
      document.body.appendChild(link);
      try {
        setTimeout(function() {
          link.click();
        }, 150);
      } catch (e) {
        console.error(e);
      }
    };
    $scope.titulosCSVCompra = function(campos) {
      var resultado = [];
      for (var i = 0; i < campos.split(',').length; i++) {
        resultado.push({
          campo: campos.split(',')[i].split('|')[0],
          titulo: campos.split(',')[i].split('|')[1]
        });
      }
      return resultado;
    };
    $scope.obtenerLineaCompraCsv = function(doc, titulos) {
      var especiales = 'tpoTranCompra'.split(',');
      var linea = [];

      for (var t = 0; t < titulos.length; t++) {
        if (especiales.indexOf(titulos[t].campo) >= 0) {
          //es un campo especial
          switch (titulos[t].campo) {
            case 'tpoTranCompra': {
              linea.push(doc[titulos[t].campo] || '');
              break;
            }
            default: {
              linea.push(doc[titulos[t].campo] || '');
              break;
            }
          }
        } else if (typeof doc[titulos[t].campo] != 'undefined') {
          linea.push(doc[titulos[t].campo]);
        } else {
          linea.push('');
        }
      }
      return linea;
    };

    //agregar meta data a cada documento sobre sus estados (colores, descripciones e iconos)
    $scope.documentos = Datos.documentos;
    $scope.sumario = { '33': 0, '34': 0, '61': 0, '56': 0, rechazadas: 0 };
    for (var i = 0; i < $scope.documentos.length; i++) {
      $scope.documentos[i].metadata = {
        estadoSii: $scope.estadoSii($scope.documentos[i].estadoSii),
        estadoComercial: $scope.estadoComercial($scope.documentos[i].estadoComercial)
      };
      if (parseInt($scope.documentos[i].estadoComercial) == 4) {
        $scope.sumario['rechazadas'] += parseInt($scope.documentos[i].montoTotal);
      }

      $scope.sumario['' + $scope.documentos[i].tipoDocumento] += parseInt(
        $scope.documentos[i].montoTotal
      );
    }

    $scope.modalConsultarHistorialSii = function(documento) {
      var modal = UIkit.modal('#consultar_historial_sii');
      $scope.documentoActual = documento;
      $scope.masivo = 'no';
      modal.show();
    };

    $scope.modalConsultarHistorialSiiMasivo = function(documentosSeleccionados) {
      var modal = UIkit.modal('#consultar_historial_sii');
      $scope.documentosSeleccionados = documentosSeleccionados;
      $scope.masivo = 'si';
      modal.show();
    };

    $scope.consultarHistorialSii = function() {
      UIkit.modal('#consultar_historial_sii').hide();
      //var resp={"codigoSii":15,"descripcionSii":"Listado de eventos del documento","eventos":[{"codEvento":"CED","descEvento":"DTE Cedido","rutResponsable":"14172951","dvResponsable":"9","fechaEvento":"25-01-2018 11:00:09"},{"codEvento":"RCD","descEvento":"Reclama Contenido del Documento","rutResponsable":"61704000","dvResponsable":"K","fechaEvento":"30-01-2018 18:37:11"},{"codEvento":"NCA","descEvento":"Recepción de NC de Anulación que Referencia Documento","rutResponsable":"14172951","dvResponsable":"9","fechaEvento":"07-02-2018 14:55:39"}],"codigo":10,"mensaje":"Acción realizada satisfactoriamente","seguimientoId":"e38478df8d8e10465759cc8-e4dc0ae1a230","duracion":10653,"hora":"2018-05-20 16:47:44.125"};
      console.log($scope.documentoActual);
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Consultando documento el SII..." +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_sii_consulta_historial_eventos(
        { febosId: $scope.documentoActual.febosId },
        {},
        true,
        true
      ).then(function(response) {
        $scope.documentoActual.eventos_sii = response.data.eventos;
        console.log('Documento actual', $scope.documentoActual);
        UIkit.modal('#historial_sii').show();
      });
    };

    $scope.consultarHistorialSiiMasivo = function() {
      UIkit.modal('#consultar_historial_sii').hide();
      //var resp={"codigoSii":15,"descripcionSii":"Listado de eventos del documento","eventos":[{"codEvento":"CED","descEvento":"DTE Cedido","rutResponsable":"14172951","dvResponsable":"9","fechaEvento":"25-01-2018 11:00:09"},{"codEvento":"RCD","descEvento":"Reclama Contenido del Documento","rutResponsable":"61704000","dvResponsable":"K","fechaEvento":"30-01-2018 18:37:11"},{"codEvento":"NCA","descEvento":"Recepción de NC de Anulación que Referencia Documento","rutResponsable":"14172951","dvResponsable":"9","fechaEvento":"07-02-2018 14:55:39"}],"codigo":10,"mensaje":"Acción realizada satisfactoriamente","seguimientoId":"e38478df8d8e10465759cc8-e4dc0ae1a230","duracion":10653,"hora":"2018-05-20 16:47:44.125"};
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Consultando documento el SII..." +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      var objetoAuxiliarPayload = [];
      console.log($scope.documentosSeleccionados);

      for (var i = 0; i < $scope.documentosSeleccionados.length; i++) {
        objetoAuxiliarPayload.push({
          febosId: $scope.documentosSeleccionados[i]
        });
      }
      console.log(objetoAuxiliarPayload);
      console.log($scope.estadoPagoTransicion);
      FebosAPI.cl_realizar_accion_masiva(
        {},
        {
          accion: 'cl_sii_consulta_historial_eventos',
          nombre: 'Consulta historial de eventos',
          comunes: {},
          metodo: 'SERIE',
          payload: objetoAuxiliarPayload
        },
        true,
        true
      ).then(function() {
        try {
          UIkit.notify(
            "Documentos listos para actualizar. Se te confirmará por correo! <a class='notify-action'>[X]</a> ",
            {
              status: 'success',
              timeout: 2000
            }
          );
        } catch (e) {
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId
          );
        }
      });
    };

    //Consultar fecha de recepcion en el SII, se actualiza en DB y en el arreglo de documentos que estan siendo mostrados
    $scope.modalConsultarFecha = function(documento) {
      var modal = UIkit.modal('#consultar_fecha_sii');
      $scope.documentoActual = documento;
      $scope.masivo = 'no';
      modal.show();
    };

    $scope.modalConsultarFechaMasivo = function(documentosSeleccionados) {
      var modal = UIkit.modal('#consultar_fecha_sii');
      $scope.documentosSeleccionados = documentosSeleccionados;
      $scope.masivo = 'si';
      modal.show();
    };

    $scope.consultarFechaRecepcionSii = function(documento) {
      UIkit.modal('#consultar_fecha_sii').hide();
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Consultando fecha de recepción en el SII..." +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      FebosAPI.cl_consulta_fecha_recepcion_sii(
        {
          febosId: documento.febosId,
          rutEmisor: documento.rutEmisor,
          tipoDte: documento.tipoDocumento,
          folio: documento.folio
        },
        {},
        true,
        true
      ).then(function(response) {
        console.log(response);

        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}

        $scope.cargando = false;

        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            $scope.fechaRecepcionSii = response.data.fechaRecepcion;
            //Actualizo la fecha en la variable de ese mismo documento, para evitar que la vista quede desactualizada y evitar que se deba refrescar
            for (var i = 0; i < $scope.documentos.length; i++) {
              if ($scope.documentos[i].febosId == $scope.documentoActual.febosId) {
                $scope.documentos[i].fechaRecepcionSii = response.data.fechaRecepcion;
                break;
              }
            }
            //Recarga vista ya que la consulta de la fecha de recepcion actualiza en la base de datos
            $scope.textoRespuesta =
              'La ' +
              $scope.tipoDocumento(documento.tipoDocumento) +
              ', folio #' +
              documento.folio +
              ' fue recibida por el SII en ';
            var modal = UIkit.modal('#mostrar_fecha_recepcion');
            modal.show();
          } else {
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        } catch (e) {
          console.log(e);
          SesionFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
        }
      });
    };

    $scope.consultarFechaRecepcionSiiMasivo = function(documentosSeleccionados) {
      UIkit.modal('#consultar_fecha_sii').hide();
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Consultando fecha de recepción en el SII..." +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      var objetoAuxiliarPayload = [];
      console.log($scope.documentosSeleccionados);

      for (var i = 0; i < $scope.documentosSeleccionados.length; i++) {
        objetoAuxiliarPayload.push({
          febosId: $scope.documentosSeleccionados[i]
        });
      }
      console.log(objetoAuxiliarPayload);
      console.log($scope.estadoPagoTransicion);
      FebosAPI.cl_realizar_accion_masiva(
        {},
        {
          accion: 'cl_consulta_fecha_recepcion_sii',
          nombre: 'Consultar fecha de Recepción en el SII',
          comunes: {},
          metodo: 'SERIE',
          payload: objetoAuxiliarPayload
        },
        true,
        true
      ).then(function() {
        try {
          UIkit.notify(
            "Documentos listos para actualizar. Se te confirmará por correo! <a class='notify-action'>[X]</a> ",
            {
              status: 'success',
              timeout: 2000
            }
          );
        } catch (e) {
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId
          );
        }
      });
    };

    $scope.formularioEdicionPago = false;
    $scope.selectize_config_pagos = {
      maxItems: 1,
      valueField: 'valor',
      labelField: 'nombre',
      searchField: 'nombre',
      create: false,
      dropdownParent: 'body',
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.nombre) + '</div>';
        }
        //,
        //option_create: function (data, escape) {
        //    return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
        // }
      }
    };
    $scope.estadoPagoActual = {
      tipo: 0,
      monto: 0,
      lugar: '',
      comentario: '',
      medio: '',
      fecha: ''
    };
    $scope.estadoPagoTransicion = {
      tipo: 0,
      monto: 0,
      lugar: '',
      comentario: '',
      medio: '',
      fecha: ''
    };
    $scope.estadosDePago = [
      { valor: '0', nombre: 'Sin gestión' },
      { valor: '1', nombre: 'En vía de pago' },
      { valor: '2', nombre: 'Pago enviado' },
      { valor: '3', nombre: 'Pagado' },
      { valor: '4', nombre: 'Cobrado' },
      { valor: '5', nombre: 'No pagado' }
    ];
    $scope.mediosDePago = [
      { valor: '', nombre: 'Sin gestión' },
      { valor: 'CH', nombre: 'Cheque' },
      { valor: 'CF', nombre: 'Cheque a fecha' },
      { valor: 'LT', nombre: 'Letra' },
      { valor: 'EF', nombre: 'Efectivo' },
      { valor: 'PE', nombre: 'Pago a Cta. Cte.' },
      { valor: 'TC', nombre: 'Tarjeta de crédito' },
      { valor: 'VV', nombre: 'Vale vista' },
      { valor: 'CO', nombre: 'Confirming' },
      { valor: 'OT', nombre: 'Otro' }
    ];

    $scope.tipoTransaccion = { compra_tipo_iva_activo: false };
    $scope.selectize_config_tipo_transaccion_compra = {
      maxItems: 1,
      valueField: 'valor',
      labelField: 'nombre',
      searchField: 'nombre',
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.nombre) + '</div>';
        }
      },
      onChange: function(value) {
        var tipoValor = [];
        for (var i = 0; i < $scope.tipoTransaccionCompra.length; i++) {
          if ($scope.tipoTransaccionCompra[i]['valor'] == value) {
            console.log('valor ' + value + '   ==  ' + $scope.tipoTransaccionCompra[i]['valor']);
            tipoValor = $scope.tipoTransaccionCompra[i];
            break;
          }
        }
        console.log('tipoValor', JSON.stringify(tipoValor));
        $scope.tipoTransaccionCompraIva = tipoValor['cod_iva'] || [];
        setTimeout(function() {
          $scope.$apply();
          console.log('Tipo iva ' + JSON.stringify($scope.tipoTransaccionCompraIva));
          try {
            console.log(
              '$scope.tipoTransaccion.compra_tipo_iva_activo ',
              $scope.tipoTransaccion.compra_tipo_iva_activo
            );
            if (!$scope.tipoTransaccion.compra_tipo_iva_activo) {
              var usaPrimero =
                $scope.tipoTransaccion.compra_tipo_iva != undefined &&
                new String($scope.tipoTransaccion.compra_tipo_iva).trim().length > 0;
              // //console.log("usaPrimero", usaPrimero);
              // //console.log("Primero", $scope.tipoTransaccion.compra_tipo_iva);
              // //console.log("segundo", tipoValor['cod_iva'][0]['valor']);

              $scope.tipoTransaccion.compra_tipo_iva = usaPrimero
                ? $scope.tipoTransaccion.compra_tipo_iva
                : tipoValor['cod_iva'][0]['valor'];
            } else {
              var usaPrimero =
                $scope.tipoTransaccion.compra_tipo_iva != undefined &&
                new String($scope.tipoTransaccion.compra_tipo_iva).trim().length > 0;
              // //console.log("usaPrimero", usaPrimero);
              // //console.log("Primero", $scope.tipoTransaccion.compra_tipo_iva);
              // //console.log("segundo", tipoValor['cod_iva'][0]['valor'])
              $scope.tipoTransaccion.compra_tipo_iva = usaPrimero
                ? $scope.tipoTransaccion.compra_tipo_iva
                : tipoValor['cod_iva'][0]['valor'];
            }
          } catch (e) {
            console.log('cod_iva error ' + e.message);
            // $scope.tipoTransaccionCompraIva = [];
          }
          console.log(
            '$scope.tipoTransaccion.compra_tipo_iva',
            $scope.tipoTransaccion.compra_tipo_iva
          );
          $scope.tipoTransaccion.compra_tipo_iva_activo = true;
        }, 400);
      }
    };
    $scope.selectize_config_tipo_transaccion_compra_iva = {
      maxItems: 1,
      valueField: 'valor',
      labelField: 'nombre',
      searchField: 'nombre',
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.nombre) + '</div>';
        }
      }
    };
    $scope.tipoTransaccionCompra = [
      { valor: '1', nombre: 'Del Giro', cod_iva: [{ valor: 1, nombre: '1. Del giro' }] },
      {
        valor: '2',
        nombre: 'Supermercados',
        cod_iva: [{ valor: 1, nombre: '1. Del giro' }, { valor: 2, nombre: '2. Uso Común' }]
      },
      {
        valor: '3',
        nombre: 'Bien Raíz',
        cod_iva: [{ valor: 1, nombre: '1. Del giro' }, { valor: 2, nombre: '2. Uso Común' }]
      },
      {
        valor: '4',
        nombre: 'Activo Fijo',
        cod_iva: [{ valor: 1, nombre: '1. Del giro' }, { valor: 2, '2. nombre': 'Uso Común' }]
      },
      { valor: '5', nombre: 'IVA Uso Común', cod_iva: [{ valor: 2, nombre: '2. Uso Común' }] },
      {
        valor: '6',
        nombre: 'Sin derecho',
        cod_iva: [
          {
            valor: 1,
            nombre: '1. Compras destinadas a IVA a generar operaciones no gravados o exentas'
          },
          { valor: 2, nombre: '2. Facturas de proveedores registradas   fuera de plazo' },
          { valor: 3, nombre: '3. Gastos rechazados' },
          { valor: 4, nombre: '4. Entregas gratuitas recibidas' },
          { valor: 9, nombre: '9. Otros' }
        ]
      },
      { valor: '7', nombre: 'No Inculir', cod_iva: [{ valor: 9, nombre: '9. Otros' }] }
    ];
    $scope.selectize_config_tipo_transaccion_venta = {
      maxItems: 1,
      valueField: 'value',
      labelField: 'nombre',
      create: false,
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.nombre) + '</div>';
        }
      }
    };
    $scope.tipoTransaccionVenta = [
      { value: '1', nombre: 'Ventas del Giro' },
      { value: '2', nombre: 'Venta Bien Raíz' },
      { value: '3', nombre: 'Venta Activo Fijo' }
    ];
    $scope.tipoTransaccionCompraIva = [];
    $scope.popUpTipoVenta = function(masivo, documento) {
      if (masivo == 'si') {
        $scope.tipoTransaccion.venta = '';
        $scope.documentosSeleccionados = documento;
      } else {
        var aux = typeof documento['tpoTraVenta'] !== 'undefined';
        $scope.tipoTransaccion.venta = aux ? documento['tpoTraVenta'] : '';
        $scope.documentoActual = documento;
      }
      $scope.masivo = masivo;
      UIkit.modal('#mdlRegistroCompraVenta').show();
    };
    $scope.popUpTipoVentaConfirmar = function() {
      var req = {
        tipoTransaccionCompra: 0,
        tipoTransaccionVenta: $scope.tipoTransaccion.venta
      };
      if ($scope.masivo != 'si') {
        $rootScope.blockModal = UIkit.modal.blockUI(
          "<div class='uk-text-center'>Realizando Modificación<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
        );
        req['febosId'] = $scope.documentoActual.febosId;
        console.log('Solicitud simple ' + JSON.stringify(req));
        FebosAPI.cl_actualizar_compra_venta({}, req, true, true).then(function() {
          $scope.documentoActual['tpoTraVenta'] = $scope.tipoTransaccion.venta;
          console.log('REALIZADO OK');
          UIkit.modal('#mdlRegistroCompraVenta').hide();
          UIkit.notify('Operación efectuada correctamente!', { status: 'success', timeout: 3000 });
        });
      } else {
        console.log('Solicitud multiple ' + JSON.stringify(req));
        $rootScope.blockModal = UIkit.modal.blockUI(
          "<div class='uk-text-center'>Ejecuntando Acción Masiva...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
        );
        var objetoAuxiliarPayload = [];
        for (var i = 0; i < $scope.documentosSeleccionados.length; i++) {
          objetoAuxiliarPayload.push({ febosId: $scope.documentosSeleccionados[i] });
        }
        FebosAPI.cl_realizar_accion_masiva(
          {},
          {
            accion: 'cl_actualizar_compra_venta',
            nombre: 'Modificación Tipo Transaccion Venta',
            comunes: req,
            metodo: 'SERIE',
            payload: objetoAuxiliarPayload
          },
          true,
          true
        ).then(function() {
          console.log('REALIZADO OK');
          UIkit.modal('#mdlRegistroCompraVenta').hide();
          UIkit.notify(
            "Operación efectuada correctamente! Se te avisará por correo cuando termine el proceso completo.<a class='notify-action'>[X]</a> ",
            {
              status: 'success',
              timeout: 3000
            }
          );
        });
      }
    };
    $scope.popUpTipoCompra = function(masivo, documento) {
      $scope.tipoTransaccion.compra_tipo_iva_activo = false;
      if (masivo == 'si') {
        $scope.tipoTransaccion.compra = '';
        $scope.tipoTransaccion.compra_tipo_iva = '';
        $scope.documentosSeleccionados = documento;
      } else {
        var tipoValor = [];
        for (var i = 0; i < $scope.tipoTransaccionCompra.length; i++) {
          if ($scope.tipoTransaccionCompra[i]['valor'] == (documento['tpoTranCompra'] || '0')) {
            console.log(
              'valor ' +
                (documento['tpoTranCompra'] || '0') +
                '   ==  ' +
                $scope.tipoTransaccionCompra[i]['valor']
            );
            tipoValor = $scope.tipoTransaccionCompra[i];
            break;
          }
        }
        console.log('tipoValor', JSON.stringify(tipoValor));
        $scope.tipoTransaccionCompraIva = tipoValor['cod_iva'] || [];
        //
        $scope.tipoTransaccion.compra_tipo_iva = documento['tpoTranCompraCodIva'] || '';
        $scope.tipoTransaccion.compra = documento['tpoTranCompra'] || '';
        $scope.documentoActual = documento;
      }
      $scope.masivo = masivo;
      UIkit2.modal('#mdlRegistroCompraVenta').show();
      $window.document.getElementById('tipo_compra_actual').blur();
    };
    $scope.popUpTipoCompraConfirmar = function() {
      var req = {
        tipoTransaccionCompra: $scope.tipoTransaccion.compra,
        tipoTransaccionCompraCodIva: $scope.tipoTransaccion.compra_tipo_iva,
        tipoTransaccionVenta: 0
      };
      if ($scope.masivo != 'si') {
        $rootScope.blockModal = UIkit.modal.blockUI(
          "<div class='uk-text-center'>Realizando Modificación<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
        );

        req['febosId'] = $scope.documentoActual.febosId;
        console.log('Solicitud simple ' + JSON.stringify(req));
        FebosAPI.cl_actualizar_compra_venta({}, req, true, true).then(function() {
          $scope.documentoActual['tpoTranCompra'] = $scope.tipoTransaccion.compra;
          $scope.documentoActual['tpoTranCompraCodIva'] = $scope.tipoTransaccion.compra_tipo_iva;
          console.log('REALIZADO OK');
          UIkit.modal('#mdlRegistroCompraVenta').hide();
          UIkit.notify('Operación efectuada correctamente!', { status: 'success', timeout: 3000 });
        });
      } else {
        console.log('Solicitud multiple ' + JSON.stringify(req));
        $rootScope.blockModal = UIkit.modal.blockUI(
          "<div class='uk-text-center'>Ejecuntando Acción Masiva...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
        );
        var objetoAuxiliarPayload = [];
        for (var i = 0; i < $scope.documentosSeleccionados.length; i++) {
          objetoAuxiliarPayload.push({ febosId: $scope.documentosSeleccionados[i] });
        }
        FebosAPI.cl_realizar_accion_masiva(
          {},
          {
            accion: 'cl_actualizar_compra_venta',
            nombre: 'Modificación Tipo Transaccion Compra',
            comunes: req,
            metodo: 'SERIE',
            payload: objetoAuxiliarPayload
          },
          true,
          true
        ).then(function() {
          console.log('REALIZADO OK');
          UIkit.modal('#mdlRegistroCompraVenta').hide();
          UIkit.notify(
            "Operación efectuada correctamente! Se te avisará por correo cuando termine el proceso completo.<a class='notify-action'>[X]</a> ",
            {
              status: 'success',
              timeout: 3000
            }
          );
        });
      }
    };
    var modalInfoPago = UIkit.modal('#info_pago');
    $scope.modalInfoPago = function(documento) {
      $scope.formularioEdicionPago = false;
      $scope.estadoPagoActual = {
        tipo: documento.tipo,
        monto: documento.monto,
        lugar: documento.lugar,
        comentario: documento.comentario,
        medio: documento.medio,
        fecha: documento.fecha
      };
      $scope.masivo = 'no';
      $scope.documentoActual = documento;
      modalInfoPago.show();
    };
    $scope.popUpInfoPagoMasivo = function(documentosSeleccionados) {
      $scope.formularioEdicionPago = true;
      $scope.estadoPagoTransicion = {
        tipo: '',
        monto: '',
        lugar: '',
        comentario: '',
        medio: '',
        fecha: ''
      };
      $scope.masivo = 'si';
      $scope.documentosSeleccionados = documentosSeleccionados;
      modalInfoPago.show();
    };
    $scope.editarInfoPago = function() {
      $scope.estadoPagoTransicion = {
        tipo: $scope.estadoPagoActual.tipo != undefined ? $scope.estadoPagoActual.tipo : '0',
        monto: $scope.estadoPagoActual.monto != undefined ? $scope.estadoPagoActual.monto : '',
        lugar: $scope.estadoPagoActual.lugar != undefined ? $scope.estadoPagoActual.lugar : '',
        comentario:
          $scope.estadoPagoActual.comentario != undefined ? $scope.estadoPagoActual.comentario : '',
        medio: $scope.estadoPagoActual.medio != undefined ? $scope.estadoPagoActual.medio : '',
        fecha: $scope.estadoPagoActual.fecha != undefined ? $scope.estadoPagoActual.fecha : ''
      };
      $scope.formularioEdicionPago = true;
    };
    $scope.actualizarInfoPago = function() {
      modalInfoPago.hide();
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Actualizando estado de pago" +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      if ($scope.masivo == 'si') {
        var objetoAuxiliarPayload = [];
        console.log($scope.documentosSeleccionados);

        for (var i = 0; i < $scope.documentosSeleccionados.length; i++) {
          objetoAuxiliarPayload.push({
            febosId: $scope.documentosSeleccionados[i]
          });
        }

        console.log(objetoAuxiliarPayload);
        console.log($scope.estadoPagoTransicion);
        FebosAPI.cl_realizar_accion_masiva(
          {},
          {
            accion: 'cl_crea_info_pago',
            nombre: 'Modifica Info de Pago Masiva',
            comunes: $scope.estadoPagoTransicion,
            metodo: 'SERIE',
            payload: objetoAuxiliarPayload
          },
          true,
          true
        ).then(function(response) {
          try {
            UIkit.notify(
              "Estado de pago listo para actualizar. Se te confirmará por correo! <a class='notify-action'>[X]</a> ",
              {
                status: 'success',
                timeout: 2000
              }
            );
          } catch (e) {
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        });
      } else {
        console.log($scope.documentoActual.febosId);
        console.log($scope.estadoPagoTransicion);
        FebosAPI.cl_crea_info_pago(
          { febosId: $scope.documentoActual.febosId },
          $scope.estadoPagoTransicion,
          true,
          true
        ).then(function(response) {
          for (var i = 0; i < $scope.documentos.length; i++) {
            if ($scope.documentos[i].febosId == $scope.documentoActual.febosId) {
              $scope.documentos[i].tipo = $scope.estadoPagoTransicion.tipo;
              $scope.documentos[i].monto = $scope.estadoPagoTransicion.monto;
              $scope.documentos[i].lugar = $scope.estadoPagoTransicion.lugar;
              $scope.documentos[i].comentario = $scope.estadoPagoTransicion.comentario;
              $scope.documentos[i].medio = $scope.estadoPagoTransicion.medio;
              $scope.documentos[i].fecha = $scope.estadoPagoTransicion.fecha;
              break;
            }
          }
          UIkit.notify("Estado de pago actualizado <a class='notify-action'>[X]</a> ", {
            status: 'success',
            timeout: 2000
          });
        });
      }
    };

    $scope.modalEnviarDte = UIkit.modal('#mdlEnviarDte');

    $scope.popUpEnviarDte = function(documento) {
      $scope.documentoActual = documento;
      $scope.destinatario = '';
      $scope.copias = '';
      $scope.recibirCopia = 'no';
      $scope.mensaje = '';
      $scope.adjuntarXML = 'si';
      $scope.adjuntarPDF = 'si';
      $scope.cargando = 'no';
      $scope.masivo = 'no';
      $scope.modalEnviarDte.show();
    };
    $scope.popUpEnviarDteMasivo = function(documentosSeleccionados) {
      $scope.documentosSeleccionados = documentosSeleccionados;
      $scope.destinatario = '';
      $scope.copias = '';
      $scope.recibirCopia = 'no';
      $scope.mensaje = '';
      $scope.adjuntarXML = 'si';
      $scope.adjuntarPDF = 'si';
      $scope.cargando = 'no';
      $scope.masivo = 'si';
      $scope.modalEnviarDte.show();
    };

    $scope.enviarDte = function() {
      $scope.cargando = 'si';
      if ($scope.masivo == 'si') {
        var objetoAuxiliarPayload = [];
        console.log($scope.documentosSeleccionados);

        for (var i = 0; i < $scope.documentosSeleccionados.length; i++) {
          objetoAuxiliarPayload.push({ febosId: $scope.documentosSeleccionados[i] });
        }

        console.log(objetoAuxiliarPayload);
        FebosAPI.cl_realizar_accion_masiva(
          {},
          {
            accion: 'cl_correo_envio_dte',
            nombre: 'Envío de documentos DTE por correo',
            metodo: 'SERIE',
            comunes: {
              destinatario: $scope.destinatario,
              copias: $scope.copias,
              recibirCopia: $scope.recibirCopia,
              mensaje: btoa($scope.mensaje),
              adjuntarXml: $scope.adjuntarXML,
              adjuntarPdf: $scope.adjuntarPDF
            },
            payload: objetoAuxiliarPayload
          },
          true,
          true
        ).then(function(response) {
          $scope.cargando = 'no';
          try {
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
              UIkit.modal('#mdlEnviarDte').hide();
              UIkit.notify(
                "Operación efectuada correctamente! Se te avisará por correo cuando termine la acción.<a class='notify-action'>[X]</a> ",
                {
                  status: 'success',
                  timeout: 2000
                }
              );
            } else {
              SesionFebos().error(
                response.data.codigo,
                response.data.mensaje,
                response.data.seguimientoId
              );
            }
          } catch (e) {
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        });
      } else {
        FebosAPI.cl_correo_envio_dte(
          {
            febosId: $scope.documentoActual.febosId
          },
          {
            destinatario: $scope.destinatario,
            copias: $scope.copias,
            recibirCopia: $scope.recibirCopia,
            mensaje: btoa($scope.mensaje),
            adjuntarXml: $scope.adjuntarXML,
            adjuntarPdf: $scope.adjuntarPDF
          },
          true,
          false
        ).then(function(response) {
          console.log(response);
          try {
            $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
          } catch (e) {}

          //$rootScope.blockModal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Enviando bits a destino...<br/><img width="30"class=\'uk-margin-top\' src=\'assets/img/logos/FEBOS_LOADER.svg\' alt=\'\'>')

          $scope.cargando = 'no';
          try {
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
              UIkit.modal('#mdlEnviarDte').hide();
              UIkit.notify("Documento enviado!<a class='notify-action'>[X]</a> ", {
                status: 'success',
                timeout: 3000
              });
            } else {
              SesionFebos().error(
                response.data.codigo,
                response.data.mensaje,
                response.data.seguimientoId
              );
            }
          } catch (e) {
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        });
      }
    };

    var $crear_card = $('#crear_card'),
      $crear_form = $('#crear_form'),
      $crear_error = $('#crear_error'),
      $crear_ok = $('#crear_ok');
    var $crear_cardOC = $('#crear_cardOC'),
      $crear_formOC = $('#crear_formOC'),
      $crear_errorOC = $('#crear_errorOC'),
      $crear_okOC = $('#crear_okOC');
    var $crear_cardHES = $('#crear_cardHES'),
      $crear_formHES = $('#crear_formHES'),
      $crear_errorHES = $('#crear_errorHES'),
      $crear_okHES = $('#crear_okHES');
    var $crear_cardRM = $('#crear_cardRM'),
      $crear_formRM = $('#crear_formRM'),
      $crear_errorRM = $('#crear_errorRM'),
      $crear_okRM = $('#crear_okRM');
    //var $crear_ok_show = $('#crear_ok_show'), $crear_ok_show = $('#crear_ok_show'), $crear_errorRM = $('#crear_errorRM'), $crear_okRM = $('#crear_okRM');

    $scope.modalCargaHes = function(documento) {
      console.log('modal carga HES');
      $scope.cargando.l = true;
      $scope.textoCargarHES = 'CARGAR DOCUMENTO';
      $scope.documentoActual = documento;
      $scope.cargando.l = false;

      $scope.adjunto = {
        folioRef: '',
        razonRef: '',
        fechaRef: ''
      };

      UIkit.modal('#mdFrmCargaHes').show();
    };

    $scope.backToCrear = function($event) {
      $event.preventDefault();
      utils.card_show_hide($crear_form, undefined, crear_form_show, undefined);
      utils.card_show_hide($crear_formOC, undefined, crear_form_show, undefined);
      utils.card_show_hide($crear_formHES, undefined, crear_form_show, undefined);
      utils.card_show_hide($crear_formRM, undefined, crear_form_show, undefined);
      //utils.card_show_hide($crear_formOC, undefined, crear_ok_show, undefined);

      $scope.textoDetalle = 'DETALLES';
      $scope.textoVerPdf = 'PDF';
      $scope.textoVerXml = 'XML';
      $scope.textoOrCom = 'OC';
      $scope.textoBitacora = 'BITACORA';
    };

    $scope.cargarHes = function() {
      if ($scope.textoCargarHES != 'CARGAR DOCUMENTO') return;

      $scope.textoCargarHES = 'Cargando documento...';

      var filesHES = document.getElementById('fileHES').files;

      if (filesHES.length > 0) {
        if (filesHES[0].type === 'text/xml' || filesHES[0].type === 'application/pdf') {
          var reader = new FileReader();
          reader.readAsDataURL(filesHES[0]);

          reader.onload = function() {
            var data = reader.result;
            var payload = data.replace(/^[^,]*,/, '');

            FebosAPI.cl_adjuntar_documento(
              {
                febosId: $scope.documentoActual.febosId,
                token: SesionFebos().usuario.token,
                empresa: SesionFebos().empresa.iut,
                grupo: SesionFebos().grupo.id,
                debug: '',
                simular: ''
              },
              {
                tipo: '2',
                nombreArchivo: filesHES[0].name,
                tipoOrigen: '2',
                payload: payload,
                folioRef: document.getElementById('folioRef').value,
                fechaRef: document.getElementById('fechaRef').value,
                razonRef: document.getElementById('razonRef').value
              },
              true,
              false
            ).then(function(response) {
              console.log(response);
              try {
                if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                  $scope.cargando = false;
                  $scope.textoCargarHES = 'CARGAR DOCUMENTO';
                  $scope.mensaje_okHES = response.data.mensaje;

                  UIkit.modal('#mdFrmCargaHes').hide();

                  UIkit.modal.alert(
                    '<div class=\'uk-text-center\'><h2 class="heading_b uk-text-success">' +
                      'El documento ha sido adjunto satisfactoriamente al documento</h2><p>' +
                      $scope.mensaje_okHES +
                      '</p>'
                  );

                  utils.card_show_hide($crear_formHES, undefined, crear_okHES, undefined);
                } else {
                  SesionFebos().error(
                    response.data.codigo,
                    response.data.mensaje,
                    response.data.seguimientoId
                  );
                }
              } catch (e) {
                console.log(e);
                SesionFebos().error(
                  response.data.codigo,
                  response.data.mensaje,
                  response.data.seguimientoId
                );
              }
            });
          };
          reader.onerror = function(error) {
            console.log('Error: ', error);
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          };
        } else {
          console.log('No soporta el tipo de archivo ' + filesHES[0].type);
          //febosSingleton.error('1', 'No soporta el tipo de archivo' + filesHES[0].type, '');
          alert('No soporta el tipo de archivo' + filesHES[0].type);
          $scope.textoCargarHES = 'CARGAR DOCUMENTO';
          return false;
        }
      } else {
        //febosSingleton.error('1', 'El documento es obligatorio y en formato XML o PDF', '');
        alert('El documento es obligatorio y en formato XML o PDF');
        $scope.textoCargarHES = 'CARGAR DOCUMENTO';
        return false;
      }
    };

    $scope.modalCargaRecepcionMateriales = function(documento) {
      console.log('modal carga recepción de materiales');
      $scope.cargando.m = true;
      $scope.textoCargarRM = 'CARGAR DOCUMENTO';
      $scope.documentoActual = documento;

      $scope.adjunto = {
        folioRef: '',
        razonRef: '',
        fechaRef: ''
      };

      $scope.cargando.m = false;
      UIkit.modal('#mdFrmCargaRecepcionMateriales').show();
    };

    $scope.cargarRecepcionMateriales = function() {
      if ($scope.textoCargarRM != 'CARGAR DOCUMENTO') return;

      $scope.textoCargarRM = 'Cargando documento...';

      var filesRM = document.getElementById('fileRM').files;

      if (filesRM.length > 0) {
        if (filesRM[0].type === 'text/xml' || filesRM[0].type === 'application/pdf') {
          var reader = new FileReader();
          reader.readAsDataURL(filesRM[0]);

          reader.onload = function() {
            var data = reader.result;
            var payload = data.replace(/^[^,]*,/, '');

            $scope.cargando = true;

            FebosAPI.cl_adjuntar_documento(
              {
                febosId: $scope.documentoActual.febosId,
                token: SesionFebos().usuario.token,
                empresa: SesionFebos().empresa.iut,
                grupo: SesionFebos().grupo.id,
                debug: '',
                simular: ''
              },
              {
                tipo: '3',
                nombreArchivo: filesRM[0].name,
                tipoOrigen: '2',
                payload: payload,
                folioRef: document.getElementById('folioRef').value,
                fechaRef: document.getElementById('fechaRef').value,
                razonRef: document.getElementById('razonRef').value
              },
              true,
              false
            ).then(function(response) {
              console.log(response);
              try {
                if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                  $scope.cargando = false;
                  $scope.textoCargarRM = 'CARGAR DOCUMENTO';
                  $scope.mensaje_okRM = response.data.mensaje;

                  UIkit.modal('#mdFrmCargaRecepcionMateriales').hide();

                  UIkit.modal.alert(
                    '<div class=\'uk-text-center\'><h2 class="heading_b uk-text-success">' +
                      'El documento ha sido adjunto satisfactoriamente al documento</h2><p>' +
                      $scope.mensaje_okRM +
                      '</p>'
                  );

                  utils.card_show_hide($crear_formRM, undefined, crear_okRM, undefined);
                } else {
                  SesionFebos().error(
                    response.data.codigo,
                    response.data.mensaje,
                    response.data.seguimientoId
                  );
                }
              } catch (e) {
                console.log(e);
                SesionFebos().error(
                  response.data.codigo,
                  response.data.mensaje,
                  response.data.seguimientoId
                );
              }
            });
          };

          reader.onerror = function(error) {
            console.log('Error: ', error);
            febosSingleton.error('1', 'Error, Falló el envío de la solicitud', '');
          };
        } else {
          console.log('No soporta el tipo de archivo ' + filesRM[0].type);
          //febosSingleton.error('1', 'No soporta el tipo de archivo' + filesRM[0].type, '');
          alert('No soporta el tipo de archivo' + filesRM[0].type);
          $scope.textoCargarRM = 'CARGAR DOCUMENTO';
          return false;
        }
      } else {
        //febosSingleton.error('1', 'El documento es obligatorio y en formato XML o PDF', '');
        alert('El documento es obligatorio y en formato XML o PDF');
        $scope.textoCargarRM = 'CARGAR DOCUMENTO';
        return false;
      }
    };

    var modalReferencias = UIkit.modal('#modalReferencias');
    $scope.verReferencias = function(documento) {
      $scope.documentoActual = documento;
      $scope.referencias.loadingReferencias = true;
      FebosAPI.cl_listar_referencias(
        {
          febosId: documento.febosId
        },
        {},
        false,
        true
      ).then(function(response) {
        //$scope.block.hide();
        console.log(response);

        $scope.referencias.referenciasDte = response.data.referenciasTipoDte;
        $scope.referencias.referenciasDnt = response.data.referenciasTipoDnt;
        $scope.referencias.referenciadosDte = response.data.referenciadosTipoDte;

        $scope.referencias.loadingReferencias = false;
      });
      modalReferencias.show();
    };

    $scope.verPDFDnt = function(id) {
      console.log(id);
      console.log('ver PDF');

      $scope.url_pdf = [];
      $scope.cargando.e = true;
      $scope.textoVerPdf = 'Cargando ...';

      $scope.block = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Generando representación impresa...<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
      );

      FebosAPI.cl_obtener_dnt(
        {
          febosId: id,
          imagen: 'si',
          regenerar: 'si',
          incrustar: 'no'
        },
        {},
        false,
        true
      ).then(function(response) {
        console.log(response);
        $scope.block.hide();
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        $scope.cargando.e = false;
        window.open(response.data.imagenLink);
        $scope.cambiarPagina($scope.config.paginaActual);
        $scope.textoVerPdf = 'PDF';
      });
    };

    $scope.modalSolicitarDte = function(documento) {
      $scope.documentoActual = documento;
      UIkit.modal('#mdlSolicitarDte').show();
    };

    $scope.modalSolicitarDteMasivo = function(documentosSeleccionados) {
      $scope.documentosSeleccionados = documentosSeleccionados;
      $scope.masivo = 'si';
      UIkit.modal('#mdlSolicitarDte').show();
    };

    $scope.cargando = false;

    $scope.solicitarDte = function() {
      $scope.cargando = true;

      if ($scope.masivo == 'si') {
        var objetoAuxiliarPayload = [];
        console.log($scope.documentosSeleccionados);

        for (var i = 0; i < $scope.documentosSeleccionados.length; i++) {
          objetoAuxiliarPayload.push({ febosId: $scope.documentosSeleccionados[i] });
        }

        console.log(objetoAuxiliarPayload);
        FebosAPI.cl_realizar_accion_masiva(
          {},
          {
            accion: 'cl_solicitar_dte_no_recibido',
            nombre: 'Solicitud de documentos DTE por correo',
            metodo: 'SERIE',
            comunes: {
              contacto: $scope.datos.contacto,
              copias: $scope.datos.copias,
              recibirCopia: $scope.datos.recibirCopia,
              mensaje: btoa($scope.datos.mensaje)
            },
            payload: objetoAuxiliarPayload
          },
          true,
          true
        ).then(function(response) {
          $scope.cargando = false;
          try {
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
              UIkit.modal('#mdlSolicitarDte').hide();
              UIkit.modal.alert(
                'Operación efectuada correctamente! Se te avisará por correo cuando termine la acción.'
              );
            } else {
              SesionFebos().error(
                response.data.codigo,
                response.data.mensaje,
                response.data.seguimientoId
              );
            }
          } catch (e) {
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        });
      } else {
        FebosAPI.cl_solicitar_dte_no_recibido(
          {
            febosId: $scope.documentoActual.febosId,
            contacto: $scope.datos.contacto,
            copias: $scope.datos.copias,
            recibirCopia: $scope.datos.recibirCopia,
            mensaje: btoa($scope.datos.mensaje)
          },
          {},
          false,
          true
        ).then(function(response) {
          console.log(response);
          $scope.cargando = false;
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;

          try {
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
              UIkit.modal('#mdlSolicitarDte').hide();
              UIkit.modal.alert('DTE Solicitado!');
            } else {
              SesionFebos().error(
                response.data.codigo,
                response.data.mensaje,
                response.data.seguimientoId,
                response.data.errores
              );
            }
          } catch (e) {
            console.log(e);
            SesionFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
          }
        });
      }
      $scope.datos = {
        contacto: '',
        copias: '',
        recibirCopia: 'no',
        mensaje: ''
      };
    };

    // -- función que abre modal con formulario para adjuntar una orden de compra
    $scope.modalCargaOrdenCompra = function(documento) {
      console.log('modal carga orden de compra');
      $scope.cargando.k = true;
      $scope.textoCargarOC = 'CARGAR DOCUMENTO';
      $scope.documentoActual = documento;

      $scope.oc = {
        folioOc: '',
        fechaOc: '',
        razonOc: ''
      };

      UIkit.modal('#mdFrmCargaOrdenCompra').show();
      $scope.cargando.k = false;
    };
    // --

    // -- función que adjunta una orden de compra en formato XML y PDF, los convierte en formato base64, envía los datos a la API, el cual retorna un objeto como resultado
    $scope.cargarOrdenCompras = function() {
      if ($scope.textoCargarOC != 'CARGAR DOCUMENTO') return;

      $scope.textoCargarOC = 'Cargando documento...';

      if (!$scope.validacionOrdenCompra())
        //< valida compos obligatorios
        return false;

      var filesOC = document.getElementById('fileOC').files;

      if (filesOC.length > 0) {
        if (filesOC[0].type === 'text/xml' || filesOC[0].type === 'application/pdf') {
          var reader = new FileReader();
          reader.readAsDataURL(filesOC[0]);

          reader.onload = function() {
            var data = reader.result;
            var payload = data.replace(/^[^,]*,/, '');

            $scope.cargando = true;

            FebosAPI.cl_adjuntar_documento(
              {
                febosId: $scope.documentoActual.febosId,
                token: SesionFebos().usuario.token,
                empresa: SesionFebos().empresa.iut,
                grupo: SesionFebos().grupo.id,
                debug: '',
                simular: ''
              },
              {
                tipo: '1',
                nombreArchivo: filesOC[0].name,
                tipoOrigen: '1',
                payload: payload,
                folioRef: $scope.oc.folioOc,
                fechaRef: $scope.oc.fechaOc,
                razonRef: $scope.oc.razonOc
              },
              true,
              true
            ).then(function(response) {
              console.log(response);
              $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
              $scope.cargando = false;

              try {
                if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                  $scope.textoCargarOC = 'CARGAR DOCUMENTO';
                  $scope.mensaje_okOC = response.data.mensaje;

                  UIkit.modal('#mdFrmCargaOrdenCompra').hide();

                  UIkit.modal.alert(
                    '<div class=\'uk-text-center\'><h2 class="heading_b uk-text-success">' +
                      'El documento ha sido adjunto satisfactoriamente al documento</h2><p>' +
                      $scope.mensaje_okOC +
                      '</p>'
                  );

                  //utils.card_show_hide($crear_formOC, undefined, crear_okOC, undefined);
                  //$scope.limpiaOrdenCompra();
                } else {
                  SesionFebos().error(
                    response.data.codigo,
                    response.data.mensaje,
                    response.data.seguimientoId
                  );
                }
              } catch (e) {
                console.log(e);
                SesionFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
              }
            });
          };

          reader.onerror = function(error) {
            console.log('Error: ', error);
            SesionFebos().error('1', 'Error, Falló el envío de la solicitud', '');
          };
        } else {
          console.log('No soporta el tipo de archivo ' + filesOC[0].type);
          alert('No soporta el tipo de archivo ' + filesOC[0].type);
          $scope.textoCargarOC = 'CARGAR DOCUMENTO';
          return false;
        }
      } else {
        alert('El documento es obligatorio y en formato XML o PDF');
        $scope.textoCargarOC = 'CARGAR DOCUMENTO';
        return false;
      }
    };
    // --

    // -- función que valida formulario orden de compras
    $scope.validacionOrdenCompra = function() {
      var errores = '';

      if ($scope.oc.folioOc == '' || $scope.oc.folioOc == 'undefined') {
        errores += 'Debe ingresar un folio.\n';
      }

      if (isNumber($scope.oc.folioOc) === 0) {
        errores += 'Debe ingresar un folio de valor numérico.\n';
      }

      if ($scope.oc.fechaOc == '' || $scope.oc.fechaOc == 'undefined') {
        errores += 'Debe ingresar una fecha de referencia.\n';
      }

      if ($scope.oc.razonOc == '' || $scope.oc.razonOc == 'undefined') {
        errores += 'Debe ingresar razón social.\n';
      }

      if (errores != '') {
        alert('Errores encontrados campos obligatorios vacíos :\n' + errores);
        $scope.textoCargarOC = 'CARGAR DOCUMENTO';
        return false;
      } else {
        return true;
      }

      return true;
    };
    // --

    // -- función que limpia los input del formulario de ordenes de comparas
    $scope.limpiaOrdenCompra = function() {
      $scope.oc.folioOc = '';
      $scope.oc.fechaOc = '';
      $scope.oc.razonOc = '';
    };

    // --

    function isNumber(input) {
      return (
        typeof input === 'number' || Object.prototype.toString.call(input) === '[object Number]'
      );
    }

    // Asociar contacto
    $scope.query = '';
    $scope.usuarioSeleccionado = '';
    $scope.todosLosUsuarios = [];

    $scope.modalAsociarContacto = function(documento) {
      $scope.cargando = 'no';
      $scope.documentoActual = documento;
      UIkit.modal('#mdlAsociarContacto').show();
      $scope.cargarUsuarios();
    };

    $scope.cargarUsuarios = function() {
      $scope.cargandoUsuarios = true;
      $scope.usuarios = [];
      FebosAPI.cl_listar_arbol_usuarios(
        {
          empresaId: SesionFebos().empresa.iut
        },
        true,
        false
      ).then(function(response) {
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        $scope.todosLosUsuarios = response.data.usuarios;

        for (var i = 0; i < $scope.todosLosUsuarios.length; i++) {
          var row = {
            id: '',
            rut: '',
            nombre: '',
            alias: '',
            correo: '',
            estado: '',
            seleccionado: false
          };

          row.id = response.data.usuarios[i].usuario.id;
          row.rut = response.data.usuarios[i].usuario.iut;
          row.nombre = response.data.usuarios[i].usuario.nombre;
          row.alias = response.data.usuarios[i].usuario.alias;
          row.correo = response.data.usuarios[i].usuario.correo;
          row.estado = response.data.usuarios[i].usuario.estado;

          $scope.usuarios.push(row);
        }
        $scope.cargandoUsuarios = false;
      });
    };
    $scope.archivoNomina = '';
    $scope.nomina = {
      original: '',
      csv: [],
      conCabeceras: true,
      mapa: {}
    };
    $scope.popUpNomina = function() {
      var modal = UIkit.modal('#nomina');
      $scope.pasoNomina(1);
      modal.show();
    };
    $scope.numeroPaso = 1;
    $scope.mostrarInstruccionesNomina = false;
    $scope.toggleInstruccionesNomina = function() {
      $scope.mostrarInstruccionesNomina = !$scope.mostrarInstruccionesNomina;
    };
    $scope.pasoNomina = function(paso) {
      switch (paso) {
        case 0:
          if ($scope.archivoNomina == '') {
          }
          UIkit.modal('#nomina').hide();
          break;

        case 1:
          $scope.archivoNomina = '';
          break;
        case 2:
          $scope.preprocesarNomina();
          break;
        case 3:
      }
      $scope.numeroPaso = paso;
    };
    $scope.preprocesarNomina = function() {
      console.log('preprosesando nomina');
      var file = document.getElementById('nominaCSV').files;
      console.log(file);
      if (file.length > 0) {
        if (file[0].type === 'text/plain' || file[0].type === 'text/csv') {
          var reader = new FileReader();
          reader.readAsDataURL(file[0]);
          console.log('leyendo');
          reader.onload = function() {
            var data = reader.result;
            var payload = data.replace(/^[^,]*,/, '');
            var csv = FebosUtil.decode(payload);
            var comas = (csv.match(/,/g) || []).length;
            var pcomas = (csv.match(/;/g) || []).length;
            var separador = comas > pcomas ? ',' : ';';
            var lineas = csv.split('\n');
            var titulos = [
              'febosid',
              'rut',
              'tipo',
              'folio',
              'estado',
              'medio',
              'monto',
              'lugar',
              'comentarios'
            ];
            for (var i = 0; i < lineas.length; i++) {
              var campos = lineas[i].split(separador);
              if (i == 0) {
                for (var x = 0; x < campos.length; x++) {
                  if (titulos.indexOf(campos[x]) > -1) {
                    $scope.nomina.mapa[x] = campos[x];
                  }
                }
              }
              $scope.nomina.csv.push(campos);
            }
            $scope.nomina.original = csv;
            console.log(csv);
            $scope.$apply();
          };
        }
      }
    };

    $scope.buscarUsuario = function(item) {
      if ($scope.query == '') return true;
      if (
        item.nombre.toLowerCase().indexOf($scope.query.toLowerCase()) >= 0 ||
        item.correo.toLowerCase().indexOf($scope.query.toLowerCase()) >= 0
      ) {
        return true;
      }
      return false;
    };

    $scope.seleccionarContacto = function(usuario) {
      for (var i = 0; i < $scope.usuarios.length; i++) {
        if ($scope.usuarios[i].id == usuario.id) {
          $scope.usuarios[i].seleccionado = true;
        } else {
          $scope.usuarios[i].seleccionado = false;
        }
      }
      $scope.usuarioSeleccionado = usuario;
    };

    $scope.asociarContacto = function() {
      $scope.cargando = 'si';
      if ($scope.masivo == 'si') {
      } else {
        FebosAPI.cl_asociar_contacto(
          {
            usuarioId: $scope.usuarioSeleccionado.id,
            febosId: $scope.documentoActual.febosId
          },
          {},
          true,
          true
        ).then(function(response) {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;

          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            for (var i = 0; i < $scope.documentos.length; i++) {
              if ($scope.documentos[i].febosId == $scope.documentoActual.febosId) {
                $scope.documentos[i].correoReceptor = $scope.usuarioSeleccionado.correo;
                break;
              }
            }
            UIkit.modal('#mdlAsociarContacto').hide();
            UIkit.modal.alert(
              'Operación efectuada correctamente!<br> Se ha asociado el correo: ' +
                $scope.usuarioSeleccionado.correo,
              { labels: { Ok: 'Ok' } }
            );
          } else {
            $scope.cargando = 'si';
            UIkit.modal.alert(
              'Ocurrió un error al asociar correo. Por favor intentarlo nuevamente',
              { labels: { Ok: 'Ok' } }
            );
          }
        });
      }
    };

    // FEB-345: Ver XML
    $scope.cargando = false;
    $scope.cargando.e = false;
    $scope.verXML = function(id) {
      console.log(id);
      console.log('ver XML');

      $scope.url_xml = [];
      $scope.cargando.e = true;
      $scope.textoVerXml = 'Cargando ...';
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Generando representación en XML...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      FebosAPI.cl_obtener_documento(
        {
          febosId: id,
          xml: 'si',
          xmlFirmado: 'si',
          regenerar: 'no',
          incrustar: 'no',
          tipoImagen: 0
        },
        {},
        true,
        true
      ).then(function(response) {
        console.log(response);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        $scope.cargando.e = false;

        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            window.open(response.data.xmlLink);
            $scope.textoVerXml = 'XML';
          } else {
            $scope.textoVerXml = 'XML';
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        } catch (e) {
          $scope.textoVerXml = 'XML';
          SesionFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
        }
      });
    };

    //FEB-345: Consultar estado Sii y actualiza en DB
    $scope.modalConsultarEstadoSii = function(documento) {
      var modal = UIkit.modal('#consultar_estado_sii');
      $scope.documentoActual = documento;
      $scope.masivo = 'no';
      modal.show();
    };
    /*
        $scope.modalConsultarEstadoSiiMasivo = function (documentosSeleccionados) {
            var modal = UIkit.modal("#consultar_estado_sii");
            $scope.documentosSeleccionados = documentosSeleccionados;
            $scope.masivo = 'si';
            modal.show();
        }
        */
    $scope.consultarEstadoSii = function(documento) {
      UIkit.modal('#consultar_estado_sii').hide();
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Consultando Estado del Documento en el SII..." +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      FebosAPI.cl_sii_consulta_dte(
        {
          febosId: $scope.documentoActual.id,
          rutEmisor: $scope.documentoActual.rutEmisor,
          rutReceptor: $scope.documentoActual.rutReceptor,
          tipo: $scope.documentoActual.tipoDocumento,
          folio: $scope.documentoActual.folio,
          monto: $scope.documentoActual.montoTotal,
          fechaEmision: $scope.documentoActual.fechaEmision
        },
        {},
        true,
        true
      ).then(function(response) {
        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}

        $scope.cargando = false;

        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            $scope.estadoSiiDocumento = {
              codigo: response.data.codigoSii,
              glosa: response.data.glosaSii,
              estaEnElSii:
                ['DOK', 'TMD', 'TMC', 'MMD', 'MMC', 'AND', 'ANC'].indexOf(
                  response.data.codigoSii
                ) >= 0
                  ? true
                  : false
            };
            console.log($scope.estadoSiiDocumento);
            var modal = UIkit.modal('#mostrar_estado_sii');
            modal.show();
          } else {
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        } catch (e) {
          console.log(e);
          SesionFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
        }
      });
    };

    $scope.correoValidacionSii = function() {
      FebosAPI.cl_sii_solicitar_correo(
        {
          rutEmisor: $scope.documentoActual.rutEmisor,
          trackId: $scope.documentoActual.trackId
        },
        {},
        true,
        true
      ).then(function(response) {
        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}

        try {
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            UIkit.modal.alert(response.data.estado + ': ' + response.data.mensajeResultado, {
              labels: { Ok: 'Ok' }
            });
          } else {
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        } catch (e) {
          console.log(e);
          SesionFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
        }
      });
    };

    console.log('app, vista y categoria', $scope.app, $scope.vista, $scope.categoria);

    $scope.esMisRecibidos =
      $scope.vista === 'misRecibidos' || $scope.vista === 'misRecibidosUnidad' ? true : false;

    $scope.verificarMasivoMod = function(documentosSeleccionados) {
      var modal = UIkit.modal('#verificar');
      $scope.documentosSeleccionados = documentosSeleccionados;
      $scope.masivo = 'si';
      modal.show();
    };

    $scope.verificar = function(febosId) {
      $scope.block = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Enviando documento...<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
      );

      $scope.reencolando = true;
      FebosAPI.cl_obtener_documento(
        {
          febosId: febosId,
          debug: 'si',
          simular: 'no',
          xml: 'si',
          regenerar: 'no',
          incrustar: 'si',
          xmlFirmado: 'si'
        },
        {},
        true,
        true
      ).then(
        function(response) {
          console.log(response);
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
          try {
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
              xml = response.data.xmlData;
              $scope.enviar(febosId, xml);
            } else {
              $scope.block.hide();
              UIkit.modal.alert(response.data.mensaje + '<br/>' + response.data.mensajeResultado);
              //SesionFebos().error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
              //
            }
          } catch (e) {
            $scope.block.hide();
            UIkit.modal.alert(response.data.mensaje + '<br/>' + response.data.mensajeResultado);
            //SesionFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
          }
        },
        function(response) {
          $scope.block.hide();
          console.log(response);
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
          UIkit.modal.alert(response.data.mensaje + '<br/>' + response.data.mensajeResultado);
          //SesionFebos().error('1', 'Error, Falló el envío de la solicitud', '');
        }
      );
    };

    $scope.verificarMasivo = function(documentosSeleccionados) {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Ejecutando Acción Masiva...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      var objetoAuxiliarPayload = [];
      for (var i = 0; i < $scope.documentosSeleccionados.length; i++) {
        objetoAuxiliarPayload.push({ febosId: $scope.documentosSeleccionados[i] });
        FebosAPI.cl_realizar_accion_masiva(
          {},
          {
            accion: 'cl_obtener_documento',
            nombre: 'Verificar Documentos Pendientes',
            metodo: 'SERIE',
            comunes: {
              debug: 'si',
              simular: 'no',
              xml: 'si',
              regenerar: 'no',
              incrustar: 'si',
              xmlFirmado: 'si'
            },
            payload: objetoAuxiliarPayload
          },
          true,
          true
        ).then(function() {
          console.log('REALIZADO OK');
          UIkit.modal('#verificar').hide();
          UIkit.notify(
            "Operación efectuada correctamente! Se te avisará por correo cuando termine el proceso completo.<a class='notify-action'>[X]</a> ",
            {
              status: 'success',
              timeout: 3000
            }
          );
        });
      }
    };

    $scope.enviar = function(febosId, xml) {
      console.log('XML', xml);
      console.log('febosid', febosId);
      var nombre = febosId + new Date().getTime() + '.xml';
      var ambiente = FebosUtil.obtenerAmbiente();
      var sufijo = ambiente.charAt(0).toUpperCase() + ambiente.slice(1);
      var flujoId = 'RecepcionDte' + sufijo;

      FebosAPI.cl_invocar_flujo(
        {
          empresaId: SesionFebos().empresa.iut,
          flujoId: flujoId
        },
        {
          nombreArchivo: nombre,
          payload: xml
        },
        true,
        false
      ).then(
        function(response) {
          $scope.reencolando = false;
          $scope.block.hide();
          try {
            console.log('Seguimiento', response.data.seguimientoId);
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
              UIkit.modal.alert(response.data.mensaje + '<br/>' + response.data.mensajeResultado);
            } else {
              UIkit.modal.alert(response.data.mensaje + '<br/>' + response.data.mensajeResultado);
              //SesionFebos().error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
            }
          } catch (e) {
            UIkit.modal.alert(response.data.mensaje + '<br/>' + response.data.mensajeResultado);
            //SesionFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
          }
        },
        function(response) {
          $scope.reencolando = false;
          $scope.block.hide();
        }
      );
    };

    $scope.recuperarCorreo = function(documento) {
      UIkit.modal.confirm(
        '¿Deseas recuperar el correo Sii? ',
        function() {
          FebosAPI.cl_recuperar_correo_sii(
            {
              febosId: documento.febosId
            },
            {},
            true,
            false
          ).then(function() {
            UIkit.modal.alert('¡Se ha hecho tu solicitud satisfactoriamente!');
          });
        },
        function() {
          //console.log('no hace nada');
        }
      );
    };

    $scope.recuperarCorreoMasivo = function(seleccionados) {
      UIkit.modal.confirm(
        '¿Deseas recuperar el correo Sii? ',
        function() {
          var payloadSeleccionados = [];
          for (var i = 0; i < Object.keys(seleccionados).length; i++) {
            payloadSeleccionados.push({ febosId: seleccionados[i] });
          }
          FebosAPI.cl_realizar_accion_masiva(
            {},
            {
              accion: 'cl_recuperar_correo_sii',
              nombre: 'Recuperar correo Sii',
              comunes: {},
              metodo: 'SERIE',
              payload: payloadSeleccionados
            },
            true,
            true
          ).then(function() {
            UIkit.notify(
              "Operación efectuada correctamente! Se te avisará por correo cuando termine el proceso completo.<a class='notify-action'>[X]</a> ",
              {
                status: 'success',
                timeout: 3000
              }
            );
          });
        },
        function() {
          //console.log('no hace nada');
        }
      );
    };

    $scope.reenviarAlSii = function(documento) {
      UIkit.modal.confirm(
        '¿Deseas reenviar el documento al Sii? ',
        function() {
          FebosAPI.cl_reenviar_al_sii(
            {
              febosId: documento.febosId
            },
            {},
            true,
            false
          ).then(function() {
            UIkit.modal.alert('¡Se ha hecho tu solicitud satisfactoriamente!');
          });
        },
        function() {
          //console.log('no hace nada');
        }
      );
    };

    $scope.reenviarAlSiiMasivo = function(seleccionados) {
      UIkit.modal.confirm(
        '¿Deseas reenviar al Sii? ',
        function() {
          var payloadSeleccionados = [];
          for (var i = 0; i < Object.keys(seleccionados).length; i++) {
            payloadSeleccionados.push({ febosId: seleccionados[i] });
          }
          FebosAPI.cl_realizar_accion_masiva(
            {},
            {
              accion: 'cl_reenviar_al_sii',
              nombre: 'Reenviar al Sii',
              comunes: {},
              metodo: 'SERIE',
              payload: payloadSeleccionados
            },
            true,
            true
          ).then(function() {
            UIkit.notify(
              "Operación efectuada correctamente! Se te avisará por correo cuando termine el proceso completo.<a class='notify-action'>[X]</a> ",
              {
                status: 'success',
                timeout: 3000
              }
            );
          });
        },
        function() {
          //console.log('no hace nada');
        }
      );
    };

    $scope.modalCederDocumentos = function(documentos) {
      $scope.masivo = 'si';
      $scope.cargando = 'no';
      $scope.documentosActual = documentos;
      UIkit.modal('#mdlCederDocumento').show();
      // $scope.cargarUsuarios();
    };
    $scope.modalCederDocumento = function(documento) {
      $scope.masivo = 'no';
      $scope.cargando = 'no';
      $scope.documentoActual = documento;
      UIkit.modal('#mdlCederDocumento').show();
      // $scope.cargarUsuarios();
    };
    $scope.enviarCesion = function() {
      if (!$scope.cesionario) {
        $scope.cesionario = {};
      }
      if (!$scope.contacto) {
        $scope.contacto = {};
      }
      var cesion = {
        cesionarioRut: null,
        cesionarioRazonSocial: null,
        cesionarioDireccion: null,
        cesionarioMail: null,
        contactoNombreContacto: null,
        contactoEmailContato: null,
        contactoTelefonoContacto: null
      };
      var todoValido = true;
      try {
        $scope.cesionario.rut = formatRut($scope.cesionario.rut);
        console.log(
          'telefono valido con 5 ' + utils.validaciones.largo($scope.contacto.telefono, 5)
        );
        console.log(
          'telefono valido con 4 ' + utils.validaciones.largo($scope.contacto.telefono, 4)
        );
        console.log(
          'telefono valido con 3 ' + utils.validaciones.largo($scope.contacto.telefono, 3)
        );
        if (!utils.validaciones.largo($scope.contacto.telefono, 3)) {
          todoValido = false;
          $scope.contactoTelefonoInvalid = true;
          $window.document.getElementById('telefonoContacto').focus();
        } else {
          $scope.contactoTelefonoInvalid = false;
        }
        if (!utils.validaciones.email($scope.contacto.email)) {
          todoValido = false;
          $scope.contactoEmailInvalid = true;
          $window.document.getElementById('emailContacto').focus();
        } else {
          $scope.contactoEmailInvalid = false;
        }
        if (!utils.validaciones.largo($scope.contacto.nombre, 4)) {
          todoValido = false;
          $scope.contactoNombreInvalid = true;
          $window.document.getElementById('nombreContacto').focus();
        } else {
          $scope.contactoNombreInvalid = false;
        }

        if (!utils.validaciones.email($scope.cesionario.correo)) {
          todoValido = false;
          $scope.cesionarioCorreoInvalid = true;
          $window.document.getElementById('correoCesionario').focus();
        } else {
          $scope.cesionarioCorreoInvalid = false;
        }
        if (!utils.validaciones.largo($scope.cesionario.direccion, 4)) {
          todoValido = false;
          $scope.cesionarioDireccionInvalid = true;
          $window.document.getElementById('direccionCesionario').focus();
        } else {
          $scope.cesionarioDireccionInvalid = false;
        }
        if (!utils.validaciones.largo($scope.cesionario.razon, 4)) {
          todoValido = false;
          $scope.cesionarioRazonInvalid = true;
          $window.document.getElementById('razonCesionario').focus();
        } else {
          $scope.cesionarioRazonInvalid = false;
        }
        if (!utils.validaciones.rut($scope.cesionario.rut)) {
          todoValido = false;
          $scope.cesionarioRutInvalid = true;
          $window.document.getElementById('rutCesionario').focus();
        } else {
          $scope.cesionarioRutInvalid = false;
        }
      } catch (e) {
        todoValido = false;
        console.error(e);
      }
      if (!todoValido) {
        return;
      }

      cesion['cesionarioRut'] = $scope.cesionario.rut;
      cesion['cesionarioRazonSocial'] = $scope.cesionario.razon;
      cesion['cesionarioDireccion'] = $scope.cesionario.direccion;
      cesion['cesionarioMail'] = $scope.cesionario.correo;
      cesion['contactoNombreContacto'] = $scope.contacto.nombre;
      cesion['contactoEmailContato'] = $scope.contacto.email;
      cesion['contactoTelefonoContacto'] = $scope.contacto.telefono;
      if ($scope.masivo == 'no') {
        $rootScope.blockModal = UIkit.modal.blockUI(
          "<div class='uk-text-center'>Generando Archivo Cesión...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
        );

        cesion['febosId'] = $scope.documentoActual.febosId;
        FebosAPI.cl_generar_archivo_cesion({}, cesion, true, true).then(function() {
          console.log('REALIZADO OK');
          UIkit.modal('#mdlCederDocumento').hide();
          UIkit.notify('Operación efectuada correctamente!', { status: 'success', timeout: 3000 });
        });
      } else {
        $rootScope.blockModal = UIkit.modal.blockUI(
          "<div class='uk-text-center'>Ejecuntando Acción Masiva...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
        );

        var objetoAuxiliarPayload = [];
        for (var i = 0; i < $scope.documentosSeleccionados.length; i++) {
          objetoAuxiliarPayload.push({ febosId: $scope.documentosSeleccionados[i] });
        }

        FebosAPI.cl_realizar_accion_masiva(
          {},
          {
            accion: 'cl_generar_archivo_cesion',
            nombre: 'Generacion cesion documentos',
            comunes: cesion,
            metodo: 'SERIE',
            payload: objetoAuxiliarPayload
          },
          true,
          true
        ).then(function() {
          console.log('REALIZADO OK');
          UIkit.modal('#mdlCederDocumento').hide();
          UIkit.notify(
            "Operación efectuada correctamente! Se te avisará por correo cuando termine el proceso completo.<a class='notify-action'>[X]</a> ",
            {
              status: 'success',
              timeout: 3000
            }
          );
        });
      }
    };

    $scope.infoPagoMasivaOk = false;
    $scope.archivoInfoPago = UIkit.modal('#archivoInfoPago');
    $scope.errorArchivoInfoPago = '';
    $scope.popArchivoInfoPago = function() {
      $scope.infoPagoMasivaOk = false;
      $scope.errorArchivoInfoPago = '';
      $scope.archivoInfoPago.show();
    };

    $scope.procesarArchivo = function($fileContent) {
      ////console.log("archivo", $fileContent);
      //campos: febos id, rut emisor, tipo dte, folio, estado de pago, medio de pago, monto, lugar, fecha, comentario
      $scope.llamadas = [];
      var lineas = $fileContent.split('\n');

      try {
        var campos = lineas[0].split(';');
        console.log(campos);
        if (
          campos[0].trim() === 'febosId' &&
          campos[1].trim() === 'rutEmisor' &&
          campos[2].trim() === 'codigoTipoDte' &&
          campos[3].trim() === 'folio' &&
          campos[4].trim() === 'estadoPago' &&
          campos[5].trim() === 'medioPago' &&
          campos[6].trim() === 'monto' &&
          campos[7].trim() === 'lugar' &&
          campos[8].trim() === 'fecha' &&
          campos[9].trim() === 'comentario'
        ) {
          //todo ok
        } else {
          throw 'Los nombres de las columnas no coinciden con el formato requerido';
        }

        for (var i = 1; i < lineas.length; i++) {
          var campos = lineas[i].split(';');

          var obj = {
            febosId: campos[0].trim(),
            rutEmisor: campos[1]
              .trim()
              .split('.')
              .join(''),
            tipoDte: campos[2].trim(),
            folio: campos[3].trim(),
            tipo: campos[4].trim(),
            medio: campos[5].trim(),
            monto: campos[6].trim(),
            lugar: campos[7].trim(),
            fecha: campos[8].trim(),
            comentario: campos[9]
          };
          for (var property in obj) {
            if (obj.hasOwnProperty(property)) {
              $scope.validarCampoCSV(i, property, obj[property]);
            }
          }
          $scope.llamadas.push(obj);
        }
        $scope.errorArchivoInfoPago = '';
        $scope.infoPagoMasivaOk = true;
      } catch (e) {
        $scope.errorArchivoInfoPago = e;
        console.log(e);
      }
    };
    $scope.enviarInfoMasivaDePago = function() {
      $scope.archivoInfoPago.hide();
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Enviando archivo para proceso...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_realizar_accion_masiva(
        {},
        {
          accion: 'cl_crea_info_pago',
          nombre: 'Modifica Info de Pago Masiva mediante archivo',
          comunes: {},
          metodo: 'SERIE',
          payload: $scope.llamadas
        },
        true,
        true
      ).then(function() {
        UIkit.notify(
          "Operación efectuada correctamente! Se te avisará por correo cuando termine el proceso completo.<a class='notify-action'>[X]</a> ",
          {
            status: 'success',
            timeout: 3000
          }
        );
      });
    };
    $scope.validarRut = {
      validar: function(rutCompleto) {
        if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(rutCompleto)) return false;
        var tmp = rutCompleto.split('-');
        var digv = tmp[1];
        var rut = tmp[0];
        if (digv == 'K') digv = 'k';
        return $scope.validarRut.dv(rut) == digv;
      },
      dv: function(T) {
        var M = 0,
          S = 1;
        for (; T; T = Math.floor(T / 10)) S = (S + (T % 10) * (9 - (M++ % 6))) % 11;
        return S ? S - 1 : 'k';
      }
    };

    $scope.validarCampoCSV = function(linea, tipoCampo, valor) {
      linea++;
      try {
        switch (tipoCampo) {
          case 'febosId':
            if (valor.length < 36 && valor != '') {
              throw 'Error en la linea ' +
                linea +
                ", el valor '" +
                valor +
                "' no es un dato valido para el campo " +
                tipoCampo;
            }
            break;
          case 'rutEmisor':
            if (!$scope.validarRut.validar(valor)) {
              throw 'Error en la linea ' +
                linea +
                ", el valor '" +
                valor +
                "' no es un dato valido para el campo " +
                tipoCampo;
            }
            break;
          case 'tipoDte':
            var tipos = ['33', '34', '43', '46', '56', '39', '41'];
            if (!tipos.includes(valor)) {
              throw 'Error en la linea ' +
                linea +
                ", el valor '" +
                valor +
                "' no es un dato valido para el campo " +
                tipoCampo;
            }
            break;
          case 'folio':
            var pattern = /^\d+$/;
            if (!pattern.test(valor)) {
              throw 'Error en la linea ' +
                linea +
                ", el valor '" +
                valor +
                "' no es un dato valido para el campo " +
                tipoCampo;
            }
            break;
          case 'tipo':
            var tipos = ['1', '2', '3', '4', '5'];
            if (!tipos.includes(valor)) {
              throw 'Error en la linea ' +
                linea +
                ", el valor '" +
                valor +
                "' no es un dato valido para el campo " +
                tipoCampo;
            }
            break;
          case 'medio':
            var tipos = ['', 'CH', 'CF', 'LT', 'EF', 'PE', 'TC', 'VV', 'CO', 'OT'];
            if (!tipos.includes(valor)) {
              throw 'Error en la linea ' +
                linea +
                ", el valor '" +
                valor +
                "' no es un dato valido para el campo " +
                tipoCampo;
            }
            break;
          case 'monto':
            var pattern = /^\d+$/;
            if (!pattern.test(valor)) {
              throw 'Error en la linea ' +
                linea +
                ", el valor '" +
                valor +
                "' no es un dato valido para el campo " +
                tipoCampo;
            }
            break;
          case 'lugar':
            break;
          case 'fecha':
            try {
              var f = valor.split('-');
              if (f.length != 3) {
                throw 'error';
              }
              var yy = parseInt(f[0]);
              var mm = parseInt(f[1]);
              var dd = parseInt(f[2]);
              if (yy > 2100 && yy < 2000) {
                throw 'error';
              }
              if (mm > 12 && mm < 1) {
                throw 'error';
              }
              if (dd > 31 && dd < 1) {
                throw 'error';
              }
            } catch (e) {
              throw 'Error en la linea ' +
                linea +
                ", el valor '" +
                valor +
                "' no es un dato valido para el campo " +
                tipoCampo;
            }
            break;
          case 'comentario':
            break;
        }
      } catch (e) {
        throw e;
      }
    };

    /** Carga de documentos borradores **/
    $scope.cargaBorradoresMasivaOk = false;
    $scope.cargaInfoBorradores = UIkit.modal('#cargaInfoBorradores');
    $scope.errorCargaBorradores = '';
    $scope.borradorOpciones = {
      cargando: false,
      iniciado: false,
      errores: '',
      delimitador: undefined
    };
    $scope.camposCargaBorradores = [];

    $scope.popUpCargaBorradores = function() {
      if (!$scope.borradorOpciones.iniciado) {
        var cargando = 0,
          cargado = 0;

        function finCargando() {
          cargado++;
          if (cargando == cargado) {
            $scope.borradorOpciones.cargando = false;
          }
        }

        $scope.borradorOpciones.iniciado = true;
        $scope.borradorOpciones.cargando = true;
        cargando++;
        FebosAPI.cl_adm_configuracion_listar({ parametros: 'parser.borrador.separador' })
          .then(function(response) {
            console.log('separador', response.data.configuraciones[0].valor);
            $scope.borradorOpciones.delimitador = response.data.configuraciones[0].valor;
            try {
              $scope.borradorOpciones.delimitador = JSON.parse($scope.borradorOpciones.delimitador);
            } catch (e) {}
            finCargando();
          })
          .catch(function(reason) {
            $scope.cargando = false;
            console.log('error', reason);
          });
        cargando++;
        FebosAPI.cl_adm_opciones_listar({ grupoOpcion: 'cabeceras.borrador' })
          .then(function(response) {
            var cabeceras = FebosUtil.sortObjectByKey(response.data.opciones, 'orden', true);
            for (var i = 0; i < cabeceras.length; i++) {
              var cabecera = cabeceras[i];
              try {
                var extra = JSON.parse(cabecera.extra);
                $scope.camposCargaBorradores.push(
                  Object.assign({ campo: cabecera.valor, descripcion: cabecera.descripcion }, extra)
                );
              } catch (e) {
                console.log('ERROR', cabecera, e);
              }
            }
            finCargando();
          })
          .catch(function(reason) {
            $scope.cargando = false;
            console.log('error', reason);
          });
      }
      $scope.cargaInfoBorradores.show();
    };

    $scope.validacionCargaBorradores = function(validador, parametros, valor) {
      //console.log('[validacionCargaBorradores]');
      ////console.log("validador = '" + validador + "'\nparametros = " + parametros + '\nvalor = ' + valor);
      var valorValido = true;

      switch (validador) {
        case 'formatoFecha':
          var formato = parametros[0];
          switch (formato) {
            case 'dd-MM-yyyy':
              console.log('Formato ' + formato + ' valido');
              var f = valor.split('-');
              if (f.length != 3) {
                valorValido = false;
              }
              var yy = parseInt(f[0]);
              var mm = parseInt(f[1]);
              var dd = parseInt(f[2]);
              if (yy > 2100 && yy < 2000) {
                valorValido = false;
              }
              if (mm > 12 && mm < 1) {
                valorValido = false;
              }
              if (dd > 31 && dd < 1) {
                valorValido = false;
              }
              break;
            default:
              valorValido = false;
              break;
          }
          break;
        case 'ok':
          valorValido = true;
          break;
        case 'rut':
          valorValido = $scope.validarRut.validar(valor);
          break;
        case 'regex':
          var pattern = new RegExp(parametros[0]);
          var valid = pattern.test(new String(valor).trim());
          if (!valid) {
            valorValido = false;
          }
          break;
        case 'enLista':
          var tipos = parametros[0];
          if (!tipos.includes(valor)) {
            valorValido = false;
          }
          break;
      }

      return valorValido;
    };

    $scope.procesarArchivoCargaBorradores = function($fileContent, s) {
      $scope.llamadas = [];
      var lineas = $fileContent.split('\n');
      try {
        var errores = new Array();
        var cabecera = lineas[0].split(new String($scope.borradorOpciones.delimitador.delimitador));
        for (var i = 0; i < $scope.camposCargaBorradores.length; i++) {
          if (cabecera[i]) {
            var nombreCampoCSV = cabecera[i].trim();
            var nombreCampoValidacion = $scope.camposCargaBorradores[i].campo;
            if (nombreCampoCSV.localeCompare(nombreCampoValidacion) != 0) {
              throw 'La cabecera ' +
                nombreCampoCSV +
                ' no es valida para la columna ' +
                (i + 1) +
                '.\nEn su lugar debe ser ' +
                nombreCampoValidacion;
            }
          } else {
            if (!$scope.camposCargaBorradores[i].opcional) {
              throw 'La cabecera ' +
                nombreCampoValidacion +
                ' no es valida para la columna ' +
                (i + 1) +
                ' es obligatoria';
            }
          }
        }

        console.log('validacion registros');
        for (var i = 1; i < lineas.length; i++) {
          var linea = new String(lineas[i]);
          console.log('linea borrador  ' + i, linea.length);
          if (linea.trim().length == 0) {
            continue;
          }
          var valores = linea.split($scope.borradorOpciones.delimitador.delimitador);
          console.log('=====================================');
          console.log('registros ' + i + ': ' + valores);
          //Validacion de valor en columna
          for (var j = 0; j < $scope.camposCargaBorradores.length; j++) {
            var valor = valores[j];
            var campo = $scope.camposCargaBorradores[j].campo;
            console.log('valor ' + campo + '  = ' + valor);
            if (valor) {
              var validador = $scope.camposCargaBorradores[j].validacion.validador;
              var parametros = $scope.camposCargaBorradores[j].validacion.parametros;

              if (!$scope.validacionCargaBorradores(validador, parametros, valor)) {
                throw 'Error en la linea ' +
                  i +
                  ", el valor '" +
                  valor +
                  "' no es un dato valido para el campo " +
                  campo;
              }
            } else {
              if (!$scope.camposCargaBorradores[j].opcional) {
                throw 'La cabecera ' +
                  campo +
                  ' no es valida para la columna ' +
                  (j + 1) +
                  ' es obligatoria: fila ' +
                  i +
                  '  opcional ' +
                  $scope.camposCargaBorradores[i].opcional;
              }
            }
          }
        }

        for (var i = 1; i < lineas.length; i++) {
          var linea = new String(lineas[i]).trim();
          if (linea.length == 0) {
            continue;
          }
          var encodeLinea = FebosUtil.encode(linea);
          $scope.llamadas.push({ payload: encodeLinea });
        }

        console.log('LLAMADAS', $scope.llamadas);

        $scope.errorCargaBorradores = '';
        $scope.cargaBorradoresMasivaOk = true;

        console.log(
          'error=' + $scope.errorCargaBorradores + '; cargaok=' + $scope.cargaBorradoresMasivaOk
        );
      } catch (e) {
        $scope.errorCargaBorradores = e;
        console.log(e);
      }
    };

    function validarFechaCargaCSV(valor) {
      var valido = true;

      try {
        var f = valor.split('-');
        if (f.length != 3) {
          throw 'error';
        }
        var yy = parseInt(f[0]);
        var mm = parseInt(f[1]);
        var dd = parseInt(f[2]);
        if (yy > 2100 && yy < 2000) {
          throw 'error';
        }
        if (mm > 12 && mm < 1) {
          throw 'error';
        }
        if (dd > 31 && dd < 1) {
          throw 'error';
        }
      } catch (e) {
        valido = false;
      }

      return valido;
    }

    $scope.enviarCargaMasivaDeBorradores = function() {
      $scope.cargaInfoBorradores.hide();
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Enviando archivo para procesar...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      var data = {
        accion: 'cl_crea_docto_borrador',
        nombre: 'Realiza la carga de documentos borradores de forma masiva mediante archivo',
        comunes: {},
        metodo: 'SERIE',
        payload: $scope.llamadas
      };

      console.log('data borradores: \n' + JSON.stringify(data));
      FebosAPI.cl_realizar_accion_masiva({}, data, true, true).then(function() {
        UIkit.notify(
          "Operación efectuada correctamente! Se te avisará por correo cuando termine el proceso completo.<a class='notify-action'>[X]</a> ",
          {
            status: 'success',
            timeout: 3000
          }
        );
      });
    };

    $scope.emitirDesdeBorrador = function(documento) {
      console.log('Emitir documento desde el id', documento);
      var indice = $scope.documentos.indexOf(documento);
      console.log('Indice  ', indice);
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Enviando archivo para procesar...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      var data = { febosId: documento.febosId };
      console.log(data);
      FebosAPI.cl_crear_dte_desde_borrador({}, data, true, true).then(function(resp) {
        console.log(resp);
        if (indice > -1) {
          $scope.documentos.splice(indice, 1);
        }
        UIkit.notify(
          "Operación efectuada correctamente! Se te avisará por correo cuando termine el proceso completo.<a class='notify-action'>[X]</a> ",
          {
            status: 'success',
            timeout: 3000
          }
        );
      });
    };

    $scope.emitirBorradoresMasivos = function(ids) {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Enviando archivos para procesar...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      var llamadas = new Array();
      for (var i = 0; i < ids.length; i++) {
        llamadas.push({ febosId: ids[i] });
      }
      console.log('Emitir documento desde el id', ids);
      var data = {
        accion: 'cl_crear_dte_desde_borrador',
        nombre: 'Emision de documentos borradores masiva',
        comunes: {},
        metodo: 'SERIE',
        payload: llamadas
      };

      console.log('data borradores: \n' + JSON.stringify(data));
      FebosAPI.cl_realizar_accion_masiva({}, data, true, true).then(function() {
        UIkit.notify(
          "Operación efectuada correctamente! Se te avisará por correo cuando termine el proceso completo.<a class='notify-action'>[X]</a> ",
          {
            status: 'success',
            timeout: 3000
          }
        );
      });
    };

    $scope.enviarSistemaExterno = function(documento) {
      if (typeof documento == 'undefined') $scope.enviarSistemaExternoMasivo();
      else {
        $rootScope.blockModal = UIkit.modal.blockUI(
          "<div class='uk-text-center'>Gatillando envío de documento...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
        );
        var data = {
          id: documento.febosId,
          tipoVista: $scope.categoria == 'emitidos' ? 2 : 3,
          comentario: ''
        };
        FebosAPI.cl_comentarios_bitacoras(
          { gatillarEnvioErp: 'si', id: documento.febosId },
          data,
          true,
          true
        ).then(function(resp) {
          UIkit.notify(
            "Se ha gatillado un envio del documento a un sistema externo.<a class='notify-action'>[X]</a> ",
            {
              status: 'success',
              timeout: 3000
            }
          );
        });
      }
    };
    /**FIN  Carga de documentos borradores **/

    $scope.enviarSistemaExterno = function(documento) {
      if (typeof documento == 'undefined') $scope.enviarSistemaExternoMasivo();
      else {
        $rootScope.blockModal = UIkit.modal.blockUI(
          "<div class='uk-text-center'>Gatillando envío de documento...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
        );
        var data = {
          id: documento.febosId,
          tipoVista: $scope.categoria == 'emitidos' ? 2 : 3,
          comentario: ''
        };
        FebosAPI.cl_comentarios_bitacoras(
          { gatillarEnvioErp: 'si', id: documento.febosId },
          data,
          true,
          true
        ).then(function(resp) {
          UIkit.notify(
            "Se ha gatillado un envio del documento a un sistema externo.<a class='notify-action'>[X]</a> ",
            {
              status: 'success',
              timeout: 3000
            }
          );
        });
      }
    };
    $scope.enviarSistemaExternoMasivo = function() {
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Gatillando envío de documentos...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      var llamadas = new Array();
      for (var i = 0; i < ids.length; i++) {
        llamadas.push({ id: ids[i] });
      }
      console.log('Emitir documento desde el id', ids);
      var data = {
        accion: 'cl_comentarios_bitacoras',
        nombre: 'Envio de documentos a ERP',
        comunes: {
          tipoVista: $scope.categoria == 'emitidos' ? 2 : 3,
          gatillarEnvioErp: 'si',
          comentario: ''
        },
        metodo: 'SERIE',
        payload: llamadas
      };

      FebosAPI.cl_realizar_accion_masiva({}, data, true, true).then(function() {
        UIkit.notify(
          "Operación efectuada correctamente! Se te avisará por correo cuando termine el proceso completo.<a class='notify-action'>[X]</a> ",
          {
            status: 'success',
            timeout: 3000
          }
        );
      });
    };
  }
]);
