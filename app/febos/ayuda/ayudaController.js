angular.module('febosApp').controller('ayudaCtrl', [
  '$timeout',
  '$scope',
  '$window',
  '$state',
  '$location',
  '$rootScope',
  '$http',
  'SesionFebos',
  'FebosUtil',
  'FebosAPI',
  'FebosQueueAPI',
  function(
    $timeout,
    $scope,
    $window,
    $state,
    $location,
    $rootScope,
    $http,
    SesionFebos,
    FebosUtil,
    FebosAPI,
    FebosQueueAPI
  ) {
    $scope.esSuperAdmin = true;
    $scope.modoEdicion = false;
    $scope.markup = '';
    $scope.datos = {
      markup: '',
      html: ''
    };
    $scope.hash = '';
    var converter = new showdown.Converter();

    $scope.refrescar = function() {
      ////console.log("refrescando!", $scope.datos.markup);
      $scope.datos.html = converter.makeHtml($scope.datos.markup);
      $scope.$apply();
      var h = $('#ayudaMarkup').height() - 70;
      $('#markup').height(h + 'px');
      $('#html').height(h + 'px');
      if ($scope.modoEdicion) {
        setTimeout(function() {
          $scope.refrescar();
        }, 2000);
      }
    };
    $scope.toggleEdicion = function() {
      $scope.modoEdicion = !$scope.modoEdicion;
      if ($scope.modoEdicion) {
        setTimeout(function() {
          console.log('dividiendo!');
          Split(['#ayudaMarkup', '#ayudaHtml'], {
            sizes: [50, 50],
            cursor: 'col-resize',
            glutterSize: 8
          });
        }, 200);
        setTimeout(function() {
          $scope.refrescar();
        }, 100);
      } else {
        var h = $('#ayudaContainer').height() - 70;
        console.log('Seteando alto de ventanaAyuda: ' + h + 'px');
        $('#ventanaAyuda').css({ height: h + 'px' });
      }
    };
    $rootScope.$on('desplegarAyuda', function(event, args) {
      var archivo = $scope.generarNombreDeArchivo();
      console.log('RUTA: ' + window.location.href);
      console.log('Archivos: ' + archivo);
      $scope.datos.html =
        "<br/><br/><br/><br/><center>Estamos cargando la página de ayuda, solo son unos segundos!<br/><br/><img width='50px' src='assets/img/logos/FEBOS_LOADER.svg'/></center>";
      $scope.cargarArchivo(archivo);
    });

    $scope.generarNombreDeArchivo = function() {
      return (
        window.location.href
          .split('#')[1]
          .split('?')[0]
          .substring(1)
          .replace(/\//g, '.') + '.md'
      );
    };
    $scope.cargarArchivo = function(nombre) {
      if (typeof SesionFebos().ayuda == 'undefined') {
        SesionFebos().ayuda = {};
      }
      if (typeof SesionFebos().ayuda[nombre] == 'undefined') {
        FebosAPI.cl_obtener_archivo(
          { ruta: '/febos-io/chile/' + FebosUtil.obtenerAmbiente() + '/ayuda/' + nombre },
          {},
          false,
          false
        ).then(function(response) {
          try {
            console.log(response);
            if (response.data.codigo == 10) {
              $scope.hash = response.data.hash;
              $scope.desplegarContenidoAyuda(response.data.contenido);
              SesionFebos().ayuda[nombre] = response.data.contenido;
            } else if (response.data.codigo == 121) {
              var mensaje =
                '\n\n\nUps! Aún estamos trabajando en la ayuda de esta pantalla, porfavor despliega el chat de ayuda y con gusto atenderemos todas tus consultas.';
              $scope.desplegarContenidoAyuda(btoa(mensaje));
            }
          } catch (e) {
            console.log(e);
          }
        });
      } else {
        $scope.desplegarContenidoAyuda(SesionFebos().ayuda[nombre]);
        FebosQueueAPI(
          'cl_obtener_archivo',
          {
            ruta: '/febos-io/chile/' + FebosUtil.obtenerAmbiente() + '/ayuda/' + nombre
          },
          {}
        ).then(function(response) {
          if (response.data.codigo == 10) {
            SesionFebos().ayuda[nombre] = response.data.contenido;
            $scope.hash = response.data.hash;
            $scope.desplegarContenidoAyuda(response.data.contenido);
          }
        });
      }
    };
    $scope.desplegarContenidoAyuda = function(contenidoEnBase64) {
      var contenido = atob(contenidoEnBase64);
      $scope.datos.markup = contenido;
      $scope.datos.html = converter.makeHtml(contenido);
      var h = $('#ayudaContainer').height() - 70;
      $('#ventanaAyuda').css({ height: h + 'px' });
      setTimeout(function() {
        $scope.refrescar();
      }, 100);
    };
    $scope.guardarAyuda = function() {
      var nombre = $scope.generarNombreDeArchivo();
      var contenido = btoa($scope.datos.markup);
      var img =
        "<img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>";
      $rootScope.blockModal = UIkit.modal.blockUI(
        '<div id="login-modal" class=\'uk-text-center\'>Guardando ayuda...<br/>' + img + '</div>'
      );
      FebosAPI.cl_guardar_archivo(
        {
          ruta: '/febos-io/chile/' + FebosUtil.obtenerAmbiente() + '/ayuda/' + nombre,
          hash: $scope.hash
        },
        {
          payload: contenido
        },
        true,
        true
      ).then(function(response) {
        SesionFebos().ayuda[nombre] = btoa($scope.datos.markup);
        $scope.toggleEdicion();
        UIkit.notify(
          "Se ha guardado correctamente esta página de ayuda <a class='notify-action'>[X]</a> ",
          { status: 'success', timeout: 2000 }
        );
      });
    };
  }
]);
