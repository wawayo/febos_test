var JSONE = {};

angular.module('febosApp').controller('novedadEditarCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$location',
  'variables',
  '$http',
  '$stateParams',
  '$state',
  'FebosAPI',
  'FebosUtil',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    variables,
    $http,
    $stateParams,
    $state,
    FebosAPI,
    FebosUtil
  ) {
    try {
      $scope.ckeditor_inline = '';
      $scope.letrasTitulo = 0;
      $scope.letrasResumen = 0;
      $scope.app = $stateParams.app;
      $scope.datos = {
        titulo: '',
        resumen: '',
        linkImagen: '',
        linkVideo: '',
        visible: false,
        payload: ''
      };
      $scope.cargando = true;

      FebosAPI.cl_ver_novedad(
        {
          novedadId: $stateParams.novedadId
        },
        true,
        false
      ).then(function(response) {
        $scope.datos.titulo = response.data.novedad.titulo;
        $scope.datos.resumen = response.data.novedad.resumen;
        $scope.datos.linkVideo = response.data.novedad.videoCabeceraUrl;
        $scope.datos.linkImagen = response.data.novedad.imagenCabeceraUrl;
        $scope.datos.visible = response.data.novedad.visible;
        $scope.cargando = false;
      });

      var req = {
        method: 'GET',
        url: 'https://archivos.febos.io/chile/novedades/' + $stateParams.novedadId + '.html'
      };
      $http(req).then(
        function(response) {
          console.log(response);
          $scope.ckeditor_inline = response.data;
        },
        function(response) {
          console.log(response);
        }
      );

      $scope.contarLetras = function(tipo) {
        if (tipo == 'titulo') {
          $scope.letrasTitulo = $scope.datos.titulo.length;
          if ($scope.letrasTitulo > 300) {
            $scope.datos.titulo = $scope.datos.titulo.substring(0, 300);
            $scope.letrasTitulo = $scope.datos.titulo.length;
          }
        } else {
          $scope.letrasResumen == $scope.datos.resumen.length;
          if ($scope.letrasResumen > 300) {
            $scope.datos.resumen = $scope.datos.resumen.substring(0, 300);
            $scope.letrasResumen = $scope.datos.resumen.length;
          }
        }
      };

      $scope.ckeditor_options = {
        allowedContent: true,
        customConfig: '../../assets/js/custom/ckeditor_config.js',
        contenteditable: true
      };

      $scope.creando = false;
      $scope.editar = function() {
        $scope.creando = true;
        modal = UIkit.modal.blockUI(
          "<div class='uk-text-center'>Guardando" +
            ($scope.datos.visible ? '' : ' en privado') +
            "...<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
        );
        if ($scope.datos.linkVideo === '') {
          $scope.datos.linkVideo = null;
        }
        var body = $scope.datos;
        //console.log("$scope.ckeditor_inline",$scope.ckeditor_inline);
        body.payload = FebosUtil.encode($('#id_ckeditor_edit').html());
        console.log($('#id_ckeditor_edit').html());
        console.log(body.payload);
        console.log(FebosUtil.encode(body.payload));
        FebosAPI.cl_editar_novedad(
          {
            novedadId: $stateParams.novedadId,
            simular: 'no',
            debug: 'si'
          },
          body,
          true,
          false
        ).then(function(response) {
          $scope.creando = false;
          modal.hide();
          console.log(response);
          setTimeout(function() {
            $state.go('restringido.verNovedad', {
              novedadId: $stateParams.novedadId,
              app: $stateParams.app
            });
          }, 150);
        });
      };
    } catch (e) {
      console.log('Error', e);
    }
  }
]);
