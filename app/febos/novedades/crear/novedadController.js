var JSONE = {};

angular.module('febosApp').controller('novedadCrearCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$location',
  'variables',
  '$http',
  '$state',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  '$stateParams',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    variables,
    $http,
    $state,
    SesionFebos,
    FebosAPI,
    FebosUtil,
    $stateParams
  ) {
    $scope.ckeditor_inline = '';
    $scope.letrasTitulo = 0;
    $scope.letrasResumen = 0;
    $scope.app = $stateParams.app;
    $scope.datos = {
      titulo: '',
      resumen: '',
      linkImagen: '',
      linkVideo: '',
      visible: false,
      payload: ''
    };

    $scope.contarLetras = function(tipo) {
      if (tipo == 'titulo') {
        $scope.letrasTitulo = $scope.datos.titulo.length;
        if ($scope.letrasTitulo > 300) {
          $scope.datos.titulo = $scope.datos.titulo.substring(0, 300);
          $scope.letrasTitulo = $scope.datos.titulo.length;
        }
      } else {
        $scope.letrasResumen == $scope.datos.resumen.length;
        if ($scope.letrasResumen > 300) {
          $scope.datos.resumen = $scope.datos.resumen.substring(0, 300);
          $scope.letrasResumen = $scope.datos.resumen.length;
        }
      }
    };
    $scope.ckeditor_options = {
      customConfig: '../../assets/js/custom/ckeditor_config.js',
      allowedContent: true
    };
    $scope.creando = false;

    $scope.publicar = function() {
      $scope.creando = true;
      modal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Publicando" +
          ($scope.datos.visible ? '' : ' en privado') +
          "...<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
      );

      var body = $scope.datos;
      //console.log("$scope.ckeditor_inline",$scope.ckeditor_inline);
      body.payload = FebosUtil.encode($scope.ckeditor_inline);

      FebosAPI.cl_nueva_novedad({}, body, true, false).then(function(response) {
        $scope.creando = false;
        modal.hide();
        console.log(response);
        setTimeout(function() {
          $state.go('restringido.verNovedad', {
            novedadId: response.data.novedadId,
            app: $stateParams.app
          });
        }, 150);
      });
    };
  }
]);
