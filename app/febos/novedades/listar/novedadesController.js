var JSONE = {};

angular.module('febosApp').controller('novedadesListarCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$location',
  'variables',
  '$http',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  '$stateParams',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    variables,
    $http,
    SesionFebos,
    FebosAPI,
    FebosUtil,
    $stateParams
  ) {
    $scope.cargando = true;
    $scope.novedades = [];
    $scope.puedeEditar = false;
    $scope.app = $stateParams.app;
    setTimeout(function() {
      $scope.puedeEditar = FebosUtil.tienePermiso('FEB99');
      //console.log("puede editar", $scope.puedeEditar);
      $scope.$apply();
    }, 4000);

    FebosAPI.cl_listar_novedades(
      {
        limite: '40',
        mostrarTodas: 'si'
      },
      true,
      false
    ).then(function(response) {
      console.log(response);
      $scope.novedades = response.data.novedades;
      $scope.cargando = false;
    });

    $scope.nuevo = function() {
      //window.location = "#"+$stateParams.app+"/novedades/escribir";
      var url = '/' + $stateParams.app + '/novedades/escribir';
      $location.path(url);
    };
  }
]);
