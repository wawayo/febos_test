var JSONE = {};

angular.module('febosApp').controller('novedadVerCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$location',
  'variables',
  '$stateParams',
  '$http',
  'FebosAPI',
  'FebosUtil',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    variables,
    $stateParams,
    $http,
    FebosAPI,
    FebosUtil
  ) {
    $scope.contenido = 'Cargando...';
    $scope.cargando = true;
    $scope.puedeEditar = false;
    $scope.app = $stateParams.app;
    //console.log("puede editar",$scope.puedeEditar);
    setTimeout(function() {
      $scope.puedeEditar = FebosUtil.tienePermiso('FEB99');
      //console.log("puede editar",$scope.puedeEditar);
    }, 2000);

    $scope.novedad = {};

    FebosAPI.cl_ver_novedad(
      {
        novedadId: $stateParams.novedadId
      },
      true,
      false
    ).then(function(response) {
      //console.log("api",response);
      $scope.novedad = response.data.novedad;
      $scope.nombreAutor = response.data.nombreAutor;
      $scope.correoAutor = response.data.correoAutor;
      $scope.cargando = false;
    });

    var req = {
      method: 'GET',
      url: 'https://archivos.febos.io/chile/novedades/' + $stateParams.novedadId + '.html'
    };
    $http(req).then(
      function(response) {
        //console.log("archivo",response);
        $scope.contenido = response.data;
      },
      function(response) {
        console.log(response);
      }
    );
  }
]);
