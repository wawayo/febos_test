febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider
                .state("restringido.listarNovedades", {
                    url: "/:app/novedades",
                            templateUrl: 'app/febos/novedades/listar/novedadesView.html',
                            controller: 'novedadesListarCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/novedades/listar.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_clndr',
                                            'app/febos/novedades/listar/novedadesController.js'
                                        ], {serie: true});
                                    }]
                            }
                })
                .state("restringido.crearNovedad", {
                    url: "/:app/novedades/escribir",
                    templateUrl: 'app/febos/novedades/crear/novedadView.html',
                    controller: 'novedadCrearCtrl',
                    resolve: {
                        
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_ckeditor',
                                    'lazy_clndr',
                                    'app/febos/novedades/crear/novedadController.js'
                                ], {serie: true});
                            }]
                    }
                })
                .state("restringido.verNovedad", {
                    url: "/:app/novedades/ver/:novedadId",
                    templateUrl: 'app/febos/novedades/ver/novedadView.html',
                    controller: 'novedadVerCtrl',
                    resolve: {                       
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_clndr',
                                    'app/febos/novedades/ver/novedadController.js'
                                ], {serie: true});
                            }]
                    }
                })
                .state("restringido.editarNovedad", {
                    url: "/:app/novedades/editar/:novedadId",
                    templateUrl: 'app/febos/novedades/editar/novedadView.html',
                    controller: 'novedadEditarCtrl',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_ckeditor',
                                    'lazy_clndr',                                    
                                    'app/febos/novedades/editar/novedadController.js'
                                ], {serie: true});
                            }]
                    }
                })

            }]);
