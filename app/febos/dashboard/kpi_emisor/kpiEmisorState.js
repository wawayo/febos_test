febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.emisorkpi", {
                    url: "/:app/kpiEmisor",
                    templateUrl: 'app/febos/dashboard/kpi_emisor/kpiEmisorView.html',
                    controller: 'kpiEmisorCtrl',
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_countUp',
                                'lazy_charts_peity',
                                'lazy_charts_easypiechart',
                                'lazy_charts_metricsgraphics',
                                'lazy_charts_chartist',
                                'lazy_weathericons',
                                'lazy_google_maps',
                                'lazy_clndr',
                                'lazy_charts_c3',
                                'app/febos/dashboard/kpi_emisor/kpiEmisorController.js'
                            ], {serie: true});
                        }]
                    },
                    data: {
                        pageTitle: 'KPI Emisor'
                    },
                    ncyBreadcrumb: {
                        label: 'Home'
                    }
                })
            }]);