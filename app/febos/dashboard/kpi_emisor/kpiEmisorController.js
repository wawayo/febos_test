var JSONE = {};
var yaCorriendo = false;
var modalKPI;
angular.module('febosApp').controller('kpiEmisorCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$location',
  'variables',
  '$http',
  '$window',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    variables,
    $http,
    $window,
    SesionFebos,
    FebosAPI,
    FebosUtil
  ) {
    $scope.cargando = false;
    $scope.opcionesCargadas = false;
    var hoy = new Date();
    var hace60Dias = new Date();
    hace60Dias.setDate(hoy.getDate() - 60);
    var desde = FebosUtil.formatearFecha(hace60Dias);
    var hasta = FebosUtil.formatearFecha(hoy);
    if (desde.split('-')[0] != hasta.split('-')[0]) {
      desde = hasta.split('-')[0] + '-01-01';
    }
    //configuracion de filtros
    //Math.round(num * 100) / 100

    $scope.opciones = {
      unidades: [{ nombre: 'Todas', valor: '' }],
      clientes: [{ nombre: 'Todos', valor: '' }]
    };
    $scope.selecciones = {
      unidad: '',
      cliente: '',
      desde: desde,
      hasta: hasta
    };

    $scope.selectizeConfig = {
      plugins: {
        remove_button: {
          label: ''
        }
      },
      maxItems: null,
      valueField: 'valor',
      labelField: 'nombre',
      searchField: ['valor', 'nombre'],
      create: false,
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.nombre) + '</a></div>';
        }
      }
    };

    $scope.generarGraficoCompras = function(datos) {
      var sumaTotalActual = 0;
      var sumaTotalAnterior = 0;
      var anioActual = parseInt($scope.selecciones.desde.split('-')[0]);
      var anioAnterior = parseInt($scope.selecciones.desde.split('-')[0]) - 1;
      for (i = 0; i < datos.length; i++) {
        console.log(datos[i].fecha, datos[i].fecha.indexOf(anioActual));
        if (datos[i].fecha.indexOf(anioActual) >= 0) {
          //console.log("AÑO ACTUAL!");
          sumaTotalActual += datos[i].monto;
        } else {
          sumaTotalAnterior += datos[i].monto;
        }
      }
      for (i = 0; i < datos.length; i++) {
        if (datos[i].fecha.indexOf(anioActual) >= 0) {
          datos[i].porcentaje = Math.round(((datos[i].monto * 100) / sumaTotalActual) * 100) / 100;
        } else {
          datos[i].porcentaje =
            Math.round(((datos[i].monto * 100) / sumaTotalAnterior) * 100) / 100;
        }
      }
      console.log(sumaTotalActual);
      console.log(sumaTotalAnterior);
      console.log(datos);
      var fechas = ['x'];
      var pesoComprasActual = ['$ Ventas ' + anioActual];
      var pesoComprasAnterior = ['$ Ventas ' + anioAnterior];
      var percentComprasActual = ['% ' + anioActual];
      var percentComprasAnterior = ['% ' + anioAnterior];
      for (i = 0; i < datos.length; i++) {
        if (datos[i].fecha.indexOf(anioActual) >= 0) {
          fechas.push(datos[i].fecha);
          pesoComprasActual.push(datos[i].monto);
          percentComprasActual.push(datos[i].porcentaje);
        } else {
          pesoComprasAnterior.push(datos[i].monto);
          percentComprasAnterior.push(datos[i].porcentaje);
        }
      }

      var axes = {};
      var types = {};
      axes['$ Ventas ' + anioAnterior] = 'y';
      axes['$ Ventas ' + anioActual] = 'y';
      axes['% ' + anioAnterior] = 'y2';
      axes['% ' + anioActual] = 'y2';
      types['$ Ventas ' + anioAnterior] = 'spline';
      types['$ Ventas ' + anioActual] = 'spline';
      types['% ' + anioAnterior] = 'bar';
      types['% ' + anioActual] = 'bar';
      var data = {
        x: 'x',
        columns: [
          fechas,
          pesoComprasAnterior,
          pesoComprasActual,
          percentComprasAnterior,
          percentComprasActual
          /*['x', '2017-01-01', '2017-02-01', '2017-03-01', '2017-04-01', '2017-05-01', '2017-06-01'],
                             ['$ Compras '+(parseInt(anio)-1), 3033000, 20000000, 10000000, 40000000, 15000000, 25000000],
                             ['$ Compras '+anio, 13004000, 10000000, 14000000, 20000000, 10050000, 5000000],
                             ['% '+(parseInt(anio)-1), 5, 30, 20, 15, 7, 5],
                             ['% '+anio, 30, 10, 15, 10, 5, 7]*/
        ],
        types: types,
        /*{
                         '$ Compras 2016': 'spline', //spline line, bar, o area
                         '$ Compras 2017': 'spline',
                         '% 2016': 'bar',
                         '% 2017': 'bar'
                         }*/ axes: axes
        /*{
                         '$ Compras 2016': 'y',
                         '$ Compras 2017': 'y',
                         '% 2016': 'y2',
                         '% 2017': 'y2'
                         }*/
      };
      console.log(data);
      c3.generate({
        bindto: '#grafico_compras',
        data: data,
        color: {
          pattern: ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728']
        },
        axis: {
          y: {
            tick: {
              format: function(d) {
                return '$ ' + (d / 1000000).toFixed(2).toLocaleString('de-DE') + ' MM';
              }
            }
          },
          y2: {
            show: true,
            tick: {
              format: function(d) {
                return d + '%';
              }
            }
          },
          x: {
            type: 'timeseries',
            tick: {
              format: function(x) {
                var meses = [
                  'Enero',
                  'Febrero',
                  'Marzo',
                  'Abril',
                  'Mayo',
                  'Junio',
                  'Julio',
                  'Agosto',
                  'Septiembre',
                  'Octubre',
                  'Noviembre',
                  'Diciembre'
                ];
                return meses[x.getMonth()];
              }
            }
          }
        }
      });
    };

    $scope.generarDonaRecibidos = function(tipo, fuente, titulo, idContainer) {
      //tipo puede ser '-', '#' o '$'
      var datos = [];
      for (var key in fuente) {
        datos.push([key, fuente[key][tipo]]);
      }
      console.log(datos);
      c3.generate({
        bindto: idContainer,
        data: {
          columns: datos,
          type: 'donut',
          onclick: function(d, i) {
            ////console.log("onclick", d, i);
          },
          onmouseover: function(d, i) {
            ////console.log("onmouseover", d, i);
          },
          onmouseout: function(d, i) {
            ////console.log("onmouseout", d, i);
          }
        },
        tooltip: {
          format: {
            //title: function (d) {
            //    return 'Data ' + d;
            //},
            value: function(value, ratio, id) {
              switch (tipo) {
                case '$':
                  return '$ ' + (value / 1000000).toFixed(2).toLocaleString('de-DE') + ' MM';
                default:
                  return value;
              }
            }
          }
        },
        donut: {
          title: titulo,
          width: 40
          /*label: {
                     format: function (value) {
                     return ((value / 1000000).toFixed(2)).toLocaleString('de-DE') + " MM";
                     }
                     }*/
        },
        color: {
          pattern: [
            '#1f77b4',
            '#ff7f0e',
            '#2ca02c',
            '#d62728',
            '#9467bd',
            '#8c564b',
            '#e377c2',
            '#7f7f7f',
            '#bcbd22',
            '#17becf'
          ]
        }
      });
    };

    $scope.generarKPI = function() {
      $scope.cargando = true;
      modalKPI = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Cargando KPI...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      FebosAPI.cl_kpi_emisor(
        {
          empresaId: SesionFebos().empresa.id,
          identificador: 'EMISION',
          desde: $scope.selecciones.desde,
          hasta: $scope.selecciones.hasta,
          grupos: $scope.selecciones.unidad,
          clientes: $scope.selecciones.cliente
        },
        {},
        false,
        false
      ).then(
        function(response) {
          modalKPI.hide();
          if (response.data.codigo == 10) {
            console.log(response);
            $scope.cargarOpciones(response.data);
            $scope.generarGraficoCompras(response.data.kpi.ventas);
            $scope.cargando = false;
          } else {
            UIkit.modal.alert(response.data.errores[0]);
          }
        },
        function(error) {
          // hacer algo con el error
          modalKPI.hide();
          console.log(error);
          UIkit.modal.alert(error.data.errores[0]);
        }
      );
    };

    $scope.cargarOpciones = function(data) {
      if ($scope.opcionesCargadas) return;
      $scope.opcionesCargadas = true;
      $scope.opciones = {
        unidades: [{ nombre: 'Todas', valor: '' }],
        clientes: [{ nombre: 'Todos', valor: '' }]
      };
      for (i = 0; i < data.clientes.length; i++) {
        if (data.clientes[i].nombre == '')
          data.clientes[i].nombre = '(' + data.clientes[i].rut + ')';
        $scope.opciones.clientes.push({
          nombre: data.clientes[i].nombre,
          valor: data.clientes[i].rut
        });
      }
      for (i = 0; i < data.grupos.length; i++) {
        $scope.opciones.unidades.push({
          nombre: data.grupos[i].nombre,
          valor: data.grupos[i].id
        });
      }
    };
    //No cargar los indicadores al iniciar página
    //$scope.generarKPI();
    $scope.exportar = function(idContainer, nombreArchivo) {
      //console.log("exportar");
      function createChartImages(element, config) {
        //console.log("createChartImages");
        var chartEl = $(element);
        var svgEl = $(element.find('svg')).first()[0];
        var svgCopyEl = angular.element(svgEl.outerHTML)[0];
        var canvasEl = angular.element('<canvas id="canvasOriginal"></canvas>')[0];
        var emptySvgEl = angular.element(
          '<svg id="emptysvg" xmlns="http://www.w3.org/2000/svg" version="1.1" height="2" />'
        )[0];
        var emptyCanvasEl = angular.element('<canvas id="canvasComputed"></canvas>')[0];
        if (config.removeDefs) {
          $(svgCopyEl)
            .find('defs')
            .remove();
        }

        canvasEl.width = chartEl.width();
        emptyCanvasEl.width = chartEl.width();
        canvasEl.height = chartEl.height();
        emptyCanvasEl.height = chartEl.height();
        var container = angular.element('<div style="display: none;" class="c3"></div>');
        element.append(container);
        container.append(canvasEl);
        container.append(emptyCanvasEl);
        container.append(emptySvgEl);
        container.append(svgCopyEl);
        exportSvgToCanvas(svgCopyEl, canvasEl);
        var canvasComputed = exportStyles(canvasEl, emptyCanvasEl, svgCopyEl, emptySvgEl);
        exportSvgToCanvas(svgCopyEl, canvasComputed);
        exportCanvasToPng(chartEl.find('.savePNG'), canvasComputed, config.exportedFileName);
        canvasEl.remove();
        emptyCanvasEl.remove();
        emptySvgEl.remove();
        svgCopyEl.remove();
      }

      function exportSvgToCanvas(svg, canvas) {
        //console.log("exportSvgToCanvas");
        canvg(canvas, new XMLSerializer().serializeToString(svg));
      }

      function exportCanvasToPng(linkEl, canvasEl, filename) {
        //window.location = canvasEl.toDataURL('png');
        /*linkEl.attr('href', canvasEl.toDataURL('png'))
                 .attr('download', function () {
                 return filename + '.png';
                 });
                 //console.log("LINK DESCARGA",linkEl.html());
                 linkEl.click();*/
        var link = document.createElement('a');
        link.href = canvasEl.toDataURL('png');
        link.download = filename + '.png';
        document.body.appendChild(link);
        try {
          setTimeout(function() {
            link.click();
          }, 150);
        } catch (e) {}
      }

      function exportCanvasToImage(canvasComputed) {
        //console.log("exportCanvasToImage");
        Canvas2Image.saveAsPNG(canvasComputed);
      }

      function exportStyles(canvasOriginal, canvasComputed, svg, emptySvg) {
        //console.log("exportStyles");
        var tree = [];
        var emptySvgDeclarationComputed = getComputedStyle(emptySvg);
        var allElements = traverse(svg, tree);
        var i = allElements.length;
        while (i--) {
          explicitlySetStyle(allElements[i], emptySvgDeclarationComputed);
        }

        return canvasComputed;
      }

      function traverse(obj, tree) {
        //console.log("traverse");
        tree.push(obj);
        if (obj.hasChildNodes()) {
          var child = obj.firstChild;
          while (child) {
            if (child.nodeType === 1 && child.nodeName != 'SCRIPT') {
              traverse(child, tree);
            }
            child = child.nextSibling;
          }
        }
        return tree;
      }

      function explicitlySetStyle(element, emptySvgDeclarationComputed) {
        //console.log("explicitlySetStyle");
        var cSSStyleDeclarationComputed = getComputedStyle(element);
        var i, len, key, value;
        var computedStyleStr = '';
        for (i = 0, len = cSSStyleDeclarationComputed.length; i < len; i++) {
          key = cSSStyleDeclarationComputed[i];
          value = cSSStyleDeclarationComputed.getPropertyValue(key);
          if (value !== emptySvgDeclarationComputed.getPropertyValue(key)) {
            if (key == 'visibility' && value == 'hidden') {
              computedStyleStr += 'display: none;';
            } else {
              computedStyleStr += key + ':' + value + ';';
            }
          }
        }
        element.setAttribute('style', computedStyleStr);
      }

      var container = $(idContainer);
      var config = {
        exportedFileName: nombreArchivo,
        removeDefs: true
      };
      createChartImages(container, config);
    };
    $scope.tareaId = '';
    $scope.tipoCSV = '';
    $scope.csv = function(tipo) {
      $scope.tipoCSV = tipo;
      var Base64 = {
        _keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',
        encode: function(e) {
          var t = '';
          var n, r, i, s, o, u, a;
          var f = 0;
          e = Base64._utf8_encode(e);
          while (f < e.length) {
            n = e.charCodeAt(f++);
            r = e.charCodeAt(f++);
            i = e.charCodeAt(f++);
            s = n >> 2;
            o = ((n & 3) << 4) | (r >> 4);
            u = ((r & 15) << 2) | (i >> 6);
            a = i & 63;
            if (isNaN(r)) {
              u = a = 64;
            } else if (isNaN(i)) {
              a = 64;
            }
            t =
              t +
              this._keyStr.charAt(s) +
              this._keyStr.charAt(o) +
              this._keyStr.charAt(u) +
              this._keyStr.charAt(a);
          }
          return t;
        },
        decode: function(e) {
          var t = '';
          var n, r, i;
          var s, o, u, a;
          var f = 0;
          e = e.replace(/[^A-Za-z0-9+/=]/g, '');
          while (f < e.length) {
            s = this._keyStr.indexOf(e.charAt(f++));
            o = this._keyStr.indexOf(e.charAt(f++));
            u = this._keyStr.indexOf(e.charAt(f++));
            a = this._keyStr.indexOf(e.charAt(f++));
            n = (s << 2) | (o >> 4);
            r = ((o & 15) << 4) | (u >> 2);
            i = ((u & 3) << 6) | a;
            t = t + String.fromCharCode(n);
            if (u != 64) {
              t = t + String.fromCharCode(r);
            }
            if (a != 64) {
              t = t + String.fromCharCode(i);
            }
          }
          t = Base64._utf8_decode(t);
          return t;
        },
        _utf8_encode: function(e) {
          e = e.replace(/rn/g, 'n');
          var t = '';
          for (var n = 0; n < e.length; n++) {
            var r = e.charCodeAt(n);
            if (r < 128) {
              t += String.fromCharCode(r);
            } else if (r > 127 && r < 2048) {
              t += String.fromCharCode((r >> 6) | 192);
              t += String.fromCharCode((r & 63) | 128);
            } else {
              t += String.fromCharCode((r >> 12) | 224);
              t += String.fromCharCode(((r >> 6) & 63) | 128);
              t += String.fromCharCode((r & 63) | 128);
            }
          }
          return t;
        },
        _utf8_decode: function(e) {
          var t = '';
          var n = 0;
          var r = (c1 = c2 = 0);
          while (n < e.length) {
            r = e.charCodeAt(n);
            if (r < 128) {
              t += String.fromCharCode(r);
              n++;
            } else if (r > 191 && r < 224) {
              c2 = e.charCodeAt(n + 1);
              t += String.fromCharCode(((r & 31) << 6) | (c2 & 63));
              n += 2;
            } else {
              c2 = e.charCodeAt(n + 1);
              c3 = e.charCodeAt(n + 2);
              t += String.fromCharCode(((r & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
              n += 3;
            }
          }
          return t;
        }
      };
      modalKPI = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Generando archivo para descarga...<br/><span id='avance'></span><br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      var subreq = {};
      if (tipo === 'recibidos') {
        var subreq = {
          campos:
            'tipoDocumento,folio,rutEmisor,razonSocialEmisor,rutReceptor,razonSocialReceptor,rutCesionario,razonSocialCesionario,fechaCesion,codigoSii,fechaEmision,fechaRecepcion,fechaRecepcionSii,estadoComercial,fechaReciboMercaderia,formaDePago,montoTotal,contacto,correoReceptor',
          filtros:
            'fechaRecepcion:' +
            $scope.selecciones.desde +
            '--' +
            $scope.selecciones.hasta +
            '|rutReceptor:60706000-2|estadoSii:4,5',
          itemsPorPagina: 24000000,
          orden: '-fechaRecepcion',
          pagina: 1
        };
      } else {
        var subreq = {
          campos:
            'tipoDocumento,folio,rutEmisor,razonSocialEmisor,rutReceptor,razonSocialReceptor,rutCesionario,razonSocialCesionario,fechaCesion,codigoSii,fechaEmision,fechaRecepcion,fechaRecepcionSii,estadoComercial,fechaReciboMercaderia,formaDePago,montoTotal,contacto,correoReceptor',
          filtros:
            'fechaRecepcion:' +
            $scope.selecciones.desde +
            '--' +
            $scope.selecciones.hasta +
            '|rutReceptor:60706000-2|estadoSii:4,5|estadoComercial:3,4,5,6,7',
          itemsPorPagina: 24000000,
          orden: '-fechaRecepcion',
          pagina: 1
        };
      }
      if ($scope.selecciones.cliente != '' && $scope.selecciones.cliente.length > 5) {
        subreq.filtros += '|rutEmisor:' + $scope.selecciones.cliente;
      }

      //falta para la unidad, aun no definido

      //console.log("subrequest", subreq);
      var reqEncoded = Base64.encode(JSON.stringify(subreq));
      $('#avance').html('Espere un momento...');
      FebosAPI.cl_encolar_tarea_background(
        {
          proceso: 'cl_listar_dte'
        },
        { payload: reqEncoded },
        true,
        false
      ).then(
        function(response) {
          console.log(response);
          $scope.tareaId = response.data.tareaId;
          $('#avance').html('Seleccionando documentos...');
          $scope.avance();
        },
        function(response) {
          modalKPI.hide();
          console.log(response);
          $scope.cargando = false;
        }
      );
    };

    $scope.avance = function() {
      FebosAPI.cl_consulta_estado_tarea_background(
        {
          tareaId: $scope.tareaId
        },
        {},
        true,
        false
      ).then(function(response) {
        //console.log("RESPUESTA", response);
        yaCorriendo = false;
        if (response.data.estado < 3) {
          $('#avance').html(response.data.avance + '%');
          setTimeout(function() {
            $scope.avance();
          }, 3000);
        }
        if (response.data.estado == 3) {
          $('#avance').html('100%');
          var r = {
            method: 'GET',
            url: response.data.resultado,
            headers: {
              'Content-Type': 'text/plain'
            }
          };
          console.log(r);
          //viviana: No se puede factorizar porque no es una llamada a la API Gateway
          $http(r).then(
            function(data) {
              $('#avance').html('Listo!');
              $scope.construirCSV(data.data);
              modalKPI.hide();
            },
            function(data) {
              //console.log("ERROR!");
            }
          );
        }
        if (response.data.estado == 4) {
          $('#avance').html('Ups ocurrio un error!');
        }
      });
    };
    $scope.construirCSV = function(json) {
      var csv = $scope.obtenerCabeceras(json.documentos[0]) + '\n';
      for (var i = 0; i < json.documentos.length; i++) {
        csv += $scope.obtenerValores(json.documentos[i]) + '\n';
      }
      try {
        setTimeout(function() {
          var blob = $scope.b64toBlob(FebosUtil.encode(csv), 'text/csv');
          var blobUrl = URL.createObjectURL(blob);
          window.location = blobUrl;
        }, 150);
      } catch (e) {}
    };
    $scope.obtenerCabeceras = function(primeraFila) {
      var cabeceras = [];
      for (campo in primeraFila) {
        cabeceras.push(campo);
      }
      return cabeceras.join(',');
    };
    $scope.obtenerValores = function(fila) {
      var valores = [];
      for (campo in fila) {
        switch (campo) {
          case 'tipoDocumento':
            var tp = '';
            switch (fila[campo] + '') {
              case '33':
                tp = 'Factura Electrónica Afecta';
                break;
              case '34':
                tp = 'Factura Electrónica Exenta';
                break;
              case '39':
                tp = 'Boleta Electrónica Afecta';
                break;
              case '41':
                tp = 'Boleta Electrónica Exenta';
                break;
              case '43':
                tp = 'Liquidación de Factura Electrónica';
                break;
              case '46':
                tp = 'Factura de Compra Electrónica';
                break;
              case '56':
                tp = 'Nota de Débito Electrónica';
                break;
              case '61':
                tp = 'Nota de Crédito Electrónica';
                break;
              case '110':
                tp = 'Factura de Exportación Electrónica';
                break;
              case '111':
                tp = 'Nota de Débito de Exportación Electrónica';
                break;
              case '112':
                tp = 'Nota de Crédito de Exportación Electrónica';
                break;
            }
            valores.push('"' + tp + ' (' + fila[campo] + ')"');
            break;
          case 'formaDePago':
            var tp = '';
            switch (fila[campo]) {
              case '1':
                tp = 'Contado';
              case '3':
                tp = 'Sin costo';
              default:
                tp = 'Crédito';
            }
            valores.push('"' + tp + '"');
            break;
          case 'estadoComercial':
            var tp = '';
            switch (fila[campo]) {
              case '1':
                tp = 'ACEPTADO';
                break;
              case '2':
                tp = 'ACEPTADO EN EL SII';
                break;
              case '3':
                tp = 'PRE-RECHAZADO';
                break;
              case '4':
                tp = 'RECHAZADO EN EL SII';
                break;
              case '5':
                tp = 'RECLAMO PARCIAL EN EL SII';
                break;
              case '6':
                tp = 'RECLAMO TOTAL EN EL SII';
                break;
              case '7':
                tp = 'RECIBO MERCADERIAS EN EL SII';
                break;
              default:
                tp = 'SIN ACCIÓN COMERCIAL';
            }
            valores.push('"' + tp + '"');
            break;
          default:
            if (typeof fila[campo] == 'number') {
              valores.push(fila[campo]);
            } else {
              valores.push('"' + fila[campo] + '"');
            }
        }
      }
      return valores.join(';');
    };

    $scope.b64toBlob = function(b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;
      var byteCharacters = atob(b64Data);
      var byteArrays = [];
      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, { type: contentType });
      return blob;
    };
  }
]);
