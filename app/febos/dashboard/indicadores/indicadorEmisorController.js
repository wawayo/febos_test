angular.module('febosApp').controller('indicadorEmisorCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$http',
  'utils',
  '$state',
  '$location',
  'SesionFebos',
  'FebosAPI',
  '$stateParams',
  'Datos',
  'FebosUtil',
  'usuarios',
  'grupos',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $http,
    utils,
    $state,
    $location,
    SesionFebos,
    FebosAPI,
    $stateParams,
    Datos,
    FebosUtil,
    usuarios,
    grupos
  ) {
    $scope.usuario = SesionFebos().usuario;
    //$scope.AYUDA = AYUDA;

    $scope.valor = $stateParams.valor;
    $scope.query = '';
    $scope.totalEmitidos = '';
    $scope.enviadoSII = '';
    $scope.aprobadoReparos = '';
    $scope.aprobados = '';
    $scope.rechazados = '';
    $scope.entregadoCliente = '';
    $scope.aprobadosCliente = '';
    $scope.rechazadosCliente = '';
    $scope.reciboMercaderia = '';
    $scope.docsEmitidos = {};
    $scope.plazoRechazo = {};
    $scope.opcion =
      (typeof $stateParams.opcion == 'undefined' || $stateParams.opcion == ''
        ? '3'
        : $stateParams.opcion) + '';
    $scope.itemsEspecificos = [];

    $scope.correoscope = '';
    $scope.gruposcope = '';

    //console.log("usuario",$scope.usuario);
    //console.log("params",$stateParams);
    //console.log("scope",$scope.especifico);

    if (Datos != null && Datos != undefined) {
      $scope.totalEmitidos = Datos.totalEmitidos;
      $scope.enviadoSII = Datos.enviadoSII;
      $scope.aprobadoReparos = Datos.aprobadoReparos;
      $scope.aprobados = Datos.aprobados;
      $scope.rechazados = Datos.rechazados;
      $scope.entregadoCliente = Datos.entregadoCliente;
      $scope.aprobadosCliente = Datos.aprobadosCliente;
      $scope.rechazadosCliente = Datos.rechazadosCliente;
      $scope.reciboMercaderia = Datos.reciboMercaderia;
      $scope.docsEmitidos.nacionales = Datos.docsEmitidos.nacionales;
      $scope.docsEmitidos.exportacion = Datos.docsEmitidos.exportacion;
      $scope.docsEmitidos.oc = Datos.docsEmitidos.oc;
      $scope.docsEmitidos.boletas = Datos.docsEmitidos.boletas;
      $scope.plazoRechazo.rango2a1Dias = Datos.plazoRechazo.rango2a1Dias;
      $scope.plazoRechazo.rango3a5Dias = Datos.plazoRechazo.rango3a5Dias;
      $scope.plazoRechazo.rango6a8Dias = Datos.plazoRechazo.rango6a8Dias;
      $scope.plazoRechazo.rangoVencido = Datos.plazoRechazo.rangoVencido;
    }

    $scope.valor = {
      fechaDesde: $scope.valor.fechaDesde,
      fechaHasta: $scope.valor.fechaHasta
    };

    $scope.opcionesFechas = $scope.fechasSimplesSelect;

    $scope.formatearMonto = function(numero, simbolo) {
      var partes = numero.toString().split('.');
      partes[0] = partes[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
      return simbolo + ' ' + partes.join(',');
    };

    $scope.espec = function(d) {
      //console.log("espec",d);
      $scope.opc = d.especifico.id;
    };
    $scope.opc = '';
    $scope.aplicar = function() {
      var op = {
        app: $stateParams.app,
        valor: $scope.valor,
        opcion: $scope.opcion,
        grupo: $scope.opcion == '2' ? $scope.opc : '',
        usuario: $scope.opcion == '1' ? $scope.opc : ''
      };
      //console.log("aplicando",op);
      $state.go('restringido.indicadoresemisor', op);
      //$state.reload();
    };

    var misGruposDirectos = [];
    var misGrupos = [];
    try {
      for (var i = 0; i < usuarios.length; i++) {
        if (usuarios[i].usuario.correo.toLowerCase() == SesionFebos().usuario.correo) {
          //misGruposDirectos=usuarios[i].grupos;

          for (var x = 0; x < usuarios[i].grupos.length; x++) {
            misGruposDirectos.push(usuarios[i].grupos[x].id);
          }
          break;
        }
      }
    } catch (error) {}

    $scope.usuarioPerteneceAlGrupo = function(usuarioId, grupoId) {
      //console.log(usuarioId, grupoId);
      for (var i = 0; i < usuarios.length; i++) {
        if (usuarios[i].usuario.usuarioId == usuarioId) {
          if (typeof usuarios[i].grupos != 'undefined') {
            for (var j = 0; j < usuarios[i].grupos.length; j++) {
              if (usuarios[i].grupos[j] == grupoId) {
                return true;
              }
            }
          }
        }
      }
      return false;
    };

    function obtenerGruposHijos(padreId) {
      ////console.log("Buscando hijos para: "+padreId);
      var hijos = [];
      for (var i = 0; i < grupos.length; i++) {
        if (typeof padreId == 'undefined' || padreId == 'null' || padreId == '') {
          if (typeof grupos[i].padreId == 'undefined') {
            hijos.push({
              title: grupos[i].nombre,
              nombre: grupos[i].nombre,
              grupoId: grupos[i].id,
              codigo: grupos[i].codigo,
              descripcion: grupos[i].descripcion,
              lazy: true
            });
          }
        } else {
          if (grupos[i].padreId == padreId) {
            hijos.push({
              title: grupos[i].nombre,
              nombre: grupos[i].nombre,
              grupoId: grupos[i].id,
              codigo: grupos[i].codigo,
              descripcion: grupos[i].descripcion
            });
          }
        }
      }
      return hijos;
    }

    $scope.armarArbol = function() {
      var raiz = obtenerGruposHijos('');
      for (var i = 0; i < raiz.length; i++) {
        raiz[i].children = obtenerGruposHijos(raiz[i].grupoId);
        var incluirHijosI = false;
        if (misGruposDirectos.indexOf(raiz[i].grupoId) >= 0) {
          misGrupos.push(raiz[i]);
          var incluirHijosI = true;
        }
        for (var w = 0; w < raiz[i].children.length; w++) {
          raiz[i].children[w].children = obtenerGruposHijos(raiz[i].children[w].grupoId);
          var incluirHijosW = false;
          if (misGruposDirectos.indexOf(raiz[i].children[w].grupoId) >= 0 || incluirHijosI) {
            misGrupos.push(raiz[i].children[w]);
            var incluirHijosW = true;
          }
          for (var x = 0; x < raiz[i].children[w].children.length; x++) {
            raiz[i].children[w].children[x].children = obtenerGruposHijos(
              raiz[i].children[w].children[x].grupoId
            );
            var incluirHijosX = false;
            if (
              misGruposDirectos.indexOf(raiz[i].children[w].children[x].grupoId) >= 0 ||
              incluirHijosW
            ) {
              misGrupos.push(raiz[i].children[w].children[x]);
              var incluirHijosX = true;
            }

            for (var y = 0; y < raiz[i].children[w].children[x].children.length; y++) {
              raiz[i].children[w].children[x].children[y].children = obtenerGruposHijos(
                raiz[i].children[w].children[x].children[y].grupoId
              );
              var incluirHijosY = false;
              if (
                misGruposDirectos.indexOf(raiz[i].children[w].children[x].children[y].grupoId) >=
                  0 ||
                incluirHijosX
              ) {
                misGrupos.push(raiz[i].children[w].children[x].children[y]);
                var incluirHijosY = true;
              }

              for (
                var z = 0;
                z < raiz[i].children[w].children[x].children[y].children.length;
                z++
              ) {
                raiz[i].children[w].children[x].children[y].children[
                  z
                ].children = obtenerGruposHijos(
                  raiz[i].children[w].children[x].children[y].children[z].grupoId
                );
                if (
                  misGruposDirectos.indexOf(
                    raiz[i].children[w].children[x].children[y].children[z].grupoId
                  ) >= 0 ||
                  incluirHijosY
                ) {
                  misGrupos.push(raiz[i].children[w].children[x].children[y].children[z]);
                }
              }
            }
          }
        }
      }
      return raiz;
    };

    $scope.armarArbol();

    var misUsuarios = [];
    var gruposids = [];
    for (var x = 0; x < misGrupos.length; x++) {
      gruposids.push(misGrupos[x].grupoId);
      if ($stateParams.grupo == misGrupos[x].grupoId) {
        $scope.especifico = {
          id: misGrupos[x].grupoId,
          nombre: misGrupos[x].codigo + ' - ' + misGrupos[x].nombre
        };
      }
    }
    for (var i = 0; i < usuarios.length; i++) {
      if ($stateParams.usuario == usuarios[i].usuario.id) {
        //console.log("encontrado!",usuarios[i]);
        $scope.especifico = {
          id: usuarios[i].usuario.id,
          nombre: usuarios[i].usuario.nombre
        };
      }
      if (typeof usuarios[i].grupos == 'undefined') continue;
      for (var x = 0; x < usuarios[i].grupos.length; x++) {
        ////console.log("buscando",usuarios[i].grupos[x].grupoId,gruposids);
        if (gruposids.indexOf(usuarios[i].grupos[x].id) >= 0) {
          misUsuarios.push(usuarios[i]);
        }
      }
    }

    $scope.cambio = function() {
      //$scope.correoscope="";
      //$scope.gruposcope="";
      $scope.itemsEspecificos = [];
      if ($scope.opcion == '3') {
        $scope.correoscope = '';
        $scope.gruposcope = '';
      }
      if ($scope.opcion == '2') {
        $scope.correoscope = '';
        $scope.gruposcope = '';
        //grupos
        for (var i = 0; i < misGrupos.length; i++) {
          $scope.itemsEspecificos.push({
            id: misGrupos[i].grupoId,
            nombre: misGrupos[i].codigo + ' - ' + misGrupos[i].nombre
          });
        }
      }
      if ($scope.opcion == '1') {
        $scope.correoscope = '';
        $scope.gruposcope = '';
        //persona
        for (var i = 0; i < misUsuarios.length; i++) {
          $scope.itemsEspecificos.push({
            id: misUsuarios[i].usuario.id,
            nombre: misUsuarios[i].usuario.nombre
          });
        }
      }
    };

    $scope.detalle = function(nombre) {
      var filtros =
        $stateParams.opcion == '1' ? 'correoReceptor:' + SesionFebos().usuario.correo + '|' : '';
      var vista = 'indicadores';
      switch (nombre) {
        case 'totalEmitidos':
          filtros +=
            'fechaCreacion:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,39,41,56,61,110,111,112,801|incompleto:N';
          break;
        case 'enviadoSII':
          filtros +=
            'fechaCreacion:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,39,41,56,61,110,111,112,801|estadoSii:2|incompleto:N'; //enviadoSII
          break;
        case 'aprobadoReparos':
          filtros +=
            'fechaCreacion:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,39,41,56,61,110,111,112,801|estadoSii:5|incompleto:N'; //aprobadoReparos
          break;
        case 'aprobados':
          filtros +=
            'fechaCreacion:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,39,41,56,61,110,111,112,801|estadoSii:4|incompleto:N'; //aprobados
          break;
        case 'rechazados':
          filtros +=
            'fechaCreacion:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,39,41,56,61,110,111,112,801|estadoSii:6|incompleto:N'; //rechazados
          break;
        case 'entregadoCliente':
          filtros +=
            'fechaCreacion:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,39,41,56,61,110,111,112,801|estadoComercial:0|incompleto:N'; //entregadoCliente
          break;
        case 'aprobadosCliente':
          filtros +=
            'fechaCreacion:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,39,41,56,61,110,111,112,801|estadoComercial:1,2|incompleto:N'; //aprobadosCliente
          break;
        case 'rechazadosCliente':
          filtros +=
            'fechaCreacion:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,39,41,56,61,110,111,112,801|estadoComercial:4,6|incompleto:N'; //rechazadosCliente
          break;
        case 'reciboMercaderia':
          filtros +=
            'fechaCreacion:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,39,41,56,61,110,111,112,801|estadoComercial:7|incompleto:N'; //reciboMercaderia
          break;
        case 'nacionales':
          filtros +=
            'fechaCreacion:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,56,61|incompleto:N';
          break;
        case 'exportacion':
          filtros +=
            'fechaCreacion:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:110,111,112|incompleto:N';
          break;
        case 'oc':
          filtros +=
            'fechaCreacion:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:801|incompleto:N';
          break;
        case 'boletas':
          filtros +=
            'fechaCreacion:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:39,41|incompleto:N';
          break;
        case 'plazo6-8':
          filtros +=
            'estadoComercial:0,1,3|fechaRecepcionSii:' +
            (moment()
              .subtract(1, 'days')
              .format('YYYY-MM-DD') +
              '--' +
              moment().format('YYYY-MM-DD')) +
            '|tipoDocumento:33,34,43,46,56|estadoSii:4,5|incompleto:N';
          break;
        case 'plazo3-5':
          filtros +=
            'estadoComercial:0,1,3|fechaRecepcionSii:' +
            (moment()
              .subtract(4, 'days')
              .format('YYYY-MM-DD') +
              '--' +
              moment()
                .subtract(2, 'days')
                .format('YYYY-MM-DD')) +
            '|tipoDocumento:33,34,43,46,56|estadoSii:4,5|incompleto:N';
          break;
        case 'plazo2-1':
          filtros +=
            'estadoComercial:0,1,3|fechaRecepcionSii:' +
            (moment()
              .subtract(7, 'days')
              .format('YYYY-MM-DD') +
              '--' +
              moment()
                .subtract(6, 'days')
                .format('YYYY-MM-DD')) +
            '|tipoDocumento:33,34,43,46,56|estadoSii:4,5|incompleto:N';
          break;
        case 'plazoVencido':
          filtros +=
            'estadoComercial:0,1,3|fechaRecepcionSii:' +
            (moment()
              .subtract(90, 'days')
              .format('YYYY-MM-DD') +
              '--' +
              moment()
                .subtract(8, 'days')
                .format('YYYY-MM-DD')) +
            '|tipoDocumento:33,34,43,46,56|estadoSii:4,5|incompleto:N';
          break;
      }
      var url = $state.href('restringido.dte', {
        app: 'cloud',
        categoria: 'emitidos',
        vista: vista,
        filtros: filtros,
        orden: '-fechaRecepcion',
        pagina: '1',
        itemsPorPagina: '20'
      });
      window.open(url, '_blank');
    };
    $scope.cambio();
    //console.log("scope",$scope.especifico);
    $scope.complementoTitulo = '';
    if ($stateParams.opcion == '2') {
      $scope.complementoTitulo += ' mis grupos';
    } else if ($stateParams.opcion == '3') {
      $scope.complementoTitulo += ' de ' + SesionFebos().empresa.fantasia;
    } else {
      $scope.complementoTitulo += ' míos';
    }
  }
]);
