angular.module('febosApp').controller('indicadorGestionCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$http',
  'utils',
  '$state',
  '$location',
  'SesionFebos',
  'FebosAPI',
  '$stateParams',
  'Datos',
  'FebosUtil',
  'usuarios',
  'grupos',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $http,
    utils,
    $state,
    $location,
    SesionFebos,
    FebosAPI,
    $stateParams,
    Datos,
    FebosUtil,
    usuarios,
    grupos
  ) {
    //console.log(usuarios);
    //console.log(grupos);
    $scope.usuario = SesionFebos().usuario;
    //$scope.AYUDA = AYUDA;

    $scope.valor = $stateParams.valor;
    $scope.query = '';
    $scope.totalRecibidos = '';
    $scope.aprobados = '';
    $scope.enviadosPago = '';
    $scope.noPagados = '';
    $scope.pagados = '';
    $scope.pendientesPago = '';
    $scope.rechazados = '';
    $scope.revisados = '';
    $scope.sinRevisar = '';
    $scope.sinRecepcionSII = '';
    $scope.pagoOportuno = {};
    $scope.plazoRechazo = {};
    $scope.opcion =
      (typeof $stateParams.opcion == 'undefined' || $stateParams.opcion == ''
        ? '3'
        : $stateParams.opcion) + '';
    $scope.itemsEspecificos = [];

    //console.log("OPCION!",$scope.opcion);
    $scope.correoscope = '';
    $scope.gruposcope = '';

    //console.log("params",$stateParams);
    //console.log("scope",$scope.especifico);

    var elementos = {
      atajosDeRangosDeFecha: {
        '[hoy]': {
          desde: moment().format('YYYY-MM-DD'),
          hasta: moment().format('YYYY-MM-DD')
        },
        '[ayerYhoy]': {
          desde: moment()
            .subtract(1, 'days')
            .format('YYYY-MM-DD'),
          hasta: moment().format('YYYY-MM-DD')
        },
        '[estaSemana]': {
          desde: moment()
            .startOf('week')
            .format('YYYY-MM-DD'),
          hasta: moment().format('YYYY-MM-DD')
        },
        '[las2UltimasSemanas]': {
          desde: moment()
            .startOf('week')
            .subtract(1, 'week')
            .format('YYYY-MM-DD'),
          hasta: moment().format('YYYY-MM-DD')
        },
        '[las3UltimasSemanas]': {
          desde: moment()
            .startOf('week')
            .subtract(2, 'weeks')
            .format('YYYY-MM-DD'),
          hasta: moment().format('YYYY-MM-DD')
        },
        '[losUltimos5Dias]': {
          desde: moment()
            .subtract(5, 'days')
            .format('YYYY-MM-DD'),
          hasta: moment().format('YYYY-MM-DD')
        },
        '[losUltimos10Dias]': {
          desde: moment()
            .subtract(10, 'days')
            .format('YYYY-MM-DD'),
          hasta: moment().format('YYYY-MM-DD')
        },
        '[losUltimos15Dias]': {
          desde: moment()
            .subtract(15, 'days')
            .format('YYYY-MM-DD'),
          hasta: moment().format('YYYY-MM-DD')
        },
        '[losUltimos20Dias]': {
          desde: moment()
            .subtract(20, 'days')
            .format('YYYY-MM-DD'),
          hasta: moment().format('YYYY-MM-DD')
        },
        '[esteMes]': {
          desde: moment()
            .startOf('month')
            .format('YYYY-MM-DD'),
          hasta: moment().format('YYYY-MM-DD')
        },
        '[elMesPasado]': {
          desde: moment()
            .subtract(1, 'month')
            .startOf('month')
            .format('YYYY-MM-DD'),
          hasta: moment()
            .subtract(1, 'month')
            .endOf('month')
            .format('YYYY-MM-DD')
        },
        '[elMesAntesPasado]': {
          desde: moment()
            .subtract(2, 'month')
            .startOf('month')
            .format('YYYY-MM-DD'),
          hasta: moment()
            .subtract(2, 'month')
            .endOf('month')
            .format('YYYY-MM-DD')
        },
        '[losUltimos2Meses]': {
          desde: moment()
            .subtract(1, 'month')
            .startOf('month')
            .format('YYYY-MM-DD'),
          hasta: moment().format('YYYY-MM-DD')
        },
        '[losUltimos3Meses]': {
          desde: moment()
            .subtract(2, 'month')
            .startOf('month')
            .format('YYYY-MM-DD'),
          hasta: moment().format('YYYY-MM-DD')
        },
        '[losUltimos4Meses]': {
          desde: moment()
            .subtract(3, 'month')
            .startOf('month')
            .format('YYYY-MM-DD'),
          hasta: moment().format('YYYY-MM-DD')
        },
        '[losUltimos5Meses]': {
          desde: moment()
            .subtract(4, 'month')
            .startOf('month')
            .format('YYYY-MM-DD'),
          hasta: moment().format('YYYY-MM-DD')
        },
        '[losUltimos6Meses]': {
          desde: moment()
            .subtract(5, 'month')
            .startOf('month')
            .format('YYYY-MM-DD'),
          hasta: moment().format('YYYY-MM-DD')
        }
      }
    };

    if (Datos != null && Datos != undefined) {
      $scope.totalRecibidos = Datos.totalRecibidos;
      $scope.aprobados = Datos.aprobados;
      $scope.enviadosPago = Datos.enviadosPago;
      $scope.noPagados = Datos.noPagados;
      $scope.pagados = Datos.pagados;
      $scope.pendientesPago = Datos.pendientesPago;
      $scope.rechazados = Datos.rechazados;
      $scope.revisados = Datos.revisados;
      $scope.sinRevisar = Datos.sinRevisar;
      $scope.sinRecepcionSII = Datos.sinRecepcionSII;
      $scope.pagoOportuno.rango0a7Dias = Datos.pagoOportuno.rango0a7Dias;
      $scope.pagoOportuno.rango8a15Dias = Datos.pagoOportuno.rango8a15Dias;
      $scope.pagoOportuno.rango16a30Dias = Datos.pagoOportuno.rango16a30Dias;
      $scope.pagoOportuno.rango30oMasDias = Datos.pagoOportuno.rango30oMasDias;
      $scope.plazoRechazo.rango2a1Dias = Datos.plazoRechazo.rango2a1Dias;
      $scope.plazoRechazo.rango3a5Dias = Datos.plazoRechazo.rango3a5Dias;
      $scope.plazoRechazo.rango6a8Dias = Datos.plazoRechazo.rango6a8Dias;
      $scope.plazoRechazo.rangoVencido = Datos.plazoRechazo.rangoVencido;
    }

    $scope.selectize_fecha_config = {
      maxItems: 1,
      valueField: 'valor',
      labelField: 'nombre',
      searchField: 'nombre',
      placeholder: 'Seleccione...',
      create: false,
      dropdownParent: 'body',
      onChange: $scope.seleccionManual,
      render: {
        option: function(data, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(data.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(data, escape) {
          return '<div class="item">' + escape(data.nombre) + '</div>';
        },
        option_create: function(data, escape) {
          return '<div class="create"> agregar ' + escape(data.input) + '&hellip;</div>';
        }
      }
    };
    var misGruposDirectos = [];
    var misGrupos = [];
    try {
      for (var i = 0; i < usuarios.length; i++) {
        if (usuarios[i].usuario.correo.toLowerCase() == SesionFebos().usuario.correo) {
          //misGruposDirectos=usuarios[i].grupos;

          for (var x = 0; x < usuarios[i].grupos.length; x++) {
            misGruposDirectos.push(usuarios[i].grupos[x].id);
          }
          break;
        }
      }
    } catch (error) {}

    function obtenerGruposHijos(padreId) {
      ////console.log("Buscando hijos para: "+padreId);
      var hijos = [];
      for (var i = 0; i < grupos.length; i++) {
        if (typeof padreId == 'undefined' || padreId == 'null' || padreId == '') {
          if (typeof grupos[i].padreId == 'undefined') {
            hijos.push({
              title: grupos[i].nombre,
              nombre: grupos[i].nombre,
              grupoId: grupos[i].id,
              codigo: grupos[i].codigo,
              descripcion: grupos[i].descripcion,
              lazy: true
            });
          }
        } else {
          if (grupos[i].padreId == padreId) {
            hijos.push({
              title: grupos[i].nombre,
              nombre: grupos[i].nombre,
              grupoId: grupos[i].id,
              codigo: grupos[i].codigo,
              descripcion: grupos[i].descripcion
            });
          }
        }
      }
      return hijos;
    }

    $scope.armarArbol = function() {
      var raiz = obtenerGruposHijos('');
      for (var i = 0; i < raiz.length; i++) {
        raiz[i].children = obtenerGruposHijos(raiz[i].grupoId);
        var incluirHijosI = false;
        if (misGruposDirectos.indexOf(raiz[i].grupoId) >= 0) {
          misGrupos.push(raiz[i]);
          var incluirHijosI = true;
        }
        for (var w = 0; w < raiz[i].children.length; w++) {
          raiz[i].children[w].children = obtenerGruposHijos(raiz[i].children[w].grupoId);
          var incluirHijosW = false;
          if (misGruposDirectos.indexOf(raiz[i].children[w].grupoId) >= 0 || incluirHijosI) {
            misGrupos.push(raiz[i].children[w]);
            var incluirHijosW = true;
          }
          for (var x = 0; x < raiz[i].children[w].children.length; x++) {
            raiz[i].children[w].children[x].children = obtenerGruposHijos(
              raiz[i].children[w].children[x].grupoId
            );
            var incluirHijosX = false;
            if (
              misGruposDirectos.indexOf(raiz[i].children[w].children[x].grupoId) >= 0 ||
              incluirHijosW
            ) {
              misGrupos.push(raiz[i].children[w].children[x]);
              var incluirHijosX = true;
            }

            for (var y = 0; y < raiz[i].children[w].children[x].children.length; y++) {
              raiz[i].children[w].children[x].children[y].children = obtenerGruposHijos(
                raiz[i].children[w].children[x].children[y].grupoId
              );
              var incluirHijosY = false;
              if (
                misGruposDirectos.indexOf(raiz[i].children[w].children[x].children[y].grupoId) >=
                  0 ||
                incluirHijosX
              ) {
                misGrupos.push(raiz[i].children[w].children[x].children[y]);
                var incluirHijosY = true;
              }

              for (
                var z = 0;
                z < raiz[i].children[w].children[x].children[y].children.length;
                z++
              ) {
                raiz[i].children[w].children[x].children[y].children[
                  z
                ].children = obtenerGruposHijos(
                  raiz[i].children[w].children[x].children[y].children[z].grupoId
                );
                if (
                  misGruposDirectos.indexOf(
                    raiz[i].children[w].children[x].children[y].children[z].grupoId
                  ) >= 0 ||
                  incluirHijosY
                ) {
                  misGrupos.push(raiz[i].children[w].children[x].children[y].children[z]);
                }
              }
            }
          }
        }
      }
      return raiz;
    };

    $scope.usuarioPerteneceAlGrupo = function(usuarioId, grupoId) {
      //console.log(usuarioId, grupoId);
      for (var i = 0; i < usuarios.length; i++) {
        if (usuarios[i].usuario.usuarioId == usuarioId) {
          if (typeof usuarios[i].grupos != 'undefined') {
            for (var j = 0; j < usuarios[i].grupos.length; j++) {
              if (usuarios[i].grupos[j] == grupoId) {
                return true;
              }
            }
          }
        }
      }
      return false;
    };

    $scope.armarArbol();

    var misUsuarios = [];
    var gruposids = [];
    for (var x = 0; x < misGrupos.length; x++) {
      gruposids.push(misGrupos[x].grupoId);
      if ($stateParams.grupo == misGrupos[x].grupoId) {
        $scope.especifico = {
          id: misGrupos[x].grupoId,
          nombre: misGrupos[x].codigo + ' - ' + misGrupos[x].nombre
        };
      }
    }
    for (var i = 0; i < usuarios.length; i++) {
      if ($stateParams.usuario == usuarios[i].usuario.id) {
        //console.log("encontrado!",usuarios[i]);
        $scope.especifico = {
          id: usuarios[i].usuario.id,
          nombre: usuarios[i].usuario.nombre
        };
      }
      if (typeof usuarios[i].grupos == 'undefined') continue;
      for (var x = 0; x < usuarios[i].grupos.length; x++) {
        ////console.log("buscando",usuarios[i].grupos[x].grupoId,gruposids);
        if (gruposids.indexOf(usuarios[i].grupos[x].id) >= 0) {
          misUsuarios.push(usuarios[i]);
        }
      }
    }

    $scope.cambio = function() {
      //$scope.correoscope="";
      //$scope.gruposcope="";
      $scope.itemsEspecificos = [];
      if ($scope.opcion == '3') {
        $scope.correoscope = '';
        $scope.gruposcope = '';
      }
      if ($scope.opcion == '2') {
        $scope.correoscope = '';
        $scope.gruposcope = '';
        //grupos
        for (var i = 0; i < misGrupos.length; i++) {
          $scope.itemsEspecificos.push({
            id: misGrupos[i].grupoId,
            nombre: misGrupos[i].codigo + ' - ' + misGrupos[i].nombre
          });
        }
      }
      if ($scope.opcion == '1') {
        $scope.correoscope = '';
        $scope.gruposcope = '';
        //persona
        for (var i = 0; i < misUsuarios.length; i++) {
          $scope.itemsEspecificos.push({
            id: misUsuarios[i].usuario.id,
            nombre: misUsuarios[i].usuario.nombre
          });
        }
      }
    };

    $scope.fechasSimplesSelect = [
      { nombre: 'Sin Fecha', id: 'NULL' },
      { nombre: 'Hoy', id: '[hoy]' },
      { nombre: 'Ayer y hoy', id: '[ayerYhoy]' },
      { nombre: 'Esta semana', id: '[estaSemana]' },
      { nombre: 'Las últimas 2 semanas', id: '[las2UltimasSemanas]' },
      { nombre: 'Las últimas 3 semanas', id: '[las3UltimasSemanas]' },
      { nombre: 'Los últimos 5 días', id: '[losUltimos5Dias]' },
      { nombre: 'Los últimos 10 días', id: '[losUltimos10Dias]' },
      { nombre: 'Los últimos 15 días', id: '[losUltimos15Dias]' },
      { nombre: 'Los últimos 20 días', id: '[losUltimos20Dias]' },
      { nombre: 'Este mes', id: '[esteMes]' },
      { nombre: 'Los últimos 2 meses', id: '[losUltimos2Meses]' },
      { nombre: 'Los últimos 3 meses', id: '[losUltimos3Meses]' },
      { nombre: 'Los últimos 4 meses', id: '[losUltimos4Meses]' },
      { nombre: 'Los últimos 5 meses', id: '[losUltimos5Meses]' },
      { nombre: 'Los últimos 6 meses', id: '[losUltimos6Meses]' }
    ];

    $scope.valor = {
      fechaDesde: $scope.valor.fechaDesde,
      fechaHasta: $scope.valor.fechaHasta
    };

    /*for (var i = 0; i < $scope.fechasSimplesSelect.length; i++) {
            if ($scope.filtro == $scope.fechasSimplesSelect[i].id) {
                $scope.periodo = $scope.fechasSimplesSelect[i];
                break;
            }
        }
        

        $scope.convertirFechaEnProsa = function (valor) {
            for (var i = 0; i < $scope.fechasSimplesSelect.length; i++) {
                if ($scope.fechasSimplesSelect[i].valor == valor)
                    return $scope.fechasSimplesSelect[i].nombre;
            }
            return "Fecha inválida";
        }*/

    $scope.opcionesFechas = $scope.fechasSimplesSelect;

    $scope.formatearMonto = function(numero, simbolo) {
      var partes = numero.toString().split('.');
      partes[0] = partes[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
      return simbolo + ' ' + partes.join(',');
    };

    $scope.espec = function(d) {
      //console.log("espec",d);
      $scope.opc = d.especifico.id;
    };
    $scope.opc = '';
    $scope.aplicar = function() {
      var op = {
        app: $stateParams.app,
        valor: $scope.valor,
        opcion: $scope.opcion,
        grupo: $scope.opcion == '2' ? $scope.opc : '',
        usuario: $scope.opcion == '1' ? $scope.opc : ''
      };
      //console.log("aplicando",op);
      $state.go('restringido.indicadoresgetion', op);
      //$state.reload();
    };

    //        $scope.selOpcion = function (opt) {
    //           console.log(opt);
    //           $scope.opcion = opt;
    //       }

    $scope.detalle = function(nombre) {
      var filtros =
        $stateParams.opcion == '1' ? 'correoReceptor:' + SesionFebos().usuario.correo + '|' : '';
      var vista = 'todos';
      switch (nombre) {
        case 'totalRecibidos':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoSii:4,5|incompleto:N';
          break;
        case 'sinRevisar':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoComercial:0|estadoSii:4,5|incompleto:N';
          break;
        case 'revisados':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoComercial:1,3|estadoSii:4,5|incompleto:N';
          break;
        case 'aprobados':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoComercial:2,7|estadoSii:4,5|incompleto:N';
          break;
        case 'rechazados':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoComercial:4,5,6|estadoSii:4,5|incompleto:N';
          break;
        case 'pendientesDePago':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoComercial:2,7|tipo:0,1|estadoSii:4,5|incompleto:N';
          break;
        case 'noPagados':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoComercial:2,7|tipo:5|estadoSii:4,5|incompleto:N';
          break;
        case 'enviadosApago':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '--' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoComercial:2,7|tipo:2|estadoSii:4,5|incompleto:N';
          break;
        case 'pagados':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '-' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoSii:4,5|incompleto:N';
          break;
        case 'sinRevisar':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '-' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoComercial:0|estadoSii:4,5|incompleto:N';
          break;
        case 'revisados':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '-' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoComercial:1,3|estadoSii:4,5|incompleto:N';
          break;
        case 'aprobados':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '-' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoComercial:2,7|estadoSii:4,5|incompleto:N';
          break;
        case 'rechazados':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '-' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoComercial:4,5,6|estadoSii:4,5|incompleto:N';
          break;
        case 'pendientesDePago':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '-' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoComercial:2,7|tipo:0,1|estadoSii:4,5|incompleto:N';
          break;
        case 'noPagados':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '-' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoComercial:2,7|tipo:5|estadoSii:4,5|incompleto:N';
          break;
        case 'enviadosApago':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '-' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoComercial:2,7|tipo:2|estadoSii:4,5|incompleto:N';
          break;
        case 'pagados':
          filtros +=
            'fechaRecepcionSii:' +
            $scope.valor.fechaDesde +
            '-' +
            $scope.valor.fechaHasta +
            '|tipoDocumento:33,34,43,46,56|estadoComercial:2,7|tipo:3,4|estadoSii:4,5|incompleto:N';
          break;
        case 'sinRecepcionSII':
          filtros += 'fechaRecepcionSii:NULL|tipoDocumento:33,34,43,46,56|incompleto:N';
          vista = 'sinRecepcionSII';
          break;
        case 'pago0-7':
          filtros +=
            'fechaRecepcionSii:' +
            (moment()
              .subtract(7, 'days')
              .format('YYYY-MM-DD') +
              '--' +
              moment().format('YYYY-MM-DD')) +
            '|estadoComercial:1,2,7|tipo:0,1|tipoDocumento:33,34,43,46,56|estadoSii:4,5|incompleto:N';
          break;
        case 'pago8-15':
          filtros +=
            'fechaRecepcionSii:' +
            (moment()
              .subtract(15, 'days')
              .format('YYYY-MM-DD') +
              '--' +
              moment()
                .subtract(8, 'days')
                .format('YYYY-MM-DD')) +
            '|tipo:0,1|estadoComercial:1,2,7|tipoDocumento:33,34,43,46,56|estadoSii:4,5|incompleto:N';
          break;
        case 'pago16-30':
          filtros +=
            'fechaRecepcionSii:' +
            (moment()
              .subtract(30, 'days')
              .format('YYYY-MM-DD') +
              '--' +
              moment()
                .subtract(16, 'days')
                .format('YYYY-MM-DD')) +
            '|tipo:0,1|estadoComercial:1,2,7|tipoDocumento:33,34,43,46,56|estadoSii:4,5|incompleto:N';
          break;
        case 'pago30':
          filtros +=
            'fechaRecepcionSii:' +
            (moment()
              .subtract(90, 'days')
              .format('YYYY-MM-DD') +
              '--' +
              moment()
                .subtract(31, 'days')
                .format('YYYY-MM-DD')) +
            '|tipo:0,1|estadoComercial:1,2,7|tipoDocumento:33,34,43,46,56|estadoSii:4,5|incompleto:N';
          break;
        case 'plazo6-8':
          filtros +=
            'estadoComercial:0,1,3|fechaRecepcionSii:' +
            (moment()
              .subtract(1, 'days')
              .format('YYYY-MM-DD') +
              '--' +
              moment().format('YYYY-MM-DD')) +
            '|tipoDocumento:33,34,43,46,56|estadoSii:4,5|incompleto:N';
          break;
        case 'plazo3-5':
          filtros +=
            'estadoComercial:0,1,3|fechaRecepcionSii:' +
            (moment()
              .subtract(4, 'days')
              .format('YYYY-MM-DD') +
              '--' +
              moment()
                .subtract(2, 'days')
                .format('YYYY-MM-DD')) +
            '|tipoDocumento:33,34,43,46,56|estadoSii:4,5|incompleto:N';
          break;
        case 'plazo2-1':
          filtros +=
            'estadoComercial:0,1,3|fechaRecepcionSii:' +
            (moment()
              .subtract(7, 'days')
              .format('YYYY-MM-DD') +
              '--' +
              moment()
                .subtract(6, 'days')
                .format('YYYY-MM-DD')) +
            '|tipoDocumento:33,34,43,46,56|estadoSii:4,5|incompleto:N';
          break;
        case 'plazoVencido':
          filtros +=
            'estadoComercial:0,1,3|fechaRecepcionSii:' +
            (moment()
              .subtract(90, 'days')
              .format('YYYY-MM-DD') +
              '--' +
              moment()
                .subtract(8, 'days')
                .format('YYYY-MM-DD')) +
            '|tipoDocumento:33,34,43,46,56|estadoSii:4,5|incompleto:N';
          break;
      }
      var url = $state.href('restringido.dte', {
        app: 'cloud',
        categoria: 'recibidos',
        vista: vista,
        filtros: filtros,
        orden: '-fechaRecepcion',
        pagina: '1',
        itemsPorPagina: '20'
      });
      window.open(url, '_blank');
    };
    $scope.cambio();
    //console.log("scope",$scope.especifico);
    $scope.complementoTitulo = '';
    if ($stateParams.opcion == '2') {
      $scope.complementoTitulo += ' mis grupos';
    } else if ($stateParams.opcion == '3') {
      $scope.complementoTitulo += ' de ' + SesionFebos().empresa.fantasia;
    } else {
      $scope.complementoTitulo += ' míos';
    }
    //$scope.complementoTitulo += " durante " + $scope.convertirFechaEnProsa($scope.filtro).toLowerCase();
  }
]);
