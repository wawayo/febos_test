febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.indicadoresgetion', {
      url: '/:app/indicadores/gestion?opcion&usuario&grupo',
      templateUrl: 'app/febos/dashboard/indicadores/indicadorGestionView.html',
      controller: 'indicadorGestionCtrl',
      params: {
        app: 'cloud',
        valor: [],
        opcion: '3',
        usuario: '',
        grupo: ''
      },
      reloadOnSearch: false,
      resolve: {
        Datos: [
          'FebosAPI',
          '$location',
          '$rootScope',
          '$stateParams',
          'FebosUtil',
          '$state',
          function(FebosAPI, $location, $rootScope, $stateParams, FebosUtil, $state) {
            var desde = moment()
              .subtract(2, 'month')
              .startOf('month')
              .format('YYYY-MM-DD');
            var hasta = moment().format('YYYY-MM-DD');

            if ($stateParams.valor.fechaDesde != undefined) {
              desde = $stateParams.valor.fechaDesde;
            } else {
              $stateParams.valor.fechaDesde = desde;
            }
            if ($stateParams.valor.fechaHasta != undefined) {
              hasta = $stateParams.valor.fechaHasta;
            } else {
              $stateParams.valor.fechaHasta = hasta;
            }
            console.log(desde);
            console.log(hasta);

            var req = {
              fechaDesde: desde,
              fechaHasta: hasta,
              ambitoGrupo: $stateParams.grupo,
              ambitoUsuario: $stateParams.usuario
            };
            console.log(req);
            return FebosAPI.cl_obtener_indicadores_gestion(req).then(function(response) {
              return response.data;
            });
          }
        ],
        usuarios: [
          'FebosAPI',
          'SesionFebos',
          function(FebosAPI, SesionFebos) {
            console.log('usuarios!');
            return FebosAPI.cl_listar_arbol_usuarios(
              {
                empresaId: SesionFebos().empresa.iut
              },
              true,
              false
            ).then(function(response) {
              return response.data.usuarios;
            });
          }
        ],
        grupos: [
          'FebosAPI',
          'SesionFebos',
          function(FebosAPI, SesionFebos) {
            console.log('grupos!');
            return FebosAPI.cl_listar_grupos(
              {
                empresaId: SesionFebos().empresa.iut
              },
              true,
              false
            ).then(function(response) {
              return response.data.grupos;
            });
          }
        ],

        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load(
              [
                // ocLazyLoad config (app/app.js)
                'bower_components/jquery-ui/jquery-ui.min.js',
                'lazy_uiSelect',
                'lazy_selectizeJS',
                'app/febos/dashboard/indicadores/indicadorGestionController.js'
              ],
              { serie: true }
            );
          }
        ]
      },
      data: {
        pageTitle: 'Indicadores de Gestion'
      },
      ncyBreadcrumb: {
        label: 'Indicadores'
      }
    });
  }
]);
