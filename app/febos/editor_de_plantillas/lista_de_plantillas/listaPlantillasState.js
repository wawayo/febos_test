febosApp
  .config(
    [
      '$stateProvider',
      '$urlRouterProvider',
      '$locationProvider',
      function ($stateProvider, $urlRouterProvider, $locationProvider) {

        $stateProvider.state(
          "restringido.listarPlantillas", 
          {
            url: "/:app/editorPlantillas/listar",
            templateUrl: 'app/febos/editor_de_plantillas/lista_de_plantillas/listaPlantillasView.html',
            controller: 'listaPlantillasCtrl',
            resolve: {
              EstadoAnterior: [
                "$state",
                function ($state) {
                  var currentStateData = {
                    estado: $state.current.name,
                    parametros: $state.params,
                    url: $state.href($state.current.name, $state.params)
                  };
                  return currentStateData;
                }
              ],
/*             Autorizacion: ['Autorizacion', function (Autorizacion) {
                try {
                  Autorizacion.verificar();
                } catch (e) {
                  console.log("Error al autenticar", e);
                }
                return;
              }],*/
              deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                  'lazy_uikit',
                  'lazy_iCheck',
                  'lazy_pagination',
                  'app/febos/editor_de_plantillas/lista_de_plantillas/listaPlantillasController.js'
                ]);
              }]
            }
          }
        );
      }
    ]
  );
