/**
 * ----
 *
 * @fileoverview        Contiene Controlador para vista definida en listaPlantillas.html
 *
 * @author              Apodis - Constructora de Software <contacto@apodis.cl>
 * @copyright           apodis.cl
 *
 * ----
 */
angular.module('febosApp').controller(
  // [INICIO] funcion controller
  'listaPlantillasCtrl', // Definición de nombre de Controlador
  [
    // [INICIO] configuración de controlador listaPlantillasCtrl
    '$rootScope',
    '$scope',
    '$state',
    '$stateParams',
    '$http',
    '$timeout',
    'FebosUtil',
    'SesionFebos',
    function listaPlantillasCtrl(
      $rootScope,
      $scope,
      $state,
      $stateParams,
      $http,
      $timeout,
      FebosUtil,
      SesionFebos
    ) {
      // [INICIO] función de controlador listaPlantillasCtrl
      $scope.loading = true; // @type {boolean} Determina si la aplicación se encuentra cargado datos
      $scope.plantillas = {
        // @type {object} Objeto controlador de tabla (#tabla-plantillas)
        datos: [], // @type {array} Almacena las plantillas que son cargadas del sistema
        atributoDeOrden: '', // @type {string} Almacena el atributo que se utiliza para ordenar tabla (#tabla-plantillas)
        columnasAOrdenar: {
          // @type {object} Objeto que determina las columnas que se pueden ordenar en la tabla (#tabla-plantillas)
          valor: false, // @type {boolean} Columna nombre de plantilla
          ultimaActualizacion: false // @type {boolean} Columna última actualización de plantilla
        },
        limit: 10 // @type {number} Detemina la cantidad de datos que se mostrarán en la paginación de la tabla (#tabla-plantillas)
      };
      var plantillaNueva = {
        source: {
          headerElements: [],
          bodyElements: [],
          footerElements: [],
          backElements: []
        },
        size: 'Carta',
        heights: {
          header: 'auto',
          body: 'auto',
          footer: 'auto'
        },
        style: {
          width: 22,
          height: 28,
          'font-family': 'Tahoma, Geneva, sans-serif',
          color: '#444444',
          'padding-top': 0.8,
          'padding-right': 0.8,
          'padding-bottom': 0.8,
          'padding-left': 0.8,
          'background-color': '#fff'
        },
        border: {
          'border-color': '#000',
          'border-width': 1,
          'border-style': 'solid',
          'border-radius': 0
        }
      };
      /**
       * @description Establece el atributo que se utilizará para ordenar tabla (#tabla-plantillas) en la vista
       *
       * @param  {string} atributoDeOrden - atributo que se utiliza para ordenar tabla (#tabla-plantillas)
       */
      $scope.establecerAtributoDeOrden = function(atributoDeOrden) {
        // [INICIO] función establecerAtributoDeOrden
        // Intercambia valor de true a false en columnas para ordenar de manera ascendente o descendente
        $scope.plantillas.columnasAOrdenar[atributoDeOrden] = !$scope.plantillas.columnasAOrdenar[
          atributoDeOrden
        ];
        // Establece atributo de orden
        $scope.plantillas.atributoDeOrden = atributoDeOrden;
        // Recorre columnasAOrdenar para setear en false el valor de las columnas distintas de atributoDeOrden
        angular.forEach($scope.plantillas.columnasAOrdenar, function(valor, atributo) {
          // [INICIO] forEach de columnasAOrdenar
          if (atributo != atributoDeOrden) {
            $scope.plantillas.columnasAOrdenar[atributo] = false;
          }
        }); // [FIN] forEach de columnasAOrdenar
      }; // [FIN] función establecerAtributoDeOrden

      /**
       * @description Obtiene atributoDeOrden a utilizar en orden de tabla (#tabla-plantillas) en la vista
       *
       * @returns {string}
       */
      $scope.obtenerAtributoDeOrden = function() {
        // [INICIO] función obtenerAtributoDeOrden
        return $scope.plantillas.atributoDeOrden;
      }; // [FIN] función obtenerAtributoDeOrden

      /**
       * @description Obtiene valor actual de columnasAOrdenar para ordenar tabla (#tabla-plantillas) en la vista según atributoDeOrden
       *
       * @returns {bolean}
       */
      $scope.ordenarDatos = function() {
        // [INICIO] función ordenarDatos
        return !!$scope.plantillas.columnasAOrdenar[$scope.plantillas.atributoDeOrden];
      }; // [FIN] función ordenarDatos

      /**
       * @description Carga datos de plantillas desde el sistema
       */
      $scope.cargarPlantillas = function() {
        // [INICIO] función cargarPlantillas
        $scope.loading = true; // Activa loader para espera de carga de datos
        var req = {
          method: 'GET',
          url:
            'https://api.febos.cl/' +
            FebosUtil.obtenerAmbiente() +
            '/herramientas/opciones?grupoOpcion=documentos.tipo',
          headers: {
            token: SesionFebos().usuario.token,
            empresa: SesionFebos().empresa.iut,
            'Content-Type': 'application/json'
          }
        };
        $http(req).then(
          function cargaExitosa(response) {
            // [INICIO] función cargaExitosa
            $scope.plantillas.datos = response.data.opciones; // obtiene datos de la respuesta y los asigna en objeto plantillas
            $scope.loading = false; // Desactiva loader para espera de carga de datos
          }, // [FIN] función cargaExitosa
          function cargaFallida(error) {
            // [INICIO] función cargaFallida
            $scope.loading = false; // Desactiva loader para espera de carga de datos
            console.log(error); // muestra por consola error en carga de datos
          } // [FIN] función cargaFallida
        );
      }; // [FIN] función cargarPlantillas

      /**
       * @description Crea un modal para mostrar mensaje de carga
       *
       * @param  {string} mensaje - Mensaje que se mostrará en modal
       *
       * @returns {object} retorna objeto modal de UIkit de tipo blockUI
       */
      $scope.crearModalDeCarga = function(mensaje) {
        // [INICIO] función crearModalDeCarga
        var templateHTMLModal =
          "<div class='uk-text-center'>" +
          mensaje + // se inserta mensaje en HTML
          '<br/>' +
          "<img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>" +
          '</div>'; // @type {string} Almacena template HTML de modal para loader de clonación de plantilla
        return UIkit.modal.blockUI(templateHTMLModal); // retorna un objeto modal de UIkit de tipo blockUI basado en templateHTMLModal
      }; // [FIN] función crearModalDeCarga
      /**
       * @description Obtiene objeto plantilla por su id desde el sistema y redirecciona a editor de plantilla para iniciar su edición
       *
       * @param  {string} id - identificador a utilizar para obtener plantilla desde el sistema
       */
      $scope.editarPlantilla = function(id) {
        // [INICIO] función editarPlantilla
        var modal = $scope.crearModalDeCarga('Cargando Plantilla'); // Crea un objeto modal con mensaje 'Cargando Plantilla'
        $http({
          method: 'GET',
          url:
            'https://api.febos.cl/' +
            FebosUtil.obtenerAmbiente() +
            '/herramientas/archivos/privados?descargable=si&path=febos-io/chile/' +
            FebosUtil.obtenerAmbiente() +
            '/editor/empresas/' +
            SesionFebos().empresa.iut +
            '/editor-dte-config-' +
            id +
            '.json',
          headers: {
            token: SesionFebos().usuario.token,
            empresa: SesionFebos().empresa.iut,
            'Content-Type': 'application/json'
          }
        }) // metodo get para obtener plantilla desde URL
          .then(
            function cargaExitosa(response) {
              // [INICIO] función cargaExitosa
              if (response.data.mensaje == 'No se puede encontrar el archivo especificado') {
                $http({
                  method: 'GET',
                  url:
                    'https://api.febos.cl/' +
                    FebosUtil.obtenerAmbiente() +
                    '/herramientas/archivos?tipo=upload&key=febos-io/chile/' +
                    FebosUtil.obtenerAmbiente() +
                    '/editor/empresas/' +
                    SesionFebos().empresa.iut +
                    '/editor-dte-config-' +
                    id +
                    '.json&contenType=application/json',
                  headers: {
                    token: SesionFebos().usuario.token,
                    empresa: SesionFebos().empresa.iut,
                    'Content-Type': 'application/json'
                  }
                }) // metodo get para obtener plantilla desde URL
                  .then(
                    function(response) {
                      plantillaNueva.id = id;
                      var blob = new Blob([JSON.stringify(plantillaNueva)], {
                        type: 'application/json'
                      });
                      $.ajax({
                        type: 'PUT',
                        url: response.data.url,
                        processData: false,
                        data: blob,
                        headers: {
                          Vary: 'Access-Control-Request-Headers',
                          Vary: 'Access-Control-Request-Method',
                          Vary: 'Origin'
                        }
                      }) // metodo get para obtener plantilla desde URL
                        .success(function(response) {
                          $state.go(
                            'restringido.editorPlantilla',
                            {
                              // [INCIO] objeto $stateParams de vista editorPlantilla
                              app: $stateParams.app, // se envía valor de app almacenado en $stateParams
                              id: plantillaNueva.id, // se envía id de plantilla obtenido de la solicitud $http.get para los datos de la plantilla
                              type: 'new', // se envía la instrucción 'edit' para iniciar la edición de una plantilla en la vista editorPlantilla
                              data: plantillaNueva // Se envía objeto obtenido de la solicitud $http.get para los datos de la plantilla
                            } // [FIN] objeto $stateParams de vista editorPlantilla
                          );
                          modal.hide(); // Oculta modal
                        })
                        .error(
                          function cargaFallida(error) {
                            // [INICIO] función cargaFallida
                            console.log(error);
                            modal.hide(); // Oculta modal
                          } // [FIN] función cargaFallida
                        );
                    },
                    function cargaFallida(error) {
                      // [INICIO] función cargaFallida
                      console.log(error);
                      modal.hide(); // Oculta modal
                    } // [FIN] función cargaFallida
                  );
              } else {
                var cantIntentos = 5;
                var intentosRealizados = 0;
                var getPlantilla = function(){
                  return $.ajax({
                    url: response.data.url.replace('http:','https:')
                  });// metodo get para obtener plantilla desde URL
                };
                var successHandler = function(response) {
                  var plantilla = JSON.parse(response); // @type {object} obtiene datos de la respuesta y los asigna en objeto plantilla
                  modal.hide(); // Oculta modal
                  // Se cambia $state actual por vista de editorPlantilla
                  $state.go(
                    'restringido.editorPlantilla',
                    {
                      // [INCIO] objeto $stateParams de vista editorPlantilla
                      app: $stateParams.app, // se envía valor de app almacenado en $stateParams
                      id: plantilla.id, // se envía id de plantilla obtenido de la solicitud $http.get para los datos de la plantilla
                      type: 'edit', // se envía la instrucción 'edit' para iniciar la edición de una plantilla en la vista editorPlantilla
                      data: plantilla // Se envía objeto obtenido de la solicitud $http.get para los datos de la plantilla
                    } // [FIN] objeto $stateParams de vista editorPlantilla
                  );
                };
                var errorHandler = function(error) {
                  $timeout(function(){
                    if(intentosRealizados < cantIntentos){
                      intentosRealizados++;
                      getPlantilla()
                        .success(successHandler)
                        .error(errorHandler);
                    }else{
                      console.log(error);
                      modal.hide(); // Oculta modal
                    }
                  }, 1000);
                };
                getPlantilla()
                  .success(successHandler)
                  .error(errorHandler);
              }
            }, // [FIN] función cargaExitosa
            function cargaFallida(error) {
              // [INICIO] función cargaFallida
              console.log(error);
              modal.hide(); // Oculta modal
            } // [FIN] función cargaFallida
          );
      }; // [FIN] función editarPlantilla
      /**
       * [INICIO] inicializacion de controlador listaPlantillasCtrl
       */
      $scope.cargarPlantillas(); // Inicia la carga de datos de plantillas desde el sistema
      /**
       * [FIN] inicializacion de controlador listaPlantillasCtrl
       */
    } // [FIN] función de controlador listaPlantillasCtrl
  ] // [FIN] configuración de controlador listaPlantillasCtrl
); // [FIN] funcion controller
