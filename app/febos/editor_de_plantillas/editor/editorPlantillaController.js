/**
 * ----
 *
 * @fileoverview        Contiene Controlador para vista definida en editorPlantillas.html y algunas funcionalidades extra necesarias
 *
 * @author              Apodis - Constructora de Software <contacto@apodis.cl>
 * @copyright           apodis.cl
 *
 * ----
 */
angular
  .module(
    // [INICIO] funcion module
    'febosApp',
    [
      // [INICIO] definición de modulos
      angularDragula(angular), // Configuración de dependencia dragula para el uso en el controlador
      'color.picker' // Configuración de dependencia color.picker para el uso en el controlador
    ] // [FIN] definición de modulos
  ) // [FIN] funcion module
  .directive(
    // [INICIO] funcion directive
    'modalEditorDeElementos', // Declaración de directiva modalEditorDeElementos para apertura de modals de edicion segun elemento
    [
      // [INICIO] Configuración de directiva modalEditorDeElementos
      /**
       * @description Retorna la configuración necesaria para agregar la directiva modalEditorDeElementos
       *
       * @returns {object}
       */
      function modalEditorDeElementos() {
        // [INICIO] función modalEditorDeElementos
        return {
          // [INICIO] objeto de configuración de directiva
          restrict: 'A', // Permite que la directiva funcione como elemento ('E') en la vista
          templateUrl:
            'app/templates/editorPlantillaTemplates/modals/modalEditorDeElementos.tpl.html', // ubicacion de template de directiva
          scope: {
            // [INCIO] scope de directiva
            elemento: '=modalEditorDatos',
            acciones: '=modalEditorAcciones',
            bloquesDeElementos: '=modalEditorDatosBloques',
            estiloCssPlantilla: '=modalEditorEstiloCssPlantilla'
          }, // [FIN] scope de directiva
          controller: [
            // [INICIO] configuracion de controller
            '$rootScope',
            '$stateParams',
            '$scope',
            'funcionesUtilesEditor',
            '$timeout',
            function modalEditorDeElementosCtrl(
              $rootScope,
              $stateParams,
              $scope,
              funcionesUtilesEditor,
              $timeout
            ) {
              // [INICIO] función modalEditorDeElementosCtrl
              console.log($stateParams);
              /**
               * [INICIO] configuraciones para edicion de estilo CSS
               */
              $scope.enEdicion = true; // @type {boolean} determina que el elemento se encuentra en edicion
              $scope.templateError = false; // @type {boolean} almacena el true/false si existe un error en la carga de template de configuracion
              $scope.fonts = funcionesUtilesEditor.cssConfigurationsOptions.fonts; // @type {array} almacena arreglo de tipos de fuentes de texto
              $scope.direction = funcionesUtilesEditor.cssConfigurationsOptions.doc_direction; // @type {array} almacena direcciones permitidas para elementos de estructura
              $scope.text_align_types =
                funcionesUtilesEditor.cssConfigurationsOptions.text_align_types; // @type {array} almacena arreglo de tipos de alineacion de texto
              $scope.text_style_types =
                funcionesUtilesEditor.cssConfigurationsOptions.text_style_types; // @type {array} almacena arreglo de tipos de estilo de texto
              $scope.font_weight_types =
                funcionesUtilesEditor.cssConfigurationsOptions.font_weight_types; // @type {array} almacena arreglo de tipos de grosor de texto
              $scope.border_style_types =
                funcionesUtilesEditor.cssConfigurationsOptions.border_style_types; // @type {array} almacena arreglo de tipos de bordes de elementos
              $scope.configuracionParaValoresSelectize = {
                // [INICIO] configuracion de selectize
                maxItems: 1, // @type {integer} determina la maxima capacidad permitida de elementos seleccionados
                valueField: 'id', // @type {string} determina nombre de atributo a utilizar para el valor del elemento seleccionado
                labelField: 'name', // @type {string} determina nombre de atributo a utilizar para mostrar en elemento selectize
                create: false, // @type {boolean} evita que el usuario pueda crear nuevos items que no se encuentran en la lista original
                placeholder: 'Seleccionar...' // @type {string} texto que aparece por defecto en el elemento
              }; // [FIN] configuracion de selectize
              $scope.configuracionParaValoresFontFamilySelectize = {
                // [INICIO] configuracion de selectize para atributo font-family
                maxItems: 1, // @type {integer} determina la maxima capacidad permitida de elementos seleccionados
                create: false, // @type {boolean} evita que el usuario pueda crear nuevos items que no se encuentran en la lista original
                placeholder: 'Seleccionar...', // @type {string} texto que aparece por defecto en el elemento
                render: {
                  // [INICIO] configuracion de renderizado de items
                  option: function renderizadoPorItem(item, escape) {
                    // [INICIO] funcion renderizadoPorItem
                    // Se entrega template HTML para renderizar items en la lista de selectize
                    return (
                      '<div class="option" style="font-family:' +
                      escape(item.value) +
                      '" data-selectable="" data-value="' +
                      escape(item.value) +
                      '">' +
                      escape(item.text) +
                      '</div>'
                    );
                  } // [FIN] funcion renderizadoPorItem
                } // [FIN] configuracion de renderizado de items
              }; // [FIN] configuracion de selectize para atributo font-family
              $scope.structureBorderColorOptions = angular.extend(
                // [INICIO] configuracion de color-picker para borde
                {}, // Objeto base de configuracion
                funcionesUtilesEditor.defaultOptionsColor, // Configuracion por defecto para color-picker
                {
                  // [INICIO] Configuracion especifica
                  id: 'structure-border-color', // Id de color-picker en elemento HTML
                  name: 'structure-border-color', // Nombre de color-picker en elemento HTML
                  pos: 'bottom left' // Posicion de modal de color-picker
                } // [FIN] Configuracion especifica
              ); // [FIN] configuracion de color-picker para borde
              $scope.structureBackgroundColorOptions = angular.extend(
                // [INICIO] configuracion de color-picker para background
                {}, // Objeto base de configuracion
                funcionesUtilesEditor.defaultOptionsColor, // Configuracion por defecto para color-picker
                {
                  // [INICIO] Configuracion especifica
                  id: 'structure-background-color', // Id de color-picker en elemento HTML
                  name: 'structure-background-color', // Nombre de color-picker en elemento HTML
                  pos: 'bottom left', // Posicion de modal de color-picker
                  format: 'rgb', // establece el formato de color en rgb para permitir configuracion de alpha
                  alpha: true // permite la configuracion de alpha en color-picker
                } // [FIN] Configuracion especifica
              ); // [FIN] configuracion de color-picker para background
              $scope.structureTitleColorOptions = angular.extend(
                // [INICIO] configuracion de color-picker para titulo de estructura
                {}, // Objeto base de configuracion
                funcionesUtilesEditor.defaultOptionsColor, // Configuracion por defecto para color-picker
                {
                  // [INICIO] Configuracion especifica
                  id: 'structure-title-color', // Id de color-picker en elemento HTML
                  name: 'structure-title-color', // Nombre de color-picker en elemento HTML
                  pos: 'bottom left' // Posicion de modal de color-picker
                } // [FIN] Configuracion especifica
              ); // [FIN] configuracion de color-picker para titulo de estructura
              $scope.tableColorOptions = angular.extend(
                // [INICIO] configuracion de color-picker para titulo de estructura
                {}, // Objeto base de configuracion
                funcionesUtilesEditor.defaultOptionsColor, // Configuracion por defecto para color-picker
                {
                  // [INICIO] Configuracion especifica
                  id: 'table-color', // Id de color-picker en elemento HTML
                  name: 'table-color', // Nombre de color-picker en elemento HTML
                  pos: 'bottom left', // Posicion de modal de color-picker
                  format: 'rgb', // establece el formato de color en rgb para permitir configuracion de alpha
                  alpha: true // permite la configuracion de alpha en color-picker
                } // [FIN] Configuracion especifica
              ); // [FIN] configuracion de color-picker para titulo de estructura
              $scope.tinymce_options = {
                plugins: [
                  'advlist autolink lists charmap',
                  'searchreplace visualblocks',
                  'insertdatetime table contextmenu paste',
                  'textcolor'
                ],
                toolbar:
                  'undo redo | bold italic fontsizeselect forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | link',
                fontsize_formats: '8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 18pt 24pt 36pt',
                file_picker_callback: elFinderBrowser
              };

              function elFinderBrowser(callback, value, meta) {
                tinymce.activeEditor.windowManager.open(
                  {
                    file: '/file_manager/fm_tinymce.html',
                    title: 'File Manager',
                    width: 920,
                    height: 440,
                    resizable: 'yes'
                  },
                  {
                    oninsert: function(file, elf) {
                      var url, reg, info;

                      // URL normalization
                      url = file.url;
                      reg = /\/[^/]+?\/\.\.\//;
                      while (url.match(reg)) {
                        url = url.replace(reg, '/');
                      }

                      // Make file info
                      info = file.name + ' (' + elf.formatSize(file.size) + ')';

                      // Provide file and text for the link dialog
                      if (meta.filetype == 'file') {
                        callback(url, { text: info, title: info });
                      }

                      // Provide image and alt text for the image dialog
                      if (meta.filetype == 'image') {
                        callback(url, { alt: info });
                      }

                      // Provide alternative source and posted for the media dialog
                      if (meta.filetype == 'media') {
                        callback(url);
                      }
                    }
                  }
                );
                return false;
              }
              /**
               * [FIN] configuraciones para edicion de estilo CSS
               */
              /**
               * @description Retorna la configuración General de aspecto del contenido del Modal
               *
               * @returns {object}
               */
              $scope.configuracionGeneralContenidoModal = function() {
                // [INICIO] funcion configuracionGeneralContenidoModal
                // Se obtienen el estilo computado del elemento a mostrar
                var estiloElementoDOM = window.getComputedStyle(
                  $('#page_content ' + '#' + $scope.elemento.id)[0],
                  null
                );
                return {
                  // [INICIO] estilo del contenido del modal
                  'box-sizing':
                    funcionesUtilesEditor.doc_builderSettings.defaultOptions['box-sizing'],
                  color: $scope.estiloCssPlantilla['color'], // Color de texto definido para la plantilla
                  'background-color': $scope.estiloCssPlantilla['background-color'], // Color de fondo definido para la plantilla
                  'font-family': $scope.estiloCssPlantilla['font-family'], // fuente de texto definida para la plantilla
                  width: parseInt(estiloElementoDOM.getPropertyValue('width')) + 'px', // se define el ancho computado del elemento
                  position: 'relative', // Posicion relativa para poder centrar el contenido
                  margin: 'auto' // margenes automaticos para centrado de contenido
                }; // [FIN] estilo del contenido del modal
              }; // [FIN] funcion configuracionGeneralContenidoModal
              /**
               * @description Retorna template de campos editables para la configuracion de estilo por tipo de elemento
               *
               * @returns {object}
               */
              $scope.obtenerCamposEditables = function(tipo) {
                // [INICIO] funcion obtenerCamposEditables
                // retorna nombre del template si existe, sino retorna un template por defecto
                return tipo && !$scope.templateError
                  ? tipo + 'TemplateEditor'
                  : 'defaultTemplateEditor';
              }; // [FIN] funcion obtenerCamposEditables
              /**
               * [INICIO] configuración de eventos
               */
              $scope.$on(
                '$includeContentRequested', // Nombre del evento
                function inicioDeInclusionDeContenido(e, data) {
                  // [INICIO] funcion inicioDeInclusionDeContenido
                  $scope.cargandoCampos = true; // Determina que la carga de datos inicio
                } // [FIN] funcion inicioDeInclusionDeContenido
              );
              $scope.$on(
                '$includeContentError', // Nombre del evento
                function errorEnInclusionDeContenido(e, data) {
                  // [INICIO] funcion errorEnInclusionDeContenido
                  // Si existe error se determina templateError como true
                  $scope.templateError = true;
                } // [FIN] funcion errorEnInclusionDeContenido
              );
              var destruirObservarFilas;
              var destruirObservarColumnas;
              /**
               * @description Se configura un observador para cuando la variable 'elemento' cambie
               */
              $scope.$watch(
                // [INICIO] $watch
                'elemento',
                /**
                 * @description se ejecuta cuando cambia el valor de 'elemento'
                 *
                 * @params {*} valorNuevo - Obtiene el valor nuevo para el atributo 'elemento'
                 * @params {*} valorAnterior - Obtiene el valor anterior para el atributo 'elemento'
                 */
                function cuandoElementoCambia(valorNuevo, valorAnterior) {
                  // [INICIO] funcion cuandoElementoCambia
                  // Comprueba si el valor anterior y el valor nuevo son iguales
                  if (valorNuevo === valorAnterior) {
                    // En caso de ser iguales, se termina la ejecucion de la funcion cuandoElementoCambia
                    return;
                  }
                  if (valorAnterior && valorAnterior.type == 'discounts_totals_element') {
                    destruirObservarFilas();
                    destruirObservarColumnas();
                  }
                } // [FIN] funcion cuandoElementoCambia
              ); // [FIN] $watch
              $scope.$on(
                '$includeContentLoaded', // Nombre del evento
                function inclusionDeContenidoTerminado(e, data) {
                  // [INICIO] funcion inclusionDeContenidoTerminado
                  // Si no contiene 'Editor' se finaliza la funcion
                  if (data.indexOf('Editor') === -1) {
                    return;
                  }
                  if ($scope.elemento.type == 'sender_information_element') {
                    // Configuración para elemento 'sender_information_element'
                    $scope.newSenderInformation = '';
                    $scope.newCode = '';
                    $scope.agregarNuevaInformacionEmisor = function(texto, codigo) {
                      $scope.elemento.options.senderInformation.push(texto);
                      $scope.elemento.options.code.push(codigo);
                      $scope.newSenderInformation = '';
                      $scope.newCode = '';
                    };
                    $scope.eliminarNuevaInformacionEmisor = function(indice) {
                      $scope.elemento.options.senderInformation.splice(indice, 1);
                      $scope.elemento.options.code.splice(indice, 1);
                    };
                  }
                  if ($scope.elemento.type == 'discounts_totals_element') {
                    /**
                     * @description Se configura un observador para cuando la variable numRows de un elemento de tabla cambie
                     */
                    destruirObservarFilas = $scope.$watch(
                      // [INICIO] $watch
                      /**
                       * @description visualiza el valor actual de variable numRows
                       *
                       * @param {object} $scope - scope del elemento
                       *
                       * @returns {number}
                       */
                      function observarFilas($scope) {
                        // [INICIO] funcion observarFilas
                        return $scope.elemento.options.numRows;
                      }, // [FIN] funcion observarFilas
                      /**
                       * @description se ejecuta cuando cambia el valor de numRows
                       *
                       * @params {*} valorNuevo - Obtiene el valor nuevo para el atributo numRows
                       * @params {*} valorAnterior - Obtiene el valor anterior para el atributo numRows
                       */
                      function cuandoCambianFilas(valorNuevo, valorAnterior) {
                        // [INICIO] funcion cuandoCambianFilas
                        // Comprueba si el valor anterior y el valor nuevo son iguales
                        if (valorNuevo === valorAnterior) {
                          // En caso de ser iguales, se termina la ejecucion de la funcion cuandoCambianFilas
                          return;
                        }
                        // Comprueba si el valor nuevo de filas es menor a 0 o undefined
                        if (valorNuevo < 0 || !valorNuevo) {
                          // Si es menor a 0 o undefined, se establece el valor en 0 y se termina la ejecucion de la función
                          $scope.elemento.options.numRows = 0;
                          valorNuevo = 0;
                        }
                        // Cuando cambia el valor de filas, redimensiona los arreglos de texto
                        // Comprueba si el valor nuevo de Filas es menor al valor anterior
                        if (valorNuevo < valorAnterior) {
                          // Si es menor, elimina la cantidad de filas igual a la diferencia entre el valor anterior y el actual
                          var diferencia = valorAnterior - valorNuevo;
                          for (var j = 0; j < diferencia; j++) {
                            $scope.elemento.options.cells.pop();
                          }
                        }
                        // Comprueba si el valor nuevo de Filas es mayor al valor anterior
                        else if (valorNuevo > valorAnterior) {
                          // Si es mayor, agrega la cantidad de filas igual a la diferencia entre el valor anterior y el actual, con celdas nuevas con un texto por defecto
                          var row; // $type {array} almacena temporalmente un arreglo de filas
                          var diferencia = valorNuevo - valorAnterior;
                          var numFilas = valorAnterior;
                          for (var j = 0; j < diferencia; j++) {
                            row = [];
                            for (var i = 0; i < $scope.elemento.options.numCells; i++) {
                              row.push({
                                header: 'Celda ' + (numFilas + j + 1) + '-' + (i + 1),
                                example: '0',
                                variable: ''
                              });
                            }
                            $scope.elemento.options.cells.push(row);
                          }
                        }
                      } // [FIN] funcion cuandoCambianFilas
                    ); // [FIN] $watch
                    /**
                     * @description Se configura un observador para cuando la variable numCells de un elemento de tabla cambie
                     */
                    destruirObservarColumnas = $scope.$watch(
                      // [INICIO] $watch
                      /**
                       * @description visualiza el valor actual de variable numCells
                       *
                       * @param {object} $scope - scope del elemento
                       *
                       * @returns {string}
                       */
                      function observarColumnas($scope) {
                        // [INICIO] funcion observarColumnas
                        return $scope.elemento.options.numCells;
                      }, // [FIN] funcion observarColumnas
                      /**
                       * @description se ejecuta cuando cambia el valor de numCells
                       *
                       * @params {*} valorNuevo - Obtiene el valor nuevo para el atributo numCells
                       * @params {*} valorAnterior - Obtiene el valor anterior para el atributo numCells
                       */
                      function cuandoCambianColumnas(valorNuevo, valorAnterior) {
                        // [INICIO] funcion cuandoCambianColumnas
                        // Comprueba si el valor anterior y el valor nuevo son iguales
                        if (valorNuevo === valorAnterior) {
                          // En caso de ser iguales, se termina la ejecucion de la funcion cuandoCambianColumnas
                          return;
                        }
                        // Comprueba si el valor nuevo de columnas es menor a 0 o undefined
                        if (valorNuevo < 0 || !valorNuevo) {
                          // Si es menor a 0 o undefined, se establece el valor en 0 y se termina la ejecucion de la función
                          $scope.elemento.options.numCells = 0;
                          valorNuevo = 0;
                        }
                        // Cuando cambia el valor de columnas, redimensiona los arreglos de texto
                        // Comprueba si el valor nuevo de Columnas es menor al valor anterior
                        if (valorNuevo < valorAnterior) {
                          // Si es menor, elimina la cantidad de Columnas igual a la diferencia entre el valor anterior y el actual
                          var diferencia = valorAnterior - valorNuevo;
                          for (var j = 0; j < $scope.elemento.options.numRows; j++) {
                            for (var i = 0; i < diferencia; i++) {
                              $scope.elemento.options.cells[j].pop();
                            }
                          }
                        }
                        // Comprueba si el valor nuevo de Columnas es mayor al valor anterior
                        else if (valorNuevo > valorAnterior) {
                          // Si es mayor, agrega la cantidad de Columnas igual a la diferencia entre el valor anterior y el actual, con celdas nuevas con un texto por defecto
                          var diferencia = valorNuevo - valorAnterior;
                          var numColumnas = valorAnterior;
                          for (var j = 0; j < $scope.elemento.options.numRows; j++) {
                            for (var i = 0; i < diferencia; i++) {
                              $scope.elemento.options.cells[j].push({
                                header: 'Celda ' + (j + 1) + '-' + (numColumnas + i + 1),
                                example: '0',
                                variable: ''
                              });
                            }
                          }
                        }
                      } // [FIN] funcion cuandoCambianColumnas
                    ); // [FIN] $watch
                  }
                  if ($scope.elemento.type == 'receiver_information_element') {
                    // Configuración para elemento 'receiver_information_element'
                    var nuevaLinea = {
                      label: 'Etiqueta 1:',
                      ejemplo: 'Nueva información',
                      code: '',
                      combinar: false,
                      otherCell: {
                        label: 'Etiqueta 2:',
                        ejemplo: 'Otra Información',
                        code: ''
                      }
                    };
                    $scope.nuevaLineaInformacionReceptor = function() {
                      $scope.elemento.options.data.push(angular.copy(nuevaLinea));
                    };
                    $scope.eliminarLineaInformacionReceptor = function(indice) {
                      $scope.elemento.options.data.splice(indice, 1);
                    };
                    $scope.calcularWidth = function(item, cell) {
                      var anchoTabla = $('#table_receiver_information').innerWidth();
                      var anchoEtiqueta1 = $($('.label_receiver_information')[0]).innerWidth();
                      var anchoEtiqueta2 = $($('.label_receiver_information')[1]).innerWidth();
                      if (item.combinar) {
                        item.textWidth = anchoTabla - anchoEtiqueta1;
                      } else {
                        var width = (anchoTabla - anchoEtiqueta1 - anchoEtiqueta2) / 2;
                        item.textWidth = width;
                        item.otherCell.textWidth = width;
                      }
                      $scope.elemento.options.widthTexts =
                        (anchoTabla - anchoEtiqueta1 - anchoEtiqueta2) / 2;
                      if (cell == 'cell1') {
                        return item.textWidth;
                      }
                      if (cell == 'cell2') {
                        return item.otherCell.textWidth;
                      }
                    };
                  }
                  if (
                    $scope.elemento.type == 'aditionals_documents_element' ||
                    $scope.elemento.type == 'transport_data_element'
                  ) {
                    // Configuración para elemento 'aditionals_documents_element'
                    var nuevaLinea = {
                      label: 'Etiqueta 1:',
                      ejemplo: 'Nueva información',
                      code: '',
                      combinar: 0,
                      cell2: {
                        label: 'Etiqueta Celda 2:',
                        ejemplo: 'Otra Información 2',
                        code: '',
                        combinar: false
                      },
                      cell3: {
                        label: 'Etiqueta Celda 3:',
                        ejemplo: 'Otra Información 3',
                        code: ''
                      }
                    };
                    $scope.nuevaLineaDocumentosAdicionales = function() {
                      $scope.elemento.options.data.push(angular.copy(nuevaLinea));
                    };
                    $scope.eliminarLineaDocumentosAdicionales = function(indice) {
                      $scope.elemento.options.data.splice(indice, 1);
                    };
                    $scope.obtenerCantidadColumnasACombinar = function(indice) {
                      if (indice == 3) {
                        return 3;
                      }
                      if (indice == 5) {
                        return 4;
                      }
                      return 1;
                    };
                    $scope.calcularWidth = function(item, cell) {
                      if (item) {
                        var anchoTabla = $('#table_aditionals_documents').innerWidth();
                        var anchoEtiqueta1 = $($('.label_aditionals_documents1')[0]).innerWidth();
                        var anchoEtiqueta2 = $($('.label_aditionals_documents2')[0]).innerWidth();
                        var anchoEtiqueta3 = $($('.label_aditionals_documents3')[0]).innerWidth();
                        if (item.combinar == 0 && !item.cell2.combinar) {
                          item.textWidth = item.cell2.textWidth = item.cell3.textWidth =
                            (anchoTabla - anchoEtiqueta1 - anchoEtiqueta2 - anchoEtiqueta3) / 3;
                        }
                        if (item.combinar == 0 && item.cell2.combinar) {
                          var width;
                          if (anchoEtiqueta3 == 0) {
                            width = anchoTabla - anchoEtiqueta1 - anchoEtiqueta2;
                            item.textWidth = width / 3;
                            item.cell2.textWidth = (width / 3) * 2;
                          } else if (anchoEtiqueta3 > 0) {
                            width =
                              (anchoTabla - anchoEtiqueta1 - anchoEtiqueta2 - anchoEtiqueta3) / 3;
                            item.textWidth = width;
                            item.cell2.textWidth = 2 * width + anchoEtiqueta3;
                          }
                        }
                        if (item.combinar == 3) {
                          var width;
                          if (anchoEtiqueta2 == 0) {
                            width = anchoTabla - anchoEtiqueta1 - anchoEtiqueta3;
                            item.textWidth = (width / 3) * 2;
                            item.cell3.textWidth = width / 3;
                          } else if (anchoEtiqueta2 > 0) {
                            width =
                              (anchoTabla - anchoEtiqueta1 - anchoEtiqueta2 - anchoEtiqueta3) / 3;
                            item.textWidth = 2 * width + anchoEtiqueta2;
                            item.cell2.textWidth = width;
                          }
                        }
                        if (item.combinar == 5) {
                          item.textWidth = anchoTabla - anchoEtiqueta1;
                        }
                        if (cell == 'cell1') {
                          return item.textWidth;
                        }
                        if (cell == 'cell2') {
                          return item.cell2.textWidth;
                        }
                        if (cell == 'cell3') {
                          return item.cell3.textWidth;
                        }
                      }
                    };
                    $scope.cuandoCombinaCelda2 = function(item) {
                      if (item.combinar === 3 && item.cell2.combinar) {
                        item.combinar = 5;
                      }
                    };
                  }
                  $scope.cargandoCampos = false; // Determina que la carga de datos termino
                } // [FIN] funcion inclusionDeContenidoTerminado
              );
              /**
               * [FIN] configuración de eventos
               */
            } // [FIN] función modalEditorDeElementosCtrl
          ] // [FIN] configuracion de controller
        }; // [FIN] objeto de configuración de directiva
      } // [FIN] función modalEditorDeElementos
    ] // [FIN] Configuración de directiva modalEditorDeElementos
  ) // [FIN] funcion directive
  .directive(
    // [INICIO] funcion directive
    'redimensionable', // Declaración de directiva redimensionable para cambio de tamaños en elementos HTML
    [
      // [INICIO] Configuración de directiva redimensionable
      /**
       * @description Retorna la configuración necesaria para agregar la directiva redimensionable
       *
       * @returns {object}
       */
      function redimensionable() {
        // [INICIO] función redimensionable
        return {
          // [INICIO] objeto de configuración de directiva
          restrict: 'AE', // Permite que la directiva funcione como atributo ('A') y como elemento ('E') en la vista
          scope: {
            // [INCIO] scope de directiva
            rDirecciones: '=', // @type {array} recibe las direciones en que podra modificarse el tamaño del elemento ['izquierda', 'derecha', 'arriba', 'abajo']
            rVelocidadX: '=', // @type {boolean} si es true duplica la velocidad de crecimiento en el eje x
            rVelocidadY: '=', // @type {boolean} si es true duplica la velocidad de crecimiento en el eje y
            rCapturador: '@', // @type {string} almacena el elemento html que se encarga de capturar el nuevo tamaño
            rDesactivado: '@', // @type {boolean} determina si la directiva se encuentra activada o desactivada
            rDatos: '=', // @type {array} almacena la lista de elementos a la cual pertenece el redimensionable
            rEstiloDelElemento: '=', // @type {object} almacena el objeto de estilo del elemento a editar (debe estar relacionado con ng-style)
            rPosicionDelElemento: '@' //@type {integer} almacena la posicion del elemento en la lista de rDatos
          }, // [FIN] scope de directiva
          /**
           * @description Función ejecutada al momento de iniciar la directiva redimensionable
           *
           * @param  {string} scope - scope de la directiva redimensionable
           * @param  {string} element - elemento HTML de la directiva redimensionable
           * @param  {string} attr - atributos del elemento de la directiva redimensionable
           */
          link: function linkRedimensionable(scope, element, attr) {
            // [INICIO] función linkRedimensionable
            element.addClass('redimensionable'); // Se le agrega la clase 'redimensionable' al elemento HTML de la directiva

            var estilo = scope.rDatos[parseInt(scope.rPosicionDelElemento)].options.size, // @type {object} se obtiene el objeto de estilo del elemento
              estiloDelElementoPadre = window.getComputedStyle(element[0].parentElement, null), // @type {object} se obtiene el objeto de estilo del elemento padre
              velocidadX = scope.rVelocidadX ? 2 : 1, // @type {boolean} se determina la velocidad en el eje x
              velocidadY = scope.rVelocidadY ? 2 : 1, // @type {boolean} se determina la velocidad en el eje y
              interiorCapturador = scope.rCapturador
                ? scope.rCapturador
                : '<span><i class="material-icons">more_vert</i></span>', // @type {string} se determina el elemento HTML capturador a dibujar en la vista
              elementoSiguiente, // @type {object} almacena el objeto siguiente en la lista de datos inmediatamente despues del elemento
              ancho, // @type {float} almacena el ancho del elemento
              alto, // @type {float} almacena el alto del elemento
              anchoElementoSiguiente, // @type {float} almacena el ancho de elementoSiguiente
              altoElementoSiguiente, // @type {float} almacena el alto de elementoSiguiente
              anchoElementoPadre, // @type {float} almacena el ancho de estiloDelElementoPadre
              altoElementoPadre, // @type {float} almacena el alto de estiloDelElementoPadre
              posicionInicial, // @type {number} almacena la posicion inicial del capturador
              direccionDrag, // @type {string} almacena la direccion en que se arrastra el capturador
              eje; // @type {string} detemrina el eje en que se mueve el capturador mientras se arrastra
            /**
             * @description Función ejecutada al momento de iniciar el evento drag
             *
             * @param  {object} evento - objeto de evento ejecutado
             * @param  {string} direccion - direccion en que se ejecuta el evento drag
             */
            var inicioDrag = function(evento, direccion) {
              // [INICIO] función inicioDrag
              direccionDrag = direccion; // Almacena la direccion en que se ejecuta el evento drag en variable direccionDrag
              eje = direccionDrag === 'izquierda' || direccionDrag === 'derecha' ? 'x' : 'y'; // Determina el eje en que se movera el capturador
              posicionInicial = eje === 'x' ? evento.clientX : evento.clientY; // Determina la posicion inicial del capturador
              ancho = parseFloat(estilo['width']); // obtiene el ancho del elemento
              alto = parseFloat(estilo['height']); // obtiene el alto del elemento
              anchoElementoPadre = parseInt(estiloDelElementoPadre.getPropertyValue('width')); // obtiene el ancho del elemento HTML padre
              altoElementoPadre = parseInt(estiloDelElementoPadre.getPropertyValue('height')); // obtiene el alto del elemento HTML padre
              // Si existe rDatos y rPosicionDelElemento seteados en la directiva, se obtiene el elemento siguiente
              if (scope.rDatos && scope.rPosicionDelElemento) {
                // [INCIO] if
                elementoSiguiente = scope.rDatos[parseFloat(scope.rPosicionDelElemento) + 1]; // Se obtiene de rDatos el elemento siguiente
                // Se comprueba si existe realmente un elemento siguiente
                if (!!elementoSiguiente) {
                  // [INCIO] if
                  anchoElementoSiguiente = parseFloat(elementoSiguiente.options.size['width']); // obtiene el ancho del elemento HTML siguiente
                  altoElementoSiguiente = parseFloat(elementoSiguiente.options.size['height']); // obtiene el alto del elemento HTML siguiente
                } // [FIN] if
              } // [FIN] if
              element.addClass('no-transicion'); // Previene otras transiciones mientras se ejecuta el evento dragging agregando la clase 'no-transicion' al elemento
              // Configuracion de eventos de drag
              document.addEventListener('mousemove', dragging, false); // se agrega listener de evento dragging
              document.addEventListener('mouseup', finDrag, false); // se agrega listener de evento finDrag
              // Desactiva la propagacion en la ejecución del evento dragging
              if (evento.stopPropagation) evento.stopPropagation();
              if (evento.preventDefault) evento.preventDefault();
              evento.cancelBubble = true;
              evento.returnValue = false;
              scope.$apply(); // Se aplica manualmente los cambios en el scope de elemento
            }; // [FIN] función inicioDrag
            /**
             * @description Función llamada mientras se ejecuta el evento drag
             *
             * @param  {object} evento - objeto de evento ejecutado
             */
            var dragging = function(evento) {
              // [INICIO] función dragging
              var offset =
                  eje === 'x' ? posicionInicial - evento.clientX : posicionInicial - evento.clientY, // @type {number} almacena la diferencia entre la posicion inicial y actual del capturador
                delta = Math.round(
                  eje === 'x'
                    ? ((offset * velocidadX) / anchoElementoPadre) * 100
                    : ((offset * velocidadY) / altoElementoPadre) * 100
                ); // @type {number} almacena un delta en porcentaje segun elemento padre
              // Ejecuta una actualizacion del estilo segun la dirección en que se ejecuta el drag del capturador
              switch (
                direccionDrag // [INICIO] switch
              ) {
                case 'arriba': // En caso de que la direccion del capturador sea arriba
                  scope.rEstiloDelElemento['height'] = alto + delta + '%'; // Se actualiza el alto del elemento aumentandolo según el delta calculado
                  // Se verifica si existe el elemento siguiente
                  if (!!elementoSiguiente) {
                    elementoSiguiente.options.size['height'] = altoElementoSiguiente - delta + '%'; // Se actualiza el alto del elemento siguiente disminuyendolo según el delta calculado
                  }
                  break;
                case 'abajo': // En caso de que la direccion del capturador sea abajo
                  scope.rEstiloDelElemento['height'] = alto - delta + '%'; // Se actualiza el alto del elemento disminuyendolo según el delta calculado
                  // Se verifica si existe el elemento siguiente
                  if (!!elementoSiguiente) {
                    elementoSiguiente.options.size['height'] = altoElementoSiguiente + delta + '%'; // Se actualiza el alto del elemento siguiente aumentandolo según el delta calculado
                  }
                  break;
                case 'derecha': // En caso de que la direccion del capturador sea derecha
                  scope.rEstiloDelElemento['width'] = ancho - delta + '%'; // Se actualiza el ancho del elemento siguiente disminuyendolo según el delta calculado
                  // Se verifica si existe el elemento siguiente
                  if (!!elementoSiguiente) {
                    elementoSiguiente.options.size['width'] = anchoElementoSiguiente + delta + '%'; // Se actualiza el ancho del elemento siguiente aumentandolo según el delta calculado
                  }
                  break;
                case 'izquierda': // En caso de que la direccion del capturador sea izquierda
                  scope.rEstiloDelElemento['width'] = ancho + delta + '%'; // Se actualiza el ancho del elemento aumentandolo según el delta calculado
                  // Se verifica si existe el elemento siguiente
                  if (!!elementoSiguiente) {
                    elementoSiguiente.options.size['width'] = anchoElementoSiguiente - delta + '%'; // Se actualiza el ancho del elemento siguiente disminuyendolo según el delta calculado
                  }
                  break;
              } // [FIN] switch
            }; // [FIN] función dragging
            /**
             * @description Función llamada cuando se termina la ejecución del evento drag
             *
             * @param  {object} evento - objeto de evento ejecutado
             */
            var finDrag = function(e) {
              // [INICIO] función finDrag
              scope.$apply(); // Se aplica manualmente los cambios en el scope de elemento
              document.removeEventListener('mousemove', dragging, false); // se agrega listener de evento dragging
              document.removeEventListener('mouseup', finDrag, false); // se agrega listener de evento finDrag
              element.removeClass('no-transicion'); // se elimina la clase 'no-transicion' del elemento
            }; // [FIN] función finDrag
            // Se configuran los capturadores agregados al elemento
            scope.rDirecciones.forEach(function(direccion) {
              // [INICIO] forEach
              var capturador = document.createElement('div'); // @type {object} contiene un elemento div HTML
              capturador.setAttribute('class', 'rg-' + direccion); // se le agrega una clase para configurar su estilo CSS
              capturador.innerHTML = interiorCapturador; // Se le ingresa como contenido HTML el capturador almacenado en interiorCapturador
              element[0].appendChild(capturador); // Se agrega el capturador al elemento
              capturador.ondragstart = function() {
                return false;
              }; // Se elimina el evento nativo 'ondragstart' del capturador
              capturador.addEventListener(
                'mousedown',
                function mouseDown(evento) {
                  // [INICIO] listener mouseDown
                  // Verifica si la directiva se encuentra desactivada y si se ha presionado el boton izquierdo del mouse
                  if (!(scope.rDesactivado === 'true') && evento.which === 1) {
                    inicioDrag(evento, direccion); // Se da inicio al evento drag
                  }
                },
                false
              ); // [FIN] listener mouseDown
            }); // [FIN] forEach
          } // [FIN] función linkRedimensionable
        }; // [FIN] objeto de configuración de directiva
      } // [FIN] función redimensionable
    ] // [FIN] Configuración de directiva redimensionable
  ) // [FIN] funcion directive
  .directive(
    // [INICIO] funcion directive
    'clickParaEditar', // Declaración de directiva clickParaEditar para edicion de texto inline
    [
      // [INICIO] Configuración de directiva clickParaEditar
      '$timeout',
      /**
       * @description Retorna la configuración necesaria para agregar la directiva clickParaEditar
       *
       * @returns {object}
       */
      function clickParaEditar($timeout) {
        // [INICIO] función clickParaEditar
        return {
          // [INICIO] objeto de configuración de directiva
          restrict: 'A', // Permite que la directiva funcione como atributo ('A') en la vista
          require: 'ngModel', // Exige el atributo ngModel junto a la directiva
          scope: {
            // [INCIO] scope de directiva
            modelo: '=ngModel', // @type {*} almacena modelo a trabajar en directiva
            activo: '=clickParaEditarActivo', // @type {boolean} establece si elemento se encuentra desactivado
            tipo: '@type' // @type {string} almacena tipo de elemento
          }, // [FIN] scope de directiva
          replace: true, // Reemplaza el template por elemento donde se declara la directiva
          transclude: false, // Establece que se ocupara un scope interno
          templateUrl: 'app/templates/editorPlantillaTemplates/directivas/clickParaEditar.tpl.html', // ubicacion de template de directiva
          link: function clickParaEditarLink(scope, elemento, atributos) {
            // [INICIO] función clickParaEditarLink
            // Se establece editar en 'false' para establecer estado de edicion
            scope.editar = false;
            // Se hace una referencia local para poder anular los cambios, esto solo ocurre una vez y persiste
            scope.modalLocal = scope.modelo;
            /**
             * @description almacena los cambios en modelo real
             */
            scope.guardar = function() {
              // [INICIO] función guardar
              if (!scope.activo) {
                return;
              }
              scope.modelo = scope.modalLocal;
              scope.intercambiarEstado();
            }; // [FIN] función guardar
            /**
             * @description no aplica los cambios
             */
            scope.cancelar = function() {
              // [INICIO] función cancelar
              if (!scope.activo) {
                return;
              }
              scope.modalLocal = scope.modelo;
              scope.intercambiarEstado();
            }; // [FIN] función cancelar
            /**
             * @description intercambia estado y elementos de edicion
             */
            scope.intercambiarEstado = function() {
              // [INICIO] función intercambiarEstado
              if (!scope.activo) {
                return;
              }
              // Se intercambia el valor de 'editar' de true a false (y vice versa)
              scope.editar = !scope.editar;
              // Se obtiene el elemento HTML del DOM según su tipo
              var x1 = elemento[0].querySelector('.' + scope.tipo);
              $timeout(function() {
                // enfocar si está en edición, difuminar si no. algunos IE dejarán el cursor sin el desenfoque
                scope.editar ? x1.focus() : x1.blur();
              }, 0);
            }; // [FIN] función intercambiarEstado
          } // [FIN] función clickParaEditarLink
        }; // [FIN] objeto de configuración de directiva
      } // [FIN] función clickParaEditar
    ] // [FIN] Configuración de directiva clickParaEditar
  ) // [FIN] funcion directive
  .directive(
    // [INICIO] funcion directive
    'ngEnter', // Declaración de directiva ngEnter para ejecutar evento cuando se presiona enter
    [
      // [INICIO] Configuración de directiva ngEnter
      /**
       * @description Retorna la configuración necesaria para agregar la directiva ngEnter
       *
       * @returns {object}
       */
      function ngEnter() {
        // [INICIO] función ngEnter
        return function ngEnterLink(scope, elemento, atributos) {
          // [INICIO] función ngEnterLink
          // Se le asigna al elemento evento 'keydown' y 'keypress'
          elemento.bind('keydown keypress', function eventoCuandoPresionaEnter(evento) {
            // [INICIO] listener eventoCuandoPresionaEnter
            // Se verifica si la tecla presionada es 'enter'
            if (evento.which === 13) {
              // si es asi, se ejecuta la funcion asociada a ngEnter en la vista
              scope.$apply(function() {
                scope.$eval(atributos.ngEnter);
              });
              // Desactiva la propagacion en la ejecución del evento eventoCuandoPresionaEnter
              if (evento.stopPropagation) evento.stopPropagation();
              if (evento.preventDefault) evento.preventDefault();
              evento.cancelBubble = true;
              evento.returnValue = false;
            }
          }); // [FIN] listener eventoCuandoPresionaEnter
        }; // [FIN] función ngEnterLink
      } // [FIN] función ngEnter
    ] // [FIN] Configuración de directiva ngEnter
  ) // [FIN] funcion directive
  .directive(
    // [INICIO] funcion directive
    'elementoEstructuraEditor', // Declaración de directiva elementoEstructuraEditor para almacenar elementos HTML utilizando tecnologia dragula
    [
      // [INICIO] Configuración de directiva elementoEstructuraEditor
      'dragulaService', // Se agrega modulo Angular dragulaService
      /**
       * @description Retorna la configuración necesaria para agregar la directiva elementoEstructuraEditor
       *
       * @returns {object}
       */
      function elementoEstructuraEditor(dragulaService) {
        // [INICIO] función elementoEstructuraEditor
        return {
          // [INICIO] objeto de configuración de directiva
          restrict: 'E', // Permite que la directiva funcione como elemento ('E') en la vista
          scope: {
            // [INCIO] scope de directiva
            elDatos: '=', // @type {object} almacena los datos del elemento de estructura
            bloquesDeElementos: '=elTemplates', // @type {object} almacena los elementos para editor de documentos
            acciones: '=elAcciones' // @type {object} almacena las acciones que se pueden realizar en elementos para editor de documentos
          }, // [FIN] scope de directiva
          templateUrl:
            'app/templates/editorPlantillaTemplates/directivas/elementoEstructuraEditor.tpl.html', // ubicacion de template de directiva
          controller: [
            // [INCIO] controller de directiva
            '$rootScope',
            '$scope',
            'funcionesUtilesEditor',
            function elementoEstructuraEditorCtrl($rootScope, $scope, funcionesUtilesEditor) {
              // [INICIO] función elementoEstructuraEditorCtrl
              /**
               * [INICIO] Configuración de dragula
               */
              $scope.idDragula = 'sub-container-' + $scope.elDatos.id; // @type {string} almacena el id para contenedor de dragula
              /**
               * @description obtiene el nombre de la instancia del contenedor de dragula
               *
               * @returns {string}
               */
              $scope.obtenerNombreDragula = function() {
                // [INICIO] función obtenerNombreDragula
                return '"' + $scope.idDragula + '"'; // se le agregan comillas dobles (") al idDragula para generar el nombre
              }; // [FIN] función obtenerNombreDragula
              /**
               * @description desactiva bordes
               *
               * @returns {string}
               */
              $scope.bordesInactivos = function() {
                // [INICIO] función bordesInactivos
                var obj = {};
                if (!$scope.elDatos.options.borders_actives.top) {
                  obj['border-top'] = '0px';
                }
                if (!$scope.elDatos.options.borders_actives.bottom) {
                  obj['border-bottom'] = '0px';
                }
                if (!$scope.elDatos.options.borders_actives.left) {
                  obj['border-left'] = '0px';
                }
                if (!$scope.elDatos.options.borders_actives.right) {
                  obj['border-right'] = '0px';
                }
                return obj; // bordes inactivos
              }; // [FIN] función bordesInactivos
              /**
               * Configuracion de dragula para constructor
               */
              dragulaService.options(
                // [INICIO] configuración de opciones de dragulaService
                $scope, // Se asigna scope a la configuracion
                $scope.idDragula, // Se asigna el id de dragula
                {
                  // [INCIO] opciones dragula
                  copy: false, // Se desactiva opcion de copiar elementos
                  copySortSource: false, // Se desactiva copiar elementos segun orden de fuente
                  /**
                   * @description determina si un elemento puede moverse según condiciones
                   *
                   * @param {object} el - elemento en movimiento
                   * @param {object} source - contenedor del elemento
                   * @param {object} handle - elemento que realmente esta activando el movimiento
                   * @param {object} sibling - elemento hermano en el movimiento
                   *
                   * @returns {boolean}
                   */
                  moves: function(el, source, handle, sibling) {
                    // [INICIO] función moves
                    return $(handle).hasClass('move-' + $scope.idDragula); // Retorna true si contiene la clase move-{{idDragula}}
                  }, // [FIN] función moves
                  /**
                   * @description este metodo podría retornar true para los elementos que no pueden ser arrastrados
                   *
                   * @param {object} el - elemento en movimiento
                   * @param {object} handle - elemento que realmente esta activando el movimiento
                   *
                   * @returns {boolean}
                   */
                  invalid: function(el, handle) {
                    // [INICIO] función invalid
                    return false; // permite que cualquier arrastre se inicie
                  } // [FIN] función invalid
                } // [FIN] opciones dragula
              ); // [FIN] configuración de opciones de dragulaService
              /**
               * [FIN] Configuración de dragula
               */

              /**
               * @description recalcula el ancho del contenedor y sus elementos hijos
               *
               * @param {object} data - datos del elemento
               */
              $scope.recalcularWidth = function(data) {
                // [INICIO] funcion recalcularWidth
                // Evita recalcular el ancho de los elementos cuando se encuentra en edicion
                if (data.editando) {
                  // [INICIO] if
                  return; // Termina la ejecucion de la funcion
                } // [FIN] if
                // Si el valor de direction es 'horizontal'
                if (data.options.direction == 'horizontal') {
                  // [INICIO] if
                  for (var i = 0; i < data.options.elements.length; i++) {
                    // [INICIO] for
                    // Se recorre la lista de elementos y se asigna un porcentaje del ancho del padre segun la cantidad de elementos
                    data.options.elements[i].options.size.width =
                      100 / data.options.elements.length + '%';
                  } // [fin] for
                } // [FIN] if
                // Si el valor de direction es 'vertical'
                else if (data.options.direction == 'vertical') {
                  // [INICIO] else if
                  for (var i = 0; i < data.options.elements.length; i++) {
                    // [INICIO] for
                    // Se recorre la lista de elementos y se asigna 100% de ancho a todos
                    data.options.elements[i].options.size.width = '100%';
                  } // [FIN] for
                } // [FIN] else if
              }; // [FIN] funcion recalcularWidth
              /**
               * @description Se configura un observador para cuando la variable direction de un elemento de estructura cambia
               */
              $scope.$watch(
                /**
                 * @description visualiza el valor actual de variable direction
                 *
                 * @param {object} $scope - scope del elemento
                 *
                 * @returns {string}
                 */
                function observarDireccion($scope) {
                  // [INICIO] funcion observarDireccion
                  return $scope.elDatos.options.direction;
                }, // [FIN] funcion observarDireccion
                /**
                 * @description se ejecuta cuando cambia el valor de direction
                 *
                 * @params {*} valorNuevo - Obtiene el valor nuevo para el atributo direction
                 * @params {*} valorAnterior - Obtiene el valor anterior para el atributo direction
                 */
                function cuandoCambiaDireccion(valorNuevo, valorAnterior) {
                  // [INICIO] funcion cuandoCambiaDireccion
                  // Comprueba si el valor anterior y el valor nuevo son iguales
                  if (valorNuevo === valorAnterior) {
                    // En caso de ser iguales, se termina la ejecucion de la funcion cuandoCambiaDireccion
                    return;
                  }
                  // Cuando cambia el valor de direction, recalcula el ancho en el contenedor
                  $scope.recalcularWidth($scope.elDatos);
                } // [FIN] funcion cuandoCambiaDireccion
              );
              /**
               * @description elimina un elemento de la lista de elementos
               *
               * @param {object} elemento - elemento a eliminar
               * @param {array} elementos - lista de elementos contenedor de elemento
               */
              $scope.removerElemento = function(elemento, elementos) {
                // [INICIO] función removerElemento
                // se elimina un elemento de la lista utilizando el metodo removerElemento según acciones
                $scope.acciones.removerElemento(elemento, elementos).then(function() {
                  // Cuando se elimina el elemento, se recalcula el ancho en el contenedor
                  $scope.recalcularWidth($scope.elDatos);
                });
              }; // [FIN] función removerElemento
              /**
               * @description configuracion de estilo en la vista para el titulo del contenedor en la directiva
               *
               * @param {object} options - opciones del elemento
               *
               * @returns {object}
               */
              $scope.configuracionTituloContenedor = function(options) {
                // [INICIO] función configuracionTituloContenedor
                var temp = angular.copy(options.styleTitle); // @type {object} Se obtiene una copia del estilo
                // Se configuran las opciones que requieren una unidad de medida
                temp['font-size'] += 'px';
                temp['font-family'] = options.style['font-family']; // Se asignan las configuraciones de fuente generales de la directiva
                temp['width'] = '100%'; // se le asigna fijo un ancho de 100%
                return temp; // se retorna el objeto modificado de estilo
              }; // [FIN] función configuracionTituloContenedor
              /**
               * [INICIO] Eventos de Directiva
               */
              // @type {function} se almacena el listener en eventoDragYDropParaElementos
              var eventoDragYDropParaElementos = $rootScope.$on(
                'elementos.drop', // Nombre de evento a escuchar
                /**
                 * @description listener para eventos de arrastrar y soltar de elementos.
                 *
                 * @param {object} evento - objeto que contiene el evento 'element.drop'
                 * @param {object} el - elemento en movimiento
                 * @param {object} target - contenedor objetivo del elemento
                 * @param {object} source - contenedor del elemento actual
                 * @param {object} sibling - elemento hermano en el movimiento
                 *
                 * @returns {null}
                 */
                function eventoDragYDropParaElementos(evento, el, target, source, sibling) {
                  // [INICIO] funcion eventoDragYDropParaElementos
                  // Se verifica si el contenedor objetivo corresponde a esta instancia de la directiva
                  if (
                    $(target).hasClass('sub-container') &&
                    $(target).attr('id') == $scope.elDatos.id
                  ) {
                    // [INICIO] if
                    var index = $(sibling).index(); // @type {integer} almacena la ubicacion del elemento según elemento adjunto en la lista de elementos
                    var tipoDeElemento = $(el).data('type'); // @type {string} almacena el tipo del elemento
                    var elemento = angular.extend(
                      // @type {object} se genera una instancia del elemento a almacenar
                      {}, // Se ocupa para crear una nueva instancia
                      {
                        // [INICIO] objeto para iniciar elemento
                        type: tipoDeElemento, // @type {string} almacena el tipo del elemento copiado (HTML)
                        data_type: $scope.bloquesDeElementos[tipoDeElemento].element.data_type, // @type {object} almacena el tipo del elemento (objeto fuente)
                        options: angular.copy(
                          $scope.bloquesDeElementos[tipoDeElemento].defaultOptions
                        ) // @type {object} almacena las opciones del elemento (objeto fuente)
                      } // [FIN] objeto para iniciar elemento
                    );
                    elemento.id = funcionesUtilesEditor.uid(); // @type {string} se asigna un id generado por funcion uid
                    $('.doc-container li').remove(); // Se remueve la copia nativa de dragula
                    // Si index no es un valor valido como posicion en la lista
                    if (index == -1) {
                      // [INICIO] if
                      $scope.elDatos.options.elements.push(elemento); // Se asigna a la lista en la ultima posicion
                    } // [FIN] if
                    // Si index corresponde a una posicion valida en la lista
                    else {
                      // [INICIO] else
                      $scope.elDatos.options.elements.splice(index - 1, 0, elemento); // Se asigna en la posicion anterior
                    } // [FIN] else
                    $scope.recalcularWidth($scope.elDatos); // se recalcula el ancho en el contenedor
                    // Desactiva la propagacion en la ejecución del evento eventoDragYDropParaElementos
                    if (evento.stopPropagation) evento.stopPropagation();
                    if (evento.preventDefault) evento.preventDefault();
                    evento.cancelBubble = true;
                    evento.returnValue = false;
                    $scope.$apply(); // Se aplica manualmente los cambios en el scope de elemento
                  } // [FIN] if
                  return;
                } // [FIN] funcion eventoDragYDropParaElementos
              );
              // @type {function} se almacena el listener en eventoDragYDropParaElementosEnContenedor
              var eventoDragYDropParaElementosEnContenedor = $rootScope.$on(
                $scope.idDragula + '.drop', // Nombre de evento a escuchar
                /**
                 * @description listener para eventos de arrastrar y soltar de elementos dentro del contenedor.
                 *
                 * @param {object} evento - objeto que contiene el evento '{{idDragula}}.drop'
                 * @param {object} el - elemento en movimiento
                 * @param {object} target - contenedor objetivo del elemento
                 * @param {object} source - contenedor del elemento actual
                 * @param {object} sibling - elemento hermano en el movimiento
                 *
                 * @returns {null}
                 */
                function eventoDragYDropParaElementosEnContenedor(
                  evento,
                  el,
                  target,
                  source,
                  sibling
                ) {
                  // [INICIO] funcion eventoDragYDropParaElementosEnContenedor
                  // Se verifica si el contenedor objetivo corresponde a esta instancia de la directiva
                  if (
                    target.hasClass('sub-container') &&
                    $(target).attr('id') == $scope.elDatos.id
                  ) {
                    // [INICIO] if
                    var id = $(el).attr('id'); // @type {string} almacena el id del elemento
                    var index = $(sibling).index(); // @type {integer} almacena la ubicacion del elemento adjunto en la lista de elementos
                    // Si index no es un valor valido como posicion en la lista
                    if (index == -1) {
                      // [INICIO] if
                      index = $scope.elDatos.options.elements.length - 1; // Se obtiene la ultima posicion
                    } // [FIN] if
                    // Si index corresponde a una posicion valida en la lista
                    else {
                      // [INICIO] else
                      index--; // se obtiene la posicion anterior del elemento adjunto
                    } // [FIN] else
                    // Se busca el elemento en la lista según id
                    var elemento = funcionesUtilesEditor.findWhere(
                      $scope.elDatos.options.elements, // Lista de elementos
                      {
                        // [INICIO] Opciones de búsqueda
                        id: id // se asigna id para buscar elemento
                      } // [FIN] Opciones de búsqueda
                    );
                    var oldIndex = $scope.elDatos.options.elements.indexOf(elemento); // @type {integer} Obtención de la posición anterior del elemento
                    // Verifica si el index ingresado es mayor a la cantidad de elementos
                    if (index >= $scope.elDatos.options.elements.length) {
                      // [INICIO] if
                      var k = index - $scope.elDatos.options.elements.length; // @type {integer} Obtención de un index corregido
                      while (k-- + 1) {
                        // [INICIO] while
                        // Se ingresan elementos para corregir el error
                        $scope.elDatos.options.elements.push(undefined);
                      } // [FIN] while
                    } // [FIN] if
                    $scope.elDatos.options.elements.splice(
                      index,
                      0,
                      $scope.elDatos.options.elements.splice(oldIndex, 1)[0]
                    ); // se mueve el elemento a la nueva posicion
                    $scope.recalcularWidth($scope.elDatos); // se recalcula el ancho en el contenedor
                    // Desactiva la propagacion en la ejecución del evento eventoDragYDropParaElementosEnContenedor
                    if (evento.stopPropagation) evento.stopPropagation();
                    if (evento.preventDefault) evento.preventDefault();
                    evento.cancelBubble = true;
                    evento.returnValue = false;
                    $scope.$apply(); // Se aplica manualmente los cambios en el scope de elemento
                  } // [FIN] if
                  return;
                } // [FIN] funcion eventoDragYDropParaElementosEnContenedor
              );
              $scope.$on(
                '$destroy', // Nombre de evento a escuchar
                function cuandoSeDestruyeElScope() {
                  // [INICIO] funcion cuandoSeDestruyeElScope
                  eventoDragYDropParaElementos(); // Se destruye el listener eventoDragYDropParaElementos
                  eventoDragYDropParaElementosEnContenedor(); // Se destruye el listener eventoDragYDropParaElementosEnContenedor
                } // [FIN] funcion cuandoSeDestruyeElScope
              );
              /**
               * [FIN] Eventos de Directiva
               */
            } // [FIN] función elementoEstructuraEditorCtrl
          ] // [FIN] controller de directiva
        }; // [FIN] objeto de configuración de directiva
      } // [FIN] función elementoEstructuraEditor
    ] // [FIN] Configuración de directiva elementoEstructuraEditor
  ) // [FIN] funcion directive
  .directive(
    // [INICIO] funcion directive
    'elementoTablaDinamicaEditor', // Declaración de directiva elementoTablaDinamicaEditor
    [
      // [INICIO] Configuración de directiva elementoTablaDinamicaEditor
      /**
       * @description Retorna la configuración necesaria para agregar la directiva elementoTablaDinamicaEditor
       *
       * @returns {object}
       */
      function elementoTablaDinamicaEditor() {
        // [INICIO] función elementoTablaDinamicaEditor
        return {
          // [INICIO] objeto de configuración de directiva
          restrict: 'E', // Permite que la directiva funcione como elemento ('E') en la vista
          scope: {
            // [INCIO] scope de directiva
            elemento: '=elDatos', // @type {object} almacena los datos del elemento de tabla estatica
            acciones: '=elAcciones', // @type {object} almacena las acciones que se pueden realizar en elementos para editor de documentos
            enEdicion: '=enEdicion' // @type {false} determina si el elemento se encuentra dentro del modal
          }, // [FIN] scope de directiva
          templateUrl:
            'app/templates/editorPlantillaTemplates/directivas/elementoTablaDinamicaEditor.tpl.html', // ubicacion de template de directiva
          controller: [
            // [INCIO] controller de directiva
            '$rootScope',
            '$scope',
            '$timeout',
            'funcionesUtilesEditor',
            function elementoTablaDinamicaEditorCtrl(
              $rootScope,
              $scope,
              $timeout,
              funcionesUtilesEditor
            ) {
              // [INICIO] función elementoTablaDinamicaEditorCtrl
              $scope.enEdicion = !!$scope.enEdicion;
              /**
               * @description Configuración de estilo para tablas, en general, cabecera y celdas
               *
               * @params {string} tipo - tipo de celda ['general' | 'cabecera' | 'celda']
               * @params {boolean} indice - determina que celda corresponde
               * @params {boolean} filaPar - determina si la fila de la celda es par o impar
               * @params {boolean} celdaPar - determina si la celda es par o impar
               *
               * @returns {object}
               */
              $scope.estiloTabla = function(tipo, indice, filaPar, celdaPar) {
                var style = {};
                switch (tipo) {
                  case 'general':
                    style['display'] = 'table';
                    style['table-layout'] = 'fixed';
                    style['border-collapse'] = 'collapse';
                    if ($scope.elemento.options.withBorders) {
                      style = angular.extend({}, style, $scope.elemento.options.border.style);
                      if ($scope.elemento.options.typeBorders == 'horizontal') {
                        style = angular.extend(
                          {},
                          style,
                          $scope.elemento.options.border.horizontal
                        );
                      }
                    }
                    break;
                  case 'cabecera':
                    $scope.elemento.options.textStyle.header.color = funcionesUtilesEditor.getContrastColor(
                      $scope.elemento.options.colors.primary
                    );
                    style = angular.extend(
                      {},
                      style,
                      {
                        'padding-left': '0.1cm',
                        'padding-right': '0.1cm',
                        'background-color': $scope.elemento.options.colors.primary,
                        width: $scope.elemento.options.columnsWidths[indice]
                      },
                      $scope.elemento.options.textStyle.header
                    );
                    if (
                      $scope.elemento.options.withBorders &&
                      $scope.elemento.options.typeBorders != 'external'
                    ) {
                      style = angular.extend({}, style, $scope.elemento.options.border.style);
                      if ($scope.elemento.options.typeBorders == 'horizontal') {
                        style = angular.extend(
                          {},
                          style,
                          $scope.elemento.options.border.horizontal
                        );
                      }
                    }
                    break;
                  case 'celda':
                    style = angular.extend({}, style, $scope.elemento.options.textStyle.cell, {
                      'padding-left': '0.1cm',
                      'padding-right': '0.1cm',
                      width: $scope.elemento.options.columnsWidths[indice]
                    });
                    if ($scope.elemento.options.withStripes) {
                      if (filaPar && $scope.elemento.options.directionStripes == 'horizontal') {
                        style['background-color'] = $scope.elemento.options.colors.secondary;
                        $scope.elemento.options.textStyle.cell.color = funcionesUtilesEditor.getContrastColor(
                          $scope.elemento.options.colors.secondary
                        );
                        style['color'] = $scope.elemento.options.textStyle.cell.color;
                      } else if (
                        !filaPar &&
                        $scope.elemento.options.directionStripes == 'horizontal'
                      ) {
                        style['background-color'] = $scope.elemento.options.colors.tertiary;
                        $scope.elemento.options.textStyle.cell.color = funcionesUtilesEditor.getContrastColor(
                          $scope.elemento.options.colors.tertiary
                        );
                        style['color'] = $scope.elemento.options.textStyle.cell.color;
                      }
                      if (celdaPar && $scope.elemento.options.directionStripes == 'vertical') {
                        style['background-color'] = $scope.elemento.options.colors.secondary;
                        $scope.elemento.options.textStyle.cell.color = funcionesUtilesEditor.getContrastColor(
                          $scope.elemento.options.colors.secondary
                        );
                        style['color'] = $scope.elemento.options.textStyle.cell.color;
                      } else if (
                        !celdaPar &&
                        $scope.elemento.options.directionStripes == 'vertical'
                      ) {
                        style['background-color'] = $scope.elemento.options.colors.tertiary;
                        $scope.elemento.options.textStyle.cell.color = funcionesUtilesEditor.getContrastColor(
                          $scope.elemento.options.colors.tertiary
                        );
                        style['color'] = $scope.elemento.options.textStyle.cell.color;
                      }
                    } else {
                      $scope.elemento.options.textStyle.cell.color = 'rgba(0,0,0,1)';
                      style['color'] = $scope.elemento.options.textStyle.cell.color;
                    }
                    if (
                      $scope.elemento.options.withBorders &&
                      $scope.elemento.options.typeBorders != 'external'
                    ) {
                      style = angular.extend({}, style, $scope.elemento.options.border.style);
                      if ($scope.elemento.options.typeBorders == 'horizontal') {
                        style = angular.extend(
                          {},
                          style,
                          $scope.elemento.options.border.horizontal
                        );
                      }
                    }
                    break;
                }
                // en el caso que el estilo del elemento contenga 'font-size' se configura tambien la unidad
                if ('font-size' in style) {
                  style['font-size'] += 'px';
                }
                // en el caso que el estilo del elemento contenga 'line-height' se configura tambien la unidad
                if ('line-height' in style) {
                  style['line-height'] += 'px';
                }
                return style;
              };
              /**
               * @description Se configura un observador para cuando la variable columns de un elemento de tabla cambie
               */
              $scope.configurarResize = function(indice, elemento, esElUltimo) {
                // Se configura el resize solo para entorno de edicion
                var $elemento = $(
                  '#modal_edit_element ' +
                    'elemento-tabla-dinamica-editor ' +
                    '#table' +
                    $scope.elemento.id +
                    ' ' +
                    elemento
                );
                if ($scope.enEdicion && !esElUltimo && $elemento.children('.resizer').length == 0) {
                  $elemento
                    .css({
                      /* required to allow resizer embedding */
                      position: 'relative'
                    })
                    /* check .resizer CSS */
                    .prepend("<div class='resizer'></div>")
                    .resizable({
                      resizeHeight: false,
                      // we use the column as handle and filter
                      // by the contained .resizer element
                      handleSelector: '',
                      onDragStart: function(e, $el, opt) {
                        // only drag resizer
                        if (!$(e.target).hasClass('resizer')) return false;
                        return true;
                      },
                      onDragEnd: function() {
                        $scope.elemento.options.columnsWidths[indice] =
                          Number($elemento.css('width').replace('px', '')) * 0.02645833 + 'cm';
                      }
                    });
                } else if (
                  $scope.enEdicion &&
                  esElUltimo &&
                  $elemento.children('.resizer').length > 0
                ) {
                  $elemento
                    .resizable('destroy')
                    .find('.resizer')
                    .remove();
                  $scope.elemento.options.columnsWidths[indice] = 'auto';
                }
              };
              /**
               * @description Se configura un observador para cuando la variable columns de un elemento de tabla cambie
               */
              $scope.$watch(
                // [INICIO] $watch
                /**
                 * @description visualiza el valor actual de variable columns
                 *
                 * @param {object} $scope - scope del elemento
                 *
                 * @returns {string}
                 */
                function observarColumnas($scope) {
                  // [INICIO] funcion observarColumnas
                  return $scope.elemento.options.columns;
                }, // [FIN] funcion observarColumnas
                /**
                 * @description se ejecuta cuando cambia el valor de columns
                 *
                 * @params {*} valorNuevo - Obtiene el valor nuevo para el atributo columns
                 * @params {*} valorAnterior - Obtiene el valor anterior para el atributo columns
                 */
                function cuandoCambianColumnas(valorNuevo, valorAnterior) {
                  // [INICIO] funcion cuandoCambianColumnas
                  // Comprueba si el elemento se encuentra dentro del modal de edicion
                  if ($scope.enEdicion) {
                    // Si se encuentra dentro, termina la ejecucion de watch
                    return;
                  }
                  // Comprueba si el valor anterior y el valor nuevo son iguales
                  if (valorNuevo === valorAnterior) {
                    // En caso de ser iguales, se termina la ejecucion de la funcion cuandoCambianColumnas
                    return;
                  }
                  // Comprueba si el valor nuevo de columnas es menor a 0 o undefined
                  if (valorNuevo < 0 || !valorNuevo) {
                    // Si es menor a 0 o undefined, se establece el valor en 0 y se termina la ejecucion de la función
                    $scope.elemento.options.columns = 0;
                    valorNuevo = 0;
                  }
                  // Cuando cambia el valor de columnas, redimensiona los arreglos de texto
                  // Comprueba si el valor nuevo de Columnas es menor al valor anterior
                  if (valorNuevo < valorAnterior) {
                    // Si es menor, elimina la cantidad de Columnas igual a la diferencia entre el valor anterior y el actual
                    var diferencia = valorAnterior - valorNuevo;
                    for (var i = 0; i < diferencia; i++) {
                      $scope.elemento.options.textHeaders.pop();
                      $scope.elemento.options.variables.pop();
                      $scope.elemento.options.columnsWidths.pop();
                    }
                  }
                  // Comprueba si el valor nuevo de Columnas es mayor al valor anterior
                  else if (valorNuevo > valorAnterior) {
                    // Si es mayor, agrega la cantidad de Columnas igual a la diferencia entre el valor anterior y el actual, con celdas nuevas con un texto por defecto
                    var diferencia = valorNuevo - valorAnterior;
                    var numColumnas = valorAnterior;
                    for (var i = 0; i < diferencia; i++) {
                      var textoHeader = 'Columna ' + (numColumnas + i + 1);
                      $scope.elemento.options.textHeaders.push(textoHeader);
                      $scope.elemento.options.variables.push(
                        textoHeader
                          .replace(/[^a-z0-9\s]/gi, '')
                          .replace(/[_\s]/g, '_')
                          .toLowerCase()
                      );
                      $scope.elemento.options.columnsWidths.push('auto');
                    }
                  }
                } // [FIN] funcion cuandoCambianColumnas
              ); // [FIN] $watch
            } // [FIN] función elementoTablaDinamicaEditorCtrl
          ] // [FIN] controller de directiva
        }; // [FIN] objeto de configuración de directiva
      } // [FIN] función elementoTablaDinamicaEditor
    ] // [FIN] Configuración de directiva elementoTablaDinamicaEditor
  ) // [FIN] funcion directive
  .directive(
    // [INICIO] funcion directive
    'elementoTablaEstaticaEditor', // Declaración de directiva elementoTablaEstaticaEditor
    [
      // [INICIO] Configuración de directiva elementoTablaEstaticaEditor
      /**
       * @description Retorna la configuración necesaria para agregar la directiva elementoTablaEstaticaEditor
       *
       * @returns {object}
       */
      function elementoTablaEstaticaEditor() {
        // [INICIO] función elementoTablaEstaticaEditor
        return {
          // [INICIO] objeto de configuración de directiva
          restrict: 'E', // Permite que la directiva funcione como elemento ('E') en la vista
          scope: {
            // [INCIO] scope de directiva
            elemento: '=elDatos', // @type {object} almacena los datos del elemento de tabla estatica
            acciones: '=elAcciones', // @type {object} almacena las acciones que se pueden realizar en elementos para editor de documentos
            enEdicion: '=enEdicion' // @type {false} determina si el elemento se encuentra dentro del modal
          }, // [FIN] scope de directiva
          templateUrl:
            'app/templates/editorPlantillaTemplates/directivas/elementoTablaEstaticaEditor.tpl.html', // ubicacion de template de directiva
          controller: [
            // [INCIO] controller de directiva
            '$rootScope',
            '$scope',
            '$timeout',
            'funcionesUtilesEditor',
            function elementoTablaEstaticaEditorCtrl(
              $rootScope,
              $scope,
              $timeout,
              funcionesUtilesEditor
            ) {
              // [INICIO] función elementoTablaEstaticaEditorCtrl
              $scope.enEdicion = !!$scope.enEdicion;
              /**
               * @description Configuración de estilo para tablas, en general, cabecera y celdas
               *
               * @params {string} tipo - tipo de celda ['general' | 'cabecera' | 'celda']
               * @params {boolean} indice - determina que celda corresponde
               * @params {boolean} filaPar - determina si la fila de la celda es par o impar
               * @params {boolean} celdaPar - determina si la celda es par o impar
               *
               * @returns {object}
               */
              $scope.estiloTabla = function(tipo, indice, filaPar, celdaPar) {
                var style = {};
                switch (tipo) {
                  case 'general':
                    style['display'] = 'table';
                    style['table-layout'] = 'fixed';
                    style['border-collapse'] = 'collapse';
                    if ($scope.elemento.options.withBorders) {
                      style = angular.extend({}, style, $scope.elemento.options.border.style);
                      if ($scope.elemento.options.typeBorders == 'horizontal') {
                        style = angular.extend(
                          {},
                          style,
                          $scope.elemento.options.border.horizontal
                        );
                      }
                    }
                    break;
                  case 'cabecera':
                    $scope.elemento.options.textStyle.header.color = funcionesUtilesEditor.getContrastColor(
                      $scope.elemento.options.colors.primary
                    );
                    style = angular.extend(
                      {},
                      style,
                      {
                        'padding-left': '0.1cm',
                        'padding-right': '0.1cm',
                        'background-color': $scope.elemento.options.colors.primary,
                        width: $scope.elemento.options.columnsWidths[indice]
                      },
                      $scope.elemento.options.textStyle.header
                    );
                    if (
                      $scope.elemento.options.withBorders &&
                      $scope.elemento.options.typeBorders != 'external'
                    ) {
                      style = angular.extend({}, style, $scope.elemento.options.border.style);
                      if ($scope.elemento.options.typeBorders == 'horizontal') {
                        style = angular.extend(
                          {},
                          style,
                          $scope.elemento.options.border.horizontal
                        );
                      }
                    }
                    break;
                  case 'celda':
                    style = angular.extend({}, style, $scope.elemento.options.textStyle.cell, {
                      'padding-left': '0.1cm',
                      'padding-right': '0.1cm',
                      width: $scope.elemento.options.columnsWidths[indice]
                    });
                    if ($scope.elemento.options.withStripes) {
                      if (filaPar && $scope.elemento.options.directionStripes == 'horizontal') {
                        style['background-color'] = $scope.elemento.options.colors.secondary;
                        $scope.elemento.options.textStyle.cell.color = funcionesUtilesEditor.getContrastColor(
                          $scope.elemento.options.colors.secondary
                        );
                        style['color'] = $scope.elemento.options.textStyle.cell.color;
                      } else if (
                        !filaPar &&
                        $scope.elemento.options.directionStripes == 'horizontal'
                      ) {
                        style['background-color'] = $scope.elemento.options.colors.tertiary;
                        $scope.elemento.options.textStyle.cell.color = funcionesUtilesEditor.getContrastColor(
                          $scope.elemento.options.colors.tertiary
                        );
                        style['color'] = $scope.elemento.options.textStyle.cell.color;
                      }
                      if (celdaPar && $scope.elemento.options.directionStripes == 'vertical') {
                        style['background-color'] = $scope.elemento.options.colors.secondary;
                        $scope.elemento.options.textStyle.cell.color = funcionesUtilesEditor.getContrastColor(
                          $scope.elemento.options.colors.secondary
                        );
                        style['color'] = $scope.elemento.options.textStyle.cell.color;
                      } else if (
                        !celdaPar &&
                        $scope.elemento.options.directionStripes == 'vertical'
                      ) {
                        style['background-color'] = $scope.elemento.options.colors.tertiary;
                        $scope.elemento.options.textStyle.cell.color = funcionesUtilesEditor.getContrastColor(
                          $scope.elemento.options.colors.tertiary
                        );
                        style['color'] = $scope.elemento.options.textStyle.cell.color;
                      }
                    } else {
                      $scope.elemento.options.textStyle.cell.color = 'rgba(0,0,0,1)';
                      style['color'] = $scope.elemento.options.textStyle.cell.color;
                    }
                    if (
                      $scope.elemento.options.withBorders &&
                      $scope.elemento.options.typeBorders != 'external'
                    ) {
                      style = angular.extend({}, style, $scope.elemento.options.border.style);
                      if ($scope.elemento.options.typeBorders == 'horizontal') {
                        style = angular.extend(
                          {},
                          style,
                          $scope.elemento.options.border.horizontal
                        );
                      }
                    }
                    break;
                }
                // en el caso que el estilo del elemento contenga 'font-size' se configura tambien la unidad
                if ('font-size' in style) {
                  style['font-size'] += 'px';
                }
                // en el caso que el estilo del elemento contenga 'line-height' se configura tambien la unidad
                if ('line-height' in style) {
                  style['line-height'] += 'px';
                }
                return style;
              };
              /**
               * @description Se configura un observador para cuando la variable columns de un elemento de tabla cambie
               */
              $scope.configurarResize = function(indice, elemento, esElUltimo) {
                // Se configura el resize solo para entorno de edicion
                var $elemento = $(
                  '#modal_edit_element ' +
                    'elemento-tabla-estatica-editor ' +
                    '#table' +
                    $scope.elemento.id +
                    ' ' +
                    elemento
                );
                if ($scope.enEdicion && !esElUltimo && $elemento.children('.resizer').length == 0) {
                  $elemento
                    .css({
                      /* required to allow resizer embedding */
                      position: 'relative'
                    })
                    /* check .resizer CSS */
                    .prepend("<div class='resizer'></div>")
                    .resizable({
                      resizeHeight: false,
                      // we use the column as handle and filter
                      // by the contained .resizer element
                      handleSelector: '',
                      onDragStart: function(e, $el, opt) {
                        // only drag resizer
                        if (!$(e.target).hasClass('resizer')) return false;
                        return true;
                      },
                      onDragEnd: function() {
                        $scope.elemento.options.columnsWidths[indice] =
                          Number($elemento.css('width').replace('px', '')) * 0.02645833 + 'cm';
                      }
                    });
                } else if (
                  $scope.enEdicion &&
                  esElUltimo &&
                  $elemento.children('.resizer').length > 0
                ) {
                  $elemento
                    .resizable('destroy')
                    .find('.resizer')
                    .remove();
                  $scope.elemento.options.columnsWidths[indice] = 'auto';
                }
              };
              /**
               * @description Se configura un observador para cuando la variable rows de un elemento de tabla cambie
               */
              $scope.$watch(
                // [INICIO] $watch
                /**
                 * @description visualiza el valor actual de variable rows
                 *
                 * @param {object} $scope - scope del elemento
                 *
                 * @returns {number}
                 */
                function observarFilas($scope) {
                  // [INICIO] funcion observarFilas
                  return $scope.elemento.options.rows;
                }, // [FIN] funcion observarFilas
                /**
                 * @description se ejecuta cuando cambia el valor de rows
                 *
                 * @params {*} valorNuevo - Obtiene el valor nuevo para el atributo rows
                 * @params {*} valorAnterior - Obtiene el valor anterior para el atributo rows
                 */
                function cuandoCambianFilas(valorNuevo, valorAnterior) {
                  // [INICIO] funcion cuandoCambianFilas
                  // Comprueba si el elemento se encuentra dentro del modal de edicion
                  if ($scope.enEdicion) {
                    // Si se encuentra dentro, termina la configuracion de la directiva
                    return;
                  }
                  // Comprueba si el valor anterior y el valor nuevo son iguales
                  if (valorNuevo === valorAnterior) {
                    // En caso de ser iguales, se termina la ejecucion de la funcion cuandoCambianFilas
                    return;
                  }
                  // Comprueba si el valor nuevo de filas es menor a 0 o undefined
                  if (valorNuevo < 0 || !valorNuevo) {
                    // Si es menor a 0 o undefined, se establece el valor en 0 y se termina la ejecucion de la función
                    $scope.elemento.options.rows = 0;
                    valorNuevo = 0;
                  }
                  // Cuando cambia el valor de filas, redimensiona los arreglos de texto
                  // Comprueba si el valor nuevo de Filas es menor al valor anterior
                  if (valorNuevo < valorAnterior) {
                    // Si es menor, elimina la cantidad de filas igual a la diferencia entre el valor anterior y el actual
                    var diferencia = valorAnterior - valorNuevo;
                    for (var j = 0; j < diferencia; j++) {
                      $scope.elemento.options.textArray.pop();
                    }
                  }
                  // Comprueba si el valor nuevo de Filas es mayor al valor anterior
                  else if (valorNuevo > valorAnterior) {
                    // Si es mayor, agrega la cantidad de filas igual a la diferencia entre el valor anterior y el actual, con celdas nuevas con un texto por defecto
                    var row; // $type {array} almacena temporalmente un arreglo de filas
                    var diferencia = valorNuevo - valorAnterior;
                    var numFilas = valorAnterior;
                    for (var j = 0; j < diferencia; j++) {
                      row = [];
                      for (var i = 0; i < $scope.elemento.options.columns; i++) {
                        row.push('Celda ' + (numFilas + j + 1) + '-' + (i + 1));
                      }
                      $scope.elemento.options.textArray.push(row);
                    }
                  }
                } // [FIN] funcion cuandoCambianFilas
              ); // [FIN] $watch
              /**
               * @description Se configura un observador para cuando la variable columns de un elemento de tabla cambie
               */
              $scope.$watch(
                // [INICIO] $watch
                /**
                 * @description visualiza el valor actual de variable columns
                 *
                 * @param {object} $scope - scope del elemento
                 *
                 * @returns {string}
                 */
                function observarColumnas($scope) {
                  // [INICIO] funcion observarColumnas
                  return $scope.elemento.options.columns;
                }, // [FIN] funcion observarColumnas
                /**
                 * @description se ejecuta cuando cambia el valor de columns
                 *
                 * @params {*} valorNuevo - Obtiene el valor nuevo para el atributo columns
                 * @params {*} valorAnterior - Obtiene el valor anterior para el atributo columns
                 */
                function cuandoCambianColumnas(valorNuevo, valorAnterior) {
                  // [INICIO] funcion cuandoCambianColumnas
                  // Comprueba si el elemento se encuentra dentro del modal de edicion
                  if ($scope.enEdicion) {
                    // Si se encuentra dentro, termina la configuracion de la directiva
                    return;
                  }
                  // Comprueba si el valor anterior y el valor nuevo son iguales
                  if (valorNuevo === valorAnterior) {
                    // En caso de ser iguales, se termina la ejecucion de la funcion cuandoCambianColumnas
                    return;
                  }
                  // Comprueba si el valor nuevo de columnas es menor a 0 o undefined
                  if (valorNuevo < 0 || !valorNuevo) {
                    // Si es menor a 0 o undefined, se establece el valor en 0 y se termina la ejecucion de la función
                    $scope.elemento.options.columns = 0;
                    valorNuevo = 0;
                  }
                  // Cuando cambia el valor de columnas, redimensiona los arreglos de texto
                  // Comprueba si el valor nuevo de Columnas es menor al valor anterior
                  if (valorNuevo < valorAnterior) {
                    // Si es menor, elimina la cantidad de Columnas igual a la diferencia entre el valor anterior y el actual
                    var diferencia = valorAnterior - valorNuevo;
                    for (var j = 0; j < $scope.elemento.options.rows; j++) {
                      for (var i = 0; i < diferencia; i++) {
                        $scope.elemento.options.textArray[j].pop();
                      }
                    }
                    for (var i = 0; i < diferencia; i++) {
                      $scope.elemento.options.textHeaders.pop();
                      $scope.elemento.options.columnsWidths.pop();
                    }
                  }
                  // Comprueba si el valor nuevo de Columnas es mayor al valor anterior
                  else if (valorNuevo > valorAnterior) {
                    // Si es mayor, agrega la cantidad de Columnas igual a la diferencia entre el valor anterior y el actual, con celdas nuevas con un texto por defecto
                    var diferencia = valorNuevo - valorAnterior;
                    var numColumnas = valorAnterior;
                    for (var j = 0; j < $scope.elemento.options.rows; j++) {
                      for (var i = 0; i < diferencia; i++) {
                        $scope.elemento.options.textArray[j].push(
                          'Celda ' + (j + 1) + '-' + (numColumnas + i + 1)
                        );
                      }
                    }
                    for (var i = 0; i < diferencia; i++) {
                      $scope.elemento.options.textHeaders.push('Columna ' + (numColumnas + i + 1));
                      $scope.elemento.options.columnsWidths.push('auto');
                    }
                  }
                } // [FIN] funcion cuandoCambianColumnas
              ); // [FIN] $watch
            } // [FIN] función elementoTablaEstaticaEditorCtrl
          ] // [FIN] controller de directiva
        }; // [FIN] objeto de configuración de directiva
      } // [FIN] función elementoTablaEstaticaEditor
    ] // [FIN] Configuración de directiva elementoTablaEstaticaEditor
  ) // [FIN] funcion directive
  .directive('editorUploadImage', ['$http', 'FebosUtil', 'SesionFebos', function($http, FebosUtil, SesionFebos) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
          elemento: '=data'
        },
        template: '<div id="file_upload-drop" class="uk-file-upload">'+
            '<img ng-src="{{ uploadme.src }}">'+
            '<p class="uk-text">Arrastra un archivo para subir (jpg|peg|png)</p>'+
            '<p class="uk-text-muted uk-text-small uk-margin-small-bottom">o</p>'+
            '<span class="uk-form-file md-btn">Escoge un archivo<input id="file_upload-select" type="file"></span>'+
          '</div>',
        link: function(scope, element, attrs) {
          var typeAllowed = '[image/png, image/jpeg, image/gif]';
          var maxsize = 3; // 3MB

          checkSize = function(size) {
            var _ref;
            if (((_ref = maxsize) === (void 0) || _ref === '') || (size / 1024) / 1024 < maxsize) {
              return true;
            } else {
              alert("El archivo debe ser menor a " + maxsize + " MB");
              return false;
            }
          };

          isTypeValid = function(type) {
            if ((typeAllowed === (void 0) || typeAllowed === '') || typeAllowed.indexOf(type) > -1) {
              return true;
            } else {
              alert("Tipos de archivo Inválido.  Los archivos deben corresponder a los siguientes tipos " + typeAllowed);
              return false;
            }
          };
          scope.upload = function(event, files) {
            if(!files){
              files = event.target.files;
            }
            file = files[0];
            name = file.name;
            type = file.type;
            size = file.size;
            if(!isTypeValid(type)){
              return;
            }
            if(!checkSize(size)){
              return;
            }
            $http({
              method: 'GET',
              url:
                'https://api.febos.cl/' +
                FebosUtil.obtenerAmbiente() +
                '/herramientas/archivos?tipo=upload&key=febos-io/publicar/archivos.febos.io/chile/' +
                SesionFebos().empresa.iut +
                '/editor/img/'+name+'&contenType='+type,
              headers: {
                token: SesionFebos().usuario.token,
                empresa: SesionFebos().empresa.iut,
                'Content-Type': type
              }
            }) // metodo get para obtener plantilla desde URL
              .then(
                function(response) {
                  $.ajax({
                    type: 'PUT',
                    url: response.data.url,
                    processData: false,
                    data: file,
                    headers: {
                      Vary: 'Access-Control-Request-Headers',
                      Vary: 'Access-Control-Request-Method',
                      Vary: 'Origin'
                    }
                  }) // metodo get para obtener plantilla desde URL
                    .success(function(response) {
                      scope.elemento.options.image = 'https://archivos.febos.io/chile/' +
                        SesionFebos().empresa.iut + '/editor/img/'+name;
                    })
                    .error(
                      function cargaFallida(error) {
                        // [INICIO] función cargaFallida
                        console.log(error);
                      } // [FIN] función cargaFallida
                    );
                },
                function cargaFallida(error) {
                  // [INICIO] función cargaFallida
                  console.log(error);
                } // [FIN] función cargaFallida
              );
          };
          element[0]
            .querySelector('#file_upload-select')
            element.on('change', scope.upload);
          element.on('dragover', function(e) {
            e.preventDefault();
            e.stopPropagation();
          });
          element.on('dragenter', function(e) {
            e.preventDefault();
            e.stopPropagation();
          });
          element.on('drop', function(e) {
            e.preventDefault();
            e.stopPropagation();
            if (e.originalEvent.dataTransfer){
              if (e.originalEvent.dataTransfer.files.length > 0) {
                scope.upload(e, e.originalEvent.dataTransfer.files);
              }
            }
            return false;
          });
        }
    };
  }])
  .factory(
    // [INICIO] funcion factory
    'funcionesUtilesEditor', // Declaración de factory funcionesUtilesEditor para almacenar funciones útiles
    [
      // [INICIO] Configuración de factory funcionesUtilesEditor
      /**
       * @description Retorna una instancia de factory funcionesUtilesEditor con funciones utiles
       *
       * @returns {object}
       */
      function funcionesUtilesEditor() {
        // [INICIO] función funcionesUtilesEditor
        var self = this; // @type {object} almacena instancia y contexto de funcionesUtilesEditor
        // Se crea objeto funcionesUtiles en esta instancia
        self.funcionesUtiles = {
          // [INICIO] objeto funcionesUtiles
          /**
           * @description Generador de color de contraste
           *
           * @param {string} rgb - almacena color en formato rgb/rgba
           *
           * @returns {string}
           */
          getContrastColor: function getContrastColor(rgb) {
            // [INICIO] función getContrastColor
            // Obtener codigos de color segun expresion regular
            var colores = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/);
            // separacion de codigos de color en variables
            var R = colores[1];
            var G = colores[2];
            var B = colores[3];
            var A = colores[4] || 1;

            var brillo = 0.299 * R + 0.587 * G + 0.114 * B;
            if (brillo > 70) {
              return 'rgba(0,0,0,1)';
            } else {
              return 'rgba(255,255,255,1)';
            }
          }, // [FIN] función getContrastColor
          /**
           * @description Generador de id random
           *
           * @param {string} prefijo - almacena un prefijo para utilizar al inicio del id
           *
           * @returns {string}
           */
          uid: function uid(prefijo) {
            // [INICIO] función uid
            // Se construye el id en base a prefijo + timestamp actual + 'RAND' + un numero random
            return (
              (prefijo || 'id') + new Date().getTime() + 'RAND' + Math.ceil(Math.random() * 100000)
            );
          }, // [FIN] función uid
          /**
           * @description se utiliza la funcion findwhere descrita en https://gist.github.com/aaronmccall/9751450 para realizar busquedas de elementos.
           *
           * @param {array} list - lista de elementos
           * @param {object} props - objeto utilizado para buscar
           *
           * @returns {*}
           */
          findWhere: function(list, props) {
            // [INICIO] funcion findWhere
            var idx = 0;
            var len = list.length;
            var match = false;
            var item, item_k, item_v, prop_k, prop_val;
            for (; idx < len; idx++) {
              item = list[idx];
              for (prop_k in props) {
                // If props doesn't own the property, skip it.
                if (!props.hasOwnProperty(prop_k)) continue;
                // If item doesn't have the property, no match;
                if (!item.hasOwnProperty(prop_k)) {
                  match = false;
                  break;
                }
                if (props[prop_k] === item[prop_k]) {
                  // We have a match…so far.
                  match = true;
                } else {
                  // No match.
                  match = false;
                  // Don't compare more properties.
                  break;
                }
              }
              // We've iterated all of props' properties, and we still match!
              // Return that item!
              if (match) return item;
            }
            // No matches
            return null;
          }, // [FIN] funcion findWhere
          /**
           * @description construye codigo style html en base a objeto JSON configurados
           *
           * @param {object} styleObj - estilo css inline
           *
           * @returns {string}
           */
          buildStyleCss: function(styleObj) {
            // [INICIO] funcion buildStyleCss
            var style = '';
            Object.keys(styleObj).map(function(key) {
              style += key + ':' + styleObj[key] + ';';
            });
            return style;
          }, // [FIN] funcion buildStyleCss
          /**
           * @description construye codigo html en base a objeto JSON configurados
           *
           * @param {string} seccion - corresponde a la seccion que se configura ['header', 'body', 'footer']
           * @param {array} listaElementos - lista de elementos
           * @param {object} plantilla - objeto de plantilla
           *
           * @returns {string}
           */
          buildHtml: function(seccion, listaElementos, plantilla) {
            // [INICIO] funcion buildHtml
            var initHTML = self.funcionesUtiles.initHTML(seccion, plantilla);
            var endHTML = self.funcionesUtiles.endHTML(seccion);
            var buildStyleCss = self.funcionesUtiles.buildStyleCss;
            var subModulosDinamicos = '';
            var estiloTabla = function(opciones, tipo, indice, filaPar, celdaPar) {
              var style = {};
              switch (tipo) {
                case 'general':
                  style['display'] = 'table';
                  style['table-layout'] = 'fixed';
                  style['border-collapse'] = 'collapse';
                  if (opciones.withBorders) {
                    style = angular.extend({}, style, opciones.border.style);
                    if (opciones.typeBorders == 'horizontal') {
                      style = angular.extend({}, style, opciones.border.horizontal);
                    }
                  }
                  break;
                case 'cabecera':
                  opciones.textStyle.header.color = self.funcionesUtiles.getContrastColor(
                    opciones.colors.primary
                  );
                  style = angular.extend(
                    {},
                    style,
                    {
                      'padding-left': '0.1cm',
                      'padding-right': '0.1cm',
                      'background-color': opciones.colors.primary,
                      width: opciones.columnsWidths[indice]
                    },
                    opciones.textStyle.header
                  );
                  if (opciones.withBorders && opciones.typeBorders != 'external') {
                    style = angular.extend({}, style, opciones.border.style);
                    if (opciones.typeBorders == 'horizontal') {
                      style = angular.extend({}, style, opciones.border.horizontal);
                    }
                  }
                  break;
                case 'celda':
                  style = angular.extend({}, style, opciones.textStyle.cell, {
                    'padding-left': '0.1cm',
                    'padding-right': '0.1cm',
                    width: opciones.columnsWidths[indice]
                  });
                  if (opciones.withStripes) {
                    if (filaPar && opciones.directionStripes == 'horizontal') {
                      style['background-color'] = opciones.colors.secondary;
                      opciones.textStyle.cell.color = self.funcionesUtiles.getContrastColor(
                        opciones.colors.secondary
                      );
                      style['color'] = opciones.textStyle.cell.color;
                    } else if (!filaPar && opciones.directionStripes == 'horizontal') {
                      style['background-color'] = opciones.colors.tertiary;
                      opciones.textStyle.cell.color = self.funcionesUtiles.getContrastColor(
                        opciones.colors.tertiary
                      );
                      style['color'] = opciones.textStyle.cell.color;
                    }
                    if (celdaPar && opciones.directionStripes == 'vertical') {
                      style['background-color'] = opciones.colors.secondary;
                      opciones.textStyle.cell.color = self.funcionesUtiles.getContrastColor(
                        opciones.colors.secondary
                      );
                      style['color'] = opciones.textStyle.cell.color;
                    } else if (!celdaPar && opciones.directionStripes == 'vertical') {
                      style['background-color'] = opciones.colors.tertiary;
                      opciones.textStyle.cell.color = self.funcionesUtiles.getContrastColor(
                        opciones.colors.tertiary
                      );
                      style['color'] = opciones.textStyle.cell.color;
                    }
                  } else {
                    opciones.textStyle.cell.color = 'rgba(0,0,0,1)';
                    style['color'] = opciones.textStyle.cell.color;
                  }
                  if (opciones.withBorders && opciones.typeBorders != 'external') {
                    style = angular.extend({}, style, opciones.border.style);
                    if (opciones.typeBorders == 'horizontal') {
                      style = angular.extend({}, style, opciones.border.horizontal);
                    }
                  }
                  break;
              }
              // en el caso que el estilo del elemento contenga 'font-size' se configura tambien la unidad
              if ('font-size' in style) {
                style['font-size'] += 'px';
              }
              // en el caso que el estilo del elemento contenga 'line-height' se configura tambien la unidad
              if ('line-height' in style) {
                style['line-height'] += 'px';
              }
              // en el caso que el estilo del elemento contenga 'border-width' se configura tambien la unidad
              if ('border-width' in style) {
                style['border-width'] += 'px';
              }
              return style;
            };
            var configuracionDeEstilo = function(style) {
              // [INICIO] función configuracionDeEstilo
              // Se genera una copia temporal del estilo del elemento
              var temp = angular.copy(style);
              // Se le agregan las unidades a los atributos que se almacenan como numero
              // en el caso que el estilo del elemento contenga 'padding-top' se configura tambien la unidad
              if ('padding-top' in temp) {
                temp['padding-top'] += 'cm';
              }
              // en el caso que el estilo del elemento contenga 'padding-right' se configura tambien la unidad
              if ('padding-right' in temp) {
                temp['padding-right'] += 'cm';
              }
              // en el caso que el estilo del elemento contenga 'padding-bottom' se configura tambien la unidad
              if ('padding-bottom' in temp) {
                temp['padding-bottom'] += 'cm';
              }
              // en el caso que el estilo del elemento contenga 'padding-left' se configura tambien la unidad
              if ('padding-left' in temp) {
                temp['padding-left'] += 'cm';
              }
              // en el caso que el estilo del elemento contenga 'border-width' se configura tambien la unidad
              if ('border-width' in temp) {
                temp['border-width'] += 'px';
              }
              // en el caso que el estilo del elemento contenga 'font-size' se configura tambien la unidad
              if ('font-size' in temp) {
                temp['font-size'] += 'px';
              }
              // en el caso que el estilo del elemento contenga 'line-height' se configura tambien la unidad
              if ('line-height' in temp) {
                temp['line-height'] += 'px';
              }
              // en el caso que el estilo del elemento contenga 'top' se configura tambien la unidad
              if ('top' in temp) {
                temp['top'] += 'px';
              }
              // en el caso que el estilo del elemento contenga 'bottom' se configura tambien la unidad
              if ('bottom' in temp) {
                temp['bottom'] += 'px';
              }
              // en el caso que el estilo del elemento contenga 'left' se configura tambien la unidad
              if ('left' in temp) {
                temp['left'] += 'px';
              }
              // en el caso que el estilo del elemento contenga 'right' se configura tambien la unidad
              if ('right' in temp) {
                temp['right'] += 'px';
              }
              // Se genera objeto de estilo en base a configuracion por defecto y campos editados
              return angular.extend(
                {},
                // se le asignan los campos de objeto temporal
                temp,
                {
                  // [INICIO] estilo adicional elemento
                  'box-sizing': 'border-box' // se define box-sizing del elemento como 'border-box'
                } // [FIN] estilo adicional elemento
              );
            }; // [FIN] función configuracionDeEstilo
            var configurarSubModuloTablaDinamicaHTML = function(id, elemento) {
              subModulosDinamicos +=
                '<script id="' + id + '" type="text/x-handlebars-template" class="submodulo">';
              subModulosDinamicos += '<tr>';
              for (var i = 0; i < elemento.options.columns; i++) {
                subModulosDinamicos += '<td class="td-' + i + '">';
                subModulosDinamicos += elemento.options.variables[i];
                subModulosDinamicos += '</td>';
              }
              subModulosDinamicos += '</tr></script>';
            };
            var configurarElementoHTML = function(elemento) {
              var codeElementoHTML;
              if (elemento.data_type == 'float') {
                codeElementoHTML =
                  '<div style="position: absolute;top:0;left:0;' +
                  buildStyleCss(configuracionDeEstilo(elemento.options.position)) +
                  '">';
              } else {
                codeElementoHTML =
                  '<div style="position: relative;' + buildStyleCss(elemento.options.size) + '">';
              }
              var styleElement = '';
              switch (elemento.type) {
                case 'structure_element':
                  var objContainer = {
                    margin: 'auto',
                    position: 'relative',
                    'box-shadow': 'none',
                    display: 'flex',
                    flex: 1,
                    'flex-flow': 'column nowrap',
                    'justify-content': 'space-around',
                    'align-content': 'space-around'
                  };
                  if (elemento.options.title != '') {
                    codeElementoHTML +=
                      '<div ' +
                      'style="' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.styleTitle)) +
                      buildStyleCss({
                        width: '100%',
                        'font-family': elemento.options.style['font-family']
                      }) +
                      '">' +
                      elemento.options.title +
                      '</div>';
                  }
                  if (elemento.options.direction == 'horizontal') {
                    objContainer['flex-direction'] = 'row';
                  }
                  styleElement =
                    buildStyleCss(objContainer) +
                    buildStyleCss(configuracionDeEstilo(elemento.options.style)) +
                    (elemento.options.borders_actives.top ? '' : 'border-top:0px;') +
                    (elemento.options.borders_actives.bottom ? '' : 'border-bottom:0px;') +
                    (elemento.options.borders_actives.left ? '' : 'border-left:0px;') +
                    (elemento.options.borders_actives.right ? '' : 'border-right:0px;');
                  codeElementoHTML += '<div style="' + styleElement + '">';
                  for (var i = 0; i < elemento.options.elements.length; i++) {
                    codeElementoHTML += configurarElementoHTML(elemento.options.elements[i]);
                  }
                  codeElementoHTML += '</div>';
                  break;
                case 'divider':
                  codeElementoHTML +=
                    '<table class="main" width="100%" style="display: table; border: 0;" cellspacing="0" cellpadding="0" border="0" align="center" data-type="divider">' +
                    '<tbody><tr><td class="divider-simple" style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.style)) +
                    '">' +
                    '<table width="100%" cellspacing="0" cellpadding="0" border="0" ' +
                    'style="border-top:' +
                    elemento.options.border.size +
                    'px ' +
                    elemento.options.border.style +
                    ' ' +
                    elemento.options.border.color +
                    ';">' +
                    '<tbody><tr><td width="100%"></td></tr></tbody></table></td></tr></tbody></table>';
                  break;
                case 'image':
                case 'image_float':
                  codeElementoHTML +=
                    '<table ' +
                    (elemento.data_type == 'float' ? 'width="100%" ' : '') +
                    'class="main" cellspacing="0" cellpadding="0" border="0" align="center" style="display: table;" data-type="image"><tbody><tr>' +
                    '<td align="' +
                    elemento.options.align +
                    '" style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.style)) +
                    '" class="image">' +
                    '<img border="0" align="one_image" style="display:block;max-width:100%;width:' +
                    elemento.options.width +
                    'cm" src="' +
                    elemento.options.image +
                    '" tabindex="0">' +
                    '</td></tr></tbody></table>';
                  break;
                case 'title_static':
                case 'title_dynamic':
                case 'subtitle_static':
                case 'subtitle_dynamic':
                case 'texto_simple_static':
                case 'texto_simple_dynamic':
                case 'text_float_simple':
                case 'text_float_dynamic':
                  var texto = '';
                  if (elemento.type == 'title_static') {
                    texto =
                      '<h1 style="' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.styleText)) +
                      '">' +
                      elemento.options.text +
                      '</h1>';
                  } else if (elemento.type == 'subtitle_static') {
                    texto =
                      '<h2 style="' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.styleText)) +
                      '">' +
                      elemento.options.text +
                      '</h2>';
                  } else if (elemento.type == 'texto_simple_static') {
                    texto =
                      '<h4 style="' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.styleText)) +
                      '">' +
                      elemento.options.text +
                      '</h4>';
                  } else if (elemento.type == 'title_dynamic') {
                    texto =
                      '<h1 style="' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.styleText)) +
                      '">' +
                      elemento.options.variable +
                      '</h1>';
                  } else if (elemento.type == 'subtitle_dynamic') {
                    texto =
                      '<h2 style="' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.styleText)) +
                      '">' +
                      elemento.options.variable +
                      '</h2>';
                  } else if (elemento.type == 'texto_simple_dynamic') {
                    texto =
                      '<h4 style="' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.styleText)) +
                      '">' +
                      elemento.options.variable +
                      '</h4>';
                  } else if (elemento.type == 'text_float_simple') {
                    texto =
                      '<h4 style="' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.styleText)) +
                      '">' +
                      elemento.options.text +
                      '</h4>';
                  } else if (elemento.type == 'text_float_dynamic') {
                    texto =
                      '<h4 style="' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.styleText)) +
                      '">' +
                      elemento.options.variable +
                      '</h4>';
                  }
                  codeElementoHTML +=
                    '<table class="main" ' +
                    (elemento.data_type == 'float' ? 'width="100%" ' : '') +
                    'cellspacing="0" cellpadding="0" border="0" style="display: table;table-layout: fixed; " align="center" data-type="title">' +
                    '<tbody><tr>' +
                    '<td class="title" align="left" style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.style)) +
                    '">' +
                    texto +
                    '</td>' +
                    '</tr></tbody></table>';
                  break;
                case 'paragraph':
                  codeElementoHTML +=
                    '<table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" style="display: table;" align="center" data-type="text-block">' +
                    '<tbody><tr>' +
                    '<td class="block-text" data-block-id="background" data-clonable="true" align="left">' +
                    elemento.options.content +
                    '</td>' +
                    '</tr></tbody></table>';
                  break;
                case 'table_static':
                  codeElementoHTML +=
                    '<div style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.style)) +
                    '">';
                  if (elemento.options.title != '') {
                    codeElementoHTML +=
                      '<div ' +
                      'style="width=100%;' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.textStyle.title)) +
                      '">' +
                      elemento.options.title +
                      '</div>';
                  }
                  codeElementoHTML +=
                    '  <table class="main" width="100%" cellspacing="0" cellpadding="0" border="0"' +
                    ' style="' +
                    buildStyleCss(estiloTabla(elemento.options, 'general')) +
                    '"' +
                    ' align="center">';
                  if (elemento.options.showHeaders) {
                    codeElementoHTML += '<thead><tr>';
                    for (var i = 0; i < elemento.options.columns; i++) {
                      codeElementoHTML +=
                        '<th style="' +
                        buildStyleCss(estiloTabla(elemento.options, 'cabecera', i)) +
                        '">';
                      codeElementoHTML += '<div>' + elemento.options.textHeaders[i] + '</div>';
                      codeElementoHTML += '</th>';
                    }
                    codeElementoHTML += '</tr></thead>';
                  }
                  codeElementoHTML += '<tbody>';
                  for (var i = 0; i < elemento.options.rows; i++) {
                    codeElementoHTML += '<tr>';
                    for (var j = 0; j < elemento.options.columns; j++) {
                      codeElementoHTML +=
                        '<td style="' +
                        buildStyleCss(
                          estiloTabla(elemento.options, 'celda', j, i % 2 == 0, j % 2 == 0)
                        ) +
                        '">';
                      codeElementoHTML += '<div>' + elemento.options.textArray[i][j] + '</div>';
                      codeElementoHTML += '</td>';
                    }
                    codeElementoHTML += '</tr>';
                  }
                  codeElementoHTML += '</tbody></table></div>';
                  break;
                case 'table_dynamic':
                case 'referenced_documents_element':
                case 'detail_element':
                  codeElementoHTML +=
                    '<div style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.style)) +
                    '">';
                  codeElementoHTML += '<style>';
                  for (var i = 0; i < elemento.options.columns; i++) {
                    if (elemento.options.directionStripes == 'horizontal') {
                      codeElementoHTML +=
                        'table#table-' + elemento.id + ' tr:nth-child(odd) td.td-' + i + '{';
                      codeElementoHTML += buildStyleCss(
                        estiloTabla(elemento.options, 'celda', i, true)
                      );
                      codeElementoHTML += '}';

                      codeElementoHTML +=
                        'table#table-' + elemento.id + ' tr:nth-child(even) td.td-' + i + '{';
                      codeElementoHTML += buildStyleCss(
                        estiloTabla(elemento.options, 'celda', i, false)
                      );
                      codeElementoHTML += '}';
                    } else if (elemento.options.directionStripes == 'vertical') {
                      codeElementoHTML +=
                        'table#table-' + elemento.id + ' tr td.td-' + i + ':nth-child(odd){';
                      codeElementoHTML += buildStyleCss(
                        estiloTabla(elemento.options, 'celda', i, true, true)
                      );
                      codeElementoHTML += '}';

                      codeElementoHTML +=
                        'table#table-' + elemento.id + ' tr td.td-' + i + ':nth-child(even){';
                      codeElementoHTML += buildStyleCss(
                        estiloTabla(elemento.options, 'celda', i, true, false)
                      );
                      codeElementoHTML += '}';
                    }
                  }
                  codeElementoHTML += '</style>';
                  if (elemento.options.title != '') {
                    codeElementoHTML +=
                      '<div ' +
                      'style="width=100%;' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.textStyle.title)) +
                      '">' +
                      elemento.options.title +
                      '</div>';
                  }
                  codeElementoHTML +=
                    '<table id="table-' +
                    elemento.id +
                    '" class="main" width="100%" cellspacing="0" cellpadding="0" border="0"' +
                    ' style="' +
                    buildStyleCss(estiloTabla(elemento.options, 'general')) +
                    '"' +
                    ' align="center">';
                  if (elemento.options.showHeaders) {
                    codeElementoHTML += '<thead><tr>';
                    for (var i = 0; i < elemento.options.columns; i++) {
                      codeElementoHTML +=
                        '<th style="' +
                        buildStyleCss(estiloTabla(elemento.options, 'cabecera', i)) +
                        '">';
                      codeElementoHTML += '<div>' + elemento.options.textHeaders[i] + '</div>';
                      codeElementoHTML += '</th>';
                    }
                    codeElementoHTML += '</tr></thead>';
                  }
                  codeElementoHTML += '<tbody>';
                  codeElementoHTML +=
                    '{{#if (esLista ' +
                    elemento.options.table_variable +
                    ') }}' +
                    '{{#each ' +
                    elemento.options.table_variable +
                    ' }}' +
                    '{{> lineaDetalle' +
                    elemento.id +
                    ' linea=@index}}' +
                    '{{/each}}' +
                    '{{else}}' +
                    '{{> lineaDetalle' +
                    elemento.id +
                    ' ' +
                    elemento.options.table_variable +
                    '}}' +
                    '{{/if}}';
                  codeElementoHTML += '</tbody></table></div>';
                  configurarSubModuloTablaDinamicaHTML('lineaDetalle' + elemento.id, elemento);
                  break;
                case 'folio_element':
                  codeElementoHTML +=
                    '<table width="100%" class="main" cellspacing="0" cellpadding="0" border="0" align="center" style="display: table;"><tbody><tr>' +
                    '<td style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.border)) +
                    '">' +
                    '<div style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.style)) +
                    '">' +
                    '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="display: table;table-layout: fixed;" align="center"><tbody>';
                  if (elemento.options.firstLine) {
                    codeElementoHTML +=
                      '<tr>' +
                      '<td align="center" style="' +
                      buildStyleCss({ 'padding-bottom': elemento.options.margin1 + 'cm' }) +
                      '">' +
                      '<h4 style="display: inline-block;' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.styleText)) +
                      '">' +
                      elemento.options.data.first.label +
                      '</h4>' +
                      '<h4 style="display: inline-block;' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.styleText)) +
                      '">' +
                      elemento.options.data.first.variable +
                      '</h4>' +
                      '</td></tr>';
                  }
                  if (elemento.options.secondLine) {
                    codeElementoHTML +=
                      '<tr>' +
                      '<td align="center" style="' +
                      buildStyleCss({ 'padding-bottom': elemento.options.margin2 + 'cm' }) +
                      '">' +
                      '<h4 style="display: inline-block;' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.styleText)) +
                      '">' +
                      elemento.options.data.second.label +
                      '</h4>' +
                      '<h4 style="display: inline-block;' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.styleText)) +
                      '">' +
                      elemento.options.data.second.variable +
                      '</h4>' +
                      '</td></tr>';
                  }
                  if (elemento.options.thirdLine) {
                    codeElementoHTML +=
                      '<tr><td align="center">' +
                      '<h4 style="display: inline-block;' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.styleText)) +
                      '">' +
                      elemento.options.data.third.label +
                      '</h4>' +
                      '<h4 style="display: inline-block;' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.styleText)) +
                      '">' +
                      elemento.options.data.third.variable +
                      '</h4>' +
                      '</td></tr>';
                  }
                  codeElementoHTML += '</tbody></table></div></td></tr></tbody></table>';
                  break;
                case 'sender_information_element':
                  codeElementoHTML +=
                    '<table width="100%" class="main" cellspacing="0" cellpadding="0" border="0" align="center" style="display: table;"><tbody><tr>' +
                    '<td style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.border)) +
                    '">' +
                    '<div style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.style)) +
                    '">' +
                    '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="display: table;table-layout: fixed;" align="center"><tbody><tr>';
                  if (elemento.options.withImage) {
                    codeElementoHTML +=
                      '<td style="' +
                      buildStyleCss({
                        width: elemento.options.widthImage + 'cm',
                        'padding-right': elemento.options.paddingImage + 'cm'
                      }) +
                      '" class="image">' +
                      '<img border="0" style="' +
                      buildStyleCss({
                        width: elemento.options.widthImage + 'cm',
                        display: 'block',
                        'max-width': '100%'
                      }) +
                      '" src="' +
                      elemento.options.image +
                      '" tabindex="0">' +
                      '</td>';
                  }
                  codeElementoHTML +=
                    '<td><div style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.nameSenderStyle)) +
                    '">' +
                    elemento.options.nameSenderCode +
                    '</div>';
                  for (var i = 0; i < elemento.options.code.length; i++) {
                    codeElementoHTML +=
                      '<div style="' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.othersTextsStyle)) +
                      '">' +
                      elemento.options.code[i] +
                      '</div>';
                  }
                  codeElementoHTML += '</td></tr></tbody></table></div></td></tr></tbody></table>';
                  break;
                case 'reception_SII_FEBOS_element':
                  codeElementoHTML +=
                    '<table width="100%" class="main" cellspacing="0" cellpadding="0" border="0" align="center" style="display: table;"><tbody><tr>' +
                    '<td style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.border)) +
                    '">' +
                    '<div style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.style)) +
                    '">' +
                    '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="display: table;table-layout: fixed;" align="center"><tbody>' +
                    '<tr><td style="text-align: center!important;' +
                    buildStyleCss({
                      color: elemento.options.colors.sii,
                      'padding-bottom': elemento.options.separacion + 'cm'
                    }) +
                    '">' +
                    '<div style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.textStyle)) +
                    '">' +
                    elemento.options.data.sii.label +
                    '</div>' +
                    '<div style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.datesStyle)) +
                    '">' +
                    elemento.options.data.sii.variable +
                    '</div>' +
                    '</td><td style="text-align: center!important;' +
                    buildStyleCss({
                      color: elemento.options.colors.febos,
                      'padding-bottom': elemento.options.separacion + 'cm'
                    }) +
                    '">' +
                    '<div style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.textStyle)) +
                    '">' +
                    elemento.options.data.febos.label +
                    '</div>' +
                    '<div style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.datesStyle)) +
                    '">' +
                    elemento.options.data.febos.variable +
                    '</div>' +
                    '</td></tr>' +
                    '<tr><td style="text-align: center!important;' +
                    buildStyleCss({ color: elemento.options.colors.info }) +
                    '">' +
                    '<div style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.infoStyle)) +
                    '">' +
                    elemento.options.infoText +
                    '</div>' +
                    '</td><td></td></tr>' +
                    '</tbody></table></div></td></tr></tbody></table>';
                  break;
                case 'receiver_information_element':
                  var item;
                  codeElementoHTML +=
                    '<table width="100%" class="main" cellspacing="0" cellpadding="0" border="0" align="center" style="display: table;"><tbody><tr>' +
                    '<td style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.border)) +
                    '">' +
                    '<div style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.style)) +
                    '">' +
                    '<table id="table_receiver_information" width="100%" cellspacing="0" cellpadding="0" border="0" style="display: table;table-layout: auto;border-collapse: collapse;" align="center"><thead><tr>' +
                    '<th class="label_receiver_information"></th><th class="text_receiver_information"></th><th class="label_receiver_information"></th><th class="text_receiver_information"></th>' +
                    '</tr></thead><tbody>';
                  for (var i = 0; i < elemento.options.data.length; i++) {
                    item = elemento.options.data[i];
                    codeElementoHTML +=
                      '<tr>' +
                      '<td style="' +
                      buildStyleCss({
                        width: '1px',
                        'white-space': 'nowrap',
                        'vertical-align': 'top'
                      }) +
                      '">' +
                      '<p style="' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.labelStyle)) +
                      '">' +
                      item.label +
                      '</p>' +
                      '</td>' +
                      '<td colspan="' +
                      (item.combinar ? 3 : 1) +
                      '" style="' +
                      buildStyleCss({ width: item.textWidth, 'vertical-align': 'top' }) +
                      '">' +
                      '<p style="' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.textStyle)) +
                      '">' +
                      item.code +
                      '</p>' +
                      '</td>';
                    if (!item.combinar) {
                      codeElementoHTML +=
                        '<td style="' +
                        buildStyleCss({
                          width: '1px',
                          'white-space': 'nowrap',
                          'vertical-align': 'top',
                          'padding-left': elemento.options.separationInfo + 'cm'
                        }) +
                        '">' +
                        '<p style="' +
                        buildStyleCss(configuracionDeEstilo(elemento.options.labelStyle)) +
                        '">' +
                        item.otherCell.label +
                        '</p>' +
                        '</td><td style="' +
                        buildStyleCss({
                          width: item.otherCell.textWidth,
                          'vertical-align': 'top'
                        }) +
                        '">' +
                        '<p style="' +
                        buildStyleCss(configuracionDeEstilo(elemento.options.textStyle)) +
                        '">' +
                        item.otherCell.code +
                        '</p>' +
                        '</td>';
                    }
                    codeElementoHTML += '</tr>';
                  }
                  codeElementoHTML += '</tbody></table></div></td></tr></tbody></table>';
                  break;
                case 'aditionals_documents_element':
                case 'transport_data_element':
                  var item;
                  codeElementoHTML +=
                    '<table width="100%" class="main" cellspacing="0" cellpadding="0" border="0" align="center" style="display: table;"><tbody>' +
                    '<tr><td style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.titleStyle)) +
                    '">' +
                    elemento.options.title +
                    '</td></tr>' +
                    '<tr><td style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.border)) +
                    '">' +
                    '<div style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.style)) +
                    '">' +
                    '<table id="table_aditionals_documents" width="100%" cellspacing="0" cellpadding="0" border="0" style="display: table;table-layout: auto;border-collapse: collapse;" align="center"><thead><tr>' +
                    '<th class="label_aditionals_documents1"></th><th class="text_aditionals_documents1"></th><th class="label_aditionals_documents2"></th><th class="text_aditionals_documents2"></th><th class="label_aditionals_documents3"></th><th class="text_aditionals_documents3"></th>' +
                    '</tr></thead><tbody>';
                  for (var i = 0; i < elemento.options.data.length; i++) {
                    item = elemento.options.data[i];
                    codeElementoHTML +=
                      '<tr>' +
                      '<td style="' +
                      buildStyleCss({
                        width: '1px',
                        'white-space': 'nowrap',
                        'vertical-align': 'top'
                      }) +
                      '">' +
                      '<p style="' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.labelStyle)) +
                      '">' +
                      item.label +
                      '</p>' +
                      '</td>' +
                      '<td colspan="' +
                      (item.combinar ? item.combinar : 1) +
                      '" style="' +
                      buildStyleCss({ width: item.textWidth, 'vertical-align': 'top' }) +
                      '">' +
                      '<p style="' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.textStyle)) +
                      '">' +
                      item.code +
                      '</p>' +
                      '</td>';
                    if (!item.combinar) {
                      codeElementoHTML +=
                        '<td style="' +
                        buildStyleCss({
                          width: '1px',
                          'white-space': 'nowrap',
                          'vertical-align': 'top',
                          'padding-left': elemento.options.separationInfo + 'cm'
                        }) +
                        '">' +
                        '<p style="' +
                        buildStyleCss(configuracionDeEstilo(elemento.options.labelStyle)) +
                        '">' +
                        item.cell2.label +
                        '</p>' +
                        '</td>' +
                        '<td colspan="' +
                        (item.cell2.combinar ? 3 : 1) +
                        '" style="' +
                        buildStyleCss({ width: item.cell2.textWidth, 'vertical-align': 'top' }) +
                        '">' +
                        '<p style="' +
                        buildStyleCss(configuracionDeEstilo(elemento.options.textStyle)) +
                        '">' +
                        item.cell2.code +
                        '</p>' +
                        '</td>';
                    }
                    if (!(item.combinar == 5 || item.cell2.combinar)) {
                      codeElementoHTML +=
                        '<td style="' +
                        buildStyleCss({
                          width: '1px',
                          'white-space': 'nowrap',
                          'vertical-align': 'top',
                          'padding-left': elemento.options.separationInfo + 'cm'
                        }) +
                        '">' +
                        '<p style="' +
                        buildStyleCss(configuracionDeEstilo(elemento.options.labelStyle)) +
                        '">' +
                        item.cell3.label +
                        '</p>' +
                        '</td>' +
                        '<td style="' +
                        buildStyleCss({ width: item.cell3.textWidth, 'vertical-align': 'top' }) +
                        '">' +
                        '<p style="' +
                        buildStyleCss(configuracionDeEstilo(elemento.options.textStyle)) +
                        '">' +
                        item.cell3.code +
                        '</p>' +
                        '</td>';
                    }
                    codeElementoHTML += '</tr>';
                  }
                  codeElementoHTML += '</tbody></table></div></td></tr></tbody></table>';
                  break;
                case 'discounts_totals_element':
                  codeElementoHTML +=
                    '<div style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.style)) +
                    '">';
                  if (elemento.options.title != '') {
                    codeElementoHTML +=
                      '<div style="width: 100%;' +
                      buildStyleCss(configuracionDeEstilo(elemento.options.textStyle.title)) +
                      '">' +
                      elemento.options.title +
                      '</div>';
                  }
                  codeElementoHTML +=
                    '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="display: table;table-layout: fixed; border-collapse: collapse;" align="center"><tbody>';
                  for (var i = 0; i < elemento.options.numRows; i++) {
                    codeElementoHTML +=
                      '<tr>' +
                      '<td style="border-color: #000;border-width: 1px;border-style: solid">' +
                      '<table width="100%" style="display: table;table-layout: fixed;border-collapse: collapse;"><thead><tr>';
                    for (var j = 0; j < elemento.options.numCells; j++) {
                      codeElementoHTML +=
                        '<th style="' +
                        buildStyleCss({
                          'border-left': '0px',
                          'border-top': '0px',
                          'border-bottom': '1px',
                          'border-right': j + 1 == elemento.options.numCells ? '0px' : '1px',
                          'border-color': '#000',
                          'border-style': 'solid'
                        }) +
                        buildStyleCss(configuracionDeEstilo(elemento.options.textStyle.header)) +
                        '">' +
                        '<div>' +
                        elemento.options.cells[i][j].header +
                        '</div>' +
                        '</th>';
                    }
                    codeElementoHTML += '</tr></thead><tbody><tr>';
                    for (var j = 0; j < elemento.options.numCells; j++) {
                      codeElementoHTML +=
                        '<td style="' +
                        buildStyleCss({
                          'border-left': '0px',
                          'border-top': '0px',
                          'border-bottom': '0px',
                          'border-right': j + 1 == elemento.options.numCells ? '0px' : '1px',
                          'border-color': '#000',
                          'border-style': 'solid'
                        }) +
                        buildStyleCss(configuracionDeEstilo(elemento.options.textStyle.cell)) +
                        '">' +
                        '<div>' +
                        elemento.options.cells[i][j].variable +
                        '</div>' +
                        '</td>';
                    }
                    codeElementoHTML += '</tr></tbody></table></td></tr>';
                  }
                  codeElementoHTML += '</tbody></table></div>';
                  break;
                case 'electronic_code_element':
                  codeElementoHTML +=
                    '<table width="100%" class="main" cellspacing="0" cellpadding="0" border="0" align="center" style="display: table;" data-type="image">' +
                    '<tbody>' +
                    '<tr>' +
                    '<td align="center" style="' +
                    buildStyleCss(configuracionDeEstilo(elemento.options.style)) +
                    '">' +
                    '<div id="qr" class="qr"></div>' +
                    '</td>' +
                    '</tr>' +
                    '</tbody>' +
                    '</table>';
                  break;
                default:
                  codeElementoHTML += '<div>Elemento No Existente</div>';
                  break;
              }
              codeElementoHTML += '</div>';
              return codeElementoHTML;
            };
            var configurarSeccionHTML = function() {
              var initSeccion =
                '<script id="' +
                seccion +
                '" type="text/x-handlebars-template" class="modulo" data-contenedor="container-' +
                seccion +
                '">';
              switch (seccion) {
                case 'header':
                  initSeccion +=
                    '<div ' +
                    'style="' +
                    buildStyleCss({
                      width: '100%',
                      height: plantilla.heights.header,
                      'box-sizing': 'border-box',
                      'padding-top': plantilla.style['padding-top'] + 'cm',
                      'padding-left': plantilla.style['padding-left'] + 'cm',
                      'padding-right': plantilla.style['padding-right'] + 'cm'
                    }) +
                    '">';
                  initSeccion +=
                    '<div ' +
                    'style="' +
                    buildStyleCss({
                      width: '100%',
                      'border-top': plantilla.border['border-width'] + 'px',
                      'border-bottom': '0px',
                      'border-left': plantilla.border['border-width'] + 'px',
                      'border-right': plantilla.border['border-width'] + 'px',
                      'border-style': plantilla.border['border-style'],
                      'border-color': plantilla.border['border-color'],
                      'border-top-left-radius': plantilla.border['border-radius'] + 'px',
                      'border-top-right-radius': plantilla.border['border-radius'] + 'px'
                    }) +
                    '">';
                  break;
                case 'body':
                  initSeccion +=
                    '<div ' +
                    'style="' +
                    buildStyleCss({
                      width: '100%',
                      height: plantilla.heights.body,
                      'box-sizing': 'border-box',
                      'padding-left': plantilla.style['padding-left'] + 'cm',
                      'padding-right': plantilla.style['padding-right'] + 'cm'
                    }) +
                    '">';
                  initSeccion +=
                    '<div ' +
                    'style="' +
                    buildStyleCss({
                      width: '100%',
                      'border-top': '0px',
                      'border-bottom': '0px',
                      'border-left': plantilla.border['border-width'] + 'px',
                      'border-right': plantilla.border['border-width'] + 'px',
                      'border-style': plantilla.border['border-style'],
                      'border-color': plantilla.border['border-color']
                    }) +
                    '">';
                  break;
                case 'footer':
                  initSeccion +=
                    '<div ' +
                    'style="' +
                    buildStyleCss({
                      width: '100%',
                      height: plantilla.heights.footer,
                      'box-sizing': 'border-box',
                      'padding-left': plantilla.style['padding-left'] + 'cm',
                      'padding-right': plantilla.style['padding-right'] + 'cm',
                      'padding-bottom': plantilla.style['padding-bottom'] + 'cm'
                    }) +
                    '">';
                  initSeccion +=
                    '<div ' +
                    'style="' +
                    buildStyleCss({
                      width: '100%',
                      'border-top': '0px',
                      'border-bottom': plantilla.border['border-width'] + 'px',
                      'border-left': plantilla.border['border-width'] + 'px',
                      'border-right': plantilla.border['border-width'] + 'px',
                      'border-style': plantilla.border['border-style'],
                      'border-color': plantilla.border['border-color'],
                      'border-bottom-left-radius': plantilla.border['border-radius'] + 'px',
                      'border-bottom-right-radius': plantilla.border['border-radius'] + 'px'
                    }) +
                    '">';
              }
              var endSeccion = '</div></div></script>';
              var codeHTML = '';
              var floatHTML = '';
              for (var i = 0; i < listaElementos.length; i++) {
                codeHTML += configurarElementoHTML(listaElementos[i]);
              }
              if (seccion == 'header') {
                floatHTML +=
                  '<script id="back" type="text/x-handlebars-template" class="modulo" data-contenedor="container-back">';
                for (var i = 0; i < plantilla.source.backElements.length; i++) {
                  floatHTML += configurarElementoHTML(plantilla.source.backElements[i]);
                }
                floatHTML += '</script>';
              }
              return initSeccion + codeHTML + endSeccion + floatHTML;
            };
            return initHTML + configurarSeccionHTML(seccion) + subModulosDinamicos + endHTML;
          }, // [FIN] funcion buildHtml
          /**
           * @description retorna parte inicial del archivo HTML
           *
           * @param {string} seccion - corresponde a la seccion que se configura ['header', 'body', 'footer']
           * @param {object} plantilla - objeto de plantilla
           *
           * @returns {string}
           */
          initHTML: function(seccion, plantilla) {
            // [INICIO] funcion initHTML
            var marcaFebos = '';
            var buildStyleCss = self.funcionesUtiles.buildStyleCss;
            if (seccion == 'header') {
              marcaFebos =
                '<div id="marca-febos" ' +
                'style="' +
                'bottom: calc(' +
                plantilla.style.height +
                'cm - ' +
                plantilla.style['padding-bottom'] +
                'cm);' +
                'left: calc(-' +
                plantilla.style.width / 2 +
                'cm + ' +
                plantilla.style['padding-left'] +
                'cm);' +
                '">' +
                'Este documento ha sido generado electr&oacute;nicamente por Febos &reg;. http://www.febos.co' +
                '</div>';
            }
            return (
              '<!DOCTYPE html>' +
              '<html>' +
              '<head>' +
              '<script>' +
              'function getParameterByName(name, url) {' +
              'if (!url){url = window.location.href;}' +
              'name = name.replace(/[[]]/g, "\\$&");' +
              'var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),results = regex.exec(url);' +
              'if (!results){return null;}' +
              'if (!results[2]){return "";}' +
              'return decodeURIComponent(results[2].replace(/+/g, " "));' +
              '}' +
              '</script>' +
              '<script src="assets/jquery-3.1.1.slim.min.js"></script>' +
              '<script src="assets/handlebars.min.js"></script>' +
              '<script src="assets/helpers.js"></script>' +
              '<script src="assets/factura.js"></script>' +
              '<script src="assets/qrcode.js"></script>' +
              '<script src="assets/xml2json.js"></script>' +
              '<script src="assets/moment.min.js"></script>' +
              '<link rel="stylesheet" href="assets/febos.css"/>' +
              '<script>' +
              'var dte_b64 = "";' +
              '</script>' +
              '</head>' +
              '<body style="margin:0;">' +
              '<!-- pagina -->' +
              marcaFebos +
              '<div id="contenido" ' +
              'style="margin: 0;top: 0;position: absolute;' +
              buildStyleCss({
                color: plantilla.style.color,
                width: plantilla.style.width + 'cm',
                'font-family': plantilla.style['font-family']
              }) +
              buildStyleCss(self.funcionesUtiles.doc_builderSettings.defaultOptions) +
              '">' +
              '<div id="container-back" style="box-sizing: border-box;' +
              buildStyleCss({
                width: plantilla.style['width'] + 'cm',
                height: plantilla.style['height'] + 'cm',
                'padding-top': plantilla.style['padding-top'] + 'px',
                'padding-bottom': plantilla.style['padding-bottom'] + 'px',
                'padding-left': plantilla.style['padding-left'] + 'px',
                'padding-right': plantilla.style['padding-right'] + 'px',
                'background-color': plantilla.style['background-color'],
                'font-family': plantilla.style['font-family'],
                color: plantilla.style['color'],
                position: 'absolute',
                'z-index': 0,
                top: 0,
                left: 0,
                right: 0,
                bottom: 0
              }) +
              '"></div>' +
              '<div style="clear:both"></div>' +
              '<div id="container-header" style="width: 100%;box-sizing: border-box;z-index:1;"></div>' +
              '<div style="clear:both"></div>' +
              '<div id="container-body" style="width: 100%;box-sizing: border-box;z-index:1;"></div>' +
              '<div style="clear:both"></div>' +
              '<div id="container-footer" style="width: 100%;box-sizing: border-box;z-index:1;"></div>' +
              '<div style="clear:both"></div>' +
              '</div>'
            );
          }, // [FIN] funcion initHTML
          /**
           * @description retorna parte final del archivo HTML
           *
           * @param {string} seccion - corresponde a la seccion que se configura ['header', 'body', 'footer']
           *
           * @returns {string}
           */
          endHTML: function(seccion) {
            // [INICIO] funcion endHTML
            var qrCode =
              'numfac = data.dte.Invoice.ID;' +
              'fecfac = (data.dte.Invoice.IssueDate + "" + data.dte.Invoice.IssueTime).replaceAll("-","").replaceAll(":","");' +
              'nitfac = data.dte.Invoice.AccountingSupplierParty.Party.PartyIdentification.ID["#text"];' +
              'docadq = data.dte.Invoice.AccountingCustomerParty.Party.PartyIdentification.ID["#text"] ;' +
              'valfac = data.dte.Invoice.LegalMonetaryTotal.PayableAmount["#text"] ;' +
              'valiva = data.dte.Invoice.LegalMonetaryTotal.TaxExclusiveAmount["#text"] ;' +
              'valotroim = "";' +
              'valfacim = "";' +
              'cufe = data.dte.Invoice.UUID["#text"] ;' +
              'textoRQ = "NumFac: " + numfac + "\\n"' +
              '+ "FecFac: " + fecfac + "\\n"' +
              '+ "NitFac: " + nitfac + "\\n"' +
              '+ "DocAdq: " + docadq + "\\n"' +
              '+ "ValFac: " + valfac + "\\n"' +
              '+ "ValIva: " + valiva + "\\n"' +
              '+ "ValOtroIm: " + valotroim + "\\n"' +
              '+ "ValFacIm: " + valfacim + "\\n"' +
              '+ "CUFE: " + cufe;' +
              'var qrcode = new QRCode("qr", {' +
              'text: textoRQ,' +
              'width: 110,' +
              'height: 110,' +
              'colorDark: "#000000",' +
              'colorLight: "#ffffff",' +
              'correctLevel: QRCode.CorrectLevel.H' +
              '});';
            return (
              '<script src="assets/febos.js"></script>' +
              '<script>' +
              '$(".submodulo").each(function (i, o) {' +
              'var id = $(o).prop("id");' +
              'Handlebars.registerPartial(id, $(o).html());' +
              '});' +
              '$(".modulo").each(function (i, o) {' +
              'var id = $(o).prop("id");' +
              'var contenedor = $(o).data().contenedor;' +
              'var anexar = $(o).data().anexar;' +
              'render(id, contenedor, anexar == "true");' +
              '});' +
              qrCode +
              '</script>' +
              '</body>' +
              '</html>'
            );
          }, // [FIN] funcion endHTML
          doc_builderSettings: {
            // [INICIO] objeto doc_builderSettings
            msgEmptyData: 'Arrastra y suelta elementos aquí', // Mensaje por defecto de datos vacíos
            msgEmptyDataHeader: 'Arrastra y suelta elementos aquí en el HEADER', // Mensaje por defecto de datos vacíos de header
            msgEmptyDataBody: 'Arrastra y suelta elementos aquí en el BODY', // Mensaje por defecto de datos vacíos de body
            msgEmptyDataFooter: 'Arrastra y suelta elementos aquí en el FOOTER', // Mensaje por defecto de datos vacíos de footer
            defaultOptions: {
              // [INICIO] configuracion de estilo por defecto de plantilla
              'box-sizing': 'border-box' // se define box-sizing del elemento como 'border-box'
            } // [FIN] configuracion de estilo por defecto de plantilla
          }, // [FIN] objeto doc_builderSettings
          page_sizes: [
            // [INICIO] objeto page_sizes
            { size: 'Carta', name: 'Carta (22cm - 28cm)', width: 22, height: 28 }, // Configuracion de tamaño carta
            { size: 'Oficio', name: 'Oficio (22cm - 34cm)', width: 22, height: 34 }, // Configuracion de tamaño oficio
            { size: 'Custom', name: 'Customizado', width: 12.7, height: 20.3 } // Configuracion de tamaño custom
          ], // [FIN] objeto page_sizes
          defaultOptionsColor: {
            // [INICIO] objeto defaultOptionsColor
            // atributos html
            required: true, // determina si es requerido el color
            placeholder: '', // Texto de ejemplo en el input
            inputClass: 'md-input', // clase css
            // validacion
            restrictToFormat: true, // especifca si el valor se restrinje al formato
            preserveInputFormat: true, // especifica si mantiene el formato del input
            allowEmpty: false, // determina que no permite vacío
            // color
            format: 'hexString', // formato a utilizar de color
            // sliders
            alpha: false, // desactiva el slider de alpha en el color
            // popup
            horizontal: true, // direccion del selector de colores
            close: {
              // [INICIO] configuracion de boton close
              show: true, // determina si se muestra el boton
              label: 'Ok', // texto del boton
              class: 'md-btn md-btn-primary md-btn-small' // clases css para configurar estilo del boton
            } // [FIN] configuracion de boton close
          }, // [FIN] objeto defaultOptionsColor
          cssConfigurationsOptions: {
            // [INICIO] objeto cssConfigurationsOptions
            doc_direction: [
              // [INICIO] objeto doc_direction
              { id: 'vertical', name: 'Vertical' },
              { id: 'horizontal', name: 'Horizontal' }
            ], // [FIN] objeto doc_direction
            border_style_types: [
              // [INICIO] objeto border_style_types
              { id: 'dashed', name: 'Rayas' },
              { id: 'dotted', name: 'Puntos' },
              { id: 'solid', name: 'Linea continua' }
            ], // [FIN] objeto border_style_types
            text_align_types: [
              // [INICIO] objeto text_align_types
              { id: 'left', name: 'left' },
              { id: 'right', name: 'right' },
              { id: 'center', name: 'center' },
              { id: 'justify', name: 'justify' },
              { id: 'inherit', name: 'inherit' }
            ], // [FIN] objeto text_align_types
            text_style_types: [
              // [INICIO] objeto text_style_types
              { id: 'normal', name: 'normal' },
              { id: 'italic', name: 'italic' },
              { id: 'oblique', name: 'oblique' },
              { id: 'inherit', name: 'inherit' }
            ], // [FIN] objeto text_style_types
            font_weight_types: [
              // [INICIO] objeto font_weight_types
              { id: 'bold', name: 'bold' },
              { id: 'normal', name: 'normal' },
              { id: 'bolder', name: 'bolder' },
              { id: 'lighter', name: 'lighter' },
              { id: '100', name: '100' },
              { id: '200', name: '200' },
              { id: '300', name: '300' },
              { id: '400', name: '400' },
              { id: '500', name: '500' },
              { id: '600', name: '600' },
              { id: '700', name: '700' },
              { id: '800', name: '800' },
              { id: '900', name: '900' },
              { id: 'inherit', name: 'inherit' }
            ], // [FIN] objeto font_weight_types
            fonts: [
              // [INICIO] objeto fonts
              'inherit',
              'Georgia, serif',
              "'Palatino Linotype', 'Book Antiqua', Palatino, serif",
              "'Times New Roman', Times, serif",
              'Arial, Helvetica, sans-serif',
              "'Arial Black', Gadget, sans-serif",
              "'Comic Sans MS', cursive, sans-serif",
              'Impact, Charcoal, sans-serif',
              "'Lucida Sans Unicode', 'Lucida Grande', sans-serif",
              'Tahoma, Geneva, sans-serif',
              "'Trebuchet MS', Helvetica, sans-serif",
              'Verdana, Geneva, sans-serif',
              "'Courier New', Courier, monospace",
              "'Lucida Console', Monaco, monospace"
            ] // [FIN] objeto fonts
          } // [FIN] objeto cssConfigurationsOptions
        }; // [FIN] objeto funcionesUtiles
        return self.funcionesUtiles;
      } // [FIN] función funcionesUtilesEditor
    ] // [FIN] Configuración de factory funcionesUtilesEditor
  ) // [FIN] funcion factory
  .controller(
    // [INICIO] funcion controller
    'editorPlantillaCtrl', // Definición de nombre de Controlador
    [
      // [INICIO] configuración de controlador editorPlantillaCtrl
      '$q',
      '$sce',
      '$http',
      '$scope',
      '$state',
      '$timeout',
      '$rootScope',
      '$stateParams',
      'dragulaService',
      'bloquesDeElementos',
      'funcionesUtilesEditor',    
      'FebosUtil',
      'SesionFebos',
      function editorPlantillaCtrl(
        $q,
        $sce,
        $http,
        $scope,
        $state,
        $timeout,
        $rootScope,
        $stateParams,
        dragulaService,
        bloquesDeElementos,
        funcionesUtilesEditor,
        FebosUtil,
        SesionFebos
      ) {
        // [INICIO] función de controlador editorPlantillaCtrl
        // En caso de no existir información en $stateParams.data oculta toda la pagina
        $scope.ocultarTodo = true;
        // Comprueba si dentro de $stateParams viene seteado el atributo data con información
        if (!$stateParams.data) {
          // [INICIO] if
          // Y luego redirecciona a state 'restringido.listarPlantillas'
          $state.go(
            // [INICIO] $state.go
            'restringido.listarPlantillas', // nombre de state al cual se redirigira
            {
              // [INICIO] configuracion de $stateParams
              app: $stateParams.app // Se envía $stateParam.app en el nuevo $stateParam
            } // [FIN] configuracion de $stateParams
          ); // [FIN] $state.go
          return;
        } // [FIN] if
        /**
         * [INICIO] Configuración de formulario de plantilla
         */
         $scope.ocultarTodo = false;
        $scope.plantilla = $stateParams.data; // Asignación de datos de plantilla a editar
        $scope.accionDeEdicion = false; // @type {boolean} determina si la acción a realizar es de edicion (true) o creación (false)
        $scope.abrirCerrarPanel = false; // @type {array} determina si se abre (true) o cierra (false) panel lateral
        $scope.bloquesDeElementos = bloquesDeElementos; // $type {object} Se almacena en scope objeto cargado de bloquesDeElementos
        $scope.fonts = funcionesUtilesEditor.cssConfigurationsOptions.fonts; // @type {array} almacena las opciones de fuente permitidas
        $scope.border_style_types =
          funcionesUtilesEditor.cssConfigurationsOptions.border_style_types; // @type {array} almacena arreglo de tipos de bordes de elementos
        $scope.configuracionParaValoresSelectize = {
          // [INICIO] configuracion de selectize
          maxItems: 1, // @type {integer} determina la maxima capacidad permitida de elementos seleccionados
          valueField: 'id', // @type {string} determina nombre de atributo a utilizar para el valor del elemento seleccionado
          labelField: 'name', // @type {string} determina nombre de atributo a utilizar para mostrar en elemento selectize
          create: false, // @type {boolean} evita que el usuario pueda crear nuevos items que no se encuentran en la lista original
          placeholder: 'Seleccionar...' // @type {string} texto que aparece por defecto en el elemento
        }; // [FIN] configuracion de selectize
        /**
         * @description redirecciona a editor de plantilla para iniciar la creación de una plantilla desde cero
         */
        $scope.volverAListaDePlantillas = function() {
          // [INICIO] función volverAListaDePlantillas
          // Se cambia $state actual por vista de listarPlantillas
          $state.go(
            'restringido.listarPlantillas', // Nombre de $state para vista listarPlantillas
            {
              // [INCIO] objeto $stateParams de vista listarPlantillas
              app: $stateParams.app // se envía valor de app almacenado en $stateParams
            } // [FIN] objeto $stateParams de vista listarPlantillas
          );
        }; // [FIN] función volverAListaDePlantillas

        /**
         * @description Crea un modal para mostrar mensaje de carga
         *
         * @param  {string} mensaje - Mensaje que se mostrará en modal
         *
         * @returns {object} retorna objeto modal de UIkit de tipo blockUI
         */
        $scope.crearModalDeCarga = function(mensaje) {
          // [INICIO] función crearModalDeCarga
          var templateHTMLModal =
            "<div class='uk-text-center'>" +
            mensaje + // se inserta mensaje en HTML
            '<br/>' +
            "<img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>" +
            '</div>'; // @type {string} Almacena template HTML de modal para loader de clonación de plantilla
          return UIkit.modal.blockUI(templateHTMLModal); // retorna un objeto modal de UIkit de tipo blockUI basado en templateHTMLModal
        }; // [FIN] función crearModalDeCarga
        /**
         * @description Crea un modal para mostrar mensaje de exito/fracaso
         *
         * @param  {string} mensaje - Mensaje que se mostrará en modal
         *
         * @returns {object} retorna objeto modal de UIkit de tipo blockUI
         */
        $scope.crearModalDeExitoFracaso = function(exito, mensaje) {
          // [INICIO] función crearModalDeExitoFracaso
          var templateHTMLModal =
            "<div class='uk-text-center'>" +
            ((exito)?'<i class="material-icons uk-text-success md-48">check_circle</i>':'<i class="material-icons uk-text-danger md-48">cancel</i>') +
            '<br/>' +
            mensaje + // se inserta mensaje en HTML
            '</div>'; // @type {string} Almacena template HTML de modal para loader de clonación de plantilla
          var modal = UIkit.modal.blockUI(templateHTMLModal);
          $timeout(function(){
            modal.hide();
          }, 5000);
          return modal // retorna un objeto modal de UIkit de tipo blockUI basado en templateHTMLModal
        }; // [FIN] función crearModalDeExitoFracaso
        $scope.enviarAlServidor = function(nombre, tipo, contenido) {
          var deferred = $q.defer();
          $http({
            method: 'GET',
            url:
              'https://api.febos.cl/' +
              FebosUtil.obtenerAmbiente() +
              '/herramientas/archivos?tipo=upload&key=febos-io/chile/' +
              FebosUtil.obtenerAmbiente() +
              '/editor/empresas/' +
              SesionFebos().empresa.iut +
              '/'+nombre+'&contenType='+tipo,
            headers: {
              token: SesionFebos().usuario.token,
              empresa: SesionFebos().empresa.iut,
              'Content-Type': tipo
            }
          }).then(function(response){
            var blob = new Blob([contenido], {
              type: tipo
            });
            $.ajax({
              type: 'PUT',
              url: response.data.url,
              processData: false,
              data: blob,
              headers: {
                Vary: 'Access-Control-Request-Headers',
                Vary: 'Access-Control-Request-Method',
                Vary: 'Origin'
              }
            }) // metodo get para obtener plantilla desde URL
              .success(deferred.resolve)
              .error(deferred.reject);
          }, deferred.reject);
          return deferred.promise;
        }

        $scope.generarHTML = function(retornoPromesa) {
          // [INICIO] función generarHTML
          var modal;
          if(!retornoPromesa){
            modal = $scope.crearModalDeCarga('Generando HTML'); // Crea un objeto modal con mensaje 'Cargando Plantilla'
          }
          var listaPromesas = [];
          var archivoHTMLHeader = funcionesUtilesEditor.buildHtml(
            'header',
            $scope.plantilla.source.headerElements,
            $scope.plantilla
          );
          var archivoHTMLBody = funcionesUtilesEditor.buildHtml(
            'body',
            $scope.plantilla.source.bodyElements,
            $scope.plantilla
          );
          var archivoHTMLFooter = funcionesUtilesEditor.buildHtml(
            'footer',
            $scope.plantilla.source.footerElements,
            $scope.plantilla
          );
          listaPromesas
            .push($scope.enviarAlServidor(
              'editor-dte-header-' +
              $scope.plantilla.id +
              '.html',
              'text/html',
              archivoHTMLHeader));
          listaPromesas
            .push($scope.enviarAlServidor(
              'editor-dte-body-' +
              $scope.plantilla.id +
              '.html',
              'text/html',
              archivoHTMLBody));
          listaPromesas
            .push($scope.enviarAlServidor(
              'editor-dte-footer-' +
              $scope.plantilla.id +
              '.html',
              'text/html',
              archivoHTMLFooter));
          if(!retornoPromesa){
            $q.all(listaPromesas)
            .then(function () {
              modal.hide();
              $scope.crearModalDeExitoFracaso(true, 'La Generación de HTML fue Exitosa.');
            }, function error(){
              modal.hide();
              $scope.crearModalDeExitoFracaso(false, 'La Generación de HTML fue errónea, inténtelo nuevamente.');
            });
            return;
          }
          return $q.all(listaPromesas);
        }; // [FIN] función generarHTML

        $scope.guardarPlantilla = function(retornoPromesa) {
          // [INICIO] función guardarPlantilla
          if(!retornoPromesa){
            var modal = $scope.crearModalDeCarga('Guardando Configuración de Plantilla'); // Crea un objeto modal con mensaje 'Cargando Plantilla'
            $scope.enviarAlServidor(
              'editor-dte-config-' +
              $scope.plantilla.id +
              '.json',
              'application/json',
              JSON.stringify($scope.plantilla))
            .then(function () {
              modal.hide();
              $scope.crearModalDeExitoFracaso(true, 'La Plantilla se ha guardado correctamente.');
            }, function error(){
              modal.hide();
              $scope.crearModalDeExitoFracaso(false, 'No se ha podido guardar la plantilla, inténtelo nuevamente.');
            });
            return;
          }
          return $scope.enviarAlServidor(
            'editor-dte-config-' +
            $scope.plantilla.id +
            '.json',
            'application/json',
            JSON.stringify($scope.plantilla));
        }; // [FIN] función guardarPlantilla

        $scope.guardarYGenerarPlantilla = function(retornoPromesa) {
          // [INICIO] función guardarYGenerarPlantilla
          var modal = $scope.crearModalDeCarga('Guardando Configuración de Plantilla y Generando HTML'); // Crea un objeto modal con mensaje 'Cargando Plantilla'
          $q.all([$scope.guardarPlantilla(true), $scope.generarHTML(true)])
          .then(function () {
            modal.hide();
            $scope.crearModalDeExitoFracaso(true, 'La Plantilla se ha guardado y generado correctamente.');
          }, function error(){
            modal.hide();
            $scope.crearModalDeExitoFracaso(false, 'No se ha podido guardar y generar la plantilla, inténtelo nuevamente.');
          });
          return;
        }; // [FIN] función guardarYGenerarPlantilla
        /**
         * [INICIO] Configuracion de Constructor de plantillas
         */
        /**
         * Todos los elementos drag and drop
         * @type {*[]}
         */
        $scope.elements = Object.keys(bloquesDeElementos)
          .map(function(key) {
            // [INICIO] map
            // retorna cada uno de los elementos
            return bloquesDeElementos[key].element;
          }) // [FIN] map
          .filter(function(elem) {
            // [INICIO] filter
            // filtra los elementos por si tienen tipo seteado y si se muestran en la app
            return elem.type && bloquesDeElementos[elem.type].display;
          }) // [FIN] filter
          .sort(function(elem, elem2) {
            // [INICIO] sort
            // ordena los elementos sengun la configuración establecida
            return bloquesDeElementos[elem.type].sort - bloquesDeElementos[elem2.type].sort;
          }); // [FIN] sort
        $scope.radio_layers = 'elements_to_front'; // @type {string} se utiliza para determinar cual capa es la visible en el editor
        $scope.configuracionParaValoresTamanoPlantillaSelectize = {
          // [INICIO] configuracion de selectize
          maxItems: 1, // @type {integer} determina la maxima capacidad permitida de elementos seleccionados
          valueField: 'size', // @type {string} determina nombre de atributo a utilizar para el valor del elemento seleccionado
          labelField: 'name', // @type {string} determina nombre de atributo a utilizar para mostrar en elemento selectize
          create: false, // @type {boolean} evita que el usuario pueda crear nuevos items que no se encuentran en la lista original
          placeholder: 'Seleccionar...' // @type {string} texto que aparece por defecto en el elemento
        }; // [FIN] configuracion de selectize
        $scope.configuracionParaValoresFontFamilySelectize = {
          // [INICIO] configuracion de selectize para atributo font-family
          maxItems: 1, // @type {integer} determina la maxima capacidad permitida de elementos seleccionados
          create: false, // @type {boolean} evita que el usuario pueda crear nuevos items que no se encuentran en la lista original
          placeholder: 'Seleccionar...', // @type {string} texto que aparece por defecto en el elemento
          render: {
            // [INICIO] configuracion de renderizado de items
            option: function renderizadoPorItem(item, escape) {
              // [INICIO] funcion renderizadoPorItem
              // Se entrega template HTML para renderizar items en la lista de selectize
              return (
                '<div class="option" style="font-family:' +
                escape(item.value) +
                '" data-selectable="" data-value="' +
                escape(item.value) +
                '">' +
                escape(item.text) +
                '</div>'
              );
            } // [FIN] funcion renderizadoPorItem
          } // [FIN] configuracion de renderizado de items
        }; // [FIN] configuracion de selectize para atributo font-family
        $scope.tamanosDepagina = funcionesUtilesEditor.page_sizes; // se almacena en scope los tamaños de pagina
        $scope.configuracionContrutorDeDocumentos = funcionesUtilesEditor.doc_builderSettings; // Se almacena en scope configuraciones de constructor
        /**
         * @description busca el valor de ancho o alto segun tipo de pagina.
         *
         * @param {string} tipoTamano - contiene el tipo de tamaño de pagina.
         * @param {object} atributo - almacena width/height
         *
         * @returns {null|object}
         */
        $scope.obtenerValorAnchoAlto = function(tipoTamano, atributo) {
          // [INICIO] función obtenerValorAnchoAlto
          // Recorre los tipos de pagina en el sistema
          for (var i = 0; i < $scope.tamanosDepagina.length; i++) {
            // [INICIO] for
            // Comprueba si existe el tipo de tamaño de pagina ingresado
            if (tipoTamano === $scope.tamanosDepagina[i].size) {
              // si existe, retorna el ancho/alto de pagina
              return $scope.tamanosDepagina[i][atributo];
            }
          } // [FIN] for
          return;
        }; // [FIN] función obtenerValorAnchoAlto
        /**
         * @description busca el valor de ancho o alto segun tipo de pagina.
         */
        $scope.cuandoSeSeleccionaTamanoPagina = function() {
          // [INICIO] función cuandoSeSeleccionaTamanoPagina
          // Se actualizan valores de alto y ancho en la plantilla segun tamaño de pagina seleccionado
          $scope.plantilla.style.width = $scope.obtenerValorAnchoAlto(
            $scope.plantilla.size,
            'width'
          );
          $scope.plantilla.style.height = $scope.obtenerValorAnchoAlto(
            $scope.plantilla.size,
            'height'
          );
          // Se comprueba si el tipo de tamaño de pagina es 'custom'
          if ($scope.plantilla.size !== 'Custom') {
            // Si es 'custom' se habilita la opcion de customizacion de tamaño
            $scope.disableSetCustomSize = true;
          } else {
            // Si no es 'custom' se deshabilita la opcion de customizacion de tamaño
            $scope.disableSetCustomSize = false;
          }
        }; // [FIN] función cuandoSeSeleccionaTamanoPagina
        /**
         * @description Retorna la configuración General de aspecto de la plantilla
         *
         * @returns {object}
         */
        $scope.configuracionGeneral = function() {
          // [INICIO] función configuracionGeneral
          // Se genera objeto de estilo en base a configuracion por defecto y campos editados
          return angular.extend(
            {},
            // Se obtienen el estilo por defecto para configuracion base
            $scope.configuracionContrutorDeDocumentos.defaultOptions,
            {
              // [INICIO] estilo de plantilla
              color: $scope.plantilla.style.color, // Color de texto definido para la plantilla
              width: $scope.plantilla.style.width + 'cm', // se define el ancho de la plantilla
              height: $scope.plantilla.style.height + 'cm', // se define el alto de la plantilla
              'font-family': $scope.plantilla.style['font-family'] // fuente de texto definida para la plantilla
            } // [FIN] estilo de plantilla
          );
        }; // [FIN] función configuracionGeneral
        /**
         * @description Retorna la configuración de padding general de la plantilla
         *
         * @param {string} seccion - contiene el tipo de seccion a configurar
         *
         * @returns {object}
         */
        $scope.configuracionGeneralSeccion = function(seccion) {
          // [INICIO] función configuracionGeneralSeccion
          switch (seccion) {
            case 'header':
              return {
                // [INICIO] estilo de seccion header
                'z-index': 3,
                top: 0,
                position: 'absolute', // posicion absoluta
                width: '100%', // ancho total de la plantilla
                'box-sizing': 'border-box',
                'padding-top': $scope.plantilla.style['padding-top'] + 'cm', // Padding superior definido para la plantilla
                'padding-left': $scope.plantilla.style['padding-left'] + 'cm', // Padding izquierdo definido para la plantilla
                'padding-right': $scope.plantilla.style['padding-right'] + 'cm' // Padding derecho definido para la plantilla
              }; // [FIN] estilo de seccion header
            case 'body':
              var hHeader = $('#section-container-header.section-container').outerHeight();
              var hFooter = $('#section-container-footer.section-container').outerHeight();
              $scope.plantilla.heights.header = Number(hHeader) * 0.02645833 + 'cm';
              $scope.plantilla.heights.footer = Number(hFooter) * 0.02645833 + 'cm';
              return {
                // [INICIO] estilo de seccion body
                'z-index': 2,
                position: 'absolute', // posicion absoluta
                top: hHeader, // ubicacion unica en top
                height: 'auto',
                'max-height':
                  'calc(' +
                  $scope.plantilla.style.height +
                  'cm - ' +
                  hHeader +
                  'px - ' +
                  hFooter +
                  'px)',
                width: '100%', // ancho total de la plantilla
                'box-sizing': 'border-box',
                bottom: hFooter, // ubicacion unica en bottom
                'padding-left': $scope.plantilla.style['padding-left'] + 'cm', // Padding izquierdo definido para la plantilla
                'padding-right': $scope.plantilla.style['padding-right'] + 'cm' // Padding derecho definido para la plantilla
              }; // [FIN] estilo de seccion body
            case 'footer':
              return {
                // [INICIO] estilo de seccion footer
                'z-index': 1,
                bottom: 0, // ubicacion unica en bottom
                position: 'absolute', // posicion absoluta
                width: '100%', // ancho total de la plantilla
                'box-sizing': 'border-box',
                'padding-left': $scope.plantilla.style['padding-left'] + 'cm', // Padding izquierdo definido para la plantilla
                'padding-right': $scope.plantilla.style['padding-right'] + 'cm', // Padding derecho definido para la plantilla
                'padding-bottom': $scope.plantilla.style['padding-bottom'] + 'cm' // Padding inferior definido para la plantilla
              }; // [FIN] estilo de seccion footer
          }
        }; // [FIN] función configuracionGeneralSeccion
        /**
         * @description Retorna la configuración de borde general de la plantilla
         *
         * @param {string} seccion - contiene el tipo de seccion a configurar
         *
         * @returns {object}
         */
        $scope.configuracionGeneralBordeSeccion = function(seccion) {
          // [INICIO] función configuracionGeneralBordeSeccion
          switch (seccion) {
            case 'header':
              return {
                // [INICIO] estilo de seccion header
                'border-top': $scope.plantilla.border['border-width'],
                'border-bottom': '0px',
                'border-left': $scope.plantilla.border['border-width'],
                'border-right': $scope.plantilla.border['border-width'],
                'border-style': $scope.plantilla.border['border-style'],
                'border-color': $scope.plantilla.border['border-color'],
                'border-top-left-radius': $scope.plantilla.border['border-radius'],
                'border-top-right-radius': $scope.plantilla.border['border-radius']
              }; // [FIN] estilo de seccion header
            case 'body':
              return {
                // [INICIO] estilo de seccion body
                'border-top': '0px',
                'border-bottom': '0px',
                'border-left': $scope.plantilla.border['border-width'],
                'border-right': $scope.plantilla.border['border-width'],
                'border-style': $scope.plantilla.border['border-style'],
                'border-color': $scope.plantilla.border['border-color']
              }; // [FIN] estilo de seccion body
            case 'footer':
              return {
                // [INICIO] estilo de seccion footer
                'border-top': '0px',
                'border-bottom': $scope.plantilla.border['border-width'],
                'border-left': $scope.plantilla.border['border-width'],
                'border-right': $scope.plantilla.border['border-width'],
                'border-style': $scope.plantilla.border['border-style'],
                'border-color': $scope.plantilla.border['border-color'],
                'border-bottom-left-radius': $scope.plantilla.border['border-radius'],
                'border-bottom-right-radius': $scope.plantilla.border['border-radius']
              }; // [FIN] estilo de seccion footer
          }
        }; // [FIN] función configuracionGeneralBordeSeccion
        /**
         * @description Retorna la configuración General de aspecto de la capa de fondo de la plantilla
         *
         * @returns {object}
         */
        $scope.configuracionGeneralFondo = function() {
          // [INICIO] función configuracionGeneralFondo
          // Se genera una copia temporal del estilo de plantilla
          var temp = angular.copy($scope.plantilla.style);
          // Se le agregan las unidades a los atributos que se almacenan como numero
          temp.width += 'cm';
          temp.height += 'cm';
          temp['padding-top'] += 'cm';
          temp['padding-left'] += 'cm';
          temp['padding-right'] += 'cm';
          temp['padding-bottom'] += 'cm';
          // Se genera objeto de estilo en base a configuracion por defecto y campos editados
          return angular.extend(
            {},
            // Se obtienen el estilo por defecto para configuracion base
            $scope.configuracionContrutorDeDocumentos.defaultOptions,
            // se le asignan los campos de objeto temporal
            temp,
            {
              // [INICIO] estilo capa de fondo de plantilla
              margin: 'auto', // Se establece margen auto
              position: 'absolute', // posicion absoluta
              top: 0, // ubicacion unica en top
              left: 0, // ubicacion unica en left
              right: 0, // ubicacion unica en right
              bottom: 0 // ubicacion unica en bottom
            } // [FIN] estilo capa de fondo de plantilla
          );
        }; // [FIN] función configuracionGeneralFondo
        /**
         * @description intercambia el estado del panel lateral de abierto a cerrado
         */
        $scope.abrirCerrarPanelLateral = function() {
          // [INICIO] función abrirCerrarPanelLateral
          // Intercambia el valor de abrirCerrarPanel de true a false, dependiendo del estado en que se encuentre
          $scope.abrirCerrarPanel = !$scope.abrirCerrarPanel;
        }; // [FIn] función abrirCerrarPanelLateral
        /**
         * Configuracion de dragula para elementos del constructor
         */
        dragulaService.options(
          // [INICIO] configuración de opciones de dragulaService
          $scope, // Se asigna scope a la configuracion
          'elementos', // Se asigna el nombre del contenedor de elementos de dragula
          {
            // [INICIO] opciones dragula
            copy: true, // Se activa opcion de copiar elementos
            copySortSource: false, // Se desactiva copiar elementos segun orden de fuente
            removeOnSpill: true, // elimina el elemento copiado si no se encuentra dentro de un contenedor valido de dragula
            /**
             * @description determina si el elemento del DOM al cual se copia un elemento es un contenedor Dragula
             *
             * @param {object} el - elemento de contenedor objetivo
             *
             * @returns {boolean}
             */
            isContainer: function(el) {
              // [INICIO] función isContainer
              // comprueba si el elemento objetivo contiene la clase 'doc-container' o 'doc-container-back'
              return (
                el.classList.contains('doc-container') ||
                el.classList.contains('doc-container-back')
              );
            }, // [FIN] función isContainer
            /**
             * @description determina si un elemento puede moverse según condiciones hacia el contenedor
             *
             * @param {object} el - elemento en movimiento
             * @param {object} source - contenedor del elemento
             * @param {object} handle - elemento que realmente esta activando el movimiento
             * @param {object} sibling - elemento hermano en el movimiento
             *
             * @returns {boolean}
             */
            moves: function(el, source, handle, sibling) {
              // [INICIO] función moves
              return !$(source).hasClass('doc-container'); // Retorna true si contiene la clase doc-container el elemento objetivo
            }, // [FIN] función moves
            /**
             * @description este metodo podría retornar true para los elementos que no pueden ser arrastrados
             *
             * @param {object} el - elemento en movimiento
             * @param {object} handle - elemento que realmente esta activando el movimiento
             *
             * @returns {boolean}
             */
            invalid: function(el, handle) {
              // [INICIO] función invalid
              return false; // permite que cualquier arrastre se inicie
            } // [FIN] función invalid
          } // [FIN] opciones dragula
        ); // [FIN] configuración de opciones de dragulaService
        /**
         * Configuracion de dragula para constructor
         */
        dragulaService.options(
          // [INICIO] configuración de opciones de dragulaService
          $scope, // Se asigna scope a la configuracion
          'elementos-html-header', // Se asigna el nombre del contenedor de elementos de dragula en plantilla
          {
            // [INCIO] opciones dragula
            copy: false, // Se desactiva opcion de copiar elementos
            copySortSource: false, // Se desactiva copiar elementos segun orden de fuente
            /**
             * @description determina si un elemento puede moverse según condiciones
             *
             * @param {object} el - elemento en movimiento
             * @param {object} source - contenedor del elemento
             * @param {object} handle - elemento que realmente esta activando el movimiento
             * @param {object} sibling - elemento hermano en el movimiento
             *
             * @returns {boolean}
             */
            moves: function(el, source, handle, sibling) {
              // [INICIO] función moves
              return $(handle).hasClass('move'); // Retorna true si contiene la clase move
            }, // [FIN] función moves
            /**
             * @description este metodo podría retornar true para los elementos que no pueden ser arrastrados
             *
             * @param {object} el - elemento en movimiento
             * @param {object} handle - elemento que realmente esta activando el movimiento
             *
             * @returns {boolean}
             */
            invalid: function(el, handle) {
              // [INICIO] función invalid
              return false; // permite que cualquier arrastre se inicie
            } // [FIN] función invalid
          } // [FIN] opciones dragula
        ); // [FIN] configuración de opciones de dragulaService
        dragulaService.options(
          // [INICIO] configuración de opciones de dragulaService
          $scope, // Se asigna scope a la configuracion
          'elementos-html-body', // Se asigna el nombre del contenedor de elementos de dragula en plantilla
          {
            // [INCIO] opciones dragula
            copy: false, // Se desactiva opcion de copiar elementos
            copySortSource: false, // Se desactiva copiar elementos segun orden de fuente
            /**
             * @description determina si un elemento puede moverse según condiciones
             *
             * @param {object} el - elemento en movimiento
             * @param {object} source - contenedor del elemento
             * @param {object} handle - elemento que realmente esta activando el movimiento
             * @param {object} sibling - elemento hermano en el movimiento
             *
             * @returns {boolean}
             */
            moves: function(el, source, handle, sibling) {
              // [INICIO] función moves
              return $(handle).hasClass('move'); // Retorna true si contiene la clase move
            }, // [FIN] función moves
            /**
             * @description este metodo podría retornar true para los elementos que no pueden ser arrastrados
             *
             * @param {object} el - elemento en movimiento
             * @param {object} handle - elemento que realmente esta activando el movimiento
             *
             * @returns {boolean}
             */
            invalid: function(el, handle) {
              // [INICIO] función invalid
              return false; // permite que cualquier arrastre se inicie
            } // [FIN] función invalid
          } // [FIN] opciones dragula
        ); // [FIN] configuración de opciones de dragulaService
        dragulaService.options(
          // [INICIO] configuración de opciones de dragulaService
          $scope, // Se asigna scope a la configuracion
          'elementos-html-footer', // Se asigna el nombre del contenedor de elementos de dragula en plantilla
          {
            // [INCIO] opciones dragula
            copy: false, // Se desactiva opcion de copiar elementos
            copySortSource: false, // Se desactiva copiar elementos segun orden de fuente
            /**
             * @description determina si un elemento puede moverse según condiciones
             *
             * @param {object} el - elemento en movimiento
             * @param {object} source - contenedor del elemento
             * @param {object} handle - elemento que realmente esta activando el movimiento
             * @param {object} sibling - elemento hermano en el movimiento
             *
             * @returns {boolean}
             */
            moves: function(el, source, handle, sibling) {
              // [INICIO] función moves
              return $(handle).hasClass('move'); // Retorna true si contiene la clase move
            }, // [FIN] función moves
            /**
             * @description este metodo podría retornar true para los elementos que no pueden ser arrastrados
             *
             * @param {object} el - elemento en movimiento
             * @param {object} handle - elemento que realmente esta activando el movimiento
             *
             * @returns {boolean}
             */
            invalid: function(el, handle) {
              // [INICIO] función invalid
              return false; // permite que cualquier arrastre se inicie
            } // [FIN] función invalid
          } // [FIN] opciones dragula
        ); // [FIN] configuración de opciones de dragulaService
        $scope.fontColorOptions = angular.extend(
          // [INICIO] configuracion de color-picker para color de fuente
          {}, // Objeto base de configuracion
          funcionesUtilesEditor.defaultOptionsColor, // Configuracion por defecto para color-picker
          {
            // [INICIO] Configuracion especifica
            id: 'font-color-global', // Id de color-picker en elemento HTML
            name: 'font-color-global', // Nombre de color-picker en elemento HTML
            pos: 'bottom left' // Posicion de modal de color-picker
          } // [FIN] Configuracion especifica
        ); // [FIN] configuracion de color-picker para color de fuente
        $scope.backgroundColorOptions = angular.extend(
          // [INICIO] configuracion de color-picker para color de fondo
          {}, // Objeto base de configuracion
          funcionesUtilesEditor.defaultOptionsColor, // Configuracion por defecto para color-picker
          {
            // [INICIO] Configuracion especifica
            id: 'background-color-global', // Id de color-picker en elemento HTML
            name: 'background-color-global', // Nombre de color-picker en elemento HTML
            pos: 'bottom right' // Posicion de modal de color-picker
          } // [FIN] Configuracion especifica
        ); // [FIN] configuracion de color-picker para color de fondo
        /**
         * @description Retorna la configuración General de aspecto de los elementos insertados en la plantilla
         *
         * @returns {object}
         */
        $scope.configuracionDeElementos = function(element) {
          // [INICIO] función configuracionDeElementos
          // Se genera una copia temporal del estilo del elemento
          var temp = angular.copy(element.options.style);
          // Se le agregan las unidades a los atributos que se almacenan como numero
          temp['padding-top'] += 'cm';
          temp['padding-left'] += 'cm';
          temp['padding-right'] += 'cm';
          temp['padding-bottom'] += 'cm';
          // en el caso que el estilo del elemento contenga 'border-width' se configura tambien la unidad
          if ('border-width' in temp) {
            temp['border-width'] += 'px';
          }
          // Se genera objeto de estilo en base a configuracion por defecto y campos editados
          return angular.extend(
            {},
            // se le asignan los campos de objeto temporal
            temp,
            {
              // [INICIO] estilo adicional elemento
              'box-sizing': 'border-box' // se define box-sizing del elemento como 'border-box'
            } // [FIN] estilo adicional elemento
          );
        }; // [FIN] función configuracionDeElementos
        /**
         * @description Retorna la configuración de texto de los elementos insertados en la plantilla
         *
         * @returns {object}
         */
        $scope.configuracionDeEstiloTexto = function(elementText) {
          // [INICIO] función configuracionDeEstiloTexto
          // Se genera una copia temporal del estilo de texto del elemento
          var temp = angular.copy(elementText);
          // en el caso que el estilo del elemento contenga 'font-size' se configura tambien la unidad
          if ('font-size' in temp) {
            temp['font-size'] += 'px';
          }
          // en el caso que el estilo del elemento contenga 'line-height' se configura tambien la unidad
          if ('line-height' in temp) {
            temp['line-height'] += 'px';
          }
          // en el caso que el estilo del elemento contenga 'padding-top' se configura tambien la unidad
          if ('padding-top' in temp) {
            temp['padding-top'] += 'cm';
          }
          // en el caso que el estilo del elemento contenga 'padding-right' se configura tambien la unidad
          if ('padding-right' in temp) {
            temp['padding-right'] += 'cm';
          }
          // en el caso que el estilo del elemento contenga 'padding-bottom' se configura tambien la unidad
          if ('padding-bottom' in temp) {
            temp['padding-bottom'] += 'cm';
          }
          // en el caso que el estilo del elemento contenga 'padding-left' se configura tambien la unidad
          if ('padding-left' in temp) {
            temp['padding-left'] += 'cm';
          }
          // Se genera objeto de estilo en base a configuracion por defecto y campos editados
          return angular.extend(
            {},
            // se le asignan los campos de objeto temporal
            temp
          );
        }; // [FIN] función configuracionDeEstiloTexto
        /**
         * @description inicia la edicion de un elemento
         *
         * @param elemento - es el elemento dragula con solo la configuración del DOM
         * @param lista - lista de elementos que contiene los elementos completos
         */
        $scope.edicionDeElemento = function(elemento, lista) {
          // [INICIO] función edicionDeElemento
          // Se busca el elemento completo en la 'lista' en base a 'elemento' y se asigna a 'elementoActual'
          $scope.elementoActual = funcionesUtilesEditor.findWhere(
            lista, // Lista de elementos
            {
              // [INICIO] opciones de findWhere
              id: elemento.id // se ingresa id de elemento
            } // [FIN] opciones de findWhere
          );
          // Se establece que el elemento actual se encuentra editando
          $scope.elementoActual.editando = true;
        }; // [FIN] función edicionDeElemento
        /**
         * @description remueve un elemento de la lista y del DOM
         *
         * @param elemento - es el elemento dragula con solo la configuración del DOM
         * @param lista - lista de elementos que contiene los elementos completos
         *
         * return {object}
         */
        $scope.removerElemento = function(elemento, lista) {
          // [INICIO] función removerElemento
          var deferred = $q.defer(); // @type {object} almacena una promesa
          UIkit.modal.confirm(
            // [INICIO] llamada metodo confirm
            '¿Estas seguro de eliminar el elemento?', // Mensaje de confirmación para eliminar un elemento
            function confirmar() {
              // [INICIO] función confirmar
              // Se busca el elemento completo en la 'lista' en base a 'elemento' y se asigna a 'el'
              var el = funcionesUtilesEditor.findWhere(
                lista, // Lista de elementos
                {
                  // [INICIO] opciones de findWhere
                  id: elemento.id // se ingresa id de elemento
                } // [FIN] opciones de findWhere
              );
              // Se remueve elemento de la lista
              lista.splice(lista.indexOf(el), 1);
              // Se limpia la variable elementoActual
              $scope.elementoActual = null;
              // Se aplica manualmente los cambios en el scope de elemento
              $scope.$apply();
              // Se resuelve la promesa
              deferred.resolve();
            }, // [FIN] función confirmar
            null, // No se asigna funcion cancelar
            {
              // [INICIO] opciones metodo confirm
              labels: {
                // [INICIO] opcion label
                Ok: 'Eliminar', // Cambio de texto en botón ok
                Cancel: 'Cancelar' // Cambio de texto en botón cancel
              }, // [FIN] opcion label
              stack: true // Otros modal se mantendrán ocultos
            } // [FIN] opciones metodo confirm
          ); // [FIN] llamada metodo confirm
          // Retorna una promesa
          return deferred.promise;
        }; // [FIN] función removerElemento
        /**
         * @description Obtiene nombre de template HTML por tipo de elemento
         *
         * @param {string} tipo - tipo de elemento del cual se buscará un template
         *
         * @returns {string}
         */
        $scope.obtenerTemplate = function(tipo) {
          // [INICIO] función obtenerTemplate
          // retorna el template segun el tipo, sino por defecto retorna textTemplate
          return tipo ? tipo + 'Template' : 'textTemplate';
        }; // [FIN] función obtenerTemplate
        /**
         * @description Retorna codigo 'html safe' para la vista
         *
         * @params {string} contenido - 'html unsafe'
         *
         * @returns {string}
         */
        $scope.prepararHTML = function(contenido) {
          // [INICIO] funcion prepararHTML
          // Corregir HTML para hacerlo 'safe'
          return $sce.trustAsHtml(contenido);
        }; // [FIN] funcion prepararHTML
        /**
         * @description Combina dos objetos
         *
         * @params {object} objeto1 - objeto 1 a combinar
         * @params {object} objeto2 - objeto 2 a combinar
         *
         * @returns {object}
         */
        $scope.combinarObjetos = function(objeto1, objeto2) {
          // [INICIO] funcion combinarObjetos
          // Combina con angular 2 objetos
          var temp = angular.extend({}, objeto1, objeto2);
          if ('font-size' in temp) {
            temp['font-size'] += 'px';
          }
          // en el caso que el estilo del elemento contenga 'line-height' se configura tambien la unidad
          if ('line-height' in temp) {
            temp['line-height'] += 'px';
          }
          return temp;
        }; // [FIN] funcion combinarObjetos
        /**
         * @description configuracion de eventos de modal
         */
        $('#modal_edit_element').on({
          // [INICIO] configuracion de eventos de modal
          'hide.uk.modal': function() {
            // [INICIO] funcion listener de evento 'hide.uk.modal'
            // Se establece que el elemento actual ya no se encuentra editando
            $scope.elementoActual.editando = false;
            // Se limpia la variable elementoActual
            $scope.elementoActual = null;
            // Se aplica manualmente los cambios en el scope de elemento
            $scope.$apply();
          } // [FIN] funcion listener de evento 'hide.uk.modal'
        }); // [FIN] configuracion de eventos de modal
        // @type {object} Se almacenan todas las acciones posibles de un elemento en objeto acciones
        $scope.acciones = {
          // [INICIO] objeto acciones
          prepararHTML: $scope.prepararHTML.bind(this), // se agrega prepararHTML con el contexto actual
          combinarObjetos: $scope.combinarObjetos.bind(this), // se agrega removerElemento con el contexto actual
          removerElemento: $scope.removerElemento.bind(this), // se agrega removerElemento con el contexto actual
          obtenerTemplate: $scope.obtenerTemplate.bind(this), // se agrega obtenerTemplate con el contexto actual
          edicionDeElemento: $scope.edicionDeElemento.bind(this), // se agrega edicionDeElemento con el contexto actual
          configuracionDeElementos: $scope.configuracionDeElementos.bind(this), // se agrega configuracionDeElementos con el contexto actual
          configuracionDeEstiloTexto: $scope.configuracionDeEstiloTexto.bind(this) // se agrega configuracionDeEstiloTexto con el contexto actual
        }; // [FIN] objeto acciones
        /**
         * Se configura evento 'Drag and drop' para elementos
         */
        $scope.$on(
          'elementos.drop', // Nombre de evento a escuchar
          /**
           * @description listener para eventos de arrastrar y soltar de elementos.
           *
           * @param {object} evento - objeto que contiene el evento 'element.drop'
           * @param {object} el - elemento en movimiento
           * @param {object} target - contenedor objetivo del elemento
           * @param {object} source - contenedor del elemento actual
           * @param {object} sibling - elemento hermano en el movimiento
           *
           * @returns {null}
           */
          function eventoDragYDropParaElementos(evento, el, target, source, sibling) {
            // [INICIO] funcion eventoDragYDropParaElementos
            // Se verifica si el contenedor objetivo corresponde a esta instancia
            if ($(target).hasClass('sub-container')) {
              // [INICIO] if
              // Si contiene la clase 'sub-container' termina la ejecucion del evento
              return;
            } // [FIN] if
            var tipoDeElemento = $(el).data('type'); // @type {string} almacena el tipo del elemento
            var elemento = angular.extend(
              // @type {object} se genera una instancia del elemento a almacenar
              {}, // Se ocupa para crear una nueva instancia
              {
                // [INICIO] objeto para iniciar elemento
                type: tipoDeElemento, // @type {string} almacena el tipo del elemento copiado (HTML)
                data_type: bloquesDeElementos[tipoDeElemento].element.data_type, // @type {object} almacena el tipo del elemento (objeto fuente)
                options: angular.copy(bloquesDeElementos[tipoDeElemento].defaultOptions) // @type {object} almacena las opciones del elemento (objeto fuente)
              } // [FIN] objeto para iniciar elemento
            );
            elemento.id = funcionesUtilesEditor.uid(); // @type {string} se asigna un id generado por funcion uid
            // Se remueve la copia nativa de dragula para la capa principal
            $('.doc-container li').remove();
            // Se remueve la copia nativa de dragula para la capa de fondo
            $('.doc-container-back li').remove();
            // Si el elemento no es flotante
            if (elemento.data_type !== 'float') {
              // [INICIO] if
              var index = $(sibling).index(); // @type {integer} almacena la ubicacion del elemento según elemento adjunto en la lista de elementos
              var elementos;
              switch ($(target).attr('id')) {
                case 'header-plantilla':
                  elementos = $scope.plantilla.source.headerElements;
                  break;
                case 'body-plantilla':
                  elementos = $scope.plantilla.source.bodyElements;
                  break;
                case 'footer-plantilla':
                  elementos = $scope.plantilla.source.footerElements;
                  break;
              }
              // Si index no es un valor valido como posicion en la lista
              if (index == -1) {
                // [INICIO] if
                // Se asigna a la lista de elementos en la ultima posicion
                elementos.push(elemento);
              } // [FIN] if
              // Si index corresponde a una posicion valida en la lista
              else {
                // [INICIO] else
                // Se asigna en la posicion anterior
                elementos.splice(index - 1, 0, elemento);
              } // [FIN] else
            } // [FIN] if
            // Si el elemento es flotante
            else {
              // [INICIO] else
              // Se asigna la posicion por defecto de los elementos flotantes
              elemento.options.position = angular.copy(
                bloquesDeElementos[tipoDeElemento].defaultOptions.defaultPosition
              );
              // Se asigna a la lista de elementos flotantes en la ultima posicion
              $scope.plantilla.source.backElements.push(elemento);
            } // [FIN] else
            // Desactiva la propagacion en la ejecución del evento eventoDragYDropParaElementos
            if (evento.stopPropagation) evento.stopPropagation();
            if (evento.preventDefault) evento.preventDefault();
            evento.cancelBubble = true;
            evento.returnValue = false;
            // Se aplica manualmente los cambios en el scope de elemento
            $scope.$apply();
          } // [FIN] funcion eventoDragYDropParaElementos
        );
        /**
         * Se configura evento 'dragend' para elementos flotantes
         */
        $scope.$on(
          'elementos.dragend', // Nombre de evento a escuchar
          /**
           * @description listener para evento 'dragend' de elementos para corregir la posición al final (en caso de errores).
           *
           * @param {object} evento - objeto que contiene el evento 'elementos.dragend'
           * @param {object} el - elemento en movimiento
           *
           * @returns {null}
           */
          function eventoDragEndParaElementos(evento, el) {
            // [INICIO] funcion eventoDragEndParaElementos
            // Se verifica si el elemento en movimiento contiene la clase 'builder-element-float' o si tiene un atributo 'id'
            if (!$(el).attr('id') || !$(el).hasClass('builder-element-float')) {
              // [INICIO] if
              // Si no se cumplen las condiciones termina la ejecucion del evento
              return;
            } // [FIN] if
            // Se busca el elemento completo en 'backElements' en base a 'elemento' y se asigna a 'elemento'
            var elemento = funcionesUtilesEditor.findWhere(
              $scope.plantilla.source.backElements, // Lista de elementos de fondo
              {
                // [INICIO] opciones de findWhere
                id: $(el).attr('id') // se ingresa id de elemento DOM
              } // [FIN] opciones de findWhere
            );
            var offsetDeContenedor = $('.doc-container-back').offset(); // @type {object} posiciones del contenedor
            var offsetDeElemento = $('.gu-mirror').offset(); // @type {object} posiciones del elemento en movimiento
            var dimensionesDeContenedor = {
              // @type {object} dimensiones del contenedor
              width: $('.doc-container-back').width(), // se obtiene el ancho computado del contenedor
              height: $('.doc-container-back').height() // se obtiene el alto computado del contenedor
            };
            var dimensionesDeElemento = {
              // @type {object} dimensiones del elemento
              width: $('.gu-mirror').width(), // se obtiene el ancho computado del elemento
              height: $('.gu-mirror').height() // se obtiene el alto computado del elemento
            };
            var x1 = offsetDeElemento.left - offsetDeContenedor.left; // posicion x1 del elemento dentro de la plantilla
            var y1 = offsetDeElemento.top - offsetDeContenedor.top; // posicion y1 del elemento dentro de la plantilla
            // Se comprueba si la posicion x1 es superior a 0
            if (x1 > 0) {
              // [INICIO] if
              var x2DeElemento = offsetDeElemento.left + dimensionesDeElemento.width; // posicion x2 del elemento dentro de la plantilla
              var x2DeContenedor = offsetDeContenedor.left + dimensionesDeContenedor.width; // posicion x2 de la plantilla
              // Se comprueba si la posicion x2 del elemento es superior a la posicion x2 de la plantilla
              if (x2DeElemento > x2DeContenedor) {
                // [INICIO] if
                // En este caso queda fuera de la plantilla en el eje x, por lo que se corrige la posicion del elemento restando lo exedente
                x1 -= x2DeElemento - x2DeContenedor;
              } // [FIN] if
            } // [FIN] if
            // Se comprueba si la posicion y1 es superior a 0
            if (y1 > 0) {
              // [INICIO] if
              var y2DeElemento = offsetDeElemento.top + dimensionesDeElemento.height; // posicion y2 del elemento dentro de la plantilla
              var y2DeContenedor = offsetDeContenedor.top + dimensionesDeContenedor.height; // posicion y2 de la plantilla
              if (y2DeElemento > y2DeContenedor) {
                // [INICIO] if
                // En este caso queda fuera de la plantilla en el eje y, por lo que se corrige la posicion del elemento restando lo exedente
                y1 -= y2DeElemento - y2DeContenedor;
              } // [FIN] if
            } // [FIN] if
            // Si el valor de x1 es inferior a 0, se asigna el valor 0, de lo contrario se mantiene el valor de x1
            elemento.options.position.left = x1 < 0 ? 0 : x1;
            // Si el valor de y1 es inferior a 0, se asigna el valor 0, de lo contrario se mantiene el valor de y1
            elemento.options.position.top = y1 < 0 ? 0 : y1;
          } // [FIN] funcion eventoDragEndParaElementos
        );
        /**
         * Se configura evento 'Drag and drop' para elementos dentro de la plantilla
         */
        /**
         * @description listener para eventos de arrastrar y soltar de elementos dentro del contenedor.
         *
         * @param {object} evento - objeto que contiene el evento 'elementos-html.drop'
         * @param {object} el - elemento en movimiento
         * @param {object} target - contenedor objetivo del elemento
         * @param {object} source - contenedor del elemento actual
         * @param {object} sibling - elemento hermano en el movimiento
         *
         * @returns {null}
         */
        function eventoDragYDropParaElementosEnContenedor(evento, el, target, source, sibling) {
          // [INICIO] función eventoDragYDropParaElementosEnContenedor
          // Se verifica si el contenedor objetivo corresponde a un sub contenedor
          if ($(target).hasClass('sub-container')) {
            // [INICIO] if
            // Si corresponde a un sub-contenedor termina la ejecucion del evento
            return;
          } // [FIN] if
          var id = $(el).attr('id'); // @type {string} almacena el id del elemento
          var index = $(sibling).index(); // @type {integer} almacena la ubicacion del elemento adjunto en la lista de elementos
          // Si index no es un valor valido como posicion en la lista
          var elementos;
          switch ($(source).attr('id')) {
            case 'header-plantilla':
              elementos = $scope.plantilla.source.headerElements;
              break;
            case 'body-plantilla':
              elementos = $scope.plantilla.source.bodyElements;
              break;
            case 'footer-plantilla':
              elementos = $scope.plantilla.source.footerElements;
              break;
          }
          if (index == -1) {
            // [INICIO] if
            index = elementos.length - 1;
          } // [FIN] if
          // Si index corresponde a una posicion valida en la lista
          else {
            // [INICIO] else
            index--; // se obtiene la posicion anterior del elemento adjunto
          } // [FIN] else
          // Se busca el elemento en la lista según id
          var elemento = funcionesUtilesEditor.findWhere(
            elementos, // Lista de elementos
            {
              // [INICIO] Opciones de búsqueda
              id: id // se asigna id para buscar elemento
            } // [FIN] Opciones de búsqueda
          );
          var oldIndex = elementos.indexOf(elemento); // @type {integer} Obtención de la posición anterior del elemento
          // Verifica si el index ingresado es mayor a la cantidad de elementos
          if (index >= elementos.length) {
            // [INICIO] if
            var k = index - elementos.length; // @type {integer} Obtención de un index corregido
            while (k-- + 1) {
              // [INICIO] while
              // Se ingresan elementos para corregir el error
              elementos.push(undefined);
            } // [FIN] while
          } // [FIN] if
          elementos.splice(index, 0, elementos.splice(oldIndex, 1)[0]); // se mueve el elemento a la nueva posicion
          // Desactiva la propagacion en la ejecución del evento eventoDragYDropParaElementosEnContenedor
          if (evento.stopPropagation) evento.stopPropagation();
          if (evento.preventDefault) evento.preventDefault();
          evento.cancelBubble = true;
          evento.returnValue = false;
          $scope.$apply(); // Se aplica manualmente los cambios en el scope de elemento
          return false;
        } // [FIN] función eventoDragYDropParaElementosEnContenedor
        $scope.$on(
          'elementos-html-header.drop', // Nombre de evento a escuchar
          eventoDragYDropParaElementosEnContenedor
        );
        $scope.$on(
          'elementos-html-body.drop', // Nombre de evento a escuchar
          eventoDragYDropParaElementosEnContenedor
        );
        $scope.$on(
          'elementos-html-footer.drop', // Nombre de evento a escuchar
          eventoDragYDropParaElementosEnContenedor
        );
        var abrirItemsCerradosAcordion = function() {
          var elements = UIkit.accordion(UIkit.$('.uk-accordion'), { collapse: false });
          elements.find('[data-wrapper]').each(function() {
            if (
              UIkit.$(this)
                .data('toggle')
                .hasClass('uk-active')
            )
              return;
            elements.toggleItem(UIkit.$(this), false, false); // animated true and collapse false
          });
        };
        $timeout(function() {
          abrirItemsCerradosAcordion();
          UIkit.$('.uk-tab').on('show.uk.switcher change.uk.tab', function(e, tab) {
            abrirItemsCerradosAcordion();
          });
        });
      } // [FIN] función de controlador editorPlantillaCtrl
    ] // [FIN] configuración de controlador editorPlantillaCtrl
  ); // [FIN] funcion controller
