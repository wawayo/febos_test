febosApp
  .config(
    [
      '$stateProvider',
      '$urlRouterProvider',
      '$locationProvider',
      function ($stateProvider, $urlRouterProvider, $locationProvider) {

        $stateProvider.state(
          "restringido.editorPlantilla", 
          {
            url: "/:app/editorPlantillas/editor?id&type",
            templateUrl: 'app/febos/editor_de_plantillas/editor/editorPlantillaView.html',
            controller: 'editorPlantillaCtrl',
            params: {
		          id: null,
              type: null,
              data: null
		        },
            resolve: {
              EstadoAnterior: [
                "$state",
                function ($state) {
                  var currentStateData = {
                    estado: $state.current.name,
                    parametros: $state.params,
                    url: $state.href($state.current.name, $state.params)
                  };
                  return currentStateData;
                }
              ],
              bloquesDeElementos: ['$q', '$templateCache', '$http', function ($q, $templateCache, $http){
                var deferred = $q.defer();
                var baseUrlTemplate = 'app/templates/editorPlantillaTemplates/elementos/';
                var baseUrlEditorTemplate = 'app/templates/editorPlantillaTemplates/modals/elementos/';
                $http.get('data/document_builder_blocks.json')
                .then(function(response){ 
                    var blocks = response.data;  //ajax request to fetch data into $scope.data
                    var blocks_extended = {};
                    var prom = [];
                    Object.keys(blocks).forEach(function (key) {
                      if(blocks[key].base != 'root'){
                        var elementTemp = blocks[key];
                        var baseElement = blocks_extended[elementTemp.base];
                        blocks_extended[key] = angular.merge(
                          angular.merge(
                            {}
                          , angular.copy(baseElement)
                          )
                        , angular.copy(elementTemp)
                        );
                        return;
                      }
                      blocks_extended[key] = angular.copy(blocks[key]);
                      return;
                    });
                    Object.keys(blocks_extended).forEach(function (key) {
                      if(blocks_extended[key].display){
                        prom.push($http.get(baseUrlTemplate + blocks_extended[key].templateUrl).then(function(response){
                          return $templateCache.put(blocks_extended[key].type + "Template", response.data);
                        }));
                        prom.push($http.get(baseUrlEditorTemplate + blocks_extended[key].type + 'Template.tpl.html').then(function(response){
                          return $templateCache.put(blocks_extended[key].type + "TemplateEditor", response.data);
                        }));
                      }
                    });
                    prom.push($http.get(baseUrlEditorTemplate + 'defaultTemplate.tpl.html').then(function(response){
                      return $templateCache.put("defultTemplateEditor", response.data);
                    }));
                    $q.all(prom).then(function () {
                      deferred.resolve(blocks_extended);
                    });
                  },
                  function (error){
                    deferred.resolve({});
                  });
                return deferred.promise;
              }],
/*             Autorizacion: ['Autorizacion', function (Autorizacion) {
                try {
                  Autorizacion.verificar();
                } catch (e) {
                  console.log("Error al autenticar", e);
                }
                return;
              }],*/
              deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                  'https://rawgit.com/RickStrahl/jquery-resizable/master/src/jquery-resizable.js',
                  'bower_components/tinycolor/dist/tinycolor-min.js',
                  'bower_components/angular-color-picker/dist/angularjs-color-picker.min.js',
                  'bower_components/angular-color-picker/dist/angularjs-color-picker.min.css',
                  'lazy_tinymce',
                  'lazy_uikit',
                  'lazy_iCheck',
                  'lazy_parsleyjs',
                  'lazy_dragula',
                  'app/febos/editor_de_plantillas/editor/editorPlantillaController.js'
                ], {serie: true});
              }]
            }
          }
        );
      }
    ]
  );

