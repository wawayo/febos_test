angular.module('febosApp').controller('foliosLegacyCtrl', [
  '$rootScope',
  '$scope',
  '$state',
  '$stateParams',
  'FebosAPI',
  'SesionFebos',
  'cafsDetalles',
  'FebosUtil',
  function(
    $rootScope,
    $scope,
    $state,
    $stateParams,
    FebosAPI,
    SesionFebos,
    cafsDetalles,
    FebosUtil
  ) {
    $scope.cafsDetalles = cafsDetalles;

    for (var i = 0; i < $scope.cafsDetalles.length; i++) {
      $scope.cafsDetalles[i].tipoDocumento = FebosUtil.tipoDocumento(
        parseInt($scope.cafsDetalles[i].tipoDTE)
      );
      $scope.cafsDetalles[i].tipoFoliacion =
        $scope.cafsDetalles[i].tipoCAF == 0 ? 'Manual' : 'Automático';
    }

    $scope.cafActual = {};

    $scope.propiedadFiltro = 'fechaVencimiento';
    $scope.reverse = true;

    $scope.sortBy = function(propiedad) {
      $scope.reverse = $scope.propiedadFiltro === propiedad ? !$scope.reverse : false;
      $scope.propiedadFiltro = propiedad;
    };

    $scope.modalAnularFolios = function(cafDetalle) {
      $scope.anular = {
        texto: 'ANULAR FOLIOS',
        folioInicio: '',
        folioFinal: ''
      };
      $scope.cafActual = cafDetalle;
      UIkit.modal('#modalAnularFolios').show();
    };

    $scope.validarAnularFolio = function() {
      if (isNaN($scope.anular.folioInicio) || isNaN($scope.anular.folioFinal)) {
        UIkit.modal.alert('Debe ingresar un valor numérico para los campos de folio.');
        return false;
      }

      if ($scope.anular.folioFinal <= 0 || $scope.anular.folioInicio <= 0) {
        UIkit.modal.alert('Debe ingresar un valor mayor o igual a 1 para los folios.');
        return false;
      }

      if ($scope.anular.folioInicio > $scope.anular.folioFinal) {
        UIkit.modal.alert('El folio inicial debe ser menor al folio final.');
        return false;
      }

      return true;
    };

    $scope.anularFolios = function() {
      $scope.anular.texto = 'ANULANDO FOLIOS...';

      // Validar formulario
      if ($scope.validarAnularFolio()) {
        FebosAPI.cl_anular_folios(
          {
            empresaId: SesionFebos().empresa.iut,
            cafId: $scope.cafActual.id,
            folioInicio: $scope.anular.folioInicio,
            folioFin: $scope.anular.folioFinal
          },
          {},
          true,
          true
        ).then(function(response) {
          if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
            UIkit.modal.alert(response.data.mensaje + '<br>' + response.data.mensajeWS);
            $scope.anular.texto = 'ANULAR FOLIOS';
          }
        });
      } else {
        $scope.anular.texto = 'ANULAR FOLIOS';
      }
    };
  }
]);
