febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.folios_legacy', {
      url: '/:app/cafsL/folios?tipoDTE&tipoCAF',
      templateUrl: 'app/febos/cafsLegacy/folios/foliosView.html',
      controller: 'foliosLegacyCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        cafsDetalles: [
          'FebosAPI',
          '$stateParams',
          'SesionFebos',
          function(FebosAPI, $stateParams, SesionFebos) {
            return FebosAPI.cl_listar_folios(
              {
                empresaId: SesionFebos().empresa.iut,
                tipoCAF: $stateParams.tipoCAF,
                tipoDTE: $stateParams.tipoDTE
              },
              true,
              false
            ).then(function(response) {
              if (typeof response.data.cafsDetalle != 'undefined') {
                return response.data.cafsDetalle;
              } else {
                return [];
              }
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/cafsLegacy/folios/foliosLegacyController.js'
            ]);
          }
        ]
      }
    });
  }
]);
