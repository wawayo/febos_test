angular.module('febosApp').controller('gestionFlujosCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$http',
  'utils',
  '$state',
  '$location',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $http,
    utils,
    $state,
    $location,
    SesionFebos,
    FebosAPI,
    FebosUtil
  ) {
    $scope.filtros = [];
    $scope.filtros.paginas = [];
    $scope.filtros.path = '';
    $scope.filtros.nombre = '';
    $scope.filtros.pagina = '1';
    $scope.filtros.itemsPorPagina = '10';
    $scope.filtros.numeroDePaginas = 9;
    $scope.totalElementos = 0;
    $scope.totalPaginas = 0;
    $scope.cargando = true;

    $scope.cargarFlujos = function() {
      $scope.cargando = true;
      $scope.flujos = [];
      FebosAPI.cl_listar_flujos(
        {
          filas: 100,
          pagina: 1,
          filtroNombre: $scope.filtros.nombre,
          filtroPath: $scope.filtros.path,
          empresaId: SesionFebos().empresa.id
        },
        {},
        true,
        false
      ).then(function(response) {
        console.log(JSON.stringify(response));
        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}
        $scope.cargando = false;
        if (response.data.codigo === 10 || response.data.codigo === '10') {
          $scope.flujos = response.data.flujos;
          for (i = 0; i < $scope.flujos.length; i++) {
            $scope.flujos[i].eliminando = 0;
            $scope.flujos[i].estadoAnterior = $scope.flujos[i].estaActivo;
          }

          if (
            response.data.totalElementos == undefined ||
            typeof response.data.totalElementos == 'undefined'
          ) {
            $scope.totalElementos = 0;
          } else {
            $scope.totalElementos = parseInt(response.data.totalElementos);
          }

          $scope.calcularPaginas();
        } else {
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId,
            response.data.errores
          );
        }
      });
    };
    $scope.editar = function(id) {
      var wfid = 'flujo=' + id;
      var api = '&api=api.febos.cl/' + FebosUtil.obtenerAmbiente();
      var token = '&token=' + SesionFebos().usuario.token;
      var empresa = '&empresa=' + SesionFebos().empresa.id;
      var nuevaURL = 'https://flujos.febos.io/#/?' + wfid + token + api + empresa;
      console.log('Abriendo URL: ' + nuevaURL);
      var popupWindow = window.open(nuevaURL, '_blank');
      location.reload(false);
    };
    $scope.eliminar = function(id, index) {
      $scope.flujos[index].eliminando = 1;
      FebosAPI.cl_eliminar_flujo(
        {
          empresaId: SesionFebos().empresa.id,
          flujoId: id
        },
        {},
        true,
        false
      ).then(function success(response) {
        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}
        console.log(response);
        if (response.data.codigo === 10) {
          $scope.flujos.splice(index, 1);
        } else {
          $scope.flujos[index].eliminando = 0;
          console.log(response);
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId,
            response.data.errores
          );
        }
      });
    };
    $scope.nuevo = function() {
      var msg = 'Creando nuevo flujo...';
      modal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>" +
          msg +
          "<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
      );

      FebosAPI.cl_guardar_flujo(
        {
          empresaId: SesionFebos().empresa.id
        },
        {},
        true,
        false
      ).then(function success(response) {
        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}
        //$rootScope.content_preloader_hide();
        modal.hide();
        if (response.data.codigo === 10) {
          console.log(response);
          inplace = typeof inplace === 'undefined' || inplace == '' ? false : inplace;
          console.log('inplace', inplace);
          console.log('reponse creacion nuevo flujo: ', response);
          var wfid = 'flujo=' + response.data.flujoId;
          var api = '&api=api.febos.cl/' + FebosUtil.obtenerAmbiente();
          var token = '&token=' + SesionFebos().usuario.token;
          var empresa = '&empresa=' + SesionFebos().empresa.id;
          if (typeof wfid === 'undefined' || wfid === 'undefined') {
            UIkit.modal.alert('Ups! Fallo');
            return;
          }
          var nuevaURL = 'https://flujos.febos.io/#/?' + wfid + token + api + empresa;
          console.log('abriendo URL: ' + nuevaURL);
          var popupWindow = window.open(nuevaURL, '_blank');
        } else {
          console.log(response);
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId,
            response.data.errores
          );
        }
      });
    };

    $scope.configurar = function(id) {
      console.log('CAMBIANDO URL! para configurar flujo: ' + id);
      //$state.go('restricted.configuracion_flujo', {febosId: id},{location:'replace'});
      $location.path('/cloud/flujos/configuracion/' + id);
    };
    $scope.ejecuciones = function(id) {
      console.log('CAMBIANDO URL! para listar ejecuciones flujo: ' + id);
      //$state.go('restricted.configuracion_flujo', {febosId: id},{location:'replace'});
      $location.path('/cloud/flujos/' + id + '/ejecuciones');
    };

    $scope.cambiarEstado = function(id, activo, index) {
      $scope.flujos[index].estadoAnterior = $scope.flujos[index].estaActivo;
      $scope.flujos[index].estaActivo = -1;

      FebosAPI.cl_cambiar_estado_flujo(
        {
          empresaId: SesionFebos().empresa.id,
          flujoId: id,
          estadoActivo: activo ? 'si' : 'no'
        },
        {},
        true,
        false
      ).then(function success(response) {
        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}
        console.log(response);
        //$rootScope.content_preloader_hide();
        if (response.data.codigo == 10) {
          $scope.flujos[index].estaActivo = !$scope.flujos[index].estadoAnterior;
          $scope.flujos[index].estadoAnterior = $scope.flujos[index].estaActivo;
        } else {
          $scope.flujos[index].estaActivo = $scope.flujos[index].estadoAnterior;
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId,
            response.data.errores
          );
        }
      });
    };

    $scope.limpiarFiltros = function() {
      $scope.filtros.nombre = '';
      $scope.filtros.path = '';
    };

    // -- función que calcula las páginas a mostrar en el mantenedor
    $scope.calcularPaginas = function() {
      $scope.totalPaginas = Math.ceil($scope.totalElementos / $scope.filtros.itemsPorPagina);
      var resta = -(Math.ceil($scope.filtros.numeroDePaginas) % 2 == 0
        ? Math.ceil($scope.filtros.numeroDePaginas / 2 - 1)
        : Math.ceil($scope.filtros.numeroDePaginas / 2 - 1));
      var suma =
        Math.ceil($scope.filtros.numeroDePaginas) % 2 == 0
          ? Math.ceil($scope.filtros.numeroDePaginas / 2)
          : Math.ceil($scope.filtros.numeroDePaginas / 2 - 1);
      var first = false;
      if (Math.ceil($scope.filtros.pagina) + resta <= 0) {
        first = true;
        suma -= Math.ceil($scope.filtros.pagina) + (resta - 1);
        resta -= Math.ceil($scope.filtros.pagina) + (resta - 1);
      }

      if ($scope.totalPaginas <= 0) suma = $scope.filtros.numeroDePaginas + resta - 1;

      if (
        Math.ceil($scope.filtros.pagina) + suma > $scope.totalPaginas &&
        $scope.totalPaginas > 0
      ) {
        if (first) {
          suma -= Math.ceil($scope.filtros.pagina + suma) - $scope.totalPaginas;
        } else {
          suma -= Math.ceil($scope.filtros.pagina + suma) - $scope.totalPaginas;
        }
      }

      var cont = 0;
      $scope.filtros.paginas = [];
      for (var i = $scope.filtros.pagina + resta; i <= $scope.filtros.pagina + suma; i++) {
        var nuevaPagina = {
          id: cont,
          numero: i
        };
        $scope.filtros.paginas.push(nuevaPagina);
        cont++;
      }
    };

    $scope.cargarFlujos();
  }
]);
