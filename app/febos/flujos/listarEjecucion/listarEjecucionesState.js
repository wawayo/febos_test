febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.ejecuciones_flujo", {
                    url: "/:app/flujos/:flujoId/ejecuciones",
                    templateUrl: 'app/febos/flujos/listarEjecucion/listarEjecucionesView.html',
                    controller: 'listarEjecucionesCtrl',
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_uikit',
                                    'lazy_iCheck',
                                    'app/febos/flujos/listarEjecucion/listarEjecucionesController.js'
                                ]);
                        }]
                    }
                })

            }]);