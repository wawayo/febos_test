angular.module('febosApp').controller('listarEjecucionesCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$http',
  'utils',
  '$state',
  '$location',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $http,
    utils,
    $state,
    $location,
    SesionFebos,
    FebosAPI,
    FebosUtil
  ) {
    $scope.nombre = '...';
    $scope.cargando = true;
    $scope.cargandoDetalle = false;
    $scope.fpp = 15;
    $scope.filtroSeguimientoId = '';
    $scope.filtroOrigen = '';
    $scope.filtroPath = '';
    $scope.ejecuciones = [];
    $scope.resultados = [];

    $scope.filtros = [];
    $scope.filtros.paginas = [];
    $scope.filtros.pagina = '1';
    $scope.filtros.itemsPorPagina = '20';
    $scope.filtros.numeroDePaginas = 9;

    $scope.totalElementos = 0;
    $scope.totalPaginas = 0;

    FebosAPI.cl_obtener_flujo(
      {
        empresaId: SesionFebos().empresa.id,
        flujoId: $state.params.flujoId,
        soloEstado: 'si'
      },
      {},
      true,
      false
    ).then(function success(response) {
      try {
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
      } catch (e) {}
      if (response.data.codigo == 10) {
        $scope.nombre = response.data.nombre;
      } else {
        console.log(response);
        SesionFebos().error(
          response.data.codigo,
          response.data.mensaje,
          response.data.seguimientoId,
          response.data.errores
        );
      }
    });

    // -- busqueda por filtros
    $scope.buscarFiltros = function(desdePaginacion) {
      var hoy = new Date();
      var hace30Dias = new Date();
      hace30Dias.setDate(hoy.getDate() - 30);

      if (
        typeof $scope.filtros.fecha_desde != 'undefined' &&
        (typeof $scope.filtros.fecha_hasta == 'undefined' || $scope.filtros.fecha_hasta.length == 0)
      ) {
        $scope.filtros.fecha_hasta = FebosUtil.formatearFecha(hoy);
      }

      if (
        (typeof $scope.filtros.fecha_desde == 'undefined' ||
          $scope.filtros.fecha_desde.length == 0) &&
        typeof $scope.filtros.fecha_hasta != 'undefined'
      ) {
        var fechaHace30Dias = new Date($scope.filtros.fecha_hasta);
        fechaHace30Dias.setDate(fechaHace30Dias.getDate() - 30);
        $scope.filtros.fecha_desde = FebosUtil.formatearFecha(fechaHace30Dias);
      }

      $scope.cargando = true;
      FebosAPI.cl_listar_ejecuciones_flujo(
        {
          empresaId: SesionFebos().empresa.id,
          flujoId: $state.params.flujoId,
          filas: $scope.fpp,
          pagina: desdePaginacion ? $scope.filtros.pagina : 1,
          elementosPorPagina: 20,
          filtroSeguimientoId: $scope.filtroSeguimientoId,
          filtroOrigen: $scope.filtroOrigen,
          filtroPath: typeof $scope.filtros.path == 'undefined' ? '' : $scope.filtros.path,
          fechaDesde:
            typeof $scope.filtros.fecha_desde == 'undefined' ||
            $scope.filtros.fecha_desde.length == 0
              ? FebosUtil.formatearFecha(hace30Dias)
              : $scope.filtros.fecha_desde,
          fechaHasta:
            typeof $scope.filtros.fecha_hasta == 'undefined' ||
            $scope.filtros.fecha_hasta.length == 0
              ? FebosUtil.formatearFecha(hoy)
              : $scope.filtros.fecha_hasta
        },
        {},
        true,
        false
      ).then(function success(response) {
        $scope.cargando = false;
        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}
        console.log(response);
        if (response.data.codigo === 10) {
          $scope.ejecuciones = response.data.ejecuciones;
        } else {
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId,
            response.data.errores
          );
        }

        if (
          response.data.totalElementos == undefined ||
          typeof response.data.totalElementos == 'undefined'
        ) {
          $scope.totalElementos = 0;
        } else {
          $scope.totalElementos = parseInt(response.data.totalElementos);
        }
        $scope.totalPaginas = response.data.totalPaginas;
        $scope.calcularPaginas();
      });
      $scope.cargando = false;
    };
    // -- fin busqueda por filtros

    $scope.listar = function() {
      var hoy = new Date();
      var hace30Dias = new Date();
      hace30Dias.setDate(hoy.getDate() - 30);

      $scope.cargando = true;
      FebosAPI.cl_listar_ejecuciones_flujo(
        {
          empresaId: SesionFebos().empresa.id,
          flujoId: $state.params.flujoId,
          filas: $scope.fpp,
          pagina: $scope.filtros.pagina,
          elementosPorPagina: 10,
          filtroSeguimientoId: $scope.filtroSeguimientoId,
          filtroOrigen: $scope.filtroOrigen,
          filtroPath: $scope.filtroPath,
          fechaDesde: FebosUtil.formatearFecha(hace30Dias),
          fechaHasta: FebosUtil.formatearFecha(hoy)
        },
        {},
        true,
        false
      ).then(function success(response) {
        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}
        $scope.cargando = false;
        console.log(response);
        if (response.data.codigo === 10) {
          $scope.ejecuciones = response.data.ejecuciones;
        } else {
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId,
            response.data.errores
          );
        }

        if (
          response.data.totalElementos == undefined ||
          typeof response.data.totalElementos == 'undefined'
        ) {
          $scope.totalElementos = 0;
        } else {
          $scope.totalElementos = parseInt(response.data.totalElementos);
        }
        $scope.totalPaginas = response.data.totalPaginas;
        $scope.calcularPaginas();
      });
    };
    $scope.verEjecucion = function(id, index) {
      console.log('Listando ejecucion: ' + id);
      $scope.ejecuciones[index].cargando = true;
      FebosAPI.cl_obtener_resumen_ejecucion_flujo(
        {
          empresaId: SesionFebos().empresa.id,
          flujoId: $state.params.flujoId,
          ejecucionId: id
        },
        {},
        true,
        false
      ).then(function success(response) {
        var modalEje = UIkit.modal('#modal_ejecucion');
        modalEje.show();
        $scope.ejecuciones[index].cargando = false;
        $scope.resultados = response.data.informacionDeEjecucion;
        for (i = 0; i < $scope.resultados; i++) {
          $scope.mostrarDetalles.push(false);
          $scope.comoAnterior.push(false);
          $scope.comoActual.push(false);
        }

        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}
        console.log(response);
      });
    };
    $scope.mostrarDetalles = [];
    $scope.comoAnterior = [];
    $scope.comoActual = [];
    $scope.mostrarAnterior = function(id, anterior) {
      for (i = 0; i < $scope.resultados.length; i++) {
        if ($scope.resultados[i].nodoId == id) {
          $scope.comoActual[i] = true;
        }
        if ($scope.resultados[i].nodoId == anterior) {
          $scope.comoAnterior[i] = true;
        }
      }
    };
    $scope.ocultarAnterior = function(id, anterior) {
      for (i = 0; i < $scope.resultados.length; i++) {
        if ($scope.resultados[i].nodoId == id) {
          $scope.comoActual[i] = false;
        }
        if ($scope.resultados[i].nodoId == anterior) {
          $scope.comoAnterior[i] = false;
        }
      }
    };

    $scope.limpiarFiltros = function() {
      $scope.filtros.nombre = '';
      $scope.filtros.path = '';
      $scope.filtros.fecha_desde = '';
      $scope.filtros.fecha_hasta = '';
    };

    // -- función que permite cambiar de página en un mantenedor
    $scope.cambiarPagina = function(index) {
      $scope.filtros.numeroDePaginas = Math.ceil($scope.filtros.numeroDePaginas);
      $scope.totalElementos = Math.ceil($scope.totalElementos);

      if (index == 'inicio') {
        $scope.filtros.pagina = 1;
      } else {
        if (index == 'final') {
          $scope.filtros.pagina = $scope.totalPaginas;
        } else {
          if (
            index !== '' &&
            (index != undefined || typeof index != 'undefined') &&
            ($scope.filtros.paginas[index] != undefined ||
              typeof $scope.filtros.paginas[index] != 'undefined')
          ) {
            $scope.filtros.pagina = $scope.filtros.paginas[index].numero;
          } else {
            $scope.filtros.pagina = 1;
          }
        }
      }
      if (
        (typeof $scope.filtros.fecha_desde == 'undefined' ||
          $scope.filtros.fecha_desde.length == 0) &&
        (typeof $scope.filtros.fecha_hasta == 'undefined' ||
          $scope.filtros.fecha_hasta.length == 0) &&
        (typeof $scope.filtros.fecha_hasta == 'undefined' || $scope.filtros.path.length == 0)
      ) {
        $scope.listar();
      } else {
        $scope.buscarFiltros(true);
      }
    };
    // -- función que calcula las páginas a mostrar en el mantenedor
    $scope.calcularPaginas = function() {
      $scope.totalPaginas = Math.ceil($scope.totalElementos / $scope.filtros.itemsPorPagina);

      var resta = -(Math.ceil($scope.filtros.numeroDePaginas) % 2 == 0
        ? Math.ceil($scope.filtros.numeroDePaginas / 2 - 1)
        : Math.ceil($scope.filtros.numeroDePaginas / 2 - 1));
      var suma =
        Math.ceil($scope.filtros.numeroDePaginas) % 2 == 0
          ? Math.ceil($scope.filtros.numeroDePaginas / 2)
          : Math.ceil($scope.filtros.numeroDePaginas / 2 - 1);
      var first = false;

      if (Math.ceil($scope.filtros.pagina) + resta <= 0) {
        first = true;
        suma -= Math.ceil($scope.filtros.pagina) + (resta - 1);
        resta -= Math.ceil($scope.filtros.pagina) + (resta - 1);
      }

      if ($scope.totalPaginas <= 0) suma = $scope.filtros.numeroDePaginas + resta - 1;

      if (
        Math.ceil($scope.filtros.pagina) + suma > $scope.totalPaginas &&
        $scope.totalPaginas > 0
      ) {
        if (first) {
          suma -= Math.ceil($scope.filtros.pagina + suma) - $scope.totalPaginas;
        } else {
          suma -= Math.ceil($scope.filtros.pagina + suma) - $scope.totalPaginas;
        }
      }

      var cont = 0;
      $scope.filtros.paginas = [];

      for (var i = $scope.filtros.pagina + resta; i <= $scope.filtros.pagina + suma; i++) {
        var nuevaPagina = {
          id: cont,
          numero: i
        };

        $scope.filtros.paginas.push(nuevaPagina);
        cont++;
      }
    };
    // --
    $scope.cambiarPagina('inicio'); //< iniciar la página

    //$scope.listar();
  }
]);
