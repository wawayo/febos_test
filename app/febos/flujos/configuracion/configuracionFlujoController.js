angular.module('febosApp').controller('configuracionFlujoCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$http',
  'utils',
  '$state',
  '$stateParams',
  '$location',
  'SesionFebos',
  'FebosAPI',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $http,
    utils,
    $state,
    $stateParams,
    $location,
    SesionFebos,
    FebosAPI
  ) {
    console.log($state.params);
    $scope.estado = 'Verificando estado...';
    $scope.estadoCodigo = -1;
    $scope.configs = [];
    $scope.ruta = '';
    $scope.nombre = '';
    $scope.cargando = true;

    $scope.filtros = [];
    $scope.filtros.paginas = [];
    $scope.filtros.pagina = '1';
    $scope.filtros.itemsPorPagina = '10';
    $scope.filtros.numeroDePaginas = 9;
    $scope.totalElementos = 0;
    $scope.totalPaginas = 0;

    //verificando el estado del WF
    FebosAPI.cl_obtener_flujo(
      {
        empresaId: SesionFebos().empresa.id,
        flujoId: $state.params.flujoId,
        soloEstado: 'si'
      },
      {},
      true,
      false
    ).then(function success(response) {
      //try{$rootScope.ultimoSeguimientoId = response.data.seguimientoId;}catch(e){}
      //$rootScope.content_preloader_hide();
      if (response.data.codigo == 10) {
        if (response.data.estaActivo) {
          $scope.estadoCodigo = 2;
          $scope.estado = 'Activo';
        } else {
          $scope.estadoCodigo = 1;
          $scope.estado = 'Detenido';
        }
        $scope.nombre = response.data.nombre;
      } else {
        console.log(response);
        SesionFebos().error(
          response.data.codigo,
          response.data.mensaje,
          response.data.seguimientoId,
          response.data.errores
        );
      }

      if (
        response.data.totalElementos == undefined ||
        typeof response.data.totalElementos == 'undefined'
      ) {
        $scope.totalElementos = 0;
      } else {
        $scope.totalElementos = parseInt(response.data.totalElementos);
      }

      $scope.calcularPaginas();
    });
    FebosAPI.cl_listar_configuraciones_de_flujo(
      {
        empresaId: SesionFebos().empresa.id,
        flujoId: $state.params.flujoId,
        elementosPorPagina: 10,
        pagina: 1
      },
      {},
      true,
      false
    ).then(function success(response) {
      try {
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
      } catch (e) {}
      $scope.cargando = false;
      //$rootScope.content_preloader_hide();
      console.log('CONFIGURACIONES', response);
      if (response.data.codigo == 10) {
        $scope.configs = response.data.configuraciones;
        for (i = 0; i < $scope.configs.length; i++) {
          $scope.configs[i].eliminando = 0;
        }
      } else {
        console.log(response);
        SesionFebos().error(
          response.data.codigo,
          response.data.mensaje,
          response.data.seguimientoId,
          response.data.errores
        );
      }
    });
    $scope.cambiarEstado = function(id, activo) {
      ruta = '';
      idConfig = '';
      estadoActual = '';
      if (id == '') {
        $scope.estado = $scope.estadoCodigo == 2 ? 'Deteniendo...' : 'Activando...';
        $scope.estadoCodigo = -1;
      } else {
        for (i = 0; i < $scope.configs.length; i++) {
          console.log($scope.configs[i].flujoConfiguracionId + ' == ' + id);
          if ($scope.configs[i].flujoConfiguracionId == id) {
            console.log('encontrado en la posicion: ' + i);
            estadoActual = $scope.configs[i].activo;
            $scope.configs[i].activo = -1;
            ruta = $scope.configs[i].path;
            idConfig = $scope.configs[i].flujoConfiguracionId;
          }
        }
      }

      FebosAPI.cl_cambiar_estado_flujo(
        {
          empresaId: SesionFebos().empresa.id,
          flujoId: $state.params.flujoId,
          estadoActivo: activo ? 'si' : 'no',
          path: id != '' ? ruta : ''
        },
        {},
        true,
        false
      ).then(function success(response) {
        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}
        console.log(response);
        //$rootScope.content_preloader_hide();
        if (response.data.codigo == 10) {
          if (id == '') {
            if (activo) {
              $scope.estadoCodigo = 2;
              $scope.estado = 'Activo';
            } else {
              $scope.estadoCodigo = 1;
              $scope.estado = 'Detenido';
            }
          } else {
            for (i = 0; i < $scope.configs.length; i++) {
              if ($scope.configs[i].flujoConfiguracionId === response.data.id) {
                console.log('encontrado en la posicion: ' + i);
                $scope.configs[i].activo = activo;
              }
            }
          }
        } else {
          console.log(response);
          if (id.length > 2) {
            for (i = 0; i < $scope.configs.length; i++) {
              if ($scope.configs[i].flujoConfiguracionId === response.data.id) {
                console.log('encontrado en la posicion: ' + i);
                $scope.configs[i].activo = estadoActual;
              }
            }
          }
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId,
            response.data.errores
          );
        }
      });
    };

    $scope.agregarPathModal = function() {
      var modal = UIkit.modal('#mdCrear');
      modal.show();
    };
    $scope.agregarPath = function() {
      var modal = UIkit.modal('#mdCrear');
      modal.hide();
      modal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Creando configuración...<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
      );
      modal.show();

      FebosAPI.cl_agregar_path_lectura_flujo(
        {
          empresaId: SesionFebos().empresa.id,
          flujoId: $state.params.flujoId,
          estadoActivo: 'no',
          path: $scope.ruta
        },
        {},
        true,
        false
      ).then(function success(response) {
        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}
        modal.hide();
        console.log(response);
        //$rootScope.content_preloader_hide();
        if (response.data.codigo == 10) {
          $scope.configs.push(response.data.configuracion);
        } else {
          console.log(response);
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId,
            response.data.errores
          );
        }
      });
    };
    $scope.eliminar = function(id, index, ruta) {
      UIkit.modal.confirm(
        'Esta seguro que desea eliminar la ruta ' + ruta,
        function() {
          $scope.configs[index].eliminando = 1;
          FebosAPI.cl_eliminar_path_lectura_flujo(
            {
              empresaId: SesionFebos().empresa.id,
              flujoId: $state.params.flujoId,
              configuracionId: id
            },
            {},
            true,
            false
          ).then(function success(response) {
            try {
              $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
            } catch (e) {}
            //$rootScope.content_preloader_hide();
            if (response.data.codigo == 10) {
              $scope.configs.splice(index, 1);
            } else {
              $scope.configs[index].eliminando = 0;
              console.log(response);
              SesionFebos().error(
                response.data.codigo,
                response.data.mensaje,
                response.data.seguimientoId,
                response.data.errores
              );
            }
          });
        },
        { labels: { Ok: 'Si, eliminar', Cancel: 'No, me acabo de arrepentir' } }
      );
    };

    // -- función que calcula las páginas a mostrar en el mantenedor
    $scope.calcularPaginas = function() {
      if ($scope.totalElementos == 0) {
        $scope.totalElementos = 1;
      }
      $scope.totalPaginas = Math.ceil($scope.totalElementos / $scope.filtros.itemsPorPagina);
      var resta = -(Math.ceil($scope.filtros.numeroDePaginas) % 2 == 0
        ? Math.ceil($scope.filtros.numeroDePaginas / 2 - 1)
        : Math.ceil($scope.filtros.numeroDePaginas / 2 - 1));
      var suma =
        Math.ceil($scope.filtros.numeroDePaginas) % 2 == 0
          ? Math.ceil($scope.filtros.numeroDePaginas / 2)
          : Math.ceil($scope.filtros.numeroDePaginas / 2 - 1);
      var first = false;
      if (Math.ceil($scope.filtros.pagina) + resta <= 0) {
        first = true;
        suma -= Math.ceil($scope.filtros.pagina) + (resta - 1);
        resta -= Math.ceil($scope.filtros.pagina) + (resta - 1);
      }

      if ($scope.totalPaginas <= 0) suma = $scope.filtros.numeroDePaginas + resta - 1;

      if (
        Math.ceil($scope.filtros.pagina) + suma > $scope.totalPaginas &&
        $scope.totalPaginas > 0
      ) {
        if (first) {
          suma -= Math.ceil($scope.filtros.pagina + suma) - $scope.totalPaginas;
        } else {
          suma -= Math.ceil($scope.filtros.pagina + suma) - $scope.totalPaginas;
        }
      }

      var cont = 0;
      $scope.filtros.paginas = [];
      for (var i = $scope.filtros.pagina + resta; i <= $scope.filtros.pagina + suma; i++) {
        var nuevaPagina = {
          id: cont,
          numero: i
        };
        $scope.filtros.paginas.push(nuevaPagina);
        cont++;
      }
    };
  }
]);
