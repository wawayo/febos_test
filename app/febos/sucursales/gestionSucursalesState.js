febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.gestionSucursales", {
                            url: "/empresas/:empresaid/sucursales",
                            templateUrl: 'app/febos/sucursales/gestionSucursalesView.html',
                            controller: 'gestionSucursalesCtrl',
                            resolve: {
                                EstadoAnterior: [
                                    "$state",
                                    function ($state) {
                                        var currentStateData = {
                                            estado: $state.current.name,
                                            parametros: $state.params,
                                            url: $state.href($state.current.name, $state.params)
                                        };
                                        return currentStateData;
                                    }
                                ],
                                Autorizacion: ['Autorizacion', function (Autorizacion) {
                                        try {
                                            Autorizacion.verificar();
                                        } catch (e) {
                                            console.log("Error al autenticar", e);
                                        }
                                        return;
                                    }],
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            // ocLazyLoad config (app/app.js)
                                            'lazy_countUp',
                                            'lazy_charts_peity',
                                            'lazy_charts_easypiechart',
                                            'lazy_charts_metricsgraphics',
                                            'lazy_charts_chartist',
                                            'lazy_weathericons',
                                            'lazy_google_maps',
                                            'lazy_clndr',
                                            'app/febos/sucursales/gestionSucursalesController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Dashboard'
                            },
                            ncyBreadcrumb: {
                                label: 'Home'
                            }
                        })

            }]);
