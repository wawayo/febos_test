angular.module('febosApp').controller('gestionSucursalesCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$http',
  '$state',
  '$location',
  'utils',
  'SesionFebos',
  'FebosAPI',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $http,
    $state,
    $location,
    utils,
    SesionFebos,
    FebosAPI
  ) {
    /*
      $scope.listar = function(){
        FebosAPI.cl_listar_sucursales({},{},true, false).then(
          function (response) {
              console.log(response);
              try {
                  if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
                    $scope.informacion.listar.elementos.valor = [];
                    var respuesta = response.data["sucursales"];

                    for (var i = 0; i < respuesta.length; i++) {
                      var elemento = {};
                      if (respuesta[i].id != undefined || typeof respuesta[i].id != "undefined") {
                        elemento["id"] = respuesta[i].id;
                      }

                      for (var j = 0; j < informacion.elementos.propiedades.length; j++) {
                                  if (respuesta[i][informacion.elementos.propiedades[j]] != undefined || typeof respuesta[i][informacion.elementos.propiedades[j]] != "undefined") {
                                      elemento[informacion.elementos.propiedades[j]] = respuesta[i][informacion.elementos.propiedades[j]];
                                  }
                              }

                              $scope.informacion["listar"].elementos.valor.push(elemento);
                          }

                          if (response.data.totalElementos == undefined || typeof response.data.totalElementos == "undefined") {
                              $scope.informacion["listar"].elementos.totalElementos = 0;
                          } else {
                              $scope.informacion["listar"].elementos.totalElementos = parseInt(response.data.totalElementos);
                          }
                          $scope.loadingSucursales = false;

                          $scope.calcularPaginas();
                          $scope.cambiarPagina('inicio');
                      }

                  } else {
                      febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                  }

              } catch (e) {
                  $scope.loadingSucursales = false;
              }

          }
        );
        titulo: "Gestión de Sucursales",
        listar: {                                   //< campos extra: filtro, columna. listado de sucursales
            filtros: false,                         //< no mostrar filtros
            titulo: "Filtrar Sucursales",           //< para los filtros
            cargando: "Filtrando Sucursales...",    //< para el botón
            tituloActual: "Filtrar Sucursales",     //< para el botón
            url: febosSingleton.api + "/empresas/" + febosSingleton.empresa.id + "/sucursales",
            metodo: 'GET',
            campos: [
                {titulo: "", solicitud: "debug", tipo: "param", valor: "si", filtro: false, columna: false},
                {titulo: "", solicitud: "simular", tipo:"param", valor: "no", filtro: false, columna: false},
            //    {titulo: "Departamento", solicitud: "departamento", tipo: "param", valor: "", filtro: false, columna: true},
            //    {titulo: "Subdivisión", solicitud: "subdivision", tipo: "param", valor: "", filtro: false, columna: true},
            //    {titulo: "Ciudad", solicitud: "ciudad", tipo: "param", valor: "", filtro: false, columna: true},
            //    {titulo: "Calle", solicitud: "calle", tipo:"param", valor: "", filtro: false, columna: true},
            //    {titulo: "Número", solicitud: "numero", tipo: "param", valor: "", filtro: false, columna: true},
                {titulo: "Dirección", solicitud: "direccion", tipo: "param", valor: "", filtro: false, columna: true},
                {titulo: "Correo Electrónico", solicitud: "correoElectronico", tipo: "param", valor: "", filtro: false, columna: true},
                {titulo: "Teléfono", solicitud: "telefono", tipo: "param", valor: "", filtro: false, columna: true},
                {titulo: "", solicitud: "pagina", tipo: "param", valor: "1", filtro: false, columna: false},
                {titulo: "Filas", solicitud: "filas", tipo: "param", valor: "10", filtro: false, columna: false}
            ],
            elementos: {nombre: "sucursales", valor: [], propiedades: [], paginacion: 9, totalElementos: 0, totalPaginas: 0, pagina: 1, filas: 10, paginas: []} //< propiedades se generan a partir de campos, nombre es el nombre en el response del arreglo de elementos
        }
        if ($scope.informacion[accion].tituloActual != $scope.informacion[accion].titulo) {
            return;
        }

        var informacion = $scope.informacion[accion];


        var parametros = {};
        var datos = {};

        for (var i = 0; i < informacion.campos.length; i++) {
            if (informacion.campos[i].tipo == "param") {
                if (informacion.campos[i].solicitud == "pagina") {
                    $scope.informacion[accion].campos[i].valor = $scope.informacion["listar"].elementos.pagina + "";
                }

                if (informacion.campos[i].solicitud == "filas") {
                    $scope.informacion[accion].campos[i].valor = $scope.informacion["listar"].elementos.filas + "";
                }

                parametros[informacion.campos[i].solicitud] = informacion.campos[i].valor;
            }

            if (informacion.campos[i].tipo == "data") {
                datos[informacion.campos[i].solicitud] = informacion.campos[i].valor;
            }
        }
        $scope.informacion["listar"].elementos.propiedades = [];
        for (var j = 0; j < $scope.informacion["listar"].campos.length; j++) {
            if (($scope.informacion["listar"].campos[j].columna != undefined || typeof $scope.informacion["listar"].campos[j].columna != "undefined") && $scope.informacion["listar"].campos[j].columna != "") {
                $scope.informacion["listar"].elementos.propiedades.push($scope.informacion["listar"].campos[j].solicitud);
            }
        }

      }

    $scope.loadingSucursales = true;
    // -- scope de data para los formularios y listados de sucursales
    $scope.informacion = {
        titulo: "Gestión de Sucursales",
        listar: {                                   //< campos extra: filtro, columna. listado de sucursales
            filtros: false,                         //< no mostrar filtros
            titulo: "Filtrar Sucursales",           //< para los filtros
            cargando: "Filtrando Sucursales...",    //< para el botón
            tituloActual: "Filtrar Sucursales",     //< para el botón
            url: febosSingleton.api + "/empresas/" + febosSingleton.empresa.id + "/sucursales",
            metodo: 'GET',
            campos: [
                {titulo: "", solicitud: "debug", tipo: "param", valor: "si", filtro: false, columna: false},
                {titulo: "", solicitud: "simular", tipo:"param", valor: "no", filtro: false, columna: false},
            //    {titulo: "Departamento", solicitud: "departamento", tipo: "param", valor: "", filtro: false, columna: true},
            //    {titulo: "Subdivisión", solicitud: "subdivision", tipo: "param", valor: "", filtro: false, columna: true},
            //    {titulo: "Ciudad", solicitud: "ciudad", tipo: "param", valor: "", filtro: false, columna: true},
            //    {titulo: "Calle", solicitud: "calle", tipo:"param", valor: "", filtro: false, columna: true},
            //    {titulo: "Número", solicitud: "numero", tipo: "param", valor: "", filtro: false, columna: true},
                {titulo: "Dirección", solicitud: "direccion", tipo: "param", valor: "", filtro: false, columna: true},
                {titulo: "Correo Electrónico", solicitud: "correoElectronico", tipo: "param", valor: "", filtro: false, columna: true},
                {titulo: "Teléfono", solicitud: "telefono", tipo: "param", valor: "", filtro: false, columna: true},
                {titulo: "", solicitud: "pagina", tipo: "param", valor: "1", filtro: false, columna: false},
                {titulo: "Filas", solicitud: "filas", tipo: "param", valor: "10", filtro: false, columna: false}
            ],
            elementos: {nombre: "sucursales", valor: [], propiedades: [], paginacion: 9, totalElementos: 0, totalPaginas: 0, pagina: 1, filas: 10, paginas: []} //< propiedades se generan a partir de campos, nombre es el nombre en el response del arreglo de elementos
        },
        crear: {                                //< campos extra: formulario
            titulo: "Crear Sucursal",           //< para el botón
            cargando: "Creando Sucursal...",    //< para el botón
            tituloActual: "Crear Sucursal",     //< para el botón
            url: febosSingleton.api + "/empresas/" + febosSingleton.empresa.id + "/sucursales",
            metodo: 'PUT',
            campos: [
                {titulo: "", solicitud: "debug", tipo: "param", valor: "si", formulario: false},
                {titulo: "", solicitud: "simular", tipo: "param", valor: "no", formulario: false},
            //    {titulo: "Departamento", solicitud: "departamento", tipo: "param", valor: "", filtro: false, columna: true},
            //    {titulo: "Subdivisión", solicitud: "subdivision", tipo: "param", valor: "", filtro: false, columna: true},
            //    {titulo: "Ciudad", solicitud: "ciudad", tipo: "param", valor: "", filtro: false, columna: true},
            //    {titulo: "Calle", solicitud: "calle", tipo:"param", valor: "", filtro: false, columna: true},
            //    {titulo: "Número", solicitud: "numero", tipo: "param", valor: "", filtro: false, columna: true},
                {titulo: "Dirección", solicitud: "direccion", tipo: "param", valor: "", filtro: false, columna: true},
                {titulo: "Correo Electrónico", solicitud: "correoElectronico", tipo:"data", valor: "", formulario: true},
                {titulo: "Teléfono", solicitud: "telefono", tipo: "data", valor: "", formulario: true}
            ]
        },
        modificar: {                                //< campos extra: formulario, modificable
            titulo: "Modificar Sucursal",           //< para el botón
            cargando: "Modificando Sucursal...",    //< para el botón
            tituloActual: "Modificar Sucursal",     //< para el botón
            url: febosSingleton.api + "/empresas/" + febosSingleton.empresa.id + "/sucursales/",
            index: "",                              //< para modificar
            metodo: 'POST',
            campos: [
                {titulo: "", solicitud: "debug", tipo: "param", valor: "si", formulario: false, modificable: false},
                {titulo: "", solicitud: "simular", tipo: "param", valor: "no", formulario: false, modificable: false},
            //    {titulo: "Departamento", solicitud: "departamento", tipo: "param", valor: "", filtro: false, columna: true},
            //    {titulo: "Subdivisión", solicitud: "subdivision", tipo: "param", valor: "", filtro: false, columna: true},
            //    {titulo: "Ciudad", solicitud: "ciudad", tipo: "param", valor: "", filtro: false, columna: true},
            //    {titulo: "Calle", solicitud: "calle", tipo:"param", valor: "", filtro: false, columna: true},
            //    {titulo: "Número", solicitud: "numero", tipo: "param", valor: "", filtro: false, columna: true},
                {titulo: "Dirección", solicitud: "direccion", tipo: "data", valor: "", formulario: true, modificable: true},
                {titulo: "Correo Electrónico", solicitud: "correoElectronico", tipo: "data", valor: "", formulario: true, modificable: true},
                {titulo: "Teléfono", solicitud: "telefono", tipo: "data", valor: "", formulario: true, modificable: true}
            ]
        },
        eliminar: {                     //< campos extra: ninguno
            titulo: "Eliminar",         //< para el botón
            cargando: "Eliminando...",  //< para el botón
            tituloActual: "Eliminar",   //< para el botón
            url: febosSingleton.api + "/empresas/" + febosSingleton.empresa.id + "/sucursales/",
            index: "",                  //< para eliminar
            metodo:'DELETE',
            campos: [
                {titulo: "", solicitud: "debug", tipo: "param", valor: "si"},
                {titulo: "", solicitud: "simular", tipo: "param", valor: "no"}
            ]
        }

    // -- scope listado de sucursales
    $scope.informacion["listar"].elementos.propiedades = [];
    for (var j = 0; j < $scope.informacion["listar"].campos.length; j++) {
        if (($scope.informacion["listar"].campos[j].columna != undefined || typeof $scope.informacion["listar"].campos[j].columna != "undefined") && $scope.informacion["listar"].campos[j].columna != "") {
            $scope.informacion["listar"].elementos.propiedades.push($scope.informacion["listar"].campos[j].solicitud);
        }
    }
    // --

    // -- función que tiene las acciones correspondientes a las sucursales, como listar, modificar, crear y eliminar
    $scope.enviarSolicitud = function (accion) {
        if ($scope.informacion[accion].tituloActual != $scope.informacion[accion].titulo) {
            return;
        }

        var informacion = $scope.informacion[accion];
        if (accion === "eliminar") {
            $scope.informacion.eliminar.tituloActual = $scope.informacion.eliminar.cargando;
        }

        if (accion === "crear") {
            var errores = "";
            for (var i = 0; i < informacion.campos.length; i++) {
                if (informacion.campos[i].formulario == true) {
                    if (informacion.campos[i].valor == "") {
                        errores += "\nDebe ingresar un valor para el campo " + informacion.campos[i].solicitud + ".";
                    }
                }
            }

            if (errores != "") {
                alert("Errores encontrados: " + errores);
                return;
            }

            $scope.informacion.crear.tituloActual = $scope.informacion.crear.cargando;
        }

        if (accion === "modificar") {
            var errores = "";
            for (var i = 0; i < informacion.campos.length; i++) {
                if (informacion.campos[i].formulario == true) {
                    if (informacion.campos[i].valor == "") {
                        errores += "\nDebe ingresar un valor para el campo " + informacion.campos[i].solicitud + ".";
                    }
                }
            }

            if (errores != "") {
                alert("Errores encontrados: " + errores);
                return;
            }

            $scope.informacion.modificar.tituloActual = $scope.informacion.modificar.cargando;
        }

        if (accion === "obtenerPermisos") {
            $scope.informacion["asignarPermisos"].tituloActual = $scope.informacion.obtenerPermisos.cargando;
        }

        if (accion === "asignarPermisos") {
            $scope.informacion.asignarPermisos.tituloActual = $scope.informacion.asignarPermisos.cargando;

            for (var i = 0; i < $scope.informacion.asignarPermisos.campos.length; i++) {
                if ($scope.informacion.asignarPermisos.campos[i].solicitud == "permisos") {
                    if ($scope.informacion.asignarPermisos.permisos.constructor === Array) {
                        $scope.informacion.asignarPermisos.campos[i].valor = $scope.informacion.asignarPermisos.permisos.join(",");
                    } else {
                        $scope.informacion.asignarPermisos.campos[i].valor = $scope.informacion.asignarPermisos.permisos;
                    }
                }
            }
        }

        var parametros = {};
        var datos = {};

        for (var i = 0; i < informacion.campos.length; i++) {
            if (informacion.campos[i].tipo == "param") {
                if (informacion.campos[i].solicitud == "pagina") {
                    $scope.informacion[accion].campos[i].valor = $scope.informacion["listar"].elementos.pagina + "";
                }

                if (informacion.campos[i].solicitud == "filas") {
                    $scope.informacion[accion].campos[i].valor = $scope.informacion["listar"].elementos.filas + "";
                }

                parametros[informacion.campos[i].solicitud] = informacion.campos[i].valor;
            }

            if (informacion.campos[i].tipo == "data") {
                datos[informacion.campos[i].solicitud] = informacion.campos[i].valor;
            }
        }

        var solicitud = { //< request de envío a la API
            method: informacion.metodo,
            url: informacion.url + ((informacion.index != undefined || typeof informacion.index != "undefined") && (informacion.index != "") ? $scope.informacion["listar"].elementos.valor[informacion.index].id + (accion == "obtenerPermisos" || accion == "asignarPermisos" ? "/permisos" : "") : ""),
            params: parametros,
            data: datos
        };

        $http(solicitud).then(function (response) {
            console.log(response);
            try {
                if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
                    if (accion === "obtenerPermisos" || accion === "asignarPermisos") {
                        if (accion === "obtenerPermisos") {
                            $scope.informacion.obtenerPermisos.elementos.valor = [];
                            $scope.informacion.asignarPermisos.permisos = [];
                            var respuesta = response.data[$scope.informacion.obtenerPermisos.elementos.nombre];

                            for (var i = 0; i < respuesta.length; i++) {
                                var elemento = {};
                                for (var j = 0; j < informacion.elementos.propiedades.length; j++) {
                                    if (respuesta[i][informacion.elementos.propiedades[j]] != undefined || typeof respuesta[i][informacion.elementos.propiedades[j]] != "undefined") {
                                        elemento[informacion.elementos.propiedades[j]] = respuesta[i][informacion.elementos.propiedades[j]];
                                    }
                                }

                                if ((respuesta[i].codigo != undefined || typeof respuesta[i].codigo != "undefined") && (respuesta[i].asignado != undefined || typeof respuesta[i].asignado != "undefined") && respuesta[i].asignado == "si") {
                                    $scope.informacion.asignarPermisos.permisos.push(respuesta[i].codigo);
                                }

                                $scope.informacion["obtenerPermisos"].elementos.valor.push(elemento);
                            }

                            $scope.informacion["asignarPermisos"].tituloActual = $scope.informacion.asignarPermisos.titulo;
                        }

                        if (accion === "asignarPermisos") {
                            $scope.informacion["asignarPermisos"].tituloActual = $scope.informacion.asignarPermisos.titulo;
                            alert("Permisos Asignados Correctamente.");
                        }

                        return;
                    }

                    if (accion === "listar") {
                        $scope.informacion.listar.elementos.valor = [];
                        var respuesta = response.data[$scope.informacion.listar.elementos.nombre];

                        for (var i = 0; i < respuesta.length; i++) {
                            var elemento = {};
                            if (respuesta[i].id != undefined || typeof respuesta[i].id != "undefined") {
                                elemento["id"] = respuesta[i].id;
                            }

                            for (var j = 0; j < informacion.elementos.propiedades.length; j++) {
                                if (respuesta[i][informacion.elementos.propiedades[j]] != undefined || typeof respuesta[i][informacion.elementos.propiedades[j]] != "undefined") {
                                    elemento[informacion.elementos.propiedades[j]] = respuesta[i][informacion.elementos.propiedades[j]];
                                }
                            }

                            $scope.informacion["listar"].elementos.valor.push(elemento);
                        }

                        if (response.data.totalElementos == undefined || typeof response.data.totalElementos == "undefined") {
                            $scope.informacion["listar"].elementos.totalElementos = 0;
                        } else {
                            $scope.informacion["listar"].elementos.totalElementos = parseInt(response.data.totalElementos);
                        }
                        $scope.loadingSucursales = false;

                        $scope.calcularPaginas();

                    } else {
                        if (accion === "eliminar") {
                            //$scope.informacion.eliminar.tituloActual=$scope.informacion.eliminar.cargando;
                            $scope.cerrarEliminar();
                        }

                        if (accion === "crear") {
                            //$scope.informacion.crear.tituloActual=$scope.informacion.crear.cargando;
                            $scope.cerrarCrear();
                        }

                        if (accion === "modificar") {
                            //$scope.informacion.modificar.tituloActual=$scope.informacion.modificar.cargando;
                            $scope.cerrarModificar();
                        }

                        $scope.cambiarPagina('inicio');
                    }

                } else {
                    febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
                }

            } catch (e) {
                $scope.loadingSucursales = false;
                febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId);
            }

        }, function (response) {
            console.log(response);
            febosSingleton.error(1, "Ha Fallado la Solicitud", "Sin Id de Seguimiento", "No Hay Errores que Mostrar");
        });
    }
    // --

    // -- función que limpia los input del filtrado de sucursales
    $scope.limpiarFiltros = function () {
        for (var j = 0; j < $scope.informacion["listar"].campos.length; j++) {
            if (($scope.informacion["listar"].campos[j].filtro != undefined || typeof $scope.informacion["listar"].campos[j].filtro != "undefined") && ($scope.informacion["listar"].campos[j].filtro)) {
                /*
                if ($scope.informacion["listar"].campos[j].valor.constructor === Array) {
                    $scope.informacion["listar"].campos[j].valor = [];
                } else {
                    $scope.informacion["listar"].campos[j].valor = "";
                }


                $scope.informacion["listar"].campos[j].valor = "";
            }
        }
    };
    // --

    // -- función que permite cambiar de página en un mantenedor
    $scope.cambiarPagina = function (index) {
        $scope.informacion["listar"].elementos.paginacion = Math.ceil($scope.informacion["listar"].elementos.paginacion);
        $scope.informacion["listar"].elementos.totalElementos = Math.ceil($scope.informacion["listar"].elementos.totalElementos);

        if (index == "inicio") {
            $scope.informacion["listar"].elementos.pagina = 1;
        } else {
            if (index == "final") {
                $scope.informacion["listar"].elementos.pagina = $scope.informacion["listar"].elementos.totalPaginas;
            } else {
                if ((index != undefined || typeof index != "undefined") && ($scope.informacion["listar"].elementos.paginas[index] != undefined || typeof $scope.informacion["listar"].elementos.paginas[index] != "undefined") && index !== '') {
                    $scope.informacion["listar"].elementos.pagina = $scope.informacion["listar"].elementos.paginas[index].numero;
                } else {
                    $scope.informacion["listar"].elementos.pagina = 1;
                }
            }
        }

        $scope.enviarSolicitud("listar");
    };
    // --

    // -- función que calcula las páginas a mostrar en el mantenedor
    $scope.calcularPaginas = function () {
        $scope.informacion["listar"].elementos.totalPaginas = Math.ceil($scope.informacion["listar"].elementos.totalElementos / $scope.informacion["listar"].elementos.filas);

        var resta =- (Math.ceil($scope.informacion["listar"].elementos.paginacion) % 2 == 0 ? Math.ceil($scope.informacion["listar"].elementos.paginacion / 2 - 1) : Math.ceil($scope.informacion["listar"].elementos.paginacion / 2 - 1));
        var suma = (Math.ceil($scope.informacion["listar"].elementos.paginacion) % 2 == 0 ? Math.ceil($scope.informacion["listar"].elementos.paginacion / 2) : Math.ceil($scope.informacion["listar"].elementos.paginacion / 2 - 1));
        var first = false;

        if (Math.ceil($scope.informacion["listar"].elementos.pagina) + resta <= 0) {
            first = true;
            suma -= (Math.ceil($scope.informacion["listar"].elementos.pagina) + (resta - 1));
            resta -= (Math.ceil($scope.informacion["listar"].elementos.pagina) + (resta - 1));
        }

        if ($scope.informacion["listar"].elementos.totalPaginas <= 0) suma = $scope.informacion["listar"].elementos.paginacion + resta - 1;

        if (Math.ceil($scope.informacion["listar"].elementos.pagina) + suma > $scope.informacion["listar"].elementos.totalPaginas && $scope.informacion["listar"].elementos.totalPaginas > 0) {
            if (first) {
                suma -= Math.ceil($scope.informacion["listar"].elementos.pagina + suma) - ($scope.informacion["listar"].elementos.totalPaginas);
            } else {
                suma -= Math.ceil($scope.informacion["listar"].elementos.pagina + suma) - ($scope.informacion["listar"].elementos.totalPaginas);
            }
        }

        var cont = 0;
        $scope.informacion["listar"].elementos.paginas = [];

        for (var i = $scope.informacion["listar"].elementos.pagina + resta; i <= $scope.informacion["listar"].elementos.pagina + suma; i++) {
            var nuevaPagina = {
                id: cont,
                numero: i
            };

            $scope.informacion["listar"].elementos.paginas.push(nuevaPagina);
            cont++;
        }
    };
    // --

    // -- función que activa el modal con el formulario para crear nueva sucursal
    $scope.abrirCrear = function (index) {
        $scope.informacion.crear.tituloActual = $scope.informacion.crear.titulo;

        for (var j = 0; j < $scope.informacion["crear"].campos.length; j++) {
            $scope.informacion["crear"].campos[j].valor = "";
        }

        var modal = UIkit.modal("#mdCrear");
        modal.show();
    };
    // --

    // -- función que cierra modal crear nueva sucursal
    $scope.cerrarCrear = function () {
        var modal = UIkit.modal("#mdCrear");
        modal.hide();
    };
    // --

    // -- función que activa modal para modificar una sucursal
    $scope.abrirModificar = function (index) {
        $scope.informacion.modificar.tituloActual = $scope.informacion.modificar.titulo;
        $scope.informacion["modificar"].index = index;

        for (var j = 0; j < $scope.informacion["modificar"].campos.length; j++) {
            if (($scope.informacion["modificar"].campos[j].formulario != undefined || typeof $scope.informacion["modificar"].campos[j].formulario != "undefined") && $scope.informacion["modificar"].campos[j].formulario) {
                $scope.informacion["modificar"].campos[j].valor = $scope.informacion["listar"].elementos.valor[index][$scope.informacion["modificar"].campos[j].solicitud];
            }
        }

        var modal = UIkit.modal("#mdModificar");
        modal.show();
    };
    // -

    // -- función que activa modal para eliminar una sucursal
    $scope.abrirEliminar = function (index) {
        $scope.informacion.eliminar.tituloActual = $scope.informacion.eliminar.titulo;
        $scope.informacion["eliminar"].index = index;

        var modal = UIkit.modal("#mdEliminar");
        modal.show();
    };
    // --

    // -- función que cierra modal para eliminar sucursal
    $scope.cerrarEliminar = function () {
        var modal = UIkit.modal("#mdEliminar");
        modal.hide();
    };
    // --

    // -- función que activa modal para obtener y asignar permisos a una sucursal
    $scope.abrirPermisos = function (index) {
        $scope.informacion["obtenerPermisos"].index = index;
        $scope.informacion["asignarPermisos"].index = index;
        $scope.informacion.obtenerPermisos.tituloActual = $scope.informacion.obtenerPermisos.titulo;

        var modal = UIkit.modal("#mdPermisos");
        modal.show();

        $scope.enviarSolicitud('obtenerPermisos');
    };
    // --

    // -- función que cierra modal para obtener y asignar permisos a una sucursal
    $scope.cerrarPermisos = function () {
        $scope.informacion["obtenerPermisos"].index = "";
        $scope.informacion["asignarPermisos"].index = "";

        var modal = UIkit.modal("#mdPermisos");
        modal.hide();
    };
    // --

    $scope.cambiarPagina('inicio'); //< paginación del listado de sucursales
});
*/
  }
]);
