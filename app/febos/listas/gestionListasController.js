angular.module('febosApp').controller('gestionListasCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$http',
  'utils',
  '$state',
  '$location',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $http,
    utils,
    $state,
    $location,
    SesionFebos,
    FebosAPI,
    FebosUtil
  ) {
    $scope.listas = [];
    $scope.cargando = false;
    $scope.cargandoSublista = false;
    $scope.listaActual = {
      nombre: '',
      descripcion: ''
    };
    $scope.query = '';
    $scope.queryB = '';
    $scope.subelementos = [];
    $scope.subelementosWorking = [];
    $scope.buscar = function() {
      $scope.cargando = true;
      FebosAPI.cl_listar_listas(
        {
          empresaId: SesionFebos().empresa.id
        },
        {},
        true,
        false
      ).then(function success(response) {
        console.log('listas', response);
        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}
        $scope.cargando = false;
        if (response.data.codigo === 10) {
          $scope.listas = response.data.listas;
        } else {
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId,
            response.data.errores
          );
        }
      });
    };
    $scope.buscar();
    //modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Eliminando grupo...<br/><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');
    $scope.modalCrear = function() {
      $scope.listaActual = {
        nombre: '',
        descripcion: ''
      };
      UIkit.modal('#mdlNuevaLista').show();
    };
    $scope.modalEditarElementos = function() {
      $scope.queryB = '';
      //$scope.subelementosWorking=(jQuery.extend(true, {}, $scope.subelementos));
      $scope.subelementosWorking = [];
      $scope.subelementos.forEach(function(el) {
        $scope.subelementosWorking.push({
          llave: el.llave,
          valor: el.valor
        });
      });
      UIkit.modal('#mdlModificarElementosLista').show();
    };
    $scope.saveData = function(data, fileName, contentType) {
      var a = document.createElement('a');
      document.body.appendChild(a);
      a.style = 'display: none';
      var blob = $scope.b64toBlob(FebosUtil.encode(data), contentType);
      var url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = fileName;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
    };

    $scope.descargarCSV = function() {
      console.log('DESCARGANDO!');
      var archivo = 'llave,valor\n';
      for (var i = 0; i < $scope.subelementos.length; i++) {
        archivo += $scope.subelementos[i].llave + ',' + $scope.subelementos[i].valor + '\n';
      }
      console.log(archivo);
      try {
        setTimeout(function() {
          //var blob = $scope.b64toBlob(febosSingleton.base64.encode(archivo), 'text/csv');
          //var blobUrl = URL.createObjectURL(blob);
          //window.location = blobUrl;
          $scope.saveData(
            archivo,
            'Lista personalizada - ' + $scope.listaActual.nombre + '.csv',
            'text/csv'
          );
        }, 150);
      } catch (e) {}
    };
    $scope.cargarCSV = function() {
      //se crea input temporal
      var input = document.createElement('input');
      input.type = 'file';
      input.name = 'archivo';
      input.accept = '.csv';

      //se le agrega evento al input
      input.onchange = function() {
        var file = this.files[0];
        var reader = new FileReader();
        reader.onload = function(progressEvent) {
          try {
            $scope.subelementosWorking = [];
            var lineas = this.result.split('\n');
            var separador = lineas[0].indexOf(',') > 0 && lineas[0].indexOf(';') == -1 ? ',' : ';';
            for (var linea = 0; linea < lineas.length; linea++) {
              if (lineas[linea].indexOf('llave') >= 0) continue;
              var parte = lineas[linea].split(separador);
              if (parte.length != 2) continue;
              $scope.subelementosWorking.push({
                llave: parte[0],
                valor: parte[1]
              });
            }
            console.log($scope.subelementosWorking);
            input.remove();
            setTimeout(function() {
              $scope.$apply();
            }, 100);
          } catch (e) {
            consolr.log(e);
          }
        };
        reader.readAsText(file);
      };
      input.click();
    };
    $scope.modalModificar = function(el) {
      $scope.listaActual = jQuery.extend(true, {}, el);
      UIkit.modal('#mdlModificarLista').show();
    };
    $scope.crear = function() {
      UIkit.modal('#mdlNuevaLista').hide();
      modal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Creando nueva lista...<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
      );

      FebosAPI.cl_crear_lista(
        {
          empresaId: SesionFebos().empresa.id
        },
        $scope.listaActual,
        true,
        false
      ).then(function success(response) {
        modal.hide();
        UIkit.notify("<i class='uk-icon-check'></i> Lista creada!", {
          status: 'success',
          timeout: 5000,
          pos: 'top-center'
        });
        console.log('crear', response);

        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}
        $scope.cargando = false;
        if (response.data.codigo === 10) {
          $scope.listaActual.listaId = response.data.listaId;
          $scope.listaActual.empresaId = SesionFebos().empresa.id;

          $scope.listas.push(jQuery.extend(true, {}, $scope.listaActual));
        } else {
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId,
            response.data.errores
          );
        }
      });
    };
    $scope.modificar = function() {
      UIkit.modal('#mdlModificarLista').hide();
      modal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Modificando lista...<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
      );

      FebosAPI.cl_modificar_cabecera_lista(
        {
          empresaId: SesionFebos().empresa.id,
          listaId: $scope.listaActual.listaId
        },
        $scope.listaActual,
        true,
        false
      ).then(function success(response) {
        modal.hide();

        console.log('modificar', response);
        for (var i = 0; i < $scope.listas.length; i++) {
          if ($scope.listas[i].listaId == $scope.listaActual.listaId) {
            $scope.listas[i].nombre = $scope.listaActual.nombre;
            $scope.listas[i].descripcion = $scope.listaActual.descripcion;
          }
        }

        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}
        $scope.cargando = false;
        if (response.data.codigo === 10) {
          UIkit.notify("<i class='uk-icon-check'></i> Lista modificada!", {
            status: 'success',
            timeout: 5000,
            pos: 'top-center'
          });
        } else {
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId,
            response.data.errores
          );
        }
      });
    };

    $scope.eliminarModal = function(lista) {
      UIkit.modal.confirm(
        'Esta seguro que desea eliminar la lista <b>' + $scope.listaActual.nombre + '</b>?',
        function() {
          $scope.eliminar(lista);
        },
        { labels: { Ok: 'Si, eliminar', Cancel: 'No, me acabo de arrepentir' } }
      );
    };
    $scope.eliminar = function(lista) {
      modal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Eliminando lista...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );

      FebosAPI.cl_eliminar_lista(
        {
          empresaId: SesionFebos().empresa.id,
          listaId: $scope.listaActual.listaId
        },
        {},
        true,
        false
      ).then(function success(response) {
        modal.hide();

        console.log('eliminar', response);
        var indice = -1;
        for (var i = 0; i < $scope.listas.length; i++) {
          if ($scope.listas[i].listaId == $scope.listaActual.listaId) {
            indice = i;
            break;
          }
        }
        $scope.listas.splice(indice, 1);
        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}
        $scope.cargando = false;
        if (response.data.codigo === 10) {
          UIkit.notify("<i class='uk-icon-check'></i> Lista eliminada!", {
            status: 'success',
            timeout: 5000,
            pos: 'top-center'
          });
        } else {
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId,
            response.data.errores
          );
        }
      });
    };
    $scope.ver = function(lista) {
      UIkit.modal('#mdlVerElementosLista').show();
      $scope.cargandoSublista = true;
      $scope.listaActual = lista;

      FebosAPI.cl_listar_elementos_lista(
        {
          empresaId: SesionFebos().empresa.id,
          listaId: lista.listaId
        },
        {},
        true,
        false
      ).then(function success(response) {
        console.log('subelementos', response);
        $scope.subelementos = response.data.elementos;
        try {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        } catch (e) {}
        $scope.cargandoSublista = false;
        if (response.data.codigo === 10) {
        } else {
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId,
            response.data.errores
          );
        }
      });
    };

    $scope.agregar = function() {
      if ($scope.nuevaLlave == '' || $scope.nuevoValor == '') {
        UIkit.modal.alert(
          'Debes ingresar una llave única y un valor asociado para poder agregar el elemento'
        );
        return;
      }
      var elemento = {
        llave: $scope.nuevaLlave,
        valor: $scope.nuevoValor
      };
      $scope.subelementosWorking.push(elemento);
      $scope.nuevaLlave = '';
      $scope.nuevoValor = '';
      console.log($scope.subelementosWorking, $scope.nuevaLlave, $scope.nuevoValor);
      $('#llave').focus();
    };
    $scope.eliminarFila = function(index) {
      $scope.subelementosWorking.splice(index, 1);
    };
    $scope.aplicar = function() {
      modal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Modificando lista...<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
      );

      FebosAPI.cl_modificar_lista(
        {
          empresaId: SesionFebos().empresa.id,
          listaId: $scope.listaActual.listaId
        },
        { elementos: $scope.subelementosWorking },
        true,
        false
      ).then(function success(response) {
        console.log(response);
        modal.hide();
        if (response.data.codigo === 10) {
          UIkit.notify("<i class='uk-icon-check'></i> Lista actualizada!", {
            status: 'success',
            timeout: 5000,
            pos: 'top-center'
          });
          $scope.subelementos = $scope.subelementosWorking;
          UIkit.modal('#mdlVerElementosLista').show();
        } else {
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId,
            response.data.errores
          );
        }
      });
    };

    $scope.buscarElementos = function(item) {
      if ($scope.query == '') return true;
      if (
        item.valor.toLowerCase().indexOf($scope.query.toLowerCase()) >= 0 ||
        item.llave.toLowerCase().indexOf($scope.query.toLowerCase()) >= 0
      ) {
        return true;
      }
      return false;
    };
    $scope.buscarElementosB = function(item) {
      $scope.queryB = $('#query').val();
      if ($scope.queryB == '')
        ////console.log("QUERY B: "+$scope.queryB,item,true);
        return true;
      if (
        $scope.queryB != null &&
        (item.valor.toLowerCase().indexOf($scope.queryB.toLowerCase()) >= 0 ||
          item.llave.toLowerCase().indexOf($scope.queryB.toLowerCase()) >= 0)
      ) {
        ////console.log("QUERY B: "+$scope.queryB,item,true);
        return true;
      }
      ////console.log("QUERY B: "+$scope.queryB,item,false);
      return false;
    };
    $scope.b64toBlob = function(b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;
      var byteCharacters = atob(b64Data);
      var byteArrays = [];
      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, { type: contentType });
      return blob;
    };
  }
]);
