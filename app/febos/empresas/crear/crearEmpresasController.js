angular.module('febosApp').controller('crearCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  'febosSingleton',
  '$http',
  'utils',
  '$state',
  '$location',
  '$cookies',
  'AYUDA',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    febosSingleton,
    $http,
    utils,
    $state,
    $location,
    $cookies,
    AYUDA
  ) {
    $scope.AYUDA = AYUDA;
    var $crear_card = $('#crear_card');
    var $crear_form = $('#crear_form');
    var $crear_error = $('#crear_error');
    var $crear_ok = $('#crear_ok');

    var crear_form_show = function() {
      $crear_form
        .show()
        .siblings()
        .hide();
    };

    var crear_error_show = function() {
      $crear_error
        .show()
        .siblings()
        .hide();
    };

    var crear_ok_show = function() {
      $crear_ok
        .show()
        .siblings()
        .hide();
    };

    $scope.backToCrear = function($event) {
      $event.preventDefault();
      utils.card_show_hide($crear_form, undefined, crear_form_show, undefined);
    };

    $scope.datos = {
      empresa_iut: '',
      empresa_razon: '',
      empresa_fantasia: '',
      textoCrear: 'CREAR',
      mensaje_error: '',
      mensaje_ok: ''
    };

    $scope.crear = function() {
      if ($scope.datos.textoCrear != 'CREAR') return;
      $scope.datos.textoCrear = 'Creando ...';

      req = {
        method: 'POST',
        url: febosSingleton.api + '/empresa',
        headers: {
          'Content-Type': 'application/json',
          empresa: '900989777',
          grupo: '1',
          debug: 'si',
          simular: 'si',
          token: '4887165a-98be-41e7-8b02-2ea6f2e6dec0'
        },
        data: {
          iut: $scope.datos.empresa_iut,
          nombre: $scope.datos.empresa_razon,
          alias: $scope.datos.empresa_fantasia
        }
      };

      $http(req).then(
        function(response) {
          console.log(response);
          try {
            if (response.data.codigo == 10) {
              $scope.datos.textoCrear = 'CREAR';
              $scope.datos.mensaje_ok = response.data.mensaje;
              utils.card_show_hide($crear_form, undefined, crear_ok_show, undefined);
            } else {
              $scope.datos.textoCrear = 'CREAR';
              $scope.datos.mensaje_error = response.data.mensaje;
              utils.card_show_hide($crear_form, undefined, crear_error_show, undefined);
            }
          } catch (e) {
            console.log(e);
            $scope.datos.textoCrear = 'CREAR';
            $scope.datos.mensaje_error = response.data.mensaje;
            utils.card_show_hide($crear_form, undefined, crear_error_show, undefined);
          }
        },
        function(response) {
          console.log(response);
          $scope.datos.textoCrear = 'CREAR';
          $scope.datos.mensaje_error = response.data.mensaje;
          utils.card_show_hide($crear_form, undefined, crear_error_show, undefined);
        }
      );
    };
  }
]);
