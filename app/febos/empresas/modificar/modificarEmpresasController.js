angular.module('febosApp').controller('modificarEmpresaCtrl', [
  '$rootScope',
  '$stateParams',
  '$scope',
  '$interval',
  '$timeout',
  '$http',
  'utils',
  '$state',
  '$location',
  'FebosAPI',
  'SesionFebos',
  function(
    $rootScope,
    $stateParams,
    $scope,
    $interval,
    $timeout,
    $http,
    utils,
    $state,
    $location,
    FebosAPI,
    SesionFebos
  ) {
    $scope.app = $stateParams.app;

    var $modificar_card = $('#modificar_card');
    var $modificar_form = $('#modificar_form');
    var $modificar_error = $('#modificar_error');
    var $modificar_ok = $('#modificar_ok');

    var modificar_form_show = function() {
      $modificar_form
        .show()
        .siblings()
        .hide();
    };

    var modificar_error_show = function() {
      $modificar_error
        .show()
        .siblings()
        .hide();
    };

    var modificar_ok_show = function() {
      $modificar_ok
        .show()
        .siblings()
        .hide();
    };

    $scope.backToModificar = function($event) {
      $event.preventDefault();
      utils.card_show_hide($modificar_form, undefined, crear_form_show, undefined);
    };
    $scope.permiso = {};
    $scope.holding_config = {
      plugins: {
        remove_button: {
          label: ''
        }
      },
      maxItems: 1,
      valueField: 'id',
      labelField: 'nombre',
      searchField: 'id',
      create: false,
      render: {
        option: function(permiso, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(permiso.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(permiso, escape) {
          return '<div class="item">' + escape(permiso.nombre) + '</div>';
        }
      }
    };
    $scope.tipo_cuenta_options = [
      { id: '1', nombre: 'Cuenta Corriente' },
      { id: '2', nombre: 'Cuenta Vista' },
      { id: '3', nombre: 'Ahorro' }
    ];

    $scope.banco_options = [
      { id: '1', nombre: 'BANCO DE CHILE - EDWARDS' },
      { id: '2', nombre: 'BANCO BANCO BICE' },
      { id: '3', nombre: 'BANCO CONSORCIO' },
      { id: '4', nombre: 'BANCO DEL ESTADO DE CHILE' },
      { id: '5', nombre: 'BANCO DO BRASIL S.A.' },
      { id: '6', nombre: 'BANCO FALABELLA' },
      { id: '7', nombre: 'BANCO INTERNACIONAL' },
      { id: '8', nombre: 'BANCO PARIS' },
      { id: '9', nombre: 'BANCO RIPLEY' },
      { id: '10', nombre: 'BANCO SANTANDER' },
      { id: '11', nombre: 'BANCO SECURITY' },
      { id: '12', nombre: 'BBVA (BCO BILBAO VIZCAYA ARG)' },
      { id: '13', nombre: 'BCI (BCO DE CREDITO E INV)' },
      { id: '14', nombre: 'COOPEUCH' },
      { id: '15', nombre: 'HSBC BANK' },
      { id: '16', nombre: 'ITAU CHILE' },
      { id: '17', nombre: 'ITAU - CORPBANCA' },
      { id: '18', nombre: 'SCOTIABANK' }
    ];
    $scope.datos = {
      empresa_iut: '',
      empresa_razon: '',
      empresa_fantasia: '',
      empresa_direccion: '',
      empresa_ciudad: '',
      empresa_comuna: '',
      empresa_telefono: '',
      empresa_acteco: '',
      empresa_correo: '',
      empresa_clave_sii: '',
      textoModificar: 'MODIFICAR',
      modificar_error: '',
      modificar_ok: '',
      cuentas: []
    };

    $scope.loadingCuentas = true;
    $scope.sucursalMatriz = '';
    $scope.loadingModificarPerfil = true;

    $scope.mostrarBotonVolverEmpresaPartner = function() {
      if (SesionFebos().partner == null || SesionFebos().partner == undefined) {
        return false;
      } else {
        return true;
      }
    };

    $scope.volverAEmpresaPartner = function() {
      SesionFebos().empresa = SesionFebos().partner;
      SesionFebos().partner = null;
      //console.log(SesionFebos());
      var urlActual = window.location.href;
      window.location.href = urlActual.replace('empresas/perfil', 'partners');
    };

    $scope.buscarInfoEmpresa = function() {
      console.log('BUSCANDO INFO DE EMPRESA');

      $scope.empresaPartner = SesionFebos().partner;
      FebosAPI.cl_info_empresa(
        {
          empresaId: SesionFebos().empresa.iut,
          debug: '',
          simular: '',
          token: SesionFebos().token,
          empresa: SesionFebos().empresa.iut,
          grupo: SesionFebos().grupo.id
        },
        {},
        true,
        false
      ).then(function(response) {
        $scope.loadingModificarPerfil = false;

        $scope.datos.empresa_acteco = response.data.acteco;
        $scope.datos.empresa_ciudad = response.data.ciudad;
        $scope.datos.empresa_comuna = response.data.comuna;
        $scope.datos.empresa_correo = response.data.correoElectronico;
        $scope.datos.empresa_direccion = response.data.direccion;
        $scope.datos.empresa_fantasia = response.data.fantasia;
        $scope.datos.empresa_iut = response.data.iut;
        $scope.datos.empresa_razon = response.data.razonSocial;
        $scope.datos.empresa_telefono = response.data.telefono;
        $scope.datos.fechaResolucion = response.data.fechaResolucion;
        $scope.datos.numeroResolucion = response.data.numeroResolucion;
        $scope.datos.giro = response.data.giro;
        $scope.datos.nombreRepLegal = response.data.nombreRepLegal;
        $scope.datos.rutRepLegal = response.data.rutRepLegal;
        $scope.datos.esPyme = response.data.esPyme == 'si' ? true : false;

        if (response.data.claveSiiConfigurada) {
          $scope.datos.empresa_clave_sii = '******';
        }

        for (var i = 0; i < response.data.sucursales.length; i++) {
          if (response.data.sucursales[i].casaMatriz) {
            $scope.sucursalMatriz = response.data.sucursales[i].id;
            $scope.datos.empresa_direccion = response.data.sucursales[i].direccion;
            $scope.datos.empresa_telefono = response.data.sucursales[i].telefono;
            $scope.datos.empresa_correo = response.data.sucursales[i].correoElectronico;
            $scope.datos.empresa_ciudad = response.data.sucursales[i].ciudad;
            $scope.datos.empresa_comuna = response.data.sucursales[i].comuna;
            console.log($scope.datos);
          }
        }
      });

      FebosAPI.cl_listar_cuentas(
        {
          empresaId: SesionFebos().empresa.iut
        },
        true,
        false
      ).then(function(response) {
        $scope.loadingCuentas = false;

        for (var i = 0; i < response.data.empresasCuentas.length; i++) {
          $scope.datos.cuentas.push({
            banco: response.data.empresasCuentas[i].banco,
            tipo_cuenta: response.data.empresasCuentas[i].tipoCuenta,
            nro_cuenta: response.data.empresasCuentas[i].numeroCuenta,
            mail_notificacion: response.data.empresasCuentas[i].mailNotificacion,
            id_cuenta: response.data.empresasCuentas[i].idEmpresaCuentaId
          });
        }
      });
      $scope.cargarPermisos();
    };

    $scope.esNumero = function(cadena) {
      return /^\d+$/.test(cadena);
    };

    function validarRut(rut) {
      // Despejar Puntos
      var valor = rut.replace('.', '');
      // Despejar Guión
      valor = valor.replace('-', '');

      // Aislar Cuerpo y Dígito Verificador
      var cuerpo = valor.slice(0, -1);
      var dv = valor.slice(-1).toUpperCase();

      // Formatear RUN
      rut = cuerpo + '-' + dv;

      // Si no cumple con el mínimo ej. (n.nnn.nnn)
      if (cuerpo.length < 7) {
        console.log('RUT Incompleto');
        return false;
      }

      // Calcular Dígito Verificador
      var suma = 0;
      var multiplo = 2;

      // Para cada dígito del Cuerpo
      for (i = 1; i <= cuerpo.length; i++) {
        // Obtener su Producto con el Múltiplo Correspondiente
        var index = multiplo * valor.charAt(cuerpo.length - i);

        // Sumar al Contador General
        suma = suma + index;

        // Consolidar Múltiplo dentro del rango [2,7]
        if (multiplo < 7) {
          multiplo = multiplo + 1;
        } else {
          multiplo = 2;
        }
      }

      // Calcular Dígito Verificador en base al Módulo 11
      var dvEsperado = 11 - (suma % 11);

      // Casos Especiales (0 y K)
      dv = dv === 'K' ? 10 : dv;
      dv = dv === 0 ? 11 : dv;

      // Validar que el Cuerpo coincide con su Dígito Verificador
      if (dvEsperado != dv) {
        return false;
      }

      // Si todo sale bien, eliminar errores (decretar que es válido)
      // rut.setCustomValidity('');
      return true;
    }

    // -- función que valida formato correo
    function isEmail(mail) {
      var estado = 1;

      if (
        !/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/.test(
          mail
        )
      ) {
        estado = 0;
      }

      return estado;
    }

    $scope.guardarModificacion = function() {
      var requestGuardarListos = 0;

      $scope.datos.textoModificar = 'Modificando ...';

      $scope.datos.esPyme;

      var errores = '';

      if ($scope.datos.empresa_razon == '') {
        errores += '\nDebe ingresar la razón social.';
      }
      if ($scope.datos.empresa_fantasia == '') {
        errores += '\nDebe ingresar nombre de fantasía';
      }
      if ($scope.datos.empresa_acteco == '') {
        errores += '\nDebe ingresar el ACTECO';
      }
      if ($scope.datos.numeroResolucion == '') {
        errores += '\nDebe ingresar número de resolución';
      }

      if ($scope.datos.fechaResolucion == '') {
        errores += '\nDebe ingresar fecha de resolución';
      }

      if ($scope.datos.empresa_clave_sii == '') {
        errores += '\nDebe ingresar la clave del SII';
      }

      if (!$scope.esNumero($scope.datos.empresa_acteco)) {
        errores += '\nEl código de actividad económica debe ser numérico. ';
      }
      if (!$scope.esNumero($scope.datos.numeroResolucion)) {
        errores += '\nEl numero de resolución debe ser numérico ';
      }

      if ($scope.datos.rutRepLegal) {
        if (!validarRut($scope.datos.rutRepLegal)) {
          errores += '\nEl rut del representante legal no es válido';
        }
      }

      if (errores != '') {
        $scope.datos.textoModificar = 'MODIFICAR';
        UIkit.modal.alert('Errores encontrados:' + errores);
        return false;
      }

      var body = {
        razonSocial: $scope.datos.empresa_razon,
        fantasia: $scope.datos.empresa_fantasia,
        acteco: $scope.datos.empresa_acteco,
        giro: $scope.datos.giro,
        rutRepLegal: $scope.datos.rutRepLegal,
        nombreRepLegal: $scope.datos.nombreRepLegal,
        fechaResolucion: $scope.datos.fechaResolucion,
        numeroResolucion: $scope.datos.numeroResolucion,
        claveSii: btoa($scope.datos.empresa_clave_sii),
        esPyme: $scope.datos.esPyme == true ? 'si' : 'no'
      };
      FebosAPI.cl_actualizar_empresa(
        {
          empresaId: SesionFebos().empresa.id,
          debug: '',
          simular: '',
          token: SesionFebos().token,
          empresa: SesionFebos().empresa.iut,
          grupo: SesionFebos().grupo.id
        },
        body,
        true,
        false
      )
        .then(function(response) {
          console.log(response);
          requestGuardarListos++;
          console.log('req1uestGuardarListos', requestGuardarListos);

          if (requestGuardarListos == 2) {
            UIkit.notify("<i class='uk-icon-check'></i> Perfil modificado!", {
              status: 'success',
              timeout: 5000,
              pos: 'top-center'
            });
          }
        })
        .finally(function() {
          $scope.datos.textoModificar = 'MODIFICAR';
        });
      //guarda modificacion de la sucursal

      if ($scope.datos.empresa_direccion == '') {
        errores += '\nDebe ingresar la dirección';
      }

      if ($scope.datos.empresa_correo == '') {
        errores += '\nDebe ingresar la dirección de correo';
      }

      if (
        isEmail($scope.datos.empresa_correo) == 0 ||
        isEmail($scope.datos.empresa_correo) == '0'
      ) {
        errores += '\nEl mail de la empresa no tiene el formato correcto';
      }

      if ($scope.datos.empresa_ciudad == '') {
        errores += '\nDebe ingresar la ciudad';
      }

      if ($scope.datos.empresa_comuna == '') {
        errores += '\nDebe ingresar la comuna';
      }

      if (errores != '') {
        $scope.datos.textoModificar = 'MODIFICAR';
        UIkit.modal.alert('Errores encontrados:' + errores);
        return false;
      }

      var body2 = {
        direccion: $scope.datos.empresa_direccion,
        correoElectronico: $scope.datos.empresa_correo,
        telefono: $scope.datos.empresa_telefono,
        ciudad: $scope.datos.empresa_ciudad,
        comuna: $scope.datos.empresa_comuna
      };
      FebosAPI.cl_modificar_sucursal(
        {
          empresaId: SesionFebos().empresa.id,
          sucursalId: $scope.sucursalMatriz,
          simular: 'no'
        },
        body2,
        true,
        false
      ).then(function(response) {
        requestGuardarListos++;
        console.log('req2uestGuardarListos:', requestGuardarListos);

        if (requestGuardarListos == 2) {
          $scope.datos.textoModificar = 'MODIFICAR';
          UIkit.notify("<i class='uk-icon-check'></i> Perfil modificado!", {
            status: 'success',
            timeout: 5000,
            pos: 'top-center'
          });
        }
      });
    };
    // --
    $scope.agregar = function() {
      $scope.datos.cuentas.push({
        banco: $scope.datos.cuentas.banco_insertar,
        tipo_cuenta: $scope.datos.cuentas.tipo_cuenta_insertar,
        nro_cuenta: $scope.datos.cuentas.nro_cuenta_insertar,
        mail_notificacion: $scope.datos.cuentas.mail_notificacion_insertar
      });

      var body = {
        banco: $scope.datos.cuentas.banco_insertar,
        tipoCuenta: $scope.datos.cuentas.tipo_cuenta_insertar,
        nroCuenta: $scope.datos.cuentas.nro_cuenta_insertar,
        mailNotificacion: $scope.datos.cuentas.mail_notificacion_insertar
      };
      FebosAPI.cl_crear_cuenta(
        {
          empresaId: SesionFebos().empresa.iut
        },
        body,
        true,
        false
      ).then(function(response) {
        console.log(response);
      });

      $scope.datos.cuentas.banco_insertar = '';
      $scope.datos.cuentas.tipo_cuenta_insertar = '';
      $scope.datos.cuentas.nro_cuenta_insertar = '';
      $scope.datos.cuentas.mail_notificacion_insertar = '';
    };
    // --
    // --
    $scope.eliminar = function(idx, cuenta) {
      console.log('REQUEST cl_eliminar_cuenta:' + cuenta.id_cuenta);

      UIkit.modal.confirm(
        'Esta seguro que desea eliminar la cuenta número <b>' + cuenta.nro_cuenta + '</b>?',
        function() {
          $scope.datos.cuentas.splice(idx, 1);
          FebosAPI.cl_eliminar_cuenta(
            {
              empresaId: SesionFebos().empresa.iut,
              idCuenta: cuenta.id_cuenta
            },
            true,
            false
          ).then(function(response) {
            console.log(response);

            UIkit.notify("<i class='uk-icon-check'></i> Lista eliminada!", {
              status: 'success',
              timeout: 5000,
              pos: 'top-center'
            });
          });
        },
        { labels: { Ok: 'Si, eliminar', Cancel: 'No, me acabo de arrepentir' } }
      );
    };

    $scope.modificarPermisos = function() {
      function asignarConfiguracion(item) {
        item.ultimaActualizacion = '';
        switch (item.parametroId) {
          case 'permiso.mipyme.descargar.dte':
            item.cambio = 'si'; //Siempre si
            item.valor = $scope.permiso.descargardte == true ? 'si' : 'no';
            break;
          default:
            item.cambio = 'no';
            break;
        }
      }

      var configuraciones = JSON.parse(JSON.stringify($scope.permiso.configuraciones));
      configuraciones.forEach(asignarConfiguracion);
      console.log(configuraciones);
      var cuerpoRequest = {
        origen: 'pyme_permisos',
        configuraciones: configuraciones
      };
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>" +
          '' +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_adm_configuracion_guardar({}, cuerpoRequest, true, true)
        .then(function(response) {
          console.log('?? === >>>>  ', response);
          $scope.permiso.configuraciones = response.data.configuraciones;
          $scope.cargarConfiguracionesActual($scope.permiso.configuraciones);
        })
        .catch(function(reason) {
          console.log('error', reason);
        })
        .finally(function() {});
    };
    $scope.cargarPermisos = function() {
      FebosAPI.cl_adm_configuracion_listar({ parametros: 'permiso.mipyme.descargar.dte' }).then(
        function(response) {
          if (response.data.codigo == 10 && response.data.configuraciones.length > 0) {
            $scope.permiso.configuraciones = response.data.configuraciones;
            $scope.cargarConfiguracionesActual($scope.permiso.configuraciones);
          } else {
            console.log('ERROR CARGANDO PERMISOS');
          }
        }
      );
    };
    $scope.cargarConfiguracionesActual = function(configuraciones) {
      function asignarConfiguracion(item, index) {
        switch (item.parametroId) {
          case 'permiso.mipyme.descargar.dte':
            $scope.permiso.descargardte = item.valor == 'si' ? true : false; //permiso_descargar_dte
            break;
        }
      }

      configuraciones.forEach(asignarConfiguracion);
    };
    // --
    $scope.buscarInfoEmpresa();
  }
]);
