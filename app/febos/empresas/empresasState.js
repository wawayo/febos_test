febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.
                state("restringido.perfil_empresa", {
                            url: "/:app/empresas/perfil",
                            templateUrl: 'app/febos/empresas/modificar/modificarEmpresasView.html',
                            controller: 'modificarEmpresaCtrl',
                            resolve: {
                                
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        'lazy_KendoUI',
                                        'lazy_uikit',
                                        'lazy_iCheck',
                                        'app/febos/empresas/modificar/modificarEmpresasController.js'
                                    ]);
                                }]
                            }
                        })
                    .state("restringido.empresas", {
                        url: "/:app/empresas",
                        templateUrl: 'app/febos/empresas/gestionEmpresasView.html',
                        controller: 'gestionEmpresasCtrl',
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        'lazy_uikit',
                                        'lazy_iCheck',
                                        'app/febos/empresas/gestionEmpresasController.js'
                                    ]);
                                }]
                        }
                    })                        

            }]);