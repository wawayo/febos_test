angular
  .module('febosApp')
  .controller('gestionEmpresasCtrl', function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    $http,
    $state,
    utils,
    FebosAPI,
    SesionFebos
  ) {
    var $crear_card = $('#crear_card');
    var $crear_form = $('#crear_form');
    var $crear_error = $('#crear_error');
    var $crear_ok = $('#crear_ok');

    var $modificar_card = $('#modificar_card');
    var $modificar_form = $('#modificar_form');
    var $modificar_error = $('#modificar_error');
    var $modificar_ok = $('#modificar_ok');

    var $crear_cardSu = $('#crear_cardSu');
    var $crear_formSu = $('#crear_formSu');
    var $crear_errorSu = $('#crear_errorSu');
    var $crear_okSu = $('#crear_okSu');

    var $crear_cardCaja = $('#crear_cardCaja');
    var $crear_formCaja = $('#crear_formCaja');
    var $crear_errorCaja = $('#crear_errorCaja');
    var $crear_okCaja = $('#crear_okCaja');

    var $modificar_cardSu = $('#modificar_cardSu');
    var $modificar_formSu = $('#modificar_formSu');
    var $modificar_errorSu = $('#modificar_errorSu');
    var $modificar_okSu = $('#modificar_okSu');

    var $modificar_cardCaja = $('#modificar_cardCaja');
    var $modificar_formCaja = $('#modificar_formCaja');
    var $modificar_errorCaja = $('#modificar_errorCaja');
    var $modificar_okCaja = $('#modificar_okCaja');

    var crear_form_show = function() {
      $crear_form
        .show()
        .siblings()
        .hide();
      $crear_formSu
        .show()
        .siblings()
        .hide();
      $modificar_formSu
        .show()
        .siblings()
        .hide();
      $crear_formCaja
        .show()
        .siblings()
        .hide();
      $modificar_formCaja
        .show()
        .siblings()
        .hide();
    };

    var crear_error_show = function() {
      $crear_error
        .show()
        .siblings()
        .hide();
      $crear_errorSu
        .show()
        .siblings()
        .hide();
      $modificar_errorSu
        .show()
        .siblings()
        .hide();
      $crear_errorCaja
        .show()
        .siblings()
        .hide();
      $modificar_errorCaja
        .show()
        .siblings()
        .hide();
    };

    var crear_ok_show = function() {
      $crear_ok
        .show()
        .siblings()
        .hide();
      $crear_okSu
        .show()
        .siblings()
        .hide();
      $modificar_okSu
        .show()
        .siblings()
        .hide();
      $crear_okCaja
        .show()
        .siblings()
        .hide();
      $modificar_okCaja
        .show()
        .siblings()
        .hide();
    };

    $scope.backToCrear = function($event) {
      if ($event) {
        console.log('backToCrear -> $event: ', $event);
        $event.preventDefault();
      }

      utils.card_show_hide($crear_form, undefined, crear_form_show, undefined);
      utils.card_show_hide($crear_formSu, undefined, crear_form_show, undefined);
      utils.card_show_hide($modificar_formSu, undefined, crear_form_show, undefined);
      utils.card_show_hide($crear_formCaja, undefined, crear_form_show, undefined);
      utils.card_show_hide($modificar_formCaja, undefined, crear_form_show, undefined);
    };

    // -- GESTIÓN EMPRESA --
    $scope.filtros = [];
    $scope.filtros.paginas = [];
    $scope.filtros.empresa_rut = '';
    $scope.filtros.empresa_razon = '';
    $scope.filtros.empresa_fantasia = '';

    $scope.filtros.pagina = '1';
    $scope.filtros.itemsPorPagina = '10';
    $scope.filtros.numeroDePaginas = 9; //< número de páginas máximo mostradas en la parte de cambio de página empresas

    $scope.filtrox = [];
    $scope.filtrox.paginas = [];
    $scope.filtrox.departamento = '';
    $scope.filtrox.subdivision = '';
    $scope.filtrox.rutEmpresa = '';
    $scope.filtrox.pagina = 1;
    $scope.filtrox.itemsPorPagina = 10;
    $scope.filtrox.totalElementos = 0;
    $scope.filtrox.totalPaginas = 0;
    $scope.filtrox.numeroDePaginas = 9; //< número de páginas máximo mostradas en la parte de cambio de página sucursales

    $scope.totalElementos = 0;
    $scope.totalPaginas = 0;
    $scope.index_empresa_eliminar = '';

    $scope.cargando = false; //< para el loader

    $scope.empresas = new Array(); //< arreglo de empresas

    // -- función que lista las empresas
    $scope.listar = function() {
      $scope.cargando = true;
      $scope.empresas = [];

      var query = {
        pagina: $scope.filtros.pagina,
        filas: $scope.filtros.itemsPorPagina,
        filtro:
          $scope.filtros.empresa_fantasia.replace(/\,/g, '') +
          ',' +
          $scope.filtros.empresa_razon.replace(/\,/g, '') +
          ',' +
          $scope.filtros.empresa_rut.replace(/\,/g, ''),
        simular: 'no'
      };
      FebosAPI.cl_listar_empresas(query, true, false).then(function(response) {
        $scope.cargando = false;
        if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
          $scope.empresas = [];

          for (var i = 0; i < response.data.empresas.length; i++) {
            row = {
              id: '',
              iut: '',
              razon: '',
              fantasia: '',
              regimen: '',
              holdingId: '',
              esEmisor: '',
              esReceptor: '',
              esCanal: '',
              canal: '',
              esRcof: '',
              acteco: '',
              tieneReglas: ''
            };

            row.id = response.data.empresas[i].id;
            row.iut = response.data.empresas[i].iut;
            row.razon = response.data.empresas[i].razonSocial;
            row.fantasia = response.data.empresas[i].fantasia;
            row.regimen = response.data.empresas[i].regimen;
            row.holdingId = response.data.empresas[i].holdingId;
            row.esEmisor = response.data.empresas[i].esEmisor;
            row.esReceptor = response.data.empresas[i].esReceptor;
            row.esCanal = response.data.empresas[i].esCanal;
            row.canal = response.data.empresas[i].canal;
            row.esRcof = response.data.empresas[i].esRcof;
            row.acteco = response.data.empresas[i].acteco;
            row.tieneReglas = response.data.empresas[i].reglasNegocio;
            $scope.empresas.push(row);
          }

          if (
            response.data.totalElementos == undefined ||
            typeof response.data.totalElementos == 'undefined'
          ) {
            $scope.totalElementos = 0;
          } else {
            $scope.totalElementos = parseInt(response.data.totalElementos);
          }

          $scope.calcularPaginas();
        }
      });
    };
    // --

    // -- función que permite cambiar de página en un mantenedor
    $scope.cambiarPagina = function(index) {
      $scope.filtros.numeroDePaginas = Math.ceil($scope.filtros.numeroDePaginas);
      $scope.totalElementos = Math.ceil($scope.totalElementos);

      if (index == 'inicio') {
        $scope.filtros.pagina = 1;
      } else {
        if (index == 'final') {
          $scope.filtros.pagina = $scope.totalPaginas;
        } else {
          if (
            index !== '' &&
            (index != undefined || typeof index != 'undefined') &&
            ($scope.filtros.paginas[index] != undefined ||
              typeof $scope.filtros.paginas[index] != 'undefined')
          ) {
            $scope.filtros.pagina = $scope.filtros.paginas[index].numero;
          } else {
            $scope.filtros.pagina = 1;
          }
        }
      }

      $scope.listar(); //< llama función listar empresas, para mostrar data
    };
    // --

    // -- función que calcula las páginas a mostrar en el mantenedor
    $scope.calcularPaginas = function() {
      $scope.totalPaginas = Math.ceil($scope.totalElementos / $scope.filtros.itemsPorPagina);

      var resta = -(Math.ceil($scope.filtros.numeroDePaginas) % 2 == 0
        ? Math.ceil($scope.filtros.numeroDePaginas / 2 - 1)
        : Math.ceil($scope.filtros.numeroDePaginas / 2 - 1));
      var suma =
        Math.ceil($scope.filtros.numeroDePaginas) % 2 == 0
          ? Math.ceil($scope.filtros.numeroDePaginas / 2)
          : Math.ceil($scope.filtros.numeroDePaginas / 2 - 1);
      var first = false;

      if (Math.ceil($scope.filtros.pagina) + resta <= 0) {
        first = true;
        suma -= Math.ceil($scope.filtros.pagina) + (resta - 1);
        resta -= Math.ceil($scope.filtros.pagina) + (resta - 1);
      }

      if ($scope.totalPaginas <= 0) suma = $scope.filtros.numeroDePaginas + resta - 1;

      if (
        Math.ceil($scope.filtros.pagina) + suma > $scope.totalPaginas &&
        $scope.totalPaginas > 0
      ) {
        if (first) {
          suma -= Math.ceil($scope.filtros.pagina + suma) - $scope.totalPaginas;
        } else {
          suma -= Math.ceil($scope.filtros.pagina + suma) - $scope.totalPaginas;
        }
      }

      var cont = 0;
      $scope.filtros.paginas = [];

      for (var i = $scope.filtros.pagina + resta; i <= $scope.filtros.pagina + suma; i++) {
        var nuevaPagina = {
          id: cont,
          numero: i
        };

        $scope.filtros.paginas.push(nuevaPagina);
        cont++;
      }
    };
    // --
    $scope.holding_config = {
      plugins: {
        remove_button: {
          label: ''
        }
      },
      maxItems: 1,
      valueField: 'id',
      labelField: 'nombre',
      searchField: 'id',
      create: false,
      render: {
        option: function(permiso, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(permiso.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(permiso, escape) {
          return '<div class="item">' + escape(permiso.nombre) + '</div>';
        }
      }
    };
    $scope.canal_options = [];
    $scope.holding_options = [];

    $scope.loadingCrearEmpresa = false;
    // -- función que activa el modal para crear una nueva empresa
    $scope.crearEmpresa = function() {
      $scope.backToCrear();
      console.log('modal crear');
      $scope.loadingCrearEmpresa = true;

      $scope.datox = {
        id: '',
        iut: '',
        razon: '',
        fantasia: '',
        holdingId: '1',
        codigoSii: '',
        direccion: '',
        municipioId: 'Armenia',
        telefono: '',
        correo: '',
        regimen: '',
        textoCrear: 'CREAR',
        crear_error: '',
        crear_ok: '',
        textoModificar: 'MODIFICAR'
      };

      var modal = UIkit.modal('#mdCrearEmpresa');
      modal.show();

      var responsesListos = 0;
      /// obtiene empresas holding
      var query = {
        pagina: 1,
        filas: 9000,
        filtro: '',
        simular: 'no'
      };
      FebosAPI.cl_listar_empresas(query, true, false).then(function(response) {
        responsesListos++;
        if (responsesListos == 2) $scope.loadingCrearEmpresa = false;
        console.log(response);
        for (var i = 0; i < response.data.totalElementos; i++) {
          // console.log(i, {id: response.data.empresas[i].id, title: response.data.empresas[i].razonSocial});
          $scope.holding_options.push({
            id: response.data.empresas[i].id,
            nombre: response.data.empresas[i].razonSocial
          });
        }
      });

      /// obtiene empresas canales
      FebosAPI.cl_listar_empresas_canales({}, true, false).then(function(response) {
        responsesListos++;
        if (responsesListos == 2) $scope.loadingCrearEmpresa = false;
        console.log(response);
        for (var i = 0; i < response.data.totalElementos; i++) {
          // console.log(i, {id: response.data.empresas[i].id, title: response.data.empresas[i].razonSocial});
          $scope.canal_options.push({
            id: response.data.empresas[i].id,
            nombre: response.data.empresas[i].razonSocial
          });
        }
      });
    };
    // --

    $scope.datox = {
      //< scope de la data que se incorporará en los input de creación de nueva empresa
      id: '',
      iut: '',
      razonSocial: '',
      fantasia: '',
      direccion: '',
      codigoSii: '',
      telefono: '',
      correo: '',
      empresa_regimen: '',
      textoCrear: 'CREAR',
      crear_error: '',
      crear_ok: '',
      textoModificar: 'MODIFICAR',
      ciudad: '',
      comuna: '',
      acteco: '',
      holding: '',
      canal: '',
      checkEmisor: '',
      checkReceptor: '',
      checkRcof: '',
      checkCanal: ''
    };

    $scope.limpiarHolding = function() {
      $('#holding')[0].selectize.clear();
    };

    // -- función que crea una nueva empresa
    $scope.crear = function() {
      if ($scope.datox.textoCrear != 'CREAR') return;
      if (!$scope.validarCrear()) return; //< validación de formulario, datos obligatorios

      $scope.datox.textoCrear = 'Creando ...';
      var body = {
        iut: $scope.datox.iut,
        razonSocial: $scope.datox.razonSocial,
        fantasia: $scope.datox.fantasia,
        direccion: $scope.datox.direccion,
        codigoSii: $scope.datox.codigoSii,
        ciudad: $scope.datox.ciudad,
        comuna: $scope.datox.comuna,
        telefono: $scope.datox.telefono,
        acteco: $scope.datox.acteco,
        correo: $scope.datox.correo,
        holding: $scope.datox.holding,
        canal: $scope.datox.canal,
        esEmisor: $scope.datox.checkEmisor ? 'si' : 'no',
        esReceptor: $scope.datox.checkReceptor ? 'si' : 'no',
        esRcof: $scope.datox.checkRcof ? 'si' : 'no',
        esCanal: $scope.datox.checkCanal ? 'si' : 'no',
        tieneReglas: $scope.datox.reglas ? 'si' : 'no'
      };
      FebosAPI.cl_crear_empresa({ simular: 'no' }, body, true, false).then(function(response) {
        $scope.datox.textoCrear = 'CREAR';
        if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
          var modal = UIkit.modal('#mdCrearEmpresa');
          modal.hide();
          $scope.datox.textoCrear = 'CREAR';
          console.log('OK CREADO');
          $scope.listar();
        }
      });
    };
    // --

    // -- función que valida formulario crear nueva empresa, valida campos obligatorios
    $scope.validarCrear = function() {
      var errores = '';

      if ($scope.datox.iut == '') {
        errores += '\nDebe ingresar un RUT obligatoriamente.';
      }

      if (!validarRut($scope.datox.iut)) {
        errores += '\nEl RUT ingresado no es válido.';
      }

      if ($scope.datox.razonSocial == '') {
        errores += '\nDebe ingresar una Razón Social.';
      }

      if ($scope.datox.fantasia == '') {
        errores += '\nDebe ingresar un nombre de Fantasia.';
      }

      if ($scope.datox.ciudad == '') {
        errores += '\nDebe ingresar una Ciudad.';
      }
      if ($scope.datox.comuna == '') {
        errores += '\nDebe ingresar una Comuna.';
      }

      if ($scope.datox.direccion == '') {
        errores += '\nDebe ingresar una Dirección.';
      }

      if ($scope.datox.correo == '') {
        errores += '\nDebe ingresar un Correo Electrónico.';
      }

      if (isEmail($scope.datox.correo) == 0) {
        errores += '\nEl Correo Electrónico ingresado no es válido.';
      }
      console.log('$scope.datox.checkEmisor ', $scope.datox.checkEmisor);
      console.log('$scope.datox.acteco ', $scope.datox.acteco);
      if (
        $scope.datox.checkEmisor &&
        ($scope.datox.acteco == '' || $scope.datox.acteco == undefined)
      ) {
        errores += '\nSi es emisor el ACTECO es obligatorio.';
      }

      if (errores != '') {
        UIkit.modal.alert('Errores encontrados:' + errores);
        return false;
      } else {
        return true;
      }
    };

    $scope.validarBotonCrear = function() {
      return !(
        !$scope.datox.iut ||
        !$scope.datox.razonSocial ||
        !$scope.datox.fantasia ||
        !$scope.datox.direccion ||
        !$scope.datox.correo ||
        !$scope.datox.ciudad ||
        !$scope.datox.canal
      );
    };

    $scope.validarModificar = function() {
      var errores = '';

      if ($scope.datax.empresa_ciudad == '') {
        errores += '\nDebe ingresar una Ciudad.';
      }
      if ($scope.datax.empresa_comuna == '') {
        errores += '\nDebe ingresar una Comuna.';
      }

      if ($scope.datax.empresa_razon == '') {
        errores += '\nDebe ingresar una Razón Social.';
      }

      if ($scope.datax.empresa_fantasia == '') {
        errores += '\nDebe ingresar un nombre de Fantasia.';
      }

      if ($scope.datax.empresa_direccion == '') {
        errores += '\nDebe ingresar una Dirección.';
      }

      if ($scope.datax.empresa_correo == '') {
        errores += '\nDebe ingresar un Correo Electrónico.';
      }

      if (isEmail($scope.datax.empresa_correo) == 0) {
        errores += '\nEl Correo Electrónico ingresado no es válido.';
      }

      if (
        $scope.datax.empresa_esEmisor &&
        ($scope.datax.empresa_acteco == '' || $scope.datax.empresa_acteco == undefined)
      ) {
        errores += '\nSi es emisor el ACTECO es obligatorio.';
      }

      if (errores != '') {
        UIkit.modal.alert('Errores encontrados:' + errores);
        return false;
      } else {
        return true;
      }
    };
    // --

    // -- función limpiar filtros
    $scope.limpiarFiltros = function() {
      $scope.filtros.empresa_rut = '';
      $scope.filtros.empresa_razon = '';
      $scope.filtros.empresa_fantasia = '';
    };
    // --

    $scope.loadingModificarEmpresa = false;
    $scope.sucursalMatriz = '';
    // -- función que activa modal con formulario con data de una empresa específica, para ser modificada
    $scope.modificarEmpresa = function(index) {
      $scope.backToCrear();
      $scope.loadingModificarEmpresa = true;

      $scope.datax = {
        //< scope de data para modificar
        empresa_id: '',
        empresa_iut: '',
        empresa_razon: '',
        empresa_fantasia: '',
        empresa_regimen: '',
        empresa_holdingId: [],
        empresa_esEmisor: '',
        empresa_esReceptor: '',
        empresa_esCanal: '',
        empresa_canal: [],
        empresa_esRcof: '',
        empresa_acteco: '',
        empresa_direccion: '',
        empresa_telefono: '',
        empresa_correo: '',
        empresa_ciudad: '',
        empresa_comuna: '',
        empresa_tieneReglas: '',
        modificar_error: '',
        modificar_ok: '',
        textoModificar: 'MODIFICAR'
      };

      $scope.datox.textoModificar = 'MODIFICAR';

      $scope.datax.empresa_id = $scope.empresas[index].id;
      $scope.datax.empresa_iut = $scope.empresas[index].iut;
      $scope.datax.empresa_razon = $scope.empresas[index].razon;
      $scope.datax.empresa_fantasia = $scope.empresas[index].fantasia;
      $scope.datax.empresa_regimen = $scope.empresas[index].regimen;
      $scope.datax.empresa_esEmisor = $scope.empresas[index].esEmisor;
      $scope.datax.empresa_esReceptor = $scope.empresas[index].esReceptor;
      $scope.datax.empresa_esCanal = $scope.empresas[index].esCanal;
      $scope.datax.empresa_esRcof = $scope.empresas[index].esRcof;
      $scope.datax.empresa_acteco = $scope.empresas[index].acteco;
      $scope.datax.empresa_tieneReglas = $scope.empresas[index].tieneReglas;

      console.log('EMPRESA ' + index);
      console.log($scope.empresas[index]);

      //comienzo selectize
      //holding selectize
      if (
        typeof $scope.empresas[index].holdingId != 'undefined' &&
        $scope.empresas[index].holdingId != ''
      ) {
        $scope.empresas.asignadasTemporal = [];
        $scope.empresas.asignadasTemporal.push($scope.empresas[index].holdingId);
        var idHolding = $scope.empresas.asignadasTemporal.splice(0, 1);
        console.log('id holding:');
        console.log(idHolding);
        $scope.datax.empresa_holdingId.push($scope.empresas[index].holdingId);
      }

      //canal selectize
      if (
        typeof $scope.empresas[index].canal != 'undefined' &&
        $scope.empresas[index].canal != ''
      ) {
        $scope.empresas.asignadasTemporal = [];
        $scope.empresas.asignadasTemporal.push($scope.empresas[index].canal);
        var idCanal = $scope.empresas.asignadasTemporal.splice(0, 1);
        console.log('id Canal:');
        console.log(idCanal);
        $scope.datax.empresa_canal.push($scope.empresas[index].canal);
      }

      var responsesListos = 0;
      /// obtiene empresas holding
      var query = {
        pagina: 1,
        filas: 9000,
        filtro: '',
        simular: 'no'
      };
      FebosAPI.cl_listar_empresas(query, true, false).then(function(response) {
        responsesListos++;
        console.log(response);
        for (var i = 0; i < response.data.totalElementos; i++) {
          // console.log(i, {id: response.data.empresas[i].id, title: response.data.empresas[i].razonSocial});
          $scope.holding_options.push({
            id: response.data.empresas[i].id,
            nombre: response.data.empresas[i].razonSocial
          });
        }
        if (responsesListos == 3) $scope.loadingModificarEmpresa = false;
      });

      /// obtiene empresas canales
      FebosAPI.cl_listar_empresas_canales({}, true, false).then(function(response) {
        responsesListos++;
        console.log(response);
        for (var i = 0; i < response.data.totalElementos; i++) {
          // console.log(i, {id: response.data.empresas[i].id, title: response.data.empresas[i].razonSocial});
          $scope.canal_options.push({
            id: response.data.empresas[i].id,
            nombre: response.data.empresas[i].razonSocial
          });
        }
        if (responsesListos == 3) $scope.loadingModificarEmpresa = false;
      });

      /// obtiene sucursales

      FebosAPI.cl_listar_sucursales(
        {
          empresaId: $scope.empresas[index].id,
          simular: 'no',
          debug: 'si',
          pagina: 1,
          filas: 9000,
          empresax: $scope.empresas[index].iut
        },
        true,
        false
      ).then(function(response) {
        responsesListos++;
        console.log(response);
        for (var i = 0; i < response.data.sucursales.length; i++) {
          if (response.data.sucursales[i].casaMatriz) {
            $scope.sucursalMatriz = response.data.sucursales[i].id;
            $scope.datax.empresa_direccion = response.data.sucursales[i].direccion;
            $scope.datax.empresa_telefono = response.data.sucursales[i].telefono;
            $scope.datax.empresa_correo = response.data.sucursales[i].correoElectronico;
            $scope.datax.empresa_ciudad = response.data.sucursales[i].ciudad;
            $scope.datax.empresa_comuna = response.data.sucursales[i].comuna;
            $scope.datax.codigoSii = response.data.sucursales[i].codigoSii;
          }
        }
        if (responsesListos == 3) $scope.loadingModificarEmpresa = false;
      });

      var modal = UIkit.modal('#mdModificarEmpresa');
      modal.show();
    };
    // --

    // -- función que envía la data a la API, para modificar una empresa
    $scope.guardarModificacion = function() {
      if (!$scope.validarModificar()) return; //< validación de campos obligatorios
      console.log($scope.datax);
      if ($scope.datox.textoModificar != 'MODIFICAR') return;

      $scope.datox.textoModificar = 'Modificando...';
      var id = $scope.datax.empresa_id;

      var requestListos = 0;

      var body = {
        razonSocial: $scope.datax.empresa_razon,
        fantasia: $scope.datax.empresa_fantasia,
        esEmisor: $scope.datax.empresa_esEmisor ? 'si' : 'no',
        esReceptor: $scope.datax.empresa_esReceptor ? 'si' : 'no',
        esRcof: $scope.datax.empresa_esRcof ? 'si' : 'no',
        esCanal: $scope.datax.empresa_esCanal ? 'si' : 'no',
        acteco: $scope.datax.empresa_acteco,
        tieneReglas: $scope.datax.empresa_tieneReglas ? 'si' : 'no'
      };

      console.log('empresa_holdingId', $scope.datax.empresa_holdingId);
      if (
        $scope.datax.empresa_holdingId != undefined &&
        $scope.datax.empresa_holdingId.length > 0
      ) {
        console.log('holding', $scope.datax.empresa_holdingId);
        body.holdingId = $scope.datax.empresa_holdingId;
      } else {
        body.holdingId = '';
      }
      if ($scope.datax.empresa_canal != undefined && $scope.datax.empresa_canal.length > 0) {
        console.log('canal', $scope.datax.empresa_canal);
        body.canal = $scope.datax.empresa_canal;
      } else {
        body.canal = '';
      }
      FebosAPI.cl_actualizar_empresa(
        {
          empresaId: id,
          simular: 'no'
        },
        body,
        true,
        false
      ).then(function(response) {
        $scope.datox.textoModificar = 'MODIFICAR';
        if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
          if ($scope.sucursalMatriz != '') {
            var query = {
              sucursalId: $scope.sucursalMatriz,
              empresaId: id,
              simular: 'no'
            };
            var body = {
              direccion: $scope.datax.empresa_direccion,
              correoElectronico: $scope.datax.empresa_correo,
              telefono: $scope.datax.empresa_telefono,
              ciudad: $scope.datax.empresa_ciudad,
              comuna: $scope.datax.empresa_comuna,
              codigoSii: $scope.datax.codigoSii
            };
            FebosAPI.cl_modificar_sucursal(query, body, true, false).then(function(response) {
              var modal = UIkit.modal('#mdModificarEmpresa');
              modal.hide();
              $scope.listar();

              $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
            });
          } else {
            var modal = UIkit.modal('#mdModificarEmpresa');
            modal.hide();
            $scope.listar();
          }
        }
      });

      console.log('sucursalMatriz', $scope.sucursalMatriz);
    };
    // --

    // -- función que confirma la eliminación de una empresa
    $scope.confirmarEliminarEmpresa = function(index) {
      $scope.index_empresa_eliminar = index;

      var modal = UIkit.modal('#mdConfirmar');
      modal.show();
    };
    // --

    // -- función que confirma la cancelación de eliminar una empresa
    $scope.cancelarEliminarEmpresa = function() {
      $scope.index_empresa_eliminar = '';

      var modal = UIkit.modal('#mdConfirmar');
      modal.hide();
    };
    // --

    $scope.eliminarEmpresa = function() {
      var index = $scope.index_empresa_eliminar;
      $scope.cargandoEliminar = true;

      console.log(index);
      console.log($scope.empresas);

      var body = {
        estadoEmpresa: 'no',
        regimen: $scope.empresas[index].regimen //no puede no enviarse en una modificacion! (uno de los valores corresponde a vacio)
      };
      FebosAPI.cl_actualizar_empresa(
        {
          empresaId: $scope.empresas[index].id,
          debug: 'no',
          simular: 'no'
        },
        body,
        true,
        false
      ).then(function(response) {
        $scope.cargandoEliminar = false;
        if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
          $scope.listar();
          $scope.cancelarEliminarEmpresa();
        }
      });
    };
    // --

    $scope.cambiarPagina(0); //< paginación mantenedor/listado de empresas

    // -- GESTIÓN SUCURSALES --
    $scope.sucursales = []; //< arreglo de sucursales
    $scope.cargandoSucursales = false;

    // -- función que activa modal con listado de sucursales de una empresa determinada
    $scope.listadoSucursales = function(indexs) {
      $scope.indexListaSucursales = indexs;
      $scope.empresax = $scope.empresas[indexs];
      $scope.cargandoSucursales = true;

      FebosAPI.cl_listar_sucursales(
        {
          empresaId: SesionFebos().empresa.id,
          simular: 'no',
          debug: 'si',
          pagina: $scope.filtrox.pagina,
          filas: $scope.filtrox.itemsPorPagina,
          empresax: $scope.empresas[indexs].iut
        },
        true,
        false
      ).then(function(response) {
        console.log(response);
        $scope.cargandoSucursales = false;
        $scope.sucursales = [];
        if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
          for (var j = 0; j < response.data.sucursales.length; j++) {
            var row = {
              febosId: '',
              rutEmpresa: '',
              codigoSii: '',
              direccion: '',
              correo: '',
              municipio: '',
              telefono: '',
              direccion: '',
              comuna: '',
              empresa: ''
            };

            row.febosId = response.data.sucursales[j].id;
            row.rutEmpresa = response.data.sucursales[j].rutEmpresa;
            row.codigoSii = response.data.sucursales[j].codigoSii;
            row.direccion = response.data.sucursales[j].direccion;
            row.correo = response.data.sucursales[j].correoElectronico;
            row.municipio = response.data.sucursales[j].municipioId;
            row.telefono = response.data.sucursales[j].telefono;
            row.direccion = response.data.sucursales[j].direccion;
            row.ciudad = response.data.sucursales[j].ciudad;
            row.comuna = response.data.sucursales[j].comuna;
            row.empresa = $scope.empresas[indexs].fantasia; //< empresa seleccionada

            //$scope.sucursales.push(row);
            if (row.febosId !== '') $scope.sucursales.push(row);
          }

          if (
            response.data.totalElementos == undefined ||
            typeof response.data.totalElementos == 'undefined'
          ) {
            $scope.filtrox.totalElementos = 0;
          } else {
            $scope.filtrox.totalElementos = parseInt(response.data.totalElementos);
          }

          $scope.calcularPaginasSuc();

          var modal = UIkit.modal('#mdListadoSucursal');
          modal.show();
          $scope.cargandoSucursales = false;
        }
      });
    };
    // --

    // -- función que permite cambiar de página en un mantenedor
    $scope.cambiarPaginaSuc = function(index) {
      $scope.cargandoSucursales = true;
      $scope.filtrox.numeroDePaginas = Math.ceil($scope.filtrox.numeroDePaginas);
      $scope.filtrox.totalElementos = Math.ceil($scope.filtrox.totalElementos);

      if (index == 'inicio') {
        $scope.filtrox.pagina = 1;
      } else {
        if (index == 'final') {
          $scope.filtrox.pagina = $scope.filtrox.totalPaginas;
        } else {
          if (
            index !== '' &&
            (index != undefined || typeof index != 'undefined') &&
            ($scope.filtrox.paginas[index] != undefined ||
              typeof $scope.filtrox.paginas[index] != 'undefined')
          ) {
            $scope.filtrox.pagina = $scope.filtrox.paginas[index].numero;
          } else {
            $scope.filtrox.pagina = 1;
          }
        }
      }

      $scope.listadoSucursales($scope.indexListaSucursales); //< llama función listar sucursales, para mostrar data
    };
    // --

    // -- función que calcula las páginas a mostrar en el mantenedor
    $scope.calcularPaginasSuc = function() {
      $scope.filtrox.totalPaginas = Math.ceil(
        $scope.filtrox.totalElementos / $scope.filtrox.itemsPorPagina
      );

      var resta = -(Math.ceil($scope.filtrox.numeroDePaginas) % 2 == 0
        ? Math.ceil($scope.filtrox.numeroDePaginas / 2 - 1)
        : Math.ceil($scope.filtrox.numeroDePaginas / 2 - 1));
      var suma =
        Math.ceil($scope.filtrox.numeroDePaginas) % 2 == 0
          ? Math.ceil($scope.filtrox.numeroDePaginas / 2)
          : Math.ceil($scope.filtrox.numeroDePaginas / 2 - 1);
      var first = false;

      if (Math.ceil($scope.filtrox.pagina) + resta <= 0) {
        first = true;
        suma -= Math.ceil($scope.filtrox.pagina) + (resta - 1);
        resta -= Math.ceil($scope.filtrox.pagina) + (resta - 1);
      }

      if ($scope.filtrox.totalPaginas <= 0) suma = $scope.filtrox.numeroDePaginas + resta - 1;

      if (
        Math.ceil($scope.filtrox.pagina) + suma > $scope.filtrox.totalPaginas &&
        $scope.filtrox.totalPaginas > 0
      ) {
        if (first) {
          suma -= Math.ceil($scope.filtrox.pagina + suma) - $scope.filtrox.totalPaginas;
        } else {
          suma -= Math.ceil($scope.filtrox.pagina + suma) - $scope.filtrox.totalPaginas;
        }
      }

      var cont = 0;
      $scope.filtrox.paginas = [];

      for (var i = $scope.filtrox.pagina + resta; i <= $scope.filtrox.pagina + suma; i++) {
        var nuevaPagina = {
          id: cont,
          numero: i
        };

        $scope.filtrox.paginas.push(nuevaPagina);
        cont++;
      }
    };
    // --

    // -- función que activa el modal con el formulario para crear nueva sucursal
    $scope.abrirCrear = function(empresax) {
      $scope.backToCrear();
      console.log(empresax);
      $scope.cargando = false;

      $scope.crear = {
        febosId: '',
        rutEmpresa: empresax.iut,
        codigoSii: '',
        direccion: '',
        correo: '',
        //municipio: "Febos",
        telefono: '',
        empresa: empresax.fantasia,
        empresaHide: empresax.iut,
        textoCrearSucursal: 'CREAR SUCURSAL',
        mensaje_error: '',
        mensaje_ok: '',
        municipioId: '',
        region: '',
        provincia: '',
        comuna: '',
        ciudad: '',
        regionAnterior: '',
        provinciaAnterior: '',
        comunaAnterior: ''
      };

      var modal = UIkit.modal('#mdCrear');
      modal.show();
    };
    // --

    // -- función que cierra modal crear nueva sucursal
    $scope.cerrarCrear = function() {
      var modal = UIkit.modal('#mdCrear');
      modal.hide();
    };
    // --

    // -- función que crea una sucursal, envía data a la API
    $scope.crearSucursal = function() {
      if ($scope.crear.textoCrearSucursal != 'CREAR SUCURSAL') return;

      if (!$scope.validaCrearSucursal()) return false; //< validación de campos obligatorios

      $scope.crear.textoCrearSucursal = 'Creando ...';

      var body = {
        rutEmpresa: $scope.crear.rutEmpresa,
        codigoSii: $scope.crear.codigoSii,
        direccion: $scope.crear.direccion,
        correoElectronico: $scope.crear.correo,
        municipioId: $scope.crear.municipioId,
        telefono: $scope.crear.telefono,
        comuna: $scope.crear.comuna,
        ciudad: $scope.crear.ciudad
      };
      FebosAPI.cl_crear_sucursal(
        {
          empresaId: SesionFebos().empresa.id
        },
        body,
        true,
        false
      ).then(function(response) {
        console.log(response);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
          $scope.crear.textoCrearSucursal = 'CREAR SUCURSAL';
          $scope.crear.mensaje_ok = response.data.mensaje;

          utils.card_show_hide($crear_formSu, undefined, crear_ok_show, undefined);
          $scope.limpiarCrear();
        }
      });

      //$scope.cerrarCrear();
    };
    // --

    // -- función que valida formulario de creación de sucursal
    $scope.validaCrearSucursal = function() {
      var errores = '';

      if ($scope.crear.direccion == '') {
        errores += '\nDebe ingresar una dirección.';
      }

      if ($scope.crear.comuna == '') {
        errores += '\nDebe ingresar una comuna.';
      }

      if ($scope.crear.ciudad == '') {
        errores += '\nDebe ingresar una ciudad.';
      }

      if ($scope.crear.correo == '') {
        errores += '\nDebe ingresar un correo electrónico.';
      }

      if ($scope.crear.telefono == '') {
        errores += '\nDebe ingresar un número telefónico.';
      }

      if (isNumber($scope.crear.telefono) == 0) {
        errores += '\nDebe ingresar un valor numérico';
      }

      if ($scope.crear.codigoSii == '') {
        errores += '\nDebe ingresar un Código Sii.';
      }

      if (isNumber($scope.crear.codigoSii) == 0) {
        errores += '\nDebe ingresar un valor numérico para el código Sii';
      }

      if (isEmail($scope.crear.correo) == 0) {
        errores += '\nDebe ingresar un correo electrónico válido';
      }

      if (errores != '') {
        UIkit.modal.alert('Errores encontrados: ' + errores);
        return false;
      } else {
        return true;
      }
    };
    // --

    // -- función que limpia input del formulario crear sucursal
    $scope.limpiarCrear = function() {
      $scope.crear.codigoSii = '';
      $scope.crear.direccion = '';
      $scope.crear.correo = '';
      $scope.crear.telefono = '';
      $scope.crear.comuna = '';
      $scope.crear.ciudad = '';
    };
    // --

    // -- función que activa modal para modificar una sucursal
    $scope.abrirModificarSucursal = function(sucursal) {
      $scope.backToCrear();
      console.log(sucursal);
      var telefono = parseInt(sucursal.telefono);

      $scope.cargando = false;

      $scope.modificar = {
        febosId: sucursal.febosId,
        rutEmpresa: sucursal.rutEmpresa,
        codigoSii: sucursal.codigoSii,
        direccion: sucursal.direccion,
        correo: sucursal.correo,
        municipio: 'Febos',
        telefono: telefono,
        textoModificarSucursal: 'MODIFICAR SUCURSAL',
        empresa: sucursal.empresa,
        comuna: sucursal.comuna,
        ciudad: sucursal.ciudad,
        mensaje_error: '',
        mensaje_ok: ''
      };

      var modal = UIkit.modal('#mdModificar');
      modal.show();
    };
    // --

    // -- función que modifica una sucursal, envía data a la API
    $scope.modificarSucursal = function() {
      console.log('modifica ...');
      if ($scope.modificar.textoModificarSucursal != 'MODIFICAR SUCURSAL') return;

      if (!$scope.validaModificarSucursal()) return false; //< validación de campos obligatorios

      $scope.modificar.textoModificarSucursal = 'Modificando ...';

      var telefono = $scope.modificar.telefono;
      var stringTelefono = telefono.toString();

      var body = {
        codigoSii: $scope.modificar.codigoSii,
        direccion: $scope.modificar.direccion,
        correoElectronico: $scope.modificar.correo,
        telefono: stringTelefono,
        municipioId: $scope.modificar.municipioId,
        comuna: $scope.modificar.comuna,
        ciudad: $scope.modificar.ciudad
      };
      FebosAPI.cl_modificar_sucursal(
        {
          empresaId: SesionFebos().empresa.id,
          sucursalId: $scope.modificar.febosId,
          simular: 'no'
        },
        body,
        true,
        false
      ).then(function(response) {
        console.log(response);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
          $scope.modificar.textoModificarSucursal = 'MODIFICAR SUCURSAL';
          $scope.modificar.mensaje_ok = response.data.mensaje;

          utils.card_show_hide($modificar_formSu, undefined, crear_ok_show, undefined);
        }
      });
    };
    // --

    // -- función que valida formulario de modificación de sucursal
    $scope.validaModificarSucursal = function() {
      var errores = '';

      if ($scope.modificar.direccion == '') {
        errores += '\nDebe ingresar una dirección.';
      }

      if ($scope.modificar.comuna == '') {
        errores += '\nDebe ingresar una comuna.';
      }

      if ($scope.modificar.ciudad == '') {
        errores += '\nDebe ingresar una ciudad.';
      }

      if ($scope.modificar.correo == '') {
        errores += '\nDebe ingresar un correo electrónico.';
      }

      if ($scope.modificar.telefono == '') {
        errores += '\nDebe ingresar un número telefónico.';
      }

      if (isNumber($scope.modificar.telefono) == 0) {
        errores += '\nDebe ingresar un valor numérico';
      }

      if ($scope.modificar.codigoSii == '') {
        errores += '\nDebe ingresar un Código SII.';
      }

      if (isNumber($scope.modificar.codigoSii) == 0) {
        errores += '\nDebe ingresar un valor numérico para el Código SII';
      }

      if (isEmail($scope.modificar.correo) == 0) {
        errores += '\nDebe ingresar un correo electrónico válido';
      }

      if (errores != '') {
        UIkit.modal.alert('Errores encontrados: ' + errores);
        return false;
      } else {
        return true;
      }
    };
    // --

    // -- función que activa modal para eliminar una sucursal
    $scope.abrirEliminar = function(sucursalx) {
      $scope.eliminar = {
        textoEliminarSucursal: 'ELIMINAR SUCURSAL',
        sucursalId: sucursalx.febosId
      };

      var modal = UIkit.modal('#mdEliminar');
      modal.show();
    };
    // --

    // -- función que cierra modal para eliminar sucursal
    $scope.cerrarEliminar = function() {
      $scope.eliminar = {
        textoEliminarSucursal: 'ELIMINAR SUCURSAL',
        sucursalId: ''
      };

      var modal = UIkit.modal('#mdEliminar');
      modal.hide();
    };
    // --

    // -- función que envía data a la API para elimina sucursal
    $scope.eliminarSucursal = function() {
      console.log('eliminando ...');
      console.log($scope.eliminar.sucursalId);
      if ($scope.eliminar.textoEliminarSucursal != 'ELIMINAR SUCURSAL') return;

      $scope.eliminar.textoEliminarSucursal = 'Eliminando ...';

      FebosAPI.cl_eliminar_sucursal(
        {
          empresaId: SesionFebos().empresa.id,
          sucursalId: $scope.eliminar.sucursalId,
          simular: 'no'
        },
        false,
        false
      ).then(
        function(response) {
          console.log(response);
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            $scope.crear.textoEliminarSucursal = 'ELIMINAR SUCURSAL';
            $scope.cerrarEliminar();
          } else {
            UIkit.modal.alert(response.data.mensaje);
            $scope.cerrarEliminar();
          }
        },
        function(error) {
          // hacer algo con el error
          console.log(error);
        }
      );
    };
    // --

    // -- VALIDACIONES --

    function validarRut(rut) {
      // Despejar Puntos
      var valor = rut.replace('.', '');
      // Despejar Guión
      valor = valor.replace('-', '');

      // Aislar Cuerpo y Dígito Verificador
      var cuerpo = valor.slice(0, -1);
      var dv = valor.slice(-1).toUpperCase();

      // Formatear RUN
      rut = cuerpo + '-' + dv;

      // Si no cumple con el mínimo ej. (n.nnn.nnn)
      if (cuerpo.length < 7) {
        console.log('RUT Incompleto');
        return false;
      }

      // Calcular Dígito Verificador
      var suma = 0;
      var multiplo = 2;

      // Para cada dígito del Cuerpo
      for (i = 1; i <= cuerpo.length; i++) {
        // Obtener su Producto con el Múltiplo Correspondiente
        var index = multiplo * valor.charAt(cuerpo.length - i);

        // Sumar al Contador General
        suma = suma + index;

        // Consolidar Múltiplo dentro del rango [2,7]
        if (multiplo < 7) {
          multiplo = multiplo + 1;
        } else {
          multiplo = 2;
        }
      }

      // Calcular Dígito Verificador en base al Módulo 11
      var dvEsperado = 11 - (suma % 11);

      // Casos Especiales (0 y K)
      dv = dv === 'K' ? 10 : dv;
      dv = dv === 0 ? 11 : dv;

      // Validar que el Cuerpo coincide con su Dígito Verificador
      if (dvEsperado != dv) {
        return false;
      }

      // Si todo sale bien, eliminar errores (decretar que es válido)
      // rut.setCustomValidity('');
      return true;
    }

    // -- función que valida el rut 11111111-1
    function isRut(rutCompleto) {
      if (!/^[0-9]+-[0-9kK]{1}$/.test(rutCompleto)) {
        return false;
      }

      var tmp = rutCompleto.split('-');
      var digv = tmp[1];
      var rut = tmp[0];

      if (digv == 'K') {
        digv = 'k';
      }

      var digesto = isDv(rut);

      if (digesto == digv) {
        return true;
      } else {
        return false;
      }
    }
    // --

    // -- función que valida el DV del rut
    function isDv(T) {
      var M = 0;
      var S = 1;

      for (; T; T = Math.floor(T / 10)) {
        S = (S + (T % 10) * (9 - (M++ % 6))) % 11;
      }

      return S ? S - 1 : 'k';
    }
    // --

    // -- función que valida formato correo
    function isEmail(mail) {
      var estado = 1;
      if (
        !/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/.test(
          mail
        )
      ) {
        estado = 0;
      }

      return estado;
    }
    // --

    // -- función que valida campos numéricos
    function isNumber(numero) {
      var estado = 1;
      if (isNaN(parseInt(numero))) {
        estado = 0;
      }

      console.log(estado);

      return estado;
    }
    // --

    // -- GESTIÓN CAJAS --
    $scope.cajas = []; //< arreglo de cajas
    $scope.cargandoCajas = false;

    // -- función que activa modal con listado de cajas de una sucursal determinada
    $scope.listadoCajas = function(sucursal) {
      $scope.sucursalx = sucursal;
      $scope.cargandoCajas = true;

      FebosAPI.cl_listar_cajas(
        {
          empresaId: SesionFebos().empresa.id,
          simular: 'no',
          debug: 'si',
          /*"pagina": $scope.filtrox.pagina,
                "filas": $scope.filtrox.itemsPorPagina,*/
          sucursalId: $scope.sucursalx.febosId
        },
        true,
        false
      ).then(function(response) {
        console.log(response);
        $scope.cargandoCajas = false;
        $scope.cajas = [];

        if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
          for (var j = 0; j < response.data.cajas.length; j++) {
            var row = {
              febosId: '',
              sucursal: '',
              nombre: '',
              numero: '',
              ubicacion: ''
            };

            row.febosId = response.data.cajas[j].cajasId;
            row.sucursal = $scope.sucursalx.direccion;
            row.sucursalId = $scope.sucursalx.febosId;
            row.nombre = response.data.cajas[j].nombre;
            row.numero = response.data.cajas[j].numero;
            row.ubicacion = response.data.cajas[j].ubicacion;

            if (row.febosId !== '') $scope.cajas.push(row);
          }

          /*if (response.data.totalElementos == undefined || typeof response.data.totalElementos == "undefined") {
                        $scope.totalElementos = 0;
                    } else {
                        $scope.totalElementos = parseInt(response.data.totalElementos);
                    }

                    $scope.calcularPaginas();*/

          var modal = UIkit.modal('#mdListadoCajas');
          modal.show();
        }
      });
    };

    // -- función que activa el modal con el formulario para crear nueva caja
    $scope.abrirCrearCaja = function(sucursal) {
      $scope.backToCrear();
      console.log(sucursal);
      $scope.cargandoCajas = false;

      $scope.crearCaja = {
        febosId: '',
        nombre: '',
        numero: '',
        ubicacion: '',
        sucursal: sucursal.direccion,
        sucursalHide: sucursal.febosId,
        textoCrearCaja: 'CREAR CAJA',
        mensaje_error: '',
        mensaje_ok: ''
      };

      var modal = UIkit.modal('#mdCrearCaja');
      modal.show();
    };
    // --

    // -- función que cierra modal crear nueva caja
    $scope.cerrarCrearCaja = function() {
      var modal = UIkit.modal('#mdCrearCaja');
      modal.hide();
    };
    // --

    // -- función que crea una caja, envía data a la API
    $scope.crearCajaNueva = function() {
      if ($scope.crearCaja.textoCrearCaja != 'CREAR CAJA') return;

      if (!$scope.validaCrearCaja()) return false; //< validación de campos obligatorios

      $scope.crearCaja.textoCrearCaja = 'Creando ...';

      var body = {
        sucursalId: $scope.crearCaja.sucursalHide,
        nombre: $scope.crearCaja.nombre,
        numero: $scope.crearCaja.numero,
        ubicacion: $scope.crearCaja.ubicacion
      };

      FebosAPI.cl_crear_caja(
        {
          empresaId: SesionFebos().empresa.id,
          sucursalId: $scope.crearCaja.sucursalHide
        },
        body,
        true,
        false
      ).then(function(response) {
        console.log(response);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;

        if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
          $scope.crearCaja.textoCrearCaja = 'CREAR CAJA';

          utils.card_show_hide($crear_formCaja, undefined, crear_ok_show, undefined);
          $scope.limpiarCrearCaja();
        }
      });
    };
    // --

    // -- función que valida formulario de creación de caja
    $scope.validaCrearCaja = function() {
      var errores = '';

      if ($scope.crearCaja.nombre == '') {
        errores += '\nDebe ingresar un nombre.';
      }

      if ($scope.crearCaja.numero == '') {
        errores += '\nDebe ingresar un número.';
      }

      if (isNumber($scope.crearCaja.numero) == 0) {
        errores += '\nDebe ingresar un valor numérico';
      }

      if ($scope.crearCaja.ubicacion == '') {
        errores += '\nDebe ingresar una ubicación.';
      }

      if (errores != '') {
        UIkit.modal.alert('Errores encontrados: ' + errores);
        return false;
      } else {
        return true;
      }
    };
    // --

    // -- función que limpia input del formulario crear caja
    $scope.limpiarCrearCaja = function() {
      $scope.crearCaja.sucursal = '';
      $scope.crearCaja.sucursalHide = '';
      $scope.crearCaja.nombre = '';
      $scope.crearCaja.numero = '';
      $scope.crearCaja.ubicacion = '';
    };
    // --

    // -- función que activa modal para modificar una caja
    $scope.abrirModificarCaja = function(caja) {
      $scope.backToCrear();
      console.log(caja);
      $scope.cargandoCajas = false;

      $scope.modificarCaja = {
        febosId: caja.febosId,
        sucursal: caja.sucursal,
        sucursalId: caja.sucursalId,
        nombre: caja.nombre,
        numero: caja.numero,
        ubicacion: caja.ubicacion,
        textoModificarCaja: 'MODIFICAR CAJA',
        mensaje_error: '',
        mensaje_ok: ''
      };

      var modal = UIkit.modal('#mdModificarCaja');
      modal.show();
    };
    // --

    // -- función que modifica una caja, envía data a la API
    $scope.modificarCajaSucursal = function() {
      console.log('modifica ...');
      if ($scope.modificarCaja.textoModificarCaja != 'MODIFICAR CAJA') return;

      if (!$scope.validaModificarCaja()) return false; //< validación de campos obligatorios

      $scope.modificarCaja.textoModificarCaja = 'Modificando ...';

      var body = {
        cajaId: $scope.modificarCaja.febosId,
        nombre: $scope.modificarCaja.nombre,
        numero: $scope.modificarCaja.numero,
        ubicacion: $scope.modificarCaja.ubicacion
      };

      FebosAPI.cl_modificar_caja(
        {
          empresaId: SesionFebos().empresa.id,
          sucursalId: $scope.modificarCaja.sucursalId,
          cajaId: $scope.modificarCaja.febosId,
          simular: 'no'
        },
        body,
        true,
        false
      ).then(function(response) {
        console.log(response);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;

        if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
          $scope.modificarCaja.textoModificarCaja = 'MODIFICAR CAJA';
          $scope.modificarCaja.mensaje_ok = response.data.mensaje;

          utils.card_show_hide($modificar_formCaja, undefined, crear_ok_show, undefined);
        }
      });
    };
    // --

    // -- función que valida formulario de modificación de caja
    $scope.validaModificarCaja = function() {
      var errores = '';

      if ($scope.modificarCaja.nombre == '') {
        errores += '\nDebe ingresar un nombre.';
      }

      if ($scope.modificarCaja.numero == '') {
        errores += '\nDebe ingresar un número.';
      }

      if (isNumber($scope.modificarCaja.numero) == 0) {
        errores += '\nDebe ingresar un valor numérico';
      }

      if ($scope.modificarCaja.ubicacion == '') {
        errores += '\nDebe ingresar una ubicación.';
      }

      if (errores != '') {
        UIkit.modal.alert('Errores encontrados: ' + errores);
        return false;
      } else {
        return true;
      }
    };
    // --

    // -- función que activa modal para eliminar una caja
    $scope.abrirEliminarCaja = function(caja) {
      $scope.eliminarCaja = {
        textoEliminarCaja: 'ELIMINAR CAJA',
        sucursalId: caja.sucursalId,
        cajaId: caja.febosId
      };

      var modal = UIkit.modal('#mdEliminarCaja');
      modal.show();
    };
    // --

    // -- función que cierra modal para eliminar caja
    $scope.cerrarEliminarCaja = function() {
      $scope.eliminarCaja = {
        textoEliminarCaja: 'ELIMINAR CAJA',
        sucursalId: '',
        cajaId: ''
      };

      var modal = UIkit.modal('#mdEliminarCaja');
      modal.hide();
    };
    // --

    // -- función que envía data a la API para elimina caja
    $scope.eliminarCajaAccion = function() {
      console.log('eliminando ...');
      console.log($scope.eliminarCaja.cajaId);

      if ($scope.eliminarCaja.textoEliminarCaja != 'ELIMINAR CAJA') return;

      $scope.eliminarCaja.textoEliminarCaja = 'Eliminando ...';

      FebosAPI.cl_eliminar_caja(
        {
          empresaId: SesionFebos().empresa.id,
          sucursalId: $scope.eliminarCaja.sucursalId,
          cajaId: $scope.eliminarCaja.cajaId,
          simular: 'no'
        },
        false,
        false
      ).then(
        function(response) {
          console.log(response);
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;

          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            $scope.eliminarCaja.textoEliminarCaja = 'ELIMINAR CAJA';
            $scope.cerrarEliminarCaja();
          } else {
            UIkit.modal.alert(response.data.mensaje);
            $scope.cerrarEliminarCaja();
          }
        },
        function(error) {
          // hacer algo con el error
          console.log(error);
        }
      );
    };
    // --
  });
