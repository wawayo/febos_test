angular.module('febosApp').controller('cafsStoreLegacyCtrl', [
  '$rootScope',
  '$scope',
  '$state',
  '$stateParams',
  'FebosAPI',
  'SesionFebos',
  'cafStores',
  'FebosUtil',
  function($rootScope, $scope, $state, $stateParams, FebosAPI, SesionFebos, cafStores, FebosUtil) {
    $scope.app = $stateParams.app;
    if (typeof cafStores.cafStores != 'undefined') {
      console.log('encontre caf stores: ' + cafStores.cafStores.length);
      $scope.cafStores = cafStores.cafStores;
    } else {
      console.log('No encontre caf stores');
      $scope.cafStores = [];
    }

    for (var i = 0; i < $scope.cafStores.length; i++) {
      $scope.cafStores[i].tipoDocumento = FebosUtil.tipoDocumento(
        parseInt($scope.cafStores[i].tipoDte)
      );
    }

    $scope.propiedadFiltro = 'tipoDocumento';
    $scope.reverse = true;

    $scope.sortBy = function(propiedad) {
      $scope.reverse = $scope.propiedadFiltro === propiedad ? !$scope.reverse : false;
      $scope.propiedadFiltro = propiedad;
    };

    $scope.modalCargarCAF = function() {
      $scope.carga = {
        texto: 'CARGAR CAF',
        folioInicio: '',
        folioFin: ''
      };
      UIkit.modal('#modalCargarCAF').show();
    };

    $scope.parsearXml = function(xml) {
      if (window.DOMParser) {
        parser = new DOMParser();
        xmlDoc = parser.parseFromString(xml, 'text/xml');
      } else {
        //IE
        xmlDoc = new ActiveXObject('Microsoft.XMLDOM');
        xmlDoc.async = false;
        xmlDoc.loadXML(xml);
      }
      return xmlDoc;
    };

    $scope.archivoCafCargado = function() {
      // Cargar rango de folios
      var archivoCaf = document.getElementById('archivoCaf').files;

      if (archivoCaf.length > 0) {
        if (archivoCaf[0].type === 'text/xml') {
          var reader = new FileReader();
          reader.readAsDataURL(archivoCaf[0]);

          reader.onload = function() {
            var data = reader.result;
            var payload = data.replace(/^[^,]*,/, '');

            var contenidoXml = atob(payload);
            var xmlDoc = $scope.parsearXml(contenidoXml);

            $scope.carga.folioInicio = parseInt(
              xmlDoc.getElementsByTagName('D')[0].childNodes[0].nodeValue
            );
            $scope.carga.folioFin = parseInt(
              xmlDoc.getElementsByTagName('H')[0].childNodes[0].nodeValue
            );
            $scope.$apply();
          };
        }
      }
    };

    $scope.validaCargaCaf = function() {
      // Validar formulario
      if (isNaN($scope.carga.folioInicio) || isNaN($scope.carga.folioFin)) {
        $scope.carga.texto = 'CARGAR CAF';
        UIkit.modal.alert('Debe ingresar un valor numérico para los campos de folio.');
        return false;
      }

      // validar que sea un xml de CAF
      var rutEmisor = xmlDoc.getElementsByTagName('RE')[0].childNodes[0].nodeValue;
      var razonSocial = xmlDoc.getElementsByTagName('RS')[0].childNodes[0].nodeValue;
      var tipoDte = xmlDoc.getElementsByTagName('TD')[0].childNodes[0].nodeValue;
      var fechaAut = xmlDoc.getElementsByTagName('FA')[0].childNodes[0].nodeValue;

      if (rutEmisor.length < 0) {
        UIkit.modal.alert('El archivo no contiene RUT Emisor');
        $scope.carga.texto = 'CARGAR CAF';
        return false;
      }
      if (razonSocial.length < 0) {
        UIkit.modal.alert('El archivo no contiene Razon Social del Emisor');
        return false;
      }
      if (tipoDte.length < 0) {
        UIkit.modal.alert('El archivo no contiene Tipo de Documento');
        return false;
      }
      if (fechaAut.length < 0) {
        UIkit.modal.alert('El archivo no contiene Fecha de Autorización');
        return false;
      }
      if ($scope.tipoCAF != 0 && $scope.tipoCAF != 1) {
        UIkit.modal.alert('Debe seleccionar una opción para Tipo Foliación');
        return false;
      }
      return true;
    };

    $scope.cargarCaf = function() {
      // Validar formulario
      if ($scope.validarCargaCaf()) {
        var archivoCaf = document.getElementById('archivoCaf').files;
        if (archivoCaf.length > 0) {
          $scope.carga.texto = 'CARGANDO CAF...';

          if (archivoCaf[0].type === 'text/xml') {
            var reader = new FileReader();
            reader.readAsDataURL(archivoCaf[0]);

            reader.onload = function() {
              var data = reader.result;
              var payload = data.replace(/^[^,]*,/, '');

              FebosAPI.cl_cargar_caf_store(
                {
                  empresaId: SesionFebos().empresa.iut,
                  folioInicio: $scope.carga.folioInicio,
                  folioFin: $scope.carga.folioFin
                },
                {
                  dataCafStore: payload
                },
                true,
                false
              ).then(function(response) {
                if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
                  UIkit.modal.alert(response.data.mensaje + '<br>' + response.data.mensajeWS);
                  $scope.carga.texto = 'CARGAR CAF';
                }
              });
            };
          } else {
            console.log('No corresponde el tipo de archivo ' + archivoCaf[0].type);
            UIkit.modal.alert(
              'No corresponde el tipo de archivo ' +
                archivoCaf[0].type +
                '<br>' +
                'Asegúrese de estar cargando un archivo XML'
            );
            $scope.carga.texto = 'CARGAR CAF';
            return false;
          }
        } else {
          UIkit.modal.alert('Por favor seleccionar un archivo .xml');
        }
      } else {
        $scope.carga.texto = 'CARGAR CAF';
      }
    };
  }
]);
