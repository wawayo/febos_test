angular.module('febosApp').controller('cafsStoreDeliveryLegacyCtrl', [
  '$rootScope',
  '$scope',
  '$state',
  '$stateParams',
  'FebosAPI',
  'SesionFebos',
  'cafsStoreDelivery',
  'FebosUtil',
  function(
    $rootScope,
    $scope,
    $state,
    $stateParams,
    FebosAPI,
    SesionFebos,
    cafsStoreDelivery,
    FebosUtil
  ) {
    if (typeof cafsStoreDelivery.cafStoreDelivery != 'undefined') {
      console.log('encontre caf stores: ' + cafsStoreDelivery.cafStoreDelivery.length);
      $scope.cafsStoreDelivery = cafsStoreDelivery.cafStoreDelivery;
    } else {
      console.log('No encontre caf stores');
      $scope.cafsStoreDelivery = [];
    }

    $scope.propiedadFiltro = 'fecha';
    $scope.reverse = true;

    $scope.sortBy = function(propiedad) {
      $scope.reverse = $scope.propiedadFiltro === propiedad ? !$scope.reverse : false;
      $scope.propiedadFiltro = propiedad;
    };
  }
]);
