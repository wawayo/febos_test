febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.entregaDeFolios_legacy', {
      url: '/:app/cafsDeReservaL/:cafId',
      templateUrl: 'app/febos/cafsStoreLegacy/cafsStoreDelivery/cafsStoreDeliveryView.html',
      controller: 'cafsStoreDeliveryLegacyCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        cafsStoreDelivery: [
          'FebosAPI',
          '$stateParams',
          'SesionFebos',
          function(FebosAPI, $stateParams, SesionFebos) {
            return FebosAPI.cl_listar_cafs_store_delivery(
              {
                empresaId: SesionFebos().empresa.iut,
                cafId: $stateParams.cafId,
                numeroPagina: 1,
                filasPorPagina: 200
              },
              false,
              false
            ).then(function(response) {
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/cafsStoreLegacy/cafsStoreDelivery/cafsStoreDeliveryLegacyController.js'
            ]);
          }
        ]
      }
    });
  }
]);
