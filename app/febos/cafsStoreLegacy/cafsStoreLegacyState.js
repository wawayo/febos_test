febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.cafsDeReserva_legacy', {
      url: '/:app/cafsDeReservaL',
      templateUrl: 'app/febos/cafsStoreLegacy/cafsStoreView.html',
      controller: 'cafsStoreLegacyCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        cafStores: [
          'FebosAPI',
          '$stateParams',
          'SesionFebos',
          function(FebosAPI, $stateParams, SesionFebos) {
            return FebosAPI.cl_listar_cafs_store(
              {
                empresaId: SesionFebos().empresa.id,
                numeroPagina: 1,
                filasPorPagina: 200
              },
              {},
              false,
              false
            ).then(function(response) {
              /*
                            if(reponse.data.codigo != 0) {
                                if (typeof response.data.cafStores != 'undefined') {
                                    console.log("encontre caf stores: " + response.data.cafStores.length);
                                    return response.data.cafStores;
                                } else {
                                    console.log("No encontre caf stores");
                                    return [];
                                }
                            }*/
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/cafsStoreLegacy/cafsStoreLegacyController.js'
            ]);
          }
        ]
      }
    });
  }
]);
