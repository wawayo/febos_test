angular.module('febosApp').controller('gestionReceptoresElectronicosCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$http',
  'utils',
  '$state',
  '$location',
  'SesionFebos',
  'FebosAPI',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $http,
    utils,
    $state,
    $location,
    SesionFebos,
    FebosAPI
  ) {
    $scope.filtros = [];
    $scope.filtros.paginas = [];
    $scope.filtros.receptor_rut = '';
    $scope.filtros.nombre_empresa = '';
    $scope.filtros.correo = '';
    $scope.filtros.pagina = '1';
    $scope.filtros.itemsPorPagina = '10';
    $scope.filtros.numeroDePaginas = 9; //< número de páginas máximo mostradas en la parte de cambio de página

    $scope.totalElementos = 0;
    $scope.totalPaginas = 0;

    // -- función que lista a los receptores electrónicos
    $scope.listar = function() {
      $scope.cargando = true;
      $scope.receptores = [];

      FebosAPI.cl_listar_receptores_electronicos(
        {
          paginas: $scope.filtros.pagina,
          filas: $scope.filtros.itemsPorPagina,
          filtros:
            $scope.filtros.receptor_rut.replace(/\,/g, '') +
            ',' +
            $scope.filtros.nombre_empresa.replace(/\,/g, '') +
            ',' +
            $scope.filtros.correo.replace(/\,/g, ''),
          simular: 'no',
          debug: 'si'
        },
        {},
        true,
        false
      ).then(function(response) {
        console.log(response);
        $scope.cargando = false;

        try {
          if (response.data.codigo === 10 || response.data.codigo === '10') {
            $scope.receptores = [];

            for (var i = 0; i < response.data.re.length; i++) {
              var row = {
                id: '',
                rut: '',
                razonSocial: '',
                numeroResolucion: '',
                fechaResolucion: '',
                correo: ''
              };

              row.id = response.data.re[i].receptor_id;
              row.rut = response.data.re[i].rut;
              row.razonSocial = response.data.re[i].razon_social;
              row.numeroResolucion = response.data.re[i].numero_resolucion;
              row.fechaResolucion = response.data.re[i].fecha_resolucion;
              row.correo = response.data.re[i].correo;

              $scope.receptores.push(row);
            }

            if (
              response.data.totalElementos == undefined ||
              typeof response.data.totalElementos == 'undefined'
            ) {
              $scope.totalElementos = 0;
            } else {
              $scope.totalElementos = parseInt(response.data.totalElementos);
            }

            $scope.calcularPaginas();
          } else {
            SesionFebos().error(
              response.data.codigo,
              response.data.mensaje,
              response.data.seguimientoId
            );
          }
        } catch (e) {
          SesionFebos().error(
            response.data.codigo,
            response.data.mensaje,
            response.data.seguimientoId
          );
        }
      });
    };
    // --

    // -- función que permite cambiar de página en un mantenedor
    $scope.cambiarPagina = function(index) {
      $scope.filtros.numeroDePaginas = Math.ceil($scope.filtros.numeroDePaginas);
      $scope.totalElementos = Math.ceil($scope.totalElementos);

      if (index == 'inicio') {
        $scope.filtros.pagina = 1;
      } else {
        if (index == 'final') {
          $scope.filtros.pagina = $scope.totalPaginas;
        } else {
          if (
            index !== '' &&
            (index != undefined || typeof index != 'undefined') &&
            ($scope.filtros.paginas[index] != undefined ||
              typeof $scope.filtros.paginas[index] != 'undefined')
          ) {
            $scope.filtros.pagina = $scope.filtros.paginas[index].numero;
          } else {
            $scope.filtros.pagina = 1;
          }
        }
      }

      $scope.listar(); //< llama función listar usuarios, para mostrar data
    };
    // --

    // -- función que calcula las páginas a mostrar en el mantenedor
    $scope.calcularPaginas = function() {
      $scope.totalPaginas = Math.ceil($scope.totalElementos / $scope.filtros.itemsPorPagina);

      var resta = -(Math.ceil($scope.filtros.numeroDePaginas) % 2 == 0
        ? Math.ceil($scope.filtros.numeroDePaginas / 2 - 1)
        : Math.ceil($scope.filtros.numeroDePaginas / 2 - 1));
      var suma =
        Math.ceil($scope.filtros.numeroDePaginas) % 2 == 0
          ? Math.ceil($scope.filtros.numeroDePaginas / 2)
          : Math.ceil($scope.filtros.numeroDePaginas / 2 - 1);
      var first = false;

      if (Math.ceil($scope.filtros.pagina) + resta <= 0) {
        first = true;
        suma -= Math.ceil($scope.filtros.pagina) + (resta - 1);
        resta -= Math.ceil($scope.filtros.pagina) + (resta - 1);
      }

      if ($scope.totalPaginas <= 0) suma = $scope.filtros.numeroDePaginas + resta - 1;

      if (
        Math.ceil($scope.filtros.pagina) + suma > $scope.totalPaginas &&
        $scope.totalPaginas > 0
      ) {
        if (first) {
          suma -= Math.ceil($scope.filtros.pagina + suma) - $scope.totalPaginas;
        } else {
          suma -= Math.ceil($scope.filtros.pagina + suma) - $scope.totalPaginas;
        }
      }

      var cont = 0;
      $scope.filtros.paginas = [];

      for (var i = $scope.filtros.pagina + resta; i <= $scope.filtros.pagina + suma; i++) {
        var nuevaPagina = {
          id: cont,
          numero: i
        };

        $scope.filtros.paginas.push(nuevaPagina);
        cont++;
      }
    };
    // --

    // -- función que limpia los input de los filtros
    $scope.limpiarFiltros = function() {
      $scope.filtros.receptor_rut = '';
      $scope.filtros.nombre_empresa = '';
      $scope.filtros.correo = '';
    };
    // --

    $scope.cambiarPagina('inicio'); //< iniciar la página

    // -- función que valida el rut 11111111-1
    function isRut(rutCompleto) {
      if (!/^[0-9]+-[0-9kK]{1}$/.test(rutCompleto)) {
        return false;
      }

      var tmp = rutCompleto.split('-');
      var digv = tmp[1];
      var rut = tmp[0];

      if (digv == 'K') {
        digv = 'k';
      }

      var digesto = isDv(rut);

      if (digesto == digv) {
        return true;
      } else {
        return false;
      }
    }
    // --

    // -- función que valida el DV del rut
    function isDv(T) {
      var M = 0;
      var S = 1;

      for (; T; T = Math.floor(T / 10)) {
        S = (S + (T % 10) * (9 - (M++ % 6))) % 11;
      }

      return S ? S - 1 : 'k';
    }
  }
]);
