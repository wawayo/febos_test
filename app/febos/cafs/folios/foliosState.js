febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.folios', {
      url: '/:app/cafs/folios?tipoDTE&tipoCAF',
      templateUrl: 'app/febos/cafs/folios/foliosView.html',
      controller: 'foliosCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        cafsDetalles: [
          'FebosAPI',
          '$stateParams',
          'SesionFebos',
          function(FebosAPI, $stateParams, SesionFebos) {
            return FebosAPI.cl_caf_listar_folios(
              {
                empresaId: SesionFebos().empresa.iut,
                tipoCAF: $stateParams.tipoCAF,
                tipoDTE: $stateParams.tipoDTE
              },
              true,
              false
            ).then(function(response) {
              if (typeof response.data.cafsDetalle != 'undefined') {
                return response.data.cafsDetalle;
              } else {
                return [];
              }
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/cafs/folios/foliosController.js'
            ]);
          }
        ]
      }
    });
  }
]);
