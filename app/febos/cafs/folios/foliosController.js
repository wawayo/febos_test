angular.module('febosApp').controller('foliosCtrl', [
  '$rootScope',
  '$scope',
  '$state',
  '$stateParams',
  'FebosAPI',
  'SesionFebos',
  'cafsDetalles',
  'FebosUtil',
  function(
    $rootScope,
    $scope,
    $state,
    $stateParams,
    FebosAPI,
    SesionFebos,
    cafsDetalles,
    FebosUtil
  ) {
    $scope.validarListado = function() {
      $scope.cafsDetalles = cafsDetalles;

      for (var i = 0; i < $scope.cafsDetalles.length; i++) {
        $scope.cafsDetalles[i].tipoDocumento = FebosUtil.tipoDocumento(
          parseInt($scope.cafsDetalles[i].tipoDTE)
        );
        if ($scope.cafsDetalles[i].tipoCAF == 1) {
          $scope.cafsDetalles[i].tipoFoliacion = 'Automático';
        } else if ($scope.cafsDetalles[i].tipoCAF == 2) {
          $scope.cafsDetalles[i].tipoFoliacion = 'Manual';
        } else if ($scope.cafsDetalles[i].tipoCAF == 3) {
          $scope.cafsDetalles[i].tipoFoliacion = 'Reserva';
        } else {
          $scope.cafsDetalles[i].tipoFoliacion = 'Sin determinar';
        }
      }
    };

    $scope.validarListado();

    // filtros

    $scope.propiedadFiltro = 'fechaVencimiento';
    $scope.reverse = true;

    $scope.sortBy = function(propiedad) {
      $scope.reverse = $scope.propiedadFiltro === propiedad ? !$scope.reverse : false;
      $scope.propiedadFiltro = propiedad;
    };

    $scope.cafActual = {};

    $scope.modalAnularFolios = function(cafDetalle) {
      $scope.anular = {
        texto: 'ANULAR FOLIOS',
        folioInicio: '',
        folioFinal: ''
      };
      $scope.cafActual = cafDetalle;
      UIkit.modal('#modalAnularFolios').show();
    };

    $scope.validarAnularFolio = function() {
      if (isNaN($scope.anular.folioInicio) || isNaN($scope.anular.folioFinal)) {
        UIkit.modal.alert('Debe ingresar un valor numérico para los campos de folio.');
        return false;
      }

      if ($scope.anular.folioFinal <= 0 || $scope.anular.folioInicio <= 0) {
        UIkit.modal.alert('Debe ingresar un valor mayor o igual a 1 para los folios.');
        return false;
      }

      if ($scope.anular.folioInicio > $scope.anular.folioFinal) {
        UIkit.modal.alert('El folio inicial debe ser menor al folio final.');
        return false;
      }

      return true;
    };

    $scope.anularFolios = function() {
      $scope.anular.texto = 'ANULANDO FOLIOS...';

      // Validar formulario
      if ($scope.validarAnularFolio()) {
        FebosAPI.cl_caf_anular_folios(
          {
            empresaId: SesionFebos().empresa.iut,
            cafId: $scope.cafActual.id,
            folioInicio: $scope.anular.folioInicio,
            folioFin: $scope.anular.folioFinal
          },
          {},
          true,
          true
        ).then(function(response) {
          if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
            UIkit.modal.alert(response.data.mensaje);
            $scope.anular.texto = 'ANULAR FOLIOS';
            UIkit.modal('#modalAnularFolios').hide();
            $scope.cargarListado();
          }
        });
      } else {
        $scope.anular.texto = 'ANULAR FOLIOS';
      }
    };

    $scope.cargarListado = function() {
      FebosAPI.cl_caf_listar_folios(
        {
          empresaId: SesionFebos().empresa.iut,
          tipoCAF: $stateParams.tipoCAF,
          tipoDTE: $stateParams.tipoDTE
        },
        true,
        false
      ).then(function(response) {
        if (typeof response.data.cafsDetalle != 'undefined') {
          cafsDetalles = response.data.cafsDetalle;
          $scope.validarListado();
        } else {
          cafsDetalles = [];
        }
      });
    };
  }
]);
