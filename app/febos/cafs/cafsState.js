febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.cafs', {
      url: '/:app/cafs',
      templateUrl: 'app/febos/cafs/cafsView.html',
      controller: 'cafsCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        cafs: [
          'FebosAPI',
          '$stateParams',
          'SesionFebos',
          function(FebosAPI, $stateParams, SesionFebos) {
            return FebosAPI.cl_caf_listar_cafs(
              {
                empresaId: SesionFebos().empresa.id
              },
              {},
              false,
              false
            ).then(function(response) {
              /*
                            if(typeof response.data.cafs != 'undefined') {
                                return response.data.cafs;
                            } else {
                                return [];
                            }*/
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/cafs/cafsController.js'
            ]);
          }
        ]
      }
    });
  }
]);
