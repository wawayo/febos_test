angular.module('febosApp').controller('cafsCtrl', [
  '$rootScope',
  '$scope',
  '$state',
  '$stateParams',
  'FebosAPI',
  'SesionFebos',
  'cafs',
  'FebosUtil',
  function($rootScope, $scope, $state, $stateParams, FebosAPI, SesionFebos, cafs, FebosUtil) {
    $scope.app = $stateParams.app;

    // filtros

    $scope.propiedadFiltro = 'tipoFoliacion';
    $scope.reverse = true;

    $scope.sortBy = function(propiedad) {
      $scope.reverse = $scope.propiedadFiltro === propiedad ? !$scope.reverse : false;
      $scope.propiedadFiltro = propiedad;
    };

    $scope.validarListado = function() {
      if (typeof cafs.cafs != 'undefined') {
        $scope.cafs = cafs.cafs;
      } else {
        console.log('No encontre cafs');
        $scope.cafs = [];
      }

      for (var i = 0; i < $scope.cafs.length; i++) {
        $scope.cafs[i].tipoDocumento = FebosUtil.tipoDocumento(parseInt($scope.cafs[i].tipoDTE));

        if ($scope.cafs[i].tipoCAF == 1) {
          $scope.cafs[i].tipoFoliacion = 'Automático';
        } else if ($scope.cafs[i].tipoCAF == 2) {
          $scope.cafs[i].tipoFoliacion = 'Manual';
        } else if ($scope.cafs[i].tipoCAF == 3) {
          $scope.cafs[i].tipoFoliacion = 'Reserva';
        } else {
          $scope.cafs[i].tipoFoliacion = 'Sin determinar';
        }
      }
    };

    $scope.validarListado();

    $scope.filtroTipo = function(caf) {
      return caf.tipoCAF == 1 || caf.tipoCAF == 2;
    };

    $scope.modalCafDetalle = function(tipoCAF, tipoDTE) {
      UIkit.modal('#modalCafDetalle').show();
      $scope.cafsDetalles = {};
      $scope.cargarCafDetalle(tipoCAF, tipoDTE);
    };

    $scope.cargarCafDetalle = function(tipoCAF, tipoDTE) {
      $scope.cargandoDetalle = true;

      FebosAPI.cl_listar_folios(
        {
          empresaId: SesionFebos().empresa.iut,
          tipoCAF: tipoCAF,
          tipoDTE: tipoDTE
        },
        true,
        false
      ).then(function(response) {
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        $scope.cafsDetalles = response.data.cafsDetalle;

        for (var i = 0; i < $scope.cafsDetalles.length; i++) {
          $scope.cafsDetalles[i].tipoDocumento = FebosUtil.tipoDocumento(
            parseInt($scope.cafsDetalles[i].tipoDTE)
          );
          if ($scope.cafs[i].tipoCAF == 1) {
            $scope.cafs[i].tipoFoliacion = 'Automático';
          } else if ($scope.cafs[i].tipoCAF == 2) {
            $scope.cafs[i].tipoFoliacion = 'Manual';
          } else if ($scope.cafs[i].tipoCAF == 3) {
            $scope.cafs[i].tipoFoliacion = 'Reserva';
          } else {
            $scope.cafs[i].tipoFoliacion = 'Sin determinar';
          }
        }
        $scope.cargandoDetalle = false;
      });
    };

    $scope.modalCargarCAF = function() {
      $scope.carga = {
        texto: 'CARGAR CAF',
        folioInicio: '',
        folioFin: ''
      };
      document.getElementById('archivoCaf').value = '';
      var seleccionado = document.querySelector('input[type="radio"]:checked');

      if (seleccionado != null) {
        seleccionado.checked = false;
      }
      UIkit.modal('#modalCargarCAF').show();
    };

    $scope.parsearXml = function(xml) {
      if (window.DOMParser) {
        var parser = new DOMParser();
        var xmlDoc = parser.parseFromString(xml, 'text/xml');
        return xmlDoc;
      } else {
        //IE
        var xmlDoc = new ActiveXObject('Microsoft.XMLDOM');
        xmlDoc.async = false;
        xmlDoc.loadXML(xml);
        return xmlDoc;
      }
    };

    $scope.archivoCafCargado = function() {
      // Cargar rango de folios
      var archivoCaf = document.getElementById('archivoCaf').files;

      if (archivoCaf.length > 0) {
        if (archivoCaf[0].type === 'text/xml') {
          var reader = new FileReader();
          reader.readAsDataURL(archivoCaf[0]);

          reader.onload = function() {
            var data = reader.result;
            var payload = data.replace(/^[^,]*,/, '');

            var contenidoXml = atob(payload);
            $scope.xmlDoc = $scope.parsearXml(contenidoXml);

            $scope.carga.folioInicio = parseInt(
              $scope.xmlDoc.getElementsByTagName('D')[0].childNodes[0].nodeValue
            );
            $scope.carga.folioFin = parseInt(
              $scope.xmlDoc.getElementsByTagName('H')[0].childNodes[0].nodeValue
            );
            $scope.$apply();
          };
        }
      }
    };

    $scope.validaCargaCaf = function() {
      // Validar formulario
      if (isNaN($scope.carga.folioInicio) || isNaN($scope.carga.folioFin)) {
        $scope.carga.texto = 'CARGAR CAF';
        UIkit.modal.alert('Debe ingresar un valor numérico para los campos de folio.');
        return false;
      }

      // validar que sea un xml de CAF
      var rutEmisor = $scope.xmlDoc.getElementsByTagName('RE')[0].childNodes[0].nodeValue;
      var razonSocial = $scope.xmlDoc.getElementsByTagName('RS')[0].childNodes[0].nodeValue;
      var tipoDte = $scope.xmlDoc.getElementsByTagName('TD')[0].childNodes[0].nodeValue;
      var fechaAut = $scope.xmlDoc.getElementsByTagName('FA')[0].childNodes[0].nodeValue;

      if (rutEmisor.length < 0) {
        UIkit.modal.alert('El archivo no contiene RUT Emisor');
        return false;
      }
      if (razonSocial.length < 0) {
        UIkit.modal.alert('El archivo no contiene Razon Social del Emisor');
        return false;
      }
      if (tipoDte.length < 0) {
        UIkit.modal.alert('El archivo no contiene Tipo de Documento');
        return false;
      }
      if (fechaAut.length < 0) {
        UIkit.modal.alert('El archivo no contiene Fecha de Autorización');
        return false;
      }
      if ($scope.tipoCAF != 1 && $scope.tipoCAF != 2) {
        UIkit.modal.alert('Debe seleccionar una opción para Tipo Foliación');
        return false;
      }
      return true;
    };

    $scope.cargarCaf = function() {
      if ($scope.validaCargaCaf()) {
        var archivoCaf = document.getElementById('archivoCaf').files;

        if (archivoCaf.length > 0) {
          $scope.carga.texto = 'CARGANDO CAF...';

          if (archivoCaf[0].type === 'text/xml') {
            var reader = new FileReader();
            reader.readAsDataURL(archivoCaf[0]);

            reader.onload = function() {
              var data = reader.result;
              var payload = data.replace(/^[^,]*,/, '');

              FebosAPI.cl_caf_cargar_caf(
                {
                  empresaId: SesionFebos().empresa.iut,
                  folioInicio: $scope.carga.folioInicio,
                  folioFin: $scope.carga.folioFin,
                  tipoCAF: $scope.tipoCAF
                },
                {
                  dataCaf: payload
                },
                true,
                true
              ).then(function(response) {
                if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
                  UIkit.modal.alert(response.data.mensaje);
                  $scope.carga.texto = 'CARGAR CAF';
                  var modal = UIkit.modal('#modalCargarCAF');
                  modal.hide();
                  $scope.cargarListado();
                }
              });
            };
          } else {
            console.log('No corresponde el tipo de archivo ' + archivoCaf[0].type);
            UIkit.modal.alert(
              'No corresponde el tipo de archivo ' +
                archivoCaf[0].type +
                '<br>' +
                'Asegúrese de estar cargando un archivo XML'
            );
            $scope.carga.texto = 'CARGAR CAF';
            return false;
          }
        } else {
          UIkit.modal.alert('Por favor seleccionar un archivo .xml');
        }
      } else {
        $scope.carga.texto = 'CARGAR CAF';
      }
    };
    $scope.cargarListado = function() {
      FebosAPI.cl_caf_listar_cafs(
        {
          empresaId: SesionFebos().empresa.id
        },
        {},
        false,
        false
      ).then(function(response) {
        cafs = response.data;
        $scope.validarListado();
      });
    };
  }
]);
