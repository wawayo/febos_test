angular.module('febosApp').controller('tokensCtrl', [
    '$rootScope',
    '$scope',
    '$interval',
    '$timeout',
    '$location',
    '$http',
    '$state',
    'SesionFebos',
    'FebosAPI',
    function ($rootScope, $scope, $interval, $timeout, $location, $http, $state, SesionFebos, FebosAPI) {
            $scope.cargando = true;
            $scope.tokens = [];
            $scope.sesiones = [];
            $scope.creandoToken = false;
            $scope.currentToken = SesionFebos().usuario.token;
            $scope.idtoken = '';
            //console.log("buscando");

            $scope.listar = function (){
              FebosAPI.cl_listar_tokens({
                "usuarioId": SesionFebos().usuario.iut
              },{},true, false)
                .then(function (response) {
                  try {
                      $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
                  } catch (e) {
                  }
                  $scope.cargando = false;
                  if (response.data.codigo == 10) {
                      for (i = 0; i < response.data.tokens.length; i++) {
                          t = response.data.tokens[i];
                          t.eliminando = false;
                          t.actualizando = false;
                          if (response.data.tokens[i].expira) {
                              $scope.sesiones.push(t);
                          } else {
                              $scope.tokens.push(t);
                          }

                      }

                  } else {
                      console.log(response);
                  }
              });
            }

            $scope.eliminar = function (id, alias, permanente) {
              if (typeof alias == 'undefined') {
                  alias = "sin nombre";
              }
              var mensaje = typeof permanente == 'undefined' ? "Esta seguro que desea cerrar esta sesión de forma remota?" : "Esta seguro que desea eliminar el token de integración " + alias + " ?";
              var boton = typeof permanente == 'undefined' ? "Sí, cerrar sesión" : "Sí, eliminar";

              UIkit.modal.confirm(mensaje, function () {
                  for (i = 0; i < $scope.tokens.length; i++) {
                      if ($scope.tokens[i].token == id) {
                          $scope.tokens[i].eliminando = true;
                      }
                  }
                  for (i = 0; i < $scope.sesiones.length; i++) {
                      if ($scope.sesiones[i].token == id) {
                          $scope.sesiones[i].eliminando = true;
                      }
                  }

                  FebosAPI.cl_elimina_tokens({
                    "usuarioId": SesionFebos().usuario.iut,
                    "tokenEspecifico": id
                  }, {}, true, false).then(function success(response) {
                      try {
                          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
                      } catch (e) {
                      }
                      try {
                          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
                      } catch (e) {
                      }
                      if (response.data.codigo == 10) {
                          //TODO: hacer lo necesario si la respuesta es satisfacotria
                          for (i = 0; i < $scope.tokens.length; i++) {
                              if ($scope.tokens[i].token == id) {
                                  $scope.tokens.splice(i, 1);
                              }
                          }
                          for (i = 0; i < $scope.sesiones.length; i++) {
                              if ($scope.sesiones[i].token == id) {
                                  $scope.sesiones.splice(i, 1);
                              }
                          }
                      } else {

                          for (i = 0; i < $scope.tokens.length; i++) {
                              if ($scope.tokens[i].token == id) {
                                  $scope.tokens[i].eliminando = false;
                              }
                          }
                          for (i = 0; i < $scope.sesiones.length; i++) {
                              if ($scope.sesiones[i].token == id) {
                                  $scope.sesiones[i].eliminando = false;
                              }
                          }
                          console.log(response);
                      }
                  });
              }, {labels: {'Ok': boton, 'Cancel': 'No, me acabo de arrepentir'}});
            }

            $scope.nuevo = function () {
                UIkit.modal.prompt("Ingrese un nombre para el token de integración a crear", "Integración SAP", function (alias) {
                $scope.creandoToken = true;

                FebosAPI.cl_crear_token_permanente({
                  "usuarioId": SesionFebos().usuario.iut,
                  "alias": alias
                },{},true, false).then(function success(response) {
                    try {
                        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
                    } catch (e) {
                    }
                    $scope.creandoToken = false;
                    try {
                        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
                    } catch (e) {
                    }
                    if (response.data.codigo == 10) {
                        $scope.tokens.push(response.data.token);
                    } else {
                        console.log(response);
                    }

                });
                }, {labels: {'Ok': "Crear nuevo token", 'Cancel': 'Cancelar, me acabo de arrepentir'}});
            }

            $scope.editar = function (id, alias) {
              UIkit.modal.prompt("Modifique el nombre de la integración", alias, function (alias) {
                FebosAPI.cl_actualizar_token_permanente({
                  "usuarioId": SesionFebos().usuario.iut,
                  "tokenEspecifico": id,
                  "alias": alias
                },{},true,false).then(function success(response) {
                    var posicion=0;
                    for (var i = 0; i < $scope.tokens.length; i++) {
                        if ($scope.tokens[i].token == id) {
                            posicion=i;
                            $scope.tokens[i].actualizando = false;
                        }
                    }

                    try {
                        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
                    } catch (e) {
                    }
                    try {
                        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
                    } catch (e) {
                    }
                    if (response.data.codigo == 10) {
                        $scope.tokens[posicion].alias=alias;
                    } else {
                        console.log(response);
                    }

                });
              }, {labels: {'Ok': "Modificar nombre", 'Cancel': 'Cancelar, me acabo de arrepentir'}});
            }

            $scope.ver=function(id){
                //UIkit.modal.alert(id,{labels: {'Ok': "Cerrar"}});
                $scope.idtoken = id;
                var modal = UIkit.modal("#modal_token");
                modal.show();
            }

            $scope.listar();
        }
]);
