febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.logs", {
                            url: "/:app/seguridad/logs?seguimientoId",
                            templateUrl: 'app/febos/seguridad/registros/registrosView.html',
                            controller: 'registrosCtrl',
                            resolve: {
                                AYUDA: function ($http) {
                                    return $http({method: 'GET', url: 'app/ayuda/logs.json'})
                                            .then(function (data) {
                                                console.log(data);
                                                return data.data;
                                            }, function (data) {
                                                return {};
                                            });
                                },
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            'lazy_uikit',
                                            'lazy_iCheck',
                                            'app/febos/seguridad/registros/registrosController.js'
                                        ]);
                                    }]
                            }
                        })

            }]);