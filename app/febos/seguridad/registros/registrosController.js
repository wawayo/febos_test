angular
  .module('febosApp')
  .controller('registrosCtrl', function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    SesionFebos,
    FebosAPI,
    $http,
    $state
  ) {
    var sid = $location.search().seguimientoId;
    $scope.cargando = false;
    $scope.query = '';
    $scope.datos = {};
    if (typeof sid == 'undefined') {
      $scope.datos.estado = 0; //no hay sid, hay que ingresar 1
      $scope.datos.seguimientoId = '';
    } else {
      $scope.datos.estado = 1; //hay un seguimiento id ingresado
      $scope.datos.seguimientoId = sid;
    }

    $scope.seguimiento = function() {
      setTimeout(function() {
        //console.log("haciendo click!");
        $('.uk-modal-dialog-replace input')[0].focus();
      }, 200);
      UIkit.modal.prompt(
        'Ingresa el ID de Seguimiento',
        '',
        function(sid) {
          $scope.datos.estado = 1; //hay un seguimiento id ingresado
          $scope.datos.seguimientoId = sid;
          $scope.buscar();
        },
        { labels: { Ok: 'Buscar', Cancel: 'Cancelar' } }
      );
    };

    //93a3daee7a42304f9249120-1d7d3c9c8849
    $scope.buscar = function() {
      $scope.cargando = true;
      $scope.logs = [];
      //$rootScope.content_preloader_show();
      $scope.datos.estado = 1;
      //console.log("Buscando seguimientoID: " + $scope.datos.seguimientoId);

      /*
                req = {
                    method: 'GET',
                    url: febosSingleton.api + "/logs/" + $scope.datos.seguimientoId
                };
                $http(req).then(function (response) {
                    console.log(response);
                    $scope.cargando = false;
                    $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
                    //$rootScope.content_preloader_hide();
                    if (response.data.codigo == 10) {
                        $scope.logs = response.data.logs;
                    } else {
                        febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
                    }
                }, function (response) {
                    $scope.cargando = false;
                    $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
                    //$rootScope.content_preloader_hide();
                    febosSingleton.error(response.data.codigo, response.data.mensaje, response.data.seguimientoId, response.data.errores);
                });*/

      //5fa8f4f18b4e904d6158011-e06764270982
      FebosAPI.cl_historial_eventos(
        {
          seguimientoId: new String($scope.datos.seguimientoId).trim()
        },
        true,
        false
      ).then(function(response) {
        console.log(response);
        $scope.cargando = false;
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        //$rootScope.content_preloader_hide();
        if (response.data.codigo == 10) {
          $scope.logs = response.data.logs;
        }
      });
    };

    if (typeof $scope.datos.seguimientoId != 'undefined' && $scope.datos.seguimientoId != '') {
      $scope.buscar();
    }
    $scope.logs = [];

    function replaceText(str) {
      var str1 = String(str);
      return str1.replace(/\n/g, '<br/>');
    }

    $scope.ver = function(tipo, log) {
      //$scope.texto=$scope.mostrarJson(log.objeto);
      //console.log(tipo,log);
      try {
        src = '';
        $scope.icono = tipo;
        //console.log("tipo", tipo);
        //console.log("log", log);
        switch (tipo) {
          case 'objeto':
            //console.log("mostrando objeto");
            obj = JSON.parse(log.objeto);
            src = JSON.stringify(obj, null, 2);
            $scope.titulo = 'Objeto adjunto';
            break;
          case 'mensaje':
            //console.log("mostrando mensaje");
            $scope.titulo = 'Mensaje completo';
            src = log.mensaje
              .replace(/>/g, '&gt;')
              .replace(/</g, '&lt;')
              .replace(/"/g, '&quot;');
            //console.log("src", src);
            break;
          case 'stacktrace':
            //console.log("mostrando stacktrace");
            $scope.titulo = 'Stacktrace';
            for (i = 0; i < log.stacktrace.length; i++) {
              src += log.stacktrace[i] + '\n';
            }
            break;
        }

        document.getElementById('codigo_wrapper').innerHTML =
          '<pre id="codigo" class="prettyprint linenums"></pre>';
        //document.getElementById("codigo").html = src;
        document.getElementById('codigo').innerHTML = src;
        var modal = UIkit.modal('#modal_log');
        modal.show();
        PR.prettyPrint();
      } catch (e) {
        console.log(e);
      }
    };
    if (sid == '' || typeof sid == 'undefined') {
      $scope.seguimiento();
    }
    $scope.filtrar = function(item) {
      if ($scope.query == '') return true;
      if (item.fecha.toLowerCase().indexOf($scope.query.toLowerCase()) >= 0) {
        return true;
      }
      if (item.mensaje.toLowerCase().indexOf($scope.query.toLowerCase()) >= 0) {
        return true;
      }
      if (item.tipo.toLowerCase().indexOf($scope.query.toLowerCase()) >= 0) {
        return true;
      }
      for (var i = 0; i < item.stacktrace.length; i++) {
        if (item.stacktrace[i].indexOf($scope.query.toLowerCase()) >= 0) {
          return true;
        }
      }
      return false;
    };
  });
