angular.module('febosApp').controller('cafsStoreDeliveryCtrl', [
  '$rootScope',
  '$scope',
  '$state',
  '$stateParams',
  'FebosAPI',
  'SesionFebos',
  'cafsStoreDelivery',
  'FebosUtil',
  function(
    $rootScope,
    $scope,
    $state,
    $stateParams,
    FebosAPI,
    SesionFebos,
    cafsStoreDelivery,
    FebosUtil
  ) {
    if (typeof cafsStoreDelivery.entregas != 'undefined') {
      console.log('Encontre caf stores: ' + cafsStoreDelivery.entregas.length);
      $scope.cafsStoreDelivery = cafsStoreDelivery.entregas;
    } else {
      console.log('No encontre caf stores');
      $scope.cafsStoreDelivery = [];
    }

    $scope.propiedadFiltro = 'fechaEntrega';
    $scope.reverse = true;

    $scope.sortBy = function(propiedad) {
      $scope.reverse = $scope.propiedadFiltro === propiedad ? !$scope.reverse : false;
      $scope.propiedadFiltro = propiedad;
    };
  }
]);
