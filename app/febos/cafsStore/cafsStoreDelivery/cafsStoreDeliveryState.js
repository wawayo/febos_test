febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.entregaDeFolios', {
      url: '/:app/cafsDeReserva/:cafId',
      templateUrl: 'app/febos/cafsStore/cafsStoreDelivery/cafsStoreDeliveryView.html',
      controller: 'cafsStoreDeliveryCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        cafsStoreDelivery: [
          'FebosAPI',
          '$stateParams',
          'SesionFebos',
          function(FebosAPI, $stateParams, SesionFebos) {
            return FebosAPI.cl_caf_listar_cafs_store_delivery(
              {
                empresaId: SesionFebos().empresa.id,
                cafId: $stateParams.cafId
              },
              false,
              false
            ).then(function(response) {
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/cafsStore/cafsStoreDelivery/cafsStoreDeliveryController.js'
            ]);
          }
        ]
      }
    });
  }
]);
