febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('restringido.cafsDeReserva', {
      url: '/:app/cafsDeReserva',
      templateUrl: 'app/febos/cafsStore/cafsStoreView.html',
      controller: 'cafsStoreCtrl',
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        cafStores: [
          'FebosAPI',
          '$stateParams',
          'SesionFebos',
          function(FebosAPI, $stateParams, SesionFebos) {
            return FebosAPI.cl_caf_listar_cafs_store(
              {
                empresaId: SesionFebos().empresa.id,
                numeroPagina: 1,
                filasPorPagina: 200,
                tipoCAF: 3
              },
              {},
              false,
              false
            ).then(function(response) {
              console.log(response);
              console.log(response.data);
              return response.data;
            });
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'app/febos/cafsStore/cafsStoreController.js'
            ]);
          }
        ]
      }
    });
  }
]);
