febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state('especial.selector_partner', {
      url: '/:app/partners/seleccion',
      templateUrl: 'app/febos/partners/selector/selectorEmpresaView.html',
      controller: 'selectorEmpresaCtrl',
      reloadOnSearch: false,
      data: {
        pageTitle: 'Selecciona tu empresa'
      },
      resolve: {
        EstadoAnterior: [
          '$state',
          function($state) {
            var currentStateData = {
              estado: $state.current.name,
              parametros: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ],
        Autorizacion: [
          'Autorizacion',
          function(Autorizacion) {
            try {
              Autorizacion.verificar();
            } catch (e) {
              console.log('Error al autenticar', e);
            }
            return;
          }
        ],
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              //'lazy_uikit',
              //'lazy_iCheck',
              'app/febos/partners/selector/selectorEmpresaController.js'
            ]);
          }
        ]
      }
    });
  }
]);
