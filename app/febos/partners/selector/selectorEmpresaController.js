angular.module('febosApp').controller('selectorEmpresaCtrl', [
  '$rootScope',
  '$scope',
  '$timeout',
  'variables',
  '$location',
  'SesionFebos',
  'FebosUtil',
  '$state',
  '$stateParams',
  '$window',
  'EstadoAnterior',
  'FebosAPI',
  'FebosQueueAPI',
  'CONFIGURACION',
  function(
    $rootScope,
    $scope,
    $timeout,
    variables,
    $location,
    SesionFebos,
    FebosUtil,
    $state,
    $stateParams,
    $window,
    EstadoAnterior,
    FebosAPI,
    FebosQueueAPI,
    CONFIGURACION
  ) {
    $scope.empresas = SesionFebos().misEmpresas;
    $scope.empresaActual = SesionFebos().empresa;
    if ($scope.empresas.length == 1) {
      SesionFebos().empresa = $scope.empresas[0];
      // $location.path("/" + $stateParams.app + "/inicio");
    }
    $scope.tipo = 'PRINCIPAL';
    $scope.app = $stateParams.app;
    $scope.filtro = '';
    $scope.seleccionar = function(empresa) {
      console.log($scope);
      if ($scope.tipo == 'PRINCIPAL') {
        $scope.empresaActual = empresa;
        SesionFebos().empresa = empresa;
      } else {
        if ($stateParams.app == 'proveedores') {
          SesionFebos().cliente = empresa;
        } else {
          SesionFebos().proveedor = empresa;
        }
      }
      console.log($scope);
      if ($scope.esPortalComplementario() && $scope.tipo == 'PRINCIPAL') {
        console.log($scope);
        $scope.tipo = 'SECUNDARIO';
        console.log($scope);
        $scope.filtro = '';
        $scope.setearEmpresaSegunSentido(empresa);
        if (!$scope.tieneEmpresasEnCache()) {
          $rootScope.blockModal = UIkit.modal.blockUI(
            "<div class='uk-text-center'>Danos unos segundos, estamos buscando quienes son tus " +
              ($stateParams.app == 'proveedores' ? 'clientes' : 'proveedores') +
              "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
          );
          FebosAPI.cl_listar_empresas(
            {
              pagina: 1,
              filas: 10000,
              filtro: '',
              busquedaSimple: 'si',
              ambito: FebosUtil.obtenerAmbito()
            },
            {},
            true,
            true
          ).then(function(response) {
            $scope.empresas = response.data.empresas;
            if ($stateParams.app == 'proveedores') {
              SesionFebos().misClientes[SesionFebos().empresa.iut + '-' + CONFIGURACION.DOMINIO] =
                response.data.empresas;
            } else {
              SesionFebos().misProveedores[
                SesionFebos().empresa.iut + '-' + CONFIGURACION.DOMINIO
              ] =
                response.data.empresas;
            }
          });
        } else {
          if ($stateParams.app == 'proveedores') {
            $scope.empresas = SesionFebos().misClientes[
              SesionFebos().empresa.iut + '-' + CONFIGURACION.DOMINIO
            ];
          } else {
            $scope.empresas = SesionFebos().misProveedores[
              SesionFebos().empresa.iut + '-' + CONFIGURACION.DOMINIO
            ];
          }
          FebosQueueAPI(
            'cl_listar_empresas',
            {
              pagina: 1,
              filas: 10000,
              filtro: $scope.filtro,
              busquedaSimple: 'si',
              ambito: FebosUtil.obtenerAmbito()
            },
            {}
          ).then(function(response) {
            if ($stateParams.app == 'proveedores') {
              SesionFebos().misClientes[SesionFebos().empresa.iut + '-' + CONFIGURACION.DOMINIO] =
                response.data.empresas;
            } else {
              SesionFebos().misProveedores[
                SesionFebos().empresa.iut + '-' + CONFIGURACION.DOMINIO
              ] =
                response.data.empresas;
            }
          });
        }
      } else {
        console.log($scope);
        if (EstadoAnterior.url == null) {
          console.log($scope);
          $location.path('/' + $stateParams.app + '/inicio');
        } else {
          console.log($scope);
          var url = EstadoAnterior.url.replace('#', '');
          console.log('URL ANTERIOR:' + url);
          if (
            url !== null &&
            !url.includes('/cargando') &&
            !url.includes('/ingreso') &&
            !url.includes('/cambio_clave')
          ) {
            if (url.includes('?')) {
              url = url.substring(1, url.indexOf('?'));
            }
            console.log('REDIRECCIONANDO A: ' + url);
            $location.path(url);
          } else {
            $location.path('/' + $stateParams.app + '/inicio');
          }
        }
      }
    };
    $scope.tieneEmpresasEnCache = function() {
      if (
        $stateParams.app == 'proveedores' &&
        jQuery.isEmptyObject(
          SesionFebos().misClientes[SesionFebos().empresa.iut + '-' + CONFIGURACION.DOMINIO]
        )
      ) {
        //$scope.tipo = "CLIENTE";
        return false;
      }
      if (
        $stateParams.app == 'cliente' &&
        jQuery.isEmptyObject(
          SesionFebos().misProveedores[SesionFebos().empresa.iut + '-' + CONFIGURACION.DOMINIO]
        )
      ) {
        //  $scope.tipo = "PROVEEDOR";
        return false;
      }
      return true;
    };
    $scope.setearEmpresaSegunSentido = function(empresa) {
      if ($stateParams.app == 'proveedores') {
        SesionFebos().proveedor = empresa;
      } else {
        SesionFebos().cliente = empresa;
      }
    };
    $scope.esPortalComplementario = function() {
      return $stateParams.app == 'proveedores' || $stateParams.app == 'cliente';
    };

    $scope.volver = function() {
      //  $window.history.back();
      $scope.tipo = 'PRINCIPAL';
      $scope.empresas = SesionFebos().misEmpresas;
    };
    $scope.buscar = function(item) {
      if (
        item.id.toLowerCase().includes($scope.filtro.toLowerCase()) ||
        item.razonSocial.toLowerCase().includes($scope.filtro.toLowerCase()) ||
        item.iut.toLowerCase().includes($scope.filtro.toLowerCase()) ||
        item.fantasia.toLowerCase().includes($scope.filtro.toLowerCase())
      ) {
        return true;
      }
      return false;
      //return false; // otherwise it won't be within the results
    };
    if (typeof SesionFebos().empresa.id != 'undefined' && SesionFebos().empresa.id != '') {
      $scope.filtro = SesionFebos().empresa.fantasia;
    }
  }
]);
