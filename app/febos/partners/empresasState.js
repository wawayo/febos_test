febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('restringido.perfil_partner', {
        url: '/:app/partners/perfil',
        templateUrl: 'app/febos/partners/modificar/modificarEmpresasView.html',
        controller: 'modificarEmpresaCtrl',
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_KendoUI',
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/partners/modificar/modificarEmpresasController.js'
              ]);
            }
          ]
        }
      })
      .state('restringido.partners', {
        url: '/:app/partners',
        templateUrl: 'app/febos/partners/gestionEmpresasView.html',
        controller: 'gestionEmpresasCtrl',
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/partners/gestionEmpresasController.js'
              ]);
            }
          ]
        }
      });
  }
]);
