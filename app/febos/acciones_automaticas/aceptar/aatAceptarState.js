'use strict';
febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider) {
    $stateProvider.state('restringido.aat_aceptar', {
      url: '/:app/acciones_automaticas/aceptar',
      templateUrl: 'app/febos/acciones_automaticas/aceptar/aatAceptarView.html',
      controller: 'aatAceptarCtrl',
      resolve: {
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'lazy_ionRangeSlider',
              'app/febos/acciones_automaticas/aceptar/aatAceptarController.min.js'
            ]);
          }
        ]
      }
    });
  }
]);
