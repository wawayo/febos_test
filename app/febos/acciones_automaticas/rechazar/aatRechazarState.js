'use strict';
febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider) {
    $stateProvider.state('restringido.aat_rechazar', {
      url: '/:app/acciones_automaticas/rechazar',
      templateUrl: 'app/febos/acciones_automaticas/rechazar/aatRechazarView.html',
      controller: 'aatRechazarCtrl',
      resolve: {
        deps: [
          '$ocLazyLoad',
          function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'lazy_uikit',
              'lazy_iCheck',
              'lazy_uiSelect',
              'lazy_selectizeJS',
              'lazy_ionRangeSlider',
              'app/febos/acciones_automaticas/rechazar/aatRechazarController.min.js'
            ]);
          }
        ]
      }
    });
  }
]);
