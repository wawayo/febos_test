angular
  .module('febosApp')
  .controller('aatRechazarCtrl', function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    $http,
    $state,
    utils,
    FebosAPI,
    FebosUtil,
    SesionFebos,
    $window
  ) {
    $scope.estadosSeleccionados = [];
    $scope.estadoRecepcion = false;

    $scope.compararFechas = false;
    $scope.cargando = false;
    $scope.accionAutomaticaEditable = {};
    $scope.listas = [{ nombre: 'Ninguna', listaId: '*' }];

    FebosAPI.cl_listar_listas(
      {
        empresaId: SesionFebos().empresa.id
      },
      {},
      true,
      false
    ).then(function success(response) {
      try {
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
      } catch (e) {}
      $scope.cargando = false;
      if (response.data.codigo === 10) {
        $scope.listas = response.data.listas;
      } else {
        SesionFebos().error(
          response.data.codigo,
          response.data.mensaje,
          response.data.seguimientoId,
          response.data.errores
        );
      }
    });

    $scope.estadosComerciales = [
      'Sin acción',
      'Pre Aceptado',
      'Aceptado SII',
      'Pre Rechazado',
      'Rechazado SII',
      'Reclamo Parcial SII',
      'Reclamo Total SII',
      'Recibo Mercaderías SII'
    ];

    $scope.crearReglaModal = function() {
      var modal = UIkit.modal('#agregarRegla');
      modal.show();
    };

    $scope.$watch('selected', function(nowSelected) {
      $scope.estadosSeleccionados = [];
      if (!nowSelected) {
        // here we've initialized selected already
        // but sometimes that's not the case
        // then we get null or undefined
        return;
      }
      angular.forEach(nowSelected, function(val) {
        $scope.estadosSeleccionados.push(val);
      });
    });

    $scope.$watch('selected', function(nowSelected) {
      $scope.editarEstadosSeleccionados = [];
      if (!nowSelected) {
        // here we've initialized selected already
        // but sometimes that's not the case
        // then we get null or undefined
        return;
      }
      angular.forEach(nowSelected, function(val) {
        $scope.editarEstadosSeleccionados.push(val);
      });
    });

    $scope.seleccionarHora = function(seleccionado) {
      $scope.horasDia = seleccionado;
    };

    $scope.limpiarCampos = function() {
      $scope.estadosSeleccionados = [];
      $scope.recepcionAmbas = false;
      $scope.recepcionRecibidas = false;
      $scope.horasDia = '';
      $scope.compararFechas = false;
      $scope.diasRecepcion = '0';
      $scope.motivoRegla = '';
      $scope.filtroMonto = '';
      $scope.comparacionMonto = '';
      $scope.rutEmisores = '';
      $scope.listaInclusion = '*';
      $scope.listaExclusion = '*';
    };
    $scope.limpiarCampos();
    $scope.unirTags = function(modelo) {
      var input = [];
      for (var i = 0; i < modelo.length; i++) {
        input.push(modelo[i].text);
      }
      return input.join(',');
    };
    $scope.separarTags = function(valor) {
      var valores = valor.split(',');
      var tags = [];
      for (var i = 0; i < valores.length; i++) {
        tags.push({ text: valores[i] });
      }
      return tags;
    };
    $scope.nombreLista = function(listaId) {
      for (var i = 0; i < $scope.listas.length; i++) {
        if ($scope.listas[i].listaId == listaId) return $scope.listas[i].nombre;
      }
      return 'Lista inexistente';
    };
    $scope.crearRegla = function() {
      var dias = $scope.diasRecepcion;
      var seleccionados = $scope.estadosSeleccionados.join();

      var body = {
        filtros: {
          estadoComercial: seleccionados,
          recibido: $scope.recepcionAmbas ? 'si,no' : $scope.recepcionRecibidas ? 'si' : 'no',
          dia: dias,
          compararFechas: $scope.compararFechas ? 'si' : 'no',
          tipoFecha: 'fechaRecepcionSii',
          emisores: $scope.unirTags($scope.rutEmisores),
          monto: $scope.filtroMonto != '' ? $scope.comparacionMonto + $scope.filtroMonto : '*',
          listaEmisoresIncluidos: $scope.listaInclusion,
          listaEmisoresExcluidos: $scope.listaExclusion
        },
        parametros: {
          motivoRechazo: $scope.motivoRegla
        }
      };
      console.log('FILTROS', body);
      var query = {
        tipoElemento: 'DTE',
        tipoAccion: 'RechazoAutomatico',
        hora: $scope.horasDia,
        activa: $scope.estadoRegla
      };

      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'> Agregando regla " +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_agregar_aat(
        {
          tipoElemento: 'DTE',
          tipoAccion: 'RechazoAutomatico',
          hora: $scope.horasDia,
          activa: $scope.estadoRegla ? 'si' : 'no'
        },
        body,
        false,
        true
      ).then(function(response) {
        $rootScope.blockModal.hide();
        if (response.data.codigo == 10) {
          UIkit.notify(
            "<i class='uk-icon-check'></i>¡Genial!, has creado una regla de rechazo automático.",
            {
              status: 'success',
              timeout: 5000,
              pos: 'top-center'
            }
          );
          $scope.limpiarCampos();
          var modal = UIkit.modal('#agregarRegla');
          modal.hide();
          $scope.listarAccionesAutomaticas();
        } else {
          var msg = response.data.mensaje;
          if (typeof response.data.errores !== 'undefined' && response.data.errores.length > 0) {
            msg += ': <br/><ul>';
            for (var i = 0; i < response.data.errores.length; i++) {
              msg += '<li>' + response.data.errores[i] + '</li>';
            }
            msg += '</ul>';
          }
          UIkit.modal.alert(msg);
        }
      });
    };

    $scope.listarAccionesAutomaticas = function() {
      $scope.cargando = true;
      FebosAPI.cl_listar_aat({}, {}, true, false).then(function(response) {
        var accionesAutomaticas = response.data.accionesAutomaticas;
        $scope.accionesAutomaticas = [];
        for (var i = 0; i < accionesAutomaticas.length; i++) {
          if (accionesAutomaticas[i].tipoAccion != 'RechazoAutomatico') continue;
          console.log('comprar fechas 1 ' + accionesAutomaticas[i].filtros.compararFechas);
          var compararFechas = accionesAutomaticas[i].filtros.compararFechas == 'si' ? true : false;
          console.log('comprar fechas 2 ' + compararFechas);
          accionesAutomaticas[i].filtros.compararFechas = compararFechas;
          var simbolo = accionesAutomaticas[i].filtros.monto;
          simbolo = simbolo.substring(0, 1);
          accionesAutomaticas[i].filtros.simboloMonto = simbolo;
          if (accionesAutomaticas[i].filtros.monto != '*') {
            var numero = accionesAutomaticas[i].filtros.monto;
            numero = numero.substring(1);
            numero = FebosUtil.formatoMonto(numero, '$');
            accionesAutomaticas[i].filtros.numeroMonto = numero;
          }
          $scope.accionesAutomaticas.push(accionesAutomaticas[i]);
        }
        $scope.cargando = false;
      });
    };

    $scope.listarAccionesAutomaticas();

    $scope.abrirModalEditar = function(rechazo) {
      $scope.accionAutomaticaEditable = angular.copy(rechazo);
      $scope.horasDiaEditar = rechazo.hora + '';
      $scope.estadoReglaEditar = rechazo.activa;
      $scope.recepcionRecibidasEditar = rechazo.filtros.recibido == 'si';
      $scope.recepcionAmbasEditar =
        rechazo.filtros.recibido == 'si,no' || rechazo.filtros.recibido == 'no,si';
      var dia = parseInt($scope.accionAutomaticaEditable.filtros.dia, 10);
      $scope.accionAutomaticaEditable.filtros.dia = dia;
      $scope.editarEstadosSeleccionados = rechazo.filtros.estadoComercial.split(',');
      $scope.editarRutEmisores =
        rechazo.filtros.emisores != '*' ? $scope.separarTags(rechazo.filtros.emisores) : '';
      $scope.editarListaInclusion = rechazo.filtros.listaEmisoresIncluidos;
      $scope.editarListaExclusion = rechazo.filtros.listaEmisoresExcluidos;
      $scope.editarComparacionMonto =
        rechazo.filtros.monto == '*' ? '' : rechazo.filtros.monto.substring(0, 1);
      $scope.editarFiltroMonto =
        rechazo.filtros.monto == '*' ? '' : rechazo.filtros.monto.substring(1);
      var compararFechas = $scope.accionAutomaticaEditable.filtros.compararFechas;
      $scope.accionAutomaticaEditable.filtros.compararFechas =
        compararFechas == 'si' || compararFechas == true;
      var modal = UIkit.modal('#editarRegla');
      modal.show();
    };

    $scope.editarRegla = function() {
      var dias = $scope.accionAutomaticaEditable.filtros.dia;
      var seleccionados = $scope.editarEstadosSeleccionados.join();

      var bodyEditar = {
        filtros: {
          estadoComercial: seleccionados,
          compararFechas: $scope.accionAutomaticaEditable.filtros.compararFechas ? 'si' : 'no',
          recibido: $scope.recepcionAmbasEditar
            ? 'si,no'
            : $scope.recepcionRecibidasEditar
              ? 'si'
              : 'no',
          dia: dias,
          tipoFecha: 'fechaRecepcionSii',
          emisores: $scope.unirTags($scope.editarRutEmisores),
          monto:
            $scope.editarFiltroMonto != '' && $scope.editarFiltroMonto != '*'
              ? $scope.editarComparacionMonto + $scope.editarFiltroMonto
              : '*',
          listaEmisoresIncluidos: $scope.editarListaInclusion,
          listaEmisoresExcluidos: $scope.editarListaExclusion
        },
        parametros: {
          motivoRechazo: $scope.accionAutomaticaEditable.parametros.motivoRechazo
        }
      };
      $rootScope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'> Modificando regla " +
          "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      FebosAPI.cl_actualizar_aat(
        {
          tipoElemento: 'DTE',
          tipoAccion: 'RechazoAutomatico',
          hora: $scope.horasDiaEditar,
          activa: $scope.estadoReglaEditar ? 'si' : 'no',
          accionAutomaticaId: $scope.accionAutomaticaEditable.accionAutomaticaId
        },
        bodyEditar,
        false,
        true
      ).then(function(response) {
        $rootScope.blockModal.hide();
        if (response.data.codigo == 10) {
          UIkit.notify("<i class='uk-icon-check'></i>¡Has editado esta regla!", {
            status: 'success',
            timeout: 5000,
            pos: 'top-center'
          });
          $scope.limpiarCampos();
          var modal = UIkit.modal('#editarRegla');
          modal.hide();
          // $window.location.reload();
          $scope.listarAccionesAutomaticas();
        } else {
          var msg = response.data.mensaje;
          if (typeof response.data.errores !== 'undefined' && response.data.errores.length > 0) {
            msg += ': <br/><ul>';
            for (var i = 0; i < response.data.errores.length; i++) {
              msg += '<li>' + response.data.errores[i] + '</li>';
            }
            msg += '</ul>';
          }
          UIkit.modal.alert(msg);
        }
      });
    };

    $scope.removerRegla = function(rechazo) {
      UIkit.modal.confirm(
        '¿Deseas eliminar esta regla? ',
        function() {
          $rootScope.blockModal = UIkit.modal.blockUI(
            "<div class='uk-text-center'> Eliminando regla " +
              "<br/><img class='uk-margin-top' width='50px' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
          );
          FebosAPI.cl_borrar_aat(
            {
              accionAutomaticaId: rechazo.accionAutomaticaId
            },
            {},
            false,
            true
          ).then(function(response) {
            $rootScope.blockModal.hide();
            if (response.data.codigo == 10) {
              UIkit.notify("<i class='uk-icon-check'></i>¡Se eliminó la regla!", {
                status: 'success',
                timeout: 5000,
                pos: 'top-center'
              });
              $scope.listarAccionesAutomaticas();
              // $window.location.reload()
            } else {
              var msg = response.data.mensaje;
              if (
                typeof response.data.errores !== 'undefined' &&
                response.data.errores.length > 0
              ) {
                msg += ': <br/><ul>';
                for (var i = 0; i < response.data.errores.length; i++) {
                  msg += '<li>' + response.data.errores[i] + '</li>';
                }
                msg += '</ul>';
              }
              UIkit.modal.alert(msg);
            }
          });
        },
        function() {
          //console.log('no hace nada');
        }
      );
    };
  });
