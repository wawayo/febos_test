angular.module('febosApp').controller('webhookCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$http',
  'utils',
  '$state',
  '$location',
  'SesionFebos',
  'FebosAPI',
  'FebosUtil',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $http,
    utils,
    $state,
    $location,
    SesionFebos,
    FebosAPI,
    FebosUtil
  ) {
    console.log('ENTRANDO A WEBOOK');

    $scope.usuarioNombre = SesionFebos().usuario.nombre;
    $scope.datos = {
      nombreWH: '',
      url: '',
      token: '',
      usuario: '',
      password: '',
      tipo: '',
      formato: 'JSON',
      nombreWebhook: '',
      nombreRegla: '',
      reglas:
        "dte.Encabezado.IdDoc.TipoDTE == 33 \n&& dte.Encabezado.Emisor.RUTEmisor == '12345678-9' \n&& dte.Encabezado.Totales.MntNeto>9999",
      reintentos: '',
      prioridad: 0,
      notificaciones: false,
      adjuntarDte: false,
      adjuntarDnt: false,
      adjuntarPdf: false,
      correos: [],
      textoCrear: 'CREAR',
      condicion: '',
      continuar: false,
      activa: false,
      flujo: '',
      ninterno: false,
      ntelegram: false,
      ncorreo: false
    };

    $scope.datos.crearRegla = {
      nombreRegla: '',
      prioridad: 0,
      descripcion: '',
      continuar: false,
      activa: false,
      condicion: ''
    };

    $scope.datos.modificarWebhook = {
      id: '',
      nombre: '',
      formato: '',
      url: '',
      token: '',
      usuario: '',
      password: '',
      tipo: '',
      flujo: '',
      reintentos: '',
      descripcion: '',
      notificaciones: false,
      adjuntarDte: false,
      adjuntarDnt: false,
      adjuntarPdf: false,
      continuar: false,
      activo: false,
      correos: [],
      textoModificar: 'Modificar'
    };

    $scope.datos.modificarRegla = {
      reglaId: '',
      webhookId: '',
      nombreRegla: '',
      prioridad: 0,
      descripcion: '',
      continuar: false,
      activa: false,
      condicion: ''
    };

    $scope.datos.modificarSuscripcion = {
      webhookId: '',
      ninterno: false,
      ncorreo: false,
      ntelegram: false
    };

    $scope.radioOptions1 = 'json';
    $scope.radioOptions2 = 'xml';
    $scope.flujos = [];

    $scope.config = {
      cargando: false,
      cargandoReglas: false,
      cargandoModificarWH: false,
      cargandoCrearRegla: false,
      cargandoSuscripciones: false,
      cargandoModificarSuscripciones: false,
      verboModificar: 'Modificar',
      verboCrear: 'Crear',
      cargandoCrear: false,
      tiposDeEvento: [
        { nombre: 'RECEPCION_ARCHIVO_INTEGRACION_TXT', valor: '1' },
        { nombre: 'RECEPCION_ARCHIVO_INTEGRACION_XML', valor: '2' },
        { nombre: 'ERROR_INTEGRACION', valor: '3' },
        { nombre: 'DOCUMENTO_PREPROCESADO', valor: '4' },
        { nombre: 'DOCUMENTO_FIRMADO', valor: '5' },
        { nombre: 'DOCUMENTO_ENVIADO_EAI', valor: '6' },
        { nombre: 'DOCUMENTO_RECEPCIONADO_EAI', valor: '7' },
        { nombre: 'ERROR_AL_ENVIAR_A_EAI', valor: '8' },
        { nombre: 'ERROR_RESPUESTA_EAI', valor: '9' },
        { nombre: 'DOCUMENTO_ACEPTADO_EAI', valor: '10' },
        { nombre: 'DOCUMENTO_ACEPTADO_REPAROS_EAI', valor: '11' },
        { nombre: 'DOCUMENTO_RECHAZADO_EAI', valor: '12' },
        { nombre: 'DOCUMENTO_ENVIADO_A_RECEPTOR', valor: '13' },
        { nombre: 'DOCUMENTO_ENVIADO_A_DIRECCION_PERSONALIZADA', valor: '14' },
        { nombre: 'DOCUMENTO_RECIBIDO_POR_RECEPTOR', valor: '15' },
        { nombre: 'ENVIO_RECHAZADO_POR_RECEPTOR', valor: '16' },
        { nombre: 'DOCUMENTO_ACEPTADO_COMERCIALEMENTE_POR_RECEPTOR', valor: '17' },
        { nombre: 'DOCUMENTO_ACEPTADO_REPAROS_COMERCIALEMENTE_POR_RECEPTOR', valor: '18' },
        { nombre: 'DOCUMENTO_RECHAZADO_COMERCIALEMENTE_POR_RECEPTOR', valor: '19' },
        { nombre: 'ENVIO_RECIBO_DE_MERCADERIAS', valor: '20' },
        { nombre: 'ENVIO_RECIBO_DE_MERCADERIAS_PARCIAL', valor: '21' },
        { nombre: 'DEVOLUCION_TOTAL_DE_MERCADERIAS', valor: '22' },
        { nombre: 'DEVOLUCION_PARCIAL_DE_MERCADERIAS', valor: '23' },
        { nombre: 'COMENTARIO_DE_USUARIO', valor: '24' },
        { nombre: 'ASIGNACION_FOLIO', valor: '25' },
        { nombre: 'ASIGNACION_CAF', valor: '26' },
        { nombre: 'ALMACENAMIENTO_XML_DOCUMENTO', valor: '27' },
        { nombre: 'VISUALIZACION_XML', valor: '28' },
        { nombre: 'VISUALIZACION_BASICA', valor: '29' },
        { nombre: 'VISUALIZACION_IMAGEN_TIPO_0', valor: '30' },
        { nombre: 'VISUALIZACION_IMAGEN_TIPO_1', valor: '31' },
        { nombre: 'VISUALIZACION_IMAGEN_TIPO_2', valor: '32' },
        { nombre: 'VISUALIZACION_IMAGEN_TIPO_3', valor: '33' },
        { nombre: 'VISUALIZACION_IMAGEN_TIPO_4', valor: '34' },
        { nombre: 'VISUALIZACION_IMAGEN_TIPO_5', valor: '35' },
        { nombre: 'VISUALIZACION_IMAGEN_TIPO_6', valor: '36' },
        { nombre: 'VISUALIZACION_IMAGEN_TIPO_7', valor: '37' },
        { nombre: 'RECEPCION_DTE_SIN_VERIFICAR_EN_SII', valor: '38' },
        { nombre: 'DTE_RECIBIDO_ACEPTADO_POR_EL_SII', valor: '39' },
        { nombre: 'DTE_RECIBIDO_RECHAZADO_POR_EL_SII', valor: '40' },
        { nombre: 'ALMACENAMIENTO_ADJUNTO_DOCUMENTO', valor: '41' },
        { nombre: 'DOCUMENTO_CONSULTADO_EAI', valor: '42' },
        { nombre: 'SOLICITUD_DTE_NO_RECIBIDO', valor: '43' },
        { nombre: 'DOCUMENTO_CEDIDO', valor: '44' },
        { nombre: 'CESION_RECHAZADA', valor: '45' },
        { nombre: 'INFORMACION_DE_PAGO', valor: '46' },
        { nombre: 'DTE_RECIBIDO_EN_VALIDACION', valor: '47' }
      ],
      selectize: {
        plugins: {
          remove_button: {
            label: ''
          }
        },
        maxItems: 1,
        valueField: 'valor',
        labelField: 'nombre',
        searchField: ['valor', 'nombre'],
        create: false,
        render: {
          option: function(data, escape) {
            return (
              '<div class="option">' +
              '<span class="title">' +
              escape(data.nombre) +
              '</span>' +
              '</div>'
            );
          },
          item: function(data, escape) {
            return '<div class="item">' + escape(data.nombre) + '</a></div>';
          }
        }
      },
      flujos: [],
      selectizeFlujo: {
        plugins: {
          remove_button: {
            label: ''
          }
        },
        maxItems: 1,
        valueField: 'id',
        labelField: 'nombre',
        searchField: ['id', 'nombre'],
        create: false,
        render: {
          option: function(data, escape) {
            return (
              '<div class="option">' +
              '<span class="title">' +
              escape(data.nombre) +
              '</span>' +
              '</div>'
            );
          },
          item: function(data, escape) {
            return '<div class="item">' + escape(data.nombre) + '</a></div>';
          }
        }
      }
    };

    //$scope.content = "";
    //$scope.hash = "";
    $scope.editorOptions = {
      lineNumbers: true,
      theme: 'default',
      lineWrapping: true,
      mode: 'javascript',
      styleActiveLine: true,
      matchBrackets: true,
      indentUnit: 2,
      tabSize: 2
    };

    //setear la altura de la ventana
    $scope.setHeight = function() {
      var windowHeight = $(window).innerHeight();
      windowHeight = windowHeight - 150;
      console.log(windowHeight);
      $rootScope.windowHeight = windowHeight;
    };

    angular.element(document).ready(function() {
      // al cargar la vista
      $scope.setHeight();
    });

    $scope.crear = function() {
      console.log('CREANDO...');
      if ($scope.datos.adjuntar == true) {
        var file = document.getElementById('file').files;
        console.log('ARCHIVO CARGADO');
        console.log(file);
      }

      var contenido = FebosUtil.encode($scope.datos.reglas);
      $scope.config.verboCrear = 'Creando';
      $scope.config.cargandoCrear = true;
      var webhookId = '';
      FebosAPI.cl_crear_webhook(
        {
          webhookId: $scope.datos.modificarRegla.webhookId
        },
        {
          nombre: $scope.datos.nombreWH,
          tipoEvento: $scope.datos.tipo,
          url: $scope.datos.url,
          tokenWebhook: $scope.datos.token,
          usuario: $scope.datos.usuario,
          clave: $scope.datos.password,
          flujo: $scope.datos.flujo,
          activo: $scope.datos.notificaciones ? 'si' : 'no',
          reintentos: $scope.datos.reintentos,
          formato: $scope.datos.formato,
          dte: $scope.datos.adjuntarDte ? 'si' : 'no',
          dnt: $scope.datos.adjuntarDnt ? 'si' : 'no',
          pdf: $scope.datos.adjuntarPdf ? 'si' : 'no',
          correos: $scope.datos.correos.map((e = e.text)).join(','),
          notificarPorCorreo: $scope.datos.ncorreo ? 'si' : 'no',
          notificarPorInterno: $scope.datos.ninterno ? 'si' : 'no',
          notificarPorTelegram: $scope.datos.ntelegram ? 'si' : 'no'
        },
        true,
        false
      ).then(function(response) {
        console.log('response:', response);
        $scope.limpiarDatosModificarWebhook();
        webhookId = response.data.webhookId;
        console.log('webhookid: ' + webhookId);

        FebosAPI.cl_crear_regla_webhook(
          {
            webhookId: webhookId
          },
          {
            nombre: $scope.datos.nombreRegla,
            condicion: contenido,
            continuar: $scope.datos.continuar ? 'si' : 'no',
            activa: $scope.datos.activa ? 'si' : 'no',
            prioridad: $scope.datos.prioridad,
            descripcion: $scope.datos.descripcion
          },
          true,
          false
        ).then(function(response2) {
          console.log('response2:', response2);
          $scope.config.verboCrear = 'Crear';
          $scope.config.cargandoCrear = false;
          UIkit.notify('<i class=‘uk-icon-check’></i> Webhook creado satisfactoriamente!', {
            status: 'success',
            timeout: 5000,
            pos: 'top-center'
          });
          $scope.listarWebhooks();
          $scope.limpiarDatosCrearWebhook();
        });
      });
    };

    $scope.enviarCambiosWebhook = function() {
      console.log('Modificando...');
      $scope.config.verboModificar = 'Modificando';
      $scope.config.cargandoModificarWH = true;
      FebosAPI.cl_modificar_webhook(
        {
          webhookId: $scope.datos.modificarWebhook.id
        },
        {
          tipoEvento: $scope.datos.modificarWebhook.tipo,
          url: $scope.datos.modificarWebhook.url,
          tokenWebhook: $scope.datos.modificarWebhook.token,
          usuario: $scope.datos.modificarWebhook.usuario,
          clave: $scope.datos.modificarWebhook.password,
          flujo: $scope.datos.modificarWebhook.flujo,
          activo: $scope.datos.modificarWebhook.activo ? 'si' : 'no',
          reintentos: $scope.datos.modificarWebhook.reintentos,
          dte: $scope.datos.modificarWebhook.adjuntarDte ? 'si' : 'no',
          dnt: $scope.datos.modificarWebhook.adjuntarDnt ? 'si' : 'no',
          pdf: $scope.datos.modificarWebhook.adjuntarPdf ? 'si' : 'no',
          correos: $scope.datos.modificarWebhook.correosNotificacion
        },
        true,
        false
      ).then(function(response) {
        console.log('response:', response);
        $scope.config.verboModificar = 'Modificar';
        $scope.config.cargandoModificarWH = false;
        $scope.limpiarDatosModificarWebhook();

        UIkit.modal('#modalModificarWebhook').hide();
        UIkit.notify('<i class=‘uk-icon-check’></i> Webhook actualizado!', {
          status: 'success',
          timeout: 5000,
          pos: 'top-center'
        });
        $scope.listarWebhooks();
      });
      //limpiar datos despues de modificar
      //ocultar modal modificar
    };

    $scope.enviarCambiosRegla = function() {
      $scope.config.verboModificar = 'Modificando';
      $scope.config.cargandoModificar = true;

      FebosAPI.cl_modificar_regla_webhook(
        {
          webhookId: $scope.datos.modificarRegla.webhookId
        },
        {
          nombre: $scope.datos.modificarRegla.nombreRegla,
          condicion: FebosUtil.encode($scope.datos.modificarRegla.condicion),
          continuar: $scope.datos.modificarRegla.continuar ? 'si' : 'no',
          activa: $scope.datos.modificarRegla.activa ? 'si' : 'no',
          prioridad: $scope.datos.modificarRegla.prioridad,
          descripcion: $scope.datos.modificarRegla.descripcion,
          reglaId: $scope.datos.modificarRegla.reglaId
        },
        true,
        false
      ).then(function(response) {
        console.log('response:', response);
        $scope.config.verboModificar = 'Modificar';
        $scope.config.cargandoModificar = false;
        $scope.limpiarDatosCrearWebhook();

        UIkit.modal('#modalModificarReglaWebhook').hide();
        UIkit.notify('<i class=‘uk-icon-check’></i> Regla actualizada!', {
          status: 'success',
          timeout: 5000,
          pos: 'top-center'
        });
      });
      // console.log($scope.datos.modificarRegla.id);
    };

    $scope.cancelarCambiosWebhook = function() {
      //limpiar datos despues de modificar
      $scope.limpiarDatosModificarWebhook();
      //ocultar modal modificar
      UIkit.modal('#modalModificarWebhook').hide();
    };

    $scope.cancelarCambiosRegla = function() {
      //limpiar datos despues de modificar regla
      $scope.limpiarDatosModificarRegla();
      //ocultar modal modificar regla
      UIkit.modal('#modalModificarReglaWebhook').hide();
    };

    console.log('ENTRANDO A WEBOOK');

    $scope.listarWebhooks = function() {
      $scope.config.cargando = true;
      $scope.webhooks = [];

      FebosAPI.cl_listar_webhooks({}, {}, true, false).then(function(response) {
        console.log('response:', response);
        $scope.config.cargando = false;
        $scope.webhooks = response.data.webhooks;
      });

      //listar flujos
      FebosAPI.cl_listar_flujos(
        {
          filas: '10',
          pagina: '1',
          empresaId: SesionFebos().empresa.id
        },
        {},
        true,
        false
      ).then(function(response2) {
        //$scope.config.cargando = false;
        $scope.flujos = response2.data.flujos;
        console.log('len:' + $scope.flujos.length);
        for (var i = 0; i < $scope.flujos.length; i++) {
          $scope.config.flujos.push($scope.flujos[i]);
        }
        console.log($('#flujos').val());
        console.log($scope.config.tiposDeEvento);
      });
    };

    $scope.listarWebhooksReglas = function(idWebhook) {
      UIkit.modal('#listarWebhookReglas').show();

      $scope.webhookReglas = [];
      $scope.config.cargandoReglas = true;

      FebosAPI.cl_listar_webhooks_reglas(
        {
          webhookId: idWebhook
        },
        {},
        true,
        false
      ).then(function(response) {
        console.log('response:', response);
        $scope.config.cargandoReglas = false;
        $scope.webhookReglas = response.data.webhookReglas;
      });
    };
    $scope.eliminarWebhooksRegla = function(webhookRegla, index) {
      console.log(webhookRegla);
      UIkit.modal.confirm(
        'Esta seguro que desea eliminar la regla id <b>' + webhookRegla.reglaId + '</b>?',
        function() {
          modal = UIkit.modal.blockUI(
            "<div class='uk-text-center'>Eliminando regla...<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
          );

          FebosAPI.cl_eliminar_webhooks_regla(
            {
              webhookId: webhookRegla.webhookId,
              reglaId: webhookRegla.reglaId
            },
            {},
            true,
            false
          ).then(function success(response) {
            modal.hide();
            try {
              $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
            } catch (e) {}
            //$rootScope.content_preloader_hide();
            if (response.data.codigo == 10) {
              $scope.webhookReglas.splice(index, 1);

              $('#webhookRegla_' + webhookRegla.reglaId).remove();
              UIkit.modal.alert('Regla eliminada', { labels: { Ok: 'Listo!' } });
            } else {
              console.log(response);
              SesionFebos().error(
                response.data.codigo,
                response.data.mensaje,
                response.data.seguimientoId,
                response.data.errores
              );
            }
          });
        },
        { labels: { Ok: 'Si, eliminar', Cancel: 'No, me acabo de arrepentir' } }
      );
    };

    $scope.eliminarWebhook = function(webhook, index) {
      UIkit.modal.confirm(
        'Esta seguro que desea eliminar el webook <b>' + webhook.webhookId + '</b> y sus reglas?',
        function() {
          modal = UIkit.modal.blockUI(
            "<div class='uk-text-center'>Eliminando webhook...<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
          );

          cl_eliminar_webhook; //webhookId

          FebosAPI.cl_eliminar_webhook(
            {
              webhookId: webhook.webhookId
            },
            {},
            true,
            false
          ).then(function success(response) {
            modal.hide();
            try {
              $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
            } catch (e) {}
            //$rootScope.content_preloader_hide();
            if (response.data.codigo == 10) {
              $scope.webhooks.splice(index, 1);

              $('#webhook_' + webhook.webhookId).remove();
              UIkit.modal.alert('Webhook eliminado', { labels: { Ok: 'Listo!' } });
            } else {
              console.log(response);
              SesionFebos().error(
                response.data.codigo,
                response.data.mensaje,
                response.data.seguimientoId,
                response.data.errores
              );
            }
          });
        },
        { labels: { Ok: 'Si, eliminar', Cancel: 'No, me acabo de arrepentir' } }
      );
    };

    $scope.modificarWebhook = function(webhook) {
      console.log('ID WEBHOOK: ', webhook.webhookId);
      //seteo de la id de webhook a modificar
      $scope.datos.modificarWebhook.id = webhook.webhookId;
      $scope.datos.modificarWebhook.tipo = webhook.tipoEvento;
      $scope.datos.modificarWebhook.url = webhook.url;
      $scope.datos.modificarWebhook.token = webhook.token;
      $scope.datos.modificarWebhook.usuario = webhook.httpBasicUser;
      $scope.datos.modificarWebhook.password = webhook.httpBasicPassword;
      $scope.datos.modificarWebhook.flujo = webhook.flujoId;
      $scope.datos.modificarWebhook.reintentos = parseInt(webhook.politicaReintentos);
      $scope.datos.modificarWebhook.activo = webhook.activo;
      $scope.datos.modificarWebhook.adjuntar = webhook.adjuntarDte;
      var correos = webhook.correosNotificacion.split(',');
      console.log('correos', correos);
      $scope.datos.modificarWebhook.correos = correos;
      $scope.datos.modificarWebhook.nombre = webhook.nombre;
      $scope.datos.modificarWebhook.formato = webhook.formato;
      $scope.datos.modificarWebhook.adjuntarDte = webhook.adjuntarDte;
      $scope.datos.modificarWebhook.adjuntarDnt = webhook.adjuntarDnt;
      $scope.datos.modificarWebhook.adjuntarPdf = webhook.adjuntarPdf;

      //setear campos del modal modificar antes de mostrar
      UIkit.modal('#modalModificarWebhook').show();
    };

    $scope.limpiarDatosModificarWebhook = function() {
      $scope.datos.modificarWebhook = {
        id: '',
        url: '',
        token: '',
        usuario: '',
        password: '',
        tipo: '',
        flujo: '',
        reintentos: '',
        notificaciones: false,
        adjuntar: false,
        correos: [],
        textoModificar: 'Modificar'
      };
    };

    $scope.limpiarDatosModificarRegla = function() {
      $scope.datos.modificarRegla = {
        reglaId: '',
        webhookId: '',
        nombreRegla: '',
        prioridad: 0,
        descripcion: '',
        continuar: false,
        activa: false,
        condicion: ''
      };
    };

    $scope.limpiarDatosCrearWebhook = function() {
      $scope.datos.nombreWH = '';
      $scope.datos.url = '';
      $scope.datos.token = '';
      $scope.datos.usuario = '';
      $scope.datos.password = '';
      $scope.datos.tipo = '';
      $scope.datos.formato = 'json';
      $scope.datos.nombreRegla = '';
      $scope.datos.reglas = '';
      $scope.datos.reintentos = '';
      $scope.datos.prioridad = 0;
      $scope.datos.notificaciones = false;
      $scope.datos.adjuntar = false;
      $scope.datos.correos = [];
      $scope.datos.textoCrear = 'CREAR';
      $scope.datos.condicion = '';
      $scope.datos.continuar = false;
      $scope.datos.activa = false;
    };

    $scope.modalModificarReglaWebhook = function(webhookRegla) {
      $scope.datos.modificarRegla.nombreRegla = webhookRegla.nombre;
      $scope.datos.modificarRegla.reglaId = webhookRegla.reglaId;
      $scope.datos.modificarRegla.webhookId = webhookRegla.webhookId;
      $scope.datos.modificarRegla.condicion = FebosUtil.decode(webhookRegla.condicion);
      $scope.datos.modificarRegla.continuar = webhookRegla.continuar;
      $scope.datos.modificarRegla.activa = webhookRegla.activa;
      $scope.datos.modificarRegla.prioridad = webhookRegla.prioridad;
      $scope.datos.modificarRegla.descripcion = webhookRegla.descripcion;
      UIkit.modal('#modalModificarReglaWebhook').show();
    };
    $scope.agregarWebhooksRegla = function() {
      //crear regla
      $scope.config.verboCrear = 'Creando';
      $scope.config.cargandoCrear = true;

      //cl_crear_regla_webhook
      FebosAPI.cl_crear_regla_webhook(
        {
          webhookId: $scope.datos.crearRegla.webhookId
        },
        {
          nombre: $scope.datos.crearRegla.nombreRegla,
          condicion: FebosUtil.encode($scope.datos.crearRegla.condicion),
          continuar: $scope.datos.crearRegla.continuar ? 'si' : 'no',
          activa: $scope.datos.crearRegla.activa ? 'si' : 'no',
          prioridad: $scope.datos.crearRegla.prioridad,
          descripcion: $scope.datos.crearRegla.descripcion
        },
        true,
        false
      ).then(function(response2) {
        console.log('response2:', response2);
        UIkit.modal('#agregarWebhookReglas').hide();
        UIkit.notify('<i class=‘uk-icon-check’></i> Regla agregada!', {
          status: 'success',
          timeout: 5000,
          pos: 'top-center'
        });
        $scope.config.verboCrear = 'Crear';
        $scope.config.cargandoCrear = false;
      });
    };

    $scope.modalAgregarWebhooksRegla = function(webhook, index) {
      UIkit.modal('#agregarWebhookReglas').show();
      $scope.datos.crearRegla.webhookId = webhook.webhookId;
    };

    $scope.modalSuscripciones = function(webhook, index) {
      $scope.config.cargandoSuscripciones = true;

      FebosAPI.cl_listar_suscripciones_webhook(
        {
          webhookId: webhook.webhookId
        },
        {},
        true,
        false
      ).then(function(response) {
        console.log('response webhookSuscripciones:', response);
        $scope.config.cargandoSuscripciones = false;
        //$scope.webhookReglas = response.data.webhookReglas;
        if (response.data.suscripciones != null && response.data.suscripciones.length > 0) {
          $scope.datos.modificarSuscripcion.ninterno =
            response.data.suscripciones[0].notificarPorInterno === 'Y' ? true : false;
          $scope.datos.modificarSuscripcion.ncorreo =
            response.data.suscripciones[0].notificarPorCorreo === 'Y' ? true : false;
          $scope.datos.modificarSuscripcion.ntelegram =
            response.data.suscripciones[0].notificarPorTelegram === 'Y' ? true : false;
          $scope.datos.modificarSuscripcion.webhookId = webhook.webhookId;
        }
      });
      UIkit.modal('#modalSuscripciones').show();
    };
    $scope.enviarSuscripciones = function() {
      $scope.config.cargandoModificarSuscripciones = true;
      $scope.config.verboModificar = 'Modificando';

      FebosAPI.cl_modificar_suscripcion_webhook(
        {
          webhookId: $scope.datos.modificarSuscripcion.webhookId
        },
        {
          notificarPorInterno: $scope.datos.modificarSuscripcion.ninterno ? 'si' : 'no',
          notificarPorCorreo: $scope.datos.modificarSuscripcion.ncorreo ? 'si' : 'no',
          notificarPorTelegram: $scope.datos.modificarSuscripcion.ntelegram ? 'si' : 'no'
        },
        true,
        false
      ).then(function(response) {
        console.log('response webhookSuscripciones:', response);
        $scope.config.cargandoModificarSuscripciones = false;
        $scope.config.verboModificar = 'Modificar';

        UIkit.modal('#modalSuscripciones').hide();
        UIkit.notify('<i class=‘uk-icon-check’></i> Suscripciones del usuario modificadas!', {
          status: 'success',
          timeout: 5000,
          pos: 'top-center'
        });
      });
      UIkit.modal('#modalSuscripciones').show();
    };

    $scope.cancelarSuscripciones = function() {
      //ocultar modal modificar
      UIkit.modal('#modalSuscripciones').hide();
    };
    $scope.listarWebhooks();
  }
]);
