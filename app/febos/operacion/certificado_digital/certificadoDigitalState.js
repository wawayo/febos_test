febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.certificado", {
                    url: "/:app/operacion/certificado",
                    templateUrl: 'app/febos/operacion/certificado_digital/certificadoDigitalView.html',
                    controller: 'certificadoDigitalCtrl',
                    resolve: {
                        EstadoAnterior: [
                            "$state",
                            function ($state) {
                                var currentStateData = {
                                    estado: $state.current.name,
                                    parametros: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }
                        ],
                        Autorizacion: ['Autorizacion', function (Autorizacion) {
                                try {
                                    Autorizacion.verificar();
                                } catch (e) {
                                    console.log("Error al autenticar", e);
                                }
                                return;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_uikit',
                                'lazy_iCheck',
                                'app/febos/operacion/certificado_digital/certificadoDigitalController.js'
                            ]);
                        }]
                    }
                })

            }]);