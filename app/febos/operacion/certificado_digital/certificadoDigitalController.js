angular.module('febosApp').controller('certificadoDigitalCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$location',
  '$http',
  '$state',
  'SesionFebos',
  'FebosAPI',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    $http,
    $state,
    SesionFebos,
    FebosAPI
  ) {
    $scope.cargando = false;
    $scope.base64 = '';
    $scope.clave = { dato: '' };
    $scope.nombreArchivo = '';
    $scope.subiendo = false;
    $scope.certificado = {
      /*"nombre": "Michel Daniel Muñoz Fernandez",
             "rut": "16639957-2",
             "cargado": "2017-09-12",
             "vencimiento": "2017-09-1",
             "hash": "534e08c7d6d55c1593f9c5510967351e"*/
    };
    $scope.reset = function() {
      $scope.cargando = false;
      $scope.base64 = '';
      $scope.clave.dato = '';
      $scope.nombreArchivo = '';
      $scope.subiendo = false;
      $scope.certificado = {};
    };
    $scope.verificar = function() {
      $scope.cargando = true;
      //cl_obtener_certificado
      FebosAPI.cl_obtener_certificado(
        {
          empresaId: SesionFebos().empresa.id
        },
        {},
        true,
        false
      ).then(function(response) {
        $scope.cargando = false;
        $scope.certificado = response.data.certificado;
        $scope.certificado.vencimiento = $scope.certificado.vencimiento.split('.')[0];
        $scope.certificado.cargado = $scope.certificado.cargado.split('.')[0];
        $scope.quedan = calcularDiasDeVencimiento($scope.certificado.vencimiento);
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
      });
    };

    $scope.verificar();
    var calcularDiasDeVencimiento = function(fechaVencimiento) {
      var dia = parseInt(fechaVencimiento.split('-')[2]);
      var mes = parseInt(fechaVencimiento.split('-')[1]);
      var ano = parseInt(fechaVencimiento.split('-')[0]);
      var vence = new Date();
      vence.setMonth(mes - 1);
      vence.setYear(ano);
      vence.setDate(dia);
      var hoy = new Date();
      var timeDiff = vence.getTime() - hoy.getTime();
      return Math.ceil(timeDiff / (1000 * 3600 * 24));
    };
    //$scope.quedan = calcularDiasDeVencimiento($scope.certificado.vencimiento);

    $scope.cargarCertificado = function() {
      var files = document.getElementById('file').files;
      if (files.length > 0) {
        if (files[0].type === 'application/x-pkcs12') {
          var reader = new FileReader();
          reader.readAsDataURL(files[0]);
          console.log(reader);
          reader.onload = function() {
            var base64 = reader.result;
            var documento = base64.split(',');
            var payload = documento[1];
            //console.log(payload);
            $scope.base64 = payload;
            setTimeout(function() {
              $scope.$apply();
            }, 100);
          };
          reader.onerror = function(error) {
            console.log('Error: ', error);
            SesionFebos().error('1', 'Error, Falló el envío de la solicitud', '');
          };
        } else {
          console.log('No soporta el tipo de archivo ' + files[0].type);
          SesionFebos().error('1', 'No soporta el tipo de archivo' + files[0].type, '');
        }
      } else {
        SesionFebos().error('1', 'El documento es obligatorio y en formato XML', '');
      }
    };

    $scope.subir = function() {
      if ($scope.clave.dato.length == 0) {
        SesionFebos().error('1', 'Debe ingresar una contraseña para el certificado', '');
        return;
      }
      $scope.subiendo = true;

      //cl_subir_certificado
      FebosAPI.cl_subir_certificado(
        {
          empresaId: SesionFebos().empresa.id
        },
        {
          clave: $scope.clave.dato,
          payload: $scope.base64
        },
        false,
        true
      ).then(function(response) {
        $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
        if (response.data.codigo == 10) {
          $scope.subiendo = false;
          $scope.certificado = response.data.certificado;
          $scope.certificado.vencimiento = $scope.certificado.vencimiento.split('.')[0];
          $scope.certificado.cargado = $scope.certificado.cargado.split('.')[0];
          $scope.quedan = calcularDiasDeVencimiento($scope.certificado.vencimiento);
        } else {
          $scope.subiendo = false;
          var errores = '';
          try {
            if (response.data.errores.length > 0) {
              errores += ' (' + response.data.errores[0] + ')';
            }
          } catch (e) {}
          UIkit.modal.alert(response.data.mensaje);
          // SesionFebos().error(response.data.codigo, response.data.mensaje + errores, response.data.seguimientoId);
        }
      });
    };
  }
]);
