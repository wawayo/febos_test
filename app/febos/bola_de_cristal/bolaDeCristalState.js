febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.bolaDeCristal", {
                            url: "/:app/soporte/bolaDeCristal",
                            templateUrl: 'app/febos/bola_de_cristal/bolaDeCristalView.html',
                            controller: 'bolaDeCristalCtrl',
                            resolve: {
                                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                        return $ocLazyLoad.load([
                                            // ocLazyLoad config (app/app.js)
                                            'app/febos/bola_de_cristal/bolaDeCristalController.js'
                                        ], {serie: true});
                                    }]
                            },
                            data: {
                                pageTitle: 'Bola de cristal'
                            },
                            ncyBreadcrumb: {
                                label: 'Home'
                            }
                        })

            }]);
