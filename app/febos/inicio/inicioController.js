var JSONE = {};

angular.module('febosApp').controller('inicioCtrl', [
  '$rootScope',
  '$scope',
  '$interval',
  '$timeout',
  '$location',
  'variables',
  '$http',
  'SesionFebos',
  'FebosAPI',
  '$stateParams',
  '$sce',
  'FebosUtil',
  function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    variables,
    $http,
    SesionFebos,
    FebosAPI,
    $stateParams,
    $sce,
    FebosUtil
  ) {
    SesionFebos().contador = 0;
    var today = new Date();
    var curHr = today.getHours();
    $scope.app = $stateParams.app;
    $scope.noticia = {
      listadoPara: [
        { id: 1, nombre: 'Clientes' },
        { id: 2, nombre: 'Proveedores' },
        { id: 3, nombre: 'Interno' }
      ]
    };
    $scope.noticias = [];
    $scope.cargadores = {
      noticias: true,
      actividad: true
    };
    $scope.indicadoresResumen = {
      recibidos: {
        recibidos: {
          total: 0,
          porCiento: 0
        },
        sin_accion: {
          total: 0,
          porCiento: 0
        },
        aceptados: {
          total: 0,
          porCiento: 0
        },
        pre_rechazados: {
          total: 0,
          porCiento: 0
        },
        rechazados: {
          total: 0,
          porCiento: 0
        },
        no_recibidos: {
          total: 0,
          porCiento: 0
        }
      },
      emitidos: {
        emitidos: {
          total: 0,
          porCiento: 0
        },
        aceptados_cliente: {
          total: 0,
          porCiento: 0
        },
        aceptados_sii: {
          total: 0,
          porCiento: 0
        },
        rechazados: {
          total: 0,
          porCiento: 0
        },
        rechazados_sii: {
          total: 0,
          porCiento: 0
        },
        sin_respuesta_cliente: {
          total: 0,
          porCiento: 0
        }
      }
    };
    $scope.indicadoresResumenP = {
      oc_recibidos: {
        total: 0,
        porCiento: 0
      },
      hes_recibidos: {
        total: 0,
        porCiento: 0
      },
      hem_recibidos: {
        total: 0,
        porCiento: 0
      },
      dte_emitidos: {
        total: 0,
        porCiento: 0
      },
      dte_aceptados: {
        total: 0,
        porCiento: 0
      },
      dte_rechazados: {
        total: 0,
        porCiento: 0
      },
      dte_pagados: {
        total: 0,
        porCiento: 0
      },
      dte_incompletos: {
        total: 0,
        porCiento: 0
      }
    };
    /* $scope.indicadoresResumen.recibidos.total = 0;
             $scope.indicadores.recibidos.porCiento = 0;
             $scope.indicadores.sin_accion.total = 0;
             $scope.indicadores.sin_accion.porCiento = 0;
             $scope.indicadores.aceptados.total = 0;
             $scope.indicadores.aceptados.porCiento = 0;
             $scope.indicadores.pre_rechazados.total = 0;
             $scope.indicadores.pre_rechazados.porCiento = 0;
             $scope.indicadores.rechazados.total = 0;
             $scope.indicadores.rechazados.porCiento = 0;
             $scope.indicadores.no_recibidos.total = 0;
             $scope.indicadores.no_recibidos.porCiento = 0;*/
    $scope.empresa = SesionFebos().empresa.fantasia;
    if (curHr < 12) {
      $scope.tipoSaludo = 'Buenos días';
    } else if (curHr < 21) {
      $scope.tipoSaludo = 'Buenas tardes';
    } else {
      $scope.tipoSaludo = 'Buenas noches';
    }
    $scope.eventos = [];
    $scope.dialAzul = {
      barColor: '#03a9f4',
      scaleColor: false,
      trackColor: '#f5f5f5',
      lineWidth: 5,
      size: 150
    };
    $scope.dialAmarillo = {
      barColor: '#ffff66',
      scaleColor: false,
      trackColor: '#f5f5f5',
      lineWidth: 5,
      size: 150
    };
    $scope.dialVerde = {
      barColor: '#00cc66',
      scaleColor: false,
      trackColor: '#f5f5f5',
      lineWidth: 5,
      size: 150
    };
    $scope.dialNaranjo = {
      barColor: '#ff8000',
      scaleColor: false,
      trackColor: '#f5f5f5',
      lineWidth: 5,
      size: 150
    };
    $scope.dialRojo = {
      barColor: '#cc0000',
      scaleColor: false,
      trackColor: '#f5f5f5',
      lineWidth: 5,
      size: 150
    };
    $scope.dialGris = {
      barColor: '#606060',
      scaleColor: false,
      trackColor: '#f5f5f5',
      lineWidth: 5,
      size: 150
    };
    $scope.contadores = {
      recibidos: 0,
      sin_accion: 0,
      aceptados: 0,
      pre_Rechazados: 0,
      rechazados: 0,
      no_recibidos: 0
    };
    $scope.indicadoresEconomicos = function() {
      //https://mindicador.cl/api
      $http({ method: 'GET', url: 'https://mindicador.cl/api' }).then(
        function(data) {
          console.log(data);
          $scope.indicadores = data.data;
        },
        function(data) {
          console.log(data);
        }
      );
    };
    $scope.abrir = function(url) {
      window.open(url, '_blank');
    };
    $scope.indicadoresEconomicos();
    /*
            var reqNovedades = {
                method: 'GET',
                url: febosSingleton.api + "/novedades",
                params: {
                    'limite': '2',
                    'motrarTodas': "no"
                }
            };



            $http(reqNovedades).then(function (response) {
                $scope.cargadores.noticias=false;
                console.log(response);
                $scope.novedades = response.data.novedades;
                $scope.cargando = false;
                setTimeout(function(){
                    var altura=$("#novedades_content").height();
                    //console.log("seteando altura: "+altura);
                    $("#eventos").css({"height":altura+"px"});
                    //console.log("nueva altura: "+$("#eventos").css("height"));

                },150);
            }, function (response) {
                console.log(response);
            });*/

    /*

            var reqActividad = {
                method: 'GET',
                url: febosSingleton.api + "/empresas/" + febosSingleton.empresa.iut + "/actividad"
            };
            $http(reqActividad).then(function (response) {
                $scope.cargadores.actividad=false;
                //console.log("actividad", response);
                var tmp = response.data.eventos;
                for (var i = 0; i < tmp.length; i++) {
                    tmp[i].detalle = $scope.inicioAleatorio() + " " + $scope.tipoDoc(tmp[i].tipoDocumento) + " emitido por el rut " + tmp[i].rutEmisor + " con folio " + tmp[i].folio;
                }
                $scope.eventos = tmp;
            }, function (response) {
                console.log(response);
            });
            */

    FebosAPI.cl_listar_actividad_empresa(
      {
        empresaId: SesionFebos().empresa.iut
      },
      true,
      false
    ).then(function(response) {
      $scope.cargadores.actividad = false;
      console.log('actividad', response);
      var tmp = response.data.eventos;
      for (var i = 0; i < tmp.length; i++) {
        tmp[i].detalle =
          $scope.inicioAleatorio() +
          ' ' +
          $scope.tipoDoc(tmp[i].tipoDocumento) +
          ' emitido por el rut ' +
          tmp[i].rutEmisor +
          ' con folio ' +
          tmp[i].folio;
      }
      $scope.eventos = tmp;
    });

    $scope.configSelect = {
      plugins: {
        remove_button: {
          label: ''
        }
      },
      maxItems: 1,
      valueField: 'id',
      labelField: 'nombre',
      searchField: ['nombre'],
      create: false,
      render: {
        option: function(dato, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(dato.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(dato, escape) {
          return '<div class="item">' + escape(dato.nombre) + '</div>';
        }
      }
    };

    $scope.listadoNoticias = function() {
      FebosAPI.cl_noticias_listado(
        {
          ambito: FebosUtil.obtenerAmbito()
        },
        true,
        false
      ).then(function(response) {
        var noticias = [];
        for (var i = 0; i < response.data.noticias.length; i++) {
          response.data.noticias[i].fechaFormato = new Date(
            response.data.noticias[i].fecha
          ).toLocaleString();
          noticias.push(response.data.noticias[i]);
        }
        $scope.noticias = noticias;
      });
    };
    $scope.listadoNoticias();

    $scope.modalAgregarNoticia = function() {
      $scope.noticia = {};
      UIkit.modal('#mdlAgregarNoticia').show();
    };

    $scope.modalCerrarAgregarNoticia = function() {
      UIkit.modal('#mdlAgregarNoticia').hide();
    };

    $scope.eliminarNoticia = function(noticia) {
      FebosAPI.cl_noticias_eliminar(
        {
          noticiaId: noticia.noticiaId
        },
        true,
        false
      ).then(function(response) {
        UIkit.modal.alert('Noticias eliminada satisfactoriamente');
        UIkit.modal('#mdlVerNoticia').hide();
        $scope.listadoNoticias();
      });
    };

    $scope.modalVerNoticia = function(noticia) {
      $scope.vernoticia = {};
      $scope.vernoticia.para = noticia.tipo;
      $scope.vernoticia.noticiaId = noticia.noticiaId;
      $scope.vernoticia.puedeEliminar = noticia.usuario === SesionFebos().usuario.id ? true : false;
      if (noticia.texto) {
        $scope.vernoticia.texto = $sce.trustAsHtml(atob(noticia.texto));
      }
      $scope.vernoticia.titulo = noticia.titulo;
      $scope.vernoticia.fecha = new Date(noticia.fecha).toLocaleString();

      UIkit.modal('#mdlVerNoticia').show();
    };

    $scope.modalCerrarVerNoticia = function() {
      UIkit.modal('#mdlVerNoticia').hide();
    };

    $scope.modalCrearNoticia = function() {
      $scope.creandoNoticia = true;
      var noticiaBody = {
        tipo: $scope.noticia.para,
        texto: $scope.noticia.texto,
        titulo: $scope.noticia.titulo
      };
      FebosAPI.cl_noticias_crear({}, noticiaBody, true, false).then(function(response) {
        if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
          UIkit.modal.alert('Noticias creada satisfactoriamente');
          UIkit.modal('#mdlAgregarNoticia').hide();
          $scope.creandoNoticia = false;
          $scope.listadoNoticias();
        }
      });
    };

    $scope.ckeditor_options = {
      customConfig: '../../assets/js/custom/ckeditor_config.js'
    };

    $scope.inicioAleatorio = function() {
      var min = 1;
      var max = 6;
      var num = Math.floor(Math.random() * max) + min;
      switch (num) {
        case 1:
          return 'Sobre el documento de tipo ';
        case 2:
          return 'En el documento tipo ';
        case 3:
          return 'Para el tipo de documento ';
        case 4:
          return 'Para el documento tipo ';
        case 5:
          return 'Sobre el tipo de documento ';
        case 6:
          return 'En el tipo de documento ';
      }
    };
    $scope.tipoDoc = function(tipo) {
      switch (parseInt(tipo)) {
        case 33:
          return 'Factura Electrónica';
        case 34:
          return 'Factura Electrónica Exenta';
        case 43:
          return 'Factura de compra';
        case 56:
          return 'Nota de Débito electrónica';
        case 52:
          return 'Guia de despacho electrónica';
        case 61:
          return 'Nota de Crédito electrónica';
      }
    };

    FebosAPI.cl_estadisticas(
      {
        empresaId: SesionFebos().empresa.iut,
        desde: '2030-01-01',
        hasta: '2030-01-01'
      },
      true,
      false
    ).then(function(response) {
      var ind = response.data.indicadoresCloud;
      $scope.indicadoresResumen.recibidos.recibidos.total =
        typeof ind.cRecibidos.totalRecibidos === 'undefined' ? 0 : ind.cRecibidos.totalRecibidos;

      $scope.indicadoresResumen.recibidos.sin_accion.total =
        typeof ind.cRecibidos.sinAccion === 'undefined' ? 0 : ind.cRecibidos.sinAccion;

      $scope.indicadoresResumen.recibidos.aceptados.total =
        typeof ind.cRecibidos.aceptados === 'undefined' ? 0 : ind.cRecibidos.aceptados;

      $scope.indicadoresResumen.recibidos.pre_rechazados.total =
        typeof ind.cRecibidos.preRechazados === 'undefined' ? 0 : ind.cRecibidos.preRechazados;

      $scope.indicadoresResumen.recibidos.rechazados.total =
        typeof ind.cRecibidos.rechazados === 'undefined' ? 0 : ind.cRecibidos.rechazados;

      $scope.indicadoresResumen.recibidos.no_recibidos.total =
        typeof ind.cRecibidos.noRecibidos === 'undefined' ? 0 : ind.cRecibidos.noRecibidos;

      $scope.indicadoresResumen.emitidos.emitidos.total =
        typeof ind.cEmitidos.totalEmitidos === 'undefined' ? 0 : ind.cEmitidos.totalEmitidos;

      $scope.indicadoresResumen.emitidos.aceptados_cliente.total =
        typeof ind.cEmitidos.aceptadosCliente === 'undefined' ? 0 : ind.cEmitidos.aceptadosCliente;

      $scope.indicadoresResumen.emitidos.aceptados_sii.total =
        typeof ind.cEmitidos.aceptadosSII === 'undefined' &&
        typeof ind.cEmitidos.aceptadosConReparoSII === 'undefined'
          ? 0
          : parseInt(ind.cEmitidos.aceptadosSII) + parseInt(ind.cEmitidos.aceptadosConReparoSII);

      $scope.indicadoresResumen.emitidos.rechazados.total =
        typeof ind.cEmitidos.rechazados === 'undefined' ? 0 : ind.cEmitidos.rechazados;

      $scope.indicadoresResumen.emitidos.rechazados_sii.total =
        typeof ind.cEmitidos.rechazadosSII === 'undefined' ? 0 : ind.cEmitidos.rechazadosSII;

      $scope.indicadoresResumen.emitidos.sin_respuesta_cliente.total =
        typeof ind.cEmitidos.sinRespuestaCliente === 'undefined'
          ? 0
          : ind.cEmitidos.sinRespuestaCliente;

      var indP = response.data.indicadoresProveedor;
      $scope.indicadoresResumenP.oc_recibidos.total =
        typeof indP.ocRecibidos === 'undefined' ? 0 : indP.ocRecibidos;

      $scope.indicadoresResumenP.hes_recibidos.total =
        typeof indP.hesRecibidos === 'undefined' ? 0 : indP.hesRecibidos;

      $scope.indicadoresResumenP.hem_recibidos.total =
        typeof indP.hemRecibidos === 'undefined' ? 0 : indP.hemRecibidos;

      $scope.indicadoresResumenP.dte_emitidos.total =
        typeof indP.dteEmitidos === 'undefined' ? 0 : indP.dteEmitidos;

      $scope.indicadoresResumenP.dte_aceptados.total =
        typeof indP.dteAceptados === 'undefined' ? 0 : indP.dteAceptados;

      $scope.indicadoresResumenP.dte_rechazados.total =
        typeof indP.dteRechazados === 'undefined' ? 0 : indP.dteRechazados;

      $scope.indicadoresResumenP.dte_pagados.total =
        typeof indP.dtePagados === 'undefined' ? 0 : indP.dtePagados;

      $scope.indicadoresResumenP.dte_incompletos.total =
        typeof indP.dteIncompletos === 'undefined' ? 0 : indP.dteIncompletos;
    });

    $scope.rss = [];
    FebosAPI.io_rss({ url: 'http://www.febos.cl/feed' }, {}, true, false).then(function(response) {
      $scope.rss = response.data.feed.entries;
      $scope.cargadores.noticias = false;
      //     setTimeout(function () {
      //         var altura = $("#novedades_content").height();
      //         //console.log("seteando altura: " + altura);
      //         $("#eventos").css({"height": altura + "px"});
      //         //console.log("nueva altura: " + $("#eventos").css("height"));
      //
      //       }, 150);
    });

    if (
      typeof SesionFebos().version === 'undefined' ||
      SesionFebos().version != $rootScope.appVer
    ) {
      $scope.changelog = 'Cargando...';
      SesionFebos().version = $rootScope.appVer;
      setTimeout(function() {
        UIkit.modal('#mdlChangelog').show();
      }, 200);
    }
  }
]);
