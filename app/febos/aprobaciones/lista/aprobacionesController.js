angular
  .module('febosApp')
  .controller('aprobacionesCtrl', function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    $http,
    $state,
    utils,
    FebosAPI,
    SesionFebos,
    $stateParams,
    FebosUtil,
    AprobacionService
  ) {
    $scope.app = $stateParams.app;
    $scope.filtros = {};
    $scope.filtros.nombre_aprobacion = '';
    $scope.aprobaciones = [];
    $scope.cargando = true;

    $scope.limpiarFiltros = function() {
      $scope.filtros.nombre_aprobacion = '';
    };

    $scope.crearEjecucion = function(aprobacion) {
      console.log('moving', aprobacion);
      $state.go('restringido.aprobacionejecucioncrear', {
        app: $stateParams.app,
        aprobacion: aprobacion.id
      });
    };

    $scope.verAprobacion = function(id) {
      $state.go('restringido.aprobacionver', {
        app: $stateParams.app,
        aprobacion: id
      });
    };

    $scope.modificarAprobacion = function(id) {
      $state.go('restringido.aprobacioncrear', {
        app: $stateParams.app,
        aprobacion: id
      });
    };

    $scope.buscar = function() {
      FebosAPI.cl_aprobaciones_listar(
        { estado: true, filtros: $scope.filtros.nombre_aprobacion },
        {},
        true,
        false
      ).then(function(response) {
        $scope.cargando = false;
        $scope.aprobaciones = response.data.aprobaciones;
      });
    };
    $scope.buscar();

    $scope.eliminar = function(webhook, index) {
      UIkit.modal.confirm(
        'Esta seguro que desea eliminar la notificacion <b>' +
          webhook.nombre +
          '</b> y sus reglas?',
        function() {
          modal = UIkit.modal.blockUI(
            "<div class='uk-text-center'>Eliminando notificacion...<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
          );

          FebosAPI.cl_eliminar_webhook(
            {
              webhookId: webhook.webhookId
            },
            {},
            true,
            false
          ).then(function success(response) {
            modal.hide();

            if (response.data.codigo == 10) {
              $scope.notificaciones.splice(index, 1);

              UIkit.modal.alert('Notificacion eliminada', {
                labels: { Ok: 'Listo!' }
              });
            } else {
              console.log(response);
              SesionFebos().error(
                response.data.codigo,
                response.data.mensaje,
                response.data.seguimientoId,
                response.data.errores
              );
            }
          });
        },
        { labels: { Ok: 'Si, eliminar', Cancel: 'No, me acabo de arrepentir' } }
      );
    };
  });
