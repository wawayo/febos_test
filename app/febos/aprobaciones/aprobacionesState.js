febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('restringido.aprobacion', {
        url: '/:app/aprobaciones',
        templateUrl: 'app/febos/aprobaciones/lista/aprobacionesView.html',
        controller: 'aprobacionesCtrl',
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/aprobaciones/lista/aprobacionesController.min.js',
                'app/febos/aprobaciones/aprobaciones.css'
              ]);
            }
          ]
        }
      })
      .state('restringido.aprobacionver', {
        url: '/:app/aprobaciones/ver/:aprobacion',
        templateUrl: 'app/febos/aprobaciones/crear/crear-view.html',
        controller: 'aprobacionesCrearCtrl',
        params: { ver: true },
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'lazy_ionRangeSlider',
                'app/febos/aprobaciones/crear/aprobacionesCrearController.min.js',
                'app/febos/aprobaciones/aprobaciones.css'
              ]);
            }
          ]
        }
      })

      .state('restringido.aprobacioncrear', {
        url: '/:app/aprobaciones/crear/:aprobacion',
        templateUrl: 'app/febos/aprobaciones/crear/crear-view.html',
        controller: 'aprobacionesCrearCtrl',
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'lazy_ionRangeSlider',
                'app/febos/aprobaciones/crear/aprobacionesCrearController.min.js',
                'app/febos/aprobaciones/aprobaciones.css'
              ]);
            }
          ]
        }
      })
      .state('restringido.aprobacionesbandejaobservador', {
        url: '/:app/aprobacionesbandejaobservador?pagina=&itemsPorPagina=&filtros',
        templateUrl: 'app/febos/aprobaciones/bandeja/aprobacionesBandeja.html',
        controller: 'aprobacionesBandejaCtrl',
        params: {
          pagina: '1',
          itemsPorPagina: '20',
          filtros: '',
          orden: '',
          bandeja: 'OBSERVADOR'
        },
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/aprobaciones/bandeja/aprobacionesBandejaController.min.js',
                'app/febos/aprobaciones/aprobaciones.css'
              ]);
            }
          ]
        }
      })
      .state('restringido.aprobacionesbandeja', {
        url: '/:app/aprobacionesbandeja?pagina=&itemsPorPagina=&filtros',
        templateUrl: 'app/febos/aprobaciones/bandeja/aprobacionesBandeja.html',
        controller: 'aprobacionesBandejaCtrl',
        params: {
          pagina: '1',
          itemsPorPagina: '20',
          filtros: '',
          orden: '',
          bandeja: 'ENTRADA'
        },
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/aprobaciones/bandeja/aprobacionesBandejaController.min.js',
                'app/febos/aprobaciones/aprobaciones.css'
              ]);
            }
          ]
        }
      })
      .state('restringido.aprobacionesbandejasalida', {
        url: '/:app/aprobacionesbandejasalida?pagina=&itemsPorPagina=&filtros',
        templateUrl: 'app/febos/aprobaciones/bandeja/aprobacionesBandeja.html',
        controller: 'aprobacionesBandejaCtrl',
        params: {
          pagina: '1',
          itemsPorPagina: '20',
          filtros: '',
          orden: '',
          bandeja: 'SALIDA'
        },
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/aprobaciones/bandeja/aprobacionesBandejaController.min.js',
                'app/febos/aprobaciones/aprobaciones.css'
              ]);
            }
          ]
        }
      })

      .state('restringido.aprobacionejecucioncrear', {
        url: '/:app/aprobaciones/ejecucioncrear/:aprobacion',
        templateUrl: 'app/febos/aprobaciones/crear_ejecucion/crear-view.html',
        controller: 'aprobacionesCrearEjecucionCtrl',
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'lazy_ionRangeSlider',
                'app/febos/aprobaciones/crear_ejecucion/aprobacionesCrearEjecucionController.min.js',
                'app/febos/aprobaciones/aprobaciones.css'
              ]);
            }
          ]
        }
      });
  }
]);
febosApp.filter('range', function() {
  return function(val, range) {
    range = parseInt(range);
    for (var i = 0; i < range; i++) val.push(i);
    return val;
  };
});
febosApp.directive('fbDragAndDropPaso', function($compile) {
  return {
    restrict: 'A',
    scope: { size: '=', pasos: '=' },
    link: function($scope, $element, $attrs, ngModel) {
      var $container, $items;
      function DragNSort(config) {
        this.$activeItem = null;
        $container = config.container;
        $items = $container.querySelectorAll('.' + config.itemClass);
        this.dragStartClass = config.dragStartClass;
        this.dragEnterClass = config.dragEnterClass;
      }

      DragNSort.prototype.removeClasses = function() {
        [].forEach.call(
          $items,
          function($item) {
            $item.classList.remove(this.dragStartClass, this.dragEnterClass);
          }.bind(this)
        );
      };

      DragNSort.prototype.on = function(eventType, handler) {
        [].forEach.call(
          $items,
          function(element) {
            console.log('removing');
            element.removeEventListener(eventType, handler.bind(element, this));
          }.bind(this)
        );
        [].forEach.call(
          $items,
          function(element) {
            element.addEventListener(eventType, handler.bind(element, this), false);
          }.bind(this)
        );
      };
      var counter = 0;
      DragNSort.prototype.onDragStart = function(_this, event) {
        _this.$activeItem = this;

        this.classList.add(_this.dragStartClass);
        event.dataTransfer.effectAllowed = 'move';
        event.dataTransfer.setData('text/html', this.innerHTML);
      };

      DragNSort.prototype.onDragEnd = function(_this) {
        this.classList.remove(_this.dragStartClass);
      };

      DragNSort.prototype.onDragEnter = function(_this) {
        counter++;
        this.classList.add(_this.dragEnterClass);
      };

      DragNSort.prototype.onDragLeave = function(_this) {
        counter--;
        //if (counter === 0) {
        this.classList.remove(_this.dragEnterClass);
        //}
      };

      DragNSort.prototype.onDragOver = function(_this, event) {
        if (event.preventDefault) {
          event.preventDefault();
        }
        event.dataTransfer.dropEffect = 'move';
        return false;
      };
      var lastItemDrop, lastItemDropTo;

      DragNSort.prototype.onDrop = function(_this, event) {
        console.log('ondropp');
        if (event.stopPropagation) {
          event.stopPropagation();
        }
        if (_this.$activeItem !== this && _this.$activeItem) {
          var receiveStep = _this.$activeItem.getAttribute('data-st');
          var moveStep = this.getAttribute('data-st');

          if (lastItemDrop == receiveStep && lastItemDropTo == moveStep) {
            //  _this.removeClasses();
            //return;
          }
          lastItemDrop = receiveStep;
          lastItemDropTo = moveStep;

          var tmp = [];
          for (var i = 0; i <= $scope.pasos.length - 1; i++) {
            console.log($scope.pasos[i].data.index, receiveStep, moveStep);
            tmp[i] = {};
            if ($scope.pasos[i].data.index == receiveStep) {
              console.log('recie');
              tmp[i].data = angular.copy($scope.pasos[moveStep - 1].data);
              tmp[i].data.index = receiveStep;
            }
            if ($scope.pasos[i].data.index == moveStep) {
              console.log('move');
              tmp[i].data = angular.copy($scope.pasos[receiveStep - 1].data);
              tmp[i].data.index = moveStep;
            }
          }
          for (var i = 0; i <= tmp.length - 1; i++) {
            if (tmp[i].data) {
              $scope.pasos[i].data = angular.copy(tmp[i].data);
              $scope.pasos[i].data.index = tmp[i].data.index;
            }
          }
          console.log($scope.pasos);

          this.setAttribute('data-st', moveStep);
          _this.$activeItem.setAttribute('data-st', receiveStep);
          $scope.$apply();

          //     draggable.init();
        }
        _this.removeClasses();
        return false;
      };

      DragNSort.prototype.bind = function() {
        this.on('dragstart', this.onDragStart);
        this.on('dragend', this.onDragEnd);
        this.on('dragover', this.onDragOver);
        this.on('dragenter', this.onDragEnter);
        this.on('dragleave', this.onDragLeave);
        this.on('drop', this.onDrop);
      };

      DragNSort.prototype.init = function() {
        this.bind();
      };
      var draggable;
      $scope.$watch('size', function(newValue, oldValue) {
        console.log('dragging');
        if (newValue == oldValue) {
          return;
        }

        draggable = new DragNSort({
          container: $element[0], //document.querySelector('.drag-list'),
          itemClass: 'drag-item',
          dragStartClass: 'drag-start',
          dragEnterClass: 'drag-enter'
        });
        draggable.init();
      });
    }
  };
});
