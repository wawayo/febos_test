angular
  .module('febosApp')
  .controller('aprobacionesBandejaCtrl', function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    $http,
    $state,
    utils,
    FebosAPI,
    SesionFebos,
    $stateParams,
    FebosUtil,
    AprobacionService,
    TIPOSGARANTIA
  ) {
    $scope.ambiente = '';
    try {
      var partes = window.location.href
        .replace('https://', '')
        .replace('http://', '')
        .split('/');
      $scope.ambiente = partes[1].match(posiblesAmbientes) != null ? partes[1] : 'desarrollo';
    } catch (e) {
      $scope.ambiente = 'desarrollo';
    }
    $scope.filtros = {};
    $scope.filtros.nombre_aprobacion = '';
    $scope.tipoBandeja = $stateParams.bandeja;
    $scope.aprobaciones = [];
    $scope.modal = {};
    $scope.modal.ejecucionEstadoActual = {};
    $scope.cargando = false;
    $scope.grabando = false;
    $scope.modal.continuar = false;
    $scope.cargandoDetalle = false;
    $scope.ipp = parseInt($location.search().itemsPorPagina, 10);
    $scope.totalPag = '';
    $scope.usuarios = [];
    $scope.estadoGatillaciones = {};
    const colores = ['#EF6C00', '#2196F3', '#8BC34A', '#9C27B0', '#DD2C00'];
    let coloresTomados = [];
    let coloresUsuarios = {};
    $scope.ESTADOS_EJ = {
      DESESTIMADA: '1',
      PREPARADA: '2',
      INICIADA: '3',
      APROBADA: '4',
      RECHAZADA: '5'
    };
    $scope.setColor = idUsuario => {
      if (coloresUsuarios[idUsuario]) {
        return coloresUsuarios[idUsuario];
      }
      colores.map(color => {
        if (coloresTomados.indexOf(color) < 0) {
          coloresUsuarios[idUsuario] = color;
          coloresTomados.push(color);
        }
      });
      return coloresUsuarios[idUsuario];
    };

    $scope.ESTADOS_PASO_COD = {
      '1': 'PROCESANDO',
      '2': 'PENDIENTE',
      '3': 'APROBADO',
      '4': 'RECHAZADO',
      '5': 'REENVIADO',
      '6': 'DEVUELTO'
    };

    $scope.ESTADOS_EJ_COD = {
      '1': 'DESESTIMADA',
      '2': 'PREPARADA',
      '3': 'INICIADA',
      '4': 'APROBADA',
      '5': 'RECHAZADA'
    };

    $scope.TIPO_PASO_COD = {
      1: 'GRUPO',
      2: 'USUARIO'
    };

    $scope.TIPO_ROL_COD = {
      1: 'APROBADOR',
      2: 'REVISOR'
    };

    $scope.estadoActual = estado => {
      if (estado == $scope.ESTADOS_EJ.APROBADA) {
        return 'passed';
      }
      if (estado == $scope.ESTADOS_EJ.RECHAZADA) {
        return 'error';
      }
      if (estado == $scope.ESTADOS_EJ.DESESTIMADA) {
        return 'warning';
      }

      return 'current';
    };

    $scope.limpiarFiltros = function() {
      $scope.filtros.nombre_aprobacion = '';
    };

    $scope.obtenerTipoBoleta = tipo => {
      if (!tipo) {
        return;
      }
      return TIPOSGARANTIA.find(item => {
        return item.id == tipo;
      }).title;
    };

    $scope.basicConfig = {
      plugins: {
        remove_button: {
          label: ''
        }
      },
      maxItems: 1,
      valueField: 'id',
      labelField: 'nombre',
      searchField: 'nombre',
      create: false,
      render: {
        option: function(valor, escape) {
          return (
            '<div class="option">' +
            '<div class="title">' +
            escape(valor.nombre) +
            '</div>' +
            '</div>'
          );
        },
        item: function(valor, escape) {
          return '<div class="item">' + escape(valor.nombre) + '</div>';
        }
      }
    };

    $scope.buscar = function() {
      $scope.cargando = true;
      FebosAPI.cl_aprobaciones_bandeja(
        {
          estado: true,
          filtros: $scope.filtros.nombre_aprobacion,
          tipoBandeja: $scope.tipoBandeja,
          pagina: $location.search().pagina,
          elementosPorPagina: parseInt($location.search().itemsPorPagina)
        },
        {},
        true,
        false
      ).then(function(response) {
        $scope.cargando = false;
        $scope.aprobaciones = response.data.bandejaEntradaList;
        $scope.paginacion = {
          elementoDesde:
            (parseInt($location.search().pagina) - 1) *
              parseInt($location.search().itemsPorPagina) +
            1,
          elementoHasta:
            (parseInt($location.search().pagina) - 1) *
              parseInt($location.search().itemsPorPagina) +
            $scope.aprobaciones.length,
          paginas: parseInt(response.data.totalPaginas)
        };
      });
    };
    $scope.buscar();

    $scope.acusarRecibo = function(seleccionado) {
      UIkit.modal.confirm(
        '¿Deseas acusar recibo de este documento? ',
        function() {
          FebosAPI.cl_acuse_recibo(
            {
              acuse: true,
              paso: seleccionado.pasoEjecucion.id
            },
            {},
            true,
            false
          ).then(function(response) {
            $scope.cargando = false;
            UIkit.modal.alert('¡Se ha acusado satisfactoriamente!');
          });
        },
        function() {
          seleccionado.pasoEjecucion.acuseRecibo = false;
        }
      );
    };

    /*    var query = {
      pagina: 1,
      itemsPorPagina: 10
    };
    $location.search(query);*/

    $scope.variablesPaginacion = {
      pagina: parseInt($location.search().pagina),
      itemsPorPagina: parseInt($location.search().itemsPorPagina)
    };

    $scope.avanzarPagina = function() {
      var params = $location.search();
      var siguientePagina = parseInt(params.pagina) + 1;
      if (siguientePagina > $scope.paginacion.paginas) {
        UIkit.notify("No hay mas páginas <a class='notify-action'>[X]</a> ", {
          status: 'danger',
          timeout: 2000
        });
        return;
      }
      $location.search('pagina', siguientePagina);
    };

    $scope.retrocederPagina = function() {
      var params = $location.search();
      var siguientePagina = parseInt(params.pagina) - 1;
      if (siguientePagina < 1) {
        UIkit.notify(
          "Ésta es la primera página, no se puede <br/>retroceder más =( <a class='notify-action'>[X]</a> ",
          {
            status: 'danger',
            timeout: 2000
          }
        );
        return;
      }
      $location.search('pagina', siguientePagina);
    };

    $scope.actualizarPagina = function() {
      if ($location.search().itemsPorPagina !== $scope.ipp) {
        $location.search('itemsPorPagina', $scope.ipp);
      }
    };

    $scope.procesar = accion => {
      $scope.grabando = true;
      var documentos = [];
      $scope.modal.adjuntos &&
        $scope.modal.adjuntos.map(adjunto => {
          documentos.push({
            tipo: accion,
            nombre: adjunto.adjuntoNombre,
            adjuntoId: adjunto.adjuntoId,
            adjuntoUrl: adjunto.adjuntoUrl,
            adjuntoMime: adjunto.adjuntoMime,
            adjuntoEstado: adjunto.adjuntoEstado
          });
        });
      FebosAPI.cl_aprobaciones_comentario_crear(
        {},
        {
          paso: $scope.modal.aprobacion.pasoEjecucion.id,
          comentario: {
            accion: accion,
            comentario: $scope.modal.comentario
          },
          documentos: documentos
        },
        true,
        false
      ).then(function(response) {
        $scope.procesarEjecucion($scope.modal.aprobacion, accion);
      });
    };

    $scope.buscarDetallesEjecucion = function(paso, tipo, accion) {
      $scope.modal.plantilla = tipo;
      $scope.modal.accion = accion;
      $scope.modal.aprobacion = paso;
      $scope.modal.idAprobacionActual = paso.idAprobacion;
      UIkit.modal('#verDetallesMdl').show();
      $scope.cargandoDetalle = true;
      if ($scope.modal.ejecucionEstadoActual[paso.idAprobacion]) {
        $scope.cargandoDetalle = false;
        return;
      }

      if (accion == 'reenviar') {
        FebosAPI.cl_listar_arbol_usuarios(
          {
            empresaId: SesionFebos().empresa.iut
          },
          true,
          false
        ).then(function(response) {
          var usuarios = response.data.usuarios;
          for (var i = 0; i < usuarios.length; i++) {
            $scope.usuarios.push(usuarios[i].usuario);
          }
        });
      }

      FebosAPI.cl_ejecuciones_estado_actual(
        {
          ejecucion: paso.idAprobacion
        },
        {},
        true,
        false
      ).then(function(response) {
        $scope.cargandoDetalle = false;
        $scope.modal.ejecucionEstadoActual[paso.idAprobacion] = response.data.detalle;
      });
    };

    $scope.cerrarModal = function() {
      UIkit.modal('#verDetallesMdl').hide();
    };

    $scope.ayuda = function() {
      UIkit.modal('#ayudaMdl').show();
    };

    $scope.verDocumento = function(documento) {
      $scope.cargandoDocumento = true;

      if (documento.objetoNombre == 'DNT') {
        FebosAPI.cl_obtener_dnt(
          {
            febosId: documento.objetoId,
            imagen: 'si',
            regenerar: 'si',
            incrustar: 'no'
          },
          {},
          false,
          true
        ).then(function(response) {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
          window.open(response.data.imagenLink);
          $scope.textoVerPdf = 'PDF';
          $scope.cargandoDocumento = false;
        });
      } else if (documento.tipoObjetoTexto == 'BG') {
        FebosAPI.cl_listar_boleta_garantia(
          {
            filtros: 'febosId:' + documento.objetoId,
            tipo: 'INFO'
          },
          {},
          false,
          true
        ).then(function(response) {
          console.log(response);
          $scope.boleta = response.data.boletas[0];
          UIkit.modal('#boletaMdl').show();
          $scope.cargandoDocumento = false;
        });
      } else if (documento.objetoNombre == 'DTE') {
        FebosAPI.cl_obtener_documento(
          {
            febosId: documento.objetoId,
            imagen: 'si',
            regenerar: 'si',
            incrustar: 'no',
            tipoImagen: 0
          },
          {},
          true,
          true
        ).then(function(response) {
          $rootScope.ultimoSeguimientoId = response.data.seguimientoId;
          try {
            if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
              window.open(response.data.imagenLink);
              $scope.textoVerPdf = 'PDF';
              $scope.cargandoDocumento = false;
            } else {
              $scope.textoVerPdf = 'PDF';
              SesionFebos().error(
                response.data.codigo,
                response.data.mensaje,
                response.data.seguimientoId
              );
            }
          } catch (e) {
            $scope.textoVerPdf = 'PDF';
            SesionFebos().error(response.data.codigo, e.message, response.data.seguimientoId);
          }
        });
      } else {
        $scope.cargandoDocumento = false;
      }
    };

    $scope.notificarObservador = function(observador) {
      // console.log('OBSERVADOR', observador);
    };

    $scope.procesarEjecucion = (ejecucion, accion) => {
      var temporalUsuario = '';
      var temporalUsuarioId = '';
      var temporalBool = false;
      if (accion == 'reenviar') {
        temporalUsuarioId = $scope.modal.usuarioSeleccionado;
        for (var i = 0; i < $scope.usuarios.length; i++) {
          if (temporalUsuarioId == $scope.usuarios[i].id) {
            temporalUsuario = $scope.usuarios[i].nombre;
          }
        }
        temporalBool = $scope.modal.continuar;
      }

      FebosAPI.cl_aprobaciones_procesar_flujo(
        {},
        {
          ejecucion: ejecucion.idAprobacion,
          paso: ejecucion.pasoEjecucion.id,
          estado: accion.toUpperCase(),
          temporalContinuar: temporalBool,
          temporalUsuario: temporalUsuario,
          temporalUsuarioId: temporalUsuarioId
        },
        true,
        false
      ).then(function(response) {
        UIkit.modal.alert('¡Se ha procesado la ejecución!');
        $scope.grabando = false;
        $scope.buscar();
        UIkit.modal('#verDetallesMdl').hide();
      });
    };

    $scope.seleccionarUsuario = function(usuario) {
      $scope.usuarioSeleccionado = usuario;
    };

    $scope.reenviarPaso = function() {
      UIkit.modal.confirm(
        '¿Deseas reenviar esta aprobación? ',
        function() {
          var nombreUsuario = '';
          for (var i = 0; i < $scope.usuarios.length; i++) {
            if ($scope.usuarioSeleccionado == $scope.usuarios[i].id) {
              nombreUsuario = $scope.usuarios[i].nombre;
            }
          }

          FebosAPI.cl_reenviar_ejecucion(
            {
              paso: $scope.aprobacionSeleccionada.pasoEjecucion.id,
              ejecucion: $scope.aprobacionSeleccionada.idAprobacion,
              estado: 'REENVIAR',
              temporalContinuar: $scope.modal.continuar,
              temporalUsuarioId: $scope.usuarioSeleccionado,
              temporalUsuario: nombreUsuario
            },
            {},
            true,
            false
          ).then(function() {
            UIkit.modal.alert('¡Se ha reenviado satisfactoriamente!');
            UIkit.modal('#reenviarMdl').hide();
          });
        },
        function() {
          //console.log('no hace nada');
        }
      );
    };

    $scope.usuarioConfig = {
      plugins: {
        remove_button: {
          label: ''
        }
      },
      maxItems: 1,
      valueField: 'id',
      labelField: 'nombre',
      searchField: 'nombre',
      create: false,
      render: {
        option: function(valor, escape) {
          return (
            '<div class="option">' +
            '<div class="title">' +
            escape(valor.nombre) +
            '</div>' +
            '</div>'
          );
        },
        item: function(valor, escape) {
          return '<div class="item">' + escape(valor.nombre) + '</div>';
        }
      }
    };
  });
