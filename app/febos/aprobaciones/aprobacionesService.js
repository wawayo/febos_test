febosApp.factory('AprobacionService', function($http, FebosAPI) {
  FebosAPI['cl_aprobaciones_listar'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_listar',
    'GET',
    "'/aprobaciones'"
  );

  FebosAPI['cl_aprobaciones_bandeja'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_bandeja',
    'GET',
    "'/aprobaciones/bandejaentrada'"
  );

  FebosAPI['cl_acuse_recibo'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_acusar_recibo',
    'PUT',
    "'/aprobaciones/acuserecibo'"
  );

  FebosAPI['cl_reenviar_ejecucion'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_reenviar_ejecucion',
    'PUT',
    "'/aprobaciones/procesarflujo'"
  );

  FebosAPI['cl_aprobaciones_procesar_flujo'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_procesar_flujo',
    'PUT',
    "'/aprobaciones/procesarflujo'"
  );

  FebosAPI['cl_ejecuciones_estado_actual'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_ejecuciones_estado_actual',
    'GET',
    "'/aprobaciones/ejecucionestadoactual'"
  );

  FebosAPI['cl_aprobaciones_listar_gatillar'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_listar_gatillar',
    'GET',
    "'/aprobaciones/gatilladortipo'"
  );

  FebosAPI['cl_listar_usuarios_grupos'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_listar_usuarios_grupos',
    'GET',
    "'/usuarios/porgrupo'"
  );

  FebosAPI['cl_aprobaciones_crear'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_crear',
    'POST',
    "'/aprobaciones'"
  );

  FebosAPI['cl_aprobaciones_listar_tipo_rol'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_listar_tipo_rol',
    'GET',
    "'/aprobaciones/tiporol'"
  );

  FebosAPI['cl_aprobaciones_listar_tipo_paso'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_listar_tipo_paso',
    'GET',
    "'/aprobaciones/tipopaso'"
  );

  FebosAPI['cl_aprobaciones_listar_gatillador_proceso'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_listar_gatillador_proceso',
    'GET',
    "'/aprobaciones/gatilladorproceso'"
  );

  FebosAPI['cl_aprobaciones_comentario_crear'] = FebosAPI.generarFuncionParaLlamadaAPI(
    'cl_aprobaciones_comentario_crear',
    'POST',
    "'/aprobaciones/comentario'"
  );

  return FebosAPI;
});
