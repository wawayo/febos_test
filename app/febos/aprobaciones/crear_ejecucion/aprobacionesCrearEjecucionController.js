angular
  .module('febosApp')
  .controller('aprobacionesCrearEjecucionCtrl', function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    $http,
    $state,
    utils,
    FebosAPI,
    SesionFebos,
    $stateParams,
    FebosUtil,
    AprobacionService
  ) {
    var TIPO_PASO = {
      GRUPO: 1,
      USUARIO: 2
    };

    $scope.app = $stateParams.app;
    $scope.aprobacionId = $stateParams.aprobacion;
    if (!$scope.aprobacionId) {
      $state.go('restringido.aprobacion', {
        app: $stateParams.app
      });
    }
    $scope.cargando = true;
    $scope.usuariosDelGrupo = [];

    $scope.pasos = [];
    $scope.gatilladores = [];
    $scope.gatilladoresProcesos = [];
    $scope.aprobacion = {};
    $scope.modal = {};
    $scope.aprobacion.observadores = [];
    $scope.aprobacion.gatilladores = [];
    $scope.observadores = [];
    $scope.listadoDeGrupos = [];
    $scope.responsables = [];
    $scope.roles = [];
    $scope.tipoPasos = [];
    $scope.map = {
      observadores: {},
      gatilladores: {},
      gatilladoresProceso: {},
      listadoDeGrupos: {},
      roles: {},
      tipoPasos: {}
    };
    $scope.nuevo = {
      observador: { persona: {} },
      gatilladorProceso: {},
      gatillador: {},
      tipoPaso: {},
      rol: {}
    };
    $scope.responsable = {};
    $scope.pasoSeleccionado = {};

    $scope.buscar = function() {
      FebosAPI.cl_aprobaciones_listar(
        { estado: true, id: $scope.aprobacionId },
        {},
        true,
        false
      ).then(function(response) {
        $scope.cargando = false;
        console.log(response.data);
        $scope.aprobacion = response.data.aprobaciones;
      });
    };
    $scope.buscar();

    $scope.verPaso = function(paso, index) {
      $scope.pasoSeleccionado = paso;
      $scope.pasoSeleccionado.$index = index;
      $scope.pasoSeleccionado.data.demoraMaxHoras = 0;
      $scope.modal.tipoPaso = paso.data.aprobacionesTipoPasoId;
      $scope.modal.rol = paso.data.aprobacionesTipoRolId;
      $scope.modal.responsable = paso.data.tipoValorResponsableId;

      UIkit.modal('#mdlConfigurarPaso').show();
    };

    $scope.grabar = function() {
      if ($scope.pasos.length < 1) {
        UIkit.modal.alert('No hay pasos para el flujo!');
        return;
      }
      if (!$scope.aprobacion.nombre || !$scope.aprobacion.descripcion) {
        UIkit.modal.alert('Ingrese nombre y descripcion');
        return;
      }
      // Validando pasos:
      for (var i = 0; i < $scope.pasos.length; i++) {
        if (!$scope.pasos[i].data.aprobacionesTipoPasoId) {
          UIkit.modal.alert('Ingrese tipo en paso ' + $scope.pasos[i].index);
          return;
        }
        if (!$scope.pasos[i].data.responsable) {
          UIkit.modal.alert('Ingrese responsable en paso ' + $scope.pasos[i].index);
          return;
        }
        if (!$scope.pasos[i].data.aprobacionesTipoRolId) {
          UIkit.modal.alert('Ingrese tipo de rol en paso ' + $scope.pasos[i].index);
          return;
        }
      }

      var observadores = [];
      for (var i = 0; i < $scope.aprobacion.observadores.length; i++) {
        observadores.push({
          observadorUsuarioId: $scope.aprobacion.observadores[i].id,
          observador: $scope.aprobacion.observadores[i].nombre,
          notificar: $scope.aprobacion.observadores[i].notificar
        });
      }
      var pasos = [];
      for (var i = 0; i < $scope.pasos.length; i++) {
        pasos.push({
          numero_paso: $scope.pasos[i].data.index,
          grupoUsuarios: $scope.pasos[i].data.grupoUsuarios,
          tipoValorResponsableId: $scope.pasos[i].data.tipoValorResponsableId,
          responsable: $scope.pasos[i].data.responsable.nombre,
          aprobacionesTipoRolId: $scope.pasos[i].data.aprobacionesTipoRolId,
          aprobacionesTipoPasoId: $scope.pasos[i].data.aprobacionesTipoPasoId
        });
      }

      var requestAprobacionesCrear = {
        pasos: pasos,
        observadores: observadores,
        gatilladores: $scope.aprobacion.gatilladores,
        aprobacion: $scope.aprobacion
      };
      console.log('REQUEST', requestAprobacionesCrear);
    };

    FebosAPI.cl_aprobaciones_listar_gatillar(
      {
        empresaId: SesionFebos().empresa.iut
      },
      true,
      false
    ).then(function(response) {
      $scope.gatilladores = response.data.lista;
      for (var i = 0; i < $scope.gatilladores.length; i++) {
        $scope.map.gatilladores[$scope.gatilladores[i].id] = $scope.gatilladores[i];
      }
    });

    FebosAPI.cl_aprobaciones_listar_gatillador_proceso(
      {
        empresaId: SesionFebos().empresa.iut
      },
      true,
      false
    ).then(function(response) {
      $scope.gatilladoresProcesos = response.data.lista;
      for (var i = 0; i < $scope.gatilladoresProcesos.length; i++) {
        $scope.map.gatilladoresProceso[$scope.gatilladoresProcesos[i].id] =
          $scope.gatilladoresProcesos[i];
      }
    });

    FebosAPI.cl_aprobaciones_listar_tipo_paso(
      {
        empresaId: SesionFebos().empresa.iut
      },
      true,
      false
    ).then(function(response) {
      $scope.tipoPasos = response.data.listaTipoPaso;
      for (var i = 0; i < $scope.tipoPasos.length; i++) {
        $scope.map.tipoPasos[$scope.tipoPasos[i].id] = $scope.tipoPasos[i];
      }
    });

    FebosAPI.cl_aprobaciones_listar_tipo_rol(
      {
        empresaId: SesionFebos().empresa.iut
      },
      true,
      false
    ).then(function(response) {
      $scope.roles = response.data.listaTipoRol;
      for (var i = 0; i < $scope.roles.length; i++) {
        $scope.map.roles[$scope.roles[i].id] = $scope.roles[i];
      }
    });

    $scope.tiposDeDocumentos = [
      { valor: '33', nombre: 'Factura (E) Afecta' },
      { valor: '34', nombre: 'Factura (E) Exenta' },
      { valor: '39', nombre: 'Boleta (E)' },
      { valor: '41', nombre: 'Boleta (E) Exenta' },
      { valor: '43', nombre: 'Liquidación de Factura (E)' },
      { valor: '46', nombre: 'Factura de Compra (E)' },
      { valor: '52', nombre: 'Guía de Despacho (E)' },
      { valor: '56', nombre: 'Nota de Débito (E)' },
      { valor: '61', nombre: 'Nota de Crédito (E)' },
      { valor: '66', nombre: 'Boleta (E) de honorarios' },
      { valor: '110', nombre: 'Factura de Exportación (E)' },
      { valor: '111', nombre: 'Nota de Débito de Exp. (E)' },
      { valor: '112', nombre: 'Nota de Crédito de Exp. (E)' }
    ];

    $scope.formatoFecha = {
      format: 'YYYY-MM-DD',
      i18n: {
        months: [
          'Enero',
          'Febrero',
          'Marzo',
          'Abril',
          'Mayo',
          'Junio',
          'Julio',
          'Agosto',
          'Septiembre',
          'Octubre',
          'Noviembre',
          'Diciembre'
        ],
        weekdays: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
      }
    };

    $scope.basicConfig = {
      plugins: {
        remove_button: {
          label: ''
        }
      },
      maxItems: 1,
      valueField: 'id',
      labelField: 'nombre',
      searchField: 'nombre',
      create: false,
      render: {
        option: function(valor, escape) {
          return (
            '<div class="option">' +
            '<div class="title">' +
            escape(valor.nombre) +
            '</div>' +
            '</div>'
          );
        },
        item: function(valor, escape) {
          return '<div class="item">' + escape(valor.nombre) + '</div>';
        }
      }
    };
  });
