angular
  .module('febosApp')
  .controller('aprobacionesCrearCtrl', function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    $http,
    $state,
    utils,
    FebosAPI,
    SesionFebos,
    $stateParams,
    FebosUtil,
    AprobacionService
  ) {
    var TIPO_PASO = {
      GRUPO: 1,
      USUARIO: 2
    };
    $scope.TIPO_PASO = TIPO_PASO;

    $scope.app = $stateParams.app;
    $scope.aprobacionId = $stateParams.aprobacion;
    $scope.lectura = $stateParams.ver;
    $scope.cargando = false;
    $scope.cargandoPaso = false;
    $scope.grabando = false;
    $scope.usuariosDelGrupo = [];

    $scope.titulo = getTitulo();
    $scope.pasos = [];
    $scope.gatilladores = [];
    $scope.gatilladoresProcesos = [];
    $scope.aprobacion = {};
    $scope.modal = { responsables: [] };
    $scope.aprobacion.observadores = [];
    $scope.aprobacion.gatilladores = [];
    $scope.observadores = [];
    $scope.listadoDeGrupos = [];
    $scope.responsables = [];
    $scope.roles = [];
    $scope.tipoPasos = [];
    $scope.map = {
      observadores: {},
      gatilladores: {},
      gatilladoresProceso: {},
      listadoDeGrupos: {},
      roles: {},
      tipoPasos: {}
    };
    $scope.nuevo = {
      observador: { persona: {} },
      gatilladorProceso: {},
      gatillador: {},
      tipoPaso: {},
      rol: {}
    };
    $scope.responsable = {};
    $scope.pasoSeleccionado = {};

    // Traer datos al modificar
    function cargarModificar() {
      if ($scope.aprobacionId) {
        $scope.cargando = true;
        FebosAPI.cl_aprobaciones_listar(
          { estado: true, id: $scope.aprobacionId },
          {},
          true,
          false
        ).then(function(response) {
          $scope.cargando = false;
          $scope.aprobacion = response.data.aprobacion.aprobacion;
          $scope.aprobacion.observadores = [];
          $scope.aprobacion.gatilladores = [];

          for (var i = 0; i < response.data.aprobacion.gatillaciones.length; i++) {
            var gatillacion = response.data.aprobacion.gatillaciones[i];
            $scope.aprobacion.gatilladores.push({
              gatillador_proceso: gatillacion.aprobacionesGatilladorProcesoId,
              gatillador: gatillacion.aprobacionesGatilladorId,
              tipo: gatillacion.estado,
              proceso: gatillacion.proceso
            });
          }

          for (var i = 0; i < response.data.aprobacion.observadores.length; i++) {
            var observador = response.data.aprobacion.observadores[i];
            $scope.aprobacion.observadores.push({
              id: observador.observadorUsuarioId,
              nombre: observador.observador,
              notificar: observador.notificar
            });
          }

          for (var i = 0; i < response.data.aprobacion.pasos.length; i++) {
            var paso = response.data.aprobacion.pasos[i];
            var responsable = {};
            var grupoUsuarios = [];
            var id = paso.tipoValorResponsableId;

            if (paso.aprobacionesTipoPasoId == TIPO_PASO.GRUPO) {
              responsable = $scope.map.listadoDeGrupos[id];
              $scope.cargarUsuariosDeGrupo(id, function(data) {
                $scope.pasos.push({
                  data: {
                    subDesde: paso.subrogadoDesde,
                    subHasta: paso.subrogadoHasta,
                    subrogante: paso.subrogadoId,
                    grupoUsuarios: data,
                    tipoValorResponsableId: id,
                    responsable: responsable,
                    cantidadMinAprobacion: paso.cantidadMinAprobacion,
                    demoraMaxHoras: paso.demoraMaxHoras,
                    redirigirFlujo: paso.redirigirFlujo,
                    aprobacionesTipoRolId: paso.aprobacionesTipoRolId,
                    aprobacionesTipoPasoId: paso.aprobacionesTipoPasoId,
                    index: paso.numeroPaso
                  }
                });
              });
            } else if (paso.aprobacionesTipoPasoId == TIPO_PASO.USUARIO) {
              responsable = $scope.map.observadores[id];
              $scope.pasos.push({
                data: {
                  subDesde: paso.subrogadoDesde,
                  subHasta: paso.subrogadoHasta,
                  subrogante: paso.subrogadoId,
                  grupoUsuarios: [],
                  demoraMaxHoras: paso.demoraMaxHoras,
                  redirigirFlujo: paso.redirigirFlujo,
                  tipoValorResponsableId: id,
                  responsable: responsable,
                  aprobacionesTipoRolId: paso.aprobacionesTipoRolId,
                  aprobacionesTipoPasoId: paso.aprobacionesTipoPasoId,
                  index: paso.numeroPaso
                }
              });
            }
          }
        });
      }
    }

    $scope.$watch('nuevo.tipoPaso.id', function(newValue, oldValue, scope) {
      if (newValue == oldValue) {
        return;
      }
      $scope.responsable = {};
    });

    $scope.actualizarPosiblesSubrogantes = function() {
      if ($scope.modal.responsable === $scope.modal.subrogante) {
        $scope.modal.subrogante = null;
      }
      /**
       * Porque $timeout:
       * Condición: seleccioné subrogante, luego cambié el responsable al subrogante.
       * Comportamiento esperado: se deselecciona el subrogante.
       * Problema: al deseleccionar el subrogante ocurre el filtrado de selectize y el
       * elemento queda en el listado.
       * Solución: se usa $timeout para que ocurra en el proximo ciclo, $timeout hará un $apply de su callback.
       * no se puede usar directamente $apply porque ya se encuentra en un ciclo.
       */
      $timeout(function() {
        $scope.modal.posiblesSubrogantes = $scope.modal.responsables.filter(function(responsable) {
          return responsable.id !== $scope.modal.responsable;
        });
      });
    };

    $scope.configurarPaso = function(paso, index) {
      $scope.pasoSeleccionado = angular.copy(paso);
      $scope.pasoSeleccionado.$index = index;
      $scope.modal.tipoPaso = paso.data.aprobacionesTipoPasoId;
      $scope.modal.rol = paso.data.aprobacionesTipoRolId;

      $scope.modal.demoraMaxHoras = paso.data.demoraMaxHoras ? paso.data.demoraMaxHoras : 0;
      $scope.modal.redirigirFlujo = paso.data.redirigirFlujo;
      $scope.modal.cantidadMinAprobacion = paso.data.cantidadMinAprobacion;
      $scope.modal.subDesde = paso.data.subDesde;
      $scope.modal.subHasta = paso.data.subHasta;
      $scope.modal.subrogante = paso.data.subrogante;
      $scope.modal.subrogado = paso.data.subrogante ? true : false;

      if (paso.data.aprobacionesTipoPasoId == TIPO_PASO.GRUPO) {
        $scope.modal.responsables = angular.copy($scope.listadoDeGrupos);
      } else if (paso.data.aprobacionesTipoPasoId == TIPO_PASO.USUARIO) {
        $scope.modal.responsables = angular.copy($scope.observadores);
      }

      $scope.modal.responsable = paso.data.tipoValorResponsableId;
      $scope.actualizarPosiblesSubrogantes();

      UIkit.modal('#mdlConfigurarPaso').show();
    };

    $scope.eliminarPaso = function(index) {
      for (var i = 0; i < $scope.pasos.length; i++) {
        if (i > index) {
          $scope.pasos[i].data.index--;
        }
      }
      $scope.pasos.splice(index, 1);
    };

    $scope.informacionCompletada = function(paso) {
      if (
        !paso.data.tipoValorResponsableId ||
        !paso.data.responsable ||
        !paso.data.aprobacionesTipoPasoId ||
        !paso.data.aprobacionesTipoRolId
      ) {
        return false;
      }
      return true;
    };

    $scope.agregarPaso = function() {
      var responsable = {};
      var grupoUsuarios = [];

      if (
        $scope.pasos.some(function(paso) {
          return (
            paso.data.tipoValorResponsableId === $scope.responsable.id &&
            paso.data.aprobacionesTipoRolId === $scope.nuevo.rol.id
          );
        })
      ) {
        UIkit.modal.alert('Seleccione un usuario y/o rol distinto.');
        return;
      }

      if ($scope.nuevo.tipoPaso.id == TIPO_PASO.GRUPO) {
        var id = $scope.responsable.id;
        responsable = $scope.map.listadoDeGrupos[id];
        $scope.cargandoPaso = true;
        $scope.cargarUsuariosDeGrupo(id, function(data) {
          $scope.cargandoPaso = false;
          $scope.pasos.push({
            data: {
              grupoUsuarios: data,
              tipoValorResponsableId: id,
              responsable: responsable,
              cantidadMinAprobacion: 1,
              demoraMaxHoras: 0,
              aprobacionesTipoRolId: $scope.nuevo.rol.id,
              aprobacionesTipoPasoId: $scope.nuevo.tipoPaso.id,
              tipoPaso: $scope.map.tipoPasos[$scope.nuevo.tipoPaso.id],
              tipoRol: $scope.map.roles[$scope.nuevo.rol.id],
              index: $scope.pasos.length + 1
            }
          });
        });
      } else if ($scope.nuevo.tipoPaso.id == TIPO_PASO.USUARIO) {
        var id = $scope.responsable.id;
        responsable = $scope.map.observadores[id];
        $scope.pasos.push({
          data: {
            grupoUsuarios: [],
            tipoValorResponsableId: id,
            responsable: responsable,
            aprobacionesTipoRolId: $scope.nuevo.rol.id,
            aprobacionesTipoPasoId: $scope.nuevo.tipoPaso.id,
            tipoPaso: $scope.map.tipoPasos[$scope.nuevo.tipoPaso.id],
            tipoRol: $scope.map.roles[$scope.nuevo.rol.id],
            index: $scope.pasos.length + 1
          }
        });
      }
    };

    $scope.agregarGatillador = function() {
      if (!$scope.nuevo.gatillador.id || !$scope.nuevo.gatilladorProceso.id) {
        UIkit.modal.alert('Seleccione los campos necesarios');
        return;
      }
      for (var i = 0; i < $scope.aprobacion.gatilladores.length; i++) {
        if ($scope.nuevo.gatillador.id == $scope.aprobacion.gatilladores[i].idTipo) {
          UIkit.modal.alert('Gatillador ya existe');
          return;
        }
      }
      $scope.aprobacion.gatilladores.push({
        gatillador_proceso: $scope.nuevo.gatilladorProceso.id,
        gatillador: $scope.nuevo.gatillador.id,
        tipo: $scope.map.gatilladores[$scope.nuevo.gatillador.id].nombre,
        proceso: $scope.map.gatilladoresProceso[$scope.nuevo.gatilladorProceso.id].nombre
      });
    };

    $scope.agregarObservador = function() {
      if (!$scope.nuevo.observador.id) {
        UIkit.modal.alert('Seleccione los campos necesarios');
        return;
      }
      for (var i = 0; i < $scope.aprobacion.observadores.length; i++) {
        if ($scope.nuevo.observador.id == $scope.aprobacion.observadores[i].id) {
          UIkit.modal.alert('Observador ya existe');
          return;
        }
      }
      $scope.aprobacion.observadores.push({
        id: $scope.nuevo.observador.id,
        nombre: $scope.map.observadores[$scope.nuevo.observador.id].nombre,
        notificar: $scope.nuevo.observador.notificar
      });
    };

    $scope.eliminarObservador = function(idx) {
      $scope.aprobacion.observadores.splice(idx, 1);
    };

    $scope.eliminarGatillador = function(idx) {
      $scope.aprobacion.gatilladores.splice(idx, 1);
    };

    $scope.guardarPaso = function() {
      var responsable = {};
      var grupoUsuarios = [];

      var i = $scope.pasoSeleccionado.$index;

      if ($scope.modal.tipoPaso == TIPO_PASO.GRUPO) {
        responsable = $scope.map.listadoDeGrupos[$scope.modal.responsable];
        $scope.cargarUsuariosDeGrupo($scope.modal.responsable, function(data) {
          $scope.pasos[i].data.grupoUsuarios = data;
        });
      } else if ($scope.modal.tipoPaso == TIPO_PASO.USUARIO) {
        responsable = $scope.map.observadores[$scope.modal.responsable];
        $scope.pasos[i].data.grupoUsuarios = [];
      }

      $scope.pasos[i].data.tipoValorResponsableId = $scope.modal.responsable;
      $scope.pasos[i].data.responsable = responsable;
      $scope.pasos[i].data.aprobacionesTipoRolId = $scope.modal.rol;
      $scope.pasos[i].data.aprobacionesTipoPasoId = $scope.modal.tipoPaso;
      $scope.pasos[i].data.demoraMaxHoras = $scope.modal.demoraMaxHoras;
      $scope.pasos[i].data.redirigirFlujo = $scope.modal.redirigirFlujo;

      $scope.pasos[i].data.cantidadMinAprobacion = $scope.modal.cantidadMinAprobacion
        ? $scope.modal.cantidadMinAprobacion
        : 1;

      if ($scope.modal.subrogado) {
        $scope.pasos[i].data.subDesde = $scope.modal.subDesde;
        $scope.pasos[i].data.subHasta = $scope.modal.subHasta;
        $scope.pasos[i].data.subrogante = $scope.modal.subrogante;
      } else {
        $scope.modal.subrogado = false;
        $scope.pasos[i].data.subDesde = '';
        $scope.pasos[i].data.subHasta = '';
        $scope.pasos[i].data.subrogante = '';
      }

      UIkit.modal('#mdlConfigurarPaso').hide();
    };

    $scope.cerrarPaso = function() {
      UIkit.modal('#mdlConfigurarPaso').hide();
    };

    $scope.grabar = function() {
      $scope.grabando = true;
      if ($scope.pasos.length < 1) {
        UIkit.modal.alert('No hay pasos para el flujo!');
        $scope.grabando = false;
        return;
      }
      if (!$scope.aprobacion.nombre || !$scope.aprobacion.descripcion) {
        UIkit.modal.alert('Ingrese nombre y descripcion');
        return;
      }
      // Validando pasos:
      for (var i = 0; i < $scope.pasos.length; i++) {
        if (!$scope.pasos[i].data.aprobacionesTipoPasoId) {
          UIkit.modal.alert('Ingrese tipo en paso ' + $scope.pasos[i].index);
          return;
        }
        if (!$scope.pasos[i].data.responsable) {
          UIkit.modal.alert('Ingrese responsable en paso ' + $scope.pasos[i].index);
          return;
        }
        if (!$scope.pasos[i].data.aprobacionesTipoRolId) {
          UIkit.modal.alert('Ingrese tipo de rol en paso ' + $scope.pasos[i].index);
          return;
        }
      }

      var observadores = [];
      for (var i = 0; i < $scope.aprobacion.observadores.length; i++) {
        observadores.push({
          observadorUsuarioId: $scope.aprobacion.observadores[i].id,
          observador: $scope.aprobacion.observadores[i].nombre,
          notificar: $scope.aprobacion.observadores[i].notificar
        });
      }
      var pasos = [];
      for (var i = 0; i < $scope.pasos.length; i++) {
        var p = {
          numeroPaso: $scope.pasos[i].data.index,
          grupoUsuarios: $scope.pasos[i].data.grupoUsuarios,
          cantidadMinAprobacion: $scope.pasos[i].data.cantidadMinAprobacion
            ? $scope.pasos[i].data.cantidadMinAprobacion
            : 1,
          demoraMaxHoras: $scope.pasos[i].data.demoraMaxHoras,
          redirigirFlujo: $scope.pasos[i].data.redirigirFlujo,
          tipoValorResponsableId: $scope.pasos[i].data.tipoValorResponsableId,
          responsable: $scope.pasos[i].data.responsable.nombre,
          aprobacionesTipoRolId: $scope.pasos[i].data.aprobacionesTipoRolId,
          aprobacionesTipoPasoId: $scope.pasos[i].data.aprobacionesTipoPasoId,
          subrogadoId: $scope.pasos[i].data.subrogante,
          subrogado: $scope.pasos[i].data.subrogante
            ? $scope.map.observadores[$scope.pasos[i].data.subrogante].nombre
            : ''
        };
        if (p.subrogado) {
          p.subrogadoDesde = $scope.pasos[i].data.subDesde + ' 00:00:00.000';
          p.subrogadoHasta = $scope.pasos[i].data.subHasta + ' 00:00:00.000';
        }
        pasos.push(p);
      }

      var requestAprobacionesCrear = {
        pasos: pasos,
        observadores: observadores,
        gatilladores: $scope.aprobacion.gatilladores,
        aprobacion: $scope.aprobacion
      };
      FebosAPI.cl_aprobaciones_crear({}, requestAprobacionesCrear, true, false).then(function(
        response
      ) {
        $scope.grabando = false;
        if (response.data.codigo == 10 || typeof response.data.codigo == '10') {
          $state.go('restringido.aprobacion', {
            app: $stateParams.app
          });
          UIkit.modal.alert('Aprobacion creada satisfactoriamente');
        }
      });
    };

    FebosAPI.cl_aprobaciones_listar_gatillar(
      {
        empresaId: SesionFebos().empresa.iut
      },
      true,
      false
    ).then(function(response) {
      $scope.gatilladores = response.data.lista;
      for (var i = 0; i < $scope.gatilladores.length; i++) {
        $scope.map.gatilladores[$scope.gatilladores[i].id] = $scope.gatilladores[i];
      }
    });

    FebosAPI.cl_aprobaciones_listar_gatillador_proceso(
      {
        empresaId: SesionFebos().empresa.iut
      },
      true,
      false
    ).then(function(response) {
      $scope.gatilladoresProcesos = response.data.lista;
      for (var i = 0; i < $scope.gatilladoresProcesos.length; i++) {
        $scope.map.gatilladoresProceso[$scope.gatilladoresProcesos[i].id] =
          $scope.gatilladoresProcesos[i];
      }
    });

    FebosAPI.cl_aprobaciones_listar_tipo_paso(
      {
        empresaId: SesionFebos().empresa.iut
      },
      true,
      false
    ).then(function(response) {
      $scope.tipoPasos = response.data.listaTipoPaso;
      for (var i = 0; i < $scope.tipoPasos.length; i++) {
        $scope.map.tipoPasos[$scope.tipoPasos[i].id] = $scope.tipoPasos[i];
      }
    });

    FebosAPI.cl_aprobaciones_listar_tipo_rol(
      {
        empresaId: SesionFebos().empresa.iut
      },
      true,
      false
    ).then(function(response) {
      $scope.roles = response.data.listaTipoRol;
      for (var i = 0; i < $scope.roles.length; i++) {
        $scope.map.roles[$scope.roles[i].id] = $scope.roles[i];
      }
    });

    FebosAPI.cl_listar_arbol_usuarios(
      {
        empresaId: SesionFebos().empresa.iut
      },
      true,
      false
    ).then(function(response) {
      var usuarios = response.data.usuarios;
      for (var i = 0; i < usuarios.length; i++) {
        $scope.observadores.push(usuarios[i].usuario);
        $scope.map.observadores[usuarios[i].usuario.id] = usuarios[i].usuario;
      }
    });

    FebosAPI.cl_listar_grupos(
      {
        empresaId: SesionFebos().empresa.iut
      },
      true,
      false
    ).then(function(response) {
      if (response.data.grupos.length < 1) {
        UIkit.modal.alert('No existen grupos');
      }
      $scope.listadoDeGrupos = response.data.grupos;
      for (var i = 0; i < $scope.listadoDeGrupos.length; i++) {
        $scope.map.listadoDeGrupos[$scope.listadoDeGrupos[i].id] = $scope.listadoDeGrupos[i];
      }
      cargarModificar();
    });
    $scope.cargarUsuariosDeGrupo = function(grupoId, callback) {
      var usuarios = [];
      FebosAPI.cl_listar_usuarios_grupos(
        {
          empresaId: SesionFebos().empresa.iut,
          grupoId: grupoId
        },
        true,
        false
      ).then(function(response) {
        var grupos = [];
        for (var i = 0; i < response.data.grupoUsuarios.length; i++) {
          grupos.push({
            tipoValorResponsableId: response.data.grupoUsuarios[i].usuarioId
          });
        }
        callback(grupos);
      });
    };

    function getTitulo() {
      if ($scope.lectura) {
        return 'Ver plantilla de aprobacion';
      } else if ($scope.aprobacionId) {
        return 'Modificar plantilla de aprobacion';
      } else {
        return 'Ver plantilla de aprobacion';
      }
    }
    $scope.basicConfig = {
      plugins: {
        remove_button: {
          label: ''
        }
      },
      maxItems: 1,
      valueField: 'id',
      labelField: 'nombre',
      searchField: 'nombre',
      create: false,
      render: {
        option: function(valor, escape) {
          return (
            '<div class="option">' +
            '<div class="title">' +
            escape(valor.nombre) +
            '</div>' +
            '</div>'
          );
        },
        item: function(valor, escape) {
          return '<div class="item">' + escape(valor.nombre) + '</div>';
        }
      }
    };
  });
