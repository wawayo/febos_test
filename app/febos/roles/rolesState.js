febosApp
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider.state("restringido.roles", {
                    url: "/:app/roles",
                    templateUrl: 'app/febos/roles/gestionRolesView.html',
                    controller: 'gestionRolesCtrl',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_uikit',
                                    'lazy_iCheck',
                                    'app/febos/roles/gestionRolesController.js'
                                ]);
                            }]
                    }
                })

            }]);

