angular
  .module('febosApp')
  .controller('gestionRolesCtrl', function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $http,
    utils,
    $state,
    $location,
    SesionFebos,
    FebosAPI,
    FebosUtil
  ) {
    var permisos = {
      plugins: {
        remove_button: {
          label: ''
        }
      },
      maxItems: null,
      valueField: 'codigo',
      labelField: 'nombre',
      searchField: 'nombre',
      create: false,
      render: {
        option: function(permiso, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(permiso.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(permiso, escape) {
          return '<div class="item">' + escape(permiso.nombre) + '</div>';
        }
      }
    };

    $scope.cargando = false;
    $scope.checkGlobal = false;

    // -- scope que carga información al html de roles
    $scope.informacion = {
      titulo: 'Gestión de Roles',
      listar: {
        //< campos extra: filtro, columna
        titulo: 'Filtrar Roles', //< para los filtros
        cargando: 'Filtrando Roles...', //< para el botón
        tituloActual: 'Filtrar Roles', //< para el botón
        url: SesionFebos().api + '/empresas/' + SesionFebos().empresa.id + '/roles',
        metodo: 'GET',
        campos: [
          {
            titulo: '',
            solicitud: 'debug',
            tipo: 'param',
            valor: 'si',
            filtro: false,
            columna: false
          },
          {
            titulo: '',
            solicitud: 'simular',
            tipo: 'param',
            valor: 'no',
            filtro: false,
            columna: false
          },
          {
            titulo: 'Nombre Rol',
            solicitud: 'nombre',
            tipo: 'param',
            valor: '',
            filtro: true,
            columna: true
          },
          {
            titulo: 'Descripción Rol',
            solicitud: 'descripcion',
            tipo: 'param',
            valor: '',
            filtro: true,
            columna: true
          },
          {
            titulo: '',
            solicitud: 'pagina',
            tipo: 'param',
            valor: '1',
            filtro: false,
            columna: false
          },
          {
            titulo: 'Filas',
            solicitud: 'filas',
            tipo: 'param',
            valor: '10',
            filtro: false,
            columna: false
          }
        ],
        elementos: {
          nombre: 'roles',
          valor: [],
          propiedades: [],
          paginacion: 9,
          totalElementos: 0,
          totalPaginas: 0,
          pagina: 1,
          filas: 10,
          paginas: []
        } //< propiedades se generan a partir de campos, nombre es el nombre en el response del arreglo de elementos
      },
      crear: {
        //< campos extra: formulario
        titulo: 'Crear Rol', //< para el botón
        cargando: 'Creando Rol...', //< para el botón
        tituloActual: 'Crear Rol', //< para el botón
        url: SesionFebos().api + '/empresas/' + SesionFebos().empresa.id + '/roles',
        metodo: 'POST',
        campos: [
          { titulo: '', solicitud: 'simular', tipo: 'param', valor: 'no', formulario: false },
          { titulo: 'Nombre Rol', solicitud: 'nombre', tipo: 'data', valor: '', formulario: true },
          {
            titulo: 'Descripción Rol',
            solicitud: 'descripcion',
            tipo: 'data',
            valor: '',
            formulario: true
          }
        ]
      },
      modificar: {
        //< campos extra: formulario, modificable.
        titulo: 'Modificar Rol', //< para el botón
        cargando: 'Modificando Rol...', //< para el botón
        tituloActual: 'Modificar Rol', //< para el botón
        url: SesionFebos().api + '/empresas/' + SesionFebos().empresa.id + '/roles/',
        index: '', //< para modificar
        metodo: 'PUT',
        campos: [
          {
            titulo: '',
            solicitud: 'simular',
            tipo: 'param',
            valor: 'no',
            formulario: false,
            modificable: false
          },
          {
            titulo: 'Nombre Rol',
            solicitud: 'nombre',
            tipo: 'data',
            valor: '',
            formulario: true,
            modificable: true
          },
          {
            titulo: 'Descripción Rol',
            solicitud: 'descripcion',
            tipo: 'data',
            valor: '',
            formulario: true,
            modificable: true
          }
        ]
      },
      eliminar: {
        //< campos extra: ninguno
        titulo: 'Eliminar Rol', //< para el botón
        cargando: 'Eliminando Rol...', //< para el botón
        tituloActual: 'Eliminar Rol', //< para el botón
        url: SesionFebos().api + '/empresas/' + SesionFebos().empresa.id + '/roles/',
        index: '', //< para eliminar
        metodo: 'DELETE',
        campos: [{ titulo: '', solicitud: 'simular', tipo: 'param', valor: 'no' }]
      },
      obtenerPermisos: {
        //< campos extra: ninguno
        titulo: 'Permisos', //< para el modal
        cargando: 'Obteniendo Permisos...', //< para el modal
        tituloActual: 'Permisos', //< para el modal
        url: SesionFebos().api + '/empresas/' + SesionFebos().empresa.id + '/roles/', //< el resto de la url se agrega después
        index: '', //< del rol para asignarle los permisos especificados
        metodo: 'GET',
        campos: [
          { titulo: '', solicitud: 'debug', tipo: 'param', valor: 'si' },
          { titulo: '', solicitud: 'simular', tipo: 'param', valor: 'no' }
        ],
        elementos: {
          nombre: 'permisos',
          valor: [],
          propiedades: ['codigo', 'nombre'],
          configuracion: permisos
        }
      },
      asignarPermisos: {
        //< campos extra: ninguno
        titulo: 'Asignar Permisos', //< para el botón
        cargando: 'Asignando Permisos...', //< para el botón
        tituloActual: 'Asignar Permisos', //< para el botón
        url: SesionFebos().api + '/empresas/' + SesionFebos().empresa.id + '/roles/', //< el resto de la url se agrega después
        index: '', //< del rol para asignarle los permisos especificados
        metodo: 'POST',
        campos: [
          { titulo: '', solicitud: 'simular', tipo: 'param', valor: 'no' },
          { titulo: 'Permisos', solicitud: 'permisos', tipo: 'data', valor: '' }
        ],
        permisos: []
      }
    };
    //console.log(JSON.stringify($scope.informacion));
    // --

    // -- información listado
    $scope.informacion['listar'].elementos.propiedades = [];
    for (var j = 0; j < $scope.informacion['listar'].campos.length; j++) {
      $scope.cargando = true;
      if (
        ($scope.informacion['listar'].campos[j].columna != undefined ||
          typeof $scope.informacion['listar'].campos[j].columna != 'undefined') &&
        $scope.informacion['listar'].campos[j].columna != ''
      ) {
        $scope.informacion['listar'].elementos.propiedades.push(
          $scope.informacion['listar'].campos[j].solicitud
        );
      }
    }
    // --

    // -- función que activa modal modificar rol
    $scope.abrirModificar = function(index) {
      $scope.informacion.modificar.tituloActual = $scope.informacion.modificar.titulo;
      $scope.informacion['modificar'].index = index;

      for (var j = 0; j < $scope.informacion['modificar'].campos.length; j++) {
        if (
          ($scope.informacion['modificar'].campos[j].formulario != undefined ||
            typeof $scope.informacion['modificar'].campos[j].formulario != 'undefined') &&
          $scope.informacion['modificar'].campos[j].formulario
        ) {
          $scope.informacion['modificar'].campos[j].valor =
            $scope.informacion['listar'].elementos.valor[index][
              $scope.informacion['modificar'].campos[j].solicitud
            ];
        }
      }

      var modal = UIkit.modal('#mdModificar');
      modal.show();
    };
    // --

    // -- función que cierra el modal de modificación de rol
    $scope.cerrarModificar = function() {
      $scope.informacion['modificar'].index = '';

      var modal = UIkit.modal('#mdModificar');
      modal.hide();
    };
    // --

    // -- función que activa modal para asignar permisos
    $scope.abrirPermisos = function(index, elemento) {
      $scope.permisoSeleccionado = elemento;
      $scope.informacion['obtenerPermisos'].index = index;
      $scope.informacion['asignarPermisos'].index = index;
      $scope.informacion.obtenerPermisos.tituloActual = $scope.informacion.obtenerPermisos.titulo;
      $scope.permisosDisponibles = [];
      $scope.permisosTemporales = [];
      $scope.filtro_permiso = '';
      $scope.filtro_permisotemp = '';
      $scope.enviarSolicitud('obtenerPermisos');

      var modal = UIkit.modal('#mdPermisos');
      modal.show();
    };
    // --

    // -- función que cierra el modal que asigna permisos
    $scope.cerrarPermisos = function() {
      $scope.informacion['obtenerPermisos'].index = '';
      $scope.informacion['asignarPermisos'].index = '';

      var modal = UIkit.modal('#mdPermisos');
      modal.hide();
    };
    // --

    // -- función que activa el modal para eliminar un rol
    $scope.abrirEliminar = function(index) {
      $scope.informacion.eliminar.tituloActual = $scope.informacion.eliminar.titulo;
      $scope.informacion['eliminar'].index = index;

      var modal = UIkit.modal('#mdEliminar');
      modal.show();
    };
    // --

    // -- función que cierra el modal que elimina un rol
    $scope.cerrarEliminar = function() {
      $scope.informacion['eliminar'].index = '';

      var modal = UIkit.modal('#mdEliminar');
      modal.hide();
    };
    // --

    // -- función que activa modal para crear un nuevo rol
    $scope.abrirCrear = function(index) {
      $scope.informacion.crear.tituloActual = $scope.informacion.crear.titulo;

      for (var j = 0; j < $scope.informacion['crear'].campos.length; j++) {
        $scope.informacion['crear'].campos[j].valor = '';
      }

      var modal = UIkit.modal('#mdCrear');
      modal.show();
    };
    // --

    // -- función que cierra el modal que crea un nuevo rol
    $scope.cerrarCrear = function() {
      var modal = UIkit.modal('#mdCrear');
      modal.hide();
    };
    // --

    // -- función que genera las acciones de crear, eliminar, modificar un rol

    $scope.enviarSolicitud = function(accion) {
      var parametros = {};
      var datos = {};
      if ($scope.informacion[accion].tituloActual != $scope.informacion[accion].titulo) {
        return;
      }

      var informacion = $scope.informacion[accion];

      if (accion === 'eliminar') {
        //< acción eliminar rol
        $scope.informacion.eliminar.tituloActual = $scope.informacion.eliminar.cargando;
        parametros.empresaId = SesionFebos().empresa.id;
        parametros.rolId = $scope.informacion['listar'].elementos.valor[informacion.index].id;
      }

      if (accion === 'crear') {
        //< acción crear rol
        parametros.empresaId = SesionFebos().empresa.id;
        $scope.informacion.crear.tituloActual = $scope.informacion.crear.cargando;
        var errores = '';
        for (var i = 0; i < informacion.campos.length; i++) {
          if (informacion.campos[i].formulario == true) {
            if (informacion.campos[i].valor == '') {
              $scope.informacion.crear.tituloActual = $scope.informacion.crear.titulo;
              errores +=
                '\nDebe ingresar un valor para el campo ' + informacion.campos[i].solicitud + '.';
            }
          }
        }

        if (errores != '') {
          $scope.informacion.crear.tituloActual = $scope.informacion.crear.titulo;
          UIkit.modal.alert('Errores encontrados:' + errores);
          return;
        }
      }

      if (accion === 'modificar') {
        //< acción modificar rol
        var errores = '';
        parametros.empresaId = SesionFebos().empresa.id;
        parametros.rolId = $scope.informacion['listar'].elementos.valor[informacion.index].id;
        for (var i = 0; i < informacion.campos.length; i++) {
          if (informacion.campos[i].formulario == true) {
            if (informacion.campos[i].valor == '') {
              errores +=
                '\nDebe ingresar un valor para el campo ' + informacion.campos[i].solicitud + '.';
            }
          }
        }

        if (errores != '') {
          UIkit.modal.alert('Errores encontrados:' + errores);
          return;
        }

        $scope.informacion.modificar.tituloActual = $scope.informacion.modificar.cargando;
      }

      if (accion === 'obtenerPermisos') {
        //< acción obtener permisos de un rol

        parametros.empresaId = SesionFebos().empresa.id;
        parametros.rolId = $scope.informacion['listar'].elementos.valor[informacion.index].id;
        $scope.informacion['asignarPermisos'].tituloActual =
          $scope.informacion.obtenerPermisos.cargando;
        $scope.informacion.asignarPermisos.permisos = [];
      }

      if (accion === 'asignarPermisos') {
        //< acción asignar permisos a un rol
        $scope.informacion.asignarPermisos.tituloActual =
          $scope.informacion.asignarPermisos.cargando;
        parametros.empresaId = SesionFebos().empresa.id;
        parametros.rolId = $scope.informacion['listar'].elementos.valor[informacion.index].id;
        for (var i = 0; i < $scope.informacion.asignarPermisos.campos.length; i++) {
          if ($scope.informacion.asignarPermisos.campos[i].solicitud == 'permisos') {
            if ($scope.informacion.asignarPermisos.permisos.constructor === Array) {
              $scope.informacion.asignarPermisos.campos[
                i
              ].valor = $scope.informacion.asignarPermisos.permisos.join(',');
            } else {
              $scope.informacion.asignarPermisos.campos[i].valor =
                $scope.informacion.asignarPermisos.permisos;
            }
          }
        }
      }

      for (var i = 0; i < informacion.campos.length; i++) {
        if (informacion.campos[i].tipo == 'param') {
          if (informacion.campos[i].solicitud == 'pagina') {
            $scope.informacion[accion].campos[i].valor =
              $scope.informacion['listar'].elementos.pagina + '';
          }

          if (informacion.campos[i].solicitud == 'filas') {
            $scope.informacion[accion].campos[i].valor =
              $scope.informacion['listar'].elementos.filas + '';
          }

          parametros[informacion.campos[i].solicitud] = informacion.campos[i].valor;
        }

        if (informacion.campos[i].tipo == 'data') {
          datos[informacion.campos[i].solicitud] = informacion.campos[i].valor;
        }
      }

      if (accion == 'crear') {
        datos.global = $scope.checkGlobal ? 'si' : 'no';
      }
      if (accion == 'modificar') {
        datos.global = $scope.checkGlobalModificar ? 'si' : 'no';
      }
      console.log('checkGlobal');
      console.log($scope.checkGlobal);
      var solicitud = {
        //< request que se envía a la API
        method: informacion.metodo,
        url:
          informacion.url +
          ((informacion.index != undefined || typeof informacion.index != 'undefined') &&
          informacion.index !== ''
            ? $scope.informacion['listar'].elementos.valor[informacion.index].id +
              (accion == 'obtenerPermisos' || accion == 'asignarPermisos' ? '/permisos' : '')
            : ''),
        params: parametros,
        data: datos
      };

      //Modificaciones nueva API

      if (accion === 'obtenerPermisos') {
        FebosAPI.cl_listar_permisos_rol(parametros, datos, true, false).then(function(response) {
          $scope.informacion.crear.tituloActual = $scope.informacion.crear.titulo;

          $scope.informacion.obtenerPermisos.elementos.valor = [];
          $scope.informacion.asignarPermisos.permisos = [];
          $scope.permisosDisponibles = [];
          $scope.permisosTemporales = [];

          var respuesta = response.data[$scope.informacion.obtenerPermisos.elementos.nombre];
          for (var i = 0; i < respuesta.length; i++) {
            var elemento = {};
            var temp = {};
            var dis = {};
            for (var j = 0; j < informacion.elementos.propiedades.length; j++) {
              if (
                respuesta[i][informacion.elementos.propiedades[j]] != undefined ||
                typeof respuesta[i][informacion.elementos.propiedades[j]] != 'undefined'
              ) {
                elemento[informacion.elementos.propiedades[j]] =
                  respuesta[i][informacion.elementos.propiedades[j]];
              }
            }
            $scope.informacion['obtenerPermisos'].elementos.valor.push(elemento);

            if (
              (respuesta[i].codigo != undefined || typeof respuesta[i].codigo != 'undefined') &&
              (respuesta[i].asignado != undefined || typeof respuesta[i].asignado != 'undefined') &&
              respuesta[i].asignado == 'si'
            ) {
              temp.codigo = respuesta[i].codigo;
              temp.nombre = respuesta[i].nombre;
              $scope.permisosTemporales.push(temp);
              $scope.informacion.asignarPermisos.permisos.push(respuesta[i].codigo);
            } else if (
              (respuesta[i].codigo != undefined || typeof respuesta[i].codigo != 'undefined') &&
              (respuesta[i].asignado != undefined || typeof respuesta[i].asignado != 'undefined') &&
              respuesta[i].asignado == 'no'
            ) {
              dis.codigo = respuesta[i].codigo;
              dis.nombre = respuesta[i].nombre;
              $scope.permisosDisponibles.push(dis);
            }
          }

          //console.log(JSON.stringify());
          $scope.informacion['asignarPermisos'].tituloActual =
            $scope.informacion.asignarPermisos.titulo;
        });
      }
      if (accion === 'asignarPermisos') {
        FebosAPI.cl_modificar_permisos_rol(parametros, datos, true, false).then(function(response) {
          $scope.informacion['asignarPermisos'].tituloActual =
            $scope.informacion.asignarPermisos.titulo;
          UIkit.modal.alert('Permisos Asignados Correctamente.');
        });
      }
      if (accion === 'listar') {
        parametros.empresaId = SesionFebos().empresa.id;
        parametros.ambito = FebosUtil.obtenerAmbito();
        FebosAPI.cl_listar_roles(parametros, datos, true, false).then(function(response) {
          $scope.informacion.crear.tituloActual = $scope.informacion.crear.titulo;
          $scope.loader = response.data.roles;
          $scope.informacion.listar.elementos.valor = [];
          var respuesta = response.data[$scope.informacion.listar.elementos.nombre];

          for (var i = 0; i < respuesta.length; i++) {
            var elemento = {};
            if (respuesta[i].id != undefined || typeof respuesta[i].id != 'undefined') {
              elemento['id'] = respuesta[i].id;
            }

            for (var j = 0; j < informacion.elementos.propiedades.length; j++) {
              if (
                respuesta[i][informacion.elementos.propiedades[j]] != undefined ||
                typeof respuesta[i][informacion.elementos.propiedades[j]] != 'undefined'
              ) {
                elemento[informacion.elementos.propiedades[j]] =
                  respuesta[i][informacion.elementos.propiedades[j]];
              }
            }
            elemento['empresa'] = respuesta[i].empresa;

            $scope.informacion['listar'].elementos.valor.push(elemento);
          }

          if (
            response.data.totalElementos == undefined ||
            typeof response.data.totalElementos == 'undefined'
          ) {
            $scope.informacion['listar'].elementos.totalElementos = 0;
            $scope.cargando = false;
          } else {
            $scope.informacion['listar'].elementos.totalElementos = parseInt(
              response.data.totalElementos
            );
            $scope.cargando = false;
          }

          $scope.calcularPaginas();
        });
      } else {
        if (accion === 'eliminar') {
          //< acción eliminar
          FebosAPI.cl_eliminar_rol(parametros, datos, true, false).then(function(response) {
            $scope.cerrarEliminar();
            $scope.cambiarPagina('inicio');
          });
        }

        if (accion === 'crear') {
          //< acción crear 'cerrar'
          FebosAPI.cl_crear_rol(parametros, datos, true, false).then(function(response) {
            $scope.cerrarCrear();
            $scope.cambiarPagina('inicio');
          });
        }

        if (accion === 'modificar') {
          //< acción modificar
          FebosAPI.cl_modificar_rol(parametros, datos, true, false).then(function(response) {
            $scope.cerrarModificar();
            $scope.cambiarPagina('inicio');
          });
        }
      }

      //Fin modificaciones
    };
    $scope.agregarPermisos = function(index) {
      console.log('agregado' + index);
      for (var i = 0; i < $scope.permisosDisponibles.length; i++) {
        if ($scope.permisosDisponibles[i].codigo == index) {
          $scope.permisosTemporales.push($scope.permisosDisponibles[i]);
          $scope.permisosDisponibles.splice(i, 1);
          $scope.informacion.asignarPermisos.permisos.push(index);
          //console.log(JSON.stringify($scope.rolesTemporales));
        }
      }
      console.log(JSON.stringify($scope.informacion.asignarPermisos.permisos));
      //$scope.ajustarPermisos();
      ////console.log("$scope.roles_usuario: "+$scope.roles_usuario);
    };
    $scope.quitarPermisos = function(index) {
      console.log('quitado' + index);
      for (var i = 0; i < $scope.permisosTemporales.length; i++) {
        if ($scope.permisosTemporales[i].codigo == index) {
          $scope.permisosDisponibles.push($scope.permisosTemporales[i]);
          $scope.permisosTemporales.splice(i, 1);
          $scope.informacion.asignarPermisos.permisos.splice(i, 1);
        }
      }
      console.log(JSON.stringify($scope.informacion.asignarPermisos.permisos));
      //$scope.ajustarPermisos();
    };
    $scope.ajustarPermisos = function(accion) {
      for (var i = 0; i < $scope.permisosTemporales.length; i++) {
        if ($scope.permisosTemporales[i].codigo != '') {
          $scope.informacion.asignarPermisos.permisos.push($scope.permisosTemporales[i].codigo);
        }
      }
      console.log(JSON.stringify($scope.informacion.asignarPermisos.permisos));
    };
    // --

    // -- función para limpiar filtros
    $scope.limpiarFiltros = function() {
      for (var j = 0; j < $scope.informacion['listar'].campos.length; j++) {
        if (
          ($scope.informacion['listar'].campos[j].filtro != undefined ||
            typeof $scope.informacion['listar'].campos[j].filtro != 'undefined') &&
          $scope.informacion['listar'].campos[j].filtro
        ) {
          /*if ($scope.informacion["listar"].campos[j].valor.constructor === Array) {
                    $scope.informacion["listar"].campos[j].valor = [];
                } else {
                    $scope.informacion["listar"].campos[j].valor = "";
                }*/

          $scope.informacion['listar'].campos[j].valor = '';
        }
      }
    };
    // --

    // -- función que cambia de página
    $scope.cambiarPagina = function(index) {
      $scope.informacion['listar'].elementos.paginacion = Math.ceil(
        $scope.informacion['listar'].elementos.paginacion
      );
      $scope.informacion['listar'].elementos.totalElementos = Math.ceil(
        $scope.informacion['listar'].elementos.totalElementos
      );

      if (index == 'inicio') {
        $scope.informacion['listar'].elementos.pagina = 1;
      } else {
        if (index == 'final') {
          $scope.informacion['listar'].elementos.pagina =
            $scope.informacion['listar'].elementos.totalPaginas;
        } else {
          if (
            index !== '' &&
            typeof index != 'undefined' &&
            typeof $scope.informacion['listar'].elementos.paginas[index] != 'undefined'
          ) {
            $scope.informacion['listar'].elementos.pagina =
              $scope.informacion['listar'].elementos.paginas[index].numero;
          } else {
            $scope.informacion['listar'].elementos.pagina = 1;
          }
        }
      }

      $scope.enviarSolicitud('listar');
    };
    // --

    // -- función que calcula la cantidad de páginas
    $scope.calcularPaginas = function() {
      $scope.informacion['listar'].elementos.totalPaginas = Math.ceil(
        $scope.informacion['listar'].elementos.totalElementos /
          $scope.informacion['listar'].elementos.filas
      );

      var resta = -(Math.ceil($scope.informacion['listar'].elementos.paginacion) % 2 == 0
        ? Math.ceil($scope.informacion['listar'].elementos.paginacion / 2 - 1)
        : Math.ceil($scope.informacion['listar'].elementos.paginacion / 2 - 1));
      var suma =
        Math.ceil($scope.informacion['listar'].elementos.paginacion) % 2 == 0
          ? Math.ceil($scope.informacion['listar'].elementos.paginacion / 2)
          : Math.ceil($scope.informacion['listar'].elementos.paginacion / 2 - 1);
      var first = false;

      if (Math.ceil($scope.informacion['listar'].elementos.pagina) + resta <= 0) {
        first = true;
        suma -= Math.ceil($scope.informacion['listar'].elementos.pagina) + (resta - 1);
        resta -= Math.ceil($scope.informacion['listar'].elementos.pagina) + (resta - 1);
      }

      if ($scope.informacion['listar'].elementos.totalPaginas <= 0)
        suma = $scope.informacion['listar'].elementos.paginacion + resta - 1;

      if (
        Math.ceil($scope.informacion['listar'].elementos.pagina) + suma >
          $scope.informacion['listar'].elementos.totalPaginas &&
        $scope.informacion['listar'].elementos.totalPaginas > 0
      ) {
        if (first) {
          suma -=
            Math.ceil($scope.informacion['listar'].elementos.pagina + suma) -
            $scope.informacion['listar'].elementos.totalPaginas;
        } else {
          suma -=
            Math.ceil($scope.informacion['listar'].elementos.pagina + suma) -
            $scope.informacion['listar'].elementos.totalPaginas;
        }
      }

      var cont = 0;
      $scope.informacion['listar'].elementos.paginas = [];

      for (
        var i = $scope.informacion['listar'].elementos.pagina + resta;
        i <= $scope.informacion['listar'].elementos.pagina + suma;
        i++
      ) {
        var nuevaPagina = {
          id: cont,
          numero: i
        };

        $scope.informacion['listar'].elementos.paginas.push(nuevaPagina);
        cont++;
      }
    };
    // --

    $scope.cambiarPagina('inicio'); //< inicio del mantenedor/listado de roles
  });
