angular
  .module('febosApp')
  .controller('notificacionesGestionCtrl', function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    $http,
    $state,
    utils,
    FebosAPI,
    SesionFebos,
    FebosUtil,
    $stateParams
  ) {
    $scope.notificacion = $stateParams.notificacion;

    $scope.esEditar = false;
    $scope.reglaId = '';

    $scope.holding_config = {
      plugins: {
        remove_button: {
          label: ''
        }
      },
      maxItems: 1,
      valueField: 'id',
      labelField: 'nombre',
      searchField: 'id',
      create: false,
      render: {
        option: function(permiso, escape) {
          return (
            '<div class="option">' +
            '<span class="title">' +
            escape(permiso.nombre) +
            '</span>' +
            '</div>'
          );
        },
        item: function(permiso, escape) {
          return '<div class="item">' + escape(permiso.nombre) + '</div>';
        }
      }
    };

    $scope.regla = function(type, value) {
      this.type = type;
      this.value = value;
    };

    $scope.tipoTag = function(a) {
      return FebosUtil.tipoTag(a);
    };

    $scope.tipoCondicion = function(a) {
      return FebosUtil.tipoCondicion(a);
    };

    $scope.tipoTagForCompare = function(a) {
      return FebosUtil.tipoTagForCompare(a);
    };
    $scope.tipoCondicionForCompare = function(a) {
      return FebosUtil.tipoCondicionForCompare(a);
    };

    $scope.replaceAll = function(str, find, replace) {
      return str.replace(new RegExp(find, 'g'), replace);
    };

    $scope.javascript2humano = function(str) {
      var arrTag = [];
      var arrCondicion = [];
      var arrValor = [];
      var arrRespuesta = [];
      str = $scope.replaceAll(str, '&& ', ''); //elimino todas las concatenaciones
      var arrEntrada = str.split(' '); //separo por espacio todos los valores

      arrEntrada.forEach(function(element) {
        //console.log(element);
        if (element == $scope.tipoTagForCompare(element)) {
          //reviso si el elemento es tipo tag y lo guardo en el arreglo de tags

          arrTag.push(element);
        } else if (element == $scope.tipoCondicionForCompare(element)) {
          //reviso si el elemento es tipo condicion y lo guardo en el arreglo de condiciones
          arrCondicion.push(element);
        } else if (element.includes('includes')) {
          //si el elemento tiene la palabra include quiere decir que es un
          element = element.replace(/[^\w\.,s]/gi, ''); //se eliminan todos ls caracteres especiales []() menos las comas y los puntos

          //console.log(element);

          element = $scope.replaceAll(element, '.includes', ' includes '); //se reemplaza el .inlude para luego separar el valor del tag y la condicion
          var arrIncludTag = element.split(' ');
          arrValor.push(arrIncludTag[0]); // primer elemento es siempre un tag
          arrCondicion.push(arrIncludTag[1]);
          arrTag.push(arrIncludTag[2]);
        } else if (element.includes('indexOf')) {
          element = $scope.replaceAll(element, '!=-1', '');
          element = element.replace(/[^\w\.,;&s]/gi, '');

          //console.log(element);

          element = $scope.replaceAll(element, '.indexOf', ' indexOf ');
          var arrIncludTag = element.split(' ');
          arrValor.push(arrIncludTag[0]);
          arrCondicion.push(arrIncludTag[1]);
          arrTag.push(arrIncludTag[2]);
        } else if (element.includes('startsWith')) {
          element = element.replace(/[^\w\.,;&s]/gi, '');

          //console.log(element);

          element = $scope.replaceAll(element, '.startsWith', ' startsWith ');
          var arrIncludTag = element.split(' ');
          arrValor.push(arrIncludTag[2]);
          arrCondicion.push(arrIncludTag[1]);
          arrTag.push(arrIncludTag[0]);
        } else if (element.includes('endsWith')) {
          element = element.replace(/[^\w\.,;&s]/gi, '');

          //console.log(element);

          element = $scope.replaceAll(element, '.endsWith', ' endsWith ');
          var arrIncludTag = element.split(' ');
          arrValor.push(arrIncludTag[2]);
          arrCondicion.push(arrIncludTag[1]);
          arrTag.push(arrIncludTag[0]);
        } else {
          element = element.replace(/[^\w\.,-;&s]/gi, '');
          arrValor.push(element);
        }
      });

      arrTag.forEach(function(valor, indx, array) {
        arrRespuesta.push({
          tag: valor,
          tipo_condicion: arrCondicion[indx],
          valor: arrValor[indx]
        });
      });

      /*//console.log("arrTag", arrTag);
      //console.log("arrCondicion", arrCondicion);
      //console.log("arrValor", arrValor);
      //console.log("arrRespuesta", arrRespuesta);*/

      return arrRespuesta;
    };

    $scope.humano2javascript = function(arrEntrada) {
      var str = '';
      arrEntrada.forEach(function(element) {
        //console.log(element);
        // console.log(str);
        if (element.tipo_condicion.includes('includes')) {
          var arr = element.valor.split(','); //el include siempre va a tener valores separados por coma
          str =
            (str == '' ? '' : str + ' && ') +
            '[' +
            (arr.length === 0 ? '' : "'") +
            arr.join("','") +
            "'" +
            ']'; //concateno con && si ya existe contenido y a los valores separados por coma le añado commillas si son string
          str = str + '.' + element.tipo_condicion + ''; //concateno la condicion
          str = str + '(' + element.tag + ')'; //concateno el tag
        } else if (element.tipo_condicion.includes('indexOf')) {
          str = (str == '' ? '' : str + ' && ') + "'" + element.valor + "'";
          str = str + '.' + element.tipo_condicion + '';
          str = str + '(' + element.tag + ')!=-1';
        } else if (
          element.tipo_condicion.includes('startsWith') ||
          element.tipo_condicion.includes('endsWith')
        ) {
          str = (str == '' ? '' : str + ' && ') + element.tag;
          str = str + '.' + element.tipo_condicion + '';
          str = str + "('" + element.valor + "')";
        } else {
          str = (str == '' ? '' : str + ' && ') + element.tag + ' ' + element.tipo_condicion + ' ';
          if (!isNaN(element.valor)) {
            str = str + element.valor;
          } else {
            str = str + "'" + element.valor + "'";
          }
        }
      });

      return str;
    };

    var strJs =
      "DTE.Encabezado.Emisor.RUTEmisor == '76092373-7' && ['rut1','rut2','rut2'].includes(DTE.Encabezado.Emisor.RUTEmisor)";
    strJs =
      strJs +
      " && DTE.Encabezado.Totales.MntTotal >= 2500000 && 'Servicios&nbsp;Informaticos'.indexOf(DTE.Encabezado.Receptor.GiroRecep)!=-1";
    strJs =
      strJs +
      " && DTE.Encabezado.Receptor.GiroRecep.startsWith('Industria') && DTE.Encabezado.Receptor.GiroRecep.endsWith('SPA')";

    var prueba = $scope.javascript2humano(strJs);
    console.log(prueba);
    console.log($scope.humano2javascript(prueba));

    $scope.notificaciones = {
      nombre: '',
      tipoEvento: '',
      url: '',
      token: '',
      usuario: '',
      password: '',
      formato: 'JSON',
      nombreWebhook: '',
      reintentos: '',
      notificaciones: false,
      adjuntarDte: false,
      adjuntarDnt: false,
      adjuntarPdf: false,
      correos: []
    };

    $scope.tipo_evento_options = [
      //{nombre: 'RECEPCION_ARCHIVO_INTEGRACION_TXT', id: '1'},
      //{nombre: 'RECEPCION_ARCHIVO_INTEGRACION_XML', id: '2'},
      //{nombre: 'ERROR_INTEGRACION', id: '3'},
      //{nombre: 'DOCUMENTO_PREPROCESADO', id: '4'},
      //{nombre: 'DOCUMENTO_FIRMADO', id: '5'},
      //{nombre: 'DOCUMENTO_ENVIADO_EAI', id: '6'},
      //{nombre: 'DOCUMENTO_RECEPCIONADO_EAI', id: '7'},
      //{nombre: 'ERROR_AL_ENVIAR_A_EAI', id: '8'},
      //{nombre: 'ERROR_RESPUESTA_EAI', id: '9'},
      { nombre: '1.- Cuando el documento fue aceptado por el SII', id: '10' },
      { nombre: '2.- Cuando el documento fue aceptado con reparos por el SII', id: '11' },
      { nombre: '3.- Cuando el documento fue rechazado por el SII', id: '12' },
      { nombre: '4.- Cuando el documento fue enviado al receptor electrónico', id: '13' },
      { nombre: '5.- Cuando el documento fue enviado a un receptor personalizado', id: '14' },
      { nombre: '6.- Cuando el receptor emite un acuse de recibo', id: '15' },
      { nombre: '7.- Cuando el receptor indica que no recibio el documento', id: '16' },
      { nombre: '8.- Cuando el receptor ACEPTA comercialmente el documento', id: '17' },
      //{nombre: 'DOCUMENTO_ACEPTADO_REPAROS_COMERCIALEMENTE_POR_RECEPTOR', id: '18'},
      { nombre: '9.- Cuando el receptor RECHAZA comercialmente el documento', id: '19' },
      { nombre: '10.- Cuando el receptor acusa recibo de mercaderías o servicios', id: '20' },
      { nombre: '11.- Cuando el receptor acuse recibo PARCIAL de mercaderías', id: '21' },
      { nombre: '12.- Cuando el receptor hace DEVOLUCION TOTAL de mercaderías', id: '22' },
      { nombre: '13.- Cuando el receptor hace DEVOLUCION PARCIAL de mercaderías', id: '23' },
      // {nombre: 'COMENTARIO_DE_USUARIO', id: '24'},
      // {nombre: 'ASIGNACION_FOLIO', id: '25'},
      // {nombre: 'ASIGNACION_CAF', id: '26'},
      // {nombre: 'ALMACENAMIENTO_XML_DOCUMENTO', id: '27'},
      // {nombre: 'VISUALIZACION_XML', id: '28'},
      //  {nombre: 'VISUALIZACION_BASICA', id: '29'},
      // {nombre: 'VISUALIZACION_IMAGEN_TIPO_0', id: '30'},
      //{nombre: 'VISUALIZACION_IMAGEN_TIPO_1', id: '31'},
      //{nombre: 'VISUALIZACION_IMAGEN_TIPO_2', id: '32'},
      //{nombre: 'VISUALIZACION_IMAGEN_TIPO_3', id: '33'},
      //{nombre: 'VISUALIZACION_IMAGEN_TIPO_4', id: '34'},
      //{nombre: 'VISUALIZACION_IMAGEN_TIPO_5', id: '35'},
      //{nombre: 'VISUALIZACION_IMAGEN_TIPO_6', id: '36'},
      //{nombre: 'VISUALIZACION_IMAGEN_TIPO_7', id: '37'},
      {
        nombre: '14.- Cuando se recepciona un documento, pero aun no esta verificado en el SII',
        id: '38'
      },
      { nombre: '15.- Cuando se recepciona un documento y esta APROBADO por el SII', id: '39' },
      { nombre: '16.- Cuando se recepciona un documento NO APROBADO por el SII ', id: '40' },
      //{nombre: 'ALMACENAMIENTO_ADJUNTO_DOCUMENTO', id: '41'},
      //{nombre: 'DOCUMENTO_CONSULTADO_EAI', id: '42'},
      //{nombre: 'SOLICITUD_DTE_NO_RECIBIDO', id: '43'},
      { nombre: '17.- Cuando un documento fue CEDIDO', id: '44' },
      { nombre: '18.- Cuando la cesion de un documento fue RECHAZADA', id: '45' },
      { nombre: '19.- Cuando se actualiza la información de pago de un documento', id: '46' },
      { nombre: '20.- Vencimiento Boleta Garantia', id: '65' }
      //{nombre: 'DTE_RECIBIDO_EN_VALIDACION', id: '47'}
    ];

    $scope.tipo_cuenta_options = [
      { id: '==', nombre: 'Igual a' },
      { id: '!=', nombre: 'Distinto a' },
      { id: '<', nombre: 'Menor que' },
      { id: '<=', nombre: 'Menor o igual que' },
      { id: '>', nombre: 'Mayor que' },
      { id: '>=', nombre: 'Mayor o igual que' },
      { id: 'includes', nombre: 'Esta en (lista separada por ,)' },
      { id: 'indexOf', nombre: 'Contiene' },
      { id: 'startsWith', nombre: 'Inicia con' },
      { id: 'endsWith', nombre: 'Termina con' }
    ];

    $scope.dato_options = [
      { id: 'bitacora.mensaje', nombre: 'Mensaje Bitacora' },
      { id: 'DTE.Encabezado.IdDoc.TipoDTE', nombre: 'Tipo DTE' },
      { id: 'DTE.Encabezado.IdDoc.Folio', nombre: 'Folio' },
      { id: 'DTE.Encabezado.IdDoc.FchEmis', nombre: 'Fecha Emision' },
      { id: 'DTE.Encabezado.IdDoc.IndServicio', nombre: 'Indicador Servicio' },
      { id: 'DTE.Encabezado.IdDoc.PeriodoDesde', nombre: 'Periodo desde' },
      { id: 'DTE.Encabezado.IdDoc.PeriodoHasta', nombre: 'Periodo hasta' },
      { id: 'DTE.Encabezado.Emisor.RUTEmisor', nombre: 'Rut emisor' },
      { id: 'DTE.Encabezado.Emisor.RznSoc', nombre: 'Razon Social emisor' },
      { id: 'DTE.Encabezado.Emisor.GiroEmis', nombre: 'Giro emisor' },
      { id: 'DTE.Encabezado.Emisor.Telefono', nombre: 'Telefono emisor' },
      { id: 'DTE.Encabezado.Emisor.Acteco', nombre: 'Acteco emisor' },
      { id: 'DTE.Encabezado.Emisor.Sucursal', nombre: 'Sucursal emisor' },
      { id: 'DTE.Encabezado.Emisor.DirOrigen', nombre: 'Direccion origen emisor' },
      { id: 'DTE.Encabezado.Emisor.CmnaOrigen', nombre: 'Comuna origen emisor' },
      { id: 'DTE.Encabezado.Emisor.CiudadOrigen', nombre: 'Ciudad origen emisor' },
      { id: 'DTE.Encabezado.Receptor.RUTRecep', nombre: 'Rut receptor' },
      { id: 'DTE.Encabezado.Receptor.CdgIntRecep', nombre: 'Codigo interno receptor' },
      { id: 'DTE.Encabezado.Receptor.RznSocRecep', nombre: 'Razon social receptor' },
      { id: 'DTE.Encabezado.Receptor.GiroRecep', nombre: 'Giro receptor' },
      { id: 'DTE.Encabezado.Receptor.Contacto', nombre: 'Contacto receptor' },
      { id: 'DTE.Encabezado.Receptor.DirRecep', nombre: 'Direccion receptor' },
      { id: 'DTE.Encabezado.Receptor.CmnaRecep', nombre: 'Comuna receptor' },
      { id: 'DTE.Encabezado.Receptor.CiudadRecep', nombre: 'Ciudad receptor' },
      { id: 'DTE.Encabezado.Receptor.DirPostal', nombre: 'Direccion postal receptor' },
      { id: 'DTE.Encabezado.Receptor.CmnaPostal', nombre: 'Comuna postal receptor' },
      { id: 'DTE.Encabezado.Receptor.CiudadPostal', nombre: 'Ciudad postal receptor' },
      { id: 'DTE.Encabezado.Totales.MntNeto', nombre: 'Monto neto' },
      { id: 'DTE.Encabezado.Totales.MntExe', nombre: 'Monto exento' },
      { id: 'DTE.Encabezado.Totales.IVA', nombre: 'Iva' },
      { id: 'DTE.Encabezado.Totales.MntTotal', nombre: 'Monto total' },
      { id: 'DTE.Encabezado.Totales.VlrPagar', nombre: 'Valor a pagar' }
    ];

    $scope.webhook = {
      nombre: '',
      descripcion: '',
      tipoEvento: '',
      activo: '',
      url: '',
      token: '',
      usuario: '',
      password: '',
      formato: 'JSON',
      nombreWebhook: '',
      reintentos: '',
      notificaciones: false,
      adjuntarDte: false,
      adjuntarDnt: false,
      adjuntarPdf: false,
      correos: '',
      flujo: '',
      condiciones: []
    };

    $scope.datos = {
      acciones: []
    };

    $scope.tipo_accion_options = [
      { id: '1', nombre: 'Enviar Correo' },
      { id: '2', nombre: 'Ejecutar Endpoint' },
      { id: '3', nombre: 'Ejecutar Acciones Flujo' }
    ];

    $scope.flujos_options = [
      { id: '', nombre: 'No hacer nada adicional' },
      {
        id: '11f9153d1c87b14b9a1b739115eeaab80cc71',
        nombre: 'RECHAZAR documento comercialmente CON VINCULACION al SII'
      },
      {
        id: '11f9153d1c87b14b9a1b739115eeaab80cc72',
        nombre: 'ACEPTAR documento comercialmente CON VINCULACION al SII'
      },
      {
        id: '11f9153d1c87b14b9a1b739115eeaab80cc73',
        nombre: 'RECHAZAR documento comercialmente SIN VINCULACION al SII'
      },
      {
        id: '11f9153d1c87b14b9a1b739115eeaab80cc74',
        nombre: 'ACEPTAR documento comercialmente SIN VINCULACION al SII'
      },
      {
        id: '11f9153d1c87b14b9a1b739115eeaab80cc71',
        nombre: 'RECHAZAR documento comercialmente CON VINCULACION al SII si viene SIN OC'
      },
      {
        id: '11f9153d1c87b14b9a1b739115eeaab80cc73',
        nombre: 'RECHAZAR documento comercialmente SIN VINCULACION al SII si viene SIN OC'
      }
    ];

    $scope.agregar = function() {
      $scope.datos.cuentas.push({
        banco: $scope.datos.cuentas.banco_insertar,
        tipo_cuenta: $scope.datos.cuentas.tipo_cuenta_insertar,
        nro_cuenta: $scope.datos.cuentas.nro_cuenta_insertar,
        mail_notificacion: $scope.datos.cuentas.mail_notificacion_insertar
      });

      var body = {
        banco: $scope.datos.cuentas.banco_insertar,
        tipoCuenta: $scope.datos.cuentas.tipo_cuenta_insertar,
        nroCuenta: $scope.datos.cuentas.nro_cuenta_insertar,
        mailNotificacion: $scope.datos.cuentas.mail_notificacion_insertar
      };

      $scope.datos.cuentas.banco_insertar = '';
      $scope.datos.cuentas.tipo_cuenta_insertar = '';
      $scope.datos.cuentas.nro_cuenta_insertar = '';
      $scope.datos.cuentas.mail_notificacion_insertar = '';
    };

    $scope.condicion_seleccionada = undefined;

    $scope.condSelec = function(id) {
      $scope.condicion_seleccionada = id;
      console.log(id);
    };

    $scope.agregarCondicion = function() {
      if (!$scope.validarCrearCondicion()) return;
      $scope.webhook.condiciones.push({
        tag: $scope.webhook.condiciones.tag_insertar,
        tipo_condicion: $scope.webhook.condiciones.tipo_condicion_insertar,
        valor: $scope.replaceAll($scope.webhook.condiciones.valor_insertar, ' ', '')
      });

      $scope.webhook.condiciones.tag_insertar = '';
      $scope.webhook.condiciones.tipo_condicion_insertar = '';
      $scope.webhook.condiciones.valor_insertar = '';
    };

    $scope.eliminarCondicion = function(idx, cuenta) {
      UIkit.modal.confirm(
        'Esta seguro que desea eliminar esta condicion?',
        function() {
          $scope.webhook.condiciones.splice(idx, 1);
        },
        { labels: { Ok: 'Si, eliminar', Cancel: 'No, me acabo de arrepentir' } }
      );
    };

    $scope.validarCrearCondicion = function() {
      var errores = '';

      console.log($scope.webhook.condiciones);
      console.log($scope.webhook.condiciones.length);
      console.log($scope.webhook.condiciones.tag_insertar);

      if (
        $scope.webhook.condiciones.tag_insertar == '' ||
        $scope.webhook.condiciones.tag_insertar == undefined
      ) {
        errores += '<br>Debe seleccionar una opción en el campo Dato.';
      }

      if (
        $scope.webhook.condiciones.tipo_condicion_insertar == '' ||
        $scope.webhook.condiciones.tipo_condicion_insertar == undefined
      ) {
        errores += '<br>Debe seleccionar una condición.';
      }

      if (
        $scope.webhook.condiciones.valor_insertar == '' ||
        $scope.webhook.condiciones.valor_insertar == undefined
      ) {
        errores += '<br>Debe ingresar un valor.';
      }

      if (errores != '') {
        UIkit.modal.alert('Errores encontrados:' + errores);
        return false;
      } else {
        return true;
      }
    };

    $scope.validarCrear = function() {
      var errores = '';

      if ($scope.webhook.nombre == '') {
        errores += '<br>Debe ingresar un Nombre obligatoriamente.';
      }

      if ($scope.webhook.tipoEvento == '') {
        errores += '<br>Debe seleccionar tipo de evento.';
      }

      if ($scope.webhook.condiciones == null || $scope.webhook.condiciones.length == 0) {
        errores += '<br>Debe al menos ingresar una condición.';
      }

      if (
        $scope.webhook.flujo == '' &&
        $scope.webhook.usuario == '' &&
        $scope.webhook.password == '' &&
        $scope.webhook.token == '' &&
        $scope.webhook.correos == ''
      ) {
        errores += '<br>Debe ingresar una acción.';
      }

      if (errores != '') {
        UIkit.modal.alert('Errores encontrados:' + errores);
        return false;
      } else {
        return true;
      }
    };

    $scope.guardarWebhook = function() {
      if (!$scope.validarCrear()) return;
      var url = null;
      var usuario = null;
      var password = null;
      var token = null;
      var flujo = null;
      var correos = null;
      var notificarCorreo = 'no';
      var strCondiciones = '';

      url = $scope.webhook.url;
      usuario = $scope.webhook.usuario;
      password = $scope.webhook.password;
      token = $scope.webhook.token;

      flujo = $scope.webhook.flujo;

      if ($scope.webhook.correos != null && $scope.webhook.correos != '') {
        notificarCorreo = 'si';
      }

      //convertir condiciones de lenguaje humano a javascript
      try {
        strCondiciones = $scope.humano2javascript($scope.webhook.condiciones);
      } catch (e) {
        alert(e.message);
      }

      var body = {
        nombre: $scope.webhook.nombre,
        tipoEvento: $scope.webhook.tipoEvento,
        url: url,
        formato: 'XML',
        tokenWebhook: token,
        usuario: usuario,
        clave: password,
        flujo: flujo,
        activo: $scope.webhook.activo == true ? 'si' : 'no',
        reintentos: 3,
        dte: 'si',
        dnt: 'no',
        pdf: 'si',
        correos: $scope.webhook.correos,
        notificarPorCorreo: notificarCorreo,
        notificarPorInterno: 'no',
        notificarPorTelegram: 'no'
      };
      //console.log(body);
      $scope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Guardando Datos...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      if ($scope.esEditar) {
        FebosAPI.cl_modificar_webhook(
          {
            webhookId: $scope.notificacion.webhookId,
            simular: 'no'
          },
          body,
          true,
          false
        ).then(function(response) {
          console.log(response);
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            console.log('Modificado');

            console.log(strCondiciones);
            var bodyRegla = {
              reglaId: $scope.reglaId,
              continuar: 'si',
              activa: $scope.webhook.activo == true ? 'si' : 'no',
              prioridad: '1',
              condicion: FebosUtil.encode(strCondiciones),
              nombre: $scope.webhook.nombre,
              descripcion: $scope.webhook.descripcion
            };
            console.log(bodyRegla);
            FebosAPI.cl_modificar_regla_webhook(
              {
                webhookId: $scope.notificacion.webhookId,
                simular: 'no'
              },
              bodyRegla,
              true,
              false
            ).then(function(response) {
              console.log(response);
              $scope.blockModal.hide();
              if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                UIkit.modal.alert('Muy bien! Notificacion modificada exitosamente!', {
                  labels: { Ok: 'Listo!' }
                });
                console.log('OK Modificado');
              }
            });
          }
        });
      } else {
        FebosAPI.cl_crear_webhook({ simular: 'no' }, body, true, false).then(function(response) {
          console.log(response);
          if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
            console.log('OK CREADO');

            console.log(strCondiciones);
            var bodyRegla = {
              id: response.data.webhookId,
              continuar: 'si',
              activa: $scope.webhook.activo == true ? 'si' : 'no',
              prioridad: '1',
              condicion: FebosUtil.encode(strCondiciones),
              nombre: $scope.webhook.nombre,
              descripcion: $scope.webhook.descripcion
            };

            FebosAPI.cl_crear_regla_webhook(
              {
                webhookId: response.data.webhookId,
                empresa: SesionFebos().empresa.id,
                simular: 'no'
              },
              bodyRegla,
              true,
              false
            ).then(function(response) {
              console.log(response);
              $scope.blockModal.hide();
              if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
                console.log('OK CREADO');
                UIkit.modal.alert('Muy bien! Notificacion creada exitosamente!', {
                  labels: { Ok: 'Listo!' }
                });
              }
            });
          }
        });
      }
    };

    console.log('notificacion', $scope.notificacion);

    if ($scope.notificacion != null) {
      console.log('cargando datos en pantalla...');
      $scope.blockModal = UIkit.modal.blockUI(
        "<div class='uk-text-center'>Cargando Datos...<br/><img width=\"30\"class='uk-margin-top' src='assets/img/logos/FEBOS_LOADER.svg' alt=''>"
      );
      $scope.esEditar = true;

      FebosAPI.cl_listar_webhooks_reglas(
        {
          webhookId: $scope.notificacion.webhookId,
          simular: 'no'
        },
        null,
        true,
        false
      ).then(function(response) {
        console.log(response);
        $scope.blockModal.hide();
        if (response.data.codigo === 10 || typeof response.data.codigo === '10') {
          $scope.reglaId = response.data.webhookReglas[0].reglaId;
          $scope.webhook.tipoEvento = $scope.notificacion.tipoEvento;
          $scope.webhook.nombre = response.data.webhookReglas[0].nombre;
          $scope.webhook.descripcion = response.data.webhookReglas[0].descripcion;
          $scope.webhook.activo = $scope.notificacion.activo;
          $scope.webhook.correos = $scope.notificacion.correosNotificacion;
          $scope.webhook.usuario = $scope.notificacion.httpBasicUser;
          $scope.webhook.password = $scope.notificacion.httpBasicPassword;
          $scope.webhook.token = $scope.notificacion.token;
          $scope.webhook.url = $scope.notificacion.url;
          $scope.webhook.flujo = $scope.notificacion.flujoId;

          console.log(FebosUtil.decode(response.data.webhookReglas[0].condicion));
          $scope.webhook.condiciones = $scope.javascript2humano(
            FebosUtil.decode(response.data.webhookReglas[0].condicion)
          );
          console.log($scope.webhook.condiciones);
          // FebosUtil.decode(strCondiciones)
          console.log('regla obtenida', $scope.reglaId);
        }
      });
    }
  });
