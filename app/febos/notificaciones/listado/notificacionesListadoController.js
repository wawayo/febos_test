angular
  .module('febosApp')
  .controller('notificacionListadoCtrl', function(
    $rootScope,
    $scope,
    $interval,
    $timeout,
    $location,
    $http,
    $state,
    utils,
    FebosAPI,
    SesionFebos,
    $stateParams,
    FebosUtil
  ) {
    $scope.app = $stateParams.app;

    $scope.notificaciones = [];

    $scope.tipoEvento = function(a, b, c) {
      return FebosUtil.tipoEvento(a, b, c);
    };

    $scope.cargando = true;
    FebosAPI.cl_listar_webhooks({}, {}, true, false).then(function(response) {
      $scope.cargando = false;
      $scope.notificaciones = response.data.webhooks;
    });

    $scope.editar = function(notificacion) {
      $state.go('restringido.gestion_notificaciones', {
        app: $stateParams.app,
        notificacion: notificacion
      });
      // $state.reload();
    };

    $scope.eliminar = function(webhook, index) {
      UIkit.modal.confirm(
        'Esta seguro que desea eliminar la notificacion <b>' +
          webhook.nombre +
          '</b> y sus reglas?',
        function() {
          modal = UIkit.modal.blockUI(
            "<div class='uk-text-center'>Eliminando notificacion...<br/><img class='uk-margin-top' src='assets/img/spinners/spinner.gif' alt=''>"
          );

          FebosAPI.cl_eliminar_webhook(
            {
              webhookId: webhook.webhookId
            },
            {},
            true,
            false
          ).then(function success(response) {
            modal.hide();

            if (response.data.codigo == 10) {
              $scope.notificaciones.splice(index, 1);

              UIkit.modal.alert('Notificacion eliminada', { labels: { Ok: 'Listo!' } });
            } else {
              console.log(response);
              SesionFebos().error(
                response.data.codigo,
                response.data.mensaje,
                response.data.seguimientoId,
                response.data.errores
              );
            }
          });
        },
        { labels: { Ok: 'Si, eliminar', Cancel: 'No, me acabo de arrepentir' } }
      );
    };
  });
