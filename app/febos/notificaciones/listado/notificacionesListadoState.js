febosApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('restringido.notificaciones', {
        url: '/:app/notificaciones/listado',
        templateUrl: 'app/febos/notificaciones/listado/notificacionesListadoView.html',
        controller: 'notificacionListadoCtrl',
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/notificaciones/listado/notificacionesListadoController.js'
              ]);
            }
          ]
        }
      })
      .state('restringido.gestion_notificaciones', {
        url: '/:app/notificaciones/gestion/',
        params: { notificacion: null },
        templateUrl: 'app/febos/notificaciones/gestion/notificacionesGestionView.html',
        controller: 'notificacionesGestionCtrl',
        resolve: {
          deps: [
            '$ocLazyLoad',
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                'lazy_uikit',
                'lazy_iCheck',
                'app/febos/notificaciones/gestion/notificacionesGestionController.js'
              ]);
            }
          ]
        }
      });
  }
]);
