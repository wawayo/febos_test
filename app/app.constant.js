febosApp.constant('MONEDAS', [
  { id: 'CLP', title: 'Pesos', simbolo: '$', sufijo: '' },
  { id: 'UF', title: 'UF', simbolo: ' ', sufijo: '' },
  { id: 'UTM', title: 'UTM', simbolo: ' ', sufijo: '' },
  { id: 'EUR', title: 'Euro', simbolo: '€', sufijo: '' },
  { id: 'GBP', title: 'Libra esterlina', simbolo: '£', sufijo: '' },
  { id: 'USD', title: 'Dolar', simbolo: 'US$', sufijo: '' }
]);
febosApp.constant('TIPOSGARANTIA', [
  { id: 'poliza', title: 'Poliza' },
  { id: 'boleta', title: 'Boleta' },
  { id: 'pagares', title: 'Pagares' },
  { id: 'swift', title: 'Swift' },
  { id: 'certificado_de_finanza', title: 'Certificado De Fianza' },
  { id: 'cheque_internacional', title: 'Cheque Internacional' },
  { id: 'otro', title: 'Otro Tipo De Garantia' }
]);
