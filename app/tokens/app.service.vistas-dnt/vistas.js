{
    cloud: {
      recibidos: {
        ordenesDeCompra: {
          titulo: 'Ordenes de compra recibidas',
          documentosDisponibles: [801],
          filtrosPorDefecto: [
            //{ campo: 'tipoDocumento', valor: [33, 34, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [{ campo: 'tipo', valor: '801' }]
        },
        HEM: {
          titulo: 'Hojas de entrega de materiales recibidas',
          documentosDisponibles: ['HEM'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HEM' }]
        },
        HES: {
          titulo: 'Hojas de entrega de Servicios recibidas',
          documentosDisponibles: ['HES'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HES' }]
        },
        mercadoPublico: {
          titulo: 'Documentos de Mercado Público recibidos',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        mercadoPublicoMias: {
          titulo: 'Documentos de Mercado Público recibidos por mi',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        referencia: {
          titulo: 'DNT recibidos',
          documentosDisponibles: [801, 'HEM', 'HES', 'MP'],
          filtrosPorDefecto: [],
          filtrosFijos: []
        }
      },
      emitidos: {
        //dnt/cloud/emitidos: HEM, HES, mercadoPublico, mercadoPublicoMias, ordenesDeCompra
        ordenesDeCompra: {
          titulo: 'Ordenes de compra emitidas',
          documentosDisponibles: [801],
          filtrosPorDefecto: [
            //{ campo: 'tipoDocumento', valor: [33, 34, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [{ campo: 'tipo', valor: '801' }]
        },
        HEM: {
          titulo: 'Hojas de entrega de materiales emitidas',
          documentosDisponibles: ['HEM'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HEM' }]
        },
        HES: {
          titulo: 'Hojas de entrega de Servicios emitidas',
          documentosDisponibles: ['HES'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HES' }]
        },
        mercadoPublico: {
          titulo: 'Documentos de Mercado Público emitidos',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        mercadoPublicoMias: {
          titulo: 'Documentos de Mercado Público emitidos por mi',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        referencia: {
          titulo: 'DNT emitidos',
          documentosDisponibles: [801, 'HEM', 'HES', 'MP'],
          filtrosPorDefecto: [],
          filtrosFijos: []
        }
      }
    },
    proveedores: {
      emitidos: {
        ordenesDeCompra: {
          titulo: 'Ordenes de compra emitidas',
          documentosDisponibles: [801],
          filtrosPorDefecto: [
            //{ campo: 'tipoDocumento', valor: [33, 34, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [{ campo: 'tipo', valor: '801' }]
        },
        HEM: {
          titulo: 'Hojas de entrega de materiales emitidas',
          documentosDisponibles: ['HEM'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HEM' }]
        },
        HES: {
          titulo: 'Hojas de entrega de Servicios emitidas',
          documentosDisponibles: ['HES'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HES' }]
        },
        mercadoPublico: {
          titulo: 'Documentos de Mercado Público emitidos',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        mercadoPublicoMias: {
          titulo: 'Documentos de Mercado Público emitidos por mi',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        referencia: {
          titulo: 'DNT emitidos',
          documentosDisponibles: [801, 'HEM', 'HES', 'MP'],
          filtrosPorDefecto: [],
          filtrosFijos: []
        }
      },
      recibidos: {
        //dnt/proveedores/recibidos: HEM, HES, mercadoPublico, ordenesDeCompra
        ordenesDeCompra: {
          titulo: 'Ordenes de compra recibidas',
          documentosDisponibles: [801],
          filtrosPorDefecto: [
            //{ campo: 'tipoDocumento', valor: [33, 34, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [{ campo: 'tipo', valor: '801' }]
        },
        HEM: {
          titulo: 'Hojas de entrega de materiales recibidas',
          documentosDisponibles: ['HEM'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HEM' }]
        },
        HES: {
          titulo: 'Hojas de entrega de Servicios recibidas',
          documentosDisponibles: ['HES'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HES' }]
        },
        mercadoPublico: {
          titulo: 'Documentos de Mercado Público recibidas',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        referencia: {
          titulo: 'DNT recibidos',
          documentosDisponibles: [801, 'HEM', 'HES', 'MP'],
          filtrosPorDefecto: [],
          filtrosFijos: []
        }
      }
    },
    cliente: {
      recibidos: {
        todos: {
          titulo: 'DTE recibidos',
          documentosDisponibles: [33, 34, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        }
      },
      emitidos: {
        //dnt/cliente/emitidos: HEM, HES, mercadoPublico, ordenesDeCompra
        ordenesDeCompra: {
          titulo: 'Ordenes de compra recibidas',
          documentosDisponibles: [801],
          filtrosPorDefecto: [
            //{ campo: 'tipoDocumento', valor: [33, 34, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [{ campo: 'tipo', valor: '801' }]
        },
        HEM: {
          titulo: 'Hojas de entrega de materiales recibidas',
          documentosDisponibles: ['HEM'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HEM' }]
        },
        HES: {
          titulo: 'Hojas de entrega de Servicios recibidas',
          documentosDisponibles: ['HES'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'HES' }]
        },
        mercadoPublico: {
          titulo: 'Documentos de Mercado Público recibidas',
          documentosDisponibles: ['MP'],
          filtrosPorDefecto: [],
          filtrosFijos: [{ campo: 'tipo', valor: 'MP' }]
        },
        referencia: {
          titulo: 'DNT emitidos',
          documentosDisponibles: [801, 'HEM', 'HES', 'MP'],
          filtrosPorDefecto: [],
          filtrosFijos: []
        }
      }
    }
  }