{
    cloud: {
      recibidos: {
        //dte/cloud/recibidos: cesionados, guias, honorarios, manuales, misRecibidos, misRecibidosUnidad,
        //  noRecibidos, pagados, pendientes, preRechazados, rechazados, todos
        cedidos: {
          titulo: 'DTE recibidos que se han cedido',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'rutCesionario', valor: '-' }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        }
      },
      recibidos: {
        //dte/cloud/recibidos: cesionados, guias, honorarios, manuales, misRecibidos, misRecibidosUnidad,
        //  noRecibidos, pagados, pendientes, preRechazados, rechazados, todos
        cedidos: {
          titulo: 'DTE recibidos que se han cedido',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'rutCesionario', valor: '-' }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        guias: {
          titulo: 'Guías de despacho recibidas',
          documentosDisponibles: [52],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [52] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        boletas: {
          titulo: 'Boletas recibidas',
          documentosDisponibles: [39, 41],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [39, 41] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        honorarios: {
          titulo: 'Boletas de honorarios recibidas',
          documentosDisponibles: [66],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [66] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5,6' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        manuales: {
          titulo: 'DTE manuales',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        misRecibidos: {
          titulo: 'Mis DTE recibidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            {
              campo: 'correoReceptor',
              valor: ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.correo
            }
          ]
        },
        misRecibidosUnidad: {
          titulo: 'Mis DTE recibidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            {
              campo: 'correoReceptor',
              valor: ContextoFebos.sesiones[ContextoFebos.usuarioActual].usuario.correo
            }
          ]
        },
        noRecibidos: {
          titulo: 'DTE no recibidos aún',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'Y' }
          ]
        },
        pagados: {
          titulo: 'DTE recibidos en proceso de pago',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'tipo', valor: [1, 2, 3, 4, 5] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        pendientes: {
          titulo: 'DTE recibidos pendientes de validación en el SII',
          documentosDisponibles: [33, 34, 56, 43, 46, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '7' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        preRechazados: {
          titulo: 'DTE recibidos pre-rechazados',
          documentosDisponibles: [33, 34, 43, 46, 56, 61],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            { campo: 'estadoComercial', valor: [3] }
          ]
        },
        reclamados: {
          titulo: 'DTE recibidos reclamados',
          documentosDisponibles: [33, 34, 43, 46, 56, 61],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            { campo: 'estadoComercial', valor: [4, 5, 6] }
          ]
        },
        sinRecepcionSII: {
          titulo: 'DTE recibidos sin Fecha Recepcion',
          documentosDisponibles: [33, 34, 43, 46, 56, 61],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61] }],
          filtrosFijos: [
            { campo: 'fechaRecepcionSii', valor: 'NULL' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        todos: {
          titulo: 'DTE recibidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        referencia: {
          titulo: 'DTE recibidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: []
        }
      },
      emitidos: {
        //dte/cloud/emitidos: cedidos, guias, misEmitidos, misEmitidosUnidad, noEntregados, todos
        cesionados: {
          titulo: 'DTE emitidos que se han cesionado',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'rutCesionario', valor: '-' }
          ],
          filtrosFijos: [{ campo: 'estadoSii', valor: '2,4,5' }]
        },
        guias: {
          titulo: 'Guías de despacho emitidas',
          documentosDisponibles: [52],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [52] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '2,4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        boletas: {
          titulo: 'Boletas emitidas',
          documentosDisponibles: [39, 41],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [39, 41] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        envioFolios: {
          titulo: 'Envío Consumo Folios',
          documentosDisponibles: [39, 41],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [39, 41] },
            { campo: 'estadoSii', valor: '1,2,3,4,5,6,7,8' }
          ],
          filtrosFijos: [{ campo: 'incompleto', valor: 'N' }]
        },
        prefacturas: {
          titulo: 'Guías de despacho emitidas',
          documentosDisponibles: [52],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [-33, -34, -52, -56, -61] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '0' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        misEmitidos: {
          titulo: 'Mis DTE Emitidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '1,2,4,5,6,7' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        misEmitidosUnidad: {
          titulo: 'Mis DTE Emitidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '2,4,5,6,7' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        noEntregados: {
          titulo: 'DTE emitidos que no se han entregado al receptor',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '2,4,5' },
            { campo: 'incompleto', valor: 'Y' }
          ]
        },
        indicadores: {
          titulo: 'DTE emitidos',
          documentosDisponibles: [33, 34, 39, 41, 56, 61, 110, 111, 112, 801],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 39, 41, 56, 61, 110, 111, 112, 801] }
          ],
          filtrosFijos: [{ campo: 'incompleto', valor: 'N' }]
        },
        todos: {
          titulo: 'DTE emitidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '1,2,4,5,6,7' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        borradores: {
          titulo: 'DTE Borradores',
          documentosDisponibles: [
            -33,
            -34,
            -39,
            -42,
            -43,
            -46,
            -52,
            -56,
            -61,
            -110,
            -111,
            -112
          ],
          filtrosPorDefecto: [
            {
              campo: 'tipoDocumento',
              valor: [-33, -34, -39, -42, -43, -46, -52, -56, -61, -110, -111, -112]
            }
          ],
          filtrosFijos: [
            /*{ campo: 'estadoSii', valor: '2,4,5,6,7' },*/
            /*{ campo: 'incompleto', valor: 'N' }*/
          ]
        },
        referencia: {
          titulo: 'DTE emitidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: []
        }
      }
    },
    proveedores: {
      emitidos: {
        //dte/proveedores/emitidos: cesionados, honorarios, noEntregados, pagados, preRechazados, rechazados, todos
        cesionados: {
          titulo: 'DTE emitidos que se han cesionado',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'rutCesionario', valor: '-' }
          ],
          filtrosFijos: [{ campo: 'estadoSii', valor: '4,5' }]
        },
        honorarios: {
          titulo: 'Boletas de honorarios recibidas',
          documentosDisponibles: [66],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [66] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5,6' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        noEntregados: {
          titulo: 'DTE emitidos que no se han entregado al receptor',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'Y' }
          ]
        },
        pagados: {
          titulo: 'DTE recibidos en proceso de pago',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'tipo', valor: [1, 2, 3, 4, 5] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        preRechazados: {
          titulo: 'DTE recibidos pre-rechazados',
          documentosDisponibles: [33, 34, 43, 46, 56, 61],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            { campo: 'estadoComercial', valor: [3] }
          ]
        },
        rechazados: {
          titulo: 'DTE emitidos rechazados',
          documentosDisponibles: [33, 34, 43, 46, 56, 61],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            { campo: 'estadoComercial', valor: [4, 5, 6] }
          ]
        },
        guias: {
          titulo: 'Guías de despacho emitidas',
              documentosDisponibles: [52],
              filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [52] }],
              filtrosFijos: [
            { campo: 'estadoSii', valor: '2,4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        todos: {
          titulo: 'DTE emitidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        referencia: {
          titulo: 'DTE emitidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: []
        }
      }
    },
    cliente: {
      recibidos: {
        //dte/cliente/recibidos: cesionados, pagados, preRechazados, rechazados, todos
        cesionados: {
          titulo: 'DTE emitidos que se han cesionado',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'rutCesionario', valor: '-' }
          ],
          filtrosFijos: [{ campo: 'estadoSii', valor: '4,5' }]
        },
        pagados: {
          titulo: 'DTE recibidos en proceso de pago',
          documentosDisponibles: [33, 34, 43, 46],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46] },
            { campo: 'tipo', valor: [1, 2, 3, 4, 5] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        preRechazados: {
          titulo: 'DTE recibidos pre-rechazados',
          documentosDisponibles: [33, 34, 43, 46, 56, 61],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            { campo: 'estadoComercial', valor: [3] }
          ]
        },
        rechazados: {
          titulo: 'DTE recibidos rechazados',
          documentosDisponibles: [33, 34, 43, 46, 56, 61],
          filtrosPorDefecto: [{ campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61] }],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' },
            { campo: 'estadoComercial', valor: [4, 5, 6] }
          ]
        },
        todos: {
          titulo: 'DTE recibidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: [
            { campo: 'estadoSii', valor: '4,5' },
            { campo: 'incompleto', valor: 'N' }
          ]
        },
        referencia: {
          titulo: 'DTE recibidos',
          documentosDisponibles: [33, 34, 43, 46, 56, 61, 110, 111, 112],
          filtrosPorDefecto: [
            { campo: 'tipoDocumento', valor: [33, 34, 43, 46, 56, 61, 110, 111, 112] }
          ],
          filtrosFijos: []
        }
      }
    }
  }