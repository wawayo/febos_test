[
  { nombre: 'cl_listar_dte', verbo: 'GET', url: "'/documentos'" },
  { nombre: 'cl_login', verbo: 'POST', url: "'/usuarios/login'" },
  { nombre: 'cl_latido_usuario', verbo: 'HEAD', url: "'/usuarios/0'" },
  {
    nombre: 'cl_listar_permiso_usuario',
    verbo: 'GET',
    url: "'/permisos/' + SesionFebos().usuario.iut"
  },
  { nombre: 'cl_listar_empresas', verbo: 'GET', url: "'/empresas'" },
  { nombre: 'cl_generar_menu', verbo: 'GET', url: "'/menu'" },
  {
    nombre: 'cl_historial_eventos',
    verbo: 'GET',
    url: "'/logs/' + query.seguimientoId"
  },
  {
    nombre: 'cl_listar_flujos',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/flujos'"
  },
  {
    nombre: 'cl_anular_folios',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/caf/legacy'"
  },
  {
    nombre: 'cl_actualizar_empresa',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId"
  },
  {
    nombre: 'cl_agregar_aat',
    verbo: 'POST',
    url: "'/herramientas/automatizaciones'"
  },
  {
    nombre: 'cl_listar_aat',
    verbo: 'GET',
    url: "'/herramientas/automatizaciones'"
  },
  {
    nombre: 'cl_actualizar_aat',
    verbo: 'PUT',
    url: "'/herramientas/automatizaciones/' + query.accionAutomaticaId"
  },
  {
    nombre: 'cl_borrar_aat',
    verbo: 'DELETE',
    url: "'/herramientas/automatizaciones/' + query.accionAutomaticaId"
  },
  {
    nombre: 'cl_actualizar_token_permanente',
    verbo: 'POST',
    url: "'/usuarios/' + query.usuarioId + '/token'"
  },
  {
    nombre: 'cl_actualizar_usuario_proveedor',
    verbo: 'POST',
    url: "'/usuarios/' + query.usuarioId"
  },
  { nombre: 'cl_actualizar_usuarios_grupo', verbo: 'GET', url: '' },
  {
    nombre: 'cl_actualizar_usuario_proveedor',
    verbo: 'POST',
    url: "'/usuarios/' + query.usuarioId"
  },
  {
    nombre: 'cl_actualizar_usuarios_grupo',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/grupos/' + query.grupoId"
  },
  {
    nombre: 'cl_adjuntar_documento',
    verbo: 'PUT',
    url: "'/documentos/' + query.febosId"
  },
  {
    nombre: 'cl_agregar_path_lectura_flujo',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId + '/configuracion'"
  },
  {
    nombre: 'cl_asignar_empresa_usuario',
    verbo: 'POST',
    url: "'/usuarios/' + query.usuarioId + '/empresas'"
  },
  { nombre: 'cl_asignar_permisos', verbo: 'PUT', url: "'/permisos'" },
  {
    nombre: 'cl_asociar_contacto',
    verbo: 'POST',
    url: "'/documentos/' + query.febosId + '/contacto'"
  },
  {
    nombre: 'cl_buscar_proveedores',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/proveedores'"
  },
  {
    nombre: 'cl_cambiar_estado_flujo',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId + '/configuracion'"
  },
  { nombre: 'cl_cargar_dnt', verbo: 'PUT', url: "'/notributarios'" },
  {
    nombre: 'cl_crear_dte_desde_borrador',
    verbo: 'POST',
    url: "'/documentos/borrador/emitir'"
  },{
    nombre: 'cl_crear_dte',
    verbo: 'POST',
    url: "'/documentos'"
  },
  {
    nombre: 'cl_cargar_documento_electronico',
    verbo: 'PUT',
    url: "'/documentos'"
  },
  {
    nombre: 'cl_cargar_documento_manual',
    verbo: 'PUT',
    url: "'/documentos/manuales'"
  },
  {
    nombre: 'cl_cargar_caf',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/caf/legacy'"
  },
  {
    nombre: 'cl_cargar_caf_store',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/caf/legacy/cafstore'"
  },
  { nombre: 'cl_cargar_oc_mp', verbo: 'PUT', url: "'/mercadopublico'" },
  {
    nombre: 'cl_comentarios_bitacoras',
    verbo: 'PUT',
    url: "'/documentos/' + query.id + '/bitacora'"
  },
  {
    nombre: 'cl_configurar_portal_personalizado',
    verbo: 'POST',
    url: "'/configuracion/portales/{dominio}'"
  },
  {
    nombre: 'cl_consulta_estado_tarea_background',
    verbo: 'GET',
    url: "'/herramientas/tareas/' + query.tareaId"
  },
  {
    nombre: 'cl_consulta_fecha_recepcion_sii',
    verbo: 'GET',
    url: "'/sii/dte/consulta/recepcion'"
  },
  { nombre: 'cl_consumo_de_colas_masivo', verbo: 'GET', url: "''" },
  {
    nombre: 'cl_correo_envio_dte',
    verbo: 'POST',
    url: "'/documentos/' + query.febosId + '/envio'"
  },
  {
    nombre: 'cl_crea_info_pago',
    verbo: 'POST',
    url: "'/documentos/' + query.febosId + '/pagos'"
  },
  {
    nombre: 'cl_crear_cuenta',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/cuentas'"
  },
  { nombre: 'cl_crear_empresa', verbo: 'POST', url: "'/empresas'" },
  {
    nombre: 'cl_crear_grupo',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/grupos'"
  },
  {
    nombre: 'cl_crear_lista',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/listas'"
  },
  { nombre: 'cl_crear_menu', verbo: 'PUT', url: "'/menu'" },
  {
    nombre: 'cl_crear_proyecto',
    verbo: 'POST',
    url: "'/portafolio/proyectos'"
  },
  {
    nombre: 'cl_crear_regla_webhook',
    verbo: 'POST',
    url: "'/webhook/' + query.webhookId + '/reglas'"
  },
  {
    nombre: 'cl_crear_rol',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/roles'"
  },
  {
    nombre: 'cl_crear_sucursal',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/sucursales'"
  },
  { nombre: 'cl_crear_ticket_ayuda', verbo: 'POST', url: "'/tickets'" },
  {
    nombre: 'cl_crear_token_permanente',
    verbo: 'PUT',
    url: "'/usuarios/' + query.usuarioId + '/token'"
  },
  { nombre: 'cl_crear_usuario', verbo: 'PUT', url: "'/usuarios'" },
  { nombre: 'cl_crear_webhook', verbo: 'POST', url: "'/webhook'" },
  {
    nombre: 'cl_editar_novedad',
    verbo: 'PUT',
    url: "'/novedades/' + query.novedadId"
  },
  { nombre: 'cl_ejecuta_lambda_remoto', verbo: 'GET', url: "''" },
  {
    nombre: 'cl_ejecutar_flujo',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId"
  },
  {
    nombre: 'cl_elimina_tokens',
    verbo: 'DELETE',
    url: "'/usuarios/' + query.usuarioId + '/token'"
  },
  {
    nombre: 'cl_eliminar_cuenta',
    verbo: 'DELETE',
    url: "'/empresas/' + query.empresaId + '/cuentas'"
  },
  {
    nombre: 'cl_eliminar_flujo',
    verbo: 'DELETE',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId"
  },
  {
    nombre: 'cl_eliminar_grupo',
    verbo: 'DELETE',
    url: "'/empresas/' + query.empresaId + '/grupos/' + query.grupoId"
  },
  {
    nombre: 'cl_eliminar_lista',
    verbo: 'DELETE',
    url: "'/empresas/' + query.empresaId + '/listas/' + query.listaId"
  },
  { nombre: 'cl_eliminar_menu', verbo: 'DELETE', url: "'/menu/{menuid}'" },
  {
    nombre: 'cl_eliminar_path_lectura_flujo',
    verbo: 'DELETE',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId + '/configuracion'"
  },
  {
    nombre: 'cl_eliminar_rol',
    verbo: 'DELETE',
    url: "'/empresas/' + query.empresaId + '/roles/'+query.rolId"
  },
  {
    nombre: 'cl_eliminar_sucursal',
    verbo: 'DELETE',
    url: "'/empresas/' + query.empresaId + '/sucursales/' + query.sucursalId"
  },
  { nombre: 'cl_eliminar_tokens_caducados', verbo: 'DELETE', url: "''" },
  {
    nombre: 'cl_eliminar_usuario',
    verbo: 'DELETE',
    url: "'/usuarios/' + query.usuarioId"
  },
  {
    nombre: 'cl_eliminar_webhook',
    verbo: 'DELETE',
    url: "'/webhook/' + query.webhookId"
  },
  {
    nombre: 'cl_eliminar_webhooks_regla',
    verbo: 'DELETE',
    url: "'/webhook/' + query.webhookId + '/reglas'"
  },
  {
    nombre: 'cl_encola_acciones_masivas_unitarias',
    verbo: 'GET',
    url: "''"
  },
  {
    nombre: 'cl_encolar_tarea_background',
    verbo: 'POST',
    url: "'/herramientas/tareas'"
  },
  {
    nombre: 'cl_estadisticas',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/estadisticas'"
  },
  {
    nombre: 'cl_generador_reportes_dte',
    verbo: 'GET',
    url: "'/documentos/reportes'"
  },
  {
    nombre: 'cl_generar_archivo_cesion',
    verbo: 'POST',
    url: "'/sii/dte/cesion'"
  },
  {
    nombre: 'cl_reenviar_al_sii',
    verbo: 'GET',
    url: "'/sii/dte/reenviodocumento'"
  },
  {
    nombre: 'cl_recuperar_correo_sii',
    verbo: 'GET',
    url: "'/sii/dte/reenviocorreo'"
  },
  {
    nombre: 'cl_actualizar_compra_venta',
    verbo: 'PUT',
    url: "'/documentos/datos/transaccioncompraventa'"
  },
  {
    nombre: 'cl_generar_intercambio_electronico',
    verbo: 'PUT',
    url: "'/documentos/' + query.febosId + '/intercambio'"
  },
  {
    nombre: 'cl_guardar_archivo',
    verbo: 'POST',
    url: "'/configuracion/archivos'"
  },
  {
    nombre: 'cl_guardar_flujo',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId"
  },
  {
    nombre: 'cl_info_empresa',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId"
  },
  {
    nombre: 'cl_informacion_plugins_flujo',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/flujos/plugins'"
  },
  {
    nombre: 'cl_invitar_usuario_proveedor',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/proveedores'"
  },
  {
    nombre: 'cl_invocar_flujo',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId + '/ejecuciones'"
  },
  {
    nombre: 'cl_kpi',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/kpi'"
  },
  {
    nombre: 'cl_kpi_emisor',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/kpiemisor'"
  },
  {
    nombre: 'cl_listar_actividad_empresa',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/actividad'"
  },
  {
    nombre: 'cl_listar_actividades',
    verbo: 'GET',
    url: "'/usuarios/' + query.usuarioId + '/actividades'"
  },
  {
    nombre: 'cl_listar_adjuntos',
    verbo: 'GET',
    url: "'/documentos/adjuntos'"
  },
  {
    nombre: 'cl_listar_arbol_usuarios',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/usuarios'"
  },
  {
    nombre: 'cl_listar_bitacora_dnt',
    verbo: 'GET',
    url: "'/notributarios/' + query.febosId + '/bitacora'"
  },
  {
    nombre: 'cl_listar_bitacora_dte',
    verbo: 'GET',
    url: "'/documentos/' + query.febosId + '/bitacora'"
  },
  {
    nombre: 'cl_listar_cafs',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/caf/legacy'"
  },
  {
    nombre: 'cl_listar_cafs_store',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/caf/legacy/cafstore'"
  },
  {
    nombre: 'cl_listar_cafs_store_delivery',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/caf/legacy/cafstore/' + query.cafId"
  },
  {
    nombre: 'cl_listar_comunas',
    verbo: 'GET',
    url: "'/regiones/' + query.regionid + '/provincias/' + query.provinciaid + '/comunas'"
  },
  {
    nombre: 'cl_listar_configuraciones_de_flujo',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId + '/configuracion'"
  },
  {
    nombre: 'cl_listar_cuentas',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/cuentas'"
  },
  { nombre: 'cl_listar_dnt', verbo: 'GET', url: "'/notributarios'" },
  {
    nombre: 'cl_listar_ejecuciones_flujo',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId + '/ejecuciones'"
  },
  {
    nombre: 'cl_listar_elementos_lista',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/listas/' + query.listaId"
  },
  {
    nombre: 'cl_listar_empresas_canales',
    verbo: 'GET',
    url: "'/empresas/canales'"
  },
  {
    nombre: 'cl_listar_empresas_usuario',
    verbo: 'GET',
    url: "'/usuarios/' + query.usuarioId + '/empresas'"
  },
  {
    nombre: 'cl_listar_folios',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/folios/legacy'"
  },
  {
    nombre: 'cl_listar_grupos',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/grupos'"
  },
  {
    nombre: 'cl_listar_info_pago',
    verbo: 'GET',
    url: "'/documentos/pagos'"
  },
  { nombre: 'cl_listar_lambdas', verbo: 'GET', url: "''" },
  {
    nombre: 'cl_listar_listas',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/listas'"
  },
  { nombre: 'cl_listar_novedades', verbo: 'GET', url: "'/novedades'" },
  { nombre: 'cl_listar_oc_mp', verbo: 'GET', url: "'/mercadopublico'" },
  { nombre: 'cl_listar_permisos', verbo: 'GET', url: "'/permisos'" },
  {
    nombre: 'cl_listar_unidades_tecnicas',
    verbo: 'GET',
    url: "'/portafolio/unidades'"
  },
  { nombre: 'cl_crear_unidades_tecnicas', verbo: 'POST', url: "'/portafolio/unidades'" },
  {
    nombre: 'cl_listar_permisos_rol',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/roles/'+ query.rolId + '/permisos'"
  },
  {
    nombre: 'cl_listar_portales_personalizados',
    verbo: 'GET',
    url: "'/configuracion/portales'"
  },
  {
    nombre: 'cl_listar_provincias',
    verbo: 'GET',
    url: "'/regiones/' + query.regionid + '/provincias'"
  },
  {
    nombre: 'cl_listar_proyectos',
    verbo: 'GET',
    url: "'/portafolio/proyectos'"
  },
  {
    nombre: 'cl_listar_proyectos_cabecera',
    verbo: 'GET',
    url: "'/portafolio/proyectos/cabeceras'"
  },
  {
    nombre: 'cl_listar_boleta_garantia',
    verbo: 'GET',
    url: "'/portafolio/boletas'"
  },
  {
    nombre: 'cl_listar_polizas',
    verbo: 'GET',
    url: "'/portafolio/polizas'"
  },
  {
    nombre: 'cl_listar_receptores_electronicos',
    verbo: 'GET',
    url: "'/receptores/receptoreselectronicos'"
  },
  {
    nombre: 'cl_listar_referencias',
    verbo: 'GET',
    url: "'/documentos/' + query.febosId + '/referencias'"
  },
  { nombre: 'cl_listar_regiones', verbo: 'GET', url: "'/regiones'" },
  {
    nombre: 'cl_listar_roles',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/roles'"
  },
  {
    nombre: 'cl_listar_roles_usuario',
    verbo: 'GET',
    url: "'/usuarios/' + query.usuarioId + '/roles'"
  },
  { nombre: 'cl_listar_states', verbo: 'GET', url: "'/menu/estados'" },
  {
    nombre: 'cl_listar_sucursales',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/sucursales'"
  },
  {
    nombre: 'cl_listar_suscripcion_usuario',
    verbo: 'GET',
    url: "'/webhook/suscripciones/usuario'"
  },
  {
    nombre: 'cl_listar_suscripciones_webhook',
    verbo: 'GET',
    url: "'/webhook/' + query.webhookId + '/suscripciones'"
  },
  {
    nombre: 'cl_listar_tokens',
    verbo: 'GET',
    url: "'/usuarios/' + query.usuarioId + '/token'"
  },
  { nombre: 'cl_listar_usuarios', verbo: 'GET', url: "'/usuarios'" },
  {
    nombre: 'cl_listar_bitacora_portafolio',
    verbo: 'GET',
    url: "'/portafolio/boletas/'+query.documentoId+'/bitacora'"
  },
  { nombre: 'cl_listar_webhooks', verbo: 'GET', url: "'/webhook'" },
  {
    nombre: 'cl_listar_webhooks_reglas',
    verbo: 'GET',
    url: "'/webhook/' + query.webhookId + '/reglas'"
  },
  { nombre: 'cl_mailgun2s3', verbo: 'POST', url: "''" },
  {
    nombre: 'cl_modificar_cabecera_lista',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/listas/' + query.listaId"
  },
  {
    nombre: 'cl_modificar_configuracion',
    verbo: 'PUT',
    url: "'/configuracion'"
  },
  {
    nombre: 'cl_modificar_contrasena',
    verbo: 'PUT',
    url: "'/usuarios/login/recuperar'"
  },
  {
    nombre: 'cl_modificar_grupo',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/grupos/' + query.grupoId"
  },
  {
    nombre: 'cl_modificar_info_pago',
    verbo: 'PUT',
    url: "'/documentos/' + query.febosId + '/pagos'"
  },
  {
    nombre: 'cl_modificar_lista',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/listas/' + query.listaId"
  },
  { nombre: 'cl_modificar_menu', verbo: 'POST', url: "'/menu/{menuid}'" },
  {
    nombre: 'cl_modificar_permisos_rol',
    verbo: 'POST',
    url: "'/empresas/' + query.empresaId + '/roles/' + query.rolId + '/permisos'"
  },
  {
    nombre: 'cl_modificar_regla_webhook',
    verbo: 'PUT',
    url: "'/webhook/' + query.webhookId + '/reglas'"
  },
  {
    nombre: 'cl_modificar_rol',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/roles/'+ query.rolId"
  },
  {
    nombre: 'cl_modificar_roles_usuario',
    verbo: 'POST',
    url: "'/usuarios/' + query.usuarioId + '/roles'"
  },
  {
    nombre: 'cl_modificar_sucursal',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/sucursales/' + query.sucursalId"
  },
  {
    nombre: 'cl_modificar_suscripcion_webhook',
    verbo: 'POST',
    url: "'/webhook/' + query.webhookId + '/suscripciones'"
  },
  {
    nombre: 'cl_modificar_usuario',
    verbo: 'PUT',
    url: "'/usuarios/' + query.usuarioId"
  },
  {
    nombre: 'cl_modificar_webhook',
    verbo: 'POST',
    url: "'/webhook/' + query.webhookId"
  },
  { nombre: 'cl_nueva_novedad', verbo: 'POST', url: "'/novedades'" },
  {
    nombre: 'cl_obtener_adjunto',
    verbo: 'GET',
    url: "'/documentos/adjunto'"
  },
  {
    nombre: 'cl_obtener_adjunto_bitacora',
    verbo: 'GET',
    url: "'/documentos/'+ query.documentoId +'/bitacora/adjunto'"
  },
  {
    nombre: 'cl_obtener_archivo',
    verbo: 'GET',
    url: "'/configuracion/archivos'"
  },
  {
    nombre: 'cl_obtener_certificado',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/certificado'"
  },
  {
    nombre: 'cl_obtener_configuracion',
    verbo: 'GET',
    url: "'/configuracion'"
  },
  {
    nombre: 'cl_obtener_dnt',
    verbo: 'GET',
    url: "'/notributarios/' + query.febosId"
  },
  {
    nombre: 'cl_obtener_documento',
    verbo: 'GET',
    url: "'/documentos/' + query.febosId"
  },
  {
    nombre: 'cl_obtener_documento_test',
    verbo: 'GET',
    url: "'/configuracion/configuraciones/pruebas/plantilla'"
  },
  {
    nombre: 'cl_obtener_flujo',
    verbo: 'GET',
    url: "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId"
  },
  {
    nombre: 'cl_obtener_resumen_ejecucion_flujo',
    verbo: 'GET',
    url:
      "'/empresas/' + query.empresaId + '/flujos/' + query.flujoId + '/ejecuciones/' + query.ejecucionId"
  },
  {
    nombre: 'cl_adm_configuracion_listar',
    verbo: 'GET',
    url: "'/configuracion/configuraciones'"
  },
  {
    nombre: 'cl_adm_configuracion_guardar',
    verbo: 'POST',
    url: "'/configuracion/configuraciones'"
  },
  {
    nombre: 'cl_portal_web_config',
    verbo: 'GET',
    url: "'/configuracion/portales/{dominio}'"
  },
  {
    nombre: 'cl_realizar_accion_masiva',
    verbo: 'POST',
    url: "'/herramientas/masivas'"
  },
  {
    nombre: 'cl_recuperar_clave',
    verbo: 'POST',
    url: "'/usuarios/login/recuperar'"
  },
  {
    nombre: 'cl_reencolar_tarea_background',
    verbo: 'POST',
    url: "'/herramientas/tareas'"
  },
  {
    nombre: 'cl_reenviar_invitacion_proveedor',
    verbo: 'PUT',
    url: "'/usuarios/invitar'"
  },
  {
    nombre: 'cl_sii_consulta_dte',
    verbo: 'GET',
    url: "'/sii/dte/consulta'"
  },
  {
    nombre: 'cl_sii_consulta_historial_eventos',
    verbo: 'GET',
    url: "'/sii/dte/eventos'"
  },
  { nombre: 'cl_sii_obtener_token', verbo: 'PUT', url: "''" },
  {
    nombre: 'cl_sii_solicitar_correo',
    verbo: 'PUT',
    url: "'/sii/dte/reenviocorreo'"
  },
  {
    nombre: 'cl_solicitar_dte_no_recibido',
    verbo: 'PUT',
    url: "'/documentos/'+ query.febosId +'/solicitudes'"
  },
  {
    nombre: 'cl_subir_certificado',
    verbo: 'PUT',
    url: "'/empresas/' + query.empresaId + '/certificado'"
  },
  {
    nombre: 'cl_ver_novedad',
    verbo: 'GET',
    url: "'/novedades/' + query.novedadId"
  },
  {
    nombre: 'cl_clave_unica',
    verbo: 'POST',
    url: "'/herramientas/webhooks/claveunica'"
  },
  {
    nombre: 'cl_obtener_indicadores_gestion',
    verbo: 'GET',
    url: "'/documentos/indicadoresgestion'"
  },
  {
    nombre: 'cl_obtener_indicadores_emision',
    verbo: 'GET',
    url: "'/documentos/indicadoresemision'"
  },
  {
    nombre: 'cl_obtener_url_prefirmada',
    verbo: 'GET',
    url: "'/herramientas/archivos'"
  },
  {
    nombre: 'cl_obtener_proyecto',
    verbo: 'GET',
    url: "'/portafolio/proyectos/'+query.proyectoId"
  },
  {
    nombre: 'cl_correo_envio_portafolio',
    verbo: 'POST',
    url: "'/portafolio/acciones/envio'"
  },
  {
    nombre: 'cl_listar_bitacora_portafolio',
    verbo: 'GET',
    url: "'/portafolio/boletas/'+query.documentoId+'/bitacora'"
  },
  {
    nombre: 'cl_obtener_documento_portafolio',
    verbo: 'GET',
    url: "'/portafolio/boletas/' + query.documentoId"
  },
  {
    nombre: 'cl_crear_documento_portafolio',
    verbo: 'POST',
    url: "'/portafolio/boletas/'"
  },
  {
    nombre: 'cl_obtener_archivo_privado',
    verbo: 'GET',
    url: "'/herramientas/archivos/privados'"
  },
  {
    nombre: 'io_rss',
    verbo: 'GET',
    url: "'/herramientas/rss?url=' + query.url"
  },
  { nombre: 'cl_listar_webhook', verbo: 'GET', url: "'/webhook'" },

  {
    nombre: 'cl_copiar_roles_usuario_a_usuarios',
    verbo: 'POST',
    url: "'/usuarios/roles'"
  },
  { nombre: 'cl_noticias_listado', verbo: 'GET', url: "'/noticias'" },
  { nombre: 'cl_noticias_crear', verbo: 'POST', url: "'/noticias'" },
  { nombre: 'cl_noticias_eliminar', verbo: 'DELETE', url: "'/noticias'" },
  { nombre: 'cl_crear_partner', verbo: 'POST', url: "'/partners'" },
  { nombre: 'cl_listar_partners', verbo: 'GET', url: "'/partners'" },
  { nombre: 'cl_migrar_empresas_entre_ambientes', verbo: 'PUT', url: "'/empresas/migracion'" },
  { nombre: 'cl_actualizar_partner', verbo: 'PUT', url: "'/partners'" },
  { nombre: 'cl_migracion_documentos_entre_ambientes', verbo: 'PUT', url: "'/documentos/migracion'" },
  { nombre: 'cl_actualizar_partner', verbo: 'PUT', url: "'/partners'" },
  {
      nombre: 'cl_caf_listar_folios',
      verbo: 'GET',
      url: "'/empresas/' + query.empresaId + '/folios'"
  },
  {
      nombre: 'cl_caf_anular_folios',
      verbo: 'POST',
      url: "'/empresas/' + query.empresaId + '/caf'"
  },
  {
      nombre: 'cl_caf_cargar_caf',
      verbo: 'PUT',
      url: "'/empresas/' + query.empresaId + '/caf'"
  },
  {
      nombre: 'cl_caf_cargar_caf_store',
      verbo: 'PUT',
      url: "'/empresas/' + query.empresaId + '/cafstore'"
  },
  {
      nombre: 'cl_caf_listar_cafs',
      verbo: 'GET',
      url: "'/empresas/' + query.empresaId + '/caf'"
  },
  {
      nombre: 'cl_caf_listar_cafs_store',
      verbo: 'GET',
      url: "'/empresas/' + query.empresaId + '/cafstore'"
  },
  {
      nombre: 'cl_caf_listar_cafs_store_delivery',
      verbo: 'GET',
      url: "'/empresas/' + query.empresaId + '/cafstore/' + query.cafId"
  },
  {
      nombre: 'cl_caf_listar_folios',
      verbo: 'GET',
      url: "'/empresas/' + query.empresaId + '/folios'"
  },
  {
    nombre: 'cl_listar_referencias_dnt',
    verbo: 'GET',
    url: "'/documentos/' + query.febosId + '/referenciasdnt'"
  },
  { nombre: 'cl_crear_caja', verbo: 'POST', url: "'/empresas/' + query.empresaId + '/sucursales/' + query.sucursalId + '/cajas'" },
  { nombre: 'cl_modificar_caja', verbo: 'PUT', url: "'/empresas/' + query.empresaId + '/sucursales/' + query.sucursalId + '/cajas/' + query.cajaId" },
  { nombre: 'cl_eliminar_caja', verbo: 'DELETE', url: "'/empresas/' + query.empresaId + '/sucursales/' + query.sucursalId + '/cajas/' + query.cajaId" },
  { nombre: 'cl_obtener_caja', verbo: 'GET', url: "'/empresas/' + query.empresaId + '/sucursales/' + query.sucursalId + '/cajas/' + query.cajaId" },
  { nombre: 'cl_listar_cajas', verbo: 'GET', url: "'/empresas/' + query.empresaId + '/sucursales/' + query.sucursalId + '/cajas'" }
]
