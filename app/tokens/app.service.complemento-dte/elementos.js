{
    atajosDeRangosDeFecha: {
      '[hoy]': {
        desde: moment().format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[ayerYhoy]': {
        desde: moment()
          .subtract(1, 'days')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[estaSemana]': {
        desde: moment()
          .startOf('week')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[las2UltimasSemanas]': {
        desde: moment()
          .startOf('week')
          .subtract(1, 'week')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[las3UltimasSemanas]': {
        desde: moment()
          .startOf('week')
          .subtract(2, 'weeks')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos5Dias]': {
        desde: moment()
          .subtract(5, 'days')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos10Dias]': {
        desde: moment()
          .subtract(10, 'days')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos15Dias]': {
        desde: moment()
          .subtract(15, 'days')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos20Dias]': {
        desde: moment()
          .subtract(20, 'days')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos30Dias]': {
        desde: moment()
          .subtract(30, 'days')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[esteMes]': {
        desde: moment()
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[elMesPasado]': {
        desde: moment()
          .subtract(1, 'month')
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment()
          .subtract(1, 'month')
          .endOf('month')
          .format('YYYY-MM-DD')
      },
      '[elMesAntesPasado]': {
        desde: moment()
          .subtract(2, 'month')
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment()
          .subtract(2, 'month')
          .endOf('month')
          .format('YYYY-MM-DD')
      },
      '[losUltimos2Meses]': {
        desde: moment()
          .subtract(1, 'month')
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos3Meses]': {
        desde: moment()
          .subtract(2, 'month')
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos4Meses]': {
        desde: moment()
          .subtract(3, 'month')
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos5Meses]': {
        desde: moment()
          .subtract(4, 'month')
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      },
      '[losUltimos6Meses]': {
        desde: moment()
          .subtract(5, 'month')
          .startOf('month')
          .format('YYYY-MM-DD'),
        hasta: moment().format('YYYY-MM-DD')
      }
    },
    posiblesCampos: [
      { campo: 'tipoDocumento', nombre: 'Tipo de documento', tipoFiltro: 'tag', opciones: [] },
      { campo: 'folio', nombre: 'Folio', tipoFiltro: 'tag' },
      { campo: 'rutEmisor', nombre: 'Rut Emisor', tipoFiltro: 'tag' },
      { campo: 'razonSocialEmisor', nombre: 'Razón Social Emisor', tipoFiltro: 'texto' },
      { campo: 'rutReceptor', nombre: 'Rut Receptor', tipoFiltro: 'tag' },
      { campo: 'razonSocialReceptor', nombre: 'Razón Social Receptor', tipoFiltro: 'texto' },
      { campo: 'rutCesionario', nombre: 'Rut Cesionario', tipoFiltro: 'tag' },
      {
        campo: 'razonSocialCesionario',
        nombre: 'Razón Social Cesionario',
        tipoFiltro: 'texto'
      },
      { campo: 'fechaCesion', nombre: 'Fecha de Cesión', tipoFiltro: 'fecha' },
      { campo: 'codigoSii', nombre: 'Codigo SII', tipoFiltro: 'tag', opciones: [] },
      { campo: 'fechaEmision', nombre: 'Fecha de Emisión', tipoFiltro: 'fecha' },
      { campo: 'fechaRecepcion', nombre: 'Fecha Recepción Febos', tipoFiltro: 'fecha' },
      { campo: 'fechaRecepcionSii', nombre: 'Fecha Recepción SII', tipoFiltro: 'fecha' },
      { campo: 'estadoComercial', nombre: 'Estado Comercial', tipoFiltro: 'tag', opciones: [] },
      { campo: 'estadoSii', nombre: 'Estado SII', tipoFiltro: 'tag', opciones: [] },
      {
        campo: 'fechaReciboMercaderia',
        nombre: 'Fecha Recibo Mercaderías',
        tipoFiltro: 'fecha'
      },
      { campo: 'formaDePago', nombre: 'Forma de pago', tipoFiltro: 'tag', opciones: [] },
      { campo: 'contacto', nombre: 'Contacto', tipoFiltro: 'texto' },
      { campo: 'correoReceptor', nombre: 'Correo receptor', tipoFiltro: 'texto' },
      { campo: 'codigoSii', nombre: 'Codigo Respuesta SII', tipoFiltro: 'tag', opciones: [] },
      { campo: 'fechaCesion', nombre: 'Fecha de Cesión', tipoFiltro: 'fecha' }
    ]
  };