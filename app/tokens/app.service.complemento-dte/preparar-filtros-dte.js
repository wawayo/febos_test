function prepararFiltrosDte(query, app, categoria, vista) {
    var filtros = {};
    if (app == 'cloud' && categoria === 'emitidos') {
      filtros.rutEmisor = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
    }

    if (app == 'cloud' && categoria === 'recibidos') {
      filtros.rutReceptor = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
    }

    if (app == 'proveedores' && categoria === 'emitidos') {
      filtros.rutEmisor = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
      filtros.rutReceptor = $localStorage.sesiones[$localStorage.usuarioActual].cliente.iut;
    }

    if (app == 'cliente' && categoria === 'recibidos') {
      filtros.rutReceptor = $localStorage.sesiones[$localStorage.usuarioActual].empresa.iut;
      filtros.rutEmisor = $localStorage.sesiones[$localStorage.usuarioActual].proveedor.iut;
    }

    var tieneFiltrosDeFecha = false;

    if (vista == 'referencia') {
      tieneFiltrosDeFecha = true;
    }

    if (typeof query.filtros !== 'undefined') {
      var partes = query.filtros.split('|');
      for (var i = 0; i < partes.length; i++) {
        var par = partes[i].includes(':') ? partes[i].split(':') : partes[i].split('$');
        var nombreFiltro = par[0];
        var valorFiltro = par[1];
        console.log(nombreFiltro + ' = ' + valorFiltro);
        if (nombreFiltro.includes('fecha')) {
          tieneFiltrosDeFecha = true;
        }
        if (
          (categoria === 'emitidos' && nombreFiltro === 'rutEmisor') ||
          (categoria === 'recibidos' && nombreFiltro === 'rutReceptor')
        ) {
          //no hacer nada, porque se setea en duro por seguridad
        } else {
          if (
            nombreFiltro != '' &&
            nombreFiltro != 'undefined' &&
            typeof nombreFiltro != 'undefined'
          ) {
            filtros[nombreFiltro] = valorFiltro;
          }
        }
        if (nombreFiltro == 'tipoDocumento') {
          var tiposDelUsuario = valorFiltro.split(',');
          var tiposFinales = [];
          for (var x = 0; x < tiposDelUsuario.length; x++) {
            if (
              vistas[app][categoria][vista].documentosDisponibles.indexOf(
                parseInt(tiposDelUsuario[x])
              ) >= 0
            ) {
              //tiposFinales.push(vistas[app][categoria][vista].documentosDisponibles[x]);
              tiposFinales.push(tiposDelUsuario[x]);
            }
          }
          if (tiposFinales.length == 0) {
            for (
              var x = 0;
              x < vistas[app][categoria][vista].documentosDisponibles.length;
              x++
            ) {
              tiposFinales.push(vistas[app][categoria][vista].documentosDisponibles[x]);
            }
          }
          filtros[nombreFiltro] = tiposFinales.join(',');
        }
      }
    }
    console.log(vistas);

    if (!tieneFiltrosDeFecha) {
      if (categoria === 'emitidos') {
        filtros.fechaCreacion = '[losUltimos30Dias]';
      } else {
        filtros.fechaRecepcionSii = '[losUltimos30Dias]';
      }
    }
    console.log('filtros', filtros);
    //valores POR DEFECTO SI ES QUE NO VIENEN
    console.log('app ' + app + ' categoria ' + categoria + ' vista ' + vista);
    for (var i = 0; i < vistas[app][categoria][vista].filtrosPorDefecto.length; i++) {
      var campo = vistas[app][categoria][vista].filtrosPorDefecto[i].campo;
      var valor = vistas[app][categoria][vista].filtrosPorDefecto[i].valor;
      if (typeof filtros[campo] == 'undefined') {
        filtros[campo] = valor;
      }
    }
    // filtros fijos
    for (var i = 0; i < vistas[app][categoria][vista].filtrosFijos.length; i++) {
      var campo = vistas[app][categoria][vista].filtrosFijos[i].campo;
      var valor = vistas[app][categoria][vista].filtrosFijos[i].valor;
      filtros[campo] = valor;
    }

    console.log('filtros2', filtros);
    var comoString = '';
    for (var filtro in filtros) {
      comoString += '|' + filtro + ':' + filtros[filtro];
    }

    console.log('comoString', comoString);

    return comoString.substring(1);
  };