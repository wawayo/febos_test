febosApp.directive('leerArchivoDetect', function ($parse) {
  return {
    restrict: 'A',
    scope: false,
    link: function (scope, element, attrs) {
      var fn = $parse(attrs.leerArchivoDetect);
      console.log('Entro a la directiva! leer archivo', fn);
      element.on('change', function (onChangeEvent) {
        var reader = new FileReader();
        reader.onload = function (onLoadEvent) {
          scope.$apply(function () {
            var codes = new Uint8Array(onLoadEvent.target.result);
            var encoding = Encoding.detect(codes);
            var unicodeString = Encoding.convert(codes, {
              to: 'unicode',
              from: encoding,
              type: 'string'
            });
            console.log('llamando a funcion que procesa !!  ---  ', attrs.leerArchivo, unicodeString);
            fn(scope, {$fileContent: unicodeString});
          });
        };

        reader.readAsArrayBuffer((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
      });
    }
  };
});
