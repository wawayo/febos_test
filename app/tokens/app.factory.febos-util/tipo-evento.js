function tipoEvento(codigo, mostrarCodigo, versionCompacta) {
  var nombre = '';
  versionCompacta = typeof versionCompacta == 'undefined' ? false : versionCompacta;
  switch (codigo) {
    case 1:
      nombre = versionCompacta ? 'Recep. archivo int. TXT' : 'RECEPCION_ARCHIVO_INTEGRACION_TXT';
      break;
    case 2:
      nombre = versionCompacta ? 'Recep. archivo int. XML' : 'RECEPCION_ARCHIVO_INTEGRACION_XML';
      break;
    case 3:
      nombre = versionCompacta ? 'Error integración' : 'ERROR_INTEGRACION';
      break;
    case 4:
      nombre = versionCompacta ? 'Doc. preprocesado' : 'DOCUMENTO_PREPROCESADO';
      break;
    case 5:
      nombre = versionCompacta ? 'Doc. firmado' : 'DOCUMENTO_FIRMADO';
      break;
    case 6:
      nombre = versionCompacta ? 'Doc. enviado EAI' : 'DOCUMENTO_ENVIADO_EAI';
      break;
    case 7:
      nombre = versionCompacta ? 'Doc. recep. EAI' : 'DOCUMENTO_RECEPCIONADO_EAI';
      break;
    case 8:
      nombre = versionCompacta ? 'Error al enviar a EAI' : 'ERROR_AL_ENVIAR_A_EAI';
      break;
    case 9:
      nombre = versionCompacta ? 'Error resp. EAI' : 'ERROR_RESPUESTA_EAI';
      break;
    case 10:
      nombre = versionCompacta ? 'Doc. aceptado EAI' : 'DOCUMENTO_ACEPTADO_EAI';
      break;
    case 11:
      nombre = versionCompacta ? 'Doc. aceptado reparos EAI' : 'DOCUMENTO_ACEPTADO_REPAROS_EAI';
      break;
    case 12:
      nombre = versionCompacta ? 'Doc. rechacho EAI' : 'DOCUMENTO_RECHAZADO_EAI';
      break;
    case 13:
      nombre = versionCompacta ? 'Doc. enviado a receptor' : 'DOCUMENTO_ENVIADO_A_RECEPTOR';
      break;
    case 14:
      nombre = versionCompacta
        ? 'Doc. enviado a direccion personalizada'
        : 'DOCUMENTO_ENVIADO_A_DIRECCION_PERSONALIZADA';
      break;
    case 15:
      nombre = versionCompacta ? 'Doc. recibido por receptor' : 'DOCUMENTO_RECIBIDO_POR_RECEPTOR';
      break;
    case 16:
      nombre = versionCompacta ? 'Envio rechazado por receptor' : 'ENVIO_RECHAZADO_POR_RECEPTOR';
      break;
    case 17:
      nombre = versionCompacta
        ? 'Doc. aceptado comercialmente por receptor'
        : 'DOCUMENTO_ACEPTADO_COMERCIALEMENTE_POR_RECEPTOR';
      break;
    case 18:
      nombre = versionCompacta
        ? 'Doc. aceptado reparos comercialmente por receptor'
        : 'DOCUMENTO_ACEPTADO_REPAROS_COMERCIALEMENTE_POR_RECEPTOR';
      break;
    case 19:
      nombre = versionCompacta
        ? 'Doc. rechazado comercialmente por receptor'
        : 'DOCUMENTO_RECHAZADO_COMERCIALEMENTE_POR_RECEPTOR';
      break;
    case 20:
      nombre = versionCompacta ? 'Envio recibo mercaderías' : 'ENVIO_RECIBO_DE_MERCADERIAS';
      break;
    case 21:
      nombre = versionCompacta
        ? 'Envio recibo mercaderías parcial'
        : 'ENVIO_RECIBO_DE_MERCADERIAS_PARCIAL';
      break;
    case 22:
      nombre = versionCompacta ? 'Devolución total mercaderías' : 'DEVOLUCION_TOTAL_DE_MERCADERIAS';
      break;
    case 23:
      nombre = versionCompacta
        ? 'Devolución parcial de mercadería'
        : 'DEVOLUCION_PARCIAL_DE_MERCADERIAS';
      break;
    case 24:
      nombre = versionCompacta ? 'Comentario de usuario' : 'COMENTARIO_DE_USUARIO';
      break;
    case 25:
      nombre = versionCompacta ? 'Asig. Folio' : 'ASIGNACION_FOLIO';
      break;
    case 26:
      nombre = versionCompacta ? 'Asig. CAF' : 'ASIGNACION_CAF';
      break;
    case 27:
      nombre = versionCompacta ? 'Almacenamiento XML Doc.' : 'ALMACENAMIENTO_XML_DOCUMENTO';
      break;
    case 28:
      nombre = versionCompacta ? 'Visualización XML' : 'VISUALIZACION_XML';
      break;
    case 29:
      nombre = versionCompacta ? 'Visualización Básica' : 'VISUALIZACION_BASICA';
      break;
    case 30:
      nombre = versionCompacta ? 'Visualización imagen tipo 0' : 'VISUALIZACION_IMAGEN_TIPO_0';
      break;
    case 31:
      nombre = versionCompacta ? 'Visualización imagen tipo 1' : 'VISUALIZACION_IMAGEN_TIPO_1';
      break;
    case 32:
      nombre = versionCompacta ? 'Visualización imagen tipo 2' : 'VISUALIZACION_IMAGEN_TIPO_2';
      break;
    case 33:
      nombre = versionCompacta ? 'Visualización imagen tipo 3' : 'VISUALIZACION_IMAGEN_TIPO_3';
      break;
    case 34:
      nombre = versionCompacta ? 'Visualización imagen tipo 4' : 'VISUALIZACION_IMAGEN_TIPO_4';
      break;
    case 35:
      nombre = versionCompacta ? 'Visualización imagen tipo 5' : 'VISUALIZACION_IMAGEN_TIPO_5';
      break;
    case 36:
      nombre = versionCompacta ? 'Visualización imagen tipo 6' : 'VISUALIZACION_IMAGEN_TIPO_6';
      break;
    case 37:
      nombre = versionCompacta ? 'Visualización imagen tipo 7' : 'VISUALIZACION_IMAGEN_TIPO_7';
      break;
    case 38:
      nombre = versionCompacta
        ? 'Recep. DTE sin verificar en SII'
        : 'RECEPCION_DTE_SIN_VERIFICAR_EN_SII';
      break;
    case 39:
      nombre = versionCompacta
        ? 'DTE recibido aceptado por SII'
        : 'DTE_RECIBIDO_ACEPTADO_POR_EL_SII';
      break;
    case 40:
      nombre = versionCompacta
        ? 'DTE rechazado aceptado por SII'
        : 'DTE_RECIBIDO_RECHAZADO_POR_EL_SII';
      break;
    case 41:
      nombre = versionCompacta ? 'Almacenamiento adjunto Doc.' : 'ALMACENAMIENTO_ADJUNTO_DOCUMENTO';
      break;
    case 42:
      nombre = versionCompacta ? 'Doc. consultado EAI' : 'DOCUMENTO_CONSULTADO_EAI';
      break;
    case 43:
      nombre = versionCompacta ? 'Solicitud DTE no recibido' : 'SOLICITUD_DTE_NO_RECIBIDO';
      break;
    case 44:
      nombre = versionCompacta ? 'Doc. Cedido' : 'DOCUMENTO_CEDIDO';
      break;
    case 45:
      nombre = versionCompacta ? 'Cesion Rechazada' : 'CESION_RECHAZADA';
      break;
    case 46:
      nombre = versionCompacta ? 'Información de pago' : 'INFORMACION_DE_PAGO';
      break;
    case 47:
      nombre = versionCompacta ? 'DTE recibido en validación' : 'DTE_RECIBIDO_EN_VALIDACION';
      break;
  }
  return mostrarCodigo == true ? '(' + codigo + ') ' + nombre : nombre;
}
