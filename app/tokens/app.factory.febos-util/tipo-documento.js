function tipoDocumento(codigo, mostrarCodigo, versionCompacta) {
  var nombre = '';
  versionCompacta = typeof versionCompacta == 'undefined' ? false : versionCompacta;
  switch (codigo) {
    /** Borradores **/
    case -33:
      nombre = versionCompacta ? 'F.E. Afecta (B)' : 'Factura A. Elect. (Borrador)';
      break;
    case -34:
      nombre = versionCompacta ? 'F.E. Exenta (B)' : 'Factura E. Elect. (Borrador)';
      break;
    case -39:
      nombre = versionCompacta ? 'Boleta (E)(B)' : 'Boleta Elect. (Borrador)';
      break;
    case -41:
      nombre = versionCompacta ? 'Boleta Exe. (E)(B)' : 'Boleta E. Elect. (Borrador)';
      break;
    case -43:
      nombre = versionCompacta ? 'F.E. Liq. (B)' : 'Factura Liq. Elect. (Borrador)';
      break;
    case -46:
      nombre = versionCompacta ? 'F.E. Compra. (B)' : 'Factura Compra Elect. (Borrador)';
      break;
    case -52:
      nombre = versionCompacta ? 'Guía Desp. (E)(B)' : 'Guía de Depacho Elect. (Borrador)';
      break;
    case -56:
      nombre = versionCompacta ? 'N.D. (E)(B)' : 'N. Déb. Elect. (Borrador)';
      break;
    case -61:
      nombre = versionCompacta ? 'N.C. (E)(B)' : 'N. Créd. Elect. (Borrador)';
      break;
    case -110:
      nombre = versionCompacta ? 'F.E. Exp. (E)(B)' : 'Factura Exp. Elect. (Borrador)';
      break;
    case -111:
      nombre = versionCompacta ? 'N.D. Exp. (E)(B)' : 'N. Déb. Exp. Elect. (Borrador)';
      break;
    case -112:
      nombre = versionCompacta ? 'N.C. Exp. (E)(B)' : 'N. Créd. Exp. Elect. (Borrador)';
      break;
    case 33:
      nombre = versionCompacta ? 'F.E. Afecta' : 'Factura Afecta Electrónica';
      break;
    case 34:
      nombre = versionCompacta ? 'F.E. Exenta' : 'Factura Exenta Electrónica';
      break;
    case 39:
      nombre = versionCompacta ? 'Boleta (E)' : 'Boleta Electrónica';
      break;
    case 41:
      nombre = versionCompacta ? 'Boleta Exe. (E)' : 'Boleta Exenta Electrónica';
      break;
    case 43:
      nombre = versionCompacta ? 'F.E. Liq.' : 'Factura Liquidación Electrónica';
      break;
    case 46:
      nombre = versionCompacta ? 'F.E. Compra.' : 'Factura Compra Electrónica';
      break;
    case 52:
      nombre = versionCompacta ? 'Guía Desp. (E)' : 'Guía de Depacho Electrónica';
      break;
    case 56:
      nombre = versionCompacta ? 'N.D. (E)' : 'Nota de Débito Electrónica';
      break;
    case 61:
      nombre = versionCompacta ? 'N.C. (E)' : 'Nota de Crédito Electrónica';
      break;
    case 110:
      nombre = versionCompacta ? 'F.E. Exp. (E)' : 'Factura Exportación Electrónica';
      break;
    case 111:
      nombre = versionCompacta ? 'N.D. Exp. (E)' : 'Nota de Débito Exportación Electrónica';
      break;
    case 112:
      nombre = versionCompacta ? 'N.C. Exp. (E)' : 'Nota de Crédito Exportación Electrónica';
      break;
    case 66:
      nombre = versionCompacta ? 'B. Honorarios (E)' : 'Boleta Honorarios Electrónica';
      break;
  }
  return mostrarCodigo == true ? '(' + codigo + ') ' + nombre : nombre;
}
