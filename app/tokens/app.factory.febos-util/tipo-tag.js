function tipoTag(codigo) {
  var nombre = '';

  switch (codigo) {
    case 'bitacora.mensaje':
      nombre = 'Mensaje Bitacora';
      break;
    case 'DTE.Encabezado.Emisor.RUTEmisor':
      nombre = 'Rut emisor';
      break;
    case 'DTE.Encabezado.IdDoc.TipoDTE':
      nombre = 'Tipo DTE';
      break;
    case 'DTE.Encabezado.IdDoc.Folio':
      nombre = 'Folio';
      break;
    case 'DTE.Encabezado.IdDoc.FchEmis':
      nombre = 'Fecha Emision';
      break;
    case 'DTE.Encabezado.IdDoc.IndServicio':
      nombre = 'Indicador Servicio';
      break;
    case 'DTE.Encabezado.IdDoc.PeriodoDesde':
      nombre = 'Periodo desde';
      break;
    case 'DTE.Encabezado.IdDoc.PeriodoHasta':
      nombre = 'Periodo hasta';
      break;
    case 'DTE.Encabezado.Emisor.RznSoc':
      nombre = 'Razon Social emisor';
      break;
    case 'DTE.Encabezado.Emisor.GiroEmis':
      nombre = 'Giro emisor';
      break;
    case 'DTE.Encabezado.Emisor.Telefono':
      nombre = 'Telefono emisor';
      break;
    case 'DTE.Encabezado.Emisor.Acteco':
      nombre = 'Acteco emisor';
      break;
    case 'DTE.Encabezado.Emisor.Sucursal':
      nombre = 'Sucursal emisor';
      break;
    case 'DTE.Encabezado.Emisor.DirOrigen':
      nombre = 'Direccion origen emisor';
      break;
    case 'DTE.Encabezado.Emisor.CmnaOrigen':
      nombre = 'Comuna origen emisor';
      break;
    case 'DTE.Encabezado.Receptor.RUTRecep':
      nombre = 'Rut receptor';
      break;
    case 'DTE.Encabezado.Receptor.CdgIntRecep':
      nombre = 'Codigo interno receptor';
      break;
    case 'DTE.Encabezado.Receptor.RznSocRecep':
      nombre = 'Razon social receptor';
      break;
    case 'DTE.Encabezado.Receptor.GiroRecep':
      nombre = 'Giro receptor';
      break;
    case 'DTE.Encabezado.Receptor.Contacto':
      nombre = 'Contacto receptor';
      break;
    case 'DTE.Encabezado.Receptor.DirRecep':
      nombre = 'Direccion receptor';
      break;
    case 'DTE.Encabezado.Receptor.CmnaRecep':
      nombre = 'Comuna receptor';
      break;
    case 'DTE.Encabezado.Receptor.CiudadRecep':
      nombre = 'Ciudad receptor';
      break;
    case 'DTE.Encabezado.Receptor.DirPostal':
      nombre = 'Direccion postal receptor';
      break;
    case 'DTE.Encabezado.Receptor.CmnaPostal':
      nombre = 'Comuna postal receptor';
      break;
    case 'DTE.Encabezado.Receptor.CiudadPostal':
      nombre = 'Ciudad postal receptor';
      break;
    case 'DTE.Encabezado.Totales.MntNeto':
      nombre = 'Monto neto';
      break;
    case 'DTE.Encabezado.Totales.MntExe':
      nombre = 'Monto exento';
      break;
    case 'DTE.Encabezado.Totales.IVA':
      nombre = 'Iva';
      break;
    case 'DTE.Encabezado.Totales.MntTotal':
      nombre = 'Monto total';
      break;
    case 'DTE.Encabezado.Totales.VlrPagar':
      nombre = 'Valor a pagar';
      break;
  }

  return nombre;
}
