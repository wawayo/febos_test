{
    cloud: {
      emisionWeb: {
        todos: {
          titulo: 'Emision DTE',
          documentosDisponibles: [33, 34, 39, 41, 56, 61, 110, 111, 112]
        },
        // 33 Factura electrónica - 34 Factura no afecta o exenta electrónica -
        venta: {
          titulo: 'Factura Electronica',
          documentosDisponibles: [33]
        },
        ventaExenta: {
          titulo: 'Factura Exenta Electrónica',
          documentosDisponibles: [34]
        },
        // 56 Nota de débito electrónica - 61 Nota de crédito electrónica -
        notaDebito: {
          titulo: 'Nota de Débito',
          documentosDisponibles: [56]
        },
        notaCredito: {
          titulo: 'Nota de Crédito',
          documentosDisponibles: [61]
        },
        // 52 Guía de despacho electrónica -
        guiaDespacho: {
          titulo: 'Guia de Despacho',
          documentosDisponibles: [52]
        },
        // 39 Boleta electrónica - 41 Boleta exenta electrónica -
        boleta: {
          titulo: 'Boleta Electronica',
          documentosDisponibles: [39]
        },
        boletaExenta: {
          titulo: 'Boleta Exenta Electrónica',
          documentosDisponibles: [41]
        },
        // 110 Factura de exportación electrónica - 111 Nota de débito de exportación electrónica - 112 Nota de crédito de exportación electrónica -
        exportacion: {
          titulo: 'Factura de Exportacion',
          documentosDisponibles: [110]
        },
        notaDebitoExportacion: {
          titulo: 'Nota de Débito de Exportacion',
          documentosDisponibles: [111]
        },
        notaCreditoExportacion: {
          titulo: 'Nota de Crédito de Exportacion',
          documentosDisponibles: [112]
        },
        // 46 Factura de compra electrónica -
        compra: {
          titulo: 'Factura de Compra Electrónica',
          documentosDisponibles: [46]
        }
      }
    },
    proveedores: {
      emisionWeb: {
        todos: {
          titulo: 'Emision DTE',
          documentosDisponibles: [33, 34, 39, 41, 56, 61, 110, 111, 112]
        },
        venta: {
          titulo: 'Factura Electronica',
          documentosDisponibles: [33, 34]
        },
        boleta: {
          titulo: 'Boleta Electronica',
          documentosDisponibles: [39, 41]
        },
        exportacion: {
          titulo: 'Factura de Exportacion',
          documentosDisponibles: [110]
        },
        notaDebito: {
          titulo: 'Nota de Crédito',
          documentosDisponibles: [61, 112]
        },
        notaCredito: {
          titulo: 'Nota de Débito',
          documentosDisponibles: [56, 111]
        }
      }
    },
    cliente: {
      emisionWeb: {
        todos: {
          titulo: 'Emision DTE',
          documentosDisponibles: [33, 34, 39, 41, 56, 61, 110, 111, 112]
        },
        venta: {
          titulo: 'Factura Electronica',
          documentosDisponibles: [33, 34]
        },
        boleta: {
          titulo: 'Boleta Electronica',
          documentosDisponibles: [39, 41]
        },
        exportacion: {
          titulo: 'Factura de Exportacion',
          documentosDisponibles: [110]
        },
        notaDebito: {
          titulo: 'Nota de Crédito',
          documentosDisponibles: [61, 112]
        },
        notaCredito: {
          titulo: 'Nota de Débito',
          documentosDisponibles: [56, 111]
        }
      }
    }
  };